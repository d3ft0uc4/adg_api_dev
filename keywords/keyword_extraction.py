from collections import Counter
from itertools import chain, combinations
from typing import List

from nltk.stem.porter import PorterStemmer

from bing.bing_web_core import BingWebCore
from mapper.string_cleaner_tokenization.tokenizer import DICTIONARY_WORDS
from packages.utils import tokenize, pretty, ngrams

bwc = BingWebCore()


stemmer = PorterStemmer()
STEMMED_DICTIONARY_WORDS = [stemmer.stem(word) for word in DICTIONARY_WORDS]


def is_valuable(keyword: str) -> bool:
    return \
        len(keyword) >= 3 and \
        (' ' in keyword or stemmer.stem(keyword) not in STEMMED_DICTIONARY_WORDS)


def extract_keywords(raw_input: str, max_ngram: int = 4) -> List:
    bing_response = bwc.query(raw_input)
    bing_rows = bing_response.get('webPages', {}).get('value', [])[:30]

    documents = [' '.join([row['name'], row['snippet']]) for row in bing_rows]
    documents_ngrams = [
        list(chain.from_iterable(
            map(lambda ngs: ' '.join(ngs), ngrams(tokenize(doc), n))
            for n in range(1, max_ngram + 1)
        ))
        for doc in documents
    ]

    counter = Counter()
    for doc1, doc2 in combinations(documents_ngrams, 2):
        for keyword in set(doc1).intersection(set(doc2)):
            if not is_valuable(keyword):
                continue

            counter[keyword] += 1

    # # penalize keywords that are substrings of other keywords
    # for keyword1, keyword2 in permutations(counter, 2):
    #     if keyword1 in keyword2:
    #         counter[keyword1] -= counter[keyword2]

    return [(kw, cnt) for kw, cnt in counter.most_common(10) if cnt >= 4]


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('value')
    args = parser.parse_args()

    print(pretty(extract_keywords(args.value)))
