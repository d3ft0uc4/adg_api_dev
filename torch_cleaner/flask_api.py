"""Flask Api Service"""

import argparse
import json

from flask import Flask, request
from waitress import serve

from torch_cleaner.pytorch_string_cleaner import PyTorchStringCleaner

parser = argparse.ArgumentParser()
parser.add_argument('--data_dir', default='data', help="directory containing the dataset")
parser.add_argument('--host', default='0.0.0.0', help="server host")
parser.add_argument('--port', default='5000', help="server port")
parser.add_argument('--query_key', default='text', help="default query key name for GET request")
parser.add_argument('--model_dir', default='experiments/base_model', help="directory containing params.json")
parser.add_argument('--restore_file', default='best', help="name of the file in --model_dir \
                     containing weights to load")
parser.add_argument('--batch_size', default=32, type=int,
                    help='size of the batch for processing several transaction strings together with nn model')

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def info():
    if request.method == 'GET':
        text = request.args.get(args.query_key)
        results = string_cleaner.clean_string(text)
        response = json.dumps(results)

    elif request.method == 'POST':
        data = json.loads(request.data.decode())
        results = string_cleaner.clean_string(data)
        response = json.dumps(results)

    return response


if __name__ == '__main__':
    args = parser.parse_args()
    data_dir = "data"
    model_dir = "experiments/base_model"
    restore_file = "best"
    batch_size = 32
    #string_cleaner = PyTorchStringCleaner(args.data_dir, args.model_dir, args.restore_file, args.batch_size)
    model_dir = r"C:\Users\genem_wdqp7\My Documents\gitlab\torch_cleaner\experiments\base_model"
    string_cleaner = PyTorchStringCleaner(data_dir, model_dir, restore_file, batch_size)
    cleaned=string_cleaner.clean_string("Lighter Than Air Records, LLC 790 LOWELL RD., BOX 320, GROTON, MA 01450, US")
    print(cleaned[0].get('cleanedString'))
    # serve(app.wsgi_app, host=args.host, port=args.port, threads=True)

    # this will be used for development purposes instead of serve
    # app.run(host=args.port, port=args.port, debug=True)
