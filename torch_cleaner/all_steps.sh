#! /bin/bash

python build_dataset.py --augment_mult=0
python train.py --data_dir data/ --model_dir experiments/base_model

python search_hyperparams.py --data_dir data/ --parent_dir experiments/learning_rate
python synthesize_results.py --parent_dir experiments/learning_rate
python evaluate.py --data_dir data --model_dir experiments/base_model

