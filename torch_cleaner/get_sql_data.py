import pandas as pd
import sqlalchemy

engine = sqlalchemy.create_engine(
    "mysql+pymysql://adg_read_only:dev_2018@nychousingdata.cpu3eplzthqs.us-east-1.rds.amazonaws.com:3306/altdg_merchtag?charset=utf8")

connection = engine.connect()

table_names = engine.table_names()
table_1 = 'raw_cc'
# table_2 = 'raw_to_entities_tbl'

result = connection.execute(f'select * from {table_1}')


def get_dict(result):
    data = {}
    keys = result.keys()
    for key in keys:
        data[key] = []

    for item in result:
        for index, key in enumerate(keys):
            data[key].append(item[index])

    return data


data_dict = get_dict(result)

df = pd.DataFrame.from_dict(data_dict)

df.to_csv('data/raw_to_entities_tbl.csv')
