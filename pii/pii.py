"""
© 2018 Alternative Data Group. All Rights Reserved.

Module for checking input string for personal information.
Reference: https://github.com/solvvy/redact-pii/
"""

import logging
import re

logger = logging.getLogger(f'adg.{__name__}')


aptRegex = r'(apt|bldg|dept|fl|hngr|lot|pier|rm|ste|slip|trlr|unit|#)\.? *[a-z0-9-]+\b'
poBoxRegex = r'P\.? ?O\.? *Box +\d+'
roadRegex = r'(street|st|road|rd|avenue|ave|drive|dr|loop|court|ct|circle|cir|lane|ln|boulevard|blvd|way)\.?\b'
greetingRegex = r'(^|\.\s+)(dear|hi|hello|greetings|hey|hey there)'
closingRegex = r'(thx|thanks|thank you|regards|best|[a-z]+ly|[a-z]+ regards|' \
               'all the best|happy [a-z]+ing|take care|have a [a-z]+ (weekend|night|day))'

repeatingCharsRegex = r'(\1{2,})(?=.?)(\1{2,}?)?(?=.?)(\1{2,}?)?'

regexps = [
    (
        'creditCardNumber',
        re.compile(r'\d{4}[ -]?\d{4}[ -]?\d{4}[ -]?\d{4}|\d{4}[ -]?\d{6}[ -]?\d{4}\d?'),
    ),
    (
        'streetAddress',
        re.compile(r'(\\d+\\s*(\\w+ ){1,2}' + roadRegex + r'(\\s+' + aptRegex + r')?)|(' + poBoxRegex + r')',
                   re.IGNORECASE),
    ),
    (
        'zipcode',
        re.compile(r'\b\d{5}\b(-\d{4})?\b'),
    ),
    (
        'phoneNumber',
        re.compile(
            r'(\(?\+?[0-9]{1,2}\)?[-. ]?)?(\(?[0-9]{3}\)?|[0-9]{3})[-. ]?([0-9]{3}[-. ]?[0-9]{4}|\b[A-Z0-9]{7}\b)'),
    ),
    (
        'ipAddress',
        re.compile(r'(\d{1,3}(\.\d{1,3}){3}|[0-9A-F]{4}(:[0-9A-F]{4}){5}(::|(:0000)+))', re.IGNORECASE),
    ),
    (
        'usSocialSecurityNumber',
        re.compile(r'\b\d{3}[ -.]?\d{2}[ -.]?\d{4}\b'),
    ),
    (
        'emailAddress',
        re.compile(r'([a-z0-9_\-.+]+)@\w+(\.\w+)*', re.IGNORECASE),
    ),
    (
        'credentials',
        re.compile(r'(login( cred(ential)?s| info(rmation)?)?|cred(ential)?s) ?:\s*\S+\s+\/?\s*\S+', re.IGNORECASE),
    ),
    (
        'username',
        re.compile(r'(user( ?name)?|login): \S+', re.IGNORECASE),
    ),
    (
        'password',
        re.compile(r'(pass(word|phrase)?|secret): \S+', re.IGNORECASE),
    ),
    (
        'digits',
        re.compile(r'\d{3,}(.?)\d{2,}'),
    ),
    (
        'numbers after repeating chars',
        re.compile(r'(\S)(\1{2,}).?\d{2,}'),
    ),
]


def is_pii(input: str) -> dict:
    """
    Function to check input string against list of regular expressions.

    Args:
        input: input string

    Returns:
        Dict {
            'match': whether any regexp matched,
            'match_string': string that matched,
            'match_rule': which regex produced a match,
        }
    """
    if not input.strip():
        return False

    result = {
        'match': False,
    }

    for name, regexp in regexps:
        search = regexp.search(input)
        if search:
            result['match'] = True
            result['match_string'] = search[0]
            result['match_rule'] = name
            break

    return result


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('input')
    args = parser.parse_args()

    print(is_pii(args.input))
