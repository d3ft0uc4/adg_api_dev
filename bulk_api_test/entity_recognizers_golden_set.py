from cc_cleaner.clean_cc import CCCleaner
from packages.utils import get_precision_recall
from mapper.entity_recognition import *
from datetime import datetime
from .api_qa import *
from domain_tools.utils import get
from packages.utils import equal
from mapper.response import NO_MATCH

logger = logging.getLogger(f'adg.{__name__}')

er_tools = {
    'bane': bane,
    'fwfm': fwfm,
    'whois': whois,
    'ssl': ssl,
    'copyright': copyright,
    'wikipedia': wikipedia,
    'bing': company_website
}

tools_group = {
    'domain': ['bane', 'fwfm', 'whois', 'ssl', 'copyright', 'wikipedia', 'bing'],
    'merchant': ['bane', 'fwfm', 'wikipedia', 'bing']
}

cc_cleaner = CCCleaner()


def store_test_results(mode: str,
                       raw_input: str,
                       company_name: str,
                       notes: str,
                       dt: str,
                       company_name_result: str,
                       company_name_matches: int,
                       company_name_case: str,
                       score: float,
                       target: str,
                       wiki_page: str,
                       tool_name: str,
                       version: str):
    engine.execute("""
        INSERT INTO golden_set_entity_recognition_test
        VALUES (
            %(mode)s,
            %(raw_input)s,
            %(company_name)s,
            %(notes)s,
            %(datetime)s,
            %(company_name_result)s,
            %(company_name_matches)s,
            %(company_name_case)s,
            %(score)s,
            %(target)s,
            %(wiki_page)s,
            %(tool_name)s,
            %(version)s
        )
        """,
                   mode=mode,
                   raw_input=raw_input,
                   company_name=company_name,
                   notes=notes,
                   datetime=dt,
                   company_name_result=company_name_result,
                   company_name_matches=company_name_matches,
                   company_name_case=company_name_case,
                   score=float(score),
                   target=target,
                   wiki_page=wiki_page,
                   tool_name=tool_name,
                   version=version)


def clean_str(raw_input: str, mode: str):
    if not raw_input:
        logger.warning('Raw input is empty')
        return
    if mode == 'domain':
        followed_domain = get(raw_input)
        if followed_domain:
            return followed_domain.url
    elif mode == 'merchant':
        return cc_cleaner.clean(raw_input)[0]


def run_tests():
    dt = str(datetime.now())
    df_fromdb = pd.read_sql(f'SELECT * FROM {TABLE_NAME}', engine)
    correct_domains = {}
    correct_merchants = {}
    for idx, row in df_fromdb.iterrows():
        mode = row['mode']
        data = {'company_name': row['company_name'], 'ticker': row['ticker']}
        if mode == 'merchant':
            correct_merchants[row['raw_input']] = data
        elif mode == 'domain':
            correct_domains[row['raw_input']] = data

    correct_results = {
        'domain': correct_domains,
        'merchant': correct_merchants
    }
    for mode in correct_results.keys():
        for k in list(correct_results[mode].keys()):
            clean_input = clean_str(k, mode)
            if not clean_input:
                continue

            for tool_name in tools_group[mode]:
                tool = er_tools[tool_name]
                results = tool(clean_input)
                print(results)
                if not results:
                    results = [{'value':NO_MATCH}]
                for result in results:
                    correct_company_name = correct_results[mode][k]['company_name']
                    company_name = result.get('value')
                    if not company_name:
                        company_name = NO_MATCH
                    company_name = company_name.lower()
                    company_match = any(equal(alphanum(company_name), alphanum(answer))
                                for answer in correct_company_name.split('|'))

                    if not equal(company_name, NO_MATCH) and not company_match:
                        case = 'FP'
                    elif not equal(company_name, NO_MATCH) and company_match:
                        case = 'TP'
                    elif equal(company_name, NO_MATCH) and not company_match:
                        case = 'FN'
                    else:
                        case = 'TN'

                    store_test_results(mode,
                                       k,
                                       correct_company_name,
                                       '',
                                       dt,
                                       company_name.lower(),
                                       int(company_match),
                                       case,
                                       result.get('score', 0),
                                       result.get('target', ''),
                                       str(result.get('wiki_page', '')),
                                       tool_name,
                                       result.get('version',''))



if __name__ == '__main__':
    run_tests()
