import json
import logging
from argparse import ArgumentParser
from collections import defaultdict
from datetime import datetime
from typing import List

import pandas as pd

from mapper.mapper import map_inputs_to_response
from mapper.response import NO_MATCH
from mapper.settings import db_engine
from packages.utils import similar, db_execute, apply_configuration
from unofficializing.unofficializing import make_unofficial_name

logger = logging.getLogger(f'adg.{__name__}')


def store_results(
        table: str,
        datetime: str,
        mode: str,
        output_type: str,
        FP: int,
        FN: int,
        cnt_rows: int,
        num_wrong: int,
        TP: int,
        TN: int,
        precision: float,
        recall: float,
        f1_score: float,
        notes: str,
):
    logger.info('\n'.join([
        f'Completed {mode} QA on "{table}"',
        f'Datetime: {datetime}',
        f'Notes: {notes}',
        f'Field: {output_type}',
        f'',
        f'Wrong results: {num_wrong} / {cnt_rows}',
        f'Precision: {precision:.3f}, recall: {recall:.3f}, F1 score: {f1_score:.3f}',
        f'FP: {FP}, FN: {FN}, TP: {TP}, TN: {TN}',
        f'----------------------',
    ]))

    db_engine.execute("""
        INSERT INTO golden_set_test_results
        VALUES (
            %(table)s, %(mode)s, %(output_type)s, %(FP)s, %(FN)s,
            %(cnt_rows)s, %(num_wrong)s, %(datetime)s, %(TP)s, %(TN)s, %(precision)s, %(recall)s,
            %(f1_score)s, %(notes)s
        )
        """,
                      table=table, datetime=datetime, mode=mode, output_type=output_type, FP=FP, FN=FN,
                      cnt_rows=cnt_rows, num_wrong=num_wrong, TP=TP, TN=TN,
                      precision=precision, recall=recall, f1_score=f1_score, notes=notes,
                      )


def get_last_results():
    res = db_execute("""
        SELECT * FROM golden_set_test_results
        WHERE datetime = (
            SELECT MAX(datetime) FROM golden_set_test_results
        );
    """)
    return [dict(r) for r in res]


def store_results_log(raw_input: str,
                      company_name: str,
                      datetime: str,
                      ticker: str,
                      mode: str,
                      company_name_api: str,
                      ticker_api: str,
                      company_name_matches: int,
                      ticker_matches: int):
    db_engine.execute("""
        INSERT INTO golden_set_test_results_log
        VALUES (
            %(raw_input)s,
            %(company_name)s,
            %(ticker)s,
            %(mode)s,
            %(company_name_api)s,
            %(ticker_api)s,
            %(company_name_matches)s,
            %(ticker_matches)s,
            %(datetime)s
        )
        """,
                      raw_input=raw_input,
                      company_name=company_name,
                      datetime=datetime,
                      ticker=ticker,
                      mode=mode,
                      company_name_api=company_name_api,
                      ticker_api=ticker_api,
                      company_name_matches=company_name_matches,
                      ticker_matches=ticker_matches)


def alphanum(inp: str):
    return ''.join(ch for ch in inp if ch.isalnum())


def run_tests(mode, notes: str = '', configurations: List[dict] = [{}],
              table='golden_set_domain_merchant'):
    try:
        df_fromdb = pd.read_sql(f'SELECT * FROM {table} WHERE INSTR(mode, "{mode}") ORDER BY raw_input ASC', db_engine)

        correct_results = {}
        for _, row in df_fromdb.iterrows():
            correct_results[row['raw_input']] = {
                'company_name': row['company_name'],
                'ticker': row['ticker'],
            }

        for config in configurations:
            apply_configuration(config)
            logger.info('\n'.join([
                f'Running {mode} api QA',
                f'Table: {table}',
                f'Configuration: {config}',
                f'Notes: {notes}',
            ]))
            dt = str(datetime.now())

            results = {
                'company_name': defaultdict(int),
                'ticker': defaultdict(int),
            }
            cnt_rows = 0

            for i, rawin in enumerate(correct_results.keys(), start=1):
                print(f'{i}/{len(correct_results)} - {rawin}')
                try:
                    result = map_inputs_to_response([rawin], mode=mode)[0]
                except Exception:
                    logger.exception(f'{mode} API QA error for input "{rawin}"')
                    raise

                # ---- company name ----
                company_name = result['Company Name']
                correct_company_name = correct_results[rawin]['company_name'] or ''
                correct_company_names = [
                    (name if name else NO_MATCH)
                    for name in correct_company_name.split('|')
                ]
                company_match = any(
                    similar(make_unofficial_name(company_name.strip()), make_unofficial_name(answer.strip()))
                    for answer in correct_company_names
                )

                if company_match:
                    if company_name != NO_MATCH:
                        case = 'TP'
                    else:
                        case = 'TN'
                else:  # not match
                    if company_name != NO_MATCH:
                        case = 'FP'
                    else:
                        case = 'FN'

                results['company_name'][case] += 1

                # --- ticker ----
                ticker = result['Ticker'] or ''
                correct_ticker = correct_results[rawin]['ticker'] or ''
                correct_tickers = correct_ticker.split('|')
                ticker_match = any(
                    ticker.lower().strip() == answer.lower().strip()
                    for answer in correct_tickers
                )

                if ticker_match:
                    if ticker:
                        case = 'TP'
                    else:
                        case = 'TN'
                else:  # not match
                    if ticker:
                        case = 'FP'
                    else:
                        case = 'FN'

                results['ticker'][case] += 1

                # ---- save results ----
                store_results_log(
                    rawin,
                    correct_company_name,
                    dt,
                    correct_ticker,
                    mode,
                    company_name,
                    ticker,
                    int(company_match),
                    int(ticker_match),
                )
                cnt_rows += 1

            if cnt_rows:
                notes_with_config = ((f'{notes}' or '') + ' ' + (
                    f'(config: {config})' if config else '')).strip()

                for field, res in results.items():
                    res['precision'] = res['TP'] / (res['TP'] + res['FP']) if res['TP'] + res['FP'] else 1
                    res['recall'] = res['TP'] / (res['TP'] + res['FN']) if res['TP'] + res['FN'] else 1
                    res['f1_score'] = 2 * res['precision'] * res['recall'] / (res['precision'] + res['recall'])

                    store_results(
                        table,
                        dt, mode, field, res['FP'], res['FN'], cnt_rows, res['FP'] + res['FN'], res['TP'],
                        res['TN'],
                        res['precision'], res['recall'], res['f1_score'], notes_with_config,
                    )
            else:
                logger.error('No rows were processed')
    except:
        logger.exception('QA aborted')


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--mode', type=str, default=None)
    parser.add_argument('--notes', type=str, default=None)
    parser.add_argument('--table', type=str, default=None)
    parser.add_argument('--configurations', type=json.loads, default='[{}]')
    args = parser.parse_args()

    run_tests(**{k: v for k, v in vars(args).items() if v})
