import sqlalchemy
import pandas as pd
from packages.utils import db_execute
from mapper.settings import db_engine
import time
from datetime import datetime

TABLE_NAME = 'golden_set_domain_merchant'
TRUNCATE_QUERY = "TRUNCATE TABLE " + TABLE_NAME


def populate():
    # db_execute(TRUNCATE_QUERY)
    timestamp = int(time.time())
    df = pd.read_csv(
        "/Users/admin/Dropbox/ivan/golden_set_general.csv",
        parse_dates=True, infer_datetime_format=True, error_bad_lines=False, na_filter=False)
    df['datetime'] = str(datetime.now())

    df[df.columns[df.dtypes == 'object']] = df.select_dtypes(include=['object']).apply(
        lambda x: x.astype(str).str.lower()).apply(
        lambda x: x.str.encode('utf8', 'replace'))

    df.to_sql(TABLE_NAME, db_engine, if_exists='append', index=False)


if __name__ == '__main__':
    populate()
