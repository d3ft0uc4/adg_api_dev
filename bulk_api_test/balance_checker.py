from .api_qa import TABLE_NAME
import logging
import sqlalchemy
from packages.utils import similar
import pandas as pd
from mapper.settings import db_engine
from mapper.response import NO_MATCH

logger = logging.getLogger(f'adg.{__name__}')


def check():
    """ Checks whether Golden Set table is "balanced" """
    df_fromdb = pd.read_sql(f'SELECT * FROM {TABLE_NAME}', db_engine)
    correct_domains = {}
    correct_merchants = {}
    for idx, row in df_fromdb.iterrows():
        mode = row['mode']
        data = {'company_name': row['company_name'], 'ticker': row['ticker']}
        if mode == 'merchant':
            correct_merchants[row['raw_input']] = data
        elif mode == 'domain':
            correct_domains[row['raw_input']] = data

    correct_results = {
        'domain': correct_domains,
        'merchant': correct_merchants
    }

    for mode in correct_results.keys():
        lst = list(correct_results[mode].keys())
        for idx, k in enumerate(lst):
            comp_name = correct_results[mode][k]['company_name']

            for t in lst[idx + 1:]:
                if similar(k, t):
                    logger.warn(f'Raw input values "{k}" and "{t}" are similar')
                comp_name_t = correct_results[mode][t]['company_name']
                if similar(comp_name, comp_name_t) and not similar(comp_name, NO_MATCH):
                    logger.warn(f'Company names "{comp_name}" ' \
                                 f'and "{comp_name_t}" for inputs "{k}" and "{t}" are similar')

if __name__ == '__main__':
    check()
