"""
© 2018 Alternative Data Group. All Rights Reserved.

Run test script:
    python3 -m bing.bing_entity_entity "Microsoft"
"""

from mapper.settings import ENTITY_SEARCH_API_KEY

from .bing import BingAPI


class BingEntityEntity(BingAPI):
    """
    Finds entities (by filtering 'responseFilter': 'Entities') using Bing Entities API.

    The Entity Search API lets you send a search query to Bing and get back search results that include entities and places. Place results include restaurants, hotel, or other local businesses. For places, the query can specify the name of the local business or it can ask for a list (for example, restaurants near me). Entity results include persons, places, or things. Place in this context is tourist attractions, states, countries, etc.

    See MSDN docs for more info:
    https://docs.microsoft.com/en-us/rest/api/cognitiveservices/bing-entities-api-v7-reference
    """
    API_TYPE = 'entity-entity'
    URL = 'https://api.cognitive.microsoft.com/bing/v7.0/entities'

    HEADERS = {
        **BingAPI.HEADERS,
        'Ocp-Apim-Subscription-Key': ENTITY_SEARCH_API_KEY,
    }

    PAYLOAD = {
        **BingAPI.PAYLOAD,
        # 'answerCount': 2,  # No effect on entities.
        'count': 10,
        'offset': 0,
        'freshness': 'Month',
        # 'promote': 'News',  # No effect on entities.
        'textDecorations': 'false',
        'textFormat': 'Raw',
        'responseFilter': 'Entities',
    }


if __name__ == '__main__':
    import argparse
    import json

    parser = argparse.ArgumentParser()
    parser.add_argument('input')
    args = parser.parse_args()

    r = BingEntityEntity().query(args.input)
    print(json.dumps(r, indent=4))
