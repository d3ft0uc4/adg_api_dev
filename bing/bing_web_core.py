"""
© 2018 Alternative Data Group. All Rights Reserved.

Usage from console:
    python3 -m bing.bing_web_core "Microsoft"
"""

from .bing import BingAPI
from mapper.settings import BING_PROXY_TO_GOOGLE


class BingWebCore(BingAPI):
    """
    Searches using Bing Web Search API.

    The Web Search API lets you send a search query to Bing and get back search results that include links to webpages, images, and more.

    See MSDN docs for more info:
    https://docs.microsoft.com/en-us/rest/api/cognitiveservices/bing-web-api-v7-reference
    """
    API_TYPE = 'web-core'
    URL = 'https://api.cognitive.microsoft.com/bing/v7.0/search'

    PAYLOAD = {
        **BingAPI.PAYLOAD,
        # 'answerCount': 2,  # No effect on entities.
        'count': '50',
        'offset': '0',
        # 'freshness': 'Month',
        # 'promote': 'News',  # No effect on entities.
        'textDecorations': 'false',
        'textFormat': 'Raw',
    }


if BING_PROXY_TO_GOOGLE:
    from .bing_google import BingGoogle
    BingWebCore = BingGoogle


if __name__ == '__main__':
    import argparse
    import json

    parser = argparse.ArgumentParser()
    parser.add_argument('input')
    args = parser.parse_args()

    r = BingWebCore().query(args.input)
    print(json.dumps(r, indent=4))
