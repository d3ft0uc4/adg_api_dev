"""
© 2018 Alternative Data Group. All Rights Reserved.

Set of classes to perform single/bulk queries to Bing API and caching results.
"""

import inspect
import json
import logging
import os
import re
import time
import warnings
from datetime import datetime
from functools import wraps
from hashlib import md5
from typing import List

import requests
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.dialects.mysql import MEDIUMTEXT
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from mapper.settings import PARALLEL, BING_CACHE, db_engine, WEB_SEARCH_API_KEY
from mapper.settings import QUERY_IP, QUERY_LOCATION, \
    QUERY_USER_AGENT, CACHE_TABLE_NAME
from packages.utils import conditional_decorator, LOCAL, LocalsThreadPoolExecutor, db_execute

logger = logging.getLogger(f'adg.{__name__}')

Base = declarative_base()


class BingResponse(Base):
    """
    SqlAlchemy class for storing Bing json responses in database

    Attributes:
        query_hash: MD5 hash of query to Bing
        content: Bing's json response
        timestamp: Datetime of query, used to filter stale cache
    """

    __tablename__ = CACHE_TABLE_NAME

    id = Column(Integer, primary_key=True)
    query_hash = Column(String(32), index=True)
    content = Column(MEDIUMTEXT())
    timestamp = Column(DateTime)

    def __repr__(self):
        return '<Response(timestamp={}, content={})>'.format(
            self.timestamp,
            self.content[:64],
        )


class CacheResponse:
    """
    Decorator for wrapping BingAPI's `search_and_handle_failed_response` method.
    Will try to retrieve json results from database before making a real query to Bing.

    Attributes:
        Session - a fabric for creating new SqlAlchemy sessions.
            It is used to retrieve cached data from database. Will be set to none if db connection failed.
    """
    _PENDING_QUERIES = set()

    def __init__(self, expires_in):
        """
        Setup response caching.

        Args:
            expires_in: How long cached data isn't treated as stale
        """
        try:
            # Base.metadata.create_all(db_engine)  # create table if it doesn't exist
            self.Session = sessionmaker(bind=db_engine)
        except Exception:
            warnings.warn('Could not establish connection to DB, cache is disabled')
            self.Session = None
        self.expiration_td = expires_in

    def __call__(self, fn):
        @wraps(fn)
        def caching_fn(instance, input_str):
            # omit caching if there were troubles while connecting to DB
            if not self.Session:
                return fn(instance, input_str)

            query_str = instance.API_TYPE + ':' + input_str.lower()
            query_hash = md5(query_str.encode('utf-8')).hexdigest()

            # get all cached results for such a query

            # select latest non-stale cached result
            session = self.Session()
            cached_result = session.query(BingResponse).filter(
                BingResponse.query_hash == query_hash,
                BingResponse.timestamp >= datetime.now() - self.expiration_td
            ).order_by(BingResponse.timestamp.desc()).first()
            session.close()

            if cached_result:
                logger.debug(f'Using cached result for Bing query: {input_str}')
                cache_hit = True
                result = json.loads(cached_result.content)
            elif query_hash in self._PENDING_QUERIES:
                logger.debug(f'Waiting for cache to appear: {input_str}')

                for _ in range(20):
                    if query_hash not in self._PENDING_QUERIES:
                        break
                    time.sleep(0.3)

                if query_hash not in self._PENDING_QUERIES:
                    session = self.Session()
                    cached_result = session.query(BingResponse).filter(
                        BingResponse.query_hash == query_hash,
                        BingResponse.timestamp >= datetime.now() - self.expiration_td
                    ).order_by(BingResponse.timestamp.desc()).first()
                    session.close()
                    logger.debug(f'Got cached results: {input_str}')

                    cache_hit = True
                    result = json.loads(cached_result.content)
                else:
                    logger.debug(f'Stopped waiting. Getting bing results for: {input_str}')

                    cache_hit = False
                    result = fn(instance, input_str)
                    logger.debug(f'Saving query results: {input_str}')
                    session = self.Session()
                    session.add(
                        BingResponse(
                            query_hash=query_hash,
                            content=json.dumps(result),
                            timestamp=datetime.now(),
                        )
                    )
                    session.commit()
                    session.close()
                    try:
                        self._PENDING_QUERIES.remove(query_hash)
                    except KeyError:
                        pass
            else:
                # TODO: Lock wait timeout exceeded
                # cached_results.delete()  # wipe all cached rows for this query
                self._PENDING_QUERIES.add(query_hash)
                cache_hit = False
                result = fn(instance, input_str)
                logger.debug(f'Saving query results: {input_str}')
                session = self.Session()
                session.add(
                    BingResponse(
                        query_hash=query_hash,
                        content=json.dumps(result),
                        timestamp=datetime.now(),
                    )
                )
                session.commit()
                session.close()
                try:
                    self._PENDING_QUERIES.remove(query_hash)
                except KeyError:
                    # in case some waiting thread considers this one dead and it will be revived at some moment.
                    pass

            run_uid = getattr(LOCAL, 'run_uid', None)
            if run_uid:
                # log bing query to db
                root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
                stack = inspect.stack() + getattr(LOCAL, 'stack', [])
                for frame in stack:
                    s = os.path.sep
                    if not re.match(re.escape(root_dir + s + f'(bing{s}|packages{s}utils.py)'), frame.filename) and \
                            frame.filename.startswith(root_dir):
                        caller = frame.filename[len(root_dir + s):] + ':' + frame.function + f'(L{frame.lineno})'
                        break
                else:
                    logger.warning(f'Could not retrieve caller for bing query "{input_str}"')
                    caller = ''

                db_execute(
                    """
                    INSERT INTO bing_queries (uuid, caller, query, endpoint, cache_hit)
                    VALUES (%(uuid)s, %(caller)s, %(query)s, %(endpoint)s, %(cache_hit)s)
                    """,
                    uuid=run_uid,
                    query=input_str,
                    caller=caller,
                    endpoint=getattr(LOCAL, 'request', {}).get('url', ''),
                    cache_hit=cache_hit,
                )

            return result

        return caching_fn


class BingAPI:
    """
    Base class for making queries to Bing API.

    This class defines methods for single and bulk quering Bing.
    Any subclass should define `url`, `query` and `bing_api_type` methods in order to work.
    """

    API_TYPE = None
    URL = None

    HEADERS = {
        'Accept': 'application/json',
        # 'Accept-Language': 'en-US',  # Exclusive with setLang.
        # If wanting to not use cache. No other possible value for this is specified
        # 'Pragma': 'no-cache',
        'User-Agent': QUERY_USER_AGENT,
        'X-MSEdge-ClientID': '0',
        'X-MSEdge-ClientIP': QUERY_IP,
        'X-Search-Location': QUERY_LOCATION,
        'Ocp-Apim-Subscription-Key': WEB_SEARCH_API_KEY,
    }

    PAYLOAD = {
        # 'cc': 'us',  # Exclusive with mkt.
        'mkt': 'en-US',
        'responseFormat': 'JSON',
        'safeSearch': 'Moderate',
        'setLang': 'en-US',
    }

    MAX_QUERY_TRY_NUM = 5

    def bulk_query(self, inputs) -> List[dict]:
        """
        Returns the results that were found for all input strings in the input data.
        """
        with LocalsThreadPoolExecutor(
            max_workers=len(inputs) if PARALLEL else 1,
            thread_name_prefix='BingAPIExecutor',
        ) as pool:
            results = list(pool.map(self._run_single, inputs))

        return results

    def _run_single(self,  input_str):
        logger.debug(f'Querying: {input_str}')
        entity_results = self.query(input_str)
        result = {
            'input': input_str,
            'bing_api_type': self.API_TYPE,
            'query_results': entity_results
        }

        return result

    @conditional_decorator(CacheResponse(expires_in=LOCAL.settings['BING_CACHE_EXPIRATION']), BING_CACHE)
    def search_and_handle_failed_response(self, input_str: str) -> dict:
        """ Performs the search, handles failure, waits if the response was to wait """

        for _ in range(self.MAX_QUERY_TRY_NUM):
            payload = {
                **self.PAYLOAD,
                'q':  input_str.lower(),
            }
            check_web_params(payload, self.HEADERS)

            try:
                response = requests.get(self.URL, params=payload, headers=self.HEADERS)
                response.raise_for_status()
                result_json = response.json()

            except:
                logger.exception('\n'.join([
                    f'Bing query failed',
                    f'Url: {self.URL}',
                    f'Headers: {self.HEADERS}',
                    f'Params: {payload}',
                ]))
                continue

            status_code = result_json.get('statusCode', None)

            # Handle different responses.
            if status_code is None or status_code == 200:  # Successful response.
                break

            # Response failed.
            err_message = result_json['message']
            if status_code == 401:
                err_msg = f'Invalid key: {status_code} - {err_message}'

            elif status_code == 403:
                err_msg = f'Out of monthly quota: {status_code} - {err_message}'

            elif status_code == 429:
                timeout = self._extract_timeout_seconds(err_message)
                logger.error(f"429 - sleeping for {timeout} seconds and trying again")
                time.sleep(timeout)
                continue

            else:
                err_msg = f'API failure: {status_code} - {err_message}'

            logger.error(err_msg)
            raise Exception(err_msg)

        else:
            exc_msg = f'Tried to run the query {self.MAX_QUERY_TRY_NUM} times, but never succeeded'
            logger.error(exc_msg)
            raise Exception(exc_msg)

        return result_json

    def query(self, input_str: str) -> dict:
        return self.search_and_handle_failed_response(input_str)

    @staticmethod
    def _extract_timeout_seconds(err_message):
        """ Extracts how long to wait before making another query. """
        try:
            return int(re.search('in (.+?) seconds', err_message).group(1)) + 1
        except Exception as ex:
            exc_msg = f'Unable to parse in when time to try again. Original response indicated we should wait: {ex}'
            logger.error(exc_msg)
            raise Exception(exc_msg)


def check_web_params(query_dict, header_dict):
    """
    Isolated human-error-checker class.
    All methods are static and do not modify state.
    if/else mess below forgoes optimization in favor of clarity.
    """

    response_filters = ('Entities', 'Places')

    if 'cc' in query_dict.keys():
        if query_dict['cc'] and 'Accept-Language' not in header_dict:
            raise AssertionError('Attempt to use country-code without specifying language.')
        if query_dict['mkt']:
            raise ReferenceError('cc and mkt cannot be specified simultaneously')

    if 'count' in query_dict.keys():
        if int(query_dict['count']) >= 51 or int(query_dict['count']) < 0:
            raise ValueError('Count specified out of range. 50 max objects returned.')

    if 'freshness' in query_dict.keys():
        if query_dict['freshness'] not in ('Day', 'Week', 'Month'):
            raise ValueError('Freshness must be == Day, Week, or Month. Assume Case-Sensitive.')

    if 'offset' in query_dict.keys():
        if int(query_dict['offset']) < 0:
            raise ValueError('Offset cannot be negative.')

    if 'responseFilter' in query_dict.keys():
        if query_dict['responseFilter'] not in response_filters:
            raise ValueError('Improper response filter.')

    if 'safeSearch' in query_dict.keys():
        if query_dict['safeSearch'] not in ('Off', 'Moderate', 'Strict'):
            raise ValueError('safeSearch setting must be Off, Moderate, or Strict. Assume Case-Sensitive.')
        if 'X-Search-ClientIP' in query_dict.keys():
            input('Warning: You have specified both an X-Search-ClientIP header and safesearch setting\n'
                  'Please note: header takes precedence')

    if 'setLang' in query_dict.keys():
        if 'Accept-Language' in header_dict:
            raise AssertionError('Attempt to use both language header and query param.')

    if 'textDecorations' in query_dict.keys():
        if query_dict['textDecorations'].lower() not in ('true', 'false'):
            raise TypeError('textDecorations is type bool')

    if 'textFormat' in query_dict.keys():
        if query_dict['textFormat'] not in ('Raw', 'HTML'):
            raise ValueError('textFormat must be == Raw or HTML. Assume Case-Sensitive.')
