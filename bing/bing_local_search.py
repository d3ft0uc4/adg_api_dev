import logging

from .bing import BingAPI

logger = logging.getLogger(f'adg.{__name__}')


class BingLocalSearch(BingAPI):
    API_TYPE = 'local-search'
    URL = 'https://api.cognitive.microsoft.com/bing/v7.0/localbusinesses/search'

    def __init__(self):
        raise NotImplementedError()
