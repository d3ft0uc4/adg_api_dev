"""
© 2018 Alternative Data Group. All Rights Reserved.

Run test script:
    python3 -m bing.bing_web_entity "Microsoft"
"""

from mapper.settings import ENTITY_SEARCH_API_KEY

from .bing_web_core import BingWebCore


class BingWebEntity(BingWebCore):
    """
    Finds entities (by filtering 'responseFilter': 'Entities') using Bing Web Search API.
    """
    API_TYPE = 'web-entity'

    HEADERS = {
        **BingWebCore.HEADERS,
        'Ocp-Apim-Subscription-Key': ENTITY_SEARCH_API_KEY,
        **BingWebCore.PAYLOAD,
    }

    PAYLOAD = {
        **BingWebCore.PAYLOAD,
        'responseFilter': 'Entities',
    }

    # def bwc_bwe(self, input_str):
    #     """
    #     Returns both web results and any found entities for `input_str` (using ENTITY_SEARCH_API_KEY).
    #     In other words, combines results from BingWebCore and this class (BingWebEntity)
    #     :param input_str: See BingWebCore::query
    #     :return: dict of 2 dicst: 'webPages' list, and 'entities' list.
    #     """
    #
    #     bwc_response = super().query(input_str)
    #     return {
    #         'webPages': bwc_response.get('webPages', {}).get('value', []),
    #         'entities': bwc_response.get('entities', {}).get('value', [])
    #     }


if __name__ == '__main__':
    import argparse
    import json

    parser = argparse.ArgumentParser()
    parser.add_argument('input')
    args = parser.parse_args()

    r = BingWebEntity().query(args.input)
    print(json.dumps(r, indent=4))
