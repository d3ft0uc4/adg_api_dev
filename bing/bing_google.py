from urllib.parse import urlencode

from google.google_search import GoogleSearch
from .bing import BingAPI


class BingGoogle(BingAPI):
    """
    Google search API which mimics to Bing API results.
    """
    GOOGLE = GoogleSearch()

    def query(self, input_str: str) -> dict:
        results = self.GOOGLE.query(input_str)

        query = results['queries']['request'][0]['searchTerms']
        rows = results['items']

        return {
            "_type": "SearchResponse",
            "queryContext": {
                "originalQuery": query,
            },
            "webPages": {
                "webSearchUrl": "https://www.bing.com/search?" + urlencode({'q': query}),
                "totalEstimatedMatches": results['queries']['request'][0]['totalResults'],
                "value": [
                    {
                        "id": f"https://api.cognitive.microsoft.com/api/v7/#WebPages.{i}",
                        "name": row['title'],
                        "url": row['link'],
                        "isFamilyFriendly": True,
                        "displayUrl": row['displayLink'],
                        "snippet": row['snippet'],
                        "dateLastCrawled": "2019-01-01T00:00:00.0000000Z",
                        "language": "en",
                        "isNavigational": True,
                    }
                    for i, row in enumerate(rows)
                ],
                "someResultsRemoved": True,
            },
            "rankingResponse": {
                "mainline": {
                    "items": [
                        {
                            "answerType": "WebPages",
                            "resultIndex": i,
                            "value": {
                                "id": f"https://api.cognitive.microsoft.com/api/v7/#WebPages.{i}"
                            }
                        }
                        for i in range(len(rows))
                    ],
                },
            },
        }


if __name__ == '__main__':
    import argparse
    import json

    parser = argparse.ArgumentParser()
    parser.add_argument('input')
    args = parser.parse_args()

    r = BingGoogle().query(args.input)
    print(json.dumps(r, indent=4))
