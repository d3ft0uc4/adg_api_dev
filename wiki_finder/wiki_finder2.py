"""
© 2018 Alternative Data Group. All Rights Reserved.

Module for retrieving and parsing wikipedia page info.

Cli:
    python -m wiki_finder.wiki_finder2 "Dillons"
"""
import json
import logging
import re
import unicodedata
from functools import wraps
from itertools import chain
from operator import itemgetter, attrgetter
from os import path
from typing import Dict, List, Optional, Tuple, Union, Iterator

import requests
from bs4 import BeautifulSoup, SoupStrainer, NavigableString

from bane.organization_entity_check import is_organization
from bing.bing_web_core import BingWebCore
# from packages.xgboost.prediction import xgb_load_model, xgb_predict
from packages.targets import TARGETS
from unofficializing.unofficializing import make_unofficial_name
from domain_tools.clean import get_domain
from domain_tools.utils import is_visible, get, is_domain_input
from mapper.settings import TIMERS
from packages.utils import with_timer, conditional_decorator, norecurse, RecursionException, cached_method, pretty, \
    strip_accents, similar, remove_dupes, LOCAL

logger = logging.getLogger(f'adg.{__name__}')
bing = BingWebCore()

DEBUG = __name__ == '__main__'

DATA = path.join(path.dirname(path.abspath(__file__)), 'data')


def _no_edit_link(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        url = fn(*args, **kwargs)
        if url and re.search(r'action=edit', url, flags=re.IGNORECASE):
            url = None

        return url

    return wrapper


class WikiPage:
    """ Class to represent any wikipedia page """

    BAD_VALUES = ['list', 'edit', 'view']
    END_YEAR = re.compile(r'\(.+?[-–]\d{4}\)', flags=re.IGNORECASE)

    def __init__(self, url: str):
        assert 'wikipedia.org' in url
        self.url = url
        self.internal_scores = {}
        self.score = 1

    def __str__(self) -> str:
        return self.url

    def __repr__(self) -> str:
        return f'<{self.__class__.__name__}: {self.url}>'

    def __eq__(self, other: 'WikiPage') -> bool:
        return isinstance(other, WikiPage) and self.url == other.url

    @property
    @cached_method
    def response(self) -> requests.Response:
        return requests.get(self.url)

    @property
    @conditional_decorator(with_timer, TIMERS)
    @cached_method
    def content(self) -> str:
        """ Retrieves html source code for the page """
        return getattr(self.response, 'text', '')

    @property
    @conditional_decorator(with_timer, TIMERS)
    @cached_method
    def soup(self) -> BeautifulSoup:
        """ Returns parsed data from html source code """
        return BeautifulSoup(self.content, 'html.parser', parse_only=SoupStrainer('div', attrs={'id': 'content'}))

    @staticmethod
    def _get_text(element: BeautifulSoup) -> str:
        """ Gets text from BS element """
        if hasattr(element, 'get_text'):
            return element.get_text()

        return str(element)

    # @property
    # def title(self) -> str:
    #     """ Returns page's title """
    #     return self.clean_value(self.soup.find('title').text)

    def get_article_text(self) -> str:
        content = self.soup.find('div', {'id': 'bodyContent'})
        reflist = content.find('div', {'class': 'reflist'})
        if reflist:
            reflist.decompose()

        toclist = content.find('div', {'id': 'toc'})
        if toclist:
            toclist.decompose()

        text = content.get_text(separator=' ')
        text = re.sub(r'\sReferences\s.+', '', text, flags=re.IGNORECASE | re.DOTALL)

        return text

    def get_targets(self) -> List[str]:
        text = self.get_article_text()

        targets = []
        for target in TARGETS.get_all().keys():
            if re.search(rf'\b{re.escape(target)}\b', text, flags=re.I):
                targets.append(target)

        return targets

    @property
    def article_title(self) -> str:
        """ Returns article's title """
        return self.clean_value(self.soup.find('h1', {'class': 'firstHeading'}).text)

    @staticmethod
    @_no_edit_link
    def _get_href(el: BeautifulSoup) -> Optional[str]:
        """ For any soup element, return url (if not in brackets) """
        if isinstance(el, NavigableString):
            return

        if '(' in el.text:
            el = BeautifulSoup(re.sub(r'\(.*?\)', '', str(el)), 'html.parser')

        if el.has_attr('href'):
            return el['href']

        a = getattr(el, 'a', {}) or {}
        return a.get('href') or None

    @staticmethod
    def convert_key(key: str) -> str:
        if re.match(r'.*websites?$', key, flags=re.IGNORECASE):
            return 'Website'

        if re.match(r'^owners?$', key, flags=re.IGNORECASE):
            return 'Owner'

        if re.match(r'^developer\(?s?\)?', key, flags=re.IGNORECASE):
            return 'Developer'

        if re.match(r'^production compan(y|ies)$', key, flags=re.IGNORECASE):
            return 'Production company'

        if key.lower() == 'title':
            return None  # ignore this (conflict with infobox title which is also called "Title")

        return key

    @property
    @conditional_decorator(with_timer, TIMERS)
    @cached_method
    def infobox(self) -> Dict[str, List[str]]:
        """ Retrieves contents of infobox on the right of the page (if available) """
        result = dict()
        table = self.soup.find('table', class_='infobox')

        if not table:
            return result

        caption = table.select_one('caption')
        if caption:
            # now extract first text
            caption_text = [
                c if isinstance(c, str) else c.text
                for c in caption.children
                if is_visible(c) and c.name != 'br'
            ][0]
            result['Title'] = self.clean_caption(caption_text)
        else:
            result['Title'] = self.article_title

        rows = table.select('tr')
        for row in rows:
            cols = list(row.children)
            if len(cols) != 2:
                continue

            try:
                key = unicodedata.normalize('NFKD', cols[0].text.strip())
                key = self.convert_key(key)
                if not key:
                    continue
            except:
                continue

            values = cols[1]

            # Below is a very bad wiki value parser. It used to be pretty, but then it grew up to a beast (thanks Wiki!)
            # TODO: refactor the code below.

            # dive deeper if it's a hidden list
            if getattr(values, 'ul', None) and 'NavContent' in values.ul.get('class', []):
                values = values.li

            if isinstance(values, NavigableString):
                values = [(str(values), None)]

            elif values.ul:
                values = [
                    (
                        self._get_text(li),
                        self._get_href(li),
                    )
                    for li in values.ul.find_all('li') if '\n' not in self._get_text(li)
                ]

            elif values.br or '\n' in values:
                separator = 'br' if values.br else '\n'

                temp_values = []
                temp_value = ''
                temp_url = None

                values = list(values)
                for i, value in enumerate(values):
                    if (
                        (separator == 'br' and getattr(value, 'name', None) == 'br') or
                        (separator == '\n' and value == '\n')
                    ) and (
                        # don't separate values if next value starts with `(` or `[`
                        (len(values) < i + 2) or (self._get_text(values[i+1]).strip()[:1] not in ['[', '('])
                    ):
                        # if encounter br tag, start creating new value
                        temp_values.append((temp_value, temp_url))
                        temp_value = ''
                        temp_url = None

                    else:
                        temp_value += f' {self._get_text(value)}'
                        if not temp_url:
                            temp_url = self._get_href(value)

                else:
                    # append last value
                    if temp_value:
                        temp_values.append((temp_value, temp_url))

                values = temp_values

            elif values.get_text().count(',') >= 2:
                # TODO: here we lose links
                values = [(val, None) for val in re.split(r'(?<=[\w\d^]) *, *(?=[\w$])', values.get_text(strip=True))]

            else:
                values = [(
                    values.get_text(strip=True),
                    self._get_href(values)
                )]

            values = [(unicodedata.normalize('NFKD', val).strip(), url) for val, url in values]

            cleaner = f'clean_{key.replace(" ", "_")}'  # call specific field cleaner if available
            values = getattr(self, cleaner, self.clean_values)(values)
            values = [(val, url) for val, url in values if val]  # remove empty values

            result[key] = values

        return result

    @cached_method
    def get_owners(self) -> List[Tuple[str, str]]:
        """
        Extract owners (parent companies) from wiki page.
        Highest priority first.

        Returns: list of (name, url) tuples
        """
        result = []

        # 1) extract from infobox
        infobox = self.infobox
        owners = chain(
            infobox.get('Successor', []),
            infobox.get('Parent', []),
            infobox.get('Owner', []),
            infobox.get('Developer', []),
            infobox.get('Publisher', []),
            infobox.get('Production company', []),
            infobox.get('Distributed by', []),
        )

        # 1.1) remove crap like "Simpsons family" (see https://en.wikipedia.org/wiki/JAB_Holding_Company)
        owners = [
            owner for owner in owners
            if not re.match(r'.+ family$', owner[0], flags=re.IGNORECASE)
        ]
        result += owners

        # 2) extract from text
        # https://en.wikipedia.org/wiki/Discovery_Cove: "owned and operated by"
        owned_by = self.soup.find(text=re.compile(r'\b(owned|acquired) (.*?)by\b'))
        if owned_by:
            link = owned_by.find_next('a')
            if link and 'href' in link and not '#' in link['href']:
                result.append((link.text, link['href']))

        return remove_dupes(result, key=itemgetter(0), comp_fn=similar)

    MAX_OWNERS_DEPTH = 3

    def iter_owners(self, include_self: bool = True) -> Iterator[Union['WikiPage', str, list]]:
        """
        Safely retrieve owners pages without infinite recursion.
        If multiple owners exist, return major owner first.
        """
        visited_urls = []

        if include_self:
            visited_urls.append(self.url)
            yield self

        i = 0
        page = self
        while i < self.MAX_OWNERS_DEPTH:
            owners = page.get_owners()
            if not owners:
                break

            # convert to WikiPage where possible
            pages = []
            for name, url in owners:
                if url and not self.url.startswith(url.split('#')[0]) and ('wikipedia.org' in url):
                    wiki_page = WikiPage(url)
                    if wiki_page.infobox.get('Title'):
                        pages.append(wiki_page)
                        continue

                pages.append(name)

            if len(pages) > 1:
                yield pages
                return

            # single parent - follow it
            owner = pages[0]
            pages = self.from_str(owner) if isinstance(owner, str) else [owner]
            page = pages[0] if pages else None

            if not page or page.url in visited_urls:
                yield owner
                return

            visited_urls.append(page.url)
            yield page
            i += 1

    @classmethod
    def clean_value(cls, value: str) -> Optional[str]:
        """ Given raw value, remove () and []. """
        result = re.sub(r'(\(.+?\)|\[.+?\])', '', value).strip()

        if re.match(r'\b(' + '|'.join(cls.BAD_VALUES) + r')\b', result.strip(), flags=re.IGNORECASE):
            logger.debug(f'Wiki cleaning: Bad word in value, dropping: {value}')
            return

        return result

    def clean_url(self, value: str) -> str:
        """ Converts any url to absolute url """
        if value.startswith('//'):
            value = f'https:{value}'
        elif value.startswith('/'):
            value = f'https://{get_domain(self.response.url)}{value}'

        return value

    def clean_values(self, values: List[Tuple[str, Optional[str]]]) -> List[Tuple[str, Optional[str]]]:
        return [(self.clean_value(val), self.clean_url(url) if url else url) for val, url in values if val]

    def clean_Website(self, values: List[Tuple[str, Optional[str]]]) -> List[Tuple[str, Optional[str]]]:
        """ Drop urls that lead to wikipedia """
        results = self.clean_values(values)
        return [(value, url) for value, url in results if url and 'wikipedia.org' not in url]

    def clean_Traded_as(self, values: List[Tuple[str, Optional[str]]]) -> List[Tuple[str, Optional[str]]]:
        """ Default cleanup, but drops ended ownership """
        result = []
        for value, url in values:
            end_year = self.END_YEAR.search(value)
            if end_year:
                continue  # throw away if specified year of end

            result.append((
                self.clean_value(value),
                self.clean_url(url) if url else url
            ))

        return result

    def clean_Owner(self, values: List[Tuple[str, Optional[str]]]) -> List[Tuple[str, Optional[str]]]:
        """ Default cleanup, but drops ended ownership and sorts by major owner first """
        owners = []
        for value, url in values:
            if 'owner' in value.lower():
                continue

            end_year = self.END_YEAR.search(value)
            if end_year:
                logger.debug(f'Found end year ({end_year}), dropping value {value}')
                continue  # throw away if specified year of ownership end

            ownership = None
            match = re.match(r'(?P<value>.*)\(.*?(?P<ownership>[\d.,]+)%\)', value)
            if match:
                value = match[1].strip()
                ownership = match[1].strip(),
                try:
                    ownership = float(match[2].strip())
                except ValueError:
                    pass

            owners.append((
                self.clean_value(value),
                self.clean_url(url) if url else url,
                ownership,
            ))

        return [(owner[0], owner[1]) for owner in sorted(owners, key=lambda owner: owner[2] or 100, reverse=True)]

    def clean_Parent(self, values: List[Tuple[str, Optional[str]]]) -> List[Tuple[str, Optional[str]]]:
        return self.clean_Owner(values)

    def clean_caption(self, value: str) -> str:
        value = self.clean_value(value)
        if value:
            value = strip_accents(value)
            value = re.sub(r'[^\w\d& .,+-]', '', value)
        return value

    @property
    def ticker(self) -> Tuple[Optional[str], Optional[str]]:
        """ Return first ticker and exchange from the page """
        from ticker_merchant_map.ticker_merchant_map import EXCHANGES, remove_class, normalize_tkr_exch, \
            InvalidTickerExchange

        tickers = []
        for tkr_exch in self.infobox.get('Traded as', []):
            tkr_exch = tkr_exch[0]
            tkr_exch = re.sub(r'\s?,.*', '', tkr_exch)
            # Class B Common Stock: NYSE: GHC (https://en.wikipedia.org/wiki/Graham_Holdings)
            match = re.match(r'(?:.+:)?\(?(?P<exchange>.+):(?P<ticker>.+)\)?', tkr_exch, flags=re.IGNORECASE)
            if not match:
                continue

            ticker = self.clean_value(match['ticker'])
            ticker = ticker.split('.')[0]  # handle "LSE: AV.", "NYSE: RDS.A
            ticker = remove_class(ticker)
            exchange = self.clean_value(match['exchange'])

            try:
                ticker, exchange = normalize_tkr_exch(ticker, exchange)
                if ticker and exchange:
                    tickers.append((ticker, exchange))
            except InvalidTickerExchange as exc:
                logger.warning(f'WikiPage.ticker: {exc}')

        if not tickers:
            return None, None

        # now take most relevant ticker
        tickers.sort(key=lambda item: EXCHANGES.index(item[1]))
        return tickers[0]

    def is_corporate(self) -> bool:
        """ Whether url belongs to corporate wikipedia page """
        # list or category wiki pages are not corporate
        if re.match(r'^https://\w+\.wikipedia\.org/wiki/(list|category)', self.url, flags=re.IGNORECASE):
            return False

        return \
            any(key.lower() in [
                'traded as', 'industry', 'employees', 'products', 'revenue', 'parent',
                'company', 'market cap', 'indices', 'parent company'
            ] for key in self.infobox.keys()) \
            or is_organization(self.infobox.get('Title', ''), use_bing=False)

    def is_product(self) -> bool:
        """ Whether url belongs to product wikipedia page """
        return any(key.lower() in ['product type', 'tagline'] for key in self.infobox.keys())

    def is_stale(self) -> bool:
        """ Whether wiki page refers to non-existing company """
        return bool(set.intersection({'Defunct', 'Successor'}, set(self.infobox.keys())))

    def get_internal_scores(self, value: str, mode: str) -> dict:
        """
        Retrieves a dict of internal scores which altogether show how confident we are that this wiki page
        is related to `value`.

        Args:
            value - any value that this wiki page should probably be related to
            mode - domain / merchant

        Features
            - value_in_websites (boolean) - is the cleaned string value somewhere in wikipedia infobox 'websites' section (only if `mode` is "domain")
            - references_count (int) - for each website from wikipedia infobox 'websites' section count if cleaned string value is somewhere in website's html
            - title_count (int) - for bing response count how many times is met 'title' from wiki infobox
            - article_title_count (int) - for bing response count how many times wiki article title is met
            - url_count (int) - for bing response count how many times any of urls from wiki infobox 'websites' is met
        """

        scores = {}
        websites = list(map(
            lambda value: re.sub(r'^www\d?\.', '', get_domain(value[1])),
            self.infobox.get('Website', [])),
        )

        if mode == 'domain':
            # check whether value (which is domain) is listed in "Website" section
            scores['value_in_websites'] = get_domain(value) in websites
        elif mode != 'merchant':
            raise ValueError(f'Unknown mode: {mode}')

        # go to websites and check whether they contain the value
        references_count = 0
        for website in websites:
            response = get(website)
            if response and re.search(re.escape(value), response.text, flags=re.IGNORECASE):
                references_count += 1
        scores['references_count'] = references_count

        # count # of occurrences of title, page header and url in Bing search results
        # verification_query = f'which company owns {value}'
        verification_query = value
        logger.debug(f'Wiki verification query: {verification_query}')
        response = bing.query(verification_query)
        results = response.get('webPages', {}).get('value')[:30]

        results_str = json.dumps(results)

        title = self.infobox.get('Title')
        if title:
            title = make_unofficial_name(title)

        article_title = self.article_title
        if article_title:
            article_title = make_unofficial_name(article_title)

        logger.debug(f'Title: "{title}"; article title: "{article_title}"; urls: {websites}')
        scores.update({
            'title_count': len(
                re.findall(rf'(?=\b{re.escape(title)}\b)', results_str, flags=re.IGNORECASE)
            ) if title else 0,
            'article_title_count': len(
                re.findall(rf'(?=\b{re.escape(article_title)}\b)', results_str, flags=re.IGNORECASE)
            ) if article_title else 0,
            'url_count': len(
                re.findall(rf'(?={"|".join([re.escape(url) for url in websites])})', results_str, flags=re.IGNORECASE)
            ) if websites else 0,
        })

        return scores

    BING_NUM_CANDIDATES = 3  # number of bing results which will be used as candidates in `from_str` method
    # merchant_header, merchant_model = xgb_load_model(path.join(DATA, 'wiki.merchant.xgb_boost.pickle'))
    # domain_header, domain_model = xgb_load_model(path.join(DATA, 'wiki.domain.xgb_boost.pickle'))

    @classmethod
    @norecurse
    def from_str(cls, value: str, ) -> List['WikiPage']:
        """ Tries to get possible `WikiPage`s for value """

        # 1) query Bing with company name, filtering results that only belong to wikipedia.org
        query = f'site:wikipedia.org {value} company'
        # query = f'which company owns {value} wikipedia'
        query = re.sub("company company", "company", query, flags=re.IGNORECASE)
        response = bing.query(query)

        bing_results = response.get('webPages', {}).get('value')
        if not bing_results:
            logger.debug(f'No results for query "{query}"')
            return []
        # if DEBUG:
        #     logger.debug('Bing first 5 results:\n' + pretty(results[:5]))

        #
        # # 2) select first corporate page among first results
        # page = None
        # for rank, url in enumerate(res['url'] for res in results[:cls.BING_NUM_CANDIDATES]):
        #     if not re.match(r'^https://(\w{2}\.)?wikipedia.org/wiki/', url, flags=re.IGNORECASE):
        #         continue
        #
        #     candidate = cls(url)
        #     if not candidate.is_corporate():
        #         # last chance for this candidate - check whether owner is corporate
        #         # this works for places like https://en.wikipedia.org/wiki/Grand_Victoria_Casino_Elgin
        #         try:
        #             owner = next(candidate.iter_owners(include_self=False), None)
        #         except RecursionException:
        #             logger.debug(f'Candidate "{url}" rejected - recursion detected')
        #             break
        #
        #         if not owner or not isinstance(owner, cls):
        #             logger.debug(f'Candidate "{url}" - not corporate, owner is not WikiPage -> rejecting')
        #             continue
        #
        #         if owner.is_corporate():
        #             logger.debug(f'Candidate "{url}" not corporate but has owner - accepted')
        #             page = owner
        #             break
        #
        #         logger.debug(f'Candidate "{url}" rejected - not corporate')
        #         continue
        #
        #     if candidate.is_stale():
        #         logger.debug(f'Candidate "{url}" is stale - trying to find owner/successor')
        #         candidate = next(candidate.iter_owners())
        #         if not candidate or not isinstance(candidate, cls):
        #             logger.debug(f'Could not find owner/successor of "{url}"')
        #             continue
        #
        #     page = candidate
        #     logger.debug(f'Candidate "{url}" is corporate')
        #     break
        #
        # if not page:
        #     return

        # 3) decide whether the page is really related to original company name
        pages = []
        for rank, url in enumerate(res['url'] for res in bing_results[:cls.BING_NUM_CANDIDATES]):
            if not re.match(r'^https://(\w{2}\.)?wikipedia.org/wiki/', url, flags=re.IGNORECASE):
                logger.debug(f'Not a wiki page, discarding: {url}')
                continue

            page = cls(url)
            if not page.infobox.get('Title'):
                logger.debug(f'Not a wiki page, discarding: {url}')
                continue

            mode = 'domain' if is_domain_input(value) else 'merchant'
            page.internal_scores = page.get_internal_scores(value, mode=mode)
            page.internal_scores['rank'] = rank

            # ---- model-based scoring system ----
            # page.score = xgb_predict(
            #     scores=page.internal_scores,
            #     header=cls.domain_header if mode == 'domain' else cls.merchant_header,
            #     model=cls.domain_model if mode == 'domain' else cls.merchant_model,
            # )

            # ---- rule-based scoring system ----
            page.score = 0
            if any([
                page.internal_scores.get('value_in_websites'),
                page.internal_scores['references_count'] >= 2,
                page.internal_scores['title_count'] >= 8 or page.internal_scores['article_title_count'] >= 7,
                (page.internal_scores['title_count'] >= 4 or page.internal_scores['article_title_count'] >= 4)
                and page.internal_scores['url_count'] >= 3,
                page.internal_scores['url_count'] >= 6,
            ]):
                page.score = 1

            if page.score < 0.5:
                logger.debug(f'Page "{page.url}" was rejected as not related to "{value}" (score={page.score}, '
                             f'internal_scores={page.internal_scores})')
                continue

            logger.debug(f'Page "{page.url}" was accepted as belonging to "{value}" (score={page.score}, '
                         f'internal_scores={page.internal_scores})')
            pages.append(page)

        pages.sort(key=attrgetter('score'), reverse=True)
        return pages


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('input')
    args = parser.parse_args()

    pages = WikiPage.from_str(args.input)
    print(pretty(pages))
