from .wiki_finder2 import WikiPage


def test_owners():
    """
    Tests successful retrieving ticker over multiple parents:
    ABC -> Disney–ABC Television Group -> The Walt Disney Company
    """
    page = WikiPage(url='https://en.wikipedia.org/wiki/American_Broadcasting_Company')
    for owner in page.iter_owners():
        page = owner

    assert page.ticker[0] == 'DIS'


def test_owner_fields():
    """
    Tests whether we correctly retrieve owner and parent fields.
    Also tests correct values cleanup.
    """
    page = WikiPage('https://en.wikipedia.org/wiki/Yahoo!')
    names = [name for name, url in page.get_owners()]

    assert 'Verizon' in names
    assert 'Verizon Media' in names
    assert 'Independent' not in names


def test_bad_value():
    """
    Tests whether bad values are successfully removed from infobox:
    Subsidiaries: List of subsidiaries -> None
    """
    page = WikiPage(url='https://en.wikipedia.org/wiki/LVMH')

    assert page.infobox['Subsidiaries'] == []


def test_rejection():
    """
    Tests whether we are able to not reject correct wiki page (= "which really belongs to input")
    """
    assert WikiPage.from_str('abc.go.com')[0].url == 'https://en.wikipedia.org/wiki/American_Broadcasting_Company'
    assert WikiPage.from_str('graco inc.')[0].url == 'https://en.wikipedia.org/wiki/Graco_(fluid_handling)'
    assert WikiPage.from_str('PokerStars.com')[0].url == 'https://en.wikipedia.org/wiki/PokerStars'
    assert WikiPage.from_str('www.oyster.com')[0].url != 'https://en.wikipedia.org/wiki/Oyster_(company)'
    # ^ TODO: penalize substrings?
    assert WikiPage.from_str('bogsfootwear.com')[0].url == 'https://en.wikipedia.org/wiki/Weyco_Group'
    assert WikiPage.from_str('instapro.com')[0].url == 'https://en.wikipedia.org/wiki/IAC_(company)'
    assert WikiPage.from_str('www.redbus.co')[0].url == 'https://en.wikipedia.org/wiki/Redbus.in'
    assert WikiPage.from_str('AEP INDUSTRIES INC')[0].url == 'https://en.wikipedia.org/wiki/American_Electric_Power'
    assert WikiPage.from_str('GRAHAM CORPORATION')[0].url == 'https://en.wikipedia.org/wiki/Graham_Holdings_Company'


def test_ticker():
    """
    Test whether we correctly extract ticker
    """
    assert WikiPage(url='https://en.wikipedia.org/wiki/Aviva').ticker == ('AV', 'LSE')
    assert WikiPage(url='https://en.wikipedia.org/wiki/Royal_Dutch_Shell').ticker == ('RDS', 'NYSE')


def test_company_name():
    """
    Test whether we don't take bad chars to company name

    Example: https://en.wikipedia.org/wiki/Kia_Motors
    Kia Motors 기아자동차 -> Kia Motors
    """
    assert WikiPage(url='https://en.wikipedia.org/wiki/Kia_Motors').infobox['Title'] == 'Kia Motors Corporation'


def test_complicated_owner():
    """
    Test owner extraction from description (text)

    Example: https://en.wikipedia.org/wiki/Discovery_Cove
    Discovery Cove is an amusement park owned and operated by SeaWorld Parks & Entertainment
    """
    assert WikiPage(url='https://en.wikipedia.org/wiki/Discovery_Cove').get_owners()[0][0] == 'SeaWorld Parks & Entertainment'


def test_corporate_owner():
    """
    Test whether wiki page is treated as winner even if it's not corporate BUT its parent IS corporate

    Example: https://en.wikipedia.org/wiki/Grand_Victoria_Casino_Elgin
    """
    assert WikiPage.from_str('grandvictoriacasino.com')[0].url == 'https://en.wikipedia.org/wiki/Eldorado_Resorts'


def test_caption():
    """
    Test whether we delete garbage from infobox title

    Example: https://en.wikipedia.org/wiki/Teva_Pharmaceuticals
    """
    page = WikiPage('dummy')
    assert page.clean_caption('Teva Pharmaceutical Industries Ltd. טבע תעשיות פרמצבטיות בע"מ') == 'Teva Pharmaceutical Industries Ltd.'
    assert page.clean_caption('Société Générale') == 'Societe Generale'

    assert WikiPage(url='https://en.wikipedia.org/wiki/Burger_King').infobox['Title'] == 'Burger King'


def test_recursion():
    """
    Check whether we may handle infinite recursion in owner verification:

    https://en.wikipedia.org/wiki/Apology_(horse) -> owner == Rev. John William King -> check whether it's corporate -> (1)
    """
    assert WikiPage.from_str[0]('FILLY FLAIR')


def test_subsidiaries():
    """
    Check whether we may handle crazy list of subsidiaries:

    https://en.wikipedia.org/wiki/General_Motors
    """
    subsidiaries = [val for val, _ in WikiPage(url='https://en.wikipedia.org/wiki/General_Motors').infobox['Subsidiaries']]
    assert 'Baojun' in subsidiaries
    assert 'Industrial:' not in subsidiaries