"""
© 2018 Alternative Data Group. All Rights Reserved.

Requires Python3 and:
    pandas>=0.22.0
    sqlalchemy>=1.2.1
    pymysql>=0.8.0

TODO: Specific to FindMaker, move to that module? Also used in Find Winner tools.
"""

import os
from operator import itemgetter

import pandas as pd
import sqlalchemy
from joblib import Memory
from collections import defaultdict
from typing import Optional, List

from mapper.settings import USE_CACHE, CACHE_DIR

from find_maker.preprocessing import preprocessed_text
from packages.settings import REL_DATA_DIR, TARGETS_SOURCEFILE
from packages.str_mapping import map_all_str
from packages.utils import uid, Timer, with_timer, conditional_decorator, remove_dupes, db_execute, similar

import logging

logger = logging.getLogger(f'adg.{__name__}')
memory = Memory(cachedir=CACHE_DIR, verbose=0)


class Targets:
    """ Container for dict {uid: string}. Has methods for loading data and preprocessing it. """

    def __init__(self, load_from='database'):
        """
        Loads dict from specified source.

        Args:
            load_from: Loads dict to `self.contents`. Available values:
            - disk: loads from `REL_DATA_DIR/TARGETS_SOURCEFILE`;
            - database: loads from `TARGET_TABLE_NAME` (currently 'entity_match_target_map_tmp');
            - test: loads test data, e.g. {'0': 'miscrosoft', '1': 'BP P.L.C.'}.
            - <dict>: load directly from this dict
        """

        if load_from == 'disk':
            self._targets_load_from_disk()
        elif load_from == 'database':
            self._targets_load_from_db()
        elif load_from == 'test':
            self.contents = self._test_data()
        elif isinstance(load_from, dict):
            self.contents = load_from
        else:
            # DEBUG: Error
            raise Exception(f'package.targets: [CRIT] Unknown load_from: {load_from}')

        # remove unnecessary symbols from `self.contents`, see `find_maker/preprocessing/preprocessed_text`.
        self.orig_contents = self.contents
        self.contents = {uid: preprocessed_text(target) for uid, target in self.contents.items() if target}
        # self.max_words = max(target.count(' ') for target in self.contents.values())

        # group targets by prefixes
        self.prefix_length = 4
        self.with_prefix = defaultdict(list)
        for value in self.contents.values():
            prefix = value[:self.prefix_length]
            self.with_prefix[prefix].append(value)

    def __repr__(self):
        return str(self.contents)

    # TODO: Fix; New git migration broke this.
    def _targets_load_from_disk(self):
        raise NotImplementedError()
        file_path = os.path.join(REL_DATA_DIR, TARGETS_SOURCEFILE)
        raw_targets = pd.read_csv(file_path, error_bad_lines=False)
        targets = map_all_str(raw_targets['targets_input'], invertdict=True)
        uids = [uid() for _ in targets]  # FIXME Why do we assign new uids to known targets?
        self.contents = dict(zip(uids, targets))

    def _targets_load_from_db(self):
        self.infos = self.get_all()
        self.contents = {str(i): target for i, target in enumerate(self.infos.keys())}

    def _test_data(self):
        return dict((str(i), v) for i, v in enumerate((
            'microsoft',
            'BP P.L.C.',
            'AT&T',
            'Stop & Shop',
            'New Enterprise Associates (NEA)',
        )))

    @classmethod
    # FIXME: Figure out better caching or how to manage this one easily.
    @conditional_decorator(memory.cache, USE_CACHE)  # joblib storage-based cache
    def get_all(cls):
        """
        Returns dict containing all targets info:
            {target: {
                'official_name': ...,
                'unofficial_names': ...,
                'ticker': ...,
                'exchange': ...,
                'entity_names': ...,
            }, ...}
        """

        logger.debug('Fetching all targets SQL...')
        targets = db_execute(
            f'SELECT uid, match_target, official_name, identifiers, entity_names FROM brand_master_nodes_tbl'
        ).fetchall()

        # results = {}
        # unofficial_names = defaultdict(list)  # dict {target_official: [target1, target2, ...]}

        results = {}
        for res in targets:
            identifiers = res[3].split(',')[0].split(':') if res[3] and ':' in res[3] else [None, None]
            if not len(identifiers) == 2:
                # because identifiers sometimes contain weird string
                identifiers = [None, None]

            results[res[1]] = {
                'uid': res[0],
                'official_name': res[2],
                'ticker': identifiers[0],
                'exchange': identifiers[1],
                'entity_names': res[4].split('|'),
                'unofficial_names': [res[1]],
            }

        return results

    def get_by_similar_name(self, similar_name: str, thld: float = None) -> List[str]:
        """ Returns targets with similar name (probably cleaned string). """
        params = {'thld': thld} if thld else {}

        results = []
        for target, info in self.infos.items():
            all_names = info.get('unofficial_names', []) + \
                ([info['official_name']] if info.get('official_name') else []) + \
                info.get('entity_names', [])

            similarities = [similar(name, similar_name, **params) for name in all_names]
            score = max(similarities)
            if score:
                results.append((target, score))

        results.sort(key=itemgetter(1), reverse=True)
        return [res[0] for res in results]

    def get_official_name(self, target: str) -> Optional[str]:
        """
        Converts internal (unofficial) target name to official one.
        Returns None if target has no official name (if target does not belong to nodes table).
        """
        return self.infos.get(target, {}).get('official_name')

    def get_by_unofficial_name(self, unofficial_name: str):
        return {target: info for target, info in self.infos.items() if
                unofficial_name.lower() in set(x.lower() for x in info['unofficial_names'])}

    def get_by_official_name(self, target_official):
        """ Returns info of selected official target name. """
        return {target: info for target, info in self.infos.items() if
                info['official_name'].lower() == target_official.lower()}

    def get_by_ticker(self, ticker: str, exchange: str) -> dict:
        """Returns infos on targets with same ticker."""
        return {
            target: info for target, info in self.infos.items()
            if info['ticker'] == ticker and info['exchange'] == exchange
        }

    @classmethod
    def get_related_entities(cls, target_official):
        """
        Reads related entities and corresponding scores from `brand_master_edges` table.
        Duplicates are removed, items are ordered by closeness score (biggest is first).
        # FIXME: This query uses brand_master_edges_tbl for now.

        Args:
            target_official: Official name of target

        Returns: [
            {
                'name': official related target name,
                'score': closeness score,
            },
            ...
        ]
        """

        results = db_execute(
            '''
                SELECT
                    node1_name name1,
                    node2_name name2,
                    model_score score
                FROM brand_master_edges_tbl
                WHERE
                  (node1_name = %(target_official)s OR node2_name = %(target_official)s)
                   AND edge_type = "DIRECT"
            ''',
            target_official=target_official,
        ).fetchall()

        related = []
        for name1, name2, score in results:
            name = name1 if name1 != target_official else name2
            if not name or name == target_official:
                continue

            if name not in [rel['name'] for rel in related]:
                related.append({
                    'name': name,
                    'score': score,
                })

        related = remove_dupes(related, key=itemgetter('name'))
        related.sort(key=itemgetter('score'), reverse=True)

        return related

    def get_metadata(self, target):
        """
        Retrieves all related info for specific target.

        Args:
            target: unofficial (internal) target name

        Returns: Dict:
        {
            'company_aliases': [...],
            'unofficial_names': [...],
            'ticker': ...,
            'exchange': ...,
            'related_entities': [...],
        }
        """
        info = self.infos[target]

        aliases = info['entity_names']
        official_name = info['official_name']
        unofficial_names = info['unofficial_names']

        related_entities = self.get_related_entities(official_name)

        return {
            'company_aliases': aliases,
            'unofficial_names': unofficial_names,
            'related_entities': related_entities,
            'ticker': info['ticker'],
            'exchange': info['exchange'],
        }


# singleton
with Timer('Loading targets'):
    TARGETS = Targets(load_from='database')
