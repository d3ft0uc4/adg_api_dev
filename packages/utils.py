"""
© 2018 Alternative Data Group. All Rights Reserved.

Misc. utilities
"""

import csv
import functools
import inspect
import json
import logging
import multiprocessing
import os
import random
import re
import sys
import threading
import time
import unicodedata
from argparse import ArgumentParser
from collections import defaultdict
from concurrent.futures._base import FINISHED
from datetime import datetime
from functools import wraps
from itertools import tee, filterfalse, chain, combinations
from os import path
from typing import List, Callable, Any, Tuple, Iterable, Dict, Optional, Set

from jellyfish import jaro_winkler
from unidecode import unidecode
from nltk import ngrams as nltk_ngrams


LOCAL = threading.local()

logger = logging.getLogger(f'adg.{__name__}')


ROOT_DIR = path.dirname(path.dirname(path.abspath(__file__)))


def uid():
    return '{:08x}'.format(random.randint(0, 0xffffffff))
    # return str(uuid.uuid4())


class Timer:
    """
    Just a simple timer that outputs execution time of inner block.

    Usage example:
        with Timer('Name for this block of code', prefix='-----'):
            # ... your code here ...
    """

    def __init__(self, name, prefix='  ', silent=False, store_to_db=False):
        """
        Sets up formatting for output message.

        Args:
            name: Name of tested block of code.
            prefix: Prefix that will be added to output message.
            silent: Output elapsed time to console
            store_to_db: Whether to save results to database
        """
        self.name = name
        self.prefix = prefix
        self.silent = silent
        self.store_to_db = store_to_db

    def __enter__(self):
        if not self.silent:
            logger.debug(f'Running "{self.name}"')

        self.start = time.time()
        return self

    @property
    def elapsed(self):
        return time.time() - self.start

    def __exit__(self, *args):
        val = round(self.elapsed, 3)

        if self.name in self.get_timing():
            existing_value = self.get_timing()[self.name]
            if not isinstance(existing_value, list):
                self.get_timing()[self.name] = [existing_value]
            # warn(f'Timer overwriting time measurement for {self.name}. Probably this timer is run more than once?')
            self.get_timing()[self.name].append(val)
        else:
            self.get_timing()[self.name] = val

        if not self.silent:
            logger.debug(f'Elapsed "{self.name}": {val:.2f}s')

        if self.store_to_db:
            self.store_to_db_log(val)

    def store_to_db_log(self, elapsed):
        from mapper import settings

        dt = datetime.now()
        pid = os.getpid()
        tid = threading.get_ident()
        settings.db_engine.execute("""
            INSERT INTO timer_log
            VALUES (
                %(datetime)s,
                %(name)s,
                %(time)s,
                %(pid)s,
                %(tid)s
            )
            """,
                          datetime=dt,
                          name=self.name,
                          time=elapsed * 1000,
                          pid=str(pid),
                          tid=str(tid))

    @classmethod
    def get_timing(cls):
        if not hasattr(LOCAL, 'timing'):
            LOCAL.timing = {}

        return LOCAL.timing

    @classmethod
    def clear_timing(cls):
        LOCAL.timing = {}

    @classmethod
    def get_stats(cls):
        stats = {}
        for key, value in cls.get_timing().items():
            if isinstance(value, list):
                n_calls = len(value)
                total = sum(value)
            else:
                n_calls = 1
                total = value

            stats[key] = {
                'n_calls': n_calls,
                'total': total,
            }

        return stats


def with_timer(fn):
    """
    Decorator for measuring function execution time.

    Usage example:
        @with_timer
        def some_function():
            # ...
    """

    @wraps(fn)
    def wrapper(*args, **kwargs):
        with Timer(fn.__name__):
            return fn(*args, **kwargs)

    return wrapper


def similar(value1: str, value2: str, thld: float = 0.93) -> float:
    """ Compare strings using jaro-winkler distance """
    # bc similar('AMERICAN AIRLINE', 'American Capital', thld=0.9) == True
    # and similar('werkspot', 'werkspot bv', thld=0.95) == False
    # and jaro_winkler('braintree', 'braintree town') == 0.928
    value1 = value1.lower()
    value2 = value2.lower()

    # force first letters match ("esco" vs "tesco")
    if value1[:2] != value2[:2]:
        return 0

    similarity = jaro_winkler(value1, value2)
    return similarity if similarity >= thld else 0


class conditional_decorator:
    """
    Decorator which works based on some condition.

    Condition may be static (just a value), and function will be decorated based on that condition.
    But if condition is callable, it will be checked before each function call, and based on the value
    the function will or will not be decorated at runtime.

    "Dynamic" decoration example:

    @conditional_decorator(CheckPermission, condition=lambda *args, **kwargs: request.user != 'Greg')
    def some_restricted_function(...):
        ...

    For each call of `some_restricted_function`, it will be decorated with `CheckPermission` if current request's user
    is not Greg. If user is Greg, the function is not decorated.
    """

    def __init__(self, decorator, condition):
        self.decorator = decorator
        self.condition = condition

    def __call__(self, fn):
        # dynamic decoration
        decorated = self.decorator(fn)

        if callable(self.condition):
            @wraps(fn)
            def dynamic_wrapper(*args, **kwargs):
                return (decorated if self.condition(*args, **kwargs) else fn)(*args, **kwargs)

            return dynamic_wrapper

        # static decoration
        return decorated if self.condition else fn


def remove_dupes(items: Iterable, key=lambda item: item, comp_fn: callable = lambda x, y: x == y) -> List:
    """ Removes duplicates (case-insensitive) """
    seen_items = []
    results = []

    for item in items:
        value = key(item)
        normalized = re.sub(r'[\.\'\,-]', '', value.lower()) if isinstance(value, str) else value

        occurs = any(comp_fn(normalized, seen) for seen in seen_items)
        if not occurs:
            results.append(item)

        seen_items.append(normalized)

    return results


def remove_value(items: List[str], value: str, key=lambda item: item) -> List[str]:
    """ Removes specific value from items (case-insensitive) """
    low = value.lower()
    return [item for item in items if key(item).lower() != low]


def qa(fn: Callable[[str], dict], fields: List[str], input_file: Optional[str] = None):
    """
    Runs inputs from selected file through function and outputs results to csv file.

    Args:
        input_file: TXT file to process
        fn: function to QA
        fields: CSV columns

    Returns:
        Writes results into .csv file
    """
    if not input_file:
        parser = ArgumentParser()
        parser.add_argument('input_file')
        args = parser.parse_args()

        input_file = args.input_file

    # read inputs
    assert input_file.endswith('.txt')
    inputs = open(input_file, 'r').read().splitlines()
    inputs = [inp.strip() for inp in inputs]
    num_inputs = len(inputs)
    logger.debug(f'Read {num_inputs} inputs')

    # prepare csv
    filename = re.sub('.txt$', '.csv', input_file)
    out_file = open(filename, 'w')
    writer = csv.DictWriter(
        out_file,
        fieldnames=['input', *fields],
        extrasaction='ignore',
    )
    writer.writeheader()

    # process inputs
    for i, inp in enumerate(inputs, start=1):
        logger.debug(f'{i} / {num_inputs} {inp}')

        writer.writerow({
            'input': inp,
            **fn(inp),
        })
        out_file.flush()

    out_file.close()
    logger.debug(f'Wrote results to {filename}')


def get_precision_recall(fn: Callable[[str], dict],
                         comparison_fn: Callable[[Any, Any], bool] = similar):
    """
    Load input and expected output from csv file, run `fn` on each input and
    compare result with expected outputs, calculate precision and recall,
    and output results to csv.

    Args:
        fn - function to test, should return list of dicts with results which will match columns from
        csv file with answers.
        comparison_fn - a function to compare results

    Usage:
        # script.py
        from packages.utils import get_precision_recall
        from domain_tools.ssl import SSLCertificateRetrieval

        get_precision_recall(
            lambda x: [{'output': SSLCertificateRetrieval.get_organization(x)}]
        )

    Then run from cli:
        python -m script.py <path_to_csv_with_answers>
    """
    parser = ArgumentParser()
    parser.add_argument('input_file')
    args = parser.parse_args()

    # read inputs
    assert args.input_file.endswith('.csv')
    in_file = open(args.input_file, 'r')
    reader = csv.DictReader(in_file)

    items = list(reader)
    in_fields = reader.fieldnames
    in_file.close()

    # prepare output csv
    filename = re.sub('.csv$', '_out.csv', args.input_file)
    out_file = open(filename, 'w')
    out_fields = in_fields[:]
    for field in in_fields[1:]:
        out_fields += [f'{field} result', f'{field} match', f'{field} case']

    writer = csv.DictWriter(out_file, fieldnames=out_fields, extrasaction='ignore')
    writer.writeheader()

    total = len(items)
    cases = defaultdict(lambda: defaultdict(int))
    for i, item in enumerate(items, start=1):
        value = item[in_fields[0]]
        logger.debug(f'{i}/{total}: {value}')

        results = fn(value) or [{}]
        for result in results:
            for field in in_fields[1:]:
                out = result.get(field)
                match = any(comparison_fn(out, answer) for answer in item[field].split('|'))

                if out and match:
                    case = 'TP'
                elif out and not match:
                    case = 'FP'
                elif not out and match:
                    case = 'TN'
                elif not out and not match:
                    case = 'FN'

                cases[field][case] += 1

                row = item.copy()
                row[f'{field} result'] = out
                row[f'{field} match'] = match
                row[f'{field} case'] = case

            writer.writerow(row)
            out_file.flush()

    out_file.close()
    logger.debug(cases)

    for field in cases:
        precision = cases[field]['TP'] / (cases[field]['TP'] + cases[field]['FP'])
        recall = cases[field]['TP'] / (cases[field]['TP'] + cases[field]['FN'])
        f1_score = 2 * precision * recall / (precision + recall)
        logger.debug(f'{field} precision: {precision:.2f}')
        logger.debug(f'{field} recall: {recall:.2f}')
        logger.debug(f'{field} F1 score: {f1_score:.2f}')

    logger.debug(f'Wrote results to {filename}')


# @conditional_decorator(with_timer, settings.TIMERS)
def db_execute(sql_query: str, *args, **kwargs) -> Any:
    from mapper import settings

    try:
        # logger.debug(f'Executing SQL: {sql_query}')
        return settings.db_engine.execute(sql_query, *args, **kwargs)
    except Exception as exc:
        logger.error(f'SQL query failed: "{sql_query}", {exc}')
        raise


class NeverFail:
    """ Decorator for wrapping any function into try-except """

    def __init__(self, fallback_value, silent: bool = False):
        self.fallback_value = fallback_value
        self.silent = silent

    def __call__(self, fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except Exception:
                if not self.silent:
                    logger.exception(f'"{fn.__name__}{args, kwargs}" failed, using fallback value')

                if callable(self.fallback_value):
                    try:
                        logger.debug(f'Using fallback function "{self.fallback_value.__name__}{args}{kwargs}"')
                        fallback_value = self.fallback_value(*args, **kwargs)
                        logger.debug(f'Calculated fallback value: {fallback_value}')
                        return fallback_value
                    except Exception:
                        logger.exception(f'Exception in fallback function {self.fallback_value.__name__}')
                        return None

                logger.debug(f'Using fallback value "{self.fallback_value}"')
                return self.fallback_value

        return wrapper


# ---- Json serialization ----
# This allows to serialize any python object to json using pickle
# https://stackoverflow.com/a/18561055/1935381
#
# Usage:
# json.dunps(data, cls=PythonObjectEncoder, indent=4)
# json.loads(object_hook=as_python_object)

# class PythonObjectEncoder(json.JSONEncoder):
#     def default(self, obj):
#         return {'_python_object': pickle.dumps(obj).decode('latin1')}

# def as_python_object(dct):
#     if '_python_object' in dct:
#         return pickle.loads(dct['_python_object'].encode('latin1'))
#     return dct


class StrFallbackJSONEncoder(json.JSONEncoder):
    """ Json encoder which tries to convert unserializable objects to strings """

    def default(self, obj):
        try:
            return super().default(obj)
        except TypeError:
            pass

        # Serialize to dict if object has `serialize` method
        if hasattr(obj, 'serialize'):
            return obj.serialize()

        if isinstance(obj, float):
            return round(obj, 3)

        # Just convert to string
        return str(obj)


# ---- ---- ----
def with_timeout(timeout):
    """
    Timeout decorator
    Usage:
    @with_timeout(2)
    def foo():
        ...
    """

    def decorator(decorated):
        @functools.wraps(decorated)
        def inner(*args, **kwargs):
            pool = multiprocessing.pool.ThreadPool(1)
            async_result = pool.apply_async(decorated, args, kwargs)
            try:
                return async_result.get(timeout)
            except multiprocessing.TimeoutError:
                logger.warning(f'Timeout for function {decorated.__name__}{args}')
                return

        return inner

    return decorator


def pretty(data):
    return json.dumps(data, cls=StrFallbackJSONEncoder, indent=4)


class RecursionException(Exception):
    pass


def norecurse(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        frame_infos = [info for info in inspect.stack() if info.function == fn.__name__]  # previous calls on stack

        if frame_infos:
            fn_args = inspect.signature(fn).bind(*args, **kwargs).arguments  # {argument_name: value} dict for this call
            # fn_fields = inspect.getfullargspec(fn).args  # list of fn's arguments names
            # fn_args = {  # {argument_name: value} dict for this call
            #     **dict(zip(fn_fields, args)),
            #     **kwargs,
            # }

            for info in frame_infos:
                info_args = info.frame.f_locals
                # info_args = {  # {argument_name: value} dict for previous call (from stack)
                #     k: v for k, v in inspect.getargvalues(info.frame).locals.items()
                #     if k in fn_fields
                # }

                if all(info_args.get(k) == v for k, v in fn_args.items()):
                    raise RecursionException(f'Recursion when calling {fn.__name__}({fn_args})')

        result = fn(*args, **kwargs)
        return result

    return wrapper


def cached_method(method):
    """ Decorator which caches class method """

    @wraps(method)
    def wrapper(self, *args, **kwargs):
        if not hasattr(self, '__cached_methods__'):
            self.__cached_methods__ = {}

        call_hash = (method.__name__, args, tuple(sorted(kwargs.items())))
        if call_hash not in self.__cached_methods__:
            self.__cached_methods__[call_hash] = method(self, *args, **kwargs)

        return self.__cached_methods__[call_hash]

    return wrapper


def partition(pred: callable, iterable: Iterable) -> Tuple[list, list]:
    """
    Use a predicate to partition entries into false entries and true entries.

    Args:
        pred: any callable which returns boolen and thus splits any iterable in two parts
        iterable: any collection to be split by predicate

    Returns:
        tuple of 2 lists:
        (<list of items that `pred(item) is False`>, <list of items that `pred(item) is True>)
    """
    # partition(is_odd, range(10)) --> 0 2 4 6 8   and  1 3 5 7 9
    t1, t2 = tee(iterable)
    return list(filterfalse(pred, t1)), list(filter(pred, t2))


v = sys.version_info
if v.major < 3 or (v.major == 3 and v.minor < 7):
    logger.debug(f'Version {v.major}.{v.minor} < 3.7 -> using backported ThreadPoolExecutor')
    from concurrent.futures import ThreadPoolExecutor as OrigThreadPoolExecutor


    class ThreadPoolExecutor(OrigThreadPoolExecutor):
        def __init__(self, max_workers=None, thread_name_prefix='', initializer=None, initargs=()):
            super().__init__(max_workers=max_workers, thread_name_prefix=thread_name_prefix)
            self._initializer = initializer
            self._initargs = initargs

        def submit(self, fn, *args, **kwargs):
            if self._initializer:
                def fn_with_initializer(*args, **kwargs):
                    self._initializer(*self._initargs)
                    return fn(*args, **kwargs)

                fn_new = fn_with_initializer
            else:
                fn_new = fn

            return super().submit(fn_new, *args, **kwargs)

else:
    from concurrent.futures import ThreadPoolExecutor


class LocalsThreadPoolExecutor(ThreadPoolExecutor):
    """ Thread pool executor which forwards threading.local() contents to all subthreads """

    @staticmethod
    def init_thread(args):
        from packages.utils import LOCAL
        for key, value in args.items():
            setattr(LOCAL, key, value)

        if LOCAL.settings.get('STATS', False):
            logger.debug(
                f'Active threads: {threading.activeCount()}, '
                # f'peak memory usage: {resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024 }Mb'
            )

    def __init__(self, *args, **kwargs):
        super().__init__(
            *args,
            **{
                **kwargs,
                'initializer': self.init_thread,
                'initargs': [{
                    **{
                        key: getattr(LOCAL, key)
                        for key in dir(LOCAL)
                        if not key.startswith('_')
                    },
                    'stack': inspect.stack() + getattr(LOCAL, 'stack', []),
                }],
            },
        )

        if self._max_workers == 1:
            self._tasks = []

    class FakeFuture:
        class Condition:
            def acquire(self):
                pass

            def release(self):
                pass

            def __enter__(self):
                pass

            def __exit__(self, exc_type, exc_val, exc_tb):
                pass

        def __init__(self, fn, *args, **kwargs):
            self.fn = fn
            self.args = args
            self.kwargs = kwargs
            self._state = FINISHED
            self._condition = self.Condition()
            self._waiters = []

        def result(self):
            return self.fn(*self.args, **self.kwargs)

    def submit(self, fn, *args, **kwargs):
        if self._max_workers == 1:
            return self.FakeFuture(fn, *args, **kwargs)

        return super().submit(fn, *args, **kwargs)


def strip_accents(s):
    """ Remove accents from string: Société Générale -> Societe Generale """
    # https://stackoverflow.com/a/518232/1935381
    return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')


def read_list(path: str) -> list:
    """
    Reads any file into list line-by-line.
    Assumes that file's encoding is utf-8.
    Lines startng with `#` char are ignored.

    Args:
        path: path to file

    Returns:
        List of lines from the file
    """
    with open(path, encoding='utf-8') as f:
        return list(filter(lambda s: s and not s.startswith('#'), map(str.strip, f.readlines())))


def grow_to_words(full_string: str,
                  start_closed: int,
                  end_opened: int
                  ) -> str:
    """Returns substring with non-work suffixes or preffixes, without whitespace suffixes or preffixes."""
    # re.match('[\W_]+', sub) find all non-word symbols in substring
    substring = full_string[start_closed: end_opened]

    pref = ''
    suff = ''
    for x in re.findall('[\W_]+', substring):
        if substring.endswith(x):
            suff = x
        if substring.startswith(x):
            pref = x
    modified_substring = substring.strip(pref).strip(suff)
    # print([pref, suff, modified_substring], end=' ')
    if modified_substring == '':
        return substring.strip()

    hack_string = 'QWERTASDFGZXCVB'
    hacked_substring = hack_string + modified_substring
    hacked_full_string = full_string[:start_closed + len(pref)] + hack_string + full_string[start_closed + len(pref):]
    # print([hacked_substring, hacked_full_string], end=' ')
    options = re.findall("[a-zA-Z]*" + hacked_substring.replace('\\', '\\\\') + "[a-zA-Z]*", hacked_full_string)
    # print(options, end=' ')

    if len(options) == 0:
        logger.warning(f"Given string is not a substring of raw string.\n{full_string}\n{substring}")
        return substring
    elif len(options) > 1:
        logger.warning(f"There are multiple options of grown substring.\n{substring}\n{options}")

    result = pref + options[0].replace(hack_string, '') + suff
    return result.strip()


def clean_company_name(value: str) -> str:
    """Replace all symbols like '™' with ascii versions of them (tm), strip and delete everything inside brackets."""
    return re.sub(r'\(.*?\)', '', unidecode(value).strip())


def get_settings_snapshot() -> dict:
    """ Returns all settings.py variables as dict """
    from mapper import settings
    return {
        key: getattr(settings, key)
        for key in dir(settings) if not key.startswith('__')
    }
LOCAL.settings = get_settings_snapshot()


def apply_configuration(config: dict):
    """ Applies additional configuration to LOCAL.settings """
    LOCAL.settings = get_settings_snapshot()

    for key, value in config.items():
        setting = LOCAL.settings
        subkeys = key.split('.')
        for i, subkey in enumerate(subkeys, start=1):
            if i == len(subkeys):
                setting[subkey] = value
            else:
                setting = setting[subkey]


def prefixize(lst: Iterable[str], prefix_length: int = 4, key: callable = lambda x: x) -> Dict[str, List[str]]:
    """ Converts list to {prefix: sublist} dictionary """
    res = defaultdict(list)
    for item in lst:
        value = key(item)
        for end in range(2, min(prefix_length, len(value)) + 1):
            res[value[:end].lower()].append(item)

    return res


def get_memory_usage():
    import psutil
    process = psutil.Process(os.getpid())
    return str(process.memory_info().rss / 1024 / 1024) + ' Mb'


ENCRYPTION_MAP = {
    'bane': 'bayne',
    'fwfm': 'fmfw',
    'bloomberg': 'mayor',
    'bloomberg_lite': 'mayor_lite',
    'bloomberg_search': 'mayor_search',
    'crunchbase': 'lunchbox',
    'crunchbase_api': 'lunchbox_obi',
    'about': 'wut',
    'company_website': 'prodigy',
    'copyright': 'cutleft',
    'ssl': 'SHA',
    'whois': 'ktotam',
    'wikipedia': 'britanic.A',
    'db_lookup': 'bd_search',
    'google_knowledge_graph': 'g0_KN',
    'open_figi': 'vigvam',
    'small_parsers': 'bottlerocket',
    'FacebookParser': 'F',
    'MapquestParser': 'MQ',
    'ReutersParser': 'RET',
    'TwitterParser': 'TW',
    'YahooLocalParser': 'XL',
    'YellowpagesParser': 'XP',
    'YelpParser': 'GavP',
    'bane4products': 'bayne2',
    'fwfm4products': 'fwfm2',
    'company_website4products': 'prodigy2',
    'wikipedia4products': 'britanic.B',
    'keywords': 'WK',
    'db_lookup4products': 'bd_search2',
    'transformation[parent]': '2p',
}


@NeverFail(fallback_value={})
def encrypt(data):
    """ Returns cryptic version of whatever """
    # import random

    if isinstance(data, dict):
        result = {}
        for key, value in data.items():
            encrypted_key = encrypt(key)
            if not encrypted_key:
                continue

            result[encrypted_key] = encrypt(value)

        return result

    elif isinstance(data, (list, tuple)):
        return [encrypt(item) for item in data]

    elif hasattr(data, 'serialize'):
        return encrypt(data.serialize())

    elif isinstance(data, str):
        for key, value in ENCRYPTION_MAP.items():
            data = re.sub(r'\b' + key + r'\b', value, data)

        return data

    else:
        return data


DATA = path.join(path.dirname(path.abspath(__file__)), 'data')
STOP_WORDS = [word.lower() for word in read_list(path.join(DATA, 'stopwords.txt'))]


def tokenize(value: str, remove_short_tokens: bool = True, remove_stop_words: bool = True) -> list:
    """ 'some string' -> ['some', 'string'] """
    # replace non-char + space with just space
    value = re.sub(r'\s\W+', ' ', value)
    value = re.sub(r'\W+\s', ' ', value)

    value = re.sub(r'(\d|\W){3,}', ' ', value)

    value = re.sub(r'[:;()|]', '', value)

    tokens = map(str.lower, re.split(r'\s', value))

    if remove_short_tokens:
        tokens = [token for token in tokens if len(token) > 1]

    # stop words must be applied before stemming
    if remove_stop_words:
        tokens = [token for token in tokens if token not in STOP_WORDS]

    return tokens


def get_timestamp():
    return datetime.now().strftime("%Y.%m.%d.%H.%M")


def is_abbrev(value1: str, value2: str) -> bool:
    """
    Checks whether value1 is abbreviation of value2 or vice versa.
    """
    if not value1 or not value2:
        return False

    abbrev = min(value1, value2, key=len).lower()

    # abbrev should be shorter than text
    if len(value1) == len(value2):
        return False

    # abbrev shouldn't consist of several words
    if ' ' in abbrev:
        return False

    # all chars from abbres should be in text
    text = (value1 if abbrev == value2 else value2).lower()
    if not all(ch in text for ch in abbrev):
        return False

    # none of the words can start with abbrev entirely
    if any(word.startswith(abbrev) for word in text.split()):
        return False

    # abbrev must include first letters from all text words, in the same order
    if not re.match(''.join(f'{re.escape(word[0])}.*' for word in tokenize(text)), abbrev, flags=re.IGNORECASE):
        return False

    # TODO:
    # if abbrev == 'healthy choice' and text == 'healthy choice simply steamers creamy spinach & tomato':
    #     - works forever!
    return bool(re.match(r'^' + r'(\w*\s+)*'.join(re.escape(letter) for letter in abbrev) + r'(\w*\s*)+$', text, flags=re.IGNORECASE))


def ngrams(*args, **kwargs):
    """ This is a fix for nltk.ngrams which raises `StopIteration` """
    try:
        for elem in nltk_ngrams(*args, **kwargs):
            yield elem
    except (RuntimeError, StopIteration):
        return


def common(value1: Iterable, value2: Iterable, min_ngrams: int = 1, max_ngrams: Optional[int] = None,
           include_subgrams: bool = False) -> Set[str]:
    """
    Retrieves common items from value1 and value2.
    """
    if not max_ngrams:
        max_ngrams = min(len(value1), len(value2))

    value1_ngrams = set(chain.from_iterable(
        ngrams(value1, n) for n in range(min_ngrams, max_ngrams+1)
    ))
    value2_ngrams = set(chain.from_iterable(
        ngrams(value2, n) for n in range(min_ngrams, max_ngrams+1)
    ))

    results = value1_ngrams.intersection(value2_ngrams)
    if not include_subgrams:
        for ng1, ng2 in combinations(results, 2):
            s1 = '|'.join(ng1)
            s2 = '|'.join(ng2)

            if s1 in s2 and ng1 in results:
                results.remove(ng1)
            elif s2 in s1 and ng2 in results:
                results.remove(ng2)

    return results


def common_words(value1: str, value2: str, include_subwords: bool = False) -> List[str]:
    """
    Retrieves common words from value1 and value2
    """
    return [' '.join(words) for words in common(tokenize(value1.lower()), tokenize(value2.lower()),
                                                include_subgrams=include_subwords)]


def closeness(value1: str, value2: str) -> float:
    """
    Calculates how close are `value1` and `value2`.
    """
    if not value1 or not value2:
        return 0

    return max([len(word) for word in common_words(value1, value2)] or [0]) / max(len(value1), len(value2))


def remove_empty_values(data: dict) -> dict:
    """
    Goes through dict keys and removes keys with empty values. Recursively.
    """
    result = {}
    for key, value in data.items():
        if isinstance(value, dict):
            value = remove_empty_values(value)

        if value:
            result[key] = value

    return result


def compare(value1: str, value2: str) -> bool:
    """ Compare strings """
    def remove_s(word: str) -> str:
        return re.sub(r'(\w)s$', r'\1', word)

    value1 = ''.join(map(remove_s, tokenize(value1, remove_short_tokens=False, remove_stop_words=False)))
    value2 = ''.join(map(remove_s, tokenize(value2, remove_short_tokens=False, remove_stop_words=False)))

    def only_chars(value: str) -> str:
        return re.sub(r'[^\w]', '', value)

    return only_chars(value1) == only_chars(value2)


def test_compare():
    assert compare('tesla gear', 'teslagears')
