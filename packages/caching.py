import hashlib
import inspect
import logging
import lzma
import pickle
import time
from datetime import timedelta, datetime
from functools import wraps
from operator import itemgetter
from typing import Optional

import networkx as nx
from sqlalchemy import Column, Integer, DateTime, String
from sqlalchemy.dialects.mysql import MEDIUMBLOB
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from mapper.settings import db_engine
from packages.utils import NeverFail, LOCAL

logger = logging.getLogger(f'adg.{__name__}')
Base = declarative_base()


@NeverFail(fallback_value=None)
def make_pickable(graph: nx.DiGraph):
    key = ('soup', (), ())
    for entity in map(itemgetter('entity'), graph.nodes.values()):
        wiki_page = entity.get('wiki_page')
        if wiki_page and hasattr(wiki_page, '__cached_methods__') and key in wiki_page.__cached_methods__:
            del wiki_page.__cached_methods__[key]


class Cache:
    __tablename__ = 'cache'
    __table_args__ = {
        # 'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8',
        'mysql_collate': 'utf8_general_ci',
    }

    id = Column(Integer, primary_key=True)
    cache = Column(MEDIUMBLOB)
    datetime = Column(DateTime)
    uid = Column(String(32), index=True)

    # some extra info
    name = Column(String(64))
    args = Column(String(256))


class DefaultCache(Cache, Base):
    """ Default cache, that uses "cache" table """
    pass


class ERToolCache(Cache, Base):
    """ Cache for ER tools """
    __tablename__ = 'cache_er_tools'


class TickerCache(Cache, Base):
    __tablename__ = 'cache_ticker_finder'


class DbCache:
    Session = sessionmaker(bind=db_engine)
    PENDING_QUERIES = set()

    def __init__(
        self,
        cache_class=DefaultCache,
        expiration: timedelta = timedelta(days=14),
        cache_if: Optional[callable] = None,
    ):
        Base.metadata.create_all(db_engine)  # create table if it doesn't exist
        self.cache_class = cache_class
        self.expiration = expiration
        self.cache_if = cache_if

    @staticmethod
    def to_str(value) -> str:
        return str(value).lower()

    def __call__(self, fn):
        @wraps(fn)
        def caching_fn(*args, **kwargs):
            # in order to deserialize
            from mapper.mapper import ComparisonGraph  # NOQA

            arguments = list(map(self.to_str, args)) + ['{k}={self.to_str(v)}' for k, v in kwargs.items()]
            key = hashlib.md5('::'.join(map(str, [
                fn.__name__,
                inspect.getsource(inspect.unwrap(fn)),  # TODO: doesnt work with dynamic conditional_decorator
                arguments,
            ])).encode('utf-8')).hexdigest()

            # wait if cache is already populated
            if key in self.PENDING_QUERIES:
                logger.debug(
                    f'Waiting for cache to appear for {fn.__name__}{args, kwargs}, table {self.cache_class.__tablename__}'
                )

                for _ in range(20):
                    if key not in self.PENDING_QUERIES:
                        break
                    time.sleep(0.3)

            cached_result = None

            if not LOCAL.settings.get('CACHE_UPDATE', False):
                logger.debug(f'Checking "{fn.__name__}{args, kwargs}" cache in "{self.cache_class.__tablename__}" '
                             f'(key={key})')

                session = self.Session()
                try:
                    cached_result = session.query(self.cache_class).filter(
                        self.cache_class.uid == key,
                        self.cache_class.datetime >= datetime.now() - self.expiration,
                    ).order_by(self.cache_class.datetime.desc()).first()
                except Exception:
                    logger.exception(f'Could not read cache for {fn.__name__}{args, kwargs}')
                session.close()

            if cached_result:
                try:
                    logger.debug(f'Using cached response for "{fn.__name__}{args, kwargs}"')
                    return pickle.loads(lzma.decompress(cached_result.cache))

                except Exception:
                    logger.exception(f'Failed to restore cached results for '
                                   f'"{fn.__name__}{args, kwargs}" from "{self.cache_class.__tablename__}"')

            self.PENDING_QUERIES.add(key)
            result = fn(*args, **kwargs)
            if key in self.PENDING_QUERIES:
                self.PENDING_QUERIES.remove(key)

            if isinstance(result, nx.DiGraph):
                make_pickable(result)

            if not self.cache_if or self.cache_if(result):
                logger.debug(f'Saving cache for "{fn.__name__}{args, kwargs}"')
                session = self.Session()  # TODO: replace with db_engine
                session.add(
                    self.cache_class(
                        uid=str(key),
                        cache=lzma.compress(pickle.dumps(result)),
                        datetime=datetime.now(),
                        name=fn.__name__,
                        args=', '.join(arguments),
                    )
                )
                session.commit()
                session.close()
            else:
                logger.debug(f'Not saving cache for "{fn.__name__}{args, kwargs}"')

            return result

        return caching_fn
