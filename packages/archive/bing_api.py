"""
run_bing_search_api_wf
Run Bing search on "what company owns [input string] wikipedia" and return results in JSON

Args:
    search_term (string): string to perform Bing search
    api_key (string): Bing search API key
    limit (int): max number of Bing search results

Returns:
    A dictionary with the following keys:
    input: original search term
    search_results: Bing search results in JSON
"""

from py_ms_cognitive import PyMsCognitiveWebSearch
api_key = '05d6f617289246c2a886b0a484b742ec'
    
def bing_api(search_term, api_key=api_key, limit=50):
    custom_params={"mkt": "en-US",
          "responseFilter": "Webpages",
          "safeSearch": "Moderate"}
    
    #try:
    search_results=PyMsCognitiveWebSearch(api_key,search_term,custom_params).search(limit=limit, format='json')
        
    return search_results

    #except:
    #    print('Bing API 50 Concurrent Ping Limit Exceeded')