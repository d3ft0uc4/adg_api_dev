from packages.settings import REL_DATA_DIR, SEARCH_DATA_JSON_DIR
from mapper.settings import QUERY_TYPES, SEARCH_ADDON_STRING_MAP
import os
import json


class SearchResults:
    """ A class for saving, loading and filtering search results ({uid: {query_type: json data}}) """
    def __init__(self, contents):
        self.contents = contents

    @staticmethod
    def load_from_disk(search_items, input_dir=None):
        contents = {}
        if not input_dir:
            input_dir = os.path.join(REL_DATA_DIR, SEARCH_DATA_JSON_DIR)
        all_json_files = [input_dir + '/' + obj for obj in os.listdir(input_dir) if obj.endswith(".json")]
        for file in all_json_files:
            with open(file, 'r') as fp:
                try:
                    jsn = json.load(fp)
                    input_term = jsn['input']
                    contents[input_term] = jsn
                except Exception:
                    print('Malformed JSON File: ' + file)
                    raise

        with open('terms_not_loaded.txt', 'w') as f:

            all_search_results = {}
            for uid, term in search_items:
                if uid not in all_search_results:
                    all_search_results[uid] = {}
                for query_type in QUERY_TYPES:
                    complete_term = SEARCH_ADDON_STRING_MAP[query_type].format(term)
                    try:
                        jsn = contents[complete_term]

                        all_search_results[uid][query_type] = jsn
                    except:
                        print('Term not loaded:', complete_term)
                        f.write(str(complete_term) + ';' + str(query_type) + ';' + str(term) + '\n')

        return all_search_results

    def filter_search_results(self, exclude_uids):
        """ Removes search results that are present in `exclude_uids`. """
        self.contents = {uid: value for uid, value in self.contents.items() if uid not in exclude_uids}

    def output(self, how='disk'):

        if how == 'disk':
            output_dir = os.path.join(REL_DATA_DIR, SEARCH_DATA_JSON_DIR)
            for k, v in self.contents.items():
                string = k + '$%&' + v['input']
                out_filename = os.path.join(output_dir, string.replace('/', '_') + '.json')
                with open(out_filename, 'w') as fp:
                    json.dump(v, fp)
                try:
                    assert out_filename in [output_dir + f for f in os.listdir(output_dir)]
                    #print(out_filename + ' written ')
                except AssertionError:
                    print(out_filename + ' not written')
                    raise Exception
        else:
            raise Exception
