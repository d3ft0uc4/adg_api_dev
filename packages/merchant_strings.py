"""
© 2018 Alternative Data Group. All Rights Reserved.

TODO: This class is almost the same as Targets, we should merge them. Also, it's only used in FingMaker, move to that module.
"""

import os

import sqlalchemy
import pandas as pd

from packages.settings import REL_DATA_DIR
from mapper.settings import SQL_CONNECTION_STRING
from packages.utils import uid

from find_maker.preprocessing import preprocessed_text

# TODO: These are only used in MerchantStrings which should live in find_maker... like this setting.
INPUT_STRINGS_SOURCEFILE = '5_merchant_strings.xlsx'
# INPUT_STRING_TABLE_TERM_COL = 'match_target_unofficial'
INPUT_STRING_TABLE_TERM_COL = 'merchant_string'
INPUT_STRING_TABLE_UID_COL = 'uidlist_agg_merch_master_top_merchants'
# TODO: This one is also used in find_maker.score.Score... Same thing.
INPUT_STRING_TABLE_NAME = 'agg_merch_master_top_merchants_unique'


class MerchantStrings:
    def __init__(self, load_from='database'):
        if load_from == 'disk':
            self.contents = self._is_load_from_disk(REL_DATA_DIR, INPUT_STRINGS_SOURCEFILE)
        elif load_from == 'database':
            self.contents = self._is_load_from_db()
        elif load_from == 'test':
            self.contents = self._test_data()

    def filter_merchant_strings(self, filter_list):
        old_contents = self.contents
        new_contents = {k: v for k, v in old_contents.items() if v in filter_list}
        self.contents = new_contents

    def preprocess_text(self):
        new_contents = {k: preprocessed_text(v) for k, v in self.contents.items() if v}
        self.contents = new_contents

    # TODO: Fix; New git migration broke this.
    def _is_load_from_disk(self,enclosing_dir, file_name):
        file_path = os.path.join(enclosing_dir, file_name)
        df = pd.read_excel(file_path)
        strings = df['inputs'].tolist()
        # TODO: Grab from file when it has uid.
        uids = [uid() for i in range(len(df))]
        contents = dict(zip(uids, strings))
        return contents

    def _is_load_from_db(self):
        db = sqlalchemy.create_engine(SQL_CONNECTION_STRING)
        df = pd.read_sql(
            """SELECT DISTINCT({0}) AS '{0}', SUBSTRING({1}, 1, 36) AS '{1}'
                   FROM {2}
            """.format(INPUT_STRING_TABLE_TERM_COL, INPUT_STRING_TABLE_UID_COL, INPUT_STRING_TABLE_NAME),
            db)
        df.index = df[INPUT_STRING_TABLE_UID_COL]
        contents = df[INPUT_STRING_TABLE_TERM_COL].to_dict()
        return contents

    def _test_data(self):
        return dict((str(i), v) for i, v in enumerate(('microsoft', 'skype')))

