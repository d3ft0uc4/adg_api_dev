import xgboost as xgb
import pickle
from typing import List, Tuple
import logging

logger = logging.getLogger(f'adg.{__name__}')


def xgb_load_model(pth: str) -> Tuple[List[str], xgb.Booster]:
    """ Given path of pickled file, returns header and Booster model """
    with open(pth, 'rb') as f:
        return pickle.load(f)


def xgb_predict(scores: dict, header: List[str], model: xgb.Booster) -> float:
    """ Given xgboost model and dict of scores, returns a probability of model for those scores """

    # 1) rearrange scores to fit model header
    ordered_scores = {key: 0 for key in header}

    for field, value in scores.items():
        if field.startswith('__') or field.startswith('#'):
            continue

        if field not in header:
            logger.debug(f'Field "{field}" not in model\'s header {header}, skipping')
            continue

        ordered_scores[field] = float(value)

    data = xgb.DMatrix(list(ordered_scores.values()))
    return float(model.predict(data)[0])
