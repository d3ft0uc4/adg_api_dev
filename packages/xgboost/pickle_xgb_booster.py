# script to convert Gene's XGBmodel to pickled booster
import xgboost as xgb
import csv
import os
import pickle
from os import path, listdir
from packages.utils import logger


def pack(pth):
    files = listdir(pth)
    predictors_path = next(filter(lambda name: name.endswith('.csv') and 'predictions' not in name, files))
    model_path = next(filter(lambda name: name.endswith('.XGBmodel'), files))
    logger.debug(f'Predictors: {predictors_path}')
    logger.debug(f'Model: {model_path}')

    booster = xgb.Booster({'nthread': 4}, model_file=os.path.join(pth, model_path))
    with open(os.path.join(pth, predictors_path), 'r') as f:
        data = list(csv.reader(f))
        header = [col for col in data[0] if not col.startswith('#')][1:]

    print(f'Found header:')
    for field in header:
        print(field)

    pkl_path = path.join(pth, 'xgb_boost.' + model_path[:-len('.XGBmodel')] + '.pickle')
    with open(pkl_path, 'wb') as f:
        pickle.dump((header, booster), f)
        print(f'Pickled model to "{pkl_path}"')


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('dir')
    args = parser.parse_args()

    pack(args.dir)
