import logging
from typing import List

from mapper.logs import load_logs
from mapper.tuning.graph_scores import WRONG_INPUTS
from packages.utils import similar
from unofficializing.unofficializing import make_unofficial_name as uo

logger = logging.getLogger(f'adg.{__name__}')


def load_logs_for_training():
    """
    Loads log rows which are suitable for training models.
    """
    logs = \
        load_logs(golden_only=True, mode='merchant', golden_table='golden_set_domain_merchant') + \
        load_logs(golden_only=True, mode='merchant', golden_table='golden_set_bumped')

    logs = [log for log in logs if log['raw_input'] not in WRONG_INPUTS]
    if not logs:
        raise ValueError('No logs found')

    for log in logs:
        snap = log['snapshot']
        snap['attempts'] = [
            attempt
            for attempt in snap['attempts']
            if attempt['cleaned'] and (not log['correct_cleaned'] or (log['correct_cleaned'] in attempt['cleaned']))
        ]

    return logs


def generate_training_data(recognizer: callable) -> List[dict]:
    """
    Generates data for feeding into xgboost model
    """
    logs = load_logs_for_training()

    # run bane on each cleaned input and decide whether each name is correct or not
    results = []
    for log in logs:
        for attempt in log['snapshot']['attempts']:
            cleaned = attempt['cleaned']

            for name, predictors in recognizer(cleaned):
                name_uo = uo(name)
                correct_values = [uo(value) for value in log['correct_company_names']]
                # TODO: correct_values may contain 'no match / small business'

                if any(similar(name_uo, correct_value) for correct_value in correct_values):
                    match = 1
                elif any(name_uo[:4] == correct_value[:4] for correct_value in correct_values):
                    match = '?'
                else:
                    match = 0

                results.append({
                    'match': match,
                    '#answer': '\n'.join(correct_values),
                    '#name': name,
                    '#cleaned': cleaned,
                    '#raw_input': log['snapshot']['original_input'],
                    **predictors,
                })

    return results
