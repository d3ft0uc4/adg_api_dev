"""
© 2018 Alternative Data Group. All Rights Reserved.
"""

import os

from dotenv import load_dotenv


load_dotenv()


# TODO: What are custom_settings for?
# Make a `custom_settings.py` file with the variable which won't be committed.
if os.path.exists('packages/custom_settings.py'):
    from packages.custom_settings import REL_DATA_DIR
else:
    # FIXME: This is hardocded.
    REL_DATA_DIR = '../../../adg_api_dev_share'  # This shuold be relative to the repo root.


# TODO: These are only used in Targets which should probably live in find_maker... like this setting.
TARGETS_SOURCEFILE = "small_target_list.csv"
TARGET_TABLE_UID_COLUMN = 'uid'
TARGET_TABLE_STRING_COLUMN = 'match_target'
# TODO: This one is also used in find_maker.score.Score... Same thing.
TARGET_TABLE_NAME = 'entity_match_target_map_tmp'

# TODO: All these also seem specific to FindMaker. (FindWinner may also use them.) Move to that module?

# RUN_CODE = 'T2T-3'
RUN_CODE = 'TESTT'

TOP_LEVEL_PACKAGE_NAME = 'adg_api_dev'

# These are relative to REL_DATA_DIR, which is relative to TOP_LEVEL_PACKAGE_NAME
SEARCH_DATA_JSON_DIR = os.path.join("bing_querier", "temp_rok")
FIND_MAKER_OUTPUT_DIR = os.path.join("find_maker", "temp")

