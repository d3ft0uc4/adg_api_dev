"""
© 2018 Alternative Data Group. All Rights Reserved.
"""

from collections import Counter
import json
import ast
import pandas as pd
import re

# multi-word tokens preceded by a whitespace.
multi_tokens = ['insurance company of america',                 'national association',                 'restaurant and cafe',                 'hldngs grp',                 'company the',                 'and company',                 'group of companies',                ]

# Superstring tokens preceded by a whitespace.  Superstring is a string that contains another removable token within the string itself
super_tokens = ['restaurants',                 'holdings',                 'systems',                 'corporation',                 'enterprises',                 'communications',                 'company',                 'incorporation',                'llp',               'incorporated',              ]

# Substring tokens preceded by a whitespace.
sub_tokens = ['(\(.+\))',               'restaurant',               'companies',               'international',               'limited',               'lp',               'ltd',               'llc',               'holdco',               'plc',               'nv',               'japan',               'italy',               'pcl',               'sa/nv',               'global',               'industries',               'holding',               'etf',               'entertainment',               'group',               'enterprise',               'sa$',               'se$',               'us$',               'inc$',               'com$',               'ag$',               'corp$',               'co$',               'uk$',               'ab$',               'a/s',               'pty',               'fc',               'de cv',               'sab$',             ]

# No space tokens are tokens that ARA NOT preceded or followed by a whitespace.
ns_tokens = ['(\[.+\])',              '\s$',              u'(\\u2018|\\u2019)',             ]

do_not_touch = {'American International Group, Inc.': 'american international group'}

# Map single string
def map_single_str(string, input_is_dict=False):
    if input_is_dict==True:
        string = ast.literal_eval(json.dumps(string))
    cstring = special.sub("", str(string).lower())
    cstring = dotcom.sub("dot_com", cstring)
    cstring = punct.sub("", cstring)
    cstring = bos.sub("", cstring.strip())
    if input_is_dict==False:
        cstring = no_space.sub("", cstring)
    else:
        cstring = no_space2.sub("", cstring)
    cstring = multi.sub("", cstring)
    cstring = superstring.sub("", cstring)
    if input_is_dict==False:
        cstring = substring.sub("", cstring)
    else:
        cstring = substring2.sub("", cstring)
    cstring = symbols.sub("", cstring)
    cstring = connectors.sub(" ", cstring)
    if input_is_dict==False:
        cstring = substring.sub("", cstring)
    else:
        cstring = substring2.sub("", cstring)
    cstring = spaces.sub(" ", cstring)
    cstring = comdot.sub(".com", cstring)
    if input_is_dict==False:
        cstring = no_space.sub("", cstring)
    else:
        cstring = no_space2.sub("", cstring)
    return cstring

# Map multiple strings and return a dictionary
def map_all_str(strings, do_not_touch=do_not_touch, invertdict=False, input_is_dict=False):
    str_2_cstr = {}
    mapping = {}
    for y in strings:
        cstring = map_single_str(y, input_is_dict=input_is_dict)
        str_2_cstr[y] = cstring
    c = Counter(str_2_cstr.values())
    dup = [k for k, v in str_2_cstr.items() if c[v] > 1]
    for d in dup:
        str_2_cstr[d] = d.lower()
    for k, v in do_not_touch.items():
        if k in str_2_cstr.keys():
            str_2_cstr[k] = v
    if invertdict == True:
        mapping = {v: k for k, v in str_2_cstr.iteritems()}
    else:
        mapping = str_2_cstr
    return mapping

# Convert token lists into regex strings
def rstring(tokens):
    r = '('
    count = 0
    for m in tokens:
        if count == 0:
            r = r + m
        else:
            r = r + '|' + m
        count += 1
    r = r + ')'
    return r

# Convert token lists into regex strings
rmulti = rstring(multi_tokens)
rsuper = rstring(super_tokens)
rsub = rstring(sub_tokens)
rsub2 = rstring(sub_tokens[1:])
rns = rstring(ns_tokens)
rns2 = rstring(ns_tokens[1:])

# Compile Regex routines
special = re.compile("\s(s\.p\.a\.)|(, s\.a\.b\.)|(, s\.a\.)") #remove special strings with punctuations
dotcom = re.compile("(\.com)") #replace .com with temp name
punct = re.compile(",|\.|&") #remove common punctuations
bos = re.compile("^\s|(the)\s") #remove strings and whitespace at beginning of sentences
no_space = re.compile(rns) #remove strings 
multi = re.compile("\s" + rmulti ) #remove phrases preceded by whitespace
superstring = re.compile("\s" +rsuper) #remove superstring preceded by whitespace
substring = re.compile("\s" + rsub) #remove substring preceded by whitespace
symbols = re.compile("[/'~*&%$#@!,\.`]") #remove symbols
connectors = re.compile("\s(in|st|la|the|and|on|or|co)\s") #replace connecting words with spaces
spaces = re.compile("\s+") #remove extra space
comdot = re.compile("(dot_com)") #restore temp name to .com

substring2 = re.compile("\s" + rsub2) #remove substring preceded by whitespace
no_space2 = re.compile(rns2) #remove strings

ourRaw=pd.read_clipboard()
input=ourRaw.iloc[:,0]
outDict=map_all_str(input)
output=pd.DataFrame.from_dict(outDict, orient="index")
output=pd.Series(outDict).to_frame()
output.to_clipboard()