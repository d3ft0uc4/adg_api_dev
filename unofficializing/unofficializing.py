"""
© 2018 Alternative Data Group. All Rights Reserved.

This script is used to clean corporate titles from entity names such as "Cocacola Co." -> "Cocacola". This helps us
have standard entity names that can be compared across results from our different tools.

Usage:
    python -m unofficializing.unofficializing "Wal-Mart inc."
"""


import re
from collections import defaultdict
from functools import wraps
from os import path

import pandas as pd

# FIXME: Do we need these files in the repo? They came from agd_api_dev/ match_targets/ and original_data/ (DropBox)
from mapper.settings import COMPANY_TERMINATORS
from packages.utils import strip_accents

BASE_FILE_PATH = path.dirname(path.realpath(__file__))

PREFIX_LENGTH = 4

existing_entity_map = pd.read_csv(path.join(BASE_FILE_PATH, "data/entity_match_target_map_20180226.csv"), sep=",")
mapped_entities = pd.Series(existing_entity_map.entity_name)

brands = open(path.join(BASE_FILE_PATH, "data/brand_names.txt"), 'r').read().split('\n')
brands_list = pd.Series(brands)
brands_groups = defaultdict(list)
for brand in brands:
    brands_groups[brand[:PREFIX_LENGTH]].append(brand)


dictionary = open(path.join(BASE_FILE_PATH, "data/english_words.txt"), 'r').read().split('\n')
dictionary_groups = defaultdict(list)
for word in dictionary:
    dictionary_groups[word[:PREFIX_LENGTH]].append(word)

match_targets = existing_entity_map.match_target.unique()
match_targets_groups = defaultdict(list)
for target in match_targets:
    match_targets_groups[target[:PREFIX_LENGTH]].append(target)


def _get_repeats_count(entity):
    return 0
    # return mapped_entities[mapped_entities.str.match("\\b{}\\b".format(entity))].count() \
    #     - mapped_entities[mapped_entities.str.contains(entity)].count() \
    #     + brands_list[brands_list.str.match("\\b{}\\b".format(entity))].count() \
    #     - brands_list[brands_list.str.contains(entity)].count()


# def no_empty_result(fn):
#     @wraps(fn)
#     def wrapper(*args, **kwargs):
#         # return result or original input if result is empty
#         return fn(*args, **kwargs) or args[0]
#
#     return wrapper
#
#
# @no_empty_result
def make_unofficial_name(in_entity: str, aggressive: bool = False) -> str:
    """
    "Unofficializes" input string - removes company form and other unnecessary stuff,
    leaving only pure company name.
    """
    if not in_entity:
        return ''

    entity = in_entity.lower()
    regex_tier1 = [
        r'\)|\(', r'\"',
        r'\bl\.*p\.*\b',
        r'\bl\.*t\.*d\.*\b',
        r'\bl\.*l\.*c\.*\b',
        r'\bp\.*l\.*c\.*\b',
        r'\bp\.*c\.*l\.*\b',
        r'\bl\.*l\.*p\.*\b',
        r'\bsa/nv\.*\b',
        r'\bi\.*n\.*c\.*\b',
        r'\bu\.*s\.*a\.*\b$',
        r'\bs\.*a\.*\b',
        r'\bs\.*e\.*\b',
        r'\bu\.*s\.*\b',
        r'\bc\.*o\.*\b\s+',
        r'\bu\.*k\.*\b',
        r'\ba\.*b\.*\b',
        r'\ba/s\.*\b',
        r'\bpty\.*\b',
        r'\bf\.*c\b',
        r'\bde cv\.*\b',
        '\\bs\\.*a\\.*b\\.*\\b', '\\bn\\.*a\\.*\\b', '\\bs\\.*p\\.*a\\.*\\b', ',', '\\.(?!\\bcom\\b)', '\\W*$',
        r'^\W+$',
        r'\bthe\b',
        r'\Wn.?v.?(\W|$)',
        r'\Wv.?o.?f.?(\W|$)',
    ]

    # IMPORTANT! No brackets in terms below (will break \1 and \2 in further regex)
    terms_to_remove = [
        r'restaurants',
        r'holdings?',
        r'holdco\.?',
        r'systems',
        r'stores?',
        r'corporation',
        r'corp\.?',
        r'enterprises',
        r'organization',
        r'company',
        r'co',
        r'incorporation',
        r'incorporated',
        r'global',
        r'industries',
        r'companies',
        r'international',
        r'japan',
        r'limited',
        r'pictures',
        r'motors?',
        r'etf',
        r'entertainment',
        r'services',
        r'group',
        r'enterprise',
        r'private',
        r'italy',
        # r'in',  # <- wat?
        # r'on',
        # r'or',
        # r'and',
    ] + list(map(re.escape, COMPANY_TERMINATORS))

    # replace "accent" letters with ASCII analogs (i.e. 'é' -> 'e')
    entity = strip_accents(entity)

    # replace "-" with whitespaces
    entity = entity.replace('-', ' ')

    # remove ".com"
    entity = re.sub(r'\.com\b', '', entity)

    entity = re.sub(r"(\W+$)|(^\W+)", "", entity)
    entity = re.sub(r"\s{2,}", " ", entity)

    # "kohl's corporation" -> "kohls corporation"
    entity = re.sub(r'\'s\b', 's', entity)

    # technologies -> tech
    entity = re.sub(r'\btechnologies\b', 'tech', entity)

    for regexp in regex_tier1:
        entity = re.sub(regexp, "", entity, flags=re.IGNORECASE)

    # # remove "company" suffixes
    # entity = re.sub(r'(\w|,) (' + '|'.join(map(re.escape, COMPANY_TERMINATORS)) + r')\b', r'\1', entity,
    #                 flags=re.IGNORECASE)

    if entity in dictionary_groups[entity[:PREFIX_LENGTH]]:
        return str.lower(entity)

    if entity in match_targets_groups[entity[:PREFIX_LENGTH]]:
        return entity

    if entity in brands_groups[entity[:PREFIX_LENGTH]]:
        return entity

    repeats = _get_repeats_count(entity)

    entity_new = entity

    for term in terms_to_remove:
        entity_new = re.sub(r'(\w|,)\s' + term + r'(\s|$)', r'\1\2', entity, flags=re.IGNORECASE)

        # if entity was like "something and <term>" -> don't delete the term
        if re.search(r'(&|and)\s*$', entity_new, flags=re.IGNORECASE):
            entity_new = entity
            continue

        # entity_new = re.sub(regexp, "", entity, flags=re.IGNORECASE)
        entity_new = re.sub(r"(\W+$)|(^\W+)", "", entity_new)
        entity_new = re.sub(r"\s{2,}", " ", entity_new)

        if entity_new in dictionary_groups[entity_new[:PREFIX_LENGTH]]:
            return entity_new

        if entity_new in match_targets_groups[entity_new[:PREFIX_LENGTH]]:
            return entity_new

        if entity_new in brands_groups[entity_new[:PREFIX_LENGTH]]:
            return entity_new

        new_repeats = _get_repeats_count(entity_new)

        if new_repeats <= repeats:
            repeats = new_repeats
            entity = entity_new
    return entity if len(entity_new) < 2 else entity_new


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('input_string')
    args = parser.parse_args()
    print(make_unofficial_name(args.input_string))
