"""
© 2018 Alternative Data Group. All Rights Reserved.

Module for getting domain's whois info.

Usage from CLI:
    python -m domain_tools.whois "google.com" "adg.com"
"""

import json
import logging
import re
import requests
from requests import ReadTimeout, ConnectTimeout
from typing import Optional
from urllib.parse import urlencode
from datetime import timedelta

from mapper.settings import USE_CACHE, CACHE_DIR, COMPANY_TERMINATORS
from packages.caching import DbCache
from packages.utils import Timer, conditional_decorator, pretty

logger = logging.getLogger(f'adg.{__name__}')

DEBUG = __name__ == '__main__'


class WhoisInfoRetrieval:
    # TODO: Remove secrets from repo. Move to settings file at least initially.
    API_KEY = 'at_DqB5gXVnOrQWXEkUjICJc9wI80ezN'
    API_URL = 'http://www.whoisxmlapi.com/whoisserver/WhoisService'

    # following rules cannot be overwritten by whitelist
    BLACKLIST = ['Privacy', 'Protection']
    RE_BLACKLIST = re.compile(
        r'(' + '|'.join(rf'\b{re.escape(bad)}\b' for bad in BLACKLIST) + r')',
        flags=re.IGNORECASE,
    )

    # following rules can be overwritten by whitelist
    GRAYLIST = ['Privacy', 'Protect', 'Whois', 'Guard', 'Domain', 'Private', 'Registration', 'Block',
                'Proxy', 'Disclosed', 'Secret', 'Host', 'Admin', 'Holder', 'Registration Private',
                'Redacted', 'Secret', 'N/A', 'Registry', 'Registrant', 'Available',
                'Behalf', 'Owner', 'Not Available From Registry', 'Translation Failed',
                'MarkMonitor']
    RE_GRAYLIST = re.compile(
        r'(' + '|'.join(re.escape(bad) for bad in GRAYLIST) + r')',
        flags=re.IGNORECASE,
    )

    WHITELIST = COMPANY_TERMINATORS
    RE_WHITELIST = re.compile(
        r'(' + '|'.join(rf'\b{re.escape(good)}\b' for good in WHITELIST) + r')',
        flags=re.IGNORECASE,
    )

    NUM_RETRIES = 3

    @classmethod
    @conditional_decorator(DbCache(expiration=timedelta(days=30)), USE_CACHE)
    def _query_api(cls, domain: str) -> Optional[dict]:
        """ TODO: Doc. """
        url = f'{cls.API_URL}?' + urlencode({
            'domainName': domain,
            'apiKey': cls.API_KEY,
            'outputFormat': 'json',
        })

        response = None
        for i in range(cls.NUM_RETRIES):
            try:
                response = requests.get(url, timeout=2 * (i + 1))
                break
            except (ConnectTimeout, ReadTimeout) as exc:
                logging.debug(f'Request to {url}: {exc}')
                pass

        if not response:
            logger.error(f'Request to {url} failed')
            return
        response.raise_for_status()

        result = response.json()
        error = result.get('ErrorMessage')
        if error:
            logger.error(f'Result for {domain}: {error["msg"]}')
            return

        return result['WhoisRecord']

    @classmethod
    def is_banned(cls, value: str) -> bool:
        """ Whether given value contains bad words. """
        if cls.RE_BLACKLIST.search(value):
            return True

        if cls.RE_WHITELIST.search(value):
            return False

        if cls.RE_GRAYLIST.search(value):
            return True

        return False

    @classmethod
    def _clean_whois(cls, organization: str) -> Optional[str]:
        """ TODO: Doc. """
        organization = re.sub(r'[\(\[].*?[\)\]]', '', organization).strip()
        if organization:
            return organization

    @classmethod
    def get_organization(cls, domain: str) -> Optional[str]:
        """ Returns organization name from domain's whois info. """
        try:
            result = cls._query_api(domain)
            if DEBUG:
                logger.debug(pretty(result))

            if not result:
                return
            # logging.debug(json.dumps(result, indent=4))

            # base name & organization
            registrant = result.get('registrant', {})
            name = registrant.get('name')
            organization = registrant.get('organization')

            # name & organization from registry data
            registry_registrant = result.get('registryData', {}).get('registrant', {})
            registry_name = registry_registrant.get('name')
            registry_organization = registry_registrant.get('organization')

            # winner selection logic
            if name and cls.is_banned(name):
                # "name": "zenfolio.com, Secret Registration Customer ID 1226",
                # "organization": "Oracle + Dyn",
                logger.debug(f'Name "{name}" blacklisted -> ignoring Whois completely')
                return

            all_candidates = [organization, name, registry_organization, registry_name]
            candidates = [cndt for cndt in all_candidates if cndt and not cls.is_banned(cndt)]
            if candidates:
                return cls._clean_whois(candidates[0])  # TODO: return multiple candidates and their confidences

            logger.debug(f'Could not find organization entry for {domain}, ' \
                         f'candidates were {all_candidates}')

        except Exception as exc:
            logger.error(f'Whois query for {domain} failed: {exc}')


if "__main__" == __name__:
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('input')
    args = parser.parse_args()

    with Timer(f'Whois {args.input}'):
        result = WhoisInfoRetrieval.get_organization(args.input)
    print(pretty(result))
