"""
© 2018 Alternative Data Group. All Rights Reserved.

Module for getting SSL certificate info.
Cli:
    python -m domain_tools.ssl "google.com" "adg.com"
"""

import logging
import socket
import ssl
from typing import Optional

# from cryptography import x509
# from cryptography.hazmat.backends import default_backend
from packages.utils import Timer

logger = logging.getLogger(f'adg.{__name__}')


class SSLCertificateRetrieval:
    BLACKLIST = ['godaddy', 'cert', 'comodo',
                 'amazon', 'cloudflare', 'globalsign',
                 'identrust', 'symantec', 'digicert',
                 'certum', 'entrust', 'startcom',
                 'secom', 'hydrantid', 'fastly']
    TIMEOUT = 3

    @classmethod
    def in_blacklist(cls, value: str) -> bool:
        value_low = value.lower()
        return any(bad.lower() in value_low for bad in cls.BLACKLIST)

    @classmethod
    def get_cert(cls, domain: str) -> dict:
        """ Retrieve server's certificate for specified domain """
        # https://unix.stackexchange.com/a/104624/142903

        # it is similar to ssl.get_server_certificate() but it returns a dict
        # and it verifies ssl unconditionally, assuming create_default_context does
        try:
            with socket.create_connection((domain, 443), timeout=cls.TIMEOUT) as sock:
                context = ssl.create_default_context()
                # context = ssl._create_unverified_context()
                with context.wrap_socket(sock, server_hostname=domain) as sslsock:
                    return sslsock.getpeercert()

        except Exception as exc:
            logger.debug(f'Could not get SSL certificate for domain "{domain}": {exc}')

        return {}

    @classmethod
    def get_organization(cls, domain: str) -> Optional[str]:
        cert = cls.get_cert(domain)
        if not cert:
            return

        for pairs in cert.get('subject', []):
            for key, value in pairs:
                if key == 'organizationName':
                    return value if not cls.in_blacklist(value) else None


if "__main__" == __name__:
    from argparse import ArgumentParser
    import json

    parser = ArgumentParser()
    parser.add_argument('input')
    args = parser.parse_args()

    with Timer('SSL'):
        print(json.dumps(
            SSLCertificateRetrieval.get_organization(args.input),
            indent=4,
        ))
