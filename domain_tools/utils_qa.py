"""
© 2018 Alternative Data Group. All Rights Reserved.

Script for QA-ing whois info retrieval.

Usage from CLI:
    python -m domain_tools.utils_qa <path_to_input_file>
"""

from packages.utils import qa
from domain_tools.clean import get_domain
from domain_tools.utils import get


def process(url: str) -> dict:
    domain = get_domain(url)
    return {'result': get(domain)}


if __name__ == '__main__':
    qa(process, fields=['result'])
