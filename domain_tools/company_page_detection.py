"""
© 2018 Alternative Data Group. All Rights Reserved.

Module for checking whether given url belongs to any company.

CLI:
    python -m domain_tools.company_page_detection "amazon.com"
"""
import logging
import re
from os import path
from typing import Optional, List, Tuple
from urllib.parse import urlparse

import requests
from bs4 import BeautifulSoup

from bing.bing_web_core import BingWebCore
from domain_tools import abparser
from packages.utils import pretty, NeverFail, read_list, LOCAL, common_words
from packages.xgboost.prediction import xgb_predict, xgb_load_model
from .clean import get_domain
from .utils import get

# from jellyfish import jaro_distance

logger = logging.getLogger(f'adg.{__name__}')
DEBUG = __name__ == '__main__'

LINK_ATTRS = ['href', 'src']

FOLLOW_AD_LINK = True

NUM_PARALLEL_JOBS = 1

DATA_DIR = path.join(path.dirname(path.abspath(__file__)), 'data')
NOT_COMPANY_WEBSITES = read_list(path.join(DATA_DIR, 'company_page_blacklist.txt'))
NOT_PRODUCT_WEBSITES = read_list(path.join(DATA_DIR, 'product_page_blacklist.txt'))
whitelist = read_list(path.join(DATA_DIR, 'ads_whitelist.txt'))
graylist = read_list(path.join(DATA_DIR, 'ads_graylist.txt'))

finder = abparser.lookup()


class AdDetector(object):
    def __init__(self, soup, domain, final_domain):
        self.soup = soup
        self.links = self._find_all_links()
        self.domain = domain
        self.final_domain = get_domain(final_domain)

    def get_blocking_rule(self, link: str) -> Optional:
        link_domain = get_domain(link)
        if not link_domain:
            link_domain = '#'

        if all([
            self.final_domain not in link_domain,
            link_domain not in self.final_domain,
            bool(urlparse(link).netloc),
            not self._whitelisted(link)
        ]):
            match = finder.match_url(link)  # prev. finder.match_url(get_domain(link))
            if match and self._is_redirected_link_external(link):
                logger.debug(f'Found adblock matching rule for url "{link}": {match}')
                return match

    def _whitelisted(self, link):
        for w in whitelist:
            if w in link.lower():
                return True
        return False

    def any_ads(self) -> Optional:
        """ Returns any ads (i.e. first one) detected, or None if no ads found """
        return next(filter(None, map(self.get_blocking_rule, self.links)), None)

    def count_ads(self) -> int:
        """ Returns ads count detected on webpage """
        return len(list(filter(None, map(self.get_blocking_rule, self.links))))

    def _find_all_links(self):
        links = []
        for v in LINK_ATTRS:
            links.extend(self._find_list_resources(v))
        return links

    def _find_list_resources(self, attribute):
        l = []
        for x in self.soup.findAll():
            try:
                l.append(x[attribute])
            except KeyError:
                pass
        return l

    def is_gray_link(self, link: str) -> bool:
        link = link.lower()
        return any(url in link for url in graylist)

    def _is_redirected_link_external(self, link: str) -> bool:
        if not FOLLOW_AD_LINK or not self.is_gray_link(link):
            return True

        if self.final_domain in link or link in self.final_domain:
            return False

        response = get(link)
        if not response:
            return True

        domain = get_domain(response.url)
        if self.final_domain in domain or domain in self.final_domain:
            return False

        links = set(map(lambda x: x.get('href', '#'), response.get_soup().findAll('a')))
        if links:
            head = links.pop()
            links = {head}

        for link in links:
            try:
                ad_final_domain = get_domain(get(link).url)
                if self.final_domain in ad_final_domain:
                    return False

            except Exception:
                pass

        return True


class ClassAdDetector:
    rules = set(read_list(path.join(DATA_DIR, 'classes_rules.txt')))

    @classmethod
    def is_ad(cls, attr):
        return attr in cls.rules

    @classmethod
    def get_ads(cls, attrs):
        return list(filter(cls.is_ad, attrs))

    @classmethod
    def count_ads(cls, attrs):
        return len(cls.get_ads(attrs))


def get_all_attributes(soup: BeautifulSoup, attributes=['href', 'src']):
    attrs_raw = []
    for attr in attributes:
        attrs_raw.extend([x[attr] for x in soup.find_all(attrs={attr: True})])
    attrs = []
    for attr in attrs_raw:
        if isinstance(attr, list):
            for x in attr:
                attrs.append(x)
        else:
            attrs.append(attr)
    return attrs


@NeverFail(fallback_value=False)
def has_pagination(response: requests.Response) -> bool:
    """
    Args:
        response: response object for some url

    Returns:
        bool - whether response page has pagination or not
    """
    assert response

    # if ('page' or 'pageId') in response.url.params.lower():
    #     return True

    soup = response.get_soup()
    for candidate in ['paginator', 'pagination']:
        if soup.find_all('ul', class_=candidate):
            logger.debug('Found pagination')
            return True

    return False


@NeverFail(fallback_value=False)
def is_main_page(url: str) -> bool:
    """
    Args:
        url: any url (amazon.com)

    Returns:
        bool - whether url is main page of website or not
    """
    relative_path = urlparse(url).path.lower()
    return any([
        not relative_path,
        relative_path == '/',
        relative_path.split('/')[-1].rsplit('.', maxsplit=1)[0] in
        ('default', 'index', 'main',
         'homepage', 'home', 'contact', 'grouphome')
    ])


@NeverFail(fallback_value=False)
def has_locale_path(url: str) -> bool:
    """
    Args:
        url: any url (amazon.com)

    Returns:
        bool - whether url has "locale" part
    """
    LOCALE_PARTS = [
        'us', 'en_us', 'en',
        'en-us', 'uk', 'usa',
        'global', 'locations',
    ]
    relative_path = urlparse(url).path.lower()
    return any(part in LOCALE_PARTS for part in relative_path.split('/'))


def count_depth(url: str) -> int:
    """
    Args:
        url: any url (amazon.com)

    Returns:
        int - the number of "parts" after TLD:
            https://site.com/part1/part2?q=something
    """
    if not re.match('^https?://', url):
        url = 'https://' + url

    relative_path = urlparse(url).path
    relative_path = re.sub(r'(^/|/$)', '', relative_path)  # remove first & last slashes

    if not relative_path:
        return 0

    return relative_path.count('/') + 1


def count_domain_occurrences(bing_responses: list, url: str):
    domain = get_domain(url)
    return sum(
        domain == bing_domain
        for bing_domain in map(lambda resp: get_domain(resp['url']), bing_responses)
    )


# sti = SoftTfidf(corpus=json.load(open(path.join(DATA_DIR, 'soft_tf_idf.corpus.2019.04.25.json'), 'r'))[:10000])
# SOFT_TF_IDF = pickle.load(open(path.join(DATA_DIR, 'soft_tf_idf.2019.04.26.pickle'), 'rb'))


def get_internal_scores(bing_response: dict, position: int, response: requests.Response) -> dict:
    """
    Given bing results and position, calculate features which represent whether the website belongs to a company.

    Args:
        bing_response: parsed response from bing
        position: number of bing row
        response: requests.Response for the website in bing row

    Returns:
        n_link_ads - number of links blocked as ads
        n_class_ads - number of classes blocked as ads
        rank_bing - position of website in bing results
        bing_occurrence_count - how often this website occurs in bing results
        has_deep_links - whether this bing result has "deep links" - links to most frequently used urls
        url_slashes - how "deep" is the url - i.e. how many "folders" is in url path
        is_main_page - whether it is a main page of website
        has_locale_path - whether there is some locale information in the url
        title_similarity - how much website title is similar to cleaned string
        official_site - whether title says that website is official
        occurs_twice_in_top - whether website occurs at least twice in top 3 bing results
        website_title_match - which part of query appears in website title
    """
    query = bing_response['queryContext']['originalQuery']

    bing_results = bing_response.get('webPages', {}).get('value', [])
    bing_row = bing_results[position]

    url = bing_row['url']

    return {
        'n_link_ads': AdDetector(response.get_soup(), response.request.url, response.url).count_ads() if response
        else None,
        'n_class_ads': ClassAdDetector.count_ads(get_all_attributes(response.get_soup(), ['class'])) if response
        else None,
        'rank_bing': position,
        'bing_occurrence_count': count_domain_occurrences(bing_results, url),
        'has_deep_links': int(bool(bing_results[position].get('deepLinks', []))),
        'url_slashes': count_depth(url),
        'is_main_page': int(is_main_page(url)),
        'has_locale_path': int(has_locale_path(url)),
        # 'title_similarity_tf_idf': SOFT_TF_IDF.similarity(query, bing_row['name'] + ' ' + bing_row['snippet'],
        # threshold=0.9),
        '__bing_title': bing_row['name'],
        # 'title_similarity_jaro': jaro_distance(query.lower(), bing_row["name"].lower()),
        'official_site': int('official site' in bing_row['name'].lower()),
        'occurs_twice_in_top': int(count_domain_occurrences(bing_results[:3], url) >= 2),
        'website_title_match': max(
            [len(word) / len(query) for word in common_words(query, bing_row['name'])] or [0]),
    }


def in_blacklist(url: str, blacklist: List[str] = tuple(NOT_COMPANY_WEBSITES)) -> bool:
    """
    Checks whether url is in "not company websites" list
    """
    domain = get_domain(url)
    return any(bad_site in domain for bad_site in blacklist)


XGB_HEADER, XGB_MODEL = xgb_load_model(path.join(DATA_DIR, 'xgb_boost.company_website_scores.2019.07.03.19.15.pickle'))


def get_website(value: str, query_string: str = '{}',
                blacklist: Tuple[str] = tuple(NOT_COMPANY_WEBSITES)) -> Optional[dict]:
    """
    Args:
        value: any input string (company name preferrable)
        query_string: which query string to use in bing; default is just value, but you can use smth like
            '{} official website' or so
        blacklist: list of websites that should never be returned

    Returns:
        dict of website information (or None)
        {
            'url' - related (official) website
            'title' - website title
            'name' - value of about->name field
        }
    """

    query = query_string.format(value)
    bing_response = BingWebCore().query(query)
    if not bing_response:
        if DEBUG:
            logger.debug(f'Bing produced no response')
        return

    results = bing_response.get('webPages', {}).get('value', [])
    if not results:
        if DEBUG:
            logger.debug(f'Bing produced no results')
        return

    if DEBUG:
        logger.debug(f'Bing first 5 results: \n' + pretty(results[:5]))

    candidates = results[:LOCAL.settings['BING_THRESHOLD']]
    logger.debug(f'Found company website candidates: {[cand["url"] for cand in candidates]}')

    for i, candidate in enumerate(candidates):
        url = candidate['url']
        logger.debug(f'Checking "{url}"')

        if in_blacklist(url, blacklist):
            logger.debug(f'Url "{url}" is in blacklist -> not a company')
            continue

        response = get(url)

        internal_scores = {key: value for key, value in get_internal_scores(bing_response, i, response).items()}
        logger.debug(f'Internal scores: {internal_scores}')

        if (internal_scores['n_link_ads'] or 0) > 0:
            logger.debug(f'Candidate "{url}" has {internal_scores["n_link_ads"]} link ads -> not a company website')
            continue

        if response and xgb_predict(internal_scores, XGB_HEADER, XGB_MODEL) >= 0.5 \
                or internal_scores['occurs_twice_in_top']:
            logger.debug(f'Candidate "{url}" accepted as related website')
            return {
                'url': candidate['url'],
                'title': candidate['name'],
                'name': candidate.get('about', [{}])[0].get('name'),
            }

        else:
            logger.debug(f'Candidate "{url}" rejected as a company website')

    logger.debug(f'None of candidates "{[cnd["url"] for cnd in candidates]}" is a company website')


if "__main__" == __name__:
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('input')
    args = parser.parse_args()
    print(get_website(args.input))
