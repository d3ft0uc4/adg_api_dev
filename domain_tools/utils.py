"""
© 2018 Alternative Data Group. All Rights Reserved.

Basic scraping tools.

Usage from CLI:
    python -m domain_tools.utils domains.com --ok_codes 200 304 305 401 403 404
    > Optionally send a list of acceptable HTTP response codes with the `--ok_codes` argument.
"""

import logging
import random
import re
from copy import copy
from typing import Optional, Tuple, List

import requests
import urllib3
from bs4 import BeautifulSoup
from bs4.element import Comment
from user_agent import generate_user_agent

from packages.caching import DbCache
from packages.utils import with_timer
from .clean import get_domain
from .urlextract import ADGURLExtract

logging.getLogger('chardet.charsetprober').setLevel(logging.INFO)
logging.getLogger('chardet.universaldetector').setLevel(logging.INFO)
urllib3.disable_warnings()  # See https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings

logger = logging.getLogger(f'adg.{__name__}')
DEBUG = __name__ == '__main__'

# TODO: Move these constants to module or package settings.
TIMEOUT = 6
HEADERS = {
    'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 '
                  'Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,'
              'application/xml;q=0.9,'
              'image/webp,image/apng,*/*;q=0.8',
    'Accept-Language': 'en;q=0.8, *;q=0.5',
    'Accept-Encoding': 'gzip, deflate'
}
OK_HTTP_CODES = (200, 201, 202, 203, 204, 205, 206,
                 304, 305,
                 400, 401, 403, 404, 406, 407, 409, 411, 412)
# ^ See https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html

# ^ Some domains may block crawlers based on their user-agents.


# try:
#     HTTP_PROXIES = requests.get(
#         'https://www.proxy-list.download/api/v1/get?type=http&anon=anonymous', timeout=2).text.split('\r\n')
# except Exception:
#     logger.warning('Could not load HTTP proxies')
#     HTTP_PROXIES = []
#
# try:
#     HTTPS_PROXIES = requests.get(
#         'https://www.proxy-list.download/api/v1/get?type=https', timeout=2).text.split('\r\n')
# except Exception:
#     logger.warning('Could not load HTTPS proxies')
#     HTTPS_PROXIES = []
HTTP_PROXIES = []
HTTPS_PROXIES = []


def is_visible(tag):
    if tag.name in ['style', 'script', 'head', 'meta', '[document]']:
        return False

    if isinstance(tag.string, Comment):
        if DEBUG:
            logger.debug(f'Tag is comment: {tag}')
        return False

    return True


def extract_visible_text(soup: BeautifulSoup) -> List[str]:
    # this does not work as intended
    # visible_text = [tag.text.strip() for tag in soup.findAll(is_visible, text=True)]
    # visible_text = [text for text in visible_text if text]
    # return visible_text

    dirty_soup = copy(soup)
    [tag.extract() for tag in dirty_soup.find_all(lambda tag: not is_visible(tag))]
    return [chunk for chunk in map(str.strip, re.split('\n', dirty_soup.text)) if chunk]


USER_AGENT_GOOGLE = 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'


def get_soup(self):
    if not hasattr(self, 'soup'):
        self.soup = BeautifulSoup(self.text, 'html.parser')

    return self.soup


def get_visible_text(self):
    if not hasattr(self, 'visible_text'):
        self.visible_text = extract_visible_text(self.get_soup())

    return self.visible_text


requests.Response.get_soup = get_soup
requests.Response.get_visible_text = get_visible_text


@with_timer
@DbCache(cache_if=lambda result: result and result.ok and 'unusual activity' not in result.text)
def get(url: str,
        ok_http_codes: Tuple[str] = OK_HTTP_CODES,
        use_proxy: bool = False,
        **kwargs) -> Optional[requests.Response]:
    """
    Advanced GET request, with caching, timeout and retries:
    - returns response for `url` address or None if request failed;
    - returns None if response code not in `ok_http_codes`;
    - returns None if response content type is not "text/html";
    - parses html source code to response.soup;
    - `kwargs` are passed to internal `requests.get`.
    """
    # logger.debug(f'User-agent: {HEADERS["User-agent"]}')

    # Selects which urls to try.
    if re.match(r'^https?://', url):
        urls = [url]
    else:
        urls = [f'https://{url}', f'http://{url}']  #, f'https://www.{url}', f'http://www.{url}']
        # ^-- redirect to `www.{url}` should be done automatically by website

    response = None
    for url in urls:
        for attempt in range(3):
            try:
                attrs = {
                    'timeout': TIMEOUT,
                    'headers': {
                        **HEADERS,
                        'User-agent': USER_AGENT_GOOGLE if attempt == 0 else generate_user_agent(),
                    },
                    'allow_redirects': True,
                    'verify': False,
                }
                if use_proxy:
                    attrs['proxies'] = {
                        'http': random.choice(HTTP_PROXIES),
                        'https': random.choice(HTTPS_PROXIES),
                    }

                    logger.debug(f'Using proxy {attrs["proxies"]}')

                attrs.update(kwargs)

                response = requests.get(url, **attrs)

                logger.debug(f'GET "{url}": {response.status_code}')

            except requests.exceptions.ProxyError as exc:
                logger.debug(f'Proxy error: {exc} {attrs["proxies"]}')
                continue

            except requests.exceptions.RequestException as exc:
                logger.debug(f'GET "{url}" failed: {exc}')

            break

        if not response:
            continue

        if response.status_code not in ok_http_codes:
            logger.debug(f'Status code not in acceptable options: {ok_http_codes}')
            continue

        break

    if not response:
        logger.debug(f'Could not retrieve {urls}')
        return

    content_type = response.headers.get('Content-Type', '')
    if 'text/html' not in content_type.lower():
        logger.debug(f'Bad content type "{content_type}", discarding')
        return

    # very very dumb redirect following - just to avoid headless browser
    for script_tag in response.get_soup().find_all('script'):
        script = script_tag.text.strip()

        # (c) zero regular expressions
        # sorry everyone, this is so stupid...
        redirect_stmts = [
            re.compile(r'^(?=window\.|document\.)?location\.href ?= ?["\'](.+?)["\']'),
            re.compile(r'^(?=window\.|document\.)?location\.replace\(["\'](.+?)["\']\)'),
            re.compile(r'^$\((?=location|document)\)\.(?=attr|prop)\(["\']href["\'], ["\'](.+?)["\']\)'),
        ]
        for stmt in redirect_stmts:
            match = stmt.match(script)
            if match:
                location = match[1]

                # convert remote path to absolute one
                scheme = response.url.split('//', maxsplit=1)[0]
                if location.startswith('//'):
                    location = f'{scheme}:{location}'
                elif location.startswith('/'):
                    location = f'{scheme}://{get_domain(response.url)}{location}'

                logger.debug(f'Found JS redirect: {location}')
                return get(location)

    return response


def detect_url(url: str) -> Optional[str]:
    """ Detects whether `url` actually has a URL and if so returns the first one found (`None` otherwise). """
    extractor = ADGURLExtract()
    urls = extractor.find_urls(url.strip())
    return urls[0] if urls else None


def is_domain_input(inp: str) -> bool:
    return (' ' not in inp.strip(' \n\t')) and bool(detect_url(inp))


if __name__ == '__main__':
    from argparse import ArgumentParser

    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger('urllib3').setLevel(logging.INFO)

    parser = ArgumentParser()
    parser.add_argument('input', type=str)
    parser.add_argument('--ok_codes', nargs='*', type=str, default=OK_HTTP_CODES,
                        help='HTTP response status codes considered OK (meaning the domain is live)')
    args = parser.parse_args()

    logging.debug(f'args.ok_codes: {args.ok_codes}')

    in_str = args.input.replace('\r', '')  # Windows...

    logger.debug(f'in_str: {in_str}')
    # url = detect_url(in_str)
    print(is_domain_input(in_str))
    # return
    # print(f'URL detected: {url}')
    # print(f'Response: {get(url)}')
