"""
© 2018 Alternative Data Group. All Rights Reserved.

Module for getting title from url.

Usage from CLI:
    python -m domain_tools.title "google.com"
"""

from typing import Optional
from .utils import get
from packages.utils import Timer, pretty
import logging

logger = logging.getLogger(f'adg.{__name__}')


def get_title(url: str) -> Optional[str]:
    response = get(url)
    if not response:
        return

    title = getattr(response.get_soup().title, 'text', None)

    return title


if "__main__" == __name__:
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('input')
    args = parser.parse_args()

    with Timer(f'Title {args.input}'):
        logger.debug(pretty(get_title(args.input)))
