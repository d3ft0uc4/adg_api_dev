"""
© 2018 Alternative Data Group. All Rights Reserved.

See `get_copyright`.

Usage for testing from cli:
    python -m domain_tools.copyright_extraction google.com
"""

###################################################
# DISCLAIMER                                      #
# This file is created with respect to real data. #
# Any small change may break everything,          #
# so please be careful if making changes here.    #
###################################################

import logging
import re
import unicodedata
from typing import Optional, List

from packages.utils import Timer, remove_dupes
from mapper.settings import COMPANY_TERMINATORS
from domain_tools.utils import get

log = logging.getLogger(f'adg.{__name__}')
DEBUG = __name__ == '__main__'


RE_COPYRIGHT = re.compile(r'(?:copyrights?\s|\(c\)|©)', flags=re.IGNORECASE)
ALL_RIGHTS_RESERVED = r'(all rights reserved|todos los derechos reservados)'
BAD_WORDS = r'(privacy policy|and its affiliates|or its affiliates)'
RE_TERMINATORS = '(' + '|'.join(re.escape(term) for term in COMPANY_TERMINATORS) + ')'

# how much chunks we check after chunk with copyright symbol
CHUNKS_LOOKUP = 3

MIN_LENGTH = 3  # length limits of possible results
MAX_LENGTH = 50
MIN_LETTERS = 3  # minimal number of alphabet chars in company name
BAD_CHARS = re.compile(r'[\w\d .,&"\';]')
BAD_CHARS_MAX_LENGTH = 3  # max number of bad chars in company name
MAX_RESULTS = 2  # max number of results


def clear_copyright(copyright: str) -> str:
    """ Clears copyright text, leaving only company name """
    result = unicodedata.normalize('NFKD', copyright)

    # strip garbage on both sides
    result = re.sub(r'(^\W*|\W*$)', '', result)

    # remove years
    result = re.sub(
        r'(?:(?:19|20)\d{2})?'  # start year
        r'(?:\s*[-—]\s*(?:19|20)\d{2}\s*)?',  # end year
        '',
        result,
        flags=re.IGNORECASE,
    )

    # strip garbage on both sides
    result = re.sub(r'(^\W*|\W*$)', '', result)

    # if "all rights reserved" is in the beginning, just remove it
    result = re.sub(rf'^{ALL_RIGHTS_RESERVED}', '', result, flags=re.IGNORECASE)
    # if "all rights reserved" is in the middle, remove it and everything after it as well
    result = re.sub(rf'{ALL_RIGHTS_RESERVED}.*', '', result, flags=re.IGNORECASE | re.DOTALL)

    # if bad words found, remove it
    result = re.sub(BAD_WORDS, '', result, flags=re.IGNORECASE | re.DOTALL)

    # if company terminator found, remove everything after it
    result = re.sub(rf'\b{RE_TERMINATORS}\b.*', r'\1', result, flags=re.IGNORECASE | re.DOTALL)

    # remove "by"
    result = re.sub(r'\bby\b', '', result, flags=re.IGNORECASE)

    # strip garbage on both sides
    result = re.sub(r'(^\W*|\W*$)', '', result)

    # result = result.split('.')[0]

    return result


def is_valid(value: str) -> bool:
    """ Check whether copyright value is valid """

    if len(value) > MAX_LENGTH:
        log.debug(f'Longer than {MAX_LENGTH}, discarding: "{value}"')
        return False

    if len(value) < MIN_LENGTH:
        log.debug(f'Shorter than {MIN_LENGTH}, discarding: "{value}"')
        return False

    if not len([c for c in value if c.isalpha()]) >= MIN_LETTERS:
        log.debug(f'Num of letters fewer than {MIN_LETTERS}, discarding: "{value}"')
        return False

    bad_chars = BAD_CHARS.sub('', value)
    if len(bad_chars) > BAD_CHARS_MAX_LENGTH:
        log.debug(f'Num of bad chars ("{bad_chars}") bigger than {BAD_CHARS_MAX_LENGTH}, discarding: "{value}"')
        return False

    return True


def get_copyright(domain: str) -> List[str]:
    """ Retrieves copyright information from web page. """
    try:
        response = get(domain)
        if not response or not response.get_visible_text():
            if DEBUG:
                log.debug(f'Bad response')

            return []

        # find all chunks of text
        chunks = response.get_visible_text()
        if DEBUG:
            log.debug(f'Visible chunks: {len(chunks)}')

        # chunks' indexes where copyright symbol occurs
        candidates_idxs = [idx for idx, c in enumerate(chunks) if RE_COPYRIGHT.search(c)]
        if DEBUG:
            log.debug(f'Found {len(candidates_idxs)} candidates')
        max_idx = len(chunks) - 1

        # check each chunk of with copyright sign for match;
        # if chunk doesn't match, try combining the chunk with following chunk(s) and re-checking for match
        # this way we can handle cases like "(c) <span>2018</span> Company name"
        matches = []
        for idx in candidates_idxs:
            for lookup in range(CHUNKS_LOOKUP):
                end_idx = idx + lookup
                if end_idx > max_idx:
                    break

                superchunk = ' '.join(chunks[idx:end_idx+1])
                candidate = RE_COPYRIGHT.split(superchunk)[-1]
                # candidate = ' '.join(RE_COPYRIGHT.split(superchunk))
                if DEBUG:
                    log.debug(f'Non-cleaned candidate: {candidate}')

                candidate = clear_copyright(candidate)
                if candidate and is_valid(candidate):

                    matches.append(candidate)
                    break

        # eliminate dupes
        matches = remove_dupes(matches)

        n_matches = len(matches)
        if n_matches > MAX_RESULTS:
            log.debug(f'Num of results ({n_matches}) if greater than {MAX_RESULTS}, discarding: {matches}')
            return []

        return matches

    except Exception:
        log.exception(f'Copyright retrieval failed.')


if "__main__" == __name__:
    from argparse import ArgumentParser
    import json

    parser = ArgumentParser()
    parser.add_argument('input')
    args = parser.parse_args()

    with Timer(f'© extraction from {args.input}'):
        print(json.dumps(
            get_copyright(args.input),
            indent=4,
        ))
