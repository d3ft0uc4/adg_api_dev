# Domain tools
This module provides tools to analyze domain names from URLs and some basic web scrapping.

## Dependencies
See [requirements.txt](requirements.txt). Install with `pip`

## Usage
The main submodule is [utils](utils.py). Its `get` function implements an advanced GET request. `is_alive` checks whether domains actually exist and have content.

## Testing
> See tests/ dir.

```sh
python -m pytest -xv tests
```

### QA/Bulk tools
[utils_qa.py](utils_qa.py) and [whois_qa.py](whois_qa.py) use the `qa` method from the `packages.utils` module to bulk process a list of domain names from a text file and output the results to an CSV file. Keep an aye on stderr as well.
