"""
© 2018 Alternative Data Group. All Rights Reserved.

Module with functions for testing related domains retrieval.

Use from repo root.
"""

from domain_tools.related_domains import get_related_domains


def test_no_answer():
    domains = [
        'abc.com',
        'marshallsonline.com',
        'some-weird-domain-i-just-invented.com',  # not alive
    ]
    for domain in domains:
        assert get_related_domains(domain, tlds=['com', 'net']) == []


def test_positive():
    answers = {
        'winmarkcorporation.com': ['winmarkcorporation.net'],
        'pokerstars.pt': ['pokerstars.net'],  # 'pokerstars.com' is not alive
        'starbucks.com.br': ['starbucks.com'],  #  'starbucks.net' is not alive
    }

    for domain, answer in answers.items():
        assert get_related_domains(domain, tlds=['com', 'net']) == answer
