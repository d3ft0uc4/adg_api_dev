"""
TODO: Doc
"""

from domain_tools.utils import is_alive


def test_block_bypass():
    assert is_alive('bananarepublic.ru')


def test_unavailable():
    domains = [
        'zingpopculture.com.au',  # google captcha
        'non-existend-domain.fr',  # does not exist
        '1800baskets.com',  # connecting forever - timeout
        'kfc.co',  # connecting forever - timeout
        'baskinrobbins.com',  # access denied
        'hollywoodcasinotunica.com',  # unavailable
        'az-europe.eu',  # timeout
    ]
    for domain in domains:
        assert not is_alive(domain)


def test_redirect():
    assert is_alive('ilg.com')  # redirect to www.marriottvacationsworldwide.com
    assert is_alive('sheratonhokkaidokiroro.co.jp')  # redirect to www.marriott.co.jp


def test_available():
    domains = [
        'adp.com.br',
        'fulltiltpoker.ru',
        'groupon.com.mx',
        'kfc.com.tr',
        'massmart.co.za',  # very big response time
    ]
    for domain in domains:
        assert is_alive(domain)
