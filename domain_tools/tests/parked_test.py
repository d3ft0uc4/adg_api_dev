"""
© 2018 Alternative Data Group. All Rights Reserved.

Module with function for testing whether domains are parked.

Use from repo root.
"""

from domain_tools.parked import is_parked


def test_parked():
    # list from https://sedo.com/search/?language=us
    domains = [
        'appleticket.com',
    ]

    for domain in domains:
        assert is_parked(domain)


def test_not_parked():
    domains = [
        'google.com',
        'yahoo.com',
        'apple.com',
        'aserrin.com',  # no text but may be js-only website
        'varrick.com',
        'beeghly.com',
        'youcryptomarket.com',  # this used to be parked but now is not
    ]

    for domain in domains:
        assert not is_parked(domain)

def test_complicated():
    assert not is_parked('la-z-boy.gr')
    assert not is_parked('sheratonskyline.com')
