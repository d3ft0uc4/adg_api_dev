"""
© 2018 Alternative Data Group. All Rights Reserved.

Module with function for testing domain cleaning.

Use from repo root.
"""

from domain_tools.clean import get_domain


DOMAIN = 'yahoo.com'


def test():
    urls = [
        # 'http' handling
        f'https://{DOMAIN}',
        f'http://{DOMAIN}',
        f'ftp://{DOMAIN}',
        f'//{DOMAIN}',
        f'{DOMAIN}',
        # last dot handling
        f'{DOMAIN}.',
        # slashes handling
        f'{DOMAIN}./',
        f'http://{DOMAIN}./',
        f'{DOMAIN}/some/other/path',
        # params handling
        f'{DOMAIN}/path?some=param',
        f'{DOMAIN}?some=param',
        f'https://{DOMAIN}?some=param',
        f'{DOMAIN}:443/some/other/path',
    ]

    for url in urls:
        assert get_domain(url) == DOMAIN
