from domain_tools.copyright_extraction import get_copyright


def test_chilisindia():
    assert 'TexMex Cuisine India Pvt Ltd' in get_copyright('https://www.chilisindia.com/')

def test_winmark():
    assert 'Winmark Corporation' in get_copyright('https://www.winmarkcorporation.com/')

def test_akisushi():
    assert 'A-Aki Sushi & Steakhouse' in get_copyright('a-akisushi.com')

def test_acommodity():
    assert 'American Commodity' in get_copyright('a-commodity.com')

def test_hotelsgroup():
    assert 'hotelsgroup.in' in get_copyright('crowneplaza.hotelsgroup.in')

def test_kfc():
    assert 'KFC, Inc' in get_copyright('kfc.ae')

def test_aconweb():
    # well, they wrote shit here
    assert 'action unit for communicative NPO' in get_copyright('a-conweb.net')

def test_agp():
    # lookup 3 chunks
    assert 'Ag Processing Inc a cooperative' in get_copyright('agp.com')

def test_maxivare():
    # value before copyright string
    assert 'Maxicare' in get_copyright('www.maxicare.com.ph')

def test_complicated():
    answers = {
        # comparison of answers never works
        # 'a-cordes.com': 'アッコルト出版',

        # a lot of (c) garbage symbols present
        'anacrowneplaza-fukuoka.jp': 'ANA CROWNE PLAZA FUKUOKA',

        # these are split by <span> or other tag
        'bananarepublic.com': 'BananaRepublic.com',

        # 'expedia.com.vn': 'Expedia, Inc',
        # 'secure-oldnavy.gap.ca': 'Gap Inc',
    }

    for url, answer in answers.items():
        results = get_copyright(url)
        assert answer in results, f'"{url}" produced {results}, not "{answer}"'


def test_no_copyright():
    urls = [
        'pizzahut.lk'  # ® symbol instead of ©
    ]

    for url in urls:
        results = get_copyright(url)
        assert results == [], f'"{url}" produced {results}, not empty list'
