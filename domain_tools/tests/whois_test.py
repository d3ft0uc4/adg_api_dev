"""
© 2018 Alternative Data Group. All Rights Reserved.

Module with functions for testing whois info retrieval.

Use from repo root.
"""

from domain_tools.whois import WhoisInfoRetrieval


def test_registrant_blacklist():
    domains = [
        'amazingcapture.zenfolio.com',  # registrant: zenfolio.com, Secret Registration Customer ID 1226
        'textbooks.com',  # Registrant Org	Domains By Proxy, LLC
    ]
    for domain in domains:
        assert WhoisInfoRetrieval.get_organization(domain) is None


def test_organization_blacklist():
    domains = [
        'groupon.com.mx',  # Host Master
        'ilg.com',  # Domains By Proxy, LLC
        'online.dunkinindia.com  ',  # DD IP Holder LLC
    ]
    for domain in domains:
        assert WhoisInfoRetrieval.get_organization(domain) is None


def test_correct():
    results = [
        # ('1800baskets.com', '1-800-FLOWERS.COM, INC.'),
        ('adp.com.br', 'ADP Brasil LTDA'),
        ('bananarepublic.com', 'Gap, Inc.'),
        ('kfc-ukraine.com', 'Yum! Restaurants International Russia CIS LLC'),
        ('kfc.co', 'KENTUCKY FRIED CHICKEN CORPORATION DELAWARE'),  # alternative location
        ('kfc.ae', 'KUWAIT FOOD COMPANY'),  # (AMERICANA) truncated
        ('fulltiltpoker.ru', 'Kolyma Corporation A.V.V.'),
    ]

    for domain, org in results:
        assert WhoisInfoRetrieval.get_organization(domain) == org
