from urlextract import URLExtract, CacheFileError
import logging
from mapper.settings import CACHE_DIR
import os
import re
import string
from shutil import copyfile

DATA_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')
tld_list_path = os.path.join(DATA_DIR, 'tlds-alpha-by-domain.txt')


class ADGURLExtract(URLExtract):
    """
    Fixed broken URLExtract
    `dir_path` is hardcoded in `__init__`!
    """
    def __init__(self):
        self._logger = logging.getLogger(__name__)
        # full path for cached file with list of TLDs
        self._tld_list_path = tld_list_path
        # check if cached file is readable
        if not os.access(self._tld_list_path, os.R_OK):
            raise CacheFileError("Cached file is not readable for current "
                                 "user. ({})".format(self._tld_list_path))

        self._tlds = None
        self._tlds_re = None
        self._reload_tlds_from_file()

        host_re_txt = "^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])$"
        self._hostname_re = re.compile(host_re_txt)

        # defining default stop chars left
        self._stop_chars_left = set(string.whitespace)
        self._stop_chars_left |= {'\"', '\'', '<', '>', ';'} | {'|', '@', '='}

        # defining default stop chars left
        self._stop_chars_right = set(string.whitespace)
        self._stop_chars_right |= {'\"', '\'', '<', '>', ';'}

        # preprocessed union _stop_chars is used in _validate_tld_match
        self._stop_chars = self._stop_chars_left | self._stop_chars_right

        # characters that are allowed to be right after TLD
        self._after_tld_chars = set(string.whitespace)
        self._after_tld_chars |= {'/', '\"', '\'', '<', '?', ':', '.', ','}
