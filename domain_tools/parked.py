"""
© 2018 Alternative Data Group. All Rights Reserved.

Module for checking whether domain is parked.

Usage from CLI:
    python -m domain_tools.parked appleticket.com
"""

import logging
import re

from domain_tools.utils import get
from packages.utils import with_timer

logger = logging.getLogger(f'adg.{__name__}')

PATTERNS = [
    r'\bpark(ed|ing)?.domain\b',
    r'\bdomain.park\b',
    r'\bdomain.owner\b',
]
REGEX = re.compile(f'({"|".join(PATTERNS)})', flags=re.IGNORECASE)


@with_timer
def is_parked(domain: str) -> bool:
    """
    Args:
        domain: pure domain name (yahoo.com, google.com etc)

    Returns:
        Whether selected domain is parked or not.
    """
    assert not re.match('https?://', domain)
    assert '/' not in domain
    assert '?' not in domain
    assert domain

    # paths = [
    #     # f'random.{domain}/random',
    #     # f'random.{domain}',
    #     f'{domain}/random',
    # ]

    # for path in paths:
    #     url = f'http://{path}'

    #     response = get(url)
    #     if not response:
    #         return False

    #     # # TODO: doesn't work on marriott ('sheratonskyline.com') - redirects EVERY path to marriott.com
    #     # if response.ok:  # status_code == requests.status_codes.codes.ALL_OK:
    #     #     return True

    response = get(domain)
    if not response or not response.ok:
        logger.debug(f'Could not reach {domain}')
        return False

    return not response.content or bool(REGEX.search(' '.join(response.get_visible_text())))


if '__main__' == __name__:
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('domain')
    args = parser.parse_args()

    print(f'{args.domain}: {is_parked(args.domain)}')
