"""
© 2018 Alternative Data Group. All Rights Reserved.

Utility to clean domains names

Usage from CLI:
    python -m domain_tools.clean http://www.domain.us
"""

from urllib.parse import urlparse
import re


def get_domain(url: str) -> str:
    """
    Given any url, returns just its domain name.

    Args:
        url: str  URL or URI to extract domain from

    Returns extracted domain or False if unable
    TODO: try/catch urlparse calls for invalid URLs provided?
    """
    # # prev version:
    # # netloc may be 'www.skullcandy.com:443' -> remove port
    # return urlparse(url).netloc.split(':')[0]

    url = url.strip()

    # From tools.ietf.org/html/rfc3986.html#section-2 :
    # "A URI is composed from a limited set of characters consisting of digits, letters, and a few graphic symbols."
    # ALPHA = 'a-ZA-Z'
    # DIGIT = '0-9'
    # unreserved = [ALPHA, DIGIT, "-", ".", "_", "~"]  # tools.ietf.org/html/rfc3986.html#section-2.3
    # reserved = {  # tools.ietf.org/html/rfc3986.html#section-2.2
    #     'gen-delims': [":", "/", "?", "#", "[", "]", "@"],
    #     'sub-delims': ["!", "$", "&", "'", "(", ")", "*", "+", ",", ";", "="]
    # }
    # pct_encoded = "%"

    # TODO: Should filter out all chars except unreserved, both reserved elements, and pct_encoded
    # Removes `[` and `]` characters.
    url = url.replace("[", "").replace("]", "")

    # Attempts to extract the domain name ("network location").
    parsed = urlparse(url)
    domain = parsed.hostname

    if not domain:
        # When no URL scheme (protocol) provided, urlparse returns entire string in parsed.scheme or parsed.path
        parsed = urlparse(f'http://{parsed.scheme}') if parsed.scheme else urlparse(f'http://{parsed.path}')
        domain = parsed.hostname

        # FIXME: Not sure this case ever happens.
        if not domain:
            # Attempts to extract domain from incomplete URI ourselves in case `/` are left in the string.
            domain = url.split('/', maxsplit=1)[0]

    # TODO: Consider docs.python.org/3/library/codecs.html#module-encodings.idna and/or tools.ietf.org/html/rfc5891 ?

    # Removes trailing periods, if any.
    domain = domain.strip('.')

    # remove "www.", "www1." etc
    domain = re.sub(r'^www\d?\.', '', domain, flags=re.IGNORECASE)

    domain = domain.lower()

    return domain


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('input')
    args = parser.parse_args()

    print(get_domain(args.input))
