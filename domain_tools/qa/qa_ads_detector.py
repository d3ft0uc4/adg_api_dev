from packages.utils import qa
from domain_tools.company_page_detection import *

if __name__ == '__main__':
    NA = {
        'is_company_website': 'N/A',
        'ads_count': 'N/A',
        'blocked_links': 'N/A',
    }


    def func(domain):
        try:
            result = is_company_page(domain)
            if not result:
                return NA
            return result
        except:
            return NA


    qa(lambda domain: func(domain), fields=['is_company_website', 'ads_count', 'blocked_links'])
