from packages.utils import qa
from domain_tools.related_domains import get_related_domains


if __name__ == '__main__':
    qa(lambda domain: {'result': get_related_domains(domain)}, fields=['result'])
