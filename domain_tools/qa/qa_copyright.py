from packages.utils import qa
from domain_tools.copyright_extraction import get_copyright


if __name__ == '__main__':
    qa(lambda domain: {'result': get_copyright(domain)[0]}, fields=['result'])
