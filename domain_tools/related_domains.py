"""
© 2018 Alternative Data Group. All Rights Reserved.

Module for getting domains, related to input.

CLI:
    python -m domain_tools.related_domains "amazon.com" [--tlds "net" "it"]
"""

from packages.utils import Timer
from typing import Iterable, Optional, List
import logging
import tldextract
import dns.resolver
import os
from domain_tools.utils import get
from joblib import Parallel, delayed
from mapper.settings import PARALLEL
from packages.utils import NeverFail, with_timeout
import multiprocessing

log = logging.getLogger(f'adg.{__name__}')

POPULAR_TLDS_PATH = os.path.join(
    os.path.dirname(os.path.abspath(__file__)),
    'data',
    'popular_tlds')

with open(POPULAR_TLDS_PATH) as f:
    POPULAR_TLDS = list(map(lambda x: x.strip(), f.readlines()))


@NeverFail(fallback_value=set())
def resolve_dns(domain, record_types):
    r = []
    for record_type in record_types:
        try:
            r.extend(dns.resolver.query(domain, record_type, raise_on_no_answer=False))
        except:
            pass
    return set(map(str, r))


@with_timeout(10)  # spawns new process!
def _check_tld(new_tld: str,
               base: str,
               base_tld: str,
               original_ns: Iterable[str],
               check_alive: bool,
               record_types: set) -> Optional[str]:
    """
    Check whether base with new tld is related to original domain

    Args:
        tld: new (candidate) tld, i.e. "net"
        base: original domain base, i.e. "yahoo"
        base_tld: original domain tld, i.e. "com"
        original_ns: name servers for original domain
        check_alive: whether to check candidate domain for being alive
        record_types: types of records to search
    """
    assert new_tld != base_tld

    candidate = f'{base}.{new_tld}'

    log.debug(f'Checking domain "{candidate}" for being related to "{base}.{base_tld}"')
    ns = resolve_dns(candidate, record_types)
    if ns.intersection(original_ns):
        if check_alive and get(candidate) is None:
            log.debug(f'"{candidate}" is not alive, discarding')
            return
        return candidate


def get_related_domains(domain: str,
                        tlds: List[str] = POPULAR_TLDS,
                        check_alive: bool = False,
                        record_types={'NS', 'MX'}) -> List[str]:
    """
    Args:
        domain: pure domain name (yahoo.com, google.com etc)
        tlds: list of TLDs to test
        check_alive: whether to check domain for being "alive" (accessible)
        record_types: type of record to search
    Returns:
        List of related domains - i.e. domains, which have
        at least 1 similar NS, compared to input domain NS.
    """
    extract_result = tldextract.extract(domain)
    base, base_tld = extract_result.domain, extract_result.suffix
    original_ns = resolve_dns(domain, record_types)
    results = Parallel(n_jobs=len(tlds) if PARALLEL else 1, prefer='threads')(
            delayed(_check_tld)(tld, base, base_tld, original_ns, check_alive, record_types)
            for tld in tlds if tld != base_tld
    )
    return [r for r in results if r]


if "__main__" == __name__:
    from argparse import ArgumentParser
    import json

    parser = ArgumentParser()
    parser.add_argument('--tlds', nargs='*', default=['net', 'com'])
    parser.add_argument('input')
    args = parser.parse_args()

    with Timer('Related domains'):
        print(json.dumps(
            get_related_domains(args.input, tlds=args.tlds),
            indent=4,
        ))
