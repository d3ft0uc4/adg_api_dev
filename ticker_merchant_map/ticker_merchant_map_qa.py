from packages.utils import qa
from .ticker_merchant_map import TickerFinder

ticker_finder = TickerFinder()
qa(ticker_finder.query_db, fields=['ticker', 'exchange', 'score', 'short_name'])
