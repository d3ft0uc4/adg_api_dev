import pandas as pd
import sqlalchemy

from mapper.settings import SQL_CONNECTION_STRING

TICKERS_FILE = '/tmp/top 5000.xlsx'
EXCHANGES_MAP = {
    'NASDAQ GS': 'NASDAQ',
    'NASDAQ GM': 'NASDAQ',
    'NASDAQ CM': 'NASDAQ',
    'Ney York': 'NYSE',
    'NYSEAmerican': 'NYSE',
    'London': 'LSE',
    'Tokyo': 'JPX',
}

tickers = pd.read_excel(TICKERS_FILE, sheet_name=0, header=2, usecols='A,B,C')
exchanges = pd.read_excel(TICKERS_FILE, sheet_name=1, usecols='A,G,O')

tickers = tickers.drop(0)
tickers = pd.concat([
    tickers,
    tickers.apply(
        lambda row: pd.Series(
            dict(zip(['ticker', 'exchange_locale'], row['Ticker'].replace(' Equity', '').rsplit(' ')))
        ),
        axis=1,
    ),
], axis=1)

df = pd.merge(tickers, exchanges, how='left', on='Ticker')
df.rename(columns={
    'Short Name': 'short_name',
    'Market Cap': 'market_cap',
    'Prim Exch Nm': 'exchange',
    'Name': 'long_name',
}, inplace=True)
df = df.drop('Ticker', axis=1)

df['exchange'] = df.apply(lambda row: EXCHANGES_MAP.get(row['exchange']), axis=1)

db = sqlalchemy.create_engine(SQL_CONNECTION_STRING)
df.to_sql('ticker_name_mapping', db, if_exists='replace')
print('OK')
