"""
© 2018 Alternative Data Group. All Rights Reserved.

This module helps retrieving ticker for given company names.

Cli:
    python -m ticker_merchant_map.ticker_merchant_map --method=[db|yahoo|bing|wiki] "Amazon.com"
    python -m ticker_merchant_map.ticker_merchant_map --reverse --method=[db|figi|yahoo|tradier] "[nasdaq:]msft"

"""
import json
import logging
import re
from collections import Counter
from operator import itemgetter
from traceback import format_exc
from typing import List, Dict, Iterable, Optional, Tuple

import requests
from jellyfish import levenshtein_distance, jaro_winkler
from joblib import Memory

from bing.bing_web_core import BingWebCore
from mapper.settings import TRADIER_API_KEY, OPENFIGI_API_KEY, CACHE_DIR
from packages.caching import DbCache, TickerCache
from packages.utils import Timer, db_execute, NeverFail, conditional_decorator, LOCAL, pretty, strip_accents
from unofficializing.unofficializing import make_unofficial_name as uo
from wiki_finder.wiki_finder2 import WikiPage

logger = logging.getLogger(f'adg.{__name__}')
memory = Memory(CACHE_DIR, verbose=0)
DEBUG = __name__ == '__main__'

OPENFIGI_API_URL = 'https://api.openfigi.com/v2/mapping'

# list of correct exchanges - only these are allowed
EXCHANGES = [
    'NASDAQ',
    'NYSE',
    'LSE',   # London
    'SIX',   # Swiss
    'HKG',   # Hong Kong
    'TYO',   # Tokyo
    'FWB',   # Frankfurt
    'TSX',   # Toronto
    'BME',   # Madrid
    'ASX',   # Australian Securities Exchange
    'SZSE',  # Shenzhen
    'ENX',   # Euronext
    'SSE',   # Shanghai
    'KRX',   # Korea
]
EXCHANGES_MAP = {  # global exchange conversion table
    'NYQ': 'NYSE',
    'NYS': 'NYSE',
    'NAS': 'NASDAQ',
    'JPX': 'TYO',
    'JP': 'TYO',  # appears in nodes table
    'EURONEXT': 'ENX',
    'HKE': 'HKG',
    'SEHK': 'HKG',
    'HKG': 'SEHK',
    'TSE': 'TSX',
    'SWX': 'SIX',
    'BMAD': 'BME',
    'LN': 'LSE',  # occurs in nodes table
    'LONDON': 'LSE',
    'LON': 'LSE',
    'KOSDAQ': 'KRX',
    'XETR': 'FWB',  # Xetra
    **{exch: exch for exch in EXCHANGES},

    # we are aware of these, but we ignore them:
    **{exch: None for exch in [
        'Nasdaq Stockholm',
        'PINX',
        'NASDAQOTH',
        'US',
        'CLASS A COMMON STOCK',  # wat?
        'ISEQ',  # Ireland
        'IN',  # India
        'B3',  # Brazil
        'OTCMKTS',
        'BSE',  # Bombay
        'NSE',  # India
        'UH',  # Dubai
        'BMV',  # Mexico
        'IDX',  # Indonesia
    ]},
}
assert all(exch in EXCHANGES + [None] for exch in EXCHANGES_MAP.values()), f'Incorrect exchange'


BLOOMBERG_EXCHANGES_MAP = {
    'US': None,   # NYSE / NASDAQ
    'LN': 'LSE',  # London
    'SW': 'SIX',
    'HK': 'HKG',   # Hong Kong
    'JP': 'TYO',   # Tokyo
    'GR': 'FWB',
    'CN': 'TSX',  # Toronto
    # 'BME',   # Madrid
    # 'ASX',   # Australian Securities Exchange
    'CH': 'SZSE',  # Shenzhen
    # 'ENX',   # Euronext
    # 'SSE',   # Shanghai
    # 'HKG',  # Stock Exchange of Hong Kong
    'KS': 'KRX',   # Kosdaq
    'TI': None,    # Istanbul
    'AU': None,    # ASE (wtf?)
    'IN': None,    # Natl India
    'BZ': None,    # B3 Day (wtf?)
    'TT': None,    # Taipei
    'AV': None,    # Vienna
    'PM': None,    # Philippines
    'TB': None,    # Bangkok
    'SJ': None,    # Johannesburg
    'BB': None,    # Brussels
    'CI': None,    # Sant. Comerc (wtf?!)
    'MK': None,    # Bursa Malays
    'MM': None,
    'SS': None,    # Stockholm
    'IJ': None,    # Indonesia
    'UH': None,    # Dubai
}


def remove_class(name: str) -> str:
    # FROM: GOPRO INC-CLASS A
    # TO:   GOPRO INC
    # FROM: GOPRO INC -CL A
    # TO:   GOPRO INC
    # FROM: GOPRO INC (class A)
    # TO:   GOPRO INC
    # TODO: ungroup and QA
    return re.sub(r'\W+CL(ASS)? [ABC]\W*$', '', name, flags=re.IGNORECASE).strip()
    # return re.sub(r' ?\-?\(?CL(ASS)? [ABC]\)?$', '', name, flags=re.IGNORECASE).strip()


class InvalidTickerExchange(Exception):
    pass


def normalize_tkr_exch(ticker: str, exchange: str, mapping: Dict[str, str] = EXCHANGES_MAP) -> \
        Tuple[Optional[str], Optional[str]]:
    """
    Given ticker and exchange from any external source,
    converts exchange to local one and verifies that both ticker and exchange are valid.

    Args:
        mapping - dictionary for converting external exchange to local one (one from EXCHANGES list)

    Returns:
        Tuple (ticker, exchange).
        Exchange may be None, which means that we are aware about extarnal exchange but we intentionally ignore it.
        Unknown exchange or wrong ticker will lead to `InvalidTickerExchange` exception.
    """
    mapping = {key.upper(): (value.upper() if value else value) for key, value in mapping.items()}

    if not ticker or not exchange:
        return None, None

    ticker, exchange = ticker.upper(), exchange.upper()

    # https://www.bloomberg.com/quote/AAP*:MM
    ticker = remove_class(ticker).strip(' *')

    ticker = re.split(r'[./]', ticker)[0]
    if re.search(r'[^\.\-\^\w\d]', ticker, flags=re.IGNORECASE):
        raise InvalidTickerExchange(f'Ticker "{ticker}" is not a valid ticker')

    if exchange not in mapping:
        raise InvalidTickerExchange(f'Normalization: exchange "{exchange}" not found in mapping {mapping}')

    exchange = mapping[exchange]
    if exchange is None:
        return None, None

    return ticker, exchange


@memory.cache
def get_7k_tickers():
    DB_EXCHANGES_MAP = {
        'New York':     'NYSE',
        'Tokyo':        'TYO',
        'Shanghai':     'SSE',
        'NASDAQ GS':    'NASDAQ',
        'Hong Kong':    'HKG',
        'London':       'LSE',
        'Toronto':      'TSX',
        'ASE':          None,
        'EN Paris':     None,
        'Xetra':        'FWB',
        'SIX-SW':       'SIX',
        'Stockholm':    None,
        'BrsaItaliana': None,
        'Soc.Bol SIBE': 'BME',
        'NASDAQ GM':    'NASDAQ',
        'EN Amsterdam': None,
        'EN Brussels':  None,
        'Oslo':         None,
        'Helsinki':     None,
        'Copenhagen':   None,
        'Vienna':       None,
        'NASDAQ CM':    'NASDAQ',
        'Athens':       None,
        'EN Dublin':    None,
        'EN Lisbon':    None,
        'NYSEAmerican': None,
        'London Intl':  'LSE',
        'Frankfurt':    'FWB',
        'Hamburg':      None,
        'FN Stockholm': None,
        'Stuttgart':    None,
        'HI-MTF':       None,
        'Canadian Sec': None,
        'Nagoya':       None,
        'Valletta':     None,
        'OTC-X BEKB':   None,
        'Venture':      None,
        'Reykjavik':    None,
        'Luxembourg':   None,
        'NYSE Arca':    'NYSE',
        'Munich':       None,
        'Nordic GM':    None,
        'BME Outcry':   'BME',
        'Sapporo':      None,
        'LSE EuropeQS': None,
        'Cboe BZX':     None,
        'Investors Ex': None,
    }

    with Timer('Ticker db'):
        rows = db_execute(
            '''
            SELECT
                short_name,
                name,
                ticker,
                exchange
            FROM ticker_top_7k
            -- WHERE exchange IS NOT NULL
            '''
        ).fetchall()
        rows = [dict(row) for row in rows]

        results = []
        for row in rows:
            try:
                ticker, exchange = normalize_tkr_exch(row['ticker'], row['exchange'], mapping=DB_EXCHANGES_MAP)
            except InvalidTickerExchange as exc:
                logger.warning(f'TickerFinder: {exc}')
                ticker, exchange = None, None

            name = remove_class(row['name'])
            name_uo = uo(name)

            short_name = re.sub(r'-\w$', '', row['short_name'], flags=re.IGNORECASE)
            short_name_uo = uo(short_name)

            names = [name]
            names_uo = [name_uo]

            if short_name_uo not in name_uo:
                names.append(short_name)
                names_uo.append(short_name_uo)

            info = {
                'ticker': ticker,
                'exchange': exchange,
                'names': names,
                'names_uo': names_uo,
            }

            results.append(info)

        return results


def is_ticker_cache_enabled(*args, **kwargs) -> bool:
    return LOCAL.settings.get('CACHE_TICKERS', True)


class TickerFinder:
    """ Class for retrieving ticker from on company name """

    tickers = get_7k_tickers()

    DB_STR_DIST_THLD = 0.95

    @classmethod
    @NeverFail(fallback_value={})
    def query_db(cls, company_name: str, include_empty_ticker: bool = False) -> dict:
        """
        Returns ticker/exchange for selected company name using database of known tickers.

        Args:
            company_name: official company name

        Returns:
            {
                'ticker':
                'exchange':
                'score':
                'debug':
            }
        """
        company_name = company_name.lower()
        company_name_uo = uo(company_name)

        candidates = [
            candidate for candidate in cls.tickers
            if any(
                candidate_name_uo[0].lower() == company_name_uo[0].lower()
                for candidate_name_uo in candidate['names_uo']
            )  # same letter
            and (True if include_empty_ticker else (candidate['ticker'] and candidate['exchange']))
        ]

        if not candidates:
            return {}

        for candidate in candidates:
            candidate['score'] = max(
                jaro_winkler(name.lower(), company_name_uo)
                for name in candidate['names_uo']
            )

        winners = [cndt for cndt in candidates if cndt['score'] >= cls.DB_STR_DIST_THLD]
        if not winners:
            return {}

        # prioritize winners by exchange order in EXCHANGES
        winners.sort(key=lambda winner: EXCHANGES.index(winner['exchange'])
                     if winner['exchange'] in EXCHANGES else len(EXCHANGES))

        winner = winners[0]
        try:
            winner['ticker'], winner['exchange'] = normalize_tkr_exch(winner['ticker'], winner['exchange'])
        except InvalidTickerExchange as exc:
            logger.warning(f'TickerFinder.query_db: {exc}')
            winner['ticker'], winner['exchange'] = None, None

        return winner

    YAHOO_FINANCE_URL = 'http://d.yimg.com/aq/autoc?query={}&region=US&lang=en-US'
    YAHOO_STR_DIST_THLD = 0.86
    YAHOO_EXCHANGES_MAP = {  # mapping from weird exchange name to correct one
        **EXCHANGES_MAP,
        'NGM': 'NASDAQ',
        'NMS': 'NASDAQ',
        'GER': 'FWB',
        'FRA': 'FWB',
        'TOR': 'TSX',
        'MCE': 'BME',
        'KSC': 'KRX',
    }
    assert all(exch in EXCHANGES + [None] for exch in YAHOO_EXCHANGES_MAP.values()), f'Incorrect exchange'

    YAHOO_SUFFIX_TO_EXCH = {
        'L': 'LSE',
        'IL': 'LSE',  # International Orderbook - London
        'SW': 'SIX',
        'T': 'TYO',
        'F': 'FWB',
        'TO': 'TSX',
        'MC': 'BME',
        'AX': 'ASX',
        # 'SZSE',  # Shenzhen
        # 'ENX',  # Euronext
        # 'SSE',  # Shanghai
        'HK': 'HKG',
        'KS': 'KRX',
    }

    YAHOO_EXCH_TO_SUFFIX: Dict[str, List[str]] = {}
    for suffix, exch in YAHOO_SUFFIX_TO_EXCH.items():
        YAHOO_EXCH_TO_SUFFIX.setdefault(exch, []).append(suffix)

    @classmethod
    @NeverFail(fallback_value={})
    def query_yahoo(cls, *company_names: Iterable[str], unofficialize: bool = True) -> Dict:
        """
        Returns {'ticker': ..., 'exchange': ..., 'score': ..., 'debug': {...}} for selected company name
        using Yahoo Finance API.

        Args:
            company_names: Official company names
            unofficialize: Whether to unofficialize company names before string distance comparison

        IMPORTANT: Yahoo succeeds ONLY when queried with official name. Any unofficial name will lead to empty result.
        """
        assert company_names
        debug = {}

        try:
            url = cls.YAHOO_FINANCE_URL.format(company_names[0])
            debug['url'] = url
            response = requests.get(url).json()
            results = response['ResultSet']['Result']

            # select only required fields
            results = [
                {key: value for key, value in result.items() if key in ['symbol', 'name', 'exch', 'exchDisp']}
                for result in results
            ]

            # convert ticker and exchange
            for result in results:
                ticker = result['symbol'].split('.')
                if len(ticker) == 2:
                    ticker, suffix = ticker
                else:
                    ticker, suffix = ticker[0], None

                exchange = cls.YAHOO_EXCHANGES_MAP.get(result.get('exch'))
                if not exchange and suffix:
                    exchange = cls.YAHOO_SUFFIX_TO_EXCH.get(suffix)

                result['ticker'], result['exchange'] = ticker, exchange

            # filter results which belong to selected exchanges
            results = [res for res in results if res['exchange']]

            if unofficialize:
                company_names = set([uo(name) for name in company_names])
                company_names = [name for name in company_names if name]

                for result in results:
                    result['name'] = uo(result['name'])

                results = [res for res in results if res['name']]

            debug['query'] = company_names
            debug['results'] = results
            debug['str_dist_thld'] = cls.YAHOO_STR_DIST_THLD

            for res in results:
                res['score'] = max(jaro_winkler(name.lower(), res['name'].lower()) for name in company_names)

            # filter results which are similar to company name
            results = [res for res in results if res['score'] >= cls.YAHOO_STR_DIST_THLD]
            results.sort(key=itemgetter('score'), reverse=True)

            for res in results:
                try:
                    res['ticker'], res['exchange'] = normalize_tkr_exch(res['ticker'], res['exchange'])
                    if res['ticker'] and res['exchange']:
                        return {
                            'ticker': res['ticker'],
                            'exchange': res['exchange'],
                            'name': res.get('name'),
                            'score': res.get('score'),
                            **debug,
                        }
                except InvalidTickerExchange as exc:
                    logger.warning(f'TickerFinder.query_yahoo: {exc}')

        except Exception:
            logger.debug(f'query_yahoo could not retrieve ticker/exchange for {company_names[0]}: {format_exc()}')

        logger.debug(f'Ticker finder "query_yahoo" failed for "{company_names}"')

        return {
            'ticker': None,
            'exchange': None,
            'score': None,
            **debug,
        }

    @classmethod
    def query_bloomberg(cls, company_name: str) -> dict:
        # query = f'https://search.bloomberg.com/lookup.json?callback=jQuery111305512666336423855_1542918014144&sites=bbiz&query={company_name}&frag_size=192&source=5x5kfj1bsit&types=company_public%2CIndex%2CCommodity%2CBond%2CCurrency%2CFund&group_size=10%2C10%2C10%2C10%2C10%2C10&fields=name%2Cslug%2Cticker_symbol%2Curl%2Corganization%2Ctitle&_=1542918014150'
        raise NotImplementedError()

    @classmethod
    def query_wf_output(cls, company_names: List[str]) -> Dict:
        """
        Returns {'ticker': ..., 'exchange': ..., 'score': None, 'debug': {...}} for selected company name
        using `wf_output` table.

        Args:
            company_names: Unofficial company names
        """
        raise NotImplementedError('Exchanges mapping not implemented for this method')

        ticker, exchange = None, None

        results = db_execute(
            '''
            SELECT
                child_ticker,
                child_exchange,
                parent1_ticker,
                parent1_exchange,
                parent2_ticker,
                parent2_exchange
            FROM wf_output
            WHERE
                target_bool = 1 AND (
                    child_name IN %(company_names)s OR
                    input_string IN %(company_names)s
                )
            ''',
            company_names=company_names,
        ).fetchall()

        tkr_exchs = []
        for result in results:
            for tkr_exch in [result[0:2], result[2:4], result[4:6]]:
                if tkr_exch != (None, None):
                    tkr_exchs.append(tkr_exch)

        if tkr_exchs:
            ticker, exchange = Counter(tkr_exchs).most_common(1)[0][0]

        return {
            'ticker': ticker,
            'exchange': exchange,
            'score': None,
        }

    BING_QUERY_STRING = '"{}" +ticker'
    BING_URL_PATTERNS = [
        r'https://(?:[\w-]+\.)?nasdaq\.com/symbol/(?P<tkr>[\w\.-]{1,6})(/.+)?',
        r'https://(?:[\w-]+\.)?nyse\.com/quote/(?P<exch>\w+):(?P<tkr>\w{1,6})',

        r'https://(?:\w+\.)?finance\.yahoo\.com/(?:chart|quote)/(?P<tkr>[\w-]{1,6})(?:[\./].*)?',
        r'https://www\.bloomberg\.com/quote/(?P<tkr>[\w/]{1,6})(?::(?P<exch>\w+))?.*',
        r'https://www\.bloomberg\.com/profiles/companies/(?P<tkr>[\w/]{1,6}):(?:[\w-]+)',
        r'https://money\.cnn\.com/quote/quote\.html\?symb=(?P<tkr>\w{1,6})',
        r'https://www\.marketwatch\.com/investing/(?:stock|fund)/(?P<tkr>[\w\.]{1,6})(?:\?.+)?',
        r'https://www.cnbc.com/quotes/\?symbol=(?P<tkr>[\w\.]{1,6})(?:-\w+)?',
        r'https://www\.thestreet\.com/quote/(?P<tkr>[\w\.]{1,6})\.html',
        r'https://www\.morningstar\.com/(?:stocks|funds|cefs)/(?P<exch>[\w\.]+)/(?P<tkr>\w{1,6})/quote\.html',
        r'https://quotes\.wsj\.com/(?P<tkr>\w{1,6})',
        r'https://www\.msn\.com/en-us/money/stockdetails/[\w-]+.\w+.(?P<tkr>\w{1,6}).(?P<exch>\w+)',
        r'https://www\.tradingview\.com/symbols/(?P<exch>\w+)-(?P<tkr>\w{1,6})/',

        r'https://amigobulls\.com/stocks/(?P<tkr>\w{1,6})(?:/[\w-]+)?',
        r'https://(?:\w+\.)advfn\.com/stock-market/(?P<exch>\w+)/(?:\w+-)?(?P<tkr>[\w\.]{1,6})/.*',
        r'https://seekingalpha\.com/symbol/(?P<tkr>\w{1,6}).*',
        r'https://www\.reuters\.com/finance/stocks/(?:\w+)/(?P<tkr>\w{1,6})(?:\..*)',
        r'http://thestockmarketwatch\.com/stock/\?stock=(?P<tkr>\w{1,6})',

        r'https://markets\.businessinsider\.com/stocks/(?P<tkr>\w{1,6})',
        r'https://www\.marketbeat\.com/stocks/(?P<exch>\w+)/(?P<tkr>\w{1,6})/',
        r'https://www\.zacks\.com/(?:\w+/)?stock/quote/(?P<tkr>[\w\.]{1,6})',
        r'https://www\.barchart\.com/stocks/quotes/(?P<tkr>\w{1,6})(?:\.\w+)?(?:/.*)?',

        r'https://ycharts\.com/companies/(?P<tkr>[\w\.]{1,6})',
        r'https://web\.tmxmoney\.com/quote\.php\?qm_symbol=(?P<tkr>\w{1,6})(?::(?P<exch>\w+))?',
        r'https://www\.fool\.com/quote/(?P<exch>\w+)/(?:[\w-]+)/(?P<tkr>[\w-]{1,6})',
        r'https://www\.barrons\.com/quote/stock/(?:\w+)/(?:\w+)/(?P<tkr>\w{1,6})/.*',

        r'https://stocktwits\.com/symbol/(?P<tkr>\w{1,6})',
        r'https://www\.stashinvest\.com/investments/stocks/.*-(?P<tkr>\w{1,6})',
    ]
    BING_URL_COUNTER_THLD = 3  # how many urls must produce the same result in order to be confident
    BING_EXCHANGES_MAP = {
        **EXCHANGES_MAP,
        'XNYS': 'NYSE',
        'XNAS': 'NASDAQ',
        **{exch: exch for exch in EXCHANGES},
    }
    assert all(exch in EXCHANGES + [None] for exch in BING_EXCHANGES_MAP.values()), f'Incorrect exchange'

    BING_NUM_URLS = 30  # how many bing results we take into account

    @classmethod
    @NeverFail(fallback_value={})
    def query_bing(cls, company_name: str) -> Dict:
        """
        Queries Bing for company's ticker. Tries to retrieve ticker in several special locations in json response.

        Args:
            company_name: Official name of company

        Returns:
            {'ticker': ..., 'exchange': ..., 'score': ..., 'debug': {...}}
        """
        ticker, exchange = None, None
        score = None
        debug = {}

        try:
            query = cls.BING_QUERY_STRING.format(company_name)
            response = BingWebCore().query(query)
            results = response.get('webPages', {}).get('value')
            if not results:
                logger.debug(f'Bing returned empty result for "{query}"')
                return {
                    'ticker': ticker,
                    'exchange': exchange,
                    'score': score,
                    'debug': debug,
                }

            # get all urls
            urls = [res['url'] for res in results[:cls.BING_NUM_URLS]]

            # take only specific website's urls
            debug['matched_urls'] = []
            debug['non_matched_urls'] = []

            matches = []
            for url in urls:
                for pattern in cls.BING_URL_PATTERNS:
                    match = re.fullmatch(pattern, url)
                    if not match:
                        continue

                    debug['matched_urls'].append(url)

                    match = match.groupdict()

                    # normalize ticker
                    match['tkr'] = re.sub(r'[^\w]+', '.', match['tkr'])  # remove non-alphanumeric symbols
                    match['tkr'] = match['tkr'].upper()

                    try:
                        match['ticker'], match['exchange'] = normalize_tkr_exch(
                            match['tkr'], match.get('exch'), mapping=cls.BING_EXCHANGES_MAP)
                    except InvalidTickerExchange as exc:
                        logger.warning(f'TickerFinder.query_bing: {exc}')
                        break

                    if not match['ticker'] or not match['exchange']:
                        break

                    match['url'] = url
                    matches.append(match)
                    break

                else:
                    debug['non_matched_urls'].append(url)
            debug['matches'] = matches

            # now count each ticker occurrences
            counter = Counter(m['ticker'] for m in matches)
            debug['tickers'] = counter
            # select most often ticker
            if counter:
                logger.debug(counter)
                winner, n_occurrences = counter.most_common(1)[0]
                if n_occurrences >= cls.BING_URL_COUNTER_THLD:
                    ticker = winner
                    score = n_occurrences

                # HACK!
                # i'm sorry
                # i hate tylenoll
                if winner.lower() == 'jnj':
                    ticker = winner
                    score = n_occurrences
                    exchange = 'NYSE'
                    return {
                        'ticker': ticker,
                        'exchange': exchange,
                        'score': score,
                        'debug': debug,
                    }

            # select appropriate exchange for winning ticker
            if ticker:
                exchanges = Counter([m['exchange'] for m in matches if m['ticker'] == ticker])
                debug['exchanges'] = exchanges

                if exchanges:
                    # sort by number of occurrences and by preferred exchanges
                    exchanges = sorted(
                        exchanges.items(),
                        key=lambda item: (
                            item[1],
                            -(EXCHANGES.index(item[0]) if item[0] in EXCHANGES else len(EXCHANGES))
                        ),
                        reverse=True,
                    )
                    exchange = exchanges[0][0]

                else:
                    # query Yahoo Finance for exchange
                    exchange = TickerInfo.get_info_from_yahoo(ticker).get('exchange')

        except Exception:
            logger.debug(f'query_bing could not retrieve ticker/exchange for {company_name}: {format_exc()}')

        if not ticker or not exchange:
            logger.debug(f'Ticker finder "query_bing" failed for "{company_name}"')
            ticker, exchange = None, None

        return {
            'ticker': ticker,
            'exchange': exchange,
            'score': score,
            'debug': debug,
        }

    @classmethod
    @NeverFail(fallback_value={})
    def query_wiki(cls, company_name: str) -> dict:
        """
        Queries Wikipedia for company's ticker.

        Args:
            company_name: Official name of company

        Returns:
            {'ticker': ..., 'exchange': ...,'debug': {...}}
        """
        # try to get company ticker from wikipedia
        pages = WikiPage.from_str(company_name)

        result = {
            'ticker': None,
            'exchange': None,
        }

        if not pages:
            return result

        page = pages[0]

        result['url'] = page.url
        result['debug'] = page.infobox

        for page in page.iter_owners(include_self=True):
            if not isinstance(page, WikiPage):
                break

            try:
                ticker, exchange = normalize_tkr_exch(*page.ticker)
            except InvalidTickerExchange as exc:
                logger.warning(f'TickerFinder.query_wiki: {exc}')
                continue

            if not ticker or not exchange:
                continue

            result['ticker'], result['exchange'] = ticker, exchange
            result['winner'] = page.infobox.get('Title')
            break

        if not result['ticker'] or not result['exchange']:
            logger.debug(f'Ticker finder "query_wiki" failed for "{company_name}"')

        return result

    @staticmethod
    def _norm_levenshtein(in1: str, in2: str) -> float:
        """
        Calculates string similarity using Levenshtein distance.

        Args:
            in1, in2: strings to compare

        Returns: [0-1] - closeness score (0 - completely different, 1 - completely similar)
        """
        return 1 - levenshtein_distance(in1.lower(), in2.lower()) / max(len(in1), len(in2))

    @classmethod
    @NeverFail(fallback_value={})
    @conditional_decorator(DbCache(TickerCache), condition=is_ticker_cache_enabled)
    def get_ticker(cls, *company_names: str, known_tickers: List[Tuple[str, str]] = []) -> Dict:
        """
        Finds ticker for selected company name using any possible ticker retrieval method.

        Args:
            company_names: possible company / company aliases / related entities' names
            known_tickers: tickers we already checked - they will be blocked

        Returns: Dict {
            'ticker':
            'exchange':
            'score':
            'debug':
        }
        """
        result = {'ticker': None, 'exchange': None, 'score': None, 'debug': {'company_names': company_names}}

        # 1) Local database
        for company_name in company_names:
            db_res = cls.query_db(company_name)
            result['debug']['db'] = db_res
            if db_res.get('ticker') and db_res.get('exchange'):
                if any(((db_res['ticker'], db_res['exchange']) == (tkr, exch)) for tkr, exch in known_tickers):
                    logger.debug(f'TickerFinder.get_ticker(): ticker "{db_res["exchange"]}:{db_res["ticker"]}" '
                                 f'is already known -> discarding')

                else:
                    result['ticker'], result['exchange'] = db_res['ticker'], db_res['exchange']
                    result['winner'] = company_name
                    return result

        # 2) Wikipedia
        # N.B. Only first company name is passed to wiki finder
        company_name = company_names[0]
        wiki_res = cls.query_wiki(company_name)
        result['debug']['wiki'] = wiki_res
        if wiki_res.get('ticker') and wiki_res.get('exchange'):
            if any(((wiki_res['ticker'], wiki_res['exchange']) == (tkr, exch)) for tkr, exch in known_tickers):
                logger.debug(f'TickerFinder.get_ticker(): ticker "{wiki_res["exchange"]}:{wiki_res["ticker"]}" '
                             f'is already known -> discarding')

            else:
                result['ticker'], result['exchange'] = wiki_res['ticker'], wiki_res['exchange']
                result['winner'] = wiki_res.get('winner') or company_name
                return result

        # 3) Bing
        for company_name in company_names:
            bing_res = cls.query_bing(company_name)
            result['debug']['bing'] = bing_res
            if bing_res['ticker'] and bing_res['exchange']:
                if any(((bing_res['ticker'], bing_res['exchange']) == (tkr, exch)) for tkr, exch in known_tickers):
                    logger.debug(f'TickerFinder.get_ticker(): ticker "{bing_res["exchange"]}:{bing_res["ticker"]}" '
                                 f'is already known -> discarding')

                else:
                    result['ticker'], result['exchange'] = bing_res['ticker'], bing_res['exchange']
                    result['winner'] = company_name
                    return result

        # 4) Yahoo
        yahoo_res = cls.query_yahoo(*company_names)
        result['debug']['yahoo'] = yahoo_res
        if yahoo_res['ticker'] and yahoo_res['exchange']:
            if any(((yahoo_res['ticker'], yahoo_res['exchange']) == (tkr, exch)) for tkr, exch in known_tickers):
                logger.debug(f'TickerFinder.get_ticker(): ticker "{yahoo_res["exchange"]}:{yahoo_res["ticker"]}" '
                             f'is already known -> discarding')

            else:
                result['ticker'], result['exchange'] = yahoo_res['ticker'], yahoo_res['exchange']
                result['winner'] = company_names[0]
                return result

        return result


class TickerInfo:
    """ Reverse ticker lookup - get info (company name etc) from ticker """

    @classmethod
    @NeverFail(fallback_value={})
    def get_info_from_db(cls, ticker: str, exchange: Optional[str] = None) -> dict:
        """ Retrieves ticker info from ticker_name_mapping tabel."""
        results = [
            info for info in TickerFinder.tickers
            if info['ticker']
            and info['ticker'].lower() == ticker.lower()
        ]

        if exchange:
            results = [
                info for info in results
                if info['exchange']
                and info['exchange'].lower() == exchange.lower()
            ]

        if results:
            if len(results) > 1:
                logger.warning(f'get_info_from_db: multiple results for "{exchange}:{ticker}": {results}')
                results.sort(key=lambda info: EXCHANGES.index(info['exchange']))

            return results[0]

        return {}

    # https://www.openfigi.com/assets/local/exchange-code-mic-mapping.xls
    OPENFIGI_EXCHANGES_MAP = {
        'UQ': 'NASDAQ',  # NASDAQ Global Market
        'UR': 'NASDAQ',  # NASDAQ Capital Market
        'UT': 'NASDAQ',  # NASDAQ InterMarket
        'UW': 'NASDAQ',  # NASDAQ Global Select
        'UX': 'NASDAQ',  # NASDAQ OMX PSX

        'UA': 'NYSE',    # NYSE MKT
        'UP': 'NYSE',    # NYSE Arca

        'LI': 'LSE',     # London Stock Exchange (international)
        'LN': 'LSE',     # London Stock Exchange (domestic)
        'LO': 'LSE',     # London Stock Exchange (EQS)
        'XL': 'LSE',     # London Stock Exchange (OTC and ITR)

        'XW': 'SIX',     # SIX Swiss Exchange
        'VX': 'SIX',     # SIX Swiss Exchange Europe

        'HK': 'HKG',     # Hong Kong Stock Exchange
        'JT': 'TYO',     # Tokyo Stock Exchange
        'GF': 'FWB',     # Frankfurt Stock Exchange
        'CT': 'TSX',     # TSX Toronto Exchange
        'SN': 'BME',     # Madrid Stock Exchange
        'AT': 'ASX',     # Australian Securities Exchange
        'CS': 'SZSE',    # Shenzhen Stock Exchange

        'BB': 'ENX',     # Euronext Brussels
        'FP': 'ENX',     # Euronext Paris
        'LD': 'ENX',     # Euronext London
        'NA': 'ENX',     # Euronext Amsterdam
        'PL': 'ENX',     # Euronext Lisbon

        'CG': 'SSE',     # Shanghai Stock Exchange

        'KP': 'KRX',     # KOSPI Stock Market
        'KQ': 'KRX',     # KOSDAQ
    }

    @classmethod
    @NeverFail(fallback_value={})
    def get_info_from_figi(cls, ticker: str, exchange: Optional[str] = None, names: List[str] = []) -> dict:
        """
        Retrieves ticker info from OpenFigi service.

        Args:
            ticker: Ticker name
            exchange: (optional) Exchange name. If not empty, method selects info with exchange
            most similar to the given one. If empty, method selects info with exchange from
            OPENFIGI_EXCHANGES_PRIORITIES.
            names (optional): possible company names for this ticker/exchange pair

        Returns: {
            'name': company name,
            'exchCode': exchange code,
            ...
            }
        """
        headers = {
            'content-type': 'application/json',
            'X-OPENFIGI-APIKEY': OPENFIGI_API_KEY,
        }
        data = {
            'idType': 'TICKER',
            'idValue': re.sub(r'^0+', '', ticker),  # openfigi requires converting `SEHK:0303` to `SEHK:303`
        }

        response = requests.post(
            OPENFIGI_API_URL,
            data=json.dumps([data]),
            headers=headers,
        )

        if response.text == 'Invalid API key.':
            logger.error(f'Invalid API key: {OPENFIGI_API_KEY}, fallback to free version of OpenFigi')
            response = requests.post(
                OPENFIGI_API_URL,
                data=json.dumps([data]),
                headers=headers,
            )

        if not response.ok:
            logger.error(f'OpenFigi returned bad response for {ticker}/{exchange}: {response}')
            return {}

        response = response.json()[0]
        if DEBUG:
            logger.debug(pretty(response))

        err = response.get('error')
        if err:
            logger.error(f'OpenFigi error for {ticker}/{exchange}: {err}')
            return {}

        data = response['data']
        # convert exchanges
        for item in data:
            item['exchCode'] = cls.OPENFIGI_EXCHANGES_MAP.get(item['exchCode'])

        # remove if no exchange
        data = [item for item in data if item.get('exchCode')]
        if not data:
            logger.warning(f'OpenFigi: No results found for "{exchange}:{ticker}"')

        if exchange:
            # if exchange is set, search results with exactly the same exchange
            data = [item for item in data if item['exchCode'].lower() == exchange.lower()]
            # or any(similar(d['name'].lower(), name.lower()) for name in names)

        result = data[0] if data else {}

        # clean result name
        if result and result['name']:  # OpenFigi may return name == None
            result['name'] = remove_class(result['name'])

        return {**result, 'debug': data}

    @classmethod
    @NeverFail(fallback_value={})
    def get_info_from_yahoo(cls, ticker: str, exchange: Optional[str] = None) -> dict:
        assert ticker
        debug = dict()

        # 'LSE:AV' should be searched as 'AV.L'
        suffixes = TickerFinder.YAHOO_EXCH_TO_SUFFIX.get(exchange)

        for suffix in suffixes or [None]:
            query = ticker
            if suffix:
                query += '.' + suffix

            url = TickerFinder.YAHOO_FINANCE_URL.format(query)
            debug['url'] = url

            response = requests.get(url).json()
            results = response.get('ResultSet', {}).get('Result', [])

            # select only required fields
            results = [
                {key: value for key, value in result.items() if key in ['symbol', 'name', 'exch', 'exchDisp']}
                for result in results
            ]

            # convert ticker and exchange
            for result in results:
                tkr = result['symbol'].split('.')
                if len(tkr) == 2:
                    tkr, suffix = tkr
                else:
                    tkr, suffix = tkr[0], None

                exch = TickerFinder.YAHOO_EXCHANGES_MAP.get(result.get('exch'))
                if not exch and suffix:
                    exch = TickerFinder.YAHOO_SUFFIX_TO_EXCH.get(suffix)

                result['ticker'], result['exchange'] = tkr, exch

            debug['results'] = results

            # filter results which belong to allowed exchanges
            results = [res for res in results if res['exchange']]

            # order results by exchange priority
            results.sort(key=lambda res: list(TickerFinder.YAHOO_EXCHANGES_MAP.keys()).index(res['exchange']))

            # filter by ticker
            results = [res for res in results if res['ticker'].lower() == ticker.lower()]

            if exchange:
                # filter by exchange
                results = [res for res in results if res['exchange'].lower() == exchange.lower()]

            for res in results:
                try:
                    res['ticker'], res['exchange'] = normalize_tkr_exch(res['ticker'], res['exchange'])
                    if res['ticker'] and res['exchange']:
                        return {
                            'ticker': res['ticker'],
                            'exchange': res['exchange'],
                            'name': strip_accents(res.get('name')),
                            'score': res.get('score'),
                            **debug,
                        }
                except InvalidTickerExchange as exc:
                    logger.warning(f'TickerInfo.get_info_from_yahoo: {exc}')

        if exchange not in ['NASDAQ', 'NYSE', 'ENX']:
            # yahoo ticker finder usually fails at finding tickers for these exchanges, so don't worry if it happens
            logger.warning(f'TickerInfo.get_info_from_yahoo failed for "{exchange + ":" if exchange else ""}{ticker}"')

        return {
            'ticker': None,
            'exchange': None,
            'score': None,
            **debug,
        }

    TRADIER_EXCHANGES_MAP = {
        'A': 'NYSE',    # NYSE MKT
        'B': 'NASDAQ',  # NASDAQ OMX BX
                        # C	National Stock Exchange
                        # D	FINRA ADF
                        # E	Market Independent (Generated by Nasdaq SIP)
                        # F	Mutual Funds/Money Markets (NASDAQ)
        'I': 'NASDAQ',  # International Securities Exchange
                        # J	Direct Edge A
                        # K	Direct Edge X
                        # M	Chicago Stock Exchange
        'N': 'NYSE',    # NYSE
        'P': 'NYSE',    # NYSE Arca
        'Q': 'NASDAQ',  # NASDAQ OMX
        'S': 'NASDAQ',  # NASDAQ Small Cap
        'T': 'NASDAQ',  # NASDAQ Int
                        # U	OTCBB
                        # V	OTC other
                        # W	CBOE
        'X': 'NASDAQ',  # NASDAQ OMX PSX
                        # G	GLOBEX
                        # Y	BATS Y-Exchange
                        # Z	BATS
    }

    @classmethod
    @NeverFail(fallback_value={})
    @conditional_decorator(DbCache(), condition=is_ticker_cache_enabled)
    def get_info_from_tradier(cls, ticker: str, exchange: Optional[str] = None) -> dict:

        URL = f'https://sandbox.tradier.com/v1/markets/quotes?symbols={ticker}'
        if exchange:
            if exchange not in cls.TRADIER_EXCHANGES_MAP.values():
                logger.debug(f'Exchange "{exchange}" is not served by tradier')
                return {}

            exchanges = set(value for key, value in cls.TRADIER_EXCHANGES_MAP.items() if value == exchange)
            URL += '&exchanges=' + ','.join(exchanges)

        headers = {
            'Accept': 'application/json',
            'Authorization': f'Bearer {TRADIER_API_KEY}',
        }

        response = requests.get(URL, headers=headers).json()
        quote = response['quotes'].get('quote')
        if not quote:
            logger.debug(f'Tradier returned nothing for "{URL}"')
            return {}

        if quote.get('exch') not in cls.TRADIER_EXCHANGES_MAP:
            logger.debug(f'Exchange "{quote["exch"]}" not in allowed exchanges')
            return {}

        return {
            'ticker': quote['symbol'],
            'exchange': cls.TRADIER_EXCHANGES_MAP[quote['exch']],
            # NYSE:GOLD -> "barrick gold common stock bc"
            'name': re.split(r' *Common Stock', quote['description'], flags=re.IGNORECASE)[0],
        }

    # def get_info_from_seekingalpha(self):
    #     URL = 'https://seekingalpha.com/api/common/ac/search?term=Y&symbols=1&pages=0&people=0&limit=3'


if '__main__' == __name__:
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--aliases', nargs='*', default=[])
    parser.add_argument('--method', default=None)
    parser.add_argument('--reverse', required=False, action='store_true')
    parser.add_argument('inputs', nargs='+')
    args = parser.parse_args()

    if args.reverse:
        result = getattr(TickerInfo, f'get_info_from_{args.method}')(*args.inputs[0].split(':')[::-1])

    else:
        if args.method == 'bing':
            result = []
            for inp in args.inputs:
                result.append(TickerFinder.query_bing(inp))
        elif args.method == 'db':
            result = TickerFinder.query_db(args.inputs[0])
        elif args.method == 'yahoo':
            result = TickerFinder.query_yahoo(*args.inputs)
        elif args.method == 'wiki':
            result = TickerFinder.query_wiki(args.inputs[0])
        else:
            result = TickerFinder.get_ticker(*args.inputs)

    print(pretty(result))
