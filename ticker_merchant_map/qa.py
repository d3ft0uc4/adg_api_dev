"""
© 2018 Alternative Data Group. All Rights Reserved.

QA for ticker/exchange retrieval.

Usage:
    python -m ticker_merchant_map.qa [--limit=100]
"""
from api.app.config import CSV_DIR
from packages.targets import Targets
#from ticker_merchant_map.ticker_merchant_map import TickerFinder
import sys
import pandas as pd
from datetime import datetime
import os
import argparse
from random import shuffle
from wiki_finder.wiki_finder2 import get_owner

from collections import Counter
from operator import itemgetter
from typing import List, Dict

import requests
from jellyfish import levenshtein_distance, jaro_winkler
from traceback import format_exc
import sqlalchemy

from bing.bing_web_core import BingWebCore
from unofficializing.unofficializing import make_unofficial_name
from mapper.settings import SQL_CONNECTION_STRING

import re

from packages.utils import with_timer, db_execute
from wiki_finder.wiki_finder2 import get_owner


class TickerFinder:
    YAHOO_FINANCE_URL = 'http://d.yimg.com/aq/autoc?query={}&region=US&lang=en-US'
    YAHOO_STR_DIST_THLD = 0.86
    YAHOO_EXCHANGES = [
        'NYQ', 'NYS',  # NYSE
        'NAS', 'NGM', 'NMS',  # NASDAQ
        'LSE',  # London Stock Exchange
        'HKG',  # Hong Kong Stock Exchange
        'JPX',  # Tokyo Stock Exchange
    ]
    # mapping from weird exchange name to correct one
    YAHOO_EXCHANGES_MAP = {
        'NYQ': 'NYSE',
        'NYS': 'NYSE',
        'NAS': 'NASDAQ',
        'NGM': 'NASDAQ',
        'NMS': 'NASDAQ',
    }

    @classmethod
    def query_yahoo(cls, company_names: List[str], unofficialize: bool = False) -> Dict:
        """
        Returns {'ticker': ..., 'exchange': ..., 'score': ..., 'debug': {...}} for selected company name using Yahoo Finance API.

        Args:
            company_names: Official company names
            unofficialize: Whether to unofficialize company names before string distance comparison

        IMPORTANT: Yahoo succeeds ONLY when queried with official name. Any unofficial name will lead to empty result.
        """
        assert isinstance(company_names, list)
        assert company_names

        ticker, exchange = None, None
        debug = {}
        score = None

        try:
            response = requests.get(cls.YAHOO_FINANCE_URL.format(company_names[0])).json()
            results = response['ResultSet']['Result']

            # select only required fields
            results = [
                {key: value for key, value in result.items() if key in ['symbol', 'name', 'exch']}
                for result in results
            ]
            # filter results which belong to selected exchanges
            results = [res for res in results if res['exch'] in cls.YAHOO_EXCHANGES]
            # order results by exchange priority
            results.sort(key=lambda res: cls.YAHOO_EXCHANGES.index(res['exch']))

            if unofficialize:
                company_names = set([make_unofficial_name(name) for name in company_names])
                company_names = [name for name in company_names if name]

                for result in results:
                    result['name'] = make_unofficial_name(result['name'])

                results = [res for res in results if res['name']]

            debug['company_names'] = company_names
            debug['str_dist_thld'] = cls.YAHOO_STR_DIST_THLD
            debug['results'] = results

            for res in results:
                res['score'] = max(jaro_winkler(name.lower(), res['name'].lower()) for name in company_names)

            # filter results which are similar to company name
            results = [res for res in results if res['score'] >= cls.YAHOO_STR_DIST_THLD]
            results.sort(key=itemgetter('score'), reverse=True)

            if results:
                best_result = results[0]
                ticker, exchange = best_result['symbol'].split('.')[0], best_result['exch']
                # convert weird exchange to correct one
                exchange = cls.YAHOO_EXCHANGES_MAP.get(exchange, exchange)
                score = best_result['score']

        except Exception:
            print(f'[WARN] Could not retrieve ticker/exchange for {company_names[0]}: {format_exc()}')

        return {
            'ticker': ticker,
            'exchange': exchange,
            'score': score,
            **debug,
        }

    @classmethod
    def query_wf_output(cls, company_names: List[str]) -> Dict:
        """
        Returns {'ticker': ..., 'exchange': ..., 'score': None, 'debug': {...}} for selected company name using `wf_output` table.

        Args:
            company_names: Unofficial company names
        """
        ticker, exchange = None, None

        results = db_execute(
            '''
            SELECT
                child_ticker,
                child_exchange,
                parent1_ticker,
                parent1_exchange,
                parent2_ticker,
                parent2_exchange
            FROM wf_output
            WHERE
                target_bool = 1 AND (
                    child_name IN %(company_names)s OR
                    input_string IN %(company_names)s
                )
            ''',
            company_names=company_names,
        ).fetchall()

        tkr_exchs = []
        for result in results:
            for tkr_exch in [result[0:2], result[2:4], result[4:6]]:
                if tkr_exch != (None, None):
                    tkr_exchs.append(tkr_exch)

        if tkr_exchs:
            ticker, exchange = Counter(tkr_exchs).most_common(1)[0][0]

        return {
            'ticker': ticker,
            'exchange': exchange,
            'score': None,
        }

    BING_QUERY_STRING = '{} ticker'
    BING_URL_PATTERNS = [
        'https://(?:[\w-]+\.)?nasdaq\.com/symbol/(?P<tkr>[\w\.-]{1,6})(/.+)?',
        'https://(?:[\w-]+\.)?nyse\.com/quote/(?P<exch>\w+):(?P<tkr>\w{1,6})',

        'https://(?:\w+\.)?finance\.yahoo\.com/(?:chart|quote)/(?P<tkr>[\w-]{1,6})(?:[\./].*)?',
        'https://www\.bloomberg\.com/quote/(?P<tkr>[\w/]{1,6})(?::(?P<exch>\w+))?.*',
        'https://www\.bloomberg\.com/profiles/companies/(?P<tkr>[\w/]{1,6}):(?:[\w-]+)',
        'https://money\.cnn\.com/quote/quote\.html\?symb=(?P<tkr>\w{1,6})',
        'https://www\.marketwatch\.com/investing/(?:stock|fund)/(?P<tkr>[\w\.]{1,6})(?:\?.+)?',
        'https://www.cnbc.com/quotes/\?symbol=(?P<tkr>[\w\.]{1,6})(?:-\w+)?',
        'https://www\.thestreet\.com/quote/(?P<tkr>[\w\.]{1,6})\.html',
        'https://www\.morningstar\.com/(?:stocks|funds|cefs)/(?P<exch>[\w\.]+)/(?P<tkr>\w{1,6})/quote\.html',
        'https://quotes\.wsj\.com/(?P<tkr>\w{1,6})',
        'https://www\.msn\.com/en-us/money/stockdetails/[\w-]+.\w+.(?P<tkr>\w{1,6}).(?P<exch>\w+)',
        'https://www\.tradingview\.com/symbols/(?P<exch>\w+)-(?P<tkr>\w{1,6})/',

        'https://amigobulls\.com/stocks/(?P<tkr>\w{1,6})(?:/[\w-]+)?',
        'https://(?:\w+\.)advfn\.com/stock-market/(?P<exch>\w+)/(?:\w+-)?(?P<tkr>[\w\.]{1,6})/.*',
        'https://seekingalpha\.com/symbol/(?P<tkr>\w{1,6}).*',
        'https://www\.reuters\.com/finance/stocks/(?:\w+)/(?P<tkr>\w{1,6})(?:\..*)',
        'http://thestockmarketwatch\.com/stock/\?stock=(?P<tkr>\w{1,6})',

        'https://markets\.businessinsider\.com/stocks/(?P<tkr>\w{1,6})',
        'https://www\.marketbeat\.com/stocks/(?P<exch>\w+)/(?P<tkr>\w{1,6})/',
        'https://www\.zacks\.com/(?:\w+/)?stock/quote/(?P<tkr>[\w\.]{1,6})',
        'https://www\.barchart\.com/stocks/quotes/(?P<tkr>\w{1,6})(?:\.\w+)?(?:/.*)?',

        'https://ycharts\.com/companies/(?P<tkr>[\w\.]{1,6})',
        'https://web\.tmxmoney\.com/quote\.php\?qm_symbol=(?P<tkr>\w{1,6})(?::(?P<exch>\w+))?',
        'https://www\.fool\.com/quote/(?P<exch>\w+)/(?:[\w-]+)/(?P<tkr>[\w-]{1,6})',
        'https://www\.barrons\.com/quote/stock/(?:\w+)/(?:\w+)/(?P<tkr>\w{1,6})/.*',

        'https://stocktwits\.com/symbol/(?P<tkr>\w{1,6})',
    ]
    BING_URL_COUNTER_THLD = 3  # how many urls must produce the same result in order to be confident
    BING_EXCHANGE_ALIASES = {
        'XNYS': 'NYSE',
        'NYS': 'NYSE',
        'XNAS': 'NASDAQ',
        'NAS': 'NASDAQ',
    }
    BING_PREFERRED_EXCHANGES = ['NASDAQ', 'NYSE']
    BING_BLACKLIST_EXCHANGES = ['US']

    @classmethod
    def query_bing(cls, company_name: str) -> Dict:
        """
        Queries Bing for company's ticker. Tries to retrieve ticker in several special locations in json response.

        Args:
            company_name: Official name of company

        Returns:
            {'ticker': ..., 'exchange': ..., 'score': ..., 'debug': {...}}
        """
        ticker, exchange = None, None
        score = None
        debug = {}

        try:
            query = cls.BING_QUERY_STRING.format(company_name)
            response = BingWebCore().query(query)
            results = response['webPages']['value']

            # get all urls
            urls = [res['url'] for res in results]

            # take only specific website's urls
            debug['matched_urls'] = []
            debug['non_matched_urls'] = []

            matches = []
            for url in urls:
                for pattern in cls.BING_URL_PATTERNS:
                    match = re.fullmatch(pattern, url)
                    if match:
                        debug['matched_urls'].append(url)

                        match = match.groupdict()

                        # sometimes exchange may be captured as None, we dont need it
                        if 'exch' in match and not match['exch']:
                            del match['exch']

                        # normalize ticker and exchange
                        # remove non-alphanumeric symbols
                        match['tkr'] = re.sub('[^\w]+', '.', match['tkr'])
                        match['tkr'] = match['tkr'].upper()

                        # remove blacklisted exchange
                        if match.get('exch') in cls.BING_BLACKLIST_EXCHANGES:
                            del match['exch']

                        if 'exch' in match:
                            match['exch'] = match['exch'].upper()
                            match['exch'] = cls.BING_EXCHANGE_ALIASES.get(match['exch'], match['exch'])

                        match['url'] = url

                        matches.append(match)
                        break
                else:
                    debug['non_matched_urls'].append(url)
            debug['matches'] = matches

            # now count each ticker occurrences
            counter = Counter(m['tkr'] for m in matches)
            debug['tickers'] = counter
            ticker1= None
            score1=None
            # select most often ticker
            if counter:
                print(counter)
                winner, n_occurrences = counter.most_common(1)[0]
                if n_occurrences >= cls.BING_URL_COUNTER_THLD:
                    ticker = winner
                    score = n_occurrences
                    try:
                        if counter.most_common(2)[1] >= cls.BING_URL_COUNTER_THLD:
                            ticker1, score1 = counter.most_common(2)[1]
                    except:
                        1


            # select appropriate exchange for winning ticker
            if ticker:
                exchanges = Counter([m['exch'] for m in matches if m['tkr'] == ticker and 'exch' in m])
                debug['exchanges'] = exchanges

                if exchanges:
                    # sort by number of occurrences and by preferred exchanges
                    exchanges = sorted(
                        exchanges.items(),
                        key=lambda item: (item[1], item[0] in ['NASDAQ', 'NYSE']),
                        reverse=True,
                    )
                    exchange = exchanges[0][0]

        except Exception:
            print(f'[WARN] Could not retrieve ticker/exchange for {company_name}: {format_exc()}')

        return {
            'ticker': ticker,
            'exchange': exchange,
            'score': score,
            'debug': debug,
            'ticker1':ticker1,
            'score1':score1
        }

    @staticmethod
    def _norm_levenshtein(in1: str, in2: str) -> float:
        return 1 - levenshtein_distance(in1, in2) / max(len(in1), len(in2))

    @classmethod
    def get_ticker(cls, company_names: List[str]) -> Dict:
        """
        Finds ticker for selected company name using any possible ticker retrieval method.
        First we try to use bing for ticker retrieval; we fallback to yahoo finance API if bing failed.

        Args:
            company_names: possible company / company aliases / related entities' names

        Returns: Dict {
            'ticker':
            'exchange':
            'score':
            'debug':
        }
        """
        result = {'ticker': None, 'exchange': None, 'score': None, 'debug': {'company_names': company_names}}


        for company_name in company_names:
            bing_res = cls.query_bing(company_name)
            result['debug']['bing'] = bing_res
            if bing_res['ticker']:
                result['ticker'], result['exchange'] = bing_res['ticker'], bing_res['exchange']
                return result

        yahoo_res = cls.query_yahoo(company_names)
        result['debug']['yahoo'] = yahoo_res
        if yahoo_res['ticker']:
            result['ticker'], result['exchange'] = yahoo_res['ticker'], yahoo_res['exchange']
            return result

        # now try to get parent company
        parent = get_owner(company_names[0])
        if parent and parent != company_names[0]:
            print(f'[INFO] Found parent company: {parent}')
            parent_res = cls.get_ticker([parent])
            result['debug']['parent'] = parent_res
            if parent_res['ticker']:
                result['ticker'], result['exchange'] = parent_res['ticker'], parent_res['exchange']
                return result

        return result




parser = argparse.ArgumentParser()
parser.add_argument('--limit', default=None)
args = parser.parse_args()

infos = Targets.get_all()
infos = list(infos.values())
#shuffle(infos)
limit = int(args.limit) if args.limit else len(infos)
infos = infos[:limit]
out = os.path.join(CSV_DIR, 'ticker', f'QA_ticker_{datetime.now():%Y-%m-%d-%H%M}.csv')
os.makedirs(os.path.dirname(out), exist_ok=True)

#with open(out, 'w', encoding='utf-8') as f:
for i, info in enumerate(infos):
        sys.stdout.flush()
        try:
            print(f'\t{i+1}/{limit} {info["official_name"]}')
            sys.stdout.flush()

            official_name = info['official_name']

            info.update({
                f'wf_{key}': value
                for key, value in TickerFinder.query_wf_output([official_name] + info['unofficial_names']).items()
            })

            info.update({
                f'yahoo_{key}': value
                for key, value in TickerFinder.query_yahoo([official_name]).items()
            })

            info.update({
                f'bing_{key}': value
                for key, value in TickerFinder.query_bing(official_name).items()
            })

            wf_owner=get_owner(official_name)

            info.update({
                f'wfParentBing_{key}': value
                for key, value in TickerFinder.query_bing(wf_owner).items()
            })

            info.update({
                f'wfParentYahoo_{key}': value
                for key, value in TickerFinder.query_yahoo([wf_owner]).items()
            })
            df = pd.DataFrame(infos)
            df.to_csv(out, header=False, encoding='utf-8')
            sys.stdout.flush()
        except:
            1


print(f'Wrote results to {out}')