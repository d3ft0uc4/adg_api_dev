from .ticker_merchant_map import TickerFinder


def test_complicated():
    answers = {
        # NOT COMPANY NAME --> 'intercontinental.com': 'IHG',  # not ICE (of "Intercontinental")
        'new york stock exchange': 'ICE',
    }
    for rawin, ticker in answers.items():
        assert TickerFinder.get_ticker(rawin)['ticker'] == ticker
