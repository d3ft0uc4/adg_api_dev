"""
This script is just to demo Google BigQuery (GBQ) connections.

© 2018 Alternative Data Group. All Rights Reserved.

Requires:
    pandas
    sqlalchemy
    pymysql

Usage:

With Python3 (run from repo root):
    python3 -m gbq.csv_to_mssql  # May ask for Google account BigQuery auth via URL
                                 $ (unless os.environ["GOOGLE_APPLICATION_CREDENTIALS"] is set).

Or run below uncommented commands interactively (e.g. ipython3)
"""

import os

import sqlalchemy
import pandas as pd

from pandas.io import gbq

from mapper.settings import GOOGLE_APPLICATION_CREDENTIALS, SQL_CONNECTION_STRING

# df = pd.read_csv("adg_api_dev_share/original_data\\factual_mapping_uid.csv" ,  parse_dates=True , infer_datetime_format=True, encoding="utf-8")
# df = pd.read_csv(r"C:\Dropbox (ADG)\ADG Team Folder\tech\adg_api_dev_share\find winner\t2tscores1.csv",  parse_dates=True , infer_datetime_format=True, error_bad_lines=False)

mysqlengine = sqlalchemy.create_engine(SQL_CONNECTION_STRING)
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = GOOGLE_APPLICATION_CREDENTIALS
projectid = 'peaceful-signer-187500'

#
# We'll load data from this table in the MySQL db and load it to GBQ.
#

# FIXME: Use some test table instead of the FM-specific one below?
ourTBL = "fm_output_m2t_based_freq_adj_tbl"

df = pd.read_sql_table(ourTBL, mysqlengine)
#df[df.columns[df.dtypes == 'object']] = df.select_dtypes(include=['object']).apply(lambda x: x.str.encode('utf8', 'replace'))

# df.to_sql(ourTBL, mssqlengine, if_exists='replace', index=False)
# df.to_sql('agg_merch_master', engine, if_exists='replace', index=False,chunksize =10000, dtype = { 'ticker_from_source':sqlalchemy.types.JSON, 'raw_trans_string_top_example':sqlalchemy.types.JSON})

gbq_target_table_name = 'altdg_merchtag.' + ourTBL
gbq.to_gbq(df, gbq_target_table_name, projectid, if_exists='replace', chunksize=10000, verbose=True)
