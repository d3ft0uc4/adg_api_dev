from google.cloud import bigquery
from pandas.io import gbq
import pandas as pd
import os
import unicodedata
import sys
import numpy as np
import uuid
from packages.settings import

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = GOOGLE_APPLICATION_CREDENTIALS
projectid = 'peaceful-signer-187500'
stdout = sys.stdout
reload(sys)
sys.setdefaultencoding('utf-8')
sys.stdout = stdout

def uid(x):
    return uuid.uuid4()

def uni(x):
    try:
        return unicodedata.normalize("NFKD", x).encode('utf-8').strip()
    except:
        try:
            return x.decode('utf-8', 'ignore').encode('utf-8').strip()
        except:
            return x


df = pd.read_csv("C:\\Dropbox (ADG)\\ADG Team Folder\\tech\\adg_api_dev_share\\original_data\\raw_cc_orig.csv",  parse_dates=True , infer_datetime_format=True, error_bad_lines=False, encoding="ISO-8859-1")
df.dtypes
df['uid'] = [str(uuid.uuid4()) for i in range(len(df))]
#df['uid'] = df['date'].astype(str).apply(uid)
df['date'] = pd.to_datetime(df['date'], infer_datetime_format=True)
#df = df.astype(str)
#df = df.select_dtypes(include=['object']).applymap(lambda x: uni(x))
#df[df.columns[df.dtypes=='object']]=df.select_dtypes(include=['object']).applymap(lambda x: uni(x))

#df.to_csv("C:\\Dropbox (ADG)\\ADG Team Folder\\tech\\api_dev\\original_data\\u2_raw1.csv", index=False)

gbq.to_gbq(df, 'altdg_merchtag.raw_u2', projectid, if_exists='replace' , chunksize=10000, verbose=True)