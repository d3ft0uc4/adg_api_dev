"""
This script is just to demo db connections.

© 2018 Alternative Data Group. All Rights Reserved.

Requires:
    pandas
    sqlalchemy
    pymysql

Usage:

With Python3 (run from repo root):
    python3 -m gbq.csv_to_mysql

Or run below uncommented commands interactively (e.g. ipython3)
"""

import sqlalchemy
import sys

import sqlalchemy
import pandas as pd
from packages.utils import db_execute

# FIXME: Below needed?
# stdout = sys.stdout
# reload(sys)

#
# We'll load data from CSV file and load it into a DB table.
#

# FIXME: Comlpete hardcoded path start with Dropbox adg_api_dev_share folder below.
df = pd.read_csv(r"C:\Users\genem_wdqp7\Documents\gitlab\api\csv\ticker\QA_ticker_2018-09-09-0341_ge_processed.csv", parse_dates=True , infer_datetime_format=True, error_bad_lines=False)

# ???: charset needed?
engine = sqlalchemy.create_engine(SQL_CONNECTION_STRING + "?charset=utf8")

df[df.columns[df.dtypes=='object']]=df.select_dtypes(include=['object']).apply(lambda x: x.str.encode('utf8','replace'))
tableName = 'QA_ticker4k'
truncate_query = "TRUNCATE TABLE " + tableName
db_execute(truncate_query)

df.to_sql(tableName, engine, if_exists='append', index=False)
# ???: Both above and below needed?
df.to_sql(tableName, engine, if_exists='replace', index=False)

# df.to_sql('agg_merch_master', engine, if_exists='replace', index=False,chunksize =10000, dtype = { 'ticker_from_source':sqlalchemy.types.JSON, 'raw_trans_string_top_example':sqlalchemy.types.JSON})
# ourQuery=sa_text("TRUNCATE TABLE :tbblname")

df_fromdb = pd.read_sql('''SELECT * FROM agg_merch_master_top_merchants''', engine)
