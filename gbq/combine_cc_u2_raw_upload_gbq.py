from google.cloud import bigquery
from pandas.io import gbq
from mapper.settings import GOOGLE_APPLICATION_CREDENTIALS
import pandas as pd
import os
import unicodedata
import sys
import numpy as np
import uuid
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = GOOGLE_APPLICATION_CREDENTIALS
projectid = 'peaceful-signer-187500'
stdout = sys.stdout
reload(sys)
sys.setdefaultencoding('utf-8')
sys.stdout = stdout

def uid(x):
    return uuid.uuid4()

def uni(x):
    try:
        return unicodedata.normalize("NFKD", x)
    except:
        try:
            return x.decode('utf-8', 'ignore')
        except:
            return x

df_cc = pd.read_csv('/Users/edwinchin/Dropbox (ADG)/training_tools/original_data/cc_combo_master.csv')
df_andrei = pd.read_excel('/Users/edwinchin/Dropbox (ADG)/training_tools/original_data/td_dc_all_pairs_master_mapping_v5_with_maker_v5b.xlsx')
df_u2 = pd.read_excel('/Users/edwinchin/Dropbox (ADG)/training_tools/original_data/u2_sfm.xlsx', sheetname='sfm')

df_cc['pairing'] = df_cc['td2'].astype(str).str.lower() + "_" + df_cc['dc1'].astype(str).str.lower()
df_andrei['pairing'] = df_andrei['td2'].astype(str).str.lower() + "_" + df_andrei['dc1'].astype(str).str.lower()
df_cc2 = pd.merge(df_cc, df_andrei[['parent_merchant_clean', 'child_merchant_clean', 'pairing']], on='pairing', how='left')
df_cc2 = df_cc2.rename(columns={'description': 'raw_input_string', 'parent_merchant_clean': 'ap_parent', 'child_merchant_clean': 'ap_child'})
df_cc2['source'] = 'cc'
df_u2['source'] = 'u2'
df_u2 = df_u2.rename(columns={'mid_descriptor': 'raw_input_string', 'company_name': 'u2'})
df_u2 = df_u2[['raw_input_string', 'u2', 'source']]
df = df_cc2.append(df_u2, ignore_index=True)
df = df.drop('pairing', axis=1)
df['uid'] = df['raw_input_string'].astype(str).apply(uid)
cols = ['uid', 'raw_input_string', 'source', 'cardtype', 'td2', 'dc1', 'u2', 'ap_parent', 'ap_child']
df = df[cols]
#df.to_csv('raw_all_strings.csv')

df1 = df.astype(str)
df2 = df1.applymap(lambda x: uni(x))
df2['date'] = np.nan
df2['amount'] = np.nan

gbq.to_gbq(df2, 'altdg_merchtag.raw_all_strings', projectid, if_exists='replace')

