from google.cloud import bigquery
from pandas.io import gbq
import pandas as pd
from mapper.settings import GOOGLE_APPLICATION_CREDENTIALS
import os
import sys
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = GOOGLE_APPLICATION_CREDENTIALS

project_id = 'peaceful-signer-187500'

gbq.read_gbq('SELECT * FROM altdg_merchtag.target_list', project_id)

gbq.to_gbq(df, 'altdg_merchtag.raw_train_strings', project_id, if_exists='replace')