from google.cloud import bigquery
from pandas.io import gbq
import pandas as pd
import os
import sys
import uuid
import sqlalchemy
from packages.settings import GOOGLE_APPLICATION_CREDENTIALS

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = GOOGLE_APPLICATION_CREDENTIALS
projectid = 'peaceful-signer-187500'
stdout = sys.stdout
reload(sys)
sys.setdefaultencoding('utf-8')
sys.stdout = stdout

tableName= 'fm_output'
#df=gbq.read_gbq("select * from `altdg_merchtag.agg_merch_master`", project_id=projectid, dialect='standard')
df=gbq.read_gbq("select * from `altdg_merchtag."+tableName+ "`", project_id=projectid, dialect='standard')

df[df.columns[df.dtypes=='object']]=df.select_dtypes(include=['object']).apply(lambda x: x.str.encode('latin-1','ignore'))
engine = sqlalchemy.create_engine("mysql+pymysql://adgsql:adg_dev18@nychousingdata.cpu3eplzthqs.us-east-1.rds.amazonaws.com:3306/altdg_merchtag")
df.to_sql(tableName, engine, if_exists='replace', index=False)

#df.to_sql('agg_merch_master', engine, if_exists='replace', index=False,chunksize =10000, dtype = { 'ticker_from_source':sqlalchemy.types.JSON, 'raw_trans_string_top_example':sqlalchemy.types.JSON})

#df['uid'] = [str(uuid.uuid4()) for i in range(len(df))]
#df1=pd.DataFrame()
#df1['uid'] = df['uid']
#df1['tojoin']=df['merchant_string'].map(str)+df['data_source'].map(str)
#gbq.to_gbq(df1, 'altdg_merchtag.agg_merch_master_uid_join', projectid, if_exists='replace' , chunksize=10000, verbose=True)
