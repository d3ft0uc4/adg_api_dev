"""
Module for extracting keywords from bing for any input string.
Uses Tf/Idf and all existing bing cache as corpus.

Example training TfidfVectorizer:
    CACHE_EXPIRATION=9999 LOG_LEVEL=INFO python -m tfidf.tfidf --raw-inputs=".../inputs_products.txt" --limit=100

Example running TfidfVectorizer:
    python -m tfidf.tfidf "PEPP FARM GOLDFISH 58 OZ P120 T30H4 SL140"
"""

import json
import pickle
import re
import struct
from copy import copy
from itertools import chain
from logging import getLogger
from math import log
from operator import itemgetter
from os import path
from typing import List, Tuple, Dict, Union
import string

import numpy as np
import pandas as pd
from nltk.stem.porter import PorterStemmer
from numpy import float32
from sklearn.feature_extraction.text import TfidfVectorizer
from typing.io import IO
from unidecode import unidecode

from bing.bing_web_core import BingWebCore
from packages.utils import Timer
from packages.utils import db_execute, read_list, pretty, tokenize

logger = getLogger(f'adg.{__name__}')
stemmer = PorterStemmer()


def normalize(value: str) -> str:
    value = unidecode(value).lower()
    value = value.translate(str.maketrans('', '', string.punctuation))
    value = re.sub(r'®', '', value, flags=re.IGNORECASE)
    value = stemmer.stem(value)
    return value


def tokenize_and_normalize(value: str) -> List[str]:
    return [normalize(token) for token in tokenize(value)]


vectorizer = TfidfVectorizer(
    tokenizer=tokenize_and_normalize,
    strip_accents='ascii',
    # stop_words=stopwords.words('english'),
    ngram_range=(1, 4),
    use_idf=True,
    # min_df=10,
)


def save(vectorizer: TfidfVectorizer, output_file: str):
    """
    Converts huge vectorizer to 2 files: metadata.pickle & idf.bin.
    """

    ngram_range = range(vectorizer.ngram_range[0], vectorizer.ngram_range[1] + 1)
    vocabulary = vectorizer.vocabulary_
    idf = vectorizer.idf_

    with Timer('Pickling metadata'):
        pickle.dump((ngram_range, vocabulary), open(f'{output_file}.metadata.pickle', 'wb'))

    with Timer('Pickling idf'):
        file = open(f'{output_file}.idf.bin', 'wb')
        file.writelines(struct.pack('f', float32(n)) for n in idf)

    with open(f'{output_file}.idf.bin', 'rb') as file:
        n = int(len(idf) / 2)

        size = struct.calcsize('f')
        file.seek(size * n, 0)
        assert idf[n] - struct.unpack('f', file.read(size))[0] < 0.001


# def extract_keywords(raw_input: str, vectorizer: TfidfVectorizer, num: int = 10) -> Dict[str, float]:
#     vec = vectorizer.transform([raw_input])
#
#     coo_matrix = vec.tocoo()
#     tuples = zip(coo_matrix.col, coo_matrix.data)
#     sorted_items = sorted(tuples, key=lambda x: (x[1], x[0]), reverse=True)
#
#     feature_names = vectorizer.get_feature_names()
#
#     # use only topn items from vector
#     sorted_items = sorted_items[:num]
#
#     score_vals = []
#     feature_vals = []
#
#     # word index and corresponding tf-idf score
#     for idx, score in sorted_items:
#         # keep track of feature name and its corresponding score
#         score_vals.append(round(score, 3))
#         feature_vals.append(feature_names[idx])
#
#     # create a tuples of feature,score
#     # results = zip(feature_vals,score_vals)
#     keywords = {}
#     for idx in range(len(feature_vals)):
#         keywords[feature_vals[idx]] = score_vals[idx]
#
#     return keywords

with Timer('Loading vectorizer'):
    VECTORIZER_PATH = path.join(path.dirname(path.abspath(__file__)), 'data', 'tfidf_vectorizer.bingCacheAll4gram2')
    VECTORIZER_NGRAM_RANGE, VECTORIZER_VOCABULARY = pickle.load(open(f'{VECTORIZER_PATH}.metadata.pickle', 'rb'))
    VECTORIZER_IDF = open(f'{VECTORIZER_PATH}.idf.bin', 'rb')
    float_size = struct.calcsize('f')


def extract_keywords(
    text: str,
    text_as_doc: bool = False,
    vocabulary: Dict[str, int] = VECTORIZER_VOCABULARY,
    ngram_range: range = VECTORIZER_NGRAM_RANGE,
    idfs: Union[List[float], np.ndarray, IO[bytes]] = VECTORIZER_IDF,
) -> List[Tuple[str, float]]:
    """
    Extract keywords from text.

    Args:
        text: string for extracting keywords
        text_as_doc: whether to use input text as document for tokenizing and extracting keywords; by default this
            is false and document is generated from Bing response for the queried text
        vocabulary: dict of {keyword: index_in_idfs_array}
        ngram_range: range of ngrams
        idfs: list or raw bytes of idf scores

    Returns:
        List of tuples (keyword, tfidf score).
        Keywords may be not from text if `text_as_doc` is False.
    """

    # each chunk is a tuple of (text, rank); the score means how important that piece of text is
    if text_as_doc:
        chunks = [(text, 0)]

    else:
        bing_response = BingWebCore().query(text)
        chunks = [
            (value.get('name'), i)
            for i, value in enumerate(bing_response.get('webPages', {}).get('value', []))
        ]

    tokens = list(chain.from_iterable([
        [(word, rank) for word in tokenize(chunk_text)]
        for chunk_text, rank in chunks
    ]))

    tokens_ngrams = []
    normalized_ngrams = []
    ranks = []

    for ngram_size in ngram_range:
        for i in range(len(tokens) - ngram_size + 1):
            subtokens = tokens[i:i+ngram_size]

            # don't create chunks from different ranks
            if subtokens[0][1] != subtokens[-1][1]:
                continue

            ranks.append(subtokens[0][1])
            tokens_ngrams.append(' '.join(tkn[0] for tkn in subtokens))
            normalized_ngrams.append(' '.join(normalize(tkn[0]) for tkn in subtokens))

    # generate scores {"keyword": {'tf': <tf score>, 'idf': <idf score>}, ...}
    results: Dict[str, dict] = {}
    for feature in set(normalized_ngrams):
        index = vocabulary.get(feature)
        token = tokens_ngrams[normalized_ngrams.index(feature)]

        if index:
            if isinstance(idfs, (list, np.ndarray)):
                idf = idfs[index]
            else:
                idfs.seek(float_size * index, 0)
                idf = struct.unpack('f', idfs.read(float_size))[0]
        else:
            idf = 1 / log(1000000)  # should be log(len(documents))

        feature_ranks = [rnk for i, rnk in enumerate(ranks) if normalized_ngrams[i] == feature]
        tf = sum([(-2.298*log(rnk+1) + 9.7801) / 5.5 for rnk in feature_ranks])
        # tf = len(feature_ranks)
        # tfidf = tf * idf

        results[token] = {'tf': tf, 'idf': idf}

    # # retrieve list of "final" supertokens (= which are not subtokens themselves)
    # final_supertokens: Dict[str, dict] = {}
    # for candidate, candidate_scores in results.items():
    #     for other, other_scores in results.items():
    #         if candidate != other and \
    #            re.search(r'(^|\s)' + re.escape(candidate) + r'(\s|$)', other, flags=re.IGNORECASE):
    #             # candidate is subtoken
    #             break
    #     else:
    #         # candidate is supertoken
    #         final_supertokens[candidate] = candidate_scores

    # # apply penalty to subtokens
    # penalized_results = {}
    # for token, scores in results.items():
    #     penalized_results[token] = copy(scores)
    #     #
    #     # for other, other_scores in results.items():
    #     #     if token == other:
    #     #         continue
    #     #
    #     #     if re.search(r'(^|\s)' + re.escape(token) + r'(\s|$)', other, flags=re.IGNORECASE):
    #     #         penalized_results[token]['tf'] -= other_scores['tf']
    #     #
    #
    #     if any((re.search(r'(^|\s)' + re.escape(token) + r'(\s|$)', other, flags=re.IGNORECASE) and token != other)
    #            for other, other_score in results.items()):
    #
    #         penalized_results[token]['tf'] *= 0.8

    results = sorted(
        [(token, scores['tf'] * scores['idf']) for token, scores in results.items()],
        key=itemgetter(1),
        reverse=True,
    )

    best_results = [res for res in results if res[1] >= results[0][1] * 0.60]

    best_results = sorted(best_results, key=lambda res: (
        any(res[0] in other[0] for other in best_results if other != res),
        -res[1],
    ))

    return best_results


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('value', nargs='?')
    parser.add_argument('--input-as-doc', action='store_true', default=False)
    parser.add_argument('--generate-vectorizer', action='store_true', default=False)
    parser.add_argument('--vectorizer', default=f'{VECTORIZER_PATH}')
    parser.add_argument('--corpus')
    parser.add_argument('--limit', type=int)
    args = parser.parse_args()

    if args.generate_vectorizer:
        # generate vectorizer based on bing cache
        logger.info(f'Generating vectorizer')

        if args.corpus:
            logger.info(f'Using "{args.corpus}" as raw inputs for corpus')

            bing = BingWebCore()

            with Timer('Generating source data'):
                if args.corpus.endswith('.txt'):
                    corpus = read_list(args.corpus)

                elif args.corpus.endswith('.csv'):
                    corpus = pd.read_csv(args.corpus)['content']
                    corpus.replace('', np.nan, inplace=True)
                    corpus = corpus.dropna()
                    corpus = corpus.apply(lambda row: ' '.join(json.loads(row)))
                    corpus = list(corpus)

                if args.limit:
                    corpus = corpus[:args.limit]

            #     data = {}
            #     for i, inp in enumerate(inputs, start=1):
            #         print(i, '/', len(inputs))
            #         data[inp] = bing.query(inp)
            #
            # with Timer('Generating raw inputs'):
            #     raw_inputs = [
            #         ' '.join(value.get('name') for value in bing_response.get('webPages', {}).get('value', []))
            #         for bing_response in data.values()
            #     ]

        else:
            logger.info(f'Using Bing cache as raw inputs for corpus')
            # bing_responses = {row[0]: json.loads(row[1]) for row in db_execute("""
            #     SELECT
            #       JSON_EXTRACT(content, '$.queryContext.originalQuery') AS query,
            #       content
            #     FROM bing_cache
            #     GROUP_BY query
            # """)}

            with Timer('Reading SQL'):
                data = {row[0]: json.loads(row[1]) for row in db_execute(f"""
                    SELECT JSON_EXTRACT(content, '$.queryContext.originalQuery') AS query, content
                    FROM bing_cache
                    {('LIMIT ' + str(args.limit)) if args.limit else '--'}
                """)}  # {rawin: bing_response}

            with Timer('Generating corpus'):
                corpus = [
                    ' '.join(value.get('name') for value in bing_response.get('webPages', {}).get('value', []))
                    for bing_response in data.values()
                ]

        if args.value:
            logger.info(f'Adding "{("bing response of " + args.value) if not args.input_as_doc else args.value}" '
                        f'to training corpus')

            if args.input_as_doc:
                corpus.append(args.value)

            else:
                bing_response = BingWebCore().query(args.value)
                corpus.append(
                    ' '.join(value.get('name') for value in bing_response.get('webPages', {}).get('value', [])))

        with Timer('Generating vectorizer'):
            tfidf_matrix = vectorizer.fit_transform(corpus)
            # vectorizer.corpus_size = len(corpus)
            # df = pd.DataFrame(tfidf_matrix.toarray(), columns=vectorizer.get_feature_names())
            # df.transpose().to_clipboard()
            logger.debug(f'Feature names (subset): {vectorizer.get_feature_names()[:100]}')
            # logger.debug(f'Matrix:\n{tfidf_matrix.todense()}')

        save(vectorizer, args.vectorizer)
        print(f'Wrote vectorizer to {args.vectorizer}')

    if args.value:
        # extract keywords based on vectorizer
        logger.info(f'Extracting keywords for {args.value}')

        params = {}
        if args.vectorizer:
            ngram_range, vocabulary = pickle.load(open(f'{args.vectorizer}.metadata.pickle', 'rb'))
            idfs = open(f'{args.vectorizer}.idf.bin', 'rb')
            params = {
                'ngram_range': ngram_range,
                'idfs': idfs,
                'vocabulary': vocabulary,
            }

        print(pretty(extract_keywords(
            args.value,
            text_as_doc=args.input_as_doc,
            **params,
        )[:10]))
