© 2018 Alternative Data Group. All Rights Reserved.

# ADG Tools and REST API repository

Each directory in this repo, often a Python module, represents a user-level tool and should be an independent sub=project with it's own documentation.

> TODO: Use [git lfs](https://docs.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html) ?

## General Setup

Create the virtual environment, this can be done with [./init-venv.sh](init-venv.sh) helper script.
The script will also install the api requirements from the [requirements.txt](requirements.txt) file in the venv.

The requirements.txt file includes all the dependencies for the API application (api dir) and the tools it uses (in other dirs).

## Git Flow & Continuous Integration

- Special branches are `master` (development integration version) and `production` (live production version)
- No direct commits should be sent to these special branches (only merges)
- Once the `master` branch is merged to `production` and the latter pushed, this triggers the CI system; The live production application is then automatically updated.
  The GitLab [CI settings](https://gitlab.com/geneman/adg_api_dev/settings/ci_cd) should include the followin environment variables:
  - A `X_3SCALE_SHARED_SECRET` for the app's config (any string);
  - Appropriate `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` [credentials](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/concepts-roles.html), so the GitLab CI runner can deploy to AWS EB.

> See [.gitlab-ci.yml](.gitlab-ci.yml) file

## Manual Deployment (ElasticBeanstalk)

The application (Merchant Recognition API) in api is setup to be deployed to Elastic Beanstalk.

Two application environments should be configured there, with the necessary environment variables (`SQLALCHEMY_DATABASE_URI`, `X_3SCALE_SHARED_SECRET`).

Manual deployment of the API application can be done using `eb deploy` [command](Manual deployment of the API application can be done using `eb deploy` [command]() (get it with `pip install awsebcli`).
) (get it with `pip install awsebcli`).

Development environment deployment:
```
git checkout master
git pull
eb deploy develop
```

Production environment deployment:
```
git checkout production
git pull
eb deploy production
```

## CloudWatch logs
> Read https://serebrov.github.io/html/2015-05-20-cloudwatch-setup.html

> TODO: Further document .ebextensions/

## API README

For specific info about the Merchant Recognition API including running locally and automated testing, see [api/README.md](api/README.md).

## 3Scale Integration

We are using a API proxy gateway hosted on 3Scale for the Merchant Recognition API authentication.

This is the simplest integration option, in the application code we only heed to make sure that incoming requests have
the 'X-3scale-proxy-secret-token' headers containing the shared 3Scale secret.
The secret is configured in 3Scale UI (API -> Integration -> edit APIcast configuration).

On our side, we get the secret from the X_3SCALE_SHARED_SECRET environment variable, which is configured in the
ElasticBeanstalk UI (Configuration -> Software Configuration, Environment Properties).
