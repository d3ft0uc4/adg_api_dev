Introduction
============

The ADG API provides service for mapping unstructured data (credit card transaction strings or urls) to structured company information.

The API can be accessed at
> https://api-2445582026130.production.gw.apicast.io

All data is sent and received as JSON.

Authentication
==============

The ADG API uses token-based authentication, which means that developers must pass their individual `X_user_key` parameter with every request. An example url for `/mapper` endpoint would be as follows:

```
https://api-2445582026130.production.gw.apicast.io/mapper-plus?X_user_key=[user_key]
```

You may try out the API using our free demo API key:
> `f816b9125492069f7f2e3b1cc60659f0`

We have a special `/health` endpoint which you may use to check whether API is operational and your API key is valid:

```sh
curl "https://api-2445582026130.production.gw.apicast.io/health?X_user_key=f816b9125492069f7f2e3b1cc60659f0"
```

On success you will recieve response
```json
{
  "message": "OK"
}
```

Example
=======

Request for `"boots 1136"`:

```sh
curl "https://api-2445582026130.production.gw.apicast.io/merchant-mapper-plus?X_user_key=f816b9125492069f7f2e3b1cc60659f0" \
-d '["boots 1136"]'
```

Response for `"boots 1136"`:
```json
[
  {
    "Original Input": "boots 1136",
    "Company Name": "Boots UK",
    "Confidence": 0.67,
    "Confidence Level": "Low",
    "Aliases": [
      "Boots UK Ltd"
    ],
    "Alternative Company Matches": [
      "Boot Barn Holdings Inc",
      "Boot Barn"
    ],
    "Related Entities": [
      {
        "Name": "Walgreens Boots Alliance, Inc.",
      },
      {
        "Name": "Boots Opticians",
      },
      {
        "Name": "Boots Retail Ireland",
      },
      {
        "Name": "Alliance Healthcare",
      },
      {
        "Name": "Duane Reade",
      },
      {
        "Name": "Walgreen Company",
      },
      {
        "Name": "Farmacias Benavides",
      },
      {
        "Name": "Farmacias Ahumada",
      }
    ],
    "Ticker": "WBA",
    "Exchange": "NASDAQ",
    "Majority Owner": "WALGREENS BOOTS ALLIANCE INC",
    "FIGI": "BBG000BWLSK9",
    "Websites": [
      "boots-uk.com",
      "walgreensbootsalliance.com"
    ],
    "Tree": {
      "Low Score Candidates": [],
      "High Score Candidates": [
        [
          {
            "value": "Boot Barn Holdings Inc",
            "source": "mayor_search",
            "score": 0.6285619735717773,
            "aliases": [
              "boot barn"
            ]
          },
          {
            "value": "Boot Barn Holdings Inc",
            "source": "prodigy[bane]",
            "score": 0.056236470588235284,
            "aliases": [
              "boot barn"
            ]
          },
          {
            "value": "Boot Barn Holdings Inc",
            "source": "prodigy[wut]",
            "aliases": [
              "boot barn"
            ],
            "websites": [
              "bootbarn.com"
            ]
          },
          {
            "value": "Boot Barn",
            "source": "prodigy[mayor_lite]",
            "score": 1
          },
          {
            "value": "Boot Barn",
            "source": "prodigy[cutleft]",
            "score": 1,
            "websites": [
              "bootbarn.com"
            ]
          },
          {
            "value": "Boot Barn Holdings Inc",
            "source": "prodigy[bane[fwfm]]",
            "score": 0.11880658566951752,
            "aliases": [
              "boot barn"
            ]
          },
          {
            "value": "Boot Barn",
            "source": "prodigy[fwfm]",
            "score": 0.844149649143219,
            "ticker": "BOOT",
            "exchange": "NYSE"
          },
          {
            "value": "Boot Barn",
            "source": "prodigy",
            "score": 0.9306882619857788
          },
          {
            "value": "Boot Barn",
            "source": "prodigy[ktotam]",
            "score": 1,
            "websites": [
              "bootbarn.com"
            ]
          },
          {
            "value": "Boot Barn",
            "source": "prodigy[bane]",
            "score": 0.2450788480306857
          }
        ]
      ],
      "Winner": [
        {
          "value": "Walgreens Boots Alliance, Inc.",
          "source": "britanic.A",
          "aliases": [
            "walgreens boots alliance"
          ],
          "websites": [
            "walgreensbootsalliance.com"
          ],
          "ticker": "WBA",
          "exchange": "NASDAQ",
          "children": [
            {
              "value": "Boots UK Ltd",
              "source": "britanic.A",
              "score": 1,
              "aliases": [
                "boots"
              ],
              "websites": [
                "boots-uk.com"
              ],
              "children": [
                {
                  "value": "Boots Opticians",
                  "source": "britanic.A"
                },
                {
                  "value": "Boots Retail Ireland",
                  "source": "britanic.A"
                }
              ]
            },
            {
              "value": "Alliance Healthcare",
              "source": "britanic.A"
            },
            {
              "value": "Boots UK",
              "source": "britanic.A",
              "aliases": [
                "boots"
              ]
            },
            {
              "value": "Duane Reade",
              "source": "britanic.A"
            },
            {
              "value": "Walgreen Company",
              "source": "britanic.A",
              "aliases": [
                "walgreen"
              ]
            },
            {
              "value": "Farmacias Benavides",
              "source": "britanic.A"
            },
            {
              "value": "Farmacias Ahumada",
              "source": "britanic.A"
            }
          ]
        },
        {
          "value": "Boots UK",
          "source": "mayor_lite",
          "score": 1,
          "aliases": [
            "boots"
          ]
        },
        {
          "value": "Walgreens Boots Alliance Inc",
          "source": "mayor_search",
          "score": 0.5729450583457947,
          "aliases": [
            "walgreens boots alliance"
          ]
        },
        {
          "value": "Boots UK",
          "source": "g0_KN",
          "score": 0.8550103306770325,
          "aliases": [
            "boots"
          ],
          "websites": [
            "boots-uk.com"
          ]
        }
      ]
    }
  }
]
```

Products
========

ADG API provides access to 6 products which all map unstructured data to structured company information. They differ by following parameters:

**Input type**
* `/domain-mapper` and `/domain-mapper-plus` endpoints map _domain name or url_ to structured company information
* `/merchant-mapper` and `/merchant-mapper-plus` endpoints map _transaction string_ to structured company information
*  `/mapper` and `/mapper-plus` endpoints automatically determine type of input query (_url_ or _transaction string_) and then map it to structiured company information

**Extended response**
* `/domain-mapper`, `/merchant-mapper` and `/mapper` endpoints provide base company information fields (such as `Company Name`, `Aliases`, `Ticker`, `FIGI` etc, see complete reference below)
* `/domain-mapper-plus`, `/merchant-mapper-plus` and `/mapper-plus` endpoints provide base company information fields, plus additional field `Tree`. The `Tree` field contains all information retrieved during mapping process, including results from each of our searching agents, as well as relations between all the companies we found.

Below is a summary table of our products:

|   | `/merchant-mapper` | `/merchant-mapper-plus` | `/domain-mapper` | `/domain-mapper-plus` | `/mapper` | `/mapper-plus` |
|---|---|---|---|---|---|---|
| Transaction strings mapping | Yes | Yes | | | Yes | Yes |
| Urls mapping | | | Yes | Yes | Yes | Yes
| Extended mapping information | | Yes | | Yes | | Yes


Merchant mapper
---------------
> **POST** `/merchant-mapper`

Maps transaction string to structured company information.

Works best on credit card transaction strings, but there's no strict limits on input type.

Example request:
```sh
curl "https://api-2445582026130.production.gw.apicast.io/merchant-mapper?X_user_key=f816b9125492069f7f2e3b1cc60659f0" \
-d '["azure  payroll    ***********8240~~08800~~~~46000~~0~~~~0000"]'
```

Response fields:
* `Original Input`
* `Company Name`
* `Confidence`
* `Confidence Level`
* `Aliases`
* `Alternative Company Matches`
* `Related Entities`
* `Ticker`
* `Exchange`
* `Majority Owner`
* `FIGI`
* `Websites`

Domain mapper
-------------
> **POST** `/domain-mapper`

Maps domain name or url to structured company information.

Example request:
```sh
curl "https://api-2445582026130.production.gw.apicast.io/domain-mapper?X_user_key=f816b9125492069f7f2e3b1cc60659f0" \
-d '["aws.com"]'
```

Response fields:
* `Original Input`
* `Company Name`
* `Confidence`
* `Confidence Level`
* `Aliases`
* `Alternative Company Matches`
* `Related Entities`
* `Ticker`
* `Exchange`
* `Majority Owner`
* `FIGI`
* `Websites`


Combined mapper
---------------
> **POST** `/mapper`

Automatically detects whether input query is url or transaction string. Runs domain mapper if url was detected, otherwise runs merchant mapper. Returns structured company information.

Example request:
```sh
curl "https://api-2445582026130.production.gw.apicast.io/mapper?X_user_key=f816b9125492069f7f2e3b1cc60659f0" \
-d '["azure  payroll    ***********8240~~08800~~~~46000~~0~~~~0000"]'
```

Response fields:
* `Original Input`
* `Company Name`
* `Confidence`
* `Confidence Level`
* `Aliases`
* `Alternative Company Matches`
* `Related Entities`
* `Ticker`
* `Exchange`
* `Majority Owner`
* `FIGI`
* `Websites`


Extended merchant mapper
------------------------
> **POST** `/merchant-mapper-plus`

Maps transaction string to structured company information. Has the same response fields as merchant mapper, plus the `Tree` field which contains extended information retrieved during mapping process.

Works best on credit card transaction strings, but there's no strict limits on input type.

Example request:
```sh
curl "https://api-2445582026130.production.gw.apicast.io/merchant-mapper-plus?X_user_key=f816b9125492069f7f2e3b1cc60659f0" \
-d '["azure  payroll    ***********8240~~08800~~~~46000~~0~~~~0000"]'
```

Response fields:
* `Original Input`
* `Company Name`
* `Confidence`
* `Confidence Level`
* `Aliases`
* `Alternative Company Matches`
* `Related Entities`
* `Ticker`
* `Exchange`
* `Majority Owner`
* `FIGI`
* `Websites`
* `Tree`

Extended domain mapper
----------------------
> **POST** `/domain-mapper-plus`

Maps domain name or url to structured company information. Has the same response fields as domain mapper, plus the `Tree` field which contains extended information retrieved during mapping process.

Example request:
```sh
curl "https://api-2445582026130.production.gw.apicast.io/domain-mapper-plus?X_user_key=f816b9125492069f7f2e3b1cc60659f0" \
-d '["aws.com"]'
```

Response fields:
* `Original Input`
* `Company Name`
* `Confidence`
* `Confidence Level`
* `Aliases`
* `Alternative Company Matches`
* `Related Entities`
* `Ticker`
* `Exchange`
* `Majority Owner`
* `FIGI`
* `Websites`
* `Tree`

Extended combined mapper
------------------------
> **POST** `/mapper-plus`

Maps transaction string or url to structured company information. Has the same response fields as combined mapper, plus the `Tree` field which contains extended information retrieved during mapping process.

Example request:
```sh
curl "https://api-2445582026130.production.gw.apicast.io/mapper-plus?X_user_key=f816b9125492069f7f2e3b1cc60659f0" \
-d '["azure  payroll    ***********8240~~08800~~~~46000~~0~~~~0000"]'
```

Response fields:
* `Original Input`
* `Company Name`
* `Confidence`
* `Confidence Level`
* `Aliases`
* `Alternative Company Matches`
* `Related Entities`
* `Ticker`
* `Exchange`
* `Majority Owner`
* `FIGI`
* `Websites`
* `Tree`

Response metadata reference
===========================

| Field | Type | Example | Description
| ----- | ---- | ------- | -----------
| Original Input | string | `"azure payroll ***8240~~46000~0~0000"` | Query string from the request
| Company Name | string | `"Microsoft"` <br> or <br> `"No Match / Small Business"` | Name of company which has closest relation to original input. <br><br> If no company was found, or the company has revenue smaller than certain threshold, `"No Match / Small Business"` will be returned.
| Confidence | float | `0.98` | Float number from `[0.5;1]` range which shows how confident the API is about the result. Bigger values mean higher confidence.<br><br> `0` in case of no match.
| Confidence Level | string or `null` | `"High"` | String representation of `Confidence` field. Possible values: <br> <ul><li>`"Low"` - when 0.5 <= `Confidence` < 0.7</li><li>`"Medium"` - when 0.7 <= `Confidence` < 0.9</li><li>`"High"` - when 0.9 <= `Confidence` <= 1</li><li>`null` - in case of no match</li></ul>
| Aliases | array[string] | `["Microsoft Corporation", "Microsoft Azure", "Microsoft Store"]` | List of other names of the company.<br><br>Empty list in case of no match.
Alternative Company Matches | array[string] | `["Azure Knowledge Corp"]` | List of other candidates for `Company Name` field that were considered worse than a selected one. This means that API was less confident about these companies, but still they are good candidates for an answer.<br><br>Empty list if no match.
| Related Entities | array[object] | `[{"Name": "LinkedIn"}, {"Name": "GitHub"}]` | List of companies which are somehow related to the company, for example owner company or subsidiaries.<br><br>Empty list if no match.
| Ticker | string or `null` | `"MSFT"` | Ticker symbol of matched company or any of related companies, if any of them is publicly traded.<br><br>If company is traded at several exchanges, this field contains only one of them, and we don't guarantee which one will be selected. However you can inspect `Tree` field in order to retrieve all tickers.<br><br>We have a list of supported exchanges, see `Exchange` field for detailed description. Tickers not from those exchanges are ignored.<br><br>`null` if none of the companies is publicly traded at supported exchanges.
| Exchange | string or `null` | `"NASDAQ"` | If `Ticker` field is not `null`, this field will contain corresponding exchange.<br><br>Possible values for this field:<ul><li>`NASDAQ`</li><li>`NYSE` - New York</li><li>`LSE` - London</li><li>`SIX` - Swiss</li><li>`TYO` - Tokyo</li><li>`FWB` - Frankfurt</li><li>`TSX` - Toronto</li><li>`BME` - Madrid</li><li>`ASX` - Australian Securities Exchange</li><li>`SZSE` - Shenzhen</li><li>`ENX` - Euronext</li><li>`SSE` - Shanghai</li><li>`HKG` - Stock Exchange of Hong Kong</li><li>`KRX` - Korea</li><li>`null` - if `Ticker` is `null`</li></ul>
| Majority Owner | string or `null` | `"MICROSOFT CORP"` | This field contains major owner of the company (the one with biggest percentage of ownership), or the company itself if there's no owner.<br><br>`null` in case of no match.
| FIGI | string or `null` | `"BBG000BPH9J3"` | [Financial Instrument Global Identifier](https://en.wikipedia.org/wiki/Financial_Instrument_Global_Identifier), only when `Ticker` is not `null`.<br><br>`null` if `Ticker` is `null`.
| Websites | array[string] | `["microsoft.com", "azure.microsoft.com"]` | List of all websites which were found during mapping process. Only official company websites are listed (i.e. no social links, retailers etc).<br><br>Empty list in case of no match.
| Tree | object | See below | See below

Extended mapping information
============================

For each request to the API we launch multiple agents that try to find answer to the query. Then we collect agents' results, analyze them, select best answer and generate json response.

However, if base response fields aren't enough for your needs, we expose internal results from our agents in the `Tree` field.


All the data returned by our agents, in a structured way. This is a perfect place to get any information that is not present in any of previous fields.

Each company in this tree is represented as an object with following fields:
* `value` - company name
* `source` - which agent generated this company object
* `score` (optional) - float from `[0;1]` range which represents how confident the agent is about this company object. Value close to `1` means that agens is very sure that this company objects is a correct answer to original query. If this field is not present, it means that this company object was retrieved not by the agent directly but by using some additional information.
* `aliases` (optional) - list of aliases to `value` field. Usually a simplified version of company name or alternative company name.
* `websites` (optional) - list of official websites found by agent
* `ticker` (optional) - ticker symbol of company if publicly traded, if the agent retreived it
* `exchange` (optional) - exchange of the ticker
* `children` (optional) - list of subsidiary companies objects, as found by the agent

Below is an example of company object from `ffwm` agent. Here, the agent retrieved `"Microsoft"` company, but is not very sure about the answer (confidence is `0.65`). It was able to get ticker and exchange, as well as subsidiary companies. Since "children" companies were retrieved not by the agent itself but by using some meta information about `"Microsoft"` company, the objects `"LinkedIn"` and `"GitHub"` don't have score.

```python
{
    "value": "Microsoft",
    "source": "ffwm",
    "score": 0.6546676754951477,
    "ticker": "MSFT",
    "exchange": "NASDAQ",
    "children": [
        {
            "value": "LinkedIn",
            "source": "ffwm"
        },
        {
            "value": "GitHub",
            "source": "ffwm"
        },
    ]
}
```

Companies from different agents which either
1) have similar values
2) belong to the same "company family", i.e. are related by "ownership" or "subsidiary" relations

are put in the same list, which is called "group":

```python
# "Microsoft" group
[
    {
        "value": "Microsoft",
        "source": "ffwm"
    },
    {
        "value": "Microsoft",
        "source": "britannic.A"
    }
]

# "Amazon" group (Amazon itself + its subsidiaries)
[
    {
        "value": "Amazon, Inc",
        ...
    },
    {
        "value": "Abebooks",
        ...
    },
    {
        "value": "Amazon Web Services",
        ...
    },
]
```

So if companies are somehow related, they will definitely be in one group.

The `Tree` field contains all groups, split into 3 categories:

* `Low Score Candidates` - list of groups that were considered as wrong ones (not related to original input)
* `High Score Candidates` - list of groups that were considered as correct ones (they are good answers for original input) except the winning group
* `Winner` - best matching group


Example:
```python
"Tree": {
    "Low Score Candidates": [
        [
            {
            "value": "Azure Power Global Ltd",
            "source": "mayor_search",
            "score": 0.741779625415802,
            "aliases": [
                "azure power"
            ]
            }
        ],
        [
            {
            "value": "Azure Health Technology Ltd",
            "source": "mayor_search",
            "score": 0.6186082363128662,
            "aliases": [
                "azure health technology"
            ]
            }
        ],
        [
            {
            "value": "AZURE MINERALS LTD",
            "source": "vigvam",
            "score": 1,
            "aliases": [
                "azure minerals"
            ]
            }
        ]
    ],


    "High Score Candidates": [],


    "Winner": [
        {
            "value": "Microsoft",
            "source": "ffwm",
            "score": 0.6546676754951477,
            "ticker": "MSFT",
            "exchange": "NASDAQ",
            "children": [
            {
                "value": "LinkedIn",
                "source": "ffwm"
            },
            {
                "value": "GitHub",
                "source": "ffwm"
            },
            {
                "value": "lynda.com",
                "source": "ffwm",
                "aliases": [
                "lynda"
                ]
            },
            {
                "value": "Xbox",
                "source": "ffwm",
                "aliases": [
                "xbox one"
                ]
            }
            ]
        },
        {
            "value": "Microsoft",
            "source": "prodigy[ffwm]",
            "score": 0.7708454728126526,
            "ticker": "MSFT",
            "exchange": "NASDAQ",
            "children": [
            {
                "value": "LinkedIn",
                "source": "prodigy[ffwm]"
            },
            {
                "value": "GitHub",
                "source": "prodigy[ffwm]"
            },
            {
                "value": "lynda.com",
                "source": "prodigy[ffwm]",
                "aliases": [
                "lynda"
                ]
            },
            {
                "value": "Xbox",
                "source": "prodigy[ffwm]",
                "aliases": [
                "xbox one"
                ]
            }
            ]
        },
        {
            "value": "Microsoft Azure",
            "source": "mayor_lite",
            "score": 1
        },
        {
            "value": "Microsoft Azure",
            "source": "banya",
            "score": 0.3198818023159566
        },
        {
            "value": "Microsoft",
            "source": "banya",
            "score": 0.13235371537659552
        },
        {
            "value": "Microsoft Azure",
            "source": "banya[ffwm]",
            "score": 0.8063877820968628
        },
        {
            "value": "Microsoft Azure",
            "source": "prodigy[wut]",
            "websites": [
            "azure.microsoft.com",
            "microsoft.com"
            ]
        },
        {
            "value": "Microsoft",
            "source": "prodigy[mayor_lite]",
            "score": 1
        },
        {
            "value": "Microsoft",
            "source": "prodigy[cutleft]",
            "score": 1,
            "websites": [
            "azure.microsoft.com",
            "microsoft.com"
            ]
        },
        {
            "value": "Microsoft",
            "source": "prodigy[banya]",
            "score": 0.08865059584326994
        },
        {
            "value": "Microsoft Corporation",
            "source": "prodigy[britanic.A]",
            "score": 1,
            "aliases": [
            "microsoft"
            ],
            "websites": [
            "microsoft.com"
            ],
            "ticker": "MSFT",
            "exchange": "NASDAQ"
        },
        {
            "value": "Microsoft",
            "source": "prodigy",
            "score": 0.8689349889755249
        },
        {
            "value": "Microsoft Azure",
            "source": "bottlerocket[TW]",
            "score": 0.7342640606943633
        },
        {
            "value": "Microsoft Corporation",
            "source": "britanic.A",
            "score": 1,
            "aliases": [
            "microsoft"
            ],
            "websites": [
            "microsoft.com"
            ],
            "ticker": "MSFT",
            "exchange": "NASDAQ"
        }
    ]
}
```

> All the fields `Company Name`, `Aliases`, `Related Entities`, `Ticker`, `Exchange`, `Majority Owner`, `FIGI` and `Websites` of API response are generated from the `Winner` group. `Alternative Company Matches` field is populated from `High Score Candidates` supergroup.






MISC
====

```python
import requests
import json

API_URL = 'https://api-2445582026130.production.gw.apicast.io'
ENDPOINT = '/merchant-mapper'
API_KEY = 'f816b9125492069f7f2e3b1cc60659f0'
QUERY = 'azure  payroll    ***********8240~~08800~~~~46000~~0~~~~0000'

response = requests.post(
    f"{API_URL}{ENDPOINT}?X_user_key={API_KEY}",
    data=json.dumps([QUERY]),
    timeout=30,
)
if response.ok:
    print(response.json()[0])
```

ADG API bulk mapper for Python
------------------------------

We also have a python library that wraps all interaction with the API and also provides nice method for bulk processing. Find it at https://github.com/geneman/altdg.

Tree
----


Limits
======

Query length
------------
Up to `127` chars; contact us if you have data that exceeds this limit.

Number of queries per request
-----------------------------
Currently we allow only one query per API request.

Number of concurrent requests
-----------------------------
We don't have any specific limits for how much queries you make simultaneously, but many concurrent queries may decrease API performance; currently we recommend you not to exceed the limit of 8-12 concurrent requests, but for more exact information please contact us via email.

Contacts
========
Please contact us at info@altdg.com if you have any questions.




OTHER APIS
==========

https://docs.thinknum.com/docs/query-api
------------

    {
      "sector": "Consumer Services",
      "country": "USA",
      "industry": "General Retailers",
      "display_name": "Williams-Sonoma",
      "id": "nyse:ism"
    }

https://data.crunchbase.com/docs/organization
---------

^-- good one


https://www.fullcontact.com/developer/company-api/

https://developers.hubspot.com/docs/methods/companies/get_companies_by_domain

https://clearbit.com/attributes

