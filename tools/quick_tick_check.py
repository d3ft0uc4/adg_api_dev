"""
This module runs domains through:
    - alive & non-parked check
    - whois organization retrieval

CLI:
    python -m tools.quick_tick_check <path_to_domains_file.txt>
"""

import logging
from typing import Dict

from packages.utils import qa
from domain_tools.utils import is_alive
from domain_tools.parked import is_parked
from domain_tools.whois import WhoisInfoRetrieval

logging.basicConfig()
log = logging.getLogger(f'adg.{__name__}')
log.setLevel(logging.INFO)


def process(domain: str) -> Dict:
    result = {'domain': domain}

    try:
        result['is_alive'] = is_alive(domain)
    except Exception as exc:
        log.error(f'{domain} - {exc}')
        return result

    if not result['is_alive']:
        return result

    try:
        result['is_parked'] = is_parked(domain)
    except Exception as exc:
        log.error(f'{domain} - {exc}')
        return result

    result['is_active'] = result['is_alive'] and not result['is_parked']
    if not result['is_active']:
        return result

    try:
        result['owner'] = WhoisInfoRetrieval.get_organization(domain)
    except Exception as exc:
        log.error(f'{domain} - {exc}')
        return result


qa(process, fields=['domain', 'is_alive', 'is_parked', 'is_active', 'owner'])
