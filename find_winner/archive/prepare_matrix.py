import pandas as pd

from collections import defaultdict

distinct_targets_file = 'distinct_targets.tsv'
input_file = 'identity_data_sparse.tsv'
output_file = 'test.csv'

distinct_targets = []
with open(distinct_targets_file) as f:
    for line in f:
        if line:
            distinct_targets.append(line)
all_pairs = []
for t1 in distinct_targets:
    for t2 in distinct_targets:
        all_pairs.append((t1, t2))

df = pd.read_csv(input_file, sep='\t')

size = df.shape[0]

uid_to_input_string_and_target_name = {}
d = {}
for i, row in df.iterrows():
    key = (row.uidlist_input_string, row.input_string, row.target_name)
    if key not in d:
        d[key] = {}
    d[key][(row.score_type, row.search_addon_string, row.search_result_type)] = row.score
    uid_to_input_string_and_target_name[row.uidlist_input_string] = (row.input_string, row.target_name)
    if i % 1000 == 0:
        print('Building: {:.2}%'.format((i / size) * 100))

found = defaultdict(int)

uid_inputs_and_targets = list(set([(uidlist_input_string, target_name) for uidlist_input_string, _, target_name in d.keys()]))

data = {}
data2 = {}
# for key, val in d.items():
#     uidlist_input_string, _, target_name = key
for uidlist_input_string, target_name in uid_inputs_and_targets:

    if (uidlist_input_string, target_name) not in data:
        data[(uidlist_input_string, target_name)] = {}

    # row = [input_string, target_name, '', '', '']
    for score_type in ('unweighted', 'weighted'):
        for search_addon_string in ('{}', 'who owns {}', '{} company', 'who manufactures {}'):
        # for search_addon_string in ('{}', '{} company', 'who manufactures {}'):
            row = []
            for search_result_type in ('displayUrl', 'about', 'name', 'snippet'):
                # print(score_type, search_addon_string, search_result_type)
                k = (score_type, search_addon_string, search_result_type)
                if (uidlist_input_string, search_addon_string.format(uidlist_input_string), target_name) not in d: continue
                val = d[(uidlist_input_string, search_addon_string.format(uidlist_input_string), target_name)]
                score = val.get(k, None)
                if score:
                    row.append(score)
                    found[score_type] += 1
                    found[search_addon_string] += 1
                    found[search_result_type] += 1
                else:
                    row.append('')
                data[(uidlist_input_string, target_name)][k] = score #if score else ''

                data2[(uidlist_input_string, target_name, score_type, search_addon_string, search_result_type)] = score if score else ''

                if ('adp', 'adp', "unweighted", "{}", "name") == (uidlist_input_string, target_name, score_type, search_addon_string, search_result_type):
                    print('aaaa')
                    print(data2[('adp', 'adp', "unweighted", "{}", "name")])

                # if uidlist_input_string == 'adp' and target_name == 'adp' and score:
                #     print('("{}", "{}", "{}")'.format(*k))
                #     print("{}/{}/{}: {}".format(uidlist_input_string, search_addon_string, target_name, score))
                #
                #     print(data[(uidlist_input_string, target_name)][k])

print('result:')
print(data[('adp', 'adp')][("unweighted", "{}", "name")])
print(data2[('adp', 'adp', "unweighted", "{}", "name")])

with open(output_file, 'w') as f:
    for i, key in enumerate(data):
        uidlist_input_string, target_name = key

        if not(uidlist_input_string == 'hautelook' and target_name == 'hautelook'):
            continue
    # sorted_pairs = sorted(set(all_pairs))
    # for i, (uidlist_input_string, target_name) in enumerate(sorted_pairs):

        line = [uidlist_input_string, target_name, '', '', '']
        for score_type in ('unweighted', 'weighted'):
            # for search_addon_string in ('{}', '{} company', 'who manufactures {}'):
            for search_addon_string in ('{}', 'who owns {}', '{} company', 'who manufactures {}'):
                for search_result_type in ('displayUrl', 'about', 'name', 'snippet'):
                    # val = data[key][(score_type, search_addon_string, search_result_type)]

                    if (uidlist_input_string, target_name, score_type, search_addon_string, search_result_type) in data2:
                        val = data2[(uidlist_input_string, target_name, score_type, search_addon_string, search_result_type)]
                        line.append(val)
                    else:
                        line.append('')

        # print(';'.join([str(v) for v in line]))
        f.write(';'.join([str(v) for v in line]) + '\n')

        # print('done {} out of {}'.format(i, len(sorted_pairs)))
        print('done {} out of {}'.format(i, len(data)))

print(found)
