
# coding: utf-8

# In[1]:


import sqlalchemy
# import numpy as np
import pandas as pd
#import xgboost as xgb
from google.cloud import bigquery
import os

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = 'C:/Dropbox (ADG)/ADG Team Folder/tech/repos/gene/adg_api_dev/access_keys/adgproj-0d8fbbb357be.json'

project_id = 'peaceful-signer-187500'

client = bigquery.Client()

basedir=r'C:\Dropbox (ADG)\ADG Team Folder\tech\adg_api_dev_share\find_maker'
engine = sqlalchemy.create_engine("mysql+pymysql://druce:dev2018@nychousingdata.cpu3eplzthqs.us-east-1.rds.amazonaws.com:3306/altdg_merchtag?charset=utf8")


# In[3]:


sqlstr = """SELECT  input_string search_target, target_name,
search_result_type, search_addon_string, 
adj_score1 as score
FROM fm_output_joined_substr1_tbl
where run_code = 'T2T-3'
and score_type='weighted'
"""


sqlstr = """SELECT  input_string search_target, target_name,
search_result_type, search_addon_string, adj_score2 score
FROM `peaceful-signer-187500.altdg_merchtag.fm_output_substr_combined`
where run_code = 'T2T-3'
and score_type='weighted'
"""


# sqlstr = """SELECT input_string search_target, target_name,
# search_result_type, search_addon_string,
# adjscore score
# FROM fm_output_substr_scores_joined
# where run_code = 'M2T-2'
# and score_type='weighted'
# """
#df_fromdb = pd.read_sql(sqlstr, engine)
df_fromdb = pd.read_gbq(sqlstr,project_id=project_id,dialect ='standard')

##################################################
# THESE DON'T WORK - AARGH
##################################################

# sqlstr = """SELECT search_target, target_name, 
# search_result_type, search_addon_string, 
# avg(score) score
# -- fm_datestamp, score_type 
# FROM (select * from fm_output_trimmed where fm_datestamp > '2018-01-22' and score_type='weighted') a
# where fm_datestamp > '2018-01-22' 
# and score_type='weighted'
# -- and search_addon_string='{} brands'
# -- and search_result_type = 'name'
# group by search_target, target_name, 
# search_result_type, search_addon_string
# order by search_target, target_name, 
# search_result_type, search_addon_string
# """
#df_fromdb = pd.read_sql(sqlstr, engine)



df_fromdb['pivot_index'] = df_fromdb['search_target'] + '|' + df_fromdb['target_name']
df_fromdb['pivot_cols'] = df_fromdb['search_addon_string'] + '|' + df_fromdb['search_result_type']


df_pivot_group = pd.DataFrame(df_fromdb.groupby(['pivot_index', 'pivot_cols'])['score'].mean())


df_pivot_group = df_pivot_group.reset_index()

df_pivot = df_pivot_group.pivot(index='pivot_index',columns='pivot_cols', values='score')

df_pivot = df_pivot.fillna(value=0)


#df_pivot1 = pd.DataFrame(df_pivot.pivot_index.str.split('|',1).tolist(),columns = ['FMinput','match_target'])
#namedcolumnsDF=pd.DataFrame(df_pivot.index.str.split('|',1).tolist(),columns = ['FMinput','match_target'])
#df_pivot=pd.concat(namedcolumnsDF,df_pivot, axis=1)
#df_pivot[['FMinput','match_target']]=pd.DataFrame(df_pivot.index.str.split('|',1).tolist(),columns = ['FMinput','match_target'])

# fm_pivot_train_file=basedir+r"\fm_pivot_train.csv"
# df_pivot.to_csv(fm_pivot_train_file)


df_pivot[df_pivot.columns[df_pivot.dtypes=='object']]=df_pivot.select_dtypes(include=['object']).apply(lambda x: x.str.encode('utf8','replace'))

z = list(df_pivot.index)
z2 = [s.split("|") for s in z]
col1 = [s1 for [s1, s2] in z2]
col2 = [s2 for [s1, s2] in z2]
df_pivot['search_target'] = col1
df_pivot['target_name'] = col2


tableName= 'fw_predictors_pivot_t2t'
df_pivot.to_sql(tableName, engine, if_exists='replace', index=False)

# make sure column order is correct matching the col order used to train model
df_pivot = df_pivot[
[
 '{} brands|about',
 '{} brands|displayUrl',
 '{} brands|name',
 '{} brands|snippet',
 '{} company|about',
 '{} company|displayUrl',
 '{} company|name',
 '{} company|snippet',
 '{} parent company|about',
 '{} parent company|displayUrl',
 '{} parent company|name',
 '{} parent company|snippet',
 '{} sister company|about',
 '{} sister company|displayUrl',
 '{} sister company|name',
 '{} sister company|snippet',
 '{}|about',
 '{}|displayUrl',
 '{}|name',
 '{}|snippet',
 'who owns {}|about',
 'who owns {}|displayUrl',
 'who owns {}|name',
 'who owns {}|snippet',
]
]


# In[14]:


dtest = xgb.DMatrix(df_pivot.values)
boost = xgb.Booster({'nthread': 4})  # init model
xgbmodel_file=basedir+r"\fm_output_pivot.XGBmodel"
boost.load_model(xgbmodel_file)


# In[15]:


ypred_proba = boost.predict(dtest)
# proba > 0.5 -> positive
# should choose a threshold to maximize [f1, accuracy, etc.]
# or should use native boost predict_class 
# however this is done at training time and we are loading R model
# param = {'max_depth':2, 'eta':1, 'silent':1, 'objective':'binary:logistic' }
# boost = xgb.train(param, dtrain, num_round)
ypred_class = (ypred_proba>0.5).astype(int)


df_pivot['predict_proba'] = ypred_proba
df_pivot['predict_pred'] = ypred_class

#df_pivot.to_csv('/Users/druce/Dropbox/find_maker/dv_pred.csv')


df_pivot['search_target'] = ''
df_pivot['target_name'] = ''

#df_pivot[['search_target','target_name']] = df_pivot.index.str.split('|',1)


z = list(df_pivot.index)
z2 = [s.split("|") for s in z]
col1 = [s1 for [s1, s2] in z2]
col2 = [s2 for [s1, s2] in z2]
df_pivot['search_target'] = col1
df_pivot['target_name'] = col2

# dv_pred_file=basedir+r"\dv_pred.csv"
# df_pivot.to_csv(dv_pred_file)


df_pivot[df_pivot.columns[df_pivot.dtypes=='object']]=df_pivot.select_dtypes(include=['object']).apply(lambda x: x.str.encode('utf8','replace'))
tableName= 'fw_response_pivot_m2t'
df_pivot.to_sql(tableName, engine, if_exists='replace', index=False)
