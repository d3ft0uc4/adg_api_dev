"""
© 2018 Alternative Data Group. All Rights Reserved.

This module is based on find_winner_GE.py.

Usage in interactive Python shell (from repo root):
    from find_winner.find_winner import FindWinner
    fw = FindWinner()

    # Get data from Find Maker's output table in a MySQL db:
    fw.load_raw_df_from_database(['Microsoft', 'AT&T'])

    # Pivot the data: (intermediate step)
    fw.pivot()

    # Optionally, save the intermediate results to CSV/DB:
    csv_file = '/tmp/pivoted.csv'
    fw.save_pivoted_df_to_disk(csv_file) # or `save_pivoted_df_to_database()`
    fw.load_pivoted_df_from_disk(csv_file) # or `load_pivoted_df_from_database()`

    # Run the prediction model:
    fw.predict()  # Results are in `fw.predictionsDF`

    # Optionally, save the final results to CSV/DB:
    fw.save_predicted_df_to_disk(csv_file) # or `save_predicted_df_to_database()`

Usage from terminal:
    python -m find_winner.find_winner "Microsoft" "Wallmart"

Or try ../find_winner.ipynb in Jupyter.
"""

import os
from pprint import pprint
from typing import List
from traceback import format_exc

import sqlalchemy
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.pool import NullPool
import pandas as pd
from pandas.io import gbq
import xgboost as xgb

from mapper.settings import db_engine, GOOGLE_APPLICATION_CREDENTIALS, GBQ_PROJECT_ID, \
    Configurable, FwFmConfig, SQL_CONNECTION_STRING
from packages.targets import TARGETS
from packages.utils import Timer
from mapper.settings import TIMERS
from packages.utils import with_timer, conditional_decorator


import logging

logger = logging.getLogger(f'adg.{__name__}')


def _to_utf8(dataframe):
    """Encodes dataframe content to UTF-8. """
    dataframe[dataframe.columns[dataframe.dtypes == 'object']] = dataframe.select_dtypes(include=['object']).apply(
        lambda x: x.str.encode('utf8', 'replace'))

    return dataframe


def _dict_reverse_lookup(dictionary, value):
    """ Performs reverse lookup for dictionary """
    return [k for k, v in dictionary.items() if v == value][0]


@Configurable(default_config=FwFmConfig)
class FindWinner:
    """
    This class allows to read output saved from find_maker (either in MySQL or Google Cloud) and perform
    find_winner's process.

    Attributes:
        rawFMscoresDF - Raw dataframe as received from FindMaker
        pivotedFMscoresDF = Pivoted rawFMscoresDF (see `pivot` method)
        predictionsDF = Resulting predictions (see `predict` method)
    """
    DELIMITER = '|||'

    def __init__(self, run_code):
        """
        Init FM.

        Args:
            run_code: run code for selecting specific rows from fm_output
        """
        self.rawFMscoresDF = None
        self.pivotedFMscoresDF = None
        self.predictionsDF = None
        self.run_code = run_code

        # FIXME: This query uses fm_output_m2t_based_freq_adj_tbl for performance. Such table must be maintained manually.
        self.sqlstr_fm_output_target_frequencies = f"""
            SELECT
                created_at,
                input_string,
                search_addon_string,
                search_result_type,
                a.target_name,
                score_type,
                score * coalesce(b.cnt_adj_mult, 1) as score,
                a.target_name_clean,
                a.run_code,
                a.uid
            FROM fm_output a
            LEFT JOIN fm_output_m2t_based_freq_adj_tbl b
            ON a.target_name = b.target_name
            WHERE
              a.score_type = 'weighted' AND
              a.run_code = %(run_code)s
        """

        self.sqlstr_fm_output_substr_scores = f"""
            SELECT
                MAX(a.input_string) AS input_string,
                MAX(a.target_name_clean) AS target_sub,
                MAX(a.run_code) AS run_code,
                MAX(a.score_type) AS score_type,
                MAX(b.score) / (a.score * 1.0) AS substr_ratio,
                a.score - (MAX(b.score)) AS score,
                a.uid
            FROM ({self.sqlstr_fm_output_target_frequencies}) b
            LEFT JOIN ({self.sqlstr_fm_output_target_frequencies}) a ON
                a.input_string = b.input_string AND
                a.target_name_clean <> b.target_name_clean AND
                b.target_name_clean regexp concat( '(^|[ ])', a.target_name_clean, '($|[ ])') AND
                a.search_addon_string = b.search_addon_string AND
                a.search_result_type = b.search_result_type AND
                a.score_type = b.score_type AND
                a.run_code = b.run_code
            WHERE
                a.input_string IN %(input_strings)s AND
                a.score_type = %(score_type)s AND
                a.run_code = %(run_code)s
            GROUP BY a.uid
        """

        self.sqlstr_fm_output_substr_ratios = f"""
            SELECT
                input_string,
                target_sub AS target_name,
                score_type,
                AVG(substr_ratio) AS avg_substr_ratio
            FROM
                ({self.sqlstr_fm_output_substr_scores}) AS fm_output_substr_scores
            GROUP BY
                input_string,
                target_sub,
                run_code,
                score_type
        """

        self.sqlstr_fm_output_joined_substr1 = f"""
            SELECT
                a.input_string,
                a.target_name,
                a.search_result_type,
                a.search_addon_string,
                CASE
                    WHEN
                        b.avg_substr_ratio is not null AND
                        b.avg_substr_ratio > 0.55
                    THEN 0
                    WHEN
                        b.avg_substr_ratio is not null
                    THEN a.score_subtracted * POWER((1-b.avg_substr_ratio), {self.config.SQL_SUBSTRING_PENALTY_POWER})
                    ELSE a.score
                END AS adj_score1
            FROM (
                SELECT aa.*, bb.score as score_subtracted
                FROM ({self.sqlstr_fm_output_target_frequencies}) aa
                LEFT JOIN ({self.sqlstr_fm_output_substr_scores}) bb
                ON aa.uid=bb.uid
            ) a
            LEFT JOIN ({self.sqlstr_fm_output_substr_ratios}) b ON
                a.input_string = b.input_string AND
                a.target_name = b.target_name AND
                a.score_type = b.score_type
            WHERE
                a.input_string IN %(input_strings)s AND
                a.score_type = %(score_type)s
        """

        self.sqlstr_adj_scores = f"""
            SELECT
                input_string search_target,
                target_name,
                search_result_type,
                search_addon_string,
                adj_score1 as score
            FROM ({self.sqlstr_fm_output_joined_substr1}) fm_output_joined_substr1
        """

    @conditional_decorator(with_timer, TIMERS)
    def load_raw_df_from_database(self, input_strings, score_type='weighted'):
        """
        Gets raw DataFrame from database into `self.rawFMscoresDF`.

        Args:
            input_strings: Only rows for these input strings will be selected
            score_type: 'weighted' or 'unweighted' - Type of scores to use

        Returns:
            DataFrame with fields:
            - search_target
            - target_name
            - search_result_type
            - search_addon_string
            - score
        """
        self.rawFMscoresDF = pd.read_sql(self.sqlstr_adj_scores, db_engine, params={
            'score_type': score_type,
            'input_strings': input_strings,
            'run_code': self.run_code,
        })
        self.rawFMscoresDF['search_target'] = self.rawFMscoresDF['search_target'].str.lower()

    def load_raw_df_from_gbq(self, input_strings, gbq_project_id=GBQ_PROJECT_ID, score_type='weighted'):
        """ See `load_raw_df_from_database` """

        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = GOOGLE_APPLICATION_CREDENTIALS
        self.rawFMscoresDF = gbq.read_gbq(self.sqlstr_adj_scores, gbq_project_id, dialect='standard', params={
            'score_type': score_type,
            'input_strings': input_strings,
            'run_code': self.run_code,
        })
        self.rawFMscoresDF['search_target'] = self.rawFMscoresDF['search_target'].str.lower()

    @staticmethod
    def clear_fm_output(run_code: str):
        """ Clears rows in fm_output with specific run code """
        engine = sqlalchemy.create_engine(SQL_CONNECTION_STRING, poolclass=NullPool)
        engine.execute('DELETE FROM fm_output WHERE run_code=%(run_code)s', run_code=run_code)

    @conditional_decorator(with_timer, TIMERS)
    def pivot(self):
        """
        Given dataframe with FM results, calculate mean score for each combination of "search_target|target_name" and
        "search_addon_string|search_result_type".

        Populates `self.pivotedFMscoresDF` with DataFrame:
        - "search_target|target_name" as indexes,
        - "search_addon_string|search_result_type" as cols,
        - mean scores as values.
        """
        assert self.rawFMscoresDF is not None
        if self.rawFMscoresDF.empty:
            # `pivot` won't work on empty df
            self.pivotedFMscoresDF = pd.DataFrame()
            return

        self.rawFMscoresDF['combined_target'] = \
            self.rawFMscoresDF['search_target'] + self.DELIMITER + self.rawFMscoresDF['target_name']
        self.rawFMscoresDF['combined_search'] = \
            self.rawFMscoresDF['search_addon_string'] + self.DELIMITER + self.rawFMscoresDF['search_result_type']

        groups = pd.DataFrame(
            self.rawFMscoresDF.groupby(['combined_target', 'combined_search'])['score'].mean()
        ).reset_index()

        self.pivotedFMscoresDF = groups.pivot(
            index='combined_target',
            columns='combined_search',
            values='score',
        ).fillna(value=0)

        self.pivotedFMscoresDF = _to_utf8(self.pivotedFMscoresDF)

    def save_pivoted_df_to_disk(self, filename):
        """
        Saves already pivoted dataframe from `self.pivotedFMscoresDF` into csv file.

        Args:
            filename: path to csv file to store dataframe.
        """
        assert self.pivotedFMscoresDF is not None
        self.pivotedFMscoresDF.to_csv(filename)

    def save_pivoted_df_to_database(self, table_name='fw_predictors_pivot_t2t'):
        """
        Saves already pivoted dataframe from `self.pivotedFMscoresDF` into database table.

        Args:
            table_name: table to store dataframe.
        """
        assert self.pivotedFMscoresDF is not None
        self.pivotedFMscoresDF.to_sql(table_name, db_engine, if_exists='append', index=False)

    def load_pivoted_df_from_disk(self, filename):
        """
        Loads already pivoted dataframe from csv file into `self.pivotedFMscoresDF`.
        If error happens, `self.pivotedFMscoresDF` is set to None.

        Args:
            filename: path to csv file with dataframe contents
        """
        self.pivotedFMscoresDF = None

        try:
            self.pivotedFMscoresDF = pd.read_csv(filename, index_col=0)
        except Exception:
            logger.error(f'Loading from CSV failed: {format_exc()}')

    def load_pivoted_df_from_database(self, table_name='fw_predictors_pivot_t2t'):
        """
        Loads already pivoted dataframe from mysql database into `self.pivotedFMscoresDF`.
        If error happens, `self.pivotedFMscoresDF` is set to None.

        Args:
            table_name: name of table with dataframe contents
        """
        self.pivotedFMscoresDF = None

        try:
            self.pivotedFMscoresDF = pd.read_sql(table_name, db_engine)
        except Exception as exc:
            logger.error(f'Loading from DB failed: {format_exc()}')

    @conditional_decorator(with_timer, TIMERS)
    def predict(self):
        """
        Takes pulled DataFrame, reshapes it and runs gradient boosting.

        Returns:
            DataFrame with keys like:
            - {} brands|about
            - {} sister company|about
            - who owns {}|about
            - ...
            - predict_proba
            - predict_pred
            - search_target
            - target_name
            Values for "{}"-like cols are 0 or 1.
        """

        assert self.pivotedFMscoresDF is not None

        if self.pivotedFMscoresDF.empty:
            self.predictionsDF = pd.DataFrame()
            return

        # make the order match cols order for training
        # '{} brands|about',
        # '{} brands|displayUrl',
        # '{} brands|name',
        # '{} brands|snippet',
        # '{} company|about',
        # ...
        cols = []
        for query_type, query_string in self.config.SEARCH_STRING_MAP.items():
            for field in self.config.SEARCH_RESULT_TYPES:
                cols.append(f'{query_string}{self.DELIMITER}{field}')

        # Not all of `cols` may be present in `self.pivotedFMscoresDF`.
        # For example, there may be no `displayUrl` col because its scores were zeros.
        # However, we need those cols (even zero scores), so we manually add them below.
        non_existing_cols = (set(cols) - set(self.pivotedFMscoresDF.columns))
        for non_existing_col in non_existing_cols:
            self.pivotedFMscoresDF[non_existing_col] = 0

        self.predictionsDF = self.pivotedFMscoresDF

        if self.predictionsDF.empty:
            logger.debug('PredictionsDF is empty')
            return

        xgbmodel_file = os.path.join(os.path.dirname(__file__), self.config.MODEL)
        logger.debug(f'Running xgb.Booster with model "{self.config.MODEL}"')

        boost = xgb.Booster({'nthread': 4})  # init model
        boost.load_model(xgbmodel_file)
        # pass `predictionsDF` with reordered columns so that they match trained model
        dtest = xgb.DMatrix(self.predictionsDF[cols].values)

        ypred_proba = boost.predict(dtest)
        # proba > 0.5 -> positive
        # should choose a threshold to maximize [f1, accuracy, etc.]
        # or should use native boost predict_class
        # however this is done at training time and we are loading R model
        # param = {'max_depth':2, 'eta':1, 'silent':1, 'objective':'binary:logistic' }
        # boost = xgb.train(param, dtrain, num_round)
        ypred_class = (ypred_proba > 0).astype(int)

        self.predictionsDF['predict_proba'] = ypred_proba
        self.predictionsDF['predict_pred'] = ypred_class

        # restore target data
        target_data = [s.split(self.DELIMITER) for s in list(self.predictionsDF.index)]
        self.predictionsDF['search_target'] = [search_target for search_target, target_name in target_data]
        self.predictionsDF['target_name'] = [target_name for search_target, target_name in target_data]

        self.predictionsDF = _to_utf8(self.predictionsDF)

    def save_predicted_df_to_disk(self, filename):
        """
        Saves FindWinner's output (`self.predictionsDF`) to specified file.

        Args:
            filename: Path to csv file where the result will be written.
        """
        assert self.predictionsDF is not None
        self.predictionsDF.to_csv(filename)

    def save_predicted_df_to_database(self, table_name='fw_response_pivot_m2t'):
        """
        Saves FindWinner's output (`self.predictionsDF`) to specified file.

        Args:
            table_name: Table name where the result will be written.
        """
        assert self.predictionsDF is not None
        self.predictionsDF.to_sql(table_name, db_engine, if_exists='append', index=False)

    @staticmethod
    def _normalize_score(score):
        """ Normalizes raw score from xgb.Booster prediction. Cap at 1.0 """
        # return min(pow(exp(score) / 2.718, 10), 1.0)
        # New normalization:
        # return min(score/0.3-1.85, 1.0)
        return min(score, 1.0)

    @conditional_decorator(with_timer, TIMERS)
    def all_winners(self, search_target):
        """
        List of top targets retrieved from `self.predictionsDF`.
        Only rows with `predict_pred == 1` are returned.
        The first target should be treated as winner and others as related companies.
        Empty list means that there's no winner.

        Args:
            search_target: Search item which needs to be resolved

        Returns:
            List of dicts (rows) ordered by descending score. Sub-dict keys (cols):
            - target_name - name of matched target
            - target_name_official - official version of matched target's name
            - score - target's score after running xgb.Booster
            - norm_score - normalized score from [0; 1] range
        """

        assert self.predictionsDF is not None

        if self.predictionsDF.empty:
            return []

        # first, filter `predictionsDF` and leave only winners
        winners_df = self.predictionsDF[
            (self.predictionsDF.predict_pred == 1) &
            (self.predictionsDF.search_target == search_target.lower().encode())
        ].sort_values(by='predict_proba', ascending=0)

        winners_df['target_name'] = winners_df['target_name'].apply(lambda x: x.decode())

        # now construct new dataframe with only necessary info and converted types
        results = pd.DataFrame()
        results['target'] = winners_df['target_name']
        results['winner'] = winners_df['target_name'].apply(lambda x: TARGETS.get_official_name(x))
        # we would like to have float scores, not np.float32
        results['score'] = winners_df['predict_proba'].apply(lambda x: float(x))
        results['norm_score'] = results['score'].apply(self._normalize_score)
        results = results.reset_index().drop('combined_target', 1).to_dict('records')

        return results


if '__main__' == __name__:
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('inputs', nargs='+')
    args = parser.parse_args()

    print('Inputs: {}'.format(args.inputs))

    with Timer('Find winner'):
        fw = FindWinner()
        # get data from Find Maker's output table
        fw.load_raw_df_from_database(args.inputs)

        # pivot the data (intermediate step)
        fw.pivot()

        # run the prediction model:
        fw.predict()

        # save the final results to DB:
        fw.save_predicted_df_to_database()

    for input_string in args.inputs:
        print(f'Winner for {input_string}:')
        print(pprint(fw.all_winners(input_string)))

    print('Output saved to database')
