"""
© 2018 Alternative Data Group. All Rights Reserved.

Find Winner / Find Maker helper class.

The goal is to find a best match across candidates ("targets") for selected input_strings.

FindMaker gets an input string and queries Bing for the string itself and its combinations ("who owns {}",
"{} brands" etc). Then, for each target FindMaker searches across Bing responses and sets scores for that
"response-target-location" combination. (Location is the place in the bing responses.) Score is calculated as number
of occurrences + positions of target in response. FindMaker's process result is a dataframe with cols like:
input string | query type ("{}", "who owns {}" etc) | location ("name", "snippet" etc) | target | score
> See FindMaker's docstring for exact column names.

Finally, FindMaker pushes resulting data frames to MySQL table `fm_output`, and it may be used as input for FindWinner.

FindWinner's responsibility is to decide whether any of candidates is a match for selected input string. It pulls a
dataframe (result of FindMaker), reshapes it (see FW's `pivot` method) and runs xgboost on it. The resulting
dataframe will contain these cols, among others:
search_target (input string) | target_name (target/candidate) | ... | predict_proba (probability of match)

To distinguish chunks of data (FindMaker's and FindWinner's), specify `run_code` string. Running FindMaker of
FindWinner with specific `run_code` will limit all processing items to those with specified run code.

Usage in interactive Python shell (from repo root):
    from find_winner.fw_fm import FMFW
    winners = FMFW().find_winner(['at&t'])

Usage from terminal:
    python3 -m find_winner.fw_fm "AT&T" "Microsoft Corp" --csv=1
"""
import os
import json
from multiprocessing import Process
from typing import Optional
import logging

from packages.targets import Targets, TARGETS
from packages.utils import uid, with_timer
from mapper.settings import CSV_DIR, FwFmConfig, PARALLEL

from find_maker.find_maker import FindMaker

from find_winner.find_winner import FindWinner

logger = logging.getLogger(f'adg.{__name__}')


class FWFM:
    """
    This class is a helper for running input string through FindMaker and FindWinner classes.
    """

    def __init__(self, config=FwFmConfig, run_code=None, targets: Targets = TARGETS):
        self.config = config
        self.fm = None
        self.fw = None
        self.run_code = run_code or uid()
        self.targets = targets

    @with_timer
    def _run_find_maker(self, inputs):
        """
        Runs FindMaker on input string. Resulting dataframe is stored in `fm_output` table.
        FindMaker instance may be accessed through `.fm` attribute.
        """
        assert inputs

        # define {uid: search_item} dict
        search_items = {uid(): value for value in inputs}

        # query bing for every query_type for selected input ('input', 'who_owns_input' etc)
        self.fm = FindMaker(self.config, self.run_code, self.targets)
        search_results = self.fm.get_search_results(search_items)

        # Runs FindMaker
        self.fm.bulk_process(search_results, search_items, output_choice='database')

    @with_timer
    def find_winner(self, inputs):
        """
        Executes Find Winner for selected `run_code` and
        - outputs predictions to database
        - returns dict of winners: {input_string: winners dataframe}

        FindWinner will try to load data from `fm_output` for selected inputs.
        However, if `fm_output` is missing data for any input, this input will be passed to `FindMaker`.
        `FindMaker` will populate `fm_output` with required inputs.
        """

        assert isinstance(inputs, list)

        self._run_find_maker(inputs)
        self.fw = FindWinner(self.config, run_code=self.run_code)
        self.fw.load_raw_df_from_database(inputs)
        self.fw.pivot()
        self.fw.predict()

        # if PARALLEL:
        #     Process(
        #         target=FindWinner.clear_fm_output,
        #         args=(self.run_code, )
        #     ).start()
        # else:
        #     FindWinner.clear_fm_output(self.run_code)
        FindWinner.clear_fm_output(self.run_code)
        # self.fw.save_predicted_df_to_database()

        return {input_string: self.fw.all_winners(input_string) for input_string in inputs}

    @with_timer
    def get_winner(self, clean_input: str) -> dict:
        """
        Given clean inputs, returns Bane output with corresponding scores.

        Args:
            clean_input: input string to run through Fw/Fm

        Returns:
            {
                'thld':
                'winners':
                'losers':
                'bing_results':
            }
        """
        assert clean_input

        fwfm_winners = self.find_winner([clean_input])[clean_input]

        winners = [winner for winner in fwfm_winners if winner['norm_score'] >= self.config.WINNER_THLD]
        losers = [winner for winner in fwfm_winners if winner['norm_score'] < self.config.WINNER_THLD]

        if not winners:
            logger.debug(f'Fw/Fm produced empty result for "{clean_input}"')

        for winner in winners:
            winner['is_super_winner'] = float(winner['norm_score']) >= self.config.SUPERWINNER_THLD

        return {
            'thld': self.config.WINNER_THLD,
            'winners': winners,
            'losers': losers,
        }


if "__main__" == __name__:
    import argparse
    from packages.utils import pretty

    parser = argparse.ArgumentParser()
    parser.add_argument('inputs', nargs='+')
    parser.add_argument('--csv', action='store_true')
    parser.add_argument('--mode', default='full')
    args = parser.parse_args()

    print(f'Inputs: {args.inputs}')

    fwfm = FWFM()
    winners = fwfm.find_winner(inputs=args.inputs)

    if args.csv:
        input_list_str = '_'.join(args.inputs)[:32].replace("/", "_").replace(" ", "_")
        filename = os.path.join(CSV_DIR, 'fw_fm', f'predictionsDF_n{len(args.inputs)}_{args.mode}_{input_list_str}.csv')
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        fwfm.fw.predictionsDF.to_csv(filename)
        print(f'Predictions dataframe saved to {filename}')

    print(pretty(winners))
