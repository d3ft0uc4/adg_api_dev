import json
import math
import sqlalchemy

from datetime import datetime
from sqlalchemy import Column, DateTime, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.session import sessionmaker

from find_ticker.find_ticker_module import FindTicker
from mapper.settings import SQL_CONNECTION_STRING


Base = declarative_base()
Session = sessionmaker()


class AggMerchMasterTopMerchantsUnique(Base):
    __tablename__ = 'agg_merch_master_top_merchants_unique'

    merchant_string = Column(String)
    uidlist_agg_merch_master_top_merchants = Column(String, primary_key=True)


class FtOutput(Base):
    __tablename__ = 'ft_output'

    uid = Column(String, primary_key=True)
    uid_input_string = Column(String)
    input_string_sourcetable = Column(String)
    input_string = Column(String)
    company_name = Column(String)
    company_ticker = Column(String)
    company_type = Column(String)
    exchange = Column(String)
    log = Column(String)
    run_code = Column(String)
    created_at = Column(DateTime)
    type = Column(String)


RUN_CODE = 'main-run-2'
NUM_ROWS = 9999999


class FindTickerRun:

    _find_ticker = FindTicker()

    def run(self):
        now = datetime.now()

        session = self._setup_db()

        print('Loading rows from agg_merch_master_top_merchants_unique')
        merchant_rows = self._load_merchants(session)
        print('Rows loaded')
        input_strings = [r.merchant_string for r in merchant_rows]

        for type_ in ['google', 'bing']:
            for i, input_string in enumerate(input_strings):
                print('Running query #', i, ':', input_string)
                if i % 10 == 0 and i != 0:
                    print('Running {} out of {}, {:.2f}%'.format(i, len(input_strings), (i * 100 / len(input_strings))))
                    session.commit()

                results = self._find_ticker.find_ticker_bing(input_string, type_)
                for k, v in results.items():
                    if isinstance(v, float) and math.isnan(v):
                        results[k] = None

                row = FtOutput(
                    uid=results['uid'],
                    uid_input_string=results['uid'],
                    input_string_sourcetable=AggMerchMasterTopMerchantsUnique.__tablename__,
                    input_string=results['input'],
                    company_name=results['target'],
                    company_ticker=results['target_ticker'],
                    company_type=results['type'],
                    exchange=results['exchange'],
                    log=json.dumps(results['log']),
                    run_code=RUN_CODE,
                    created_at=now,
                    type=type_,
                )

                session.add(row)
            session.commit()

    def _setup_db(self):
        engine = sqlalchemy.create_engine(SQL_CONNECTION_STRING + '?charset=utf8', encoding='utf-8')
        Session.configure(bind=engine)
        return Session()

    def _load_merchants(self, session):
        from sqlalchemy.sql.expression import func
        rows = session.query(AggMerchMasterTopMerchantsUnique.merchant_string,
                             AggMerchMasterTopMerchantsUnique.uidlist_agg_merch_master_top_merchants) \
            .order_by(func.rand()) \
            .limit(NUM_ROWS).all()
        return list(rows)


if __name__ == '__main__':
    FindTickerRun().run()
