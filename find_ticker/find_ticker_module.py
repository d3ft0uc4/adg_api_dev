import json
import numpy as np
import os
import time
import unicodedata
import urllib.request
import uuid

from bs4 import BeautifulSoup

from bing.bing_web_core import BingWebCore

input_path = '/data/original_data/'
output_path = '/data/find_ticker/output/'
tf_json_path = '/data/find_ticker/intermediary/json/'


class FindTicker:
    _headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/63.0.3239.132 Safari/537.36'
    }

    def find_ticker_bing(self, input_string, type_, *args, **kwargs):
        """
        find_ticker_bing
        Perform Bing search for "google finance [input_string]" and extract ticker and company information from the
        first link containing www.google.com/finance?q, if found among the top 10 search results.

        Args:
            input_string (string): company/brand/transaction string to perform search on.

        Returns:
            results (array): Contains company name, ticker, exchange, type (public or private), logger.
        """

        parameters = {
            'write_to_file': False
        }

        for k, v in kwargs.items():
            parameters[k] = v

        logger = {'lg0': -1, 'lg1': -1, 'lg2': -1}

        results = {
            'input': input_string,
            'target': np.nan,
            'target_ticker': np.nan,
            'exchange': np.nan,
            'log': str(sorted(logger.items())),
            'type': np.nan,
            'uid': str(uuid.uuid4())
        }

        passthrough = {}
        for a in range(len(args)):
            passthrough['arg_' + str(a)] = args[a]

        if len(args) > 0:
            tag = '_' + passthrough['arg_0']
        else:
            tag = ''

        results.update(passthrough)

        filepath = tf_json_path + input_string.replace('/', '_') + tag + '.json'

        if parameters['write_to_file']:
            if os.path.exists(filepath):
                return

        if type_ == 'bing':
            summary_html = self._find_summary_page_with_bing(input_string, logger, results)
        elif type_ == 'google':
            summary_html = self._find_summary_page_directly(input_string)
        else:
            raise Exception('Unknown type_: {}'.format(type_))

        try:
            summary_soup = BeautifulSoup(summary_html, 'html.parser')
            soup1 = summary_soup.find("div", class_="g-tpl-67-33 g-split hdg top appbar-hide")
            rawtext = soup1.find("div", class_="g-unit g-first").text

            strtext = unicodedata.normalize("NFKD", rawtext)
            results['target'] = strtext[:strtext.find("(")].strip()
            logger['lg1'] = 0

            try:
                ticker_raw = strtext[(strtext.find("(") + 1):strtext.find(")")].strip()
                ticker_raw2 = ticker_raw[(ticker_raw.find(",") + 1):].strip()
                results['exchange'] = ticker_raw2.split(":")[0]
                results['target_ticker'] = ticker_raw2.split(":")[1]
                results['type'] = 'Public'
                logger['lg2'] = 0
            except Exception as ex:
                print('Exception:', str(ex))
                results['exchange'] = np.nan
                results['target_ticker'] = np.nan
                results['type'] = 'Private'
                logger['lg2'] = 1

        except Exception as ex:
            print('Exception:', str(ex))
            logger['lg1'] = 1

        results['log'] = str(sorted(logger.items()))

        if parameters['write_to_file']:
            with open(filepath, 'w') as fp:
                json.dump(results, fp)

        return results

    def _find_summary_page_with_bing(self, input_string, logger, results):
        # bing_term = "google finance " + str(input_string).lower()
        bing_term = "site:finance.google.com " + str(input_string).lower()
        no_of_search_results = 10
        search_result = BingWebCore().search_and_handle_failed_response(bing_term, max_query_try_num=5)
        required_url_part = "finance.google.com/"
        found = False
        for link in range(no_of_search_results):

            try:
                url = search_result['webPages']['value'][link]["displayUrl"]

                print('Attempting url {} at place {}'.format(url, link))

                # url = bing_api(bing_term, limit=0)[link].json["displayUrl"]
                if 'https://' not in url:
                    address = "https://" + url
                else:
                    address = url

                if required_url_part not in address:
                    continue

                results['log'] = str(logger.items())
                found = True
                break
            except:
                address = ''
        logger['lg0'] = link + 1 if found else 0
        try:
            request = urllib.request.Request(address, None, self._headers)  # The assembled request

            raw_html = urllib.request.urlopen(request).read()
            soup = BeautifulSoup(raw_html, 'html.parser')

            summary_tag = soup.find('a', text='Summary')
            summary_address = 'https://finance.google.com' + summary_tag['href']

            return self._execute_request(summary_address)
        except Exception as ex:
            print(ex)
        return ''

    def _find_summary_page_directly(self, input_string):
        url = 'http://finance.google.com/finance?q=' + input_string
        return self._execute_request(url)

    def _execute_request(self, url):
        url = url.replace(' ', '+')
        url = unicodedata.normalize('NFKD', url).encode('ascii', 'ignore').decode('ascii')

        summary_html = ''
        for i in range(5):
            try:
                summary_html = urllib.request.urlopen(urllib.request.Request(url, None, self._headers)).read()
                break
            except Exception as ex:
                print(str(ex))
                secs_wait = 5 * (i + 1)
                print('Unable to query google finance. Retrying in', secs_wait, 'secs')
                time.sleep(secs_wait)
        return summary_html.decode('utf-8')


if __name__ == '__main__':
    print(FindTicker().find_ticker_bing("Barnaby's Café", type_='google'))
