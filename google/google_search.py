"""
Example:
    python -m google.google_search "Amazon"
"""
import logging
from datetime import timedelta

import requests

from mapper.settings import GOOGLE_SEARCH_API_KEY, GOOGLE_SEARCH_NAME
from packages.caching import Cache, Base, DbCache
from packages.utils import conditional_decorator, LOCAL

logger = logging.getLogger(f'adg.{__name__}')


class GoogleSearchCache(Cache, Base):
    __tablename__ = 'cache_google'


class GoogleSearch:
    URL = 'https://www.googleapis.com/customsearch/v1'
    MAX_QUERY_TRY_NUM = 5

    PAYLOAD = {
        'key': GOOGLE_SEARCH_API_KEY,
        'cx': GOOGLE_SEARCH_NAME,
    }

    @conditional_decorator(DbCache(GoogleSearchCache, expiration=timedelta(days=30)),
                           LOCAL.settings.get('USE_CACHE', True))
    def query(self, input_str: str) -> dict:
        for _ in range(self.MAX_QUERY_TRY_NUM):
            payload = {
                **self.PAYLOAD,
                'q':  input_str.lower(),
            }

            try:
                response = requests.get(self.URL, params=payload)
                response.raise_for_status()
                return response.json()

            except:
                logger.exception('\n'.join([
                    f'Google query failed',
                    f'Url: {self.URL}',
                    f'Params: {payload}',
                ]))
                continue

        else:
            exc_msg = f'Tried to run the query {self.MAX_QUERY_TRY_NUM} times, but never succeeded'
            logger.error(exc_msg)
            raise Exception(exc_msg)


if __name__ == '__main__':
    import argparse
    import json

    parser = argparse.ArgumentParser()
    parser.add_argument('input')
    args = parser.parse_args()

    r = GoogleSearch().query(args.input)
    print(json.dumps(r, indent=4))
