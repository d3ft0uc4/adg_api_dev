import json
from jellyfish import jaro_winkler


def _test_domain_mapper(api_client):
    JW_DISTANCE = 0.8
    ACCEPTABLE_ACCURACY = 0.9

    test_set = {  # set with manually extracted winners
        'a2central.com': [],
        'amazingcapture.zenfolio.com': [],
        'bananarepublic.com': ['Gap Inc.'],
        'kfc.com.tr': [],
        'anacrowneplaza-fukuoka.jp': ['InterContinental Hotels Group', 'ICHG', 'Six Continents Hotels, Inc.'],
        'dostavim-kfc.ru': [],
        'dunkinindia.com': [],
        'expedia.com.vn': ['Expedia'],
        'crowneplaza.hotelsgroup.in': [],
        'trivago.com': ['Trivago'],
        'secure-oldnavy.gap.ca': ['Gap Inc.'],
        'tiffany.ca': ['Tiffany & Co.'],
        'instapro.it': ['Angie\'s List', 'Werkspot BV', 'ANGI Homeservices Inc'],
        'papajohnschina.com': [],
        'walmartcentroamerica.com': ['Walmart'],
        'pizzahut.com.pe': [],
        'staples.fr': ['Staples, Inc.'],
        'hollister.it': ['Hollister Co.'],
        'pizzahut.lk': [],
        'kfc.ae': ['KFC'],
        'kfc.co': [],
        'garmin.sk': ['Garmin'],
        'garmin.com.my': ['Garmin'],
        'sheraton-maui.com': ['Marriott International'],
        'expedia.de': ['Expedia'],
        'sorel.it': ['Columbia Sportswear'],
        'starbucks.com.br': ['Starbucks'],
        'decorist.com': ['Bed Bath & Beyond', 'decorist, inc'],
        'iqos.de': ['Philip Morris International'],
        'grandvictoriacasino.com': [],
        'rentacenter.com': ['Rent-A-Center'],
        'rejuvenation.com': ['Williams-Sonoma'],
        'chilisalaska.com': [],
        'pizzahut.com.tr': [],
        'marshallsonline.com': ['TJX Companies', 'Marshalls'],
        'freepeople.com': ['Urban Outfitters'],
        'iqos.co.nz': ['Philip Morris International'],
    }

    resp = api_client.post(
        '/merchant-mapper',
        data=json.dumps(list(test_set.keys())),
    )
    assert resp.status_code == 200
    results = json.loads(resp.data)

    correct_answers = 0
    for i, answers in enumerate(test_set.values()):
        if results[i]['Company Name'] in ('Domain Unreachable', 'No Match / Small Business') and not answers:
            # no answers
            correct_answers += 1
            continue

        if any(jaro_winkler(results[i]['Company Name'].lower(), answer.lower()) >= JW_DISTANCE for answer in answers):
            correct_answers += 1
            continue

    accuracy = correct_answers / len(test_set) * 100
    assert accuracy >= ACCEPTABLE_ACCURACY, f'{correct_answers} / {len(test_set)} -> {accuracy}%'
