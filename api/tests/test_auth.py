"""
© 2018 Alternative Data Group. All Rights Reserved.

REST API authentication tests

FIXME: Use requests-mock?
"""

from flask import json


class TestAuth:

    def test_valid_authentication(self, app, client):
        """ Test that authenticated call works, and that non authenticated call fails with an error. """

        resp = client.get(
            '/health',
            headers={
                'Accept': 'application/json',
                'X-3scale-proxy-secret-token': app.config['X_3SCALE_SHARED_SECRET']
            },
        )
        assert resp.status_code == 200
        data = json.loads(resp.data)
        assert "OK" == data['message']

    def test_missing_token(self, app, client):
        resp = client.get(
            '/health',
            headers={
                'Accept': 'application/json',
                # 'X-3scale-proxy-secret-token': app.config['X_3SCALE_SHARED_SECRET']
            },
        )
        assert resp.status_code == 401
        data = json.loads(resp.data)
        assert data == {
            'details': None,
            'error': 'Can not authenticate the request',
            'name': 'Authentication Error'
        }

    def test_empty_token(self, app, client):
        resp = client.get(
            '/health',
            headers={
                'Accept': 'application/json',
                'X-3scale-proxy-secret-token': ''
            },
        )
        assert resp.status_code == 401
        data = json.loads(resp.data)
        assert data == {
            'details': None,
            'error': 'Can not authenticate the request',
            'name': 'Authentication Error'
        }

    def test_invalid_token(self, app, client):
        resp = client.get(
            '/health',
            headers={
                'Accept': 'application/json',
                'X-3scale-proxy-secret-token': 'maybe random string will work?'
            },
        )
        assert resp.status_code == 401
        data = json.loads(resp.data)
        assert data == {
            'details': None,
            'error': 'Can not authenticate the request',
            'name': 'Authentication Error'
        }
