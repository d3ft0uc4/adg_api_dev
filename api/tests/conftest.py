"""
© 2018 Alternative Data Group. All Rights Reserved.

REST API tests config
"""

# import os

import pytest
# FIXME: We should probably be using this lib below instead of actually querying Bing during tests.
# import requests_mock

from flask import _request_ctx_stack
from flask_sqlalchemy import SQLAlchemy

from api.application import application
# from api.app.models import metadata


db = SQLAlchemy()


@pytest.fixture(scope='session')
def app(request):
    """ Application factory. """

    return application


@pytest.yield_fixture
def client(app):
    """ A Flask test client. An instance of :class:`flask.testing.TestClient` by default. """

    with app.test_client() as client:
        yield client


# @pytest.fixture(scope='session')
# def dbmodels(app):
#     """ TODO: What is this? """
#     with app.app_context():
#         metadata.create_all(db.engine)


@pytest.fixture
def request_ctx(app):
    """ The request context which contains all request relevant information, e.g. `session`, `g`,
    `flashes`, etc. """

    return _request_ctx_stack.top


@pytest.fixture(scope='function')
def dbtransaction(app, request, request_ctx, monkeypatch):
    """ Temporary DB transaction.

    Use this if you want to operate on the real database but don't want changes to actually affect
    it outside of this test. This works using SQLAlchemy transactions.

    Transactions made outside of the session scope are not rolled back. """

    with app.app_context():
        connection = db.engine.connect()
        transaction = connection.begin()

        # Patch Flask-SQLAlchemy to use our connection
        monkeypatch.setattr(db, 'get_engine', lambda *args: connection)

        # Explicitly remove the session so that we'll get a new session every time we go here.
        db.session.remove()

        def teardown():
            # try:
            #     connection.execute('SET CONSTRAINTS ALL IMMEDIATE')
            # except InternalError:
            #     # This is the case when we are doing something in the tests that we
            #     # expect it to fail by executing the statement above. In this case,
            #     # the transaction will be in an already failed state, executing
            #     # further SQL statements are ignored and doing so raises an
            #     # exception.
            #     pass

            transaction.rollback()
            connection.close()
        request.addfinalizer(teardown)

        return db


@pytest.fixture
def api_client(app, client):
    """ Simplified API client fixture, automatically adds json and necessary HTTP headers. """

    headers = {
       'Accept': 'application/json',
       'X-3scale-proxy-secret-token': app.config['X_3SCALE_SHARED_SECRET']
    }

    class ApiClient:
        """Performs API requests."""

        def get(self, url, **kwargs):
            """Sends GET request and returns the response."""
            return client.get(url, headers=headers, **kwargs)

        def post(self, url, **kwargs):
            """Sends POST request and returns the response."""
            return client.post(url, headers=headers, **kwargs)

        def delete(self, url, **kwargs):
            """Sends DELETE request and returns the response."""
            return client.delete(url, headers=headers, **kwargs)

    return ApiClient()


# @pytest.fixture
# def mock_bing_api(request):
#     """ Mock bing requests with prepared responses read from json files. """
#
#     def read_data_file(file_name):
#         """Helper method to read json file from the tests/data dir."""
#         data_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
#                                       'data', file_name)
#         with open(data_file_path) as json_data:
#             return json_data.read()
#
#     with requests_mock.Mocker(real_http=False) as req:
#         req.register_uri(
#             method=requests_mock.ANY,
#             url='https://api.cognitive.microsoft.com/bing/v7.0/search?q=NETFLIX.COM',
#             text=read_data_file('bing-netflix.json')
#         )
#         req.register_uri(
#             method=requests_mock.ANY,
#             url='https://api.cognitive.microsoft.com/bing/v7.0/search?q=WAL-MART%20INC',
#             text=read_data_file('bing-walmart.json')
#         )
#         yield req
#
#
# @pytest.fixture
# def mock_bing_api_empty(request):
#     """ Mock all bing search requests. """
#     with requests_mock.Mocker(real_http=False) as req:
#         req.register_uri(
#             method=requests_mock.ANY,
#             url='https://api.cognitive.microsoft.com/bing/v7.0/search',
#             text=json.dumps({
#                 'webPages': {
#                     'totalEstimatedMatches': 0,
#                     'value': [],
#                     'webSearchUrl': 'https://www.bing.com/search?q=test_non_existing+brands'
#                 },
#                 '_type': 'SearchResponse',
#                 'queryContext': {
#                     'originalQuery': 'test_non_existing brands'
#                 },
#                 'rankingResponse': {
#                   'mainline': {
#                      'items': []
#                   }
#                 }
#             }),
#         )
#         yield req
