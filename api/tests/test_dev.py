"""
© 2018 Alternative Data Group. All Rights Reserved.

REST API development env. tests for /bane_fwfm endpoint

FIXME: Use requests-mock?
"""

import json

from api.app.config import MAX_INPUTS
from packages.utils import similar


class TestDev:
    # ---- Pii ----
    def test_pii_post_method(self, api_client):
        """ The endpoint should return the corresponding Flask message, and basic usage info. """

        resp = api_client.get('/pii-ident')

        assert resp.status_code == 405  # Method Not Allowed
        assert json.loads(resp.data) == {
            "name": "Method Not Allowed",
            "error": "The method is not allowed for the requested URL."
        }

    def test_pii_empty_request(self, api_client):
        """ When called with no POST data, the endpoint should return an error message, and basic usage info. """

        resp = api_client.post('/pii-ident')

        assert resp.status_code == 422  # Unprocessable Entity
        data = json.loads(resp.data)

        assert data['name'] == "Validation Error"
        assert data["error"] == "Request parameters validation error"

    def test_pii_invalid_request(self, api_client):
        """ When the payload contains an unexpected structure,
        the endpoint should return an error message, and basic usage info. """

        resp = api_client.post(
            '/pii-ident',
            data=json.dumps({'unexpected_key': 'unexpected_value'})
        )

        assert resp.status_code == 422  # Unprocessable Entity
        assert json.loads(resp.data) == {
            "name": "Validation Error",
            "error": "Request parameters validation error",
            "details": "Not a valid list.",
        }

    def test_pii_too_long_inputs(self, api_client):
        """ When any input is longer then MAX_INPUT_LENGTH,
        the endpoint should return an error message, and basic usage info. """
        from api.app.config import MAX_INPUT_LENGTH

        raw_input = 'o' * (MAX_INPUT_LENGTH + 1)
        resp = api_client.post(
            '/pii-ident',
            data=json.dumps([raw_input])
        )

        assert resp.status_code == 422  # Unprocessable Entity
        assert json.loads(resp.data) == {
            "name": "Validation Error",
            "error": "Request parameters validation error",
            "details": f'Input {raw_input} length is bigger than max possible length ({MAX_INPUT_LENGTH})',
        }

    def test_pii_empty_inputs(self, api_client):
        """ When the payload is correct but only includes empty string(s),
        the endpoint should return an error message, and basic usage info. """
        # FIXME: Combine with `test_invalid_request`?

        # Single empty string:
        resp = api_client.post(
            '/pii-ident',
            data=json.dumps([''])
        )
        assert resp.status_code == 422  # Unprocessable Entity
        assert json.loads(resp.data) == {
            "name": "Validation Error",
            "error": "Request parameters validation error",
            "details": "Empty inputs not allowed",
        }

    def test_pii_trivial_inputs(self, api_client):
        """ When called with single valid input in POST data, return expected result. """

        resp = api_client.post(
            '/pii-ident',
            data=json.dumps(['Some string 125$ which does not contain private info'])
        )

        assert resp.status_code == 200
        data = json.loads(resp.data)
        assert data[0]['PII Detected'] is False

        resp = api_client.post(
            '/pii-ident',
            data=json.dumps(['Some string with credit card 1234-5678-1234-5678in the middle'])
        )

        assert resp.status_code == 200
        data = json.loads(resp.data)
        assert data[0]['PII Detected'] is True

    # ---- Mapper ----
    def test_mapper_get_method(self, api_client):
        """ The endpoint should return the corresponding Flask message, and basic usage info. """

        resp = api_client.get('/merchant-mapper')

        assert resp.status_code == 405  # Method Not Allowed
        assert json.loads(resp.data) == {
            "name": "Method Not Allowed",
            "error": "The method is not allowed for the requested URL."
        }

    def test_mapper_empty_request(self, api_client):
        """ When called with no POST data, the endpoint should return an error message, and basic usage info. """

        resp = api_client.post('/merchant-mapper')

        assert resp.status_code == 422  # Unprocessable Entity
        data = json.loads(resp.data)

        assert data['name'] == "Validation Error"
        assert data["error"] == "Request parameters validation error"

    def test_mapper_invalid_request(self, api_client):
        """ When the payload contains an unexpected structure,
        the endpoint should return an error message, and basic usage info. """

        resp = api_client.post(
            '/merchant-mapper',
            data=json.dumps({'unexpected_key': 'unexpected_value'})
        )

        assert resp.status_code == 422  # Unprocessable Entity
        assert json.loads(resp.data) == {
            "name": "Validation Error",
            "error": "Request parameters validation error",
            "details": "Not a valid list.",
        }

    def test_mapper_too_many_inputs(self, api_client):
        resp = api_client.post(
            '/merchant-mapper',
            data=json.dumps(['walmart'] * 1000)
        )
        assert resp.status_code == 422
        assert json.loads(resp.data) == {
            "name": "Validation Error",
            "error": "Request parameters validation error",
            "details": f"Too many input strings, max {MAX_INPUTS}",
        }

    def test_mapper_too_long_inputs(self, api_client):
        """ When any input is longer then MAX_INPUT_LENGTH,
        the endpoint should return an error message, and basic usage info. """
        from api.app.config import MAX_INPUT_LENGTH

        bad_input = 'o'*(MAX_INPUT_LENGTH+1)
        resp = api_client.post(
            '/merchant-mapper',
            data=json.dumps([bad_input])
        )

        assert resp.status_code == 422  # Unprocessable Entity
        assert json.loads(resp.data) == {
            "name": "Validation Error",
            "error": "Request parameters validation error",
            "details": f'Input {bad_input} length is bigger than max possible length ({MAX_INPUT_LENGTH})',
        }

    def test_mapper_empty_inputs(self, api_client):
        """ When the payload is correct but only includes empty string(s),
        the endpoint should return an error message, and basic usage info. """
        # FIXME: Combine with `test_invalid_request`?

        # Single empty string:
        resp = api_client.post(
            '/merchant-mapper',
            data=json.dumps([''])
        )
        assert resp.status_code == 422  # Unprocessable Entity
        assert json.loads(resp.data) == {
            "name": "Validation Error",
            "error": "Request parameters validation error",
            "details": "Empty inputs not allowed",
        }

    #     # Multiple empty strings:
    #     resp = api_client.post(
    #         '/merchant-mapper',
    #         data=json.dumps(['', '', ''])
    #     )
    #     assert resp.status_code == 422  # Unprocessable Entity
    #     assert json.loads(resp.data) == {
    #         "name": "Validation Error",
    #         "error": "Request parameters validation error",
    #         "details": "Empty inputs not allowed",
    #     }

    def test_mapper_2_trivial_inputs_correct(self, api_client):
        """ When called with single valid input in POST data, return expected result
        with correct company name, high confidence, and ticker/exchange info. """

        resp = api_client.post(
            '/mapper',
            data=json.dumps(['Walmart'])
        )

        # FIXME: Hardcoded to August 2018 results below.
        assert resp.status_code == 200
        data = json.loads(resp.data)
        # FIXME: Use string distance to corresponding query strings?
        assert similar(data[0]['Company Name'], 'Walmart', thld=0.9)
        assert data[0]['Ticker'] == 'WMT'
        assert data[0]['Exchange'] in ['NYSE', 'NASDAQ']

        # test that a domain with the word apple in it, but is not apple, does not map to apple
        # resp = api_client.post(
        #     '/domain-mapper',
        #     data=json.dumps(['appleticket.com'])
        # )
        #
        #
        # assert resp.status_code == 200
        # data = json.loads(resp.data)
        # assert data[0]['Company Name'] == 'No Match / Small Business'
