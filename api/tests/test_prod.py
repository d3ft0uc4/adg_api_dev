"""
© 2018 Alternative Data Group. All Rights Reserved.

REST API production env. tests for /bane_fwfm endpoint

FIXME: Use requests-mock?
"""

# import datetime
import json
import random
import string
# import time
from packages.utils import similar
# from mapper.response import NO_MATCH
from unofficializing.unofficializing import make_unofficial_name


class TestProd:

    @staticmethod
    def gen_random_string(length=16):
        return ''.join(random.choices(string.ascii_uppercase + string.digits + ' ,', k=length))

    # FIXME: Test assert resp.status_code == 413  # Payload Too Large ?

    # def test_execution_time_and_nonsense(self, api_client):
    #     """
    #     Execution time: check whether a response is returned in a reasonable time.
    #     Nonsense: When called with a random string input in POST data, return expected result.
    #     """
    #     # FIXME: Check https://pypi.org/project/pytest-timeout/

    #     max_exec_time = datetime.timedelta(seconds=30)

    #     start_time = time.time()
    #     resp = api_client.post(
    #         '/merchant-mapper',
    #         data=json.dumps([self.gen_random_string()])
    #     )
    #     end_time = time.time()

    #     assert resp.status_code == 200
    #     assert datetime.timedelta(seconds=end_time - start_time) <= max_exec_time

    #     data = json.loads(resp.data)[0]
    #     assert NO_MATCH == data['Company Name']

    def test_json_structure(self, api_client):
        """ Check the JSON structure and their order in the REST response by sending a random input string. """

        resp = api_client.post(
            '/merchant-mapper',
            data=json.dumps(['USAA.COM PMT - THANK YOU SAN ANTONIO  TX']),
        )
        assert resp.status_code == 200
        data = json.loads(resp.data)[0]
        correct_keys = ['Original Input', 'Company Name', 'Aliases', 'Confidence', 'Confidence Level',
                        'Alternative Company Matches', 'Related Entities', 'Ticker', 'Exchange',
                        'Majority Owner', 'FIGI', 'Websites']
        assert set(data.keys()) == set(correct_keys)

        resp = api_client.post(
            '/merchant-mapper-plus',
            data=json.dumps(['USAA.COM PMT - THANK YOU SAN ANTONIO  TX']),
        )
        assert resp.status_code == 200
        data = json.loads(resp.data)[0]
        assert set(data.keys()) == set(correct_keys + ['Companies Retrieved'])

    def test_3_trivial_inputs_correct(self, api_client):
        """ When called with single valid input in POST data, return expected result
        with correct company name, high confidence, and ticker/exchange info. """

        resp = api_client.post(
            '/merchant-mapper',
            data=json.dumps(['Walmart'])
        )
        assert resp.status_code == 200
        data = json.loads(resp.data)[0]
        assert similar(data['Company Name'], 'Walmart', thld=0.9)
        assert data['Ticker'] == 'WMT'
        assert data['Exchange'] == 'NYSE'
        assert data['Related Entities']

        resp = api_client.post(
            '/merchant-mapper',
            data=json.dumps(['NETFLIX'])
        )
        assert resp.status_code == 200
        data = json.loads(resp.data)[0]
        assert similar(data['Company Name'], 'Netflix', thld=0.9)
        assert data['Ticker'] == 'NFLX'
        assert data['Exchange'] == 'NASDAQ'

        resp = api_client.post(
            '/merchant-mapper',
            data=json.dumps(['Facebook'])
        )
        assert resp.status_code == 200
        data = json.loads(resp.data)[0]
        assert similar(data['Company Name'], 'Facebook', thld=0.9)
        assert data['Ticker'] == 'FB'
        assert data['Exchange'] == 'NASDAQ'

    # def test_3_small_merchant_inputs(self, api_client):
    #     """ When called with single valid input in POST data, return expected result,
    #     with no company names and low confidence. """
    #     # TODO: Check for suggestions?

    #     resp = api_client.post(
    #         '/merchant-mapper',
    #         data=json.dumps([
    #             'USA*PHOTOBOOTH VEND    DALLAS       TX',
    #             'ALISO CREEK SHELL        ALISO VIEJO  CA',
    #             # 'THE HIGH NOONER          SPOKANE      WA'  # TODO: reenable when works
    #         ])
    #     )

    #     assert resp.status_code == 200
    #     data = json.loads(resp.data)
    #     assert NO_MATCH == data[0]['Company Name']
    #     assert data[0]['Confidence'] is None
    #     assert NO_MATCH == data[1]['Company Name']
    #     assert data[1]['Confidence'] is None
    #     # assert NO_MATCH == data[2]['Company Name']
    #     # assert data[2]['Confidence'] is None

    # def test_unreachable_domain(self, api_client):
    #     """ When domain does not response, return 'No Match / Samll Business' in company name """

    #     resp = api_client.post(
    #         '/domain-mapper',
    #         data=json.dumps(['some-unreachable-domain-888.com']),
    #     )

    #     assert resp.status_code == 200
    #     data = json.loads(resp.data)
    #     assert data[0]['Company Name'] == NO_MATCH
    #     assert data[0]['Confidence'] is None

    def test_miniapp_default_inputs(self, api_client):
        """ Checks whether miniapp works as expected """

        answers = {
            # 'Cheesy Crust Pizza': {
            #     'Company Name': ['Pizza Hut', 'Yum China Holdings, Inc.'],
            #     'Ticker': 'YUM',
            # },
            'Old Nvy store #1234': {
                'Company Name': ['Old Navy', 'The Gap, Inc.'],
                'Ticker': 'GPS',
            },
            # the following sometimes doesnt work bc nike.com blocks us
            # 'Purchase: Air Jordans 2018': {
            #     'Company Name': ['Nike, Inc.'],
            #     'Ticker': 'NKE',
            # },
            'MINOT BK 5705 MINOT AFB ND~~08888~~xxxxxxxx': {
                'Company Name': ['Burger King', 'Restaurant Brands International Inc.'],
                'Ticker': 'QSR',
            },
            # TODO: reenable when works
            # 'PHILASIXERS/COMCASTTIX 877-263-9372 PA': {
            #     'Company Name': ['Comcast'],
            #     'Ticker': 'CMCSA',
            # },
            'pokerstars.pt': {
                'Company Name': ['PokerStars', 'Poker Stars', 'The Stars Group Inc.'],
                'Ticker': 'TSG',
            },
            'aws.com': {
                'Company Name': ['Amazon.com, Inc.', 'amazon web services'],
                'Ticker': 'AMZN',
            },
            'fairfield.marriott.com': {
                'Company Name': ['Fairfield Inn', 'Marriott International'],
                'Ticker': 'MAR',
            },
            # TODO: reenable when works
            # 'job-kfc.net': {
            #     'Company Name': ['KFC'],
            # },
        }

        for raw_input, answer in answers.items():
            resp = api_client.post(
                '/mapper',
                data=json.dumps([raw_input])
            )

            assert resp.status_code == 200
            data = json.loads(resp.data)[0]

            result = make_unofficial_name(data['Company Name'])
            answers = answer['Company Name']
            assert any([
                similar(result, make_unofficial_name(name), thld=0.9)
                for name in answers
            ]), f'Result "{result}" didn\'t match any of {answers}'
            assert data['Ticker'] == answer['Ticker']

    def test_1_domain(self, api_client):
        """ Check whether domain mapper works. """

        resp = api_client.post(
            '/domain-mapper',
            data=json.dumps(['Yahoo.com'])
        )

        assert resp.status_code == 200
        data = json.loads(resp.data)[0]
        assert any(
            similar(data['Company Name'], answer, thld=0.9)
            for answer in ['Verizon Communications Inc.', 'Yahoo', 'Verizon Media']
        )
        assert data['Ticker'] == 'VZ'
        assert data['Exchange'] in ['NYSE', 'NASDAQ']
