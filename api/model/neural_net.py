import os
from collections import OrderedDict
from typing import Tuple
from uuid import UUID  # noqa: F401

from flask import Flask
from keras.models import load_model
import numpy as np

from neural_net.encoders import OneHotCharEncoder, OneHotCharSettingsDefault


class NeuralNet(object):
    """The neural net model wrapper."""

    def __init__(self):
        """Constructor."""
        self._model = None
        # Note: the max words and other settings come from
        # from the neural_net/mlp.py, this is
        # the default setting for the training dataset, where
        # we have max_words * max_word_length * num_chars = 5 * 25 * 26 = 3250
        # The other overlaping part is the neural net structure in the `build` method.
        # TODO:
        # - We need to guarantee that training settings match the application settings,
        # The self._max_words, self._max_word_length, num_chars and possible list of targets
        # should either be fixed somewhere in the DB or maybe saved to the file and
        # loaded along with the model (but we still want to use DB IDs for match targets).
        self._max_words = 5
        self._max_word_length = 25
        num_chars = 26
        self._x_dim = self._max_words * self._max_word_length * num_chars
        self._targets = OrderedDict()  # type: OrderedDict[str, UUID]
        self._app = None

    def init_app(self, app: Flask) -> None:
        """Initialize the `NeuralNet` extension.

        Args: The Flask application.
        """
        self.app = app
        model_path = "neural_net/data"
        self._model = load_model(os.path.join(model_path, 'model.h5'))
        self._model.load_weights(os.path.join(model_path, 'best_weights.hdf5'))

        settings = OneHotCharSettingsDefault(model_path)
        self._encoder = OneHotCharEncoder(settings)
        self._encoder.load(model_path)
        # Calling _model.predict() on the server raises, looks like the multithreading issue,
        # the model is the global variable, which is immutable except on the initialization
        # stage.
        # Calling _model._make_predict_function seems to help.
        # The error is:
        #  Tensor Tensor("dense_5/Softmax:0", shape=(?, 1399), dtype=float32)
        #  is not an element of this graph.
        # See: model._make_predict_function(), the hack-o-fix:
        # Related:
        # https://github.com/jaara/AI-blog/issues/2
        # https://github.com/keras-team/keras/issues/6124
        # https://stackoverflow.com/questions/43136293/running-keras-model-for-prediction-in-multiple-threads
        self._model._make_predict_function()

    def predict(self, input_string: str) -> Tuple[str, float]:
        """Predict the target for the given `input_string`.

        Args:
            input_string: the input to predict the target for.

        Returns: Tuple of target uid, target string and prediction confidence
            (0...1 value).
        """
        X = np.array([input_string])
        X = self._encoder.encode_X(X)
        # A dataframe of (input data size X number of unique targets),
        # where for each input item we have an array of probabilities for each target.
        prediction = self._model.predict(X, verbose=0)
        # Get the maximum probability for each input:
        [confidence] = prediction.max(axis=1)
        # Get the position (=target) for max probability.
        y_hat = prediction.argmax(axis=1)
        # Transform position numbers to target names.
        [prediction] = self._encoder.decode_y(y_hat)
        return prediction, round(float(confidence), 3)
