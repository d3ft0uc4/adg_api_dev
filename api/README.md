© 2018 Alternative Data Group. All Rights Reserved.

# Merchant Recognition Demo REST API
This application is meant as a web service based mainly on the Bane and FM/FW tools in this repo, along with several others.
The app and its tools have been developed and tested with Python 3.6

## Setup

* Run [init-venv.sh](../init-venv.sh) from the root folder to create the virtual environment and install this project's
dependencies. See the [root README](../README.md) for more info.

## Local Configuration

This app reads the `SQLALCHEMY_DATABASE_URI` and `X_3SCALE_SHARED_SECRET` environment variables, as well as additional
optional constants. (See [api.app.config](api/config.py).) You may set them in your system or in an
[.env file](https://pypi.org/project/python-dotenv/#usages) **located in the app/ sub directory**. E.g:

```sh
SQLALCHEMY_DATABASE_URI="mysql+pymysql://dev:pass@localhost:3306/altdg_merchtag"
X_3SCALE_SHARED_SECRET="test-shared-secret"
```

## Run Application Locally Manually

Command from the repo root:

```sh
FLASK_APP=api/application.py venv/bin/python -m flask run
```

> See also the Tools section below.

While running flask server, you may perform requests to localhost:5000 (via postman / curl / etc) e.g:
```sh
curl -X POST -H'X-3scale-proxy-secret-token: test-shared-secret' -H'Content-Type: application/json' http://localhost:5000/merchant-mapper -d '{"input": ["Wal*Mart", "NETFLIX"]}'
```

Results example:

```json
[
    {
        "Company Name": "Walmart",
        "Aliases": [
            "walmart",
            "Wal-Mart Stores Inc",
            "Walmart",
            "Walmart Inc"
        ],
        "Confidence": 1.981916081905365,
        "Confidence Level": "High",
        "Ticker": "WMT",
        "Exchange": "NYQ",
        "Alternative Company Matches": [],
        "Related Entities": [
            {
                "Name": "Money Mart",
                "Closeness Score": 1.4812278747558594
            },
            ...
        ]
    }
]
```

### CSV output

The API may output intermediate results into `api/csv` directory.
To enable CSV output, define "CSV_OUTPUT" env var before launching Flask.

## Tests

We use PyTest for automated tests which are found in [api/tests/](api/tests/).

A local MySQL server must be running for local tests. Setup the mysql `dev` user for tests once:

```sql
CREATE USER 'dev'@'localhost' IDENTIFIED BY 'pass';
GRANT ALL PRIVILEGES ON *.* TO 'dev'@'localhost';
FLUSH PRIVILEGES;
```

Run local tests with:

```sh
cd api && make test-local; cd ..
```

or running the linters and `pytest` module directly:

```sh
mysql -udev -ppass -e 'drop database if exists altdg_tests;'
mysql -udev -ppass -e 'create database altdg_tests;'
cd api
flake8
mypy --ignore-missing-imports .
cd ..
venv/bin/python -m pytest -v api/tests
```

> See other `pytest` commands in the [Makefile](Makefile)

To [automate flake8 linting](http://flake8.pycqa.org/en/latest/user/using-hooks.html) at least, **all developers should** setup a Git pre-commit hook in file
.git/hooks/pre-commit:

```sh
flake8 --install-hook git
```

> FIXME: Link only api/ dir?
> IDEA: See https://pre-commit.com/ to add both mypy and flake8

## Other Tools

The `Makefile` contains additional helper commands such as:

- `make local` - run the app in with local database
- `make production` - run with production database (the production database connection string is expected to be in the `SQLALCHEMY_DATABASE_URI_PRODUCTION` environment variable)
- `make test` - for GitLab CI tests

## Deployment

We use AWS ElasticBeanstalk for managing and deploying the web application, and a 3scale proxy for REST API authentication.
For further information about deployment see the repo root's [README.md](../README.md) file.
