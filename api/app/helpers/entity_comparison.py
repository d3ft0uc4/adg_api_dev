"""
© 2018 Alternative Data Group. All Rights Reserved.

This module contains helper functions for comparing entities (targets and merchant string).

Usage example:
    from api.app.helpers.entity_comparison import MerchantComparator

    ec = EntityComparator()
    merchant = 'Internation Business Company'
    target = 'ibm'

    print(ec.compare(merchant, target, method='fwfm'))
    print(ec.compare(merchant, target, method='neural_net'))

Usage from console (first pass bane_winner, then fwfm_winner) examples:
    python3 -m api.app.helpers.entity_comparison "Walmart Inc." wal-mart
    python3 -m api.app.helpers.entity_comparison "Walmart Inc." 'wal*mart'
"""
from itertools import product
from operator import itemgetter
from typing import List, Tuple

from nltk.metrics.distance import edit_distance
from jellyfish import jaro_winkler

from packages.utils import with_timer
from api.app.config import LEV_MIN_SCORE, JW_MIN_SCORE, \
    COMP_BANE_FWFM_THLD, COMP_FWFM_THLD, COMP_FWFM_MATCH_SCORE, COMP_NO_MATCH_SCORE


class EntityComparator:
    """ Class which provides several methods to compare Merchant (plain string) and Target (entity from database). """

    @staticmethod
    def _levenshtein_pct(input1, input2):
        """
        Returns percentual similarity of two strings using Levenshtein distance.
        FIXME: p.s. there's jellyfish.levenshtein_distance but the output needs to be normalized to 0.0-1 range.

        Returns:
            Similarity of inputs in 0-1 range. Identical strings return 1, totally different strings return 0.
        """
        input1 = input1.lower()
        input2 = input2.lower()
        max_length = max(len(input1), len(input2))
        return 1 - edit_distance(input1, input2) / max_length

    @classmethod
    @with_timer
    def _compare_by_str_dist(cls, item: dict, method: str = 'jaro-winkler',
                             levenshtein_thld: float = LEV_MIN_SCORE,
                             jaro_winkler_thld: float = JW_MIN_SCORE):
        """
        Normalizes input strings (from BANE and FW/FM) and compares them.

        Args:
            item: dict with `bane_winners` and `fwfm_winners` keys
            method: Which method to use for string comparison. Methods:
                - levenshtein: Uses Levenshtein distance between inputs and returns True if its threshold is passed
                - jaro-winkler: Uses Jaro-Winkler distance between inputs and returns True if its threshold is passed
            levenshtein_thld: When method == 'levenshtein', this attr defines threshold to treat strings as equal
            jaro_winkler_thld: When method == 'jaro-winkler', this attr defines threshold to treat strings as equal

        Returns:
            Updates `items` in-place, adds keys:
                'comp_str_dist_candidates' - all winners pairs comparison
                'comp_match' - whether string distance comparison method succeeded
                'comp_score' - string distance score
                'bane_winner', 'fwfm_winner', ... - best winners (winners with highest `comp_score`)
        """

        if method == 'levenshtein':
            comparator = cls._levenshtein_pct
            threshold = levenshtein_thld

        elif method == 'jaro-winkler':
            comparator = jaro_winkler
            threshold = jaro_winkler_thld

        else:
            raise ValueError('[CRIT] Unknown method: {}'.format(method))

        bane_winners = item.get('bane_winners', [])
        fwfm_winners = item.get('fwfm_winners', [])

        if not bane_winners or not fwfm_winners:
            return

        # list of tuples (bane_winner, fwfm_winner)
        pairs: List[Tuple[dict, dict]] = product(bane_winners, fwfm_winners)

        # convert pairs to dicts with scores
        candidates = []
        for bane_winner, fwfm_winner in pairs:
            score = comparator(bane_winner['winner'], fwfm_winner['winner'])
            match = score >= threshold

            candidates.append({
                **{f'bane_{key}': value for key, value in bane_winner.items()},
                **{f'fwfm_{key}': value for key, value in fwfm_winner.items()},
                'comp_score': score if match else COMP_NO_MATCH_SCORE,
                'comp_match': match,
                'comp_match_source': 'str_dist',
            })

        candidates = sorted(candidates, key=itemgetter('comp_score'), reverse=True)
        item['comp_str_dist_candidates'] = candidates
        candidates = [candidate for candidate in candidates if candidate['comp_match']]

        if not candidates:
            return

        item.update(candidates[0])

    @with_timer
    def _compare_by_fwfm(self, item: dict):
        """
        This method runs `bane_winner` (which is just a string) through FW/FM process, thus getting a "target".
        If this target is the same as original Fw\Fm winner, then we are pretty sure that Bane winner
        and Fw/Fm winner are the same.

        Updates `items` in-place, adds keys:
            'comp_bane_fwfm' - results of running Bane's winner through FW/FM
            'comp_match' - whether there is a match
            `comp_score` - reward for bane->fw/fm vs fw/fm comparison match

        Args:
            items: list of dicts containing `bane_winner`, `fwfm_winner` keys
        """
        from find_winner.fw_fm import FWFM

        if not item['bane_winners'] or not item['fwfm_winners']:
            return

        # take best (first) winners from Bane and Fw/Fm
        bane_winner = item['bane_winners'][0]
        fwfm_winner = item['fwfm_winners'][0]

        # run bane winner through Fw/Fm and filter winners with high score
        bane_fwfm_winners = [
            winner for winner in FWFM().find_winner([bane_winner['winner']])[bane_winner['winner']]
            if winner['norm_score'] >= COMP_BANE_FWFM_THLD
        ]

        item['comp_bane_fwfm'] = bane_fwfm_winners
        # if no Bane->Fw/Fm winners -> nothing to compare
        if not bane_fwfm_winners:
            return

        # check whether any of Bane->Fw/Fm winners match original Fw/Fm winner
        fwfm_winner_lower = fwfm_winner['winner'].lower()
        match = any(
            jaro_winkler(
                bane_fwfm_winner['winner'].lower(),
                fwfm_winner_lower,
            ) >= COMP_FWFM_THLD
            for bane_fwfm_winner in bane_fwfm_winners
        )

        if match:
            item.update({
                'comp_match': match,
                'comp_score': COMP_FWFM_MATCH_SCORE if match else COMP_NO_MATCH_SCORE,
                'comp_match_source': 'fwfm',
                **{f'bane_{key}': value for key, value in bane_winner.items()},
                **{f'fwfm_{key}': value for key, value in fwfm_winner.items()},
            })

    def compare_and_select_winners(self, item: dict):
        """
        Compares Bane winners to FW/FM winners. The idea is to check that the merchant entity (merchant, Bane's output)
         and target entity (FW/FM's output) represent the same company.

        Args:
            item: Dict containing Bane and Fw/Fm winners.

        Returns:
            Updates `item` in-place: adds keys
            'comp_match' - whether comparison succeeded
            'comp_score' - comparison score
            'bane_winner' - selected bane winner
            'bane_score'
            'bane_norm_score'
            'fwfm_winner' - selected fwfm winner
            'fwfm_score'
            'fwfm_norm_score'
        """
        # TODO: refactor all this
        bane_winners = item.get('bane_winners', [])
        fwfm_winners = item.get('fwfm_winners', [])

        if bane_winners and fwfm_winners:
            self._compare_by_str_dist(item)
            if item.get('comp_match'):
                return

            # if item winners didn't match by str dist, try bane->fwfm comparison on best winners
            self._compare_by_fwfm(item)
            if item.get('comp_match'):
                return

        if bane_winners:
            item.update({f'bane_{key}': value for key, value in bane_winners[0].items()})

        if fwfm_winners:
            item.update({f'fwfm_{key}': value for key, value in fwfm_winners[0].items()})


if '__main__' == __name__:
    raise NotImplementedError()

    # import argparse
    #
    #
    # parser = argparse.ArgumentParser()
    # parser.add_argument('merchant')
    # parser.add_argument('target')
    # args = parser.parse_args()
    #
    # items = [{
    #     'bane_winner': args.merchant,
    #     'fwfm_winner': args.target,
    # }]
    # EntityComparator().compare(item)
    # print(items)
