"""
© 2018 Alternative Data Group. All Rights Reserved.

REST API request authorization logic
"""

from functools import wraps

from flask import current_app, request

from api.app.exceptions import APIException


class APIAuthError(APIException):
    """ Raised for unauthorized requests to API. """

    def __init__(self):
        super().__init__(message='Can not authenticate the request')
        self.name = 'Authentication Error'
        self.code = 401


def auth_required(f):
    """ Fn. decorator to enforce API authorization. """

    @wraps(f)
    def decorated(*args, **kwargs):
        token = request.headers.get('X-3scale-proxy-secret-token')

        if token == current_app.config['X_3SCALE_SHARED_SECRET']:
            return f(*args, **kwargs)

        raise APIAuthError()

    return decorated
