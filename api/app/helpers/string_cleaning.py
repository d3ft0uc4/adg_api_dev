"""
© 2018 Alternative Data Group. All Rights Reserved.

This module contains helper classes for cleaning raw input strings.

Usage example:
    from api.app.helpers.string_cleaning import RawInputStrCleaner

    raw_inputs = ['CVS/PHARMACY #07843      WINCHESTER   VA', 'MET FOOD STATEN ISLAND NY']

    cleaner = RawInputStrCleaner()
    clean_inputs = cleaner.rule_based(raw_inputs)
    print(clean_inputs)
    print(cleaner.neural_net(raw_inputs))

    # TODO: Update docstring when NonMerchantStr.regex_based is made bulk.
    merch_detect = NonMerchantStr()
    print(merch_detect.regex_based(clean_inputs[0]))

Usage from console:
    python3 -m api.app.helpers.string_cleaning "CVS/PHARMACY #07843 WINCHESTER VA" \
                                                         "MET FOOD STATEN ISLAND NY" \
                                                         --method="[rule_based|neural_net|torch]"

Test in bulk:
    tr '\n' '\0' < ../data/QA/200_random_raw_strings.txt | xargs \
        -0 python3 -m api.app.helpers.string_cleaning --method="neural_net" --csv=1
"""

import re
import tldextract
from typing import Tuple, Optional
import logging

from bing.bing_web_core import BingWebCore

from cc_cleaner.clean_cc import CCCleaner
from domain_tools.parked import is_parked
from domain_tools.ssl import SSLCertificateRetrieval
from domain_tools.redirect import follow_redirects
from domain_tools.whois import WhoisInfoRetrieval
from domain_tools.utils import is_alive
from domain_tools.clean import get_domain
# from torch_cleaner.pytorch_string_cleaner import PyTorchStringCleaner
from keras.models import load_model
from neural_net.merchant_name.models import MerchantExtractor

logger = logging.getLogger(f'adg.{__name__}')


class BaseCleaner:
    """ A class to perform very basic cleaning - stripping raw input string """

    def __str__(self):
        return self.__class__.__name__

    def __call__(self, item: dict) -> bool:
        """
        Modifies `item` in-place, adding `clean_input`, `cleaner_score` and `cleaner` keys.

        Args:
            item: dict with `raw_input` key

        Returns:
            True if cleaning succeeded, False otherwise.
        """

        assert 'raw_input' in item

        item.update({
            'cleaner': self,
            'cleaner_score': None,
            'clean_input': item['raw_input'].strip(),
        })

        # do not further process empty input
        if not item['clean_input']:
            return False

        return True


class BaseDomainCleaner(BaseCleaner):
    """
    A class to perform basic domain cleaning:
        - remove `http[s]://`
        - remove everything after `/`
    """

    def __init__(self, check_alive: bool = True, check_parked: bool = True):
        """
        Args:
            check_alive: whether to check domain for being alive
            check_parked: whether to check domain for being parked
        """
        self.check_alive = check_alive
        self.check_parked = check_parked

    def __call__(self, item: dict) -> bool:
        if not super().__call__(item):
            return False

        item['cleaner'] = self
        item['clean_input'] = get_domain(item['clean_input'])

        if not item['clean_input']:
            return False

        if self.check_alive:
            item['is_alive'] = is_alive(item['clean_input'])
            if not item['is_alive']:
                return False

        if self.check_parked:
            item['is_parked'] = is_parked(item['clean_input'])

        return True


class DomainCleaner(BaseDomainCleaner):
    """ A class to clean raw input (domain name) using any of SSL, Whois or TLD extraction cleaners """

    def __init__(self, kind: str, follow_redirects: bool = True, check_alive: bool = True):
        """
        Initialize cleaner instance.

        Args:
            kind: cleaning type - [ssl|whois|tld]
            follow_redirects: whether to follow domain's redirects or just process "as is";
                              cleaner will be called on raw input only if redirected input is different
        """

        super().__init__(check_alive=check_alive)

        # TODO: Validate `kind` param.
        self.kind = kind

        self.follow_redirects = follow_redirects

    def __str__(self):
        return f'{self.kind} {self.__class__.__name__}'

    def __call__(self, item: dict) -> bool:
        """
        Launch cleaning process and updates `item` dict.
        Modifies `item` in-place

        Args:
            item: dict {'raw_input': ..., ...}

        Returns:
            True if cleaning succeeded, False otherwise.
        """
        if not super().__call__(item):
            return False

        item['cleaner'] = self

        redirected = follow_redirects(item['clean_input'])
        if self.follow_redirects:
            item['clean_input'] = redirected
        else:
            # quit if original input is the same as redirected
            if item['clean_input'] == redirected:
                item['clean_input'] = None
                item['cleaner_score'] = None
                return False

        clean_method = getattr(self, f'{self.kind}_clean')
        item['clean_input'], item['cleaner_score'] = clean_method(item['clean_input'])

        logger.debug(f'Cleaned {item["raw_input"]} -> {item["clean_input"]}')

        if not item['clean_input']:
            return False

        return True

    @staticmethod
    def whois_clean(domain: str) -> Tuple[str, Optional[float]]:
        assert domain
        return WhoisInfoRetrieval.get_organization(domain), None

    @staticmethod
    def ssl_clean(domain: str) -> Tuple[str, Optional[float]]:
        assert domain
        return SSLCertificateRetrieval.get_organization(domain), None

    @staticmethod
    def tld_clean(domain: str) -> Tuple[str, Optional[float]]:
        assert domain
        return tldextract.extract(domain).domain, None


class MerchantCleaner(BaseCleaner):
    """ A class to clean raw input (transaction string) using any of SSL, Whois or TLD extraction cleaners """

    # torch_cleaner = PyTorchStringCleaner(
    # data_dir='torch_cleaner/data',
    # model_dir='torch_cleaner/experiments/base_model',
    # restore_file='best',
    # )
    cc_cleaner = CCCleaner()  # pre-init CCleaner

    start_index_model = load_model('neural_net/data/raw_cc_start_index_model_2.h5')

    end_index_model = load_model('neural_net/data/raw_cc_end_index_model_2.h5')
    # prepare models for multiprocessing (https://github.com/keras-team/keras/issues/6124)
    start_index_model._make_predict_function()
    end_index_model._make_predict_function()

    merchant_extractor = MerchantExtractor(start_index_model, end_index_model)

    def __init__(self, kind: str):
        self.kind = kind

    def __str__(self):
        return f'{self.kind} {self.__class__.__name__}'

    def __call__(self, item: dict) -> bool:
        """
        Launch cleaning process and updates `item` dict.

        Args:
            item: dict {'raw_input': ..., ...}

        Returns:
            Modifies `item` in-place
        """
        if not super().__call__(item):
            return False

        item['cleaner'] = self

        clean_method = getattr(self, f'{self.kind}_clean')
        item['clean_input'], item['cleaner_score'] = clean_method(item['clean_input'])

        logger.debug(f'Cleaned {item["raw_input"]} -> {item["clean_input"]}')

        if not item['clean_input']:
            return False

        return True

    @classmethod
    def cc_clean(cls, raw_input: str) -> Tuple[str, Optional[float]]:
        """
        Cleans raw input strings using CCCleaner, and returns clean strings.
        As CCCleaner does not have a score, returns None in the score element of the tuple.
        """
        assert raw_input
        return cls.cc_cleaner.clean(raw_input)[0], None

    @staticmethod
    def _norm_nn_score(score: float) -> float:
        """ Normalize score with  =1.159771*s^2-0.0286*s  | s is the `internal_score`  Cap at 1.0 """
        return min(1.159771 * score ** 2 - 0.0286 * score, 1.0)

    @classmethod
    def nn_clean(cls, raw_input: str) -> Tuple[str, Optional[float]]:
        """
        Cleans raw input strings using neural network, and returns clean strings with their (normalized) scores.

        NOTE: nn_str_cleaner returns a start and end index from the original string to form the clean string
        e.g. "raw input string"[4:12] = "input str". start_index_prob is the confidence that the start index (4) is
        correct and end_index_prob is the same for the end index (12).
        """
        assert raw_input

        res = cls.merchant_extractor.extract_names([raw_input])[0]

        output = res['merchant_name']
        score = cls._norm_nn_score((res['start_index'][1] + res['end_index'][1]) / 2) if output else None

        return output, score

    @classmethod
    def torch_clean(cls, raw_input: str) -> Tuple[str, Optional[float]]:
        """ Cleans raw inputs using torch cleaner """
        raise NotImplementedError()
        # results = [self.torch_cleaner.clean_string(rawstr) for rawstr in raw_strings]
        # return [(
        #     res['cleanedString'],
        #     (res['beginIndexConf'] + res['endIndexConf']) / 2,  # FIXME: update normalization formula?
        # ) for res in results]

    @classmethod
    def bing_v2_clean(cls, raw_input: str) -> Tuple[str, Optional[float]]:
        """ Cleans raw input using Bing and v2 """
        from mapper.mapper import map_input
        from mapper.response import NO_MATCH, DOMAIN_UNREACHABLE

        output, score = '', None

        try:
            bing_response = BingWebCore().query(raw_input)
        except Exception:
            logger.exception('Bing request failed')
            bing_response = {}

        results = bing_response.get('webPages', {}).get('value', [])
        if not results:
            return output, score

        url = results[0].get('url')
        if not url:
            return output, score

        response = map_input(url, mode='domain')
        company_name = response['Company Name']
        if company_name not in [NO_MATCH, DOMAIN_UNREACHABLE]:
            return company_name, response['Confidence']

        return output, score


class NonMerchantStr:
    """ Helper to determine whether string is not a merchant. """

    # blacklisted words (selected manually)
    block_words = ['acct', 'automatic', 'banking', 'bil', 'bill', 'billings', 'card', 'cash', 'check', 'checkcard',
                   'checking', 'chk', 'confirmation', 'crd', 'credit', 'debit', 'debit', 'deposit', 'e-payment', 'fee',
                   'funds', 'id', 'interest', 'maintenance', 'monthly', 'online', 'paid', 'pay', 'payment', 'paymnt',
                   'pmts', 'purchase', 'pymt', 'received', 'reference', 'ret', 'srvc', 'transfer', 'withdrawal',
                   'withdrwl']

    # blacklisted articles, determiners and qualifiers
    block_determiners = ['a', 'an', 'another', 'any', 'from', 'her', 'his', 'its', 'my', 'on', 'other', 'our', 'that',
                         'the', 'their', 'these', 'this', 'those', 'to', 'what', 'which', 'whose', 'your']

    # blacklisted words may be separated by these delimiters
    words_delimiters = [' ', ':', '#']

    block_pattern = re.compile(f'(^|{"|".join(words_delimiters)})({"|".join(block_words + block_determiners)})'
                               f'(?=$|{"|".join(words_delimiters)})', re.IGNORECASE)

    @classmethod
    def is_not_merchant(cls, clean_input):
        """ Returns True if `clean_input` contains ONLY blacklisted words and thus is not a merchant string. """

        if 2 > len(clean_input):
            return True

        cleaned = cls.block_pattern.sub(' ', clean_input)
        return not bool(re.match('.*[a-zA-Z]+.*', cleaned))


if '__main__' == __name__:
    import argparse
    from api.app.config import CSV_DIR
    import os
    import csv

    parser = argparse.ArgumentParser()
    parser.add_argument('raw_inputs', nargs='+')
    parser.add_argument('--method', default='cc')
    parser.add_argument('--csv', type=bool, default=False)
    args = parser.parse_args()

    cleaner = MerchantCleaner(kind=args.method)
    results = [cleaner(rawin) for rawin in args.raw_inputs]

    if args.csv:
        filename = os.path.join(CSV_DIR, 'cleaner', f'{args.method}.csv')
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        file = open(filename, 'w')
        writer = csv.DictWriter(
            file,
            fieldnames=['raw_input', 'merchant_name', 'info'],
        )
        writer.writeheader()

        for i, result in enumerate(results):
            writer.writerow({
                'raw_input': args.raw_inputs[i],
                'merchant_name': result[0],
                'score': result[1],
            })

        file.flush()

        print(f'Wrote results to {filename}')  # DEBUG

    print(results)
