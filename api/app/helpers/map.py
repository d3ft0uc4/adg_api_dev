from collections import namedtuple

from sqlalchemy.sql import text

from api.app.extensions import db
from api.app.exceptions import APIException


NullTargetData = namedtuple('NullTargetData', ['uid', 'entity_name'])


def find_target_by_name(input_string: str, target: str):
    """Find the target by name.

    Args:
        input_string - input string used to predict the target (only used in the
            exception message)
        target - the target name to find in the database

    Returns:
        Target data from the database or null target object if no target was found.
    """
    statement = text('''
        select max(final_guess_entity) as entity_name
          from entity_chooser_beta
         where final_guess_target_map is not null
           and final_guess_entity is not null
           and final_guess_target_map = :target
      group by final_guess_target_map
      order by count(*) desc;
    ''')
    target_data = db.session.execute(statement, {'target': target}).fetchall()
    if len(target_data) > 1:
        raise APIException(f'More than 1 target found for input: {input_string}, prediction: {target}')
    if target_data:
        [target_data] = target_data
    else:
        target_data = NullTargetData(None, None)
    return target_data
