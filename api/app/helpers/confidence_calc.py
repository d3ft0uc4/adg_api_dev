"""
© 2018 Alternative Data Group. All Rights Reserved.

This module contains helper functions for calculating confidence from all scores.

Usage example:
    from api.app.helpers.confidence_calc import ConfidenceCalculator
    from api.app.helpers.entity_comparison import EntityComparator

    items = [{
        'bane_winner': 'walmart',
        'bane_norm_score': 0.9,
        'fwfm_winner': 'Wal-Mart Inc.',
        'fwfm_norm_score': 0.9,

    }]

    EntityComparator().compare_all(items))
    ConfidenceCalculator.get_confidences(items)
"""
from typing import Dict

from api.app.config import NN_CLNR_HISCORE, FWFM_ONLY_PENALTY, BANE_ONLY_PENALTY, \
    DUAL_WINNER_BONUS, CLEANER_NONE_PENALTY, COMP_SCORE_CONF_MULTIPLIER, PARKED_DOMAIN_PENALTY
from api.app.helpers.string_cleaning import BaseDomainCleaner


class ConfidenceCalculator:
    """
    Class for calculating overall confidence based on several modules' confidences.
    """
    @classmethod
    def get_confidence(cls, item: dict):
        """
        Sets confidence. Updates `item` in place, adding the 'confidence' key.

        Args:
            item: Dict with all current stages results. Possible keys:
                cleaner_score: Cleaner's score if any
                bane_norm_score: Normalized Bwne score if any
                fwfm_norm_score: Normalized FW/FM score if any
                comp_score: Score of winners' comparison

        Return: Modifies item in place, adds:
            'confidence': Float [0,1] - measure of confidence - how much we are sure that winner is a correct result for
                          original raw input string.
            'confidence_sources': which scores participated in confidence calculation and their values
        """
        scores: Dict[str, float] = dict()  # confidence sources - name: score dict

        # ---- Average score ----
        valid_keys = [key for key in ['bane_norm_score', 'fwfm_norm_score'] if item.get(key) is not None]
        avg_confidence = sum([float(item[key]) for key in valid_keys]) / len(valid_keys)

        scores[','.join(valid_keys)] = avg_confidence

        # ---- Entity comparison score ----
        comp_score = item.get('comp_score', 0)
        if comp_score:
            scores['comp_score'] = COMP_SCORE_CONF_MULTIPLIER * comp_score

        # ---- Cleaner penalty ----
        cleaner_score = item.get('cleaner_score')
        cleaner_penalty = (cleaner_score - 1) if cleaner_score and cleaner_score < NN_CLNR_HISCORE else 0
        if cleaner_penalty:
            scores['cleaner_penalty'] = cleaner_penalty

        # ---- Single winner penalty ----
        bane_winner = item.get('bane_winner')
        fwfm_winner = item.get('fwfm_winner')

        if fwfm_winner and not bane_winner:
            scores['fwfm_only_penalty'] = FWFM_ONLY_PENALTY
        elif fwfm_winner and bane_winner:
            scores['dual_winner_bonus'] = DUAL_WINNER_BONUS
        elif bane_winner and not fwfm_winner:
            scores['bane_only_penalty'] = BANE_ONLY_PENALTY

        # ---- Penalty for base cleaner ----
        cleaner = item.get('cleaner')
        if type(cleaner) == BaseDomainCleaner:
            scores['cleaner_none_penalty'] = CLEANER_NONE_PENALTY

        # ---- Parked domain penalty ----
        if item.get('is_parked', False):
            scores['parked_domain_penalty'] = PARKED_DOMAIN_PENALTY

        item.update({
            'confidence_sources': scores,
            'confidence': sum(scores.values()),
        })
