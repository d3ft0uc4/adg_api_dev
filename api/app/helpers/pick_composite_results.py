"""
© 2018 Alternative Data Group. All Rights Reserved.

This module contains helper functions for deciding whether winner exists and
which tool's output should be treated as final winner.
"""

from api.app.config import CONF_OK, BANE_NO_MATCH_REWARD
from packages.targets import TARGETS


class ResultsPicker:
    """ Helper class for picking entity recognition results and generating API output. """

    @staticmethod
    def winner_exists(confidence):
        """ Returns True if winner exists for this level of `confidence`. """
        # `bool()` converts `numpy.bool_` to `bool`
        return bool(confidence >= CONF_OK)

    @classmethod
    def final_winner(cls, item):
        """
        Chooses which winner and metadata (if any) to return based on item's values.
        It may also return suggested company/ies without being certain which (if any) is the correct answer.

        Args:
            item: dict, possible keys:
                'bane_winner': Bane's winner
                'bane_norm_score': Normalized [0;1] confidence score for Bane's winner
                'fwfm_winner': Fw/Fm's winner
                'fwfm_norm_score': Normalized [0;1] confidence score for Fw/Fm's winner
                'comp_match': Whether Bane and Fw/Fm winners match (refer to the same entity)

        Returns: updates `item` in-place with values:
            'final_winner'
            'final_winner_source': 'bane' or 'fwfm' or None
            'metadata': {...}
        """

        #
        # Winner Matrix
        #
        result = {
            'final_winner': None,
            'final_winner_source': None,
            'metadata': {},
        }

        bane_winner = item.get('bane_winner')  # None if norm_score is low
        fwfm_winner = item.get('fwfm_winner')
        fwfm_target = item.get('fwfm_target')
        bane_super_winner = item.get('bane_is_super_winner', False)
        fwfm_super_winner = item.get('fwfm_is_super_winner', False)

        # ---- match case ----
        if item.get('comp_match') or \
                (bane_super_winner and fwfm_super_winner):
            # If they're both very high score but no match, we still treat as match.
            # If there's a match: return fwfm winner and FW metadata;
            result = {
                'final_winner': fwfm_winner,
                'final_winner_source': 'fwfm',
                'metadata': TARGETS.get_metadata(fwfm_target),
            }

        # ---- non-match cases ----
        elif bane_winner and not fwfm_winner:
            # If only Fw/Fm has very low score, return Bane winner w/o metadata
            result = {
                'final_winner': bane_winner,
                'final_winner_source': 'bane',
                'metadata': {},
            }

        elif fwfm_winner and not bane_winner:
            # If only Bane has low score we can still return FW/FM winner if it's score is high enough.
            result = {
                'final_winner': fwfm_winner,
                'final_winner_source': 'fwfm',
                'metadata': TARGETS.get_metadata(fwfm_target),
            }
            if fwfm_super_winner:
                # TODO
                # If Bane didn’t return a winner and FMFW returned a super winner,
                # something is “funky”, we should penalize FMFW’s winner with -0.5
                pass

        elif bane_winner and fwfm_winner:
            # If bane winner & fwfm winner exist but there’s no match overall:
            # pick the winner with the highest norm score
            # after rewarding bane_norm_score temporarily

            if item['bane_norm_score'] + BANE_NO_MATCH_REWARD >= item['fwfm_norm_score']:
                result = {
                    'final_winner': bane_winner,
                    'final_winner_source': 'bane',
                    'metadata': {'company_aliases': [fwfm_winner]},
                }
            else:
                result = {
                    'final_winner': fwfm_winner,
                    'final_winner_source': 'fwfm',
                    'metadata': TARGETS.get_metadata(fwfm_target),
                }

        item.update(result)
