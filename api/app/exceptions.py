"""
© 2018 Alternative Data Group. All Rights Reserved.

REST API custom exceptions
"""


class APIException(Exception):

    def __init__(self, message=None, details=None, hint=None):
        self.name = 'api_exception'
        self.message = message
        self.details = details
        self.hint = hint
        self.code = 400


class APIInternalException(APIException):

    def __init__(self, details, hint=None):
        self.name = 'Internal Error'
        self.message = "API internal error"
        self.details = details
        self.hint = hint
        self.code = 500


class APIValidationException(APIException):

    def __init__(self, details, hint=None):
        self.message = "Request parameters validation error"
        self.name = 'Validation Error'
        self.details = details
        self.hint = hint
        self.code = 422
