def flag_api(input_string: str, wrong_target_uuid: str, correct_target: str):
    """The /flag API implementation.

    Args:
        input_string: the input string to report the wrong target for.
        wrong_target_uuid: the UUID of the wrong target returned by /map or /audit.
        correct_target: string, optional, correct target.

    Returns: dictionary, {status: OK}
    """
    # TODO: write to the DB
    return {
        'status': 'OK'
    }
