"""
© 2018 Alternative Data Group. All Rights Reserved.

This module contains function for /merchant-mapper API endpoint.
Uses configuration in ../config.py  See ../../README.md for details.

To run manual tests from repo root in single input use `python3 -m api.app.methods.bane_fwfm`.
Run a few input strings from command line:
    CSV_OUTPUT=1 python3 -m api.app.methods.bane_fwfm "1 & one" microsoft "Amy's Bread"

Run inputs manually:
    python3.6 -m api.app.methods.mapper "Wal*Mart" "7-Eleven" Samsung

Run all inputs from a text file with raw strings (one per line):
    python3.6 -m api.app.methods.mapper path/to/raw_inputs.txt

CLI options:
    --debug - whether resulting JSON should include debug info
    --csv-output - whether to store results into CSV file
    --mapper=[domain|merchant] - which mapper to use
"""

import csv
import json
import os
import sqlalchemy
import logging
from datetime import datetime
from multiprocessing import Process
from typing import List, Dict, Any
from sqlalchemy.pool import NullPool

from joblib import Parallel, delayed

from mapper.settings import \
    PARALLEL, \
    SQL_CONNECTION_STRING, \
    CSV_OUTPUT, DB_OUTPUT, CSV_DIR, \
    CONF_LEVEL_LOW, CONF_LEVEL_MEDIUM
from packages.targets import TARGETS
from packages.utils import db_execute, Timer, uid, remove_dupes, remove_value
from domain_tools.whois import WhoisInfoRetrieval
from unofficializing.unofficializing import make_unofficial_name
from ticker_merchant_map.ticker_merchant_map import TickerFinder, TickerInfo

from api.app.helpers.string_cleaning import BaseCleaner, MerchantCleaner, DomainCleaner, NonMerchantStr, \
    BaseDomainCleaner
from api.app.helpers.entity_comparison import EntityComparator
from api.app.helpers.confidence_calc import ConfidenceCalculator
from api.app.helpers.pick_composite_results import ResultsPicker
from bane.organization_entity_check import is_organization

from bane.bane import Bane
from find_winner.fw_fm import FWFM

logger = logging.getLogger(f'adg.{__name__}')


class ProcessingStage:
    def __str__(self):
        return self.__class__.__name__

    def __call__(self, item: dict) -> bool:
        """ Returns True if stage succeeded. If False, then other stages are not run. """
        raise NotImplementedError()


class MerchantCheck(ProcessingStage):
    """ Checks whether cleaned input is merchant string """

    def __call__(self, item: dict) -> bool:
        clean_input = item.get('clean_input')
        assert clean_input

        item['is_merchant'] = not NonMerchantStr.is_not_merchant(clean_input)
        return item['is_merchant']


class EntityRecognition(ProcessingStage):
    """ Given valid item, retrieves Bane and Fw/Fm winners and losers """

    def __call__(self, item: dict) -> bool:
        clean_input = item.get('clean_input')
        assert clean_input

        item.update({
            **{f'bane_{key}': value for key, value in Bane().get_winner(item['clean_input']).items()},
            **{f'fwfm_{key}': value for key, value in FWFM().get_winner(item['clean_input']).items()},
        })

        return True


class EntityComparison(ProcessingStage):
    """
    Compares winners from Bane and Fw/Fm and generates pairs and ther comparison scores,
    also selects best winners pair.
    """
    comparator = EntityComparator()

    def __call__(self, item: dict) -> bool:
        self.comparator.compare_and_select_winners(item)

        if not item.get('bane_winner') and not item.get('fwfm_winner'):
            return False

        return True


class ConfidenceCalculation(ProcessingStage):
    def __call__(self, item: dict) -> bool:
        ConfidenceCalculator.get_confidence(item)
        return True


class WinnerPicking(ProcessingStage):
    def __call__(self, item: dict) -> bool:
        assert 'confidence' in item

        item['winner_exists'] = ResultsPicker.winner_exists(item['confidence'])
        if not item['winner_exists']:
            return False

        ResultsPicker.final_winner(item)

        # now check result with BEF
        assert 'final_winner_source' in item
        if item['final_winner_source'] == 'bane':
            is_org = is_organization(item['final_winner'])
            item['is_org'] = is_org
            if not is_org:
                item.update({
                    'winner_exists': False,
                    'final_winner': None,
                    'metadata': {},
                })
                return False

        return True


class TickerRetrieval(ProcessingStage):
    def __init__(self, use_whois=False):
        """
        Ticker retrieving processing stage.

        Args:
            use_whois: whether to pass raw input to whois organization retrieval and use the result for ticker finding
        """
        self.use_whois = use_whois

    def __call__(self, item: dict) -> bool:
        assert 'final_winner' in item
        assert 'final_winner_source' in item

        final_winner = item['final_winner']
        metadata = item.get('metadata', {})

        # TODO: refactor this
        result = {'ticker': None, 'exchange': None}

        # # -- forward ticker lookup --
        # if item['final_winner_source'] == 'fwfm':
        #     # take ticker & exchange from database if winner is fwfm
        #     result = {
        #         'ticker': metadata['ticker'],
        #         'exchange': metadata['exchange'],
        #     }

        if not result['ticker'] or not result['exchange']:
            # ticker not found in targets database - try external services
            result = TickerFinder.get_ticker(final_winner)

        if not result['ticker'] or not result['exchange']:
            related = metadata.get('related_entities')
            if related:
                result = TickerFinder.get_ticker(related[0]['official_name'])

        if (not result['ticker'] or not result['exchange']) and self.use_whois:
            # if ticker still not found, try getting domain's whois info and then use it for ticker search
            organization = WhoisInfoRetrieval.get_organization(item['raw_input'])
            if organization:
                result = TickerFinder.get_ticker(organization)

        # -- reverse ticker lookup --
        ticker_info = TickerInfo.get_info_from_figi(result['ticker']) if result['ticker'] else {}
        result['company_name'] = ticker_info.get('cleanName')

        item['tkr_exch'] = result

        return True


def generate_response(results: List[Dict], debug: bool = False) -> Dict:
    """
    Converts internal results into user-friendly response.

    Args:
        results: list of attempts - dicts with results from all stages
        debug: whether to include debug information into response

    Returns:
        Dict of results which are returned by the API.
    """
    response = {}
    best_result = results[-1]

    # -- Raw input --
    response['Original Input'] = best_result['raw_input']

    # -- Company name --
    # Fw/Fm winner or Bane winner or 'No Match'
    final_winner = best_result.get('final_winner')
    final_winner_source = best_result.get('final_winner_source')

    if final_winner:
        company_name = final_winner
    elif best_result.get('is_alive') is False:
        company_name = 'Domain Unreachable'
    else:
        company_name = 'No Match / Small Business'
    response['Company Name'] = company_name

    if not final_winner:
        logger.debug(f'No final winner found for "{best_result["raw_input"]}"')

    # -- Aliases --
    # unofficial_names and company_aliases (no need to differentiate between the two, just add together in a list)
    # or same string as Company Name if unofficial_names and company_aliases do not exist.
    metadata = best_result.get('metadata', {})
    unofficial_names = metadata.get('unofficial_names', [])
    company_aliases = metadata.get('company_aliases', [])

    aliases = remove_dupes(unofficial_names + company_aliases)
    if final_winner:
        aliases = remove_value(aliases, final_winner)

    response['Aliases'] = aliases

    # -- Confidence --
    if final_winner:
        confidence = best_result.get('confidence', 0)
        if confidence < CONF_LEVEL_LOW:
            confidence_level = 'Low'  # type: Any
        elif confidence < CONF_LEVEL_MEDIUM:
            confidence_level = 'Medium'
        else:
            confidence_level = 'High'
    else:
        confidence = None
        confidence_level = None

    response.update({
        'Confidence': round(confidence, 2) if confidence else confidence,
        'Confidence Level': confidence_level,
    })

    # -- Alternative Company Matches --
    # If comp_match is FALSE, this has loser's name.
    alternatives: List[str] = []
    bane_winner = best_result.get('bane_winner')
    bane_is_super_winner = best_result.get('bane_is_super_winner')
    fwfm_winner = best_result.get('fwfm_winner')
    fwfm_is_super_winner = best_result.get('fwfm_is_super_winner')
    if fwfm_winner and bane_winner and best_result.get('comp_match') is False:
        if not final_winner:
            # if there's no final winner, both are losers
            alternatives.extend([bane_winner, fwfm_winner])
        else:
            # the loser’s company name (i.e. BANE or FMFW) - other than final_winner
            if final_winner_source == 'bane':
                alternatives.append(fwfm_winner)
            else:
                alternatives.append(bane_winner)

    # If bane other candidates (not highest winner) are above the winner threshold, return those as well here
    bane_winners = best_result.get('bane_winners', [])
    if len(bane_winners) > 1:
        alternatives += [winner['winner'] for winner in bane_winners[1:]]

    # All super winners should be listed in alterniatve company matches
    # if they are different from the finall winner using only string distance
    # (so if there is a match using fmfw_comp, it doesnt count as a match and thus they should still be displayed)
    if not best_result.get('comp_match', False) or best_result.get('comp_match_source') == 'fwfm':
        if (final_winner_source != 'bane') and bane_winner and bane_is_super_winner:
            alternatives.append(bane_winner)

        if (final_winner_source != 'fwfm') and fwfm_winner and fwfm_is_super_winner:
            alternatives.append(fwfm_winner)

    alternatives = remove_dupes(alternatives)
    #
    # # populate ticker & exchange
    # for alternative in alternatives:
    #     tkr_exch = TickerFinder.get_ticker(alternative['Name'])
    #     alternative['Ticker'], alternative['Exchange'] = tkr_exch['ticker'], tkr_exch['exchange']

    response['Alternative Company Matches'] = alternatives

    # -- Related Entities --
    related = [{
        'Name': related['official_name'],
        'Closeness Score': round(related['score'], 2),
    } for related in metadata.get('related_entities', [])]

    response['Related Entities'] = related

    # -- Ticker & exchange --
    tkr_exch = best_result.get('tkr_exch', {})
    response['Ticker'] = tkr_exch.get('ticker', None)
    response['Exchange'] = tkr_exch.get('exchange', None)
    response['Ticker Company Name'] = tkr_exch.get('company_name', None)

    # -- Debug & maintenance --
    if debug:
        # remove losers
        for attempt in results:
            if 'bane_losers' in attempt:
                attempt['bane_losers'] = attempt['bane_losers'][:2]

            if 'fwfm_losers' in attempt:
                attempt['fwfm_losers'] = attempt['fwfm_losers'][:2]

        response['_DEBUG'] = results

    if PARALLEL:
        Process(target=update_nodes, args=(best_result,)).start()
    else:
        update_nodes(best_result)

    return response


def to_csv(results: List[List[Dict]], run_code: str):
    filename = os.path.join(CSV_DIR, f'{run_code}.csv')
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    file_exists = os.path.isfile(filename)
    file = open(filename, 'w')

    writer = csv.DictWriter(
        file,
        fieldnames=[
            # Raw inputs
            'raw_input',  # 'input_no',
            # Clean inputs
            'cleaner', 'clean_input', 'cleaner_score',
            # Clean inputs validation
            'is_merchant', 'is_alive',
            # Entity recognition (Bane + Fw/Fm)
            # Bane
            'bane_bing_results', 'bane_winner', 'bane_is_super_winner',
            'bane_score', 'bane_norm_score', 'bane_thld', 'bane_winners', 'bane_losers',
            # Fw/Fm
            'fwfm_bing_results', 'fwfm_winner', 'fwfm_is_super_winner',
            'fwfm_score', 'fwfm_norm_score', 'fwfm_thld', 'fwfm_winners', 'fwfm_losers',
            # Entity recognition (Bane through FW/FM entity comparison or string distance)
            'comp_str_dist_candidates',
            'comp_bane_fwfm',
            'comp_match', 'comp_score',
            # Confidence calculation
            'confidence_sources', 'confidence',
            # Winner
            'is_org', 'winner_exists', 'final_winner_source', 'final_winner', 'metadata',
        ],
        extrasaction='ignore',
    )

    if not file_exists:
        writer.writeheader()

    for attempts in results:
        for item in attempts:
            writer.writerow(item)

    file.flush()

    logger.debug(f'Wrote results to {filename}')


def to_db_rowlog(results: List[List[Dict]], run_code: str):
    """ Saves `run_code` and `results` to api_db_log table in the db, as well as proxied user info if available. """
    from flask import request

    # Disable pooling using NullPool. This is the most simplistic, one shot system that prevents the Engine from
    # using any connection more than once. From
    # docs.sqlalchemy.org/en/rel_1_0/faq/connections.html#how-do-i-use-engines-connections-sessions-with-python-multiprocessing-or-os-fork
    engine = sqlalchemy.create_engine(SQL_CONNECTION_STRING, poolclass=NullPool)

    sql = "INSERT INTO altdg_merchtag.api_db_log(run_code, datetime, raw_input, cleaner, clean_input, cleaner_score, \
           is_merchant, is_alive, bane_bing_results, bane_winner, bane_is_super_winner, bane_score, \
           bane_norm_score, bane_thld, bane_winners, bane_losers, fwfm_bing_results, fwfm_winner, \
           fwfm_is_super_winner, fwfm_score, fwfm_norm_score, fwfm_thld, fwfm_winners, fwfm_losers, \
           comp_str_dist_candidates, comp_bane_fwfm, comp_match, comp_score, confidence_sources, \
           confidence, is_org, winner_exists, final_winner_source, final_winner, metadata, user_ip_addr, app_key) \
           VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, \
           %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

    for attempts in results:
        for item in attempts:
            engine.execute(sql, (
                str(run_code),
                str(datetime.now()),
                json.dumps(item.get('raw_input')),
                json.dumps(str(item.get('cleaner'))),
                json.dumps(item.get('clean_input')),
                json.dumps(item.get('cleaner_score')),
                json.dumps(item.get('is_merchant')),
                json.dumps(item.get('is_alive')),
                json.dumps(item.get('bane_bing_results')),
                json.dumps(item.get('bane_winner')),
                json.dumps(item.get('bane_is_super_winner')),
                json.dumps(item.get('bane_score')),
                json.dumps(item.get('bane_norm_score')),
                json.dumps(item.get('bane_thld')),
                json.dumps(item.get('bane_winners')),
                json.dumps(item.get('bane_losers')),
                json.dumps(item.get('fwfm_bing_results')),
                json.dumps(item.get('fwfm_winner')),
                json.dumps(item.get('fwfm_is_super_winner')),
                json.dumps(item.get('fwfm_score')),
                json.dumps(item.get('fwfm_norm_score')),
                json.dumps(item.get('fwfm_thld')),
                json.dumps(item.get('fwfm_winners')),
                json.dumps(item.get('fwfm_losers')),
                json.dumps(item.get('comp_str_dist_candidates')),
                json.dumps(item.get('comp_bane_fwfm')),
                json.dumps(item.get('comp_match')),
                json.dumps(item.get('comp_score')),
                json.dumps(item.get('confidence_sources')),
                json.dumps(item.get('confidence')),
                json.dumps(item.get('is_org')),
                json.dumps(item.get('winner_exists')),
                json.dumps(item.get('final_winner_source')),
                json.dumps(item.get('final_winner')),
                json.dumps(item.get('metadata')),
                request.headers.get('X-Forwarded-For', '').split(',')[0] if request else '',
                request.headers.get('X-User-Key', 'Not provided') if request else '',
            ))

    logger.debug(f'Wrote results to db table `api_db_log`.')


def update_nodes(step: Dict):
    """ Updates brand_master_nodes_bane_additions table. """
    tkr_exch = step.get('ticker', {})
    ticker, exchange = tkr_exch.get('ticker'), tkr_exch.get('exchange')

    if step.get('bane_is_super_winner', False) and ticker:
        bane_winner = step['bane_winner']
        bane_winner_uo = make_unofficial_name(bane_winner)

        if bane_winner_uo in TARGETS.contents.values():
            return

        logger.debug(f'Updating brand_master_nodes_bane_additions table with {bane_winner} = {exchange}:{ticker}')
        db_execute(f"""
            INSERT INTO brand_master_nodes_bane_additions
                (uid, match_target, official_name, identifiers)
            VALUES
                (%(uid)s, %(uo_bane)s, %(bane)s, %(tkr_exch)s)
            ON DUPLICATE KEY UPDATE
                match_target = %(uo_bane)s,
                official_name = %(bane)s,
                identifiers = %(tkr_exch)s
        """, uid=uid(), uo_bane=bane_winner_uo, bane=bane_winner, tkr_exch=f'{ticker}:{exchange}')


def map_input(raw_input: str, runs: List[List[ProcessingStage]]) -> List[Dict[str, Any]]:
    """
    Process single raw input and return all info from all stages in a huge dictionary.

    Args:
        raw_input: raw input string
        runs: list of processing steps to try on clean input

    Returns:
        List of attempts to find a winner. Each attempt is a dict, for complete reference see `generate_response`.
    """

    # Try running input through all the stages.
    # If all stages return True, that means that process completed successfully and the winner was found.
    attempts = []  # here we accumulate all our attempts of running cleaners resutls through process stages

    for stages in runs:
        # prepare item before processing
        item = dict(raw_input=raw_input)
        attempts.append(item)

        for stage in stages:
            with Timer(str(stage)):
                success = stage.__call__(item)
            if not success:
                # if stage failed, we need to continue with another cleaner
                logger.debug(f'Stage {str(stage)} failed, skipping to next cleaner')
                break
        else:
            # this place means that all stages were successful and we have a winner
            # no need for another attempt
            return attempts

        # if we're here, one of the stages failed and we'll try another one

    return attempts


def mapper_api(raw_inputs: List[str], mode: str, debug: bool = False, csv_output: bool = CSV_OUTPUT,
               db_output: bool = DB_OUTPUT):
    """
    /merchant-mapper and /domain-mapper endpoint
    Runs input raw strings through the entire Bane + FW/FM process and returns results.
    Results will contain additional debug info if debug is true.

    Args:
        raw_inputs: list of input strings to find targets for
        mode: [merchant|domain] - which mapper to use
        debug: whether to include additional debug info to results
        csv_output: whether to create debug CSV file
        db_output: whether to write run info to the database

    Returns:
        List with input string, corresponding winner received from Bane and FW/FM, score and additional info.
    """
    logger.debug('Raw inputs:')
    for rawin in raw_inputs:
        logger.debug(rawin)

    run_code = f"{datetime.now():%Y-%m-%d-%H%M}" \
               f"{'_debug_' if debug else ''}" \
               f"_demo-{mode}-mapper_n{len(raw_inputs)}" \
               f"_{raw_inputs[0][:16].replace(' ', '-').replace('/', '-')}"
    logger.debug(f'Run code: "{run_code}"')  # DEBUG

    # Sets up process for merchant mapper:
    if mode == 'merchant':
        processing_stages = [
            MerchantCheck(),        # Uses simple string merchant y/n check.
            EntityRecognition(),
            EntityComparison(),
            ConfidenceCalculation(),
            WinnerPicking(),
            TickerRetrieval(),
        ]
        # Each run goes over all the previous stages, with differently cleaned merchant strings:
        runs = [
            [MerchantCleaner(kind='cc'), *processing_stages],
            [MerchantCleaner(kind='nn'), *processing_stages],
            [BaseCleaner(), *processing_stages],
            # [MerchantCleaner(kind='bing_v2'), *processing_stages],
        ]

    # Sets up process for domain mapper:
    elif mode == 'domain':
        processing_stages = [
            EntityRecognition(),
            EntityComparison(),
            ConfidenceCalculation(),
            WinnerPicking(),
            TickerRetrieval(use_whois=True),  # Uses whois in ticker retrieval.
        ]
        # Each run goes over all the previous stages, with differently cleaned domain names:
        runs = [
            [DomainCleaner(kind='whois'), *processing_stages],
            [DomainCleaner(kind='whois', follow_redirects=False), *processing_stages],
            [BaseDomainCleaner(), *processing_stages],
            # [DomainCleaner(kind='ssl'), *processing_stages],
            [DomainCleaner(kind='tld'), *processing_stages],
        ]
    else:
        raise ValueError(f'Unknown mode: {mode}')

    # For each input, run a separate mapping process using multiprocessing.
    # FIXME: Try multiprocessing? Note `AssertionError: daemonic processes are not allowed to have children`
    n_jobs = len(raw_inputs) if PARALLEL else 1
    logger.debug(f'Spawning {n_jobs} processes')

    # `results` is a list containing all attempts for each input string. Last attempt is final result.
    results: List[List[Dict]] = Parallel(n_jobs=n_jobs, prefer='threads')(
        delayed(map_input)(rawin, runs=runs) for rawin in raw_inputs
    )

    # Optionally output results to csv file.
    if csv_output:
        if PARALLEL:
            Process(target=to_csv, args=(results, run_code)).start()
        else:
            to_csv(results, run_code)

    # Optionally output results to database.
    if db_output:
        if PARALLEL:
            Process(target=to_db_rowlog, args=(results, run_code)).start()
        else:
            to_db_rowlog(results, run_code)

    # For each result, run response generation thread.
    response = Parallel(n_jobs=n_jobs, prefer='threads')(
        delayed(generate_response)(attempts, debug=debug) for attempts in results
    )

    return response


if "__main__" == __name__:
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('input', nargs='+')
    parser.add_argument('--debug', type=bool, default=False)
    parser.add_argument('--csv-output', type=bool, default=CSV_OUTPUT)
    parser.add_argument('--db-output', type=bool, default=DB_OUTPUT)
    parser.add_argument('--mapper', default='merchant')
    args = parser.parse_args()

    raw_inputs = []  # type: list

    # If there's only one `args.input` and it's a file, attempt to read it for raw input strings (1 per line).
    if 1 == len(args.input) and os.path.isfile(args.input[0]):
        try:
            with open(args.input[0]) as fp:
                raw_inputs = [x.strip() for x in fp.readlines()]
        except Exception as ex:
            logger.error(ex)
            raise

    # Else use `args.input` as the raw input string list.
    else:
        raw_inputs = args.input

    with Timer('Overall process'):
        results = mapper_api(raw_inputs, mode=args.mapper, debug=args.debug, csv_output=args.csv_output,
                             db_output=args.db_output)
    print(json.dumps(results, indent=4, default=lambda x: str(x)))
