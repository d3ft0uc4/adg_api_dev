"""
© 2018 Alternative Data Group. All Rights Reserved.

REST API models (Flask)

TODO: Compare and update vs the DB structure.
"""

from collections import namedtuple

from sqlalchemy import Table, Column, MetaData
from sqlalchemy.dialects.mysql import CHAR, MEDIUMTEXT, DECIMAL, DATETIME, JSON, TEXT, VARCHAR, BIGINT


metadata = MetaData()


fm_output = Table(
    'fm_output',
    metadata,
    # - there is no PK
    # - there are no indexes
    # - why UID is MEDIUMTEXT? should be CHAR(36) - GUIDs have fixed len
    # - why all fileds are NULL-able?
    # - queries that compare text are case-insensitive (related to collation)
    # - score_type should be int / enum
    # - UID_target seems to be int in the DB
    # - search_result_type should be int / enum
    # - are UID_input_string and input_string different?
    # Note: UID in the db is MEDIUMTEXT and is not PK.
    Column('uid', CHAR(36), primary_key=True),
    Column('created_at', DATETIME(), nullable=True),
    Column('uidlist_input_string', CHAR(36)),
    Column('input_string', MEDIUMTEXT(), nullable=True),
    Column('input_string_uidsource', VARCHAR(255), nullable=True),
    Column('search_addon_string', MEDIUMTEXT(), nullable=True),
    Column('search_result_type', MEDIUMTEXT(), nullable=True),
    Column('uid_target', MEDIUMTEXT(), nullable=True),
    Column('target_name', MEDIUMTEXT(), nullable=True),
    Column('score_type', MEDIUMTEXT(), nullable=True),
    Column('search_result_type', MEDIUMTEXT(), nullable=True),
    Column('search_result_ranks', JSON(), nullable=True),
    Column('score', DECIMAL, nullable=True)
)


match_targets_unofficial_map = Table(
    'match_targets_unofficial_map',
    metadata,
    Column('match_target', MEDIUMTEXT()),
    Column('source', VARCHAR(3), nullable=False),
    Column('match_target_unofficial', TEXT(), nullable=True),
)


entity_match_target_map = Table(
    'entity_match_target_map',
    metadata,
    Column('uid', VARCHAR(36)),
    Column('entity_name', VARCHAR(255)),
    Column('entity_source', VARCHAR(10)),
    Column('match_target', VARCHAR(100)),
)


entity_chooser_beta = Table(
    'entity_chooser_beta',
    metadata,
    Column('merchant_string', TEXT()),
    Column('uidlist_agg_merch_master_top_merchants', TEXT()),
    Column('final_guess_entity', TEXT()),
    Column('final_guess_target_map', TEXT()),
    Column('is_entity_guess_good', BIGINT(20))
)


TargetData = namedtuple('TargetData', ['target', 'score'])
