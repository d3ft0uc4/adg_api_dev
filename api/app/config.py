"""
© 2018 Alternative Data Group. All Rights Reserved.

API settings file.
"""

import os
# next line populates env vars from `load_dotenv` call
import mapper.settings  # noqa: F401


# ---- Legacy settings, TODO: REMOVE ----

NN_CLNR_HISCORE = 0.847   # Level at which scores are considered OK

# Winner threshold when running Bane's winner through Fw\Fm
COMP_BANE_FWFM_THLD = 0.6
# (v2) Fw/Fw winner threshold when comparing winners through Fw/Fm
COMP_FWFM_WINNER_THLD = 0.03
# Str dist thld when comparing bane->fwfm and fwfm winners
COMP_FWFM_THLD = 0.9
# Comp score returned if there's a Bane-FwFm match
COMP_FWFM_MATCH_SCORE = 0.8
# Comparison score if both str_dist and fwfm comparisons failed
COMP_NO_MATCH_SCORE = 0
# Comparison score multiplier when participating in overall confidence calculation
COMP_SCORE_CONF_MULTIPLIER = 0.25

# Penalty if one of winners (Bane or FwFm) is NULL
BANE_ONLY_PENALTY = 0.0  # current formula does not penalize bane only winner
FWFM_ONLY_PENALTY = 0.0
DUAL_WINNER_BONUS = 0.3

# Penalty if all the cleaners fail except the last one
CLEANER_NONE_PENALTY = -0.3

# Penalty if input domain is parked
PARKED_DOMAIN_PENALTY = 0.0

# If bane winner & fwfm winner exist but there’s no match overall:
# pick the winner with the highest norm score
# after rewarding bane_norm_score with + 0.3 temporarily
BANE_NO_MATCH_REWARD = 0.3

# Ultimate confidence level to accept results
CONF_OK = 0.25

# Values for verbose confidence representation
CONF_LEVEL_LOW = 0.61
CONF_LEVEL_MEDIUM = 0.89  # Make sure it's higher than CONF_LEVEL_LOW!

# Threshold levels for entity comparison by string
LEV_MIN_SCORE = 0.9
JW_MIN_SCORE = 0.9


# ---- API settings ----

# Suppress the flask-sqlalchemy warining:
#    FSADeprecationWarning: SQLALCHEMY_TRACK_MODIFICATIONS adds significant
#    overhead and will be disabled by default in the future.
#    Set it to True or False to suppress this warning
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Set to True to echo SQL statements.
SQLALCHEMY_ECHO = False

X_3SCALE_SHARED_SECRET = os.getenv('X_3SCALE_SHARED_SECRET')

JSON_SORT_KEYS = False
JSONIFY_PRETTYPRINT_REGULAR = os.environ.get('JSONIFY_PRETTYPRINT_REGULAR', False)

# Max no. of input strings the API will accept.
MAX_INPUTS = 1
# Max length of each input
MAX_INPUT_LENGTH = 127
