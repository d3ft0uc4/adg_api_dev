"""
© 2018 Alternative Data Group. All Rights Reserved.

REST API main app (Flask)
"""
from multiprocessing import Process
from traceback import format_exc
import logging
import json

from flask import Flask, jsonify, make_response
from marshmallow import Schema, fields
from marshmallow.exceptions import ValidationError
from OpenSSL import SSL
from webargs.flaskparser import parser, use_args
from werkzeug.exceptions import BadRequest
from pyinstrument import Profiler

from api.app import config
# from api.app.methods.mapper import mapper_api
from api.app.exceptions import APIException, APIValidationException, APIInternalException
from api.app.helpers.shared_secret_auth import auth_required
from pii.pii import is_pii
from mapper.mapper import map_inputs_to_response

from packages.utils import StrFallbackJSONEncoder, LOCAL, LocalsThreadPoolExecutor, apply_configuration
from domain_tools.utils import is_domain_input
from mapper.settings import PARALLEL
from bulk_api_test.api_qa import get_last_results, run_tests as bulk_api_tests

logger = logging.getLogger(f'adg.{__name__}')

context = SSL.Context(SSL.TLSv1_2_METHOD)
# context.use_privatekey_file('/home/ec2-user/privatekey.pem')
# context.use_certificate_file('/home/ec2-user/server.crt')


application = Flask(__name__)
application.config.from_object(config)
application.json_encoder = StrFallbackJSONEncoder


@application.route('/health', methods=["GET"])
@application.route('/health-debug', methods=["GET"], defaults={'debug': True})
@auth_required
def health(debug=False):
    """
    GET /health

    Responds to authorized request with OK. """
    from flask import request

    message = {'message': "OK"}

    if debug:
        from pprint import pformat

        message['debug'] = {
            'X-Real-Ip header': pformat(request.headers.get('X-Real-Ip', 'Not provided')),
            'X-Forwarded-For header': pformat(request.headers.get('X-Forwarded-For', 'Not provided')),
            'X-User-Key header': pformat(request.headers.get('X-User-Key', 'Not provided')),
            'X_User_Key query param': pformat(request.args.get('X_User_Key', 'Not provided')),
            'app_key query param': request.args.get('app_key', 'Not provided')
        }

        logger.debug('X-Real-Ip header: ' + message['debug']['X-Real-Ip header'])
        logger.debug('X-Forwarded-For header: ' + message['debug']['X-Forwarded-For header'])
        logger.debug('X-User-Key header: ' + message['debug']['X-User-Key header'])
        logger.debug('X_User_Key query param: ' + message['debug']['X_User_Key query param'])
        logger.debug("app_key query param: " + message['debug']['app_key query param'])

    return make_response(
        jsonify(message),
        200
    )


@application.route('/get-golden-results', methods=["GET"])
@auth_required
def get_golden_results():
    """
    GET /get-golden-results

    Get latest test results for the golden set"""

    message = get_last_results()

    return make_response(
        jsonify(message),
        200
    )


@application.route('/run-golden', methods=["POST"])
@auth_required
def run_golden():
    """
    POST /run-golden

    Executing bulk API qa for the golden set (in background)
    """
    from flask import request

    headers = dict(request.headers)

    mode = headers.get('X-Mode')
    if mode:
        mode = mode.lower().strip()
        if mode not in ['domain', 'merchant', 'product']:
            raise APIValidationException(f'Unknown mode: {mode}')

    notes = headers.get('X-Notes')
    table_name = headers.get('X-Table', 'golden_set_domain_merchant')

    try:
        configurations = headers.get('X-Configurations', '[{}]')
        configurations = json.loads(configurations)
        if not isinstance(configurations, (list, tuple)):
            raise ValueError('Not a list')

    except Exception as exc:
        raise APIValidationException(f'Could not parse configurations: {configurations}; {exc}')

    t = Process(target=bulk_api_tests, args=(mode, notes, configurations, table_name))
    t.start()
    message = {'status': "STARTED"}

    return make_response(
        jsonify(message),
        200
    )


def input_not_empty(inp):
    if not inp:
        raise ValidationError(f'Empty inputs not allowed')


def input_length(inp):
    if not len(inp) <= config.MAX_INPUT_LENGTH:
        raise ValidationError(f'Input {inp} length is bigger than max possible length ({config.MAX_INPUT_LENGTH})')


def one_or_more_inputs(inputs):
    if not len(inputs) > 0:
        raise ValidationError('No inputs to process')


# TODO: Why is this needed if we have input_length ?
def inputs_length(inputs):
    for inp in inputs:
        input_length(inp)


def max_inputs(inputs):
    if len(inputs) > config.MAX_INPUTS:
        raise ValidationError(f'Too many input strings, max {config.MAX_INPUTS}')


# TODO: Why is this needed if we have input_not_empty ?
def not_empty_inputs(inputs):
    for inp in inputs:
        input_not_empty(inp)


class MapperSchema(Schema):
    """ Schema for the mapper request parameters """

    class Meta:
        strict = True

    inputs = fields.List(
        fields.String(),
        required=True,
        validate=[one_or_more_inputs, max_inputs, not_empty_inputs, inputs_length],
    )


@application.route('/pii-ident', methods=['POST'])
@application.route('/pii-ident-debug', methods=['POST'], defaults={'debug': True})
@auth_required
@use_args(MapperSchema(), locations=['force_json'])
def pii(args, debug=False):
    """
    POST /pii-ident

    This module detects pii in text.
    """
    try:
        results = []
        for input in args.get('inputs', []):
            res = is_pii(input)
            data = {
                'Original Input': input,
                'PII Detected': res['match'],
            }
            if debug:
                data['_DEBUG'] = res

            results.append(data)

        return jsonify(results)
    except Exception:
        logger.error(f'Pii error for {args}: {format_exc()}')
        raise APIInternalException(details=format_exc() if debug else None)


# merchant
@application.route('/merchant-mapper', methods=['POST'], defaults={'mode': 'merchant'})
@application.route('/merchant-mapper-plus', methods=['POST'], defaults={'mode': 'merchant', 'output_graph': True})
@application.route('/merchant-mapper-debug', methods=['POST'], defaults={'mode': 'merchant', 'debug': True})
# domain
@application.route('/domain-mapper', methods=['POST'], defaults={'mode': 'domain'})
@application.route('/domain-mapper-plus', methods=['POST'], defaults={'mode': 'domain', 'output_graph': True})
@application.route('/domain-mapper-debug', methods=['POST'], defaults={'mode': 'domain', 'debug': True})
# product
@application.route('/product-mapper', methods=['POST'], defaults={'mode': 'product'})
@application.route('/product-mapper-plus', methods=['POST'], defaults={'mode': 'product', 'output_graph': True})
@application.route('/product-mapper-debug', methods=['POST'], defaults={'mode': 'product', 'debug': True})
# combined
@application.route('/mapper', methods=['POST'], defaults={'mode': 'auto'})
@application.route('/mapper-plus', methods=['POST'], defaults={'mode': 'auto', 'output_graph': True})
@application.route('/mapper-debug', methods=['POST'], defaults={'mode': 'auto', 'debug': True})
@auth_required
@use_args(MapperSchema(), locations=['force_json'])
def mapper(args, mode, debug=False, output_graph=False):
    from flask import request

    try:
        profiler_fmt = request.headers.get('X-Profile')
        profiler = Profiler() if debug and profiler_fmt else None
        if profiler:
            profiler.start()

        configuration = {}
        if debug:
            try:
                configuration = json.loads(request.headers.get('X-Configuration', '{}'))
                if not isinstance(configuration, dict):
                    raise ValueError('Not a dict')

            except Exception as exc:
                raise APIValidationException(f'Could not parse configuration:\n{configuration}\n{exc}')

        inputs = args.get('inputs', [])

        LOCAL.request = {
            'headers': dict(request.headers),
            'args': request.args,
            'url': request.url,
            'data': request.data,
            'referrer': request.referrer,
            'remote_addr': request.remote_addr,
            'view_args': request.view_args,
            'configuration': configuration
        }

        mode_header = dict(request.headers).get('Mode')
        if mode == 'auto' and mode_header:
            mode = mode_header.lower()

        # set "X-Input: List of company names" header in order to disable cleaners
        configuration['INPUT_TYPE'] = request.headers.get('X-Input-Type', '').strip().lower()

        apply_configuration(configuration)

        if mode == 'auto':
            data = get_data_mixed_mapper(inputs, debug=debug, output_graph=output_graph)
        else:
            data = map_inputs_to_response(inputs, mode=mode, debug=debug, output_graph=output_graph)

        if profiler:
            profiler.stop()

            kwargs = {}
            if profiler_fmt == 'text':
                kwargs = {'unicode': True, 'color': True}

            res = profiler.output(profiler_fmt, **kwargs)
            return make_response(res)

        return jsonify(data)

    except Exception:
        logger.error(f'{mode} mapper error for {args}: {format_exc()}')
        raise APIInternalException(details=format_exc() if debug else None)


def get_data_mixed_mapper(inputs, *args, **kwargs):
    # TODO: add products detector
    urls, merchants, urls_pos, merchant_pos = splitter(inputs, is_domain_input)
    with LocalsThreadPoolExecutor(max_workers=2 if PARALLEL else 1, thread_name_prefix='MixedMapperPool') as pool:
        domain_future = pool.submit(map_inputs_to_response, urls, mode='domain', *args, **kwargs)
        merchant_future = pool.submit(map_inputs_to_response, merchants, mode='merchant', *args, **kwargs)

        domain_output = domain_future.result()
        merchant_output = merchant_future.result()

    data = [None] * len(inputs)
    for idx, pos in enumerate(urls_pos):
        data[pos] = domain_output[idx]
    for idx, pos in enumerate(merchant_pos):
        data[pos] = merchant_output[idx]
    return data


def splitter(data, pred):
    yes, no, yes_pos, no_pos = [], [], [], []
    for idx, d in enumerate(data):
        if pred(d):
            yes.append(d)
            yes_pos.append(idx)
        else:
            no.append(d)
            no_pos.append(idx)
    return yes, no, yes_pos, no_pos


#
# Parser settings
#


@parser.location_handler('force_json')
def json_parser(request, name, field):
    """
    Same as webargs' 'json' location, but doesn't require any Content-Type headers.
    Content-Type is always treated as 'application/json'.
    Also doesn't use any fields and takes entire json input as argument value.
    """
    try:
        return request.get_json(force=True)
    except BadRequest as exc:
        raise APIValidationException(exc.description)


#
# Error handling
#

@parser.error_handler
def handle_error(error, req, schema):
    """
    Custom error handling when args validation fails.
    """
    # concatenate all errors in a big error string
    errors = ';'.join(';'.join(err for err in err_list) for err_list in error.messages.values())
    raise APIValidationException(errors)


# This is instead of assigning all possible codes manually, like
#  @application.errorhandler(400)
#  @application.errorhandler(404)
#  ...
def handle_http_exception(exc):
    return make_response(
        jsonify(
            name=exc.name,
            error=exc.description
        ),
        exc.code
    )


application.handle_http_exception = handle_http_exception


@application.errorhandler(APIException)
def handle_api_exception(exc):
    """ Converts API errors to json. """
    return make_response(
        jsonify(**{
            'name': exc.name,
            'error': exc.message,
            'details': exc.details,
            **({'hint': exc.hint} if exc.hint else {})
        }),
        exc.code
    )


@application.after_request
def after_request(response):
    """ Updates response headers """

    # Allow cross-origin requests.
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization,X-3scale-proxy-secret-token')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    # Close connection after each request (turn off KeepAlive).
    # response.headers.add('KeepAlive', 'off')

    return response


if __name__ == '__main__':
    application.run(
        host='0.0.0.0',
        ssl_context='adhoc',
        debug=False
    )
