drop view if exists combimed_merch_to_match;

create view combined_merch_to_match as

(
        select bing.merchant_string as merchant_string, amm.uidlist_agg_merch_master_top_merchants as uidlist_agg_merch_master_top_merchants
        , (case when bing.bing_api_type = 'entity-entity' 
                then 'BEF-ENTITY'
                else 'BEF-WEB'
          end) as source
        , bing.uid as source_uid
        , bing.entity_name as source_entity_name
        , mt.match_target_unofficial as match_target
        , groups.cluster_id as cluster_id
        
        from match_targets_unofficial_map as mt
                
        left join bing_entityfinder_output as bing
                on bing.entity_name = mt.match_target
        
        left join agg_merch_master_top_merchants_unique as amm
                on bing.merchant_string = amm.merchant_string
        
        left join FW_groups as groups
                on groups.match_target = mt.match_target_unofficial
                                
        where bing.run_code = 'full_run_2' and bing.uid is not null

) union (

        select wf.input_string as merchant_string, amm.uidlist_agg_merch_master_top_merchants as uidlist_agg_merch_master_top_merchants
        , 'WF' as source
        , wf.uid_wf_run as source_uid
        , wf.child_name as source_entity_name
        , mt.match_target_unofficial as match_target
        , groups.cluster_id as cluster_id
        
        from match_targets_unofficial_map as mt
                
        left join wf_output_unique_merch_tbl as wf
                on wf.child_name = mt.match_target
        
        left join agg_merch_master_top_merchants_unique as amm
                on wf.input_string = amm.merchant_string

        left join FW_groups as groups
                on groups.match_target = mt.match_target_unofficial
                
        where wf.uid_wf_run is not null
);


drop view if exists composite_model_output;

create view composite_model_output as 
select merchant_string, uidlist_agg_merch_master_top_merchants, match_target, cluster_id from
       (select merchant_string, uidlist_agg_merch_master_top_merchants, match_target, cluster_id
               , case when count(*) > 1 then max(source_uid)
                 else source_uid
                 end source_uid
        from combined_merch_to_match_tbl
        group by uidlist_agg_merch_master_top_merchants, merchant_string, match_target, cluster_id) as uniques;



drop table if exists combined_merch_to_match_tbl;
create table combined_merch_to_match_tbl as select * from combined_merch_to_match;

drop table if exists composite_model_output_tbl;
create table composite_model_output_tbl as select * from composite_model_output;

