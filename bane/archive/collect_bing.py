"""
© 2018 Alternative Data Group. All Rights Reserved.

Creates JSON fixture with Bing API Responses for input strings in bane/files/inputs/sample_inputs.txt into
bane/files/fixture/bing_responses.json (to avoid using the API when testing Bane).

Run from repo root.

FIXME: Keep but make this work with csv, not shelvedb
"""

import os.path
import json
import time

from bing.bing_web_core import BingWebCore


def _bing_query(input_string):
    """
    Run a single query to Bing and return run data in Bane internal fomat.

    - input_string - Raw input string to query Bing for.

    Returns:
        dict with intermediate run data in the same format bane.bane uses internally.
    """
    api = BingWebCore()
    results = api.query(input_string)

    return {
        'input': input_string,
        'bing_api_type': api.bing_api_type(),
        'query_results': results
    }


with open('bane/files/inputs/sample_inputs.txt') as fp:
    doc = fp.readlines()
    inputs = [x.replace('"', '').replace(',', '').rstrip('\n') for x in doc]

if os.path.exists("bane/files/fixture/bing_responses.json"):
    print("API fixture file bane/files/fixture/bing_responses.json exists. Delete/Rename it and try again.")
    exit(0)

with open("bane/files/fixture/bing_responses.json", "w") as fp:
    bing_responses = {}

    for line in inputs:
        time.sleep(.2)
        bing_result = _bing_query(line)
        bing_responses[bing_result['input']] = bing_result

    fp.write(json.dumps(bing_responses))
