"""
Module for checking entity to be an organization.
Works like a veto: if `is_organization` returns True, then we are pretty sure it's an organization.
However, if `is_organization` is False, we just __aren't_sure__ whether it is an org or not.

CLI:
    python -m api.app.helpers.organization_entity_check "Hollywood Casino Lawrenceburg"
"""

import logging
import re

from bing.bing_web_entity import BingWebEntity
from traceback import format_exc

from mapper.settings import COMPANY_TERMINATORS
from packages.utils import pretty

logger = logging.getLogger(f'adg.{__name__}')
bing = BingWebEntity()

DEBUG = __name__ == '__main__'

ORGANIZATION_HINTS = {'Organization', 'Attraction', 'Place'}  # TODO: Generic?


def get_type_hints(entity):
    response = bing.query(entity)
    if DEBUG:
        logger.debug(pretty(response))

    entity_type_hints = []
    for data in response.get('entities', {}).get('value', []):
        try:
            type_hints = data['entityPresentationInfo'].get('entityTypeHints', [])
            entity_type_hints.extend(type_hints)

            type_display_hint = data['entityPresentationInfo'].get('entityTypeDisplayHint')
            if not type_hints and type_display_hint:
                entity_type_hints.append(type_display_hint)

        except Exception:
            logger.warning(
                f'OrganizationEntityCheck could not retrieve entityTypeHints for "{entity}": {format_exc()}'
            )

    return entity_type_hints


def is_organization(entity: str, use_bing: bool = True, strict: bool = False):
    """
    Checks whether entity is an organization using BEF (bing_entity_entity).

    If BEF's `entityTypeHints` field is NULL or contains any of ORGANIZATION_HINTS anywhere
    then we are sure entity is an organization and return True. Otherwise, return False.
    """
    if re.search(r'\s(' + '|'.join(map(re.escape, COMPANY_TERMINATORS)) + r').?$', entity, flags=re.IGNORECASE):
        return True

    if not use_bing:
        return False

    entity_type_hints = get_type_hints(entity)
    logger.debug(f'Entity type hints for "{entity}": {entity_type_hints}')
    is_org = bool(set.intersection(set(entity_type_hints), ORGANIZATION_HINTS))
    if not strict:
        return not entity_type_hints or is_org
    elif strict:
        return len(entity_type_hints) > 0 and is_org


if __name__ == '__main__':
    from argparse import ArgumentParser
    import json

    parser = ArgumentParser()
    parser.add_argument('entity', type=str)
    parser.add_argument('--strict', action='store_true', default=False)
    args = parser.parse_args()

    print('Is organization: ' + str(is_organization(args.entity, strict=args.strict)))
