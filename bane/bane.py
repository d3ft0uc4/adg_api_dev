"""
© 2018 Alternative Data Group. All Rights Reserved.

Run module as a script from root dir to print the resulting dataframe for a single query:
    python3 -m bane.bane --input "Wal*Mart Inc." \
                         --cbfname "wal_mart_inc" \
                         --fixture no
                         --winstr "zscore"

    Where
    --input     Single input string
    --cbfname   To cache intermediate data (including input and Bing search results) to a given JSON file name in
                bane/files/run_data/  e.g. "wal-mart_inc.json"  Only valid along with a single --input
    --bulk      Path to file to read bulk input strings from (bane/files/inputs/0-sample_inputs.txt by default)
    --cached    To run process on intermediate run data cached to file(s) in bane/files/run_data/ previously by using
                the --cbfname option. Writes results to bane/files/csv/cached_run.csv. Ignored if --input is present.
    --fixture   Path to file with stored Bing search results. Used for mocking the API in testing this tool.
                Send "no" to avoid default: bane/files/fixture/bing_responses.json  Ignored if --cached is present
                > Use bane.collect_bing in order to create it in the first place.
    --winstr    Winner picking strategy. See Bane::winner_methods for options. Default: "zscore"
    --skip_zs   When winstr is "zscore", whether to skip z-score calculations and adjustment. (See Bane::_winner_zscore)
    --zs_factor Factor/multiplier for final score adjustment (when not skipping z-scoring)  Default: 1

> Run script with no arguments to read input strings from --bulk default and write results to bane/files/csv/
"""

import json
import os

from operator import itemgetter
import pandas as pd
from mapper.settings import Configurable, BaneConfig
from packages.utils import with_timer, strip_accents, pretty
from collections import defaultdict

import logging

logger = logging.getLogger(f'adg.{__name__}')
DEBUG = __name__ == '__main__'


@Configurable(default_config=BaneConfig)
class Bane:
    """
    Allows to use BANE (Bing name-about Entity) method to find an entity name from a raw input string.

    - winner_methods - winner picking method references to chose from.
    - bing_api - BingWebCore instance.

    See constructor for info. on the other class properties.
    """

    winner_methods = {}
    bing_api = None

    winner_strategy = None
    winner_args = {}
    api_fixture = None

    def __init__(self, winner_strat='zscore', winner_args={}, api_fixture=False):
        """
        Sets winner_functions.

        - winner_strat - will be the self.winner_strategy method. (See `self.winner_methods`)
        - winner_args - MUST have arguments for `self.winner_strategy` (if any).
        - api_fixture - File with Bing API mock of web search results for input string(s).
          (Saved in same format as entire `run_data` below.)
        """
        from bing.bing_web_core import BingWebCore

        # TODO: Leave only useful winner methods once merged and evaluated:
        self.winner_strategy = getattr(self, f'_winner_{winner_strat}', None)
        assert self.winner_strategy

        if winner_args:
            self.winner_args = winner_args

        if api_fixture:
            assert os.path.isfile(api_fixture)
            self.api_fixture = api_fixture
        else:
            # FIXME: Should we use singleton here? And/or use results caching from BingWebCore.
            self.bing_api = BingWebCore()

    @classmethod
    def _get_names_and_locations(cls, bing_result):
        """
        Extracts all names from bing_results and returns their locations.

        - bing_result - Single Bing query result (in 'query_results'->'webPages'->'value' of API response):
            [
                {'about': {
                    'name': 'Some name',
                    ...
            ]

        Returns:  dict of names and their locations:
            {   'Name A': [1, 3, 6],
                'Name B': [2, 4, 5, 8],
                ...
            }
        """
        assert bing_result
        # FIXME: Better validation?

        result = defaultdict(list)
        for i, value in enumerate(bing_result, start=1):
            for about in value.get('about', []):
                if 'name' in about:
                    normalized_name = strip_accents(about['name'])
                    result[normalized_name].append(i)

        return result

    # TODO: Should include breaktie and threshold.
    @staticmethod
    def _winner_count(names_locs):
        """
        Simple winner picking strategy: Returns the name with the most entries (locations) found in Bing results.
        > Useful for comparing accuracy vs. more sophisticated winner pickingfunctions.
        Known problem: There's no tie breaks, returns arbitrarily (first) among ties.

        - names_locs - Processed results (names and their locations):
            {
                'Name A': [1, 3, 45],
                'Name B': [0, 2],
            }

        Returns:  tuple with str name picked as winner, and dict of intermediate steps
        """
        assert dict == type(names_locs)

        from operator import itemgetter

        loc_counts = [(name, len(locations)) for name, locations in names_locs.items()]
        loc_counts.sort(key=itemgetter(1), reverse=True)
        winner = loc_counts[0][0]

        return winner, {'Sorted location counts': loc_counts, "Winner's count": loc_counts[0][1]}

    # TODO: Remove when `_winner_count` is updated.
    @staticmethod
    def _winner_breaktie(names_locs):
        """
        Simple winner picking strategy: Picks name with the most entries, and can break ties.
        The highest ranking (1 > 10 in search results) breaks tie:
            >>> [2, 50] < [16, 17] == True
        e.g. 2 is higher ranking than 16 and 17 in search results

        - names_locs - Processed results (names and their locations):
            {
                'Name A': [1, 3, 45],
                'Name B': [0, 2],
            }

        Returns:  str name picked as winner
        """
        assert names_locs

        winner = None

        loc_count = 0
        best_rank = 0
        for name, rank in names_locs.items():
            new_loc_count = len(rank)
            if new_loc_count > loc_count:
                loc_count = new_loc_count
                best_rank = rank
                winner = name
            if new_loc_count == loc_count and rank < best_rank:
                loc_count = new_loc_count
                best_rank = rank
                winner = name

        return winner, {}

    # TODO: Remove when `_winner_count` is updated.
    @staticmethod
    def _winner_threshold(names_locs, thold=1.54):
        """
        Simple winner picking strategy with an added threshold filter.
        Picks name with the most entries after threshold.
        the highest ranking (1 > 10 in search results) breaks tie
        Returns None is if zero results are found.

        - names_locs - Processed results (names and their locations):
            {
                'Name A': [1, 3, 45],
                'Name B': [0, 2],
            }
        - thold - int Threshold level, 1.54 by default

        Returns:  str name picked as winner
        """
        assert names_locs

        import math

        winner = None

        # acceptable range may be found between 0.45-1.54
        # this just means a score of > 4 responses
        # if less than four repsonses return none
        score = math.log10(len(names_locs.values()))  # Log may not be necessary since we're using integers
        if score <= thold:
            return None
        loc_count = 0
        best_rank = 0
        for name, rank in names_locs.items():
            new_loc_count = len(rank)
            if new_loc_count > loc_count:
                loc_count = new_loc_count
                best_rank = rank
                winner = name
            if new_loc_count == loc_count and rank < best_rank:
                loc_count = new_loc_count
                best_rank = rank
                winner = name

        return winner, {}

    # TODO: Should include breaktie and threshold.
    @staticmethod
    def _winner_logsum(names_locs):
        """
        Intermediate winner picking strategy: Sums the inverse of the rankings, takes the log. (See `new_logsum`.)
        Higher rankings result in > log score. Highest score wins.

        - names_locs - Processed results (names and their locations):
            {
                'Name A': [1, 3, 45],
                'Name B': [0, 2],
            }

        Returns:  str name picked as winner
        """
        assert names_locs

        from numpy import log

        winner = None

        logsum = -1000
        for name, ranks in names_locs.items():
            new_loc_count = len(ranks)
            if new_loc_count > 0:
                # This is where the magic happens.
                new_logsum = log(sum([1.0 / x for x in ranks]))
            elif new_loc_count == 0:
                new_logsum = -1001

            # FIXME: new_logsum might be refd before assignment.
            if new_logsum > logsum:
                logsum = new_logsum
                winner = name

        return winner, {}

    def _winner_zscore(self, names_locs, weight_tpl=(-2.298, 9.7801), zscore=False, adjust_zs=1, th_zscore=None,
                       th_sum=None):
        """
        Sophisticated winner picking function with standard (z-)scores.

        - names_locs - dict of processed Bing results (about:names and their locations in search result rankings):
            {
                'Name A': [1, 3, 45],
                'Name B': [0, 2],
            }
        - weight_tpl - Tuple with 2 values (a, b) for logarithmic weighting function  s(r) = a * logn(r) + b
                          Default: (-2.298, 9.7801)
        - zscore - Whether to apply z-score (See step 3) or just use the summed scores after repetition penalization
                   (See steps 1 and 2) as final scores.
                   Default: False  (It also forces False when there's less than 4 candidates in `names_locs`)
        - adjust_zs - Z-score adjustment factor. Only used `if zscore`. (See step 3.5)  Default: 1
        - th_zscore - Value to filter out zscore-adjusted scores (when `zscore`, see step 4.1)  Default: 5.8
        - th_sum - Value to filter out summed, penalized scores (when `not zscore`, see step 4.2)  Default: 5.8

        Returns:  Tuple with str name picked as winner,
                  and dict `steps` of intermediate calculation stages:
                  (Each key below dict with a list or scalar per name:about.)
                    Weighted (simple) scores:       'scores' list per name. results weighted according to their rank in
                                                    the search results. (See `weight_tpl` in arguments above.)
                    Scores (repetition penalized):  'scores_norpt' list per name. Repeated scores are penalized.
                    Summed scores:                  'scores_sum' scalar per name. Sums all scores for each name together
                                                    See step 3.1
                    Standard (z-)score:             'z-score' scalar per name. Z-score for each name `if zscore`.
                                                    See step 3.
                    Final score (filtered):         'final' score per name. See steps 3.1 and 3.5
                                                    Filtered by appropriate threshold, see steps 4.1 and 4.2
                  > Additionally, the dict will contain a 'Mean' entry with the average, and 'Std Dev' with the std.
                    deviation of all penalized scores in all names (see step 3);
                    As well as the "Winner's final score" and "Winner's normalized score" (0.0-1 range).
        """

        # set defaults
        th_zscore = th_zscore or self.config.THLD_ZSCORE
        th_sum = th_sum or self.config.THLD_SUM

        assert isinstance(names_locs, dict)

        # FIXME: Verify and validate self.winner_args which have been expanded into
        #        weight_tpl, zscore, adjust_zs, th_zscore, th_sum.

        import numpy as np

        def penalize_dupes(scores, penalty=self.config.ZSCORE_PENALTY):
            """
            Penalizes duplicate scores by multiplying each further repetition by 0.666 (default).
            FIXME Send `penalty` in ^ method params?

            - scores - list of scores to penalize
            - penalty - float fator to multiply repeated scores by

            Returns:  list with penalized scores  (Same length as `scores`)  Preserves order.
            """
            assert list == type(scores)
            assert float == type(penalty)

            if len(scores) == 1:
                return scores

            penalized_scores = []
            repetitions = {}
            for score in scores:  # Ø n
                # Penalize repeated scores
                penalized_scores.append(score if score not in repetitions else penalty ** repetitions[score] * score)

                # Keep track of how many time each score value is repeated.
                repetitions[score] = 1 if score not in repetitions else repetitions[score] + 1

            return penalized_scores

        def normalize_bane_zscore(score, min_score_thld, certain_thld=self.config.HI_SCORE):
            """ Normalizes Bane's score (which may be approximately 0-160). """

            return (min(score, certain_thld) - min_score_thld) / (certain_thld - min_score_thld)

        name_steps = {}

        for name, ranks in names_locs.items():  # Ø n
            name_steps[name] = {}

            # 1. Rank adjustment: Rankings (r) need to be weighted into scores (s).
            name_steps[name]['scores'] = list(map(lambda x: weight_tpl[0] * np.log(x) + weight_tpl[1], ranks))

            # 2. Penalize repeated scores, multiplying each further repetition by 0.666.
            name_steps[name]['scores_norpt'] = penalize_dupes(name_steps[name]['scores'])

            # 3. Z-score calculation and adjustment:

            # 3.1 x(s) = Sum scores for each (name:about) key.
            name_steps[name]['scores_sum'] = sum(name_steps[name]['scores_norpt'])

        # NOTE: If less than 4 candidates (names), we'll skip the remaining zscore-related calculations.
        if 3 >= len(names_locs):
            zscore = False

        mean = None
        std = None
        if zscore:
            _name_sums = [ns['scores_sum'] for ns in name_steps.values()]  # Ø n fast

            # 3.2 m = Take mean of all sums in 3.1
            mean = np.mean(_name_sums)

            # 3.3 ∂ = Take stdev of all sums in 3.1
            std = np.std(_name_sums)

            for name, ranks in names_locs.items():  # Ø n

                # 3.4 Calc. std scores  z(s) = ( x(s)-m ) / ∂
                name_steps[name]['z-score'] = (name_steps[name]['scores_sum'] - mean) / std

                # 3.5 Adjust score sums by adding them to the product of the corresponding z-scores and `adjust_zs`.
                name_steps[name]['final'] = name_steps[name]['scores_sum'] + adjust_zs * name_steps[name]['z-score']

            # (4.1 `if zscore`, apply `th_zscore`...)
            th = th_zscore

        else:
            for name, ranks in names_locs.items():  # Ø n
                # NOTE: `if not zscore`, summed scores are considered final.

                name_steps[name]['z-score'] = None  # Skips z-scoring.
                name_steps[name]['final'] = name_steps[name]['scores_sum']

            # (4.2 `if not zscore`, apply `th_sum`...)
            th = th_sum

        # 4. Threshold to filter out candidates with "low scores".xw
        for name, ranks in names_locs.items():  # Ø n
            if name_steps[name]['final'] < th:
                name_steps[name]['final'] = 0

        # Pick the winner. (None if all candidates were filtered out by threshold.)
        winner_name_steps = sorted(name_steps.items(), key=lambda bn: bn[1]['final'], reverse=True)[0]
        winner = None if 0 == winner_name_steps[1]['final'] else winner_name_steps[0]

        # Transpose name-steps dict into step-names.
        # FIXME: Better way? Similar to stackoverflow.com/questions/5558418
        # FIXME: Use more pythonic keys?
        step_names = {
            # Dicts:
            'Weighted (simple) scores': {}, 'Scores (repetition penalized)': {}, 'Summed scores': {},
            'Mean': mean, 'Std Dev': std,
            'Standard (z-)score': {}, 'Final score (filtered)': {},
            'Normalized score': {},
            # Scalars:
            # 'Min. score threshold': th,
            "Winner's final score": winner_name_steps[1]['final'] if winner else None,
            "Winner's normalized score": normalize_bane_zscore(winner_name_steps[1]['final'], th) if winner else None,
        }
        for name, steps in name_steps.items():  # Ø n
            step_names['Weighted (simple) scores'][name] = steps['scores']
            step_names['Scores (repetition penalized)'][name] = steps['scores_norpt']
            step_names['Summed scores'][name] = steps['scores_sum']
            step_names['Standard (z-)score'][name] = steps['z-score']
            step_names['Final score (filtered)'][name] = steps['final']
            step_names['Normalized score'][name] = normalize_bane_zscore(steps['final'], th)

        return winner, step_names

    def get_row_from_data(self, run_data, cache_bing_fname=None):
        """
        Processes Bing search query results to construct a dict of names and their locations and select a
        winner–name with max number of locations.
        Uses `_get_names_and_locations`.

        - run_data - dict with response from Bing in this format:
            {   'query_results':
                    'webPages': {
                        {   'value': [
                            {   ... # sections of each search result
                                'about': {
                                    'name': 'Some name',
                                    ... # about may have several 'name' entries
            }
        - cache_bing_fname - File name to save `run_data` (including Bing web search).  **Only takes effect when
          input_string is also provided.**

        Returns:
            dict {
                'original_query': 'string',
                'names_locs': {
                    'Name A': [1, 3, 5, 6],
                    'Name B': [2, 7, 8]
                },
                'steps': { ... }  # See winner methods, each is different.
                'winner': "Name",
            }
        """

        names_locs = True  # This signals that we need to search for names_locs later.

        try:
            assert run_data['query_results']['webPages']['value']
        except KeyError as ke:
            # DEBUG: Warning
            print("Bane: [WARN] No search results for input string '{}'".format(
                run_data['query_results']['queryContext']['originalQuery']), ke)
            names_locs = False
            # NOTE ^ if False == names_locs there were no search results.

        if names_locs:
            names_locs = self._get_names_and_locations(run_data['query_results']['webPages']['value'])
            # NOTE ^ if None == names_locs there were no name:about keys.

        if names_locs:
            if self.winner_args:
                winner, steps = self.winner_strategy(names_locs, **self.winner_args)
            else:
                winner, steps = self.winner_strategy(names_locs)
        else:
            # DEBUG: Warning
            logger.debug(f'No name:about keys in search results for input string "{run_data["input"]}"')
            winner, steps = [None, None]

        # Cache single input run_data into JSON file if `cache_bing_fname` provided.
        if cache_bing_fname:
            try:
                if not os.path.isdir("bane/files/run_data/"):
                    os.makedirs("bane/files/run_data/")
                with open("bane/files/run_data/{}.json".format(cache_bing_fname), "w") as fp:
                    fp.write(json.dumps(run_data))
            except Exception as ex:
                print(f'Bane: [ERROR] Can NOT use file name bane/files/run_data/{cache_bing_fname}.json', ex)

        if names_locs is False:
            names_locs = None  # to avoid sending back a bool value.

        return {
            "original_query": run_data['query_results']['queryContext']['originalQuery'],  # Input String
            "names_locs": names_locs,  # Name-location dict
            "steps": steps,  # Intermediate steps
            "winner": winner  # Winner
        }

    def get_row_from_file(self, file_path):
        """
        Processes Bing search query results from file (JSON format) to construct a dict of names and their
        locations and select a winner–name with max number of locations.
        Uses `_get_names_and_locations`.

        - file_path - Absolute path to file with response from single query to Bing in JSON format:
            {   'input': "INPUT STRINGS"
                'bing_api_type': "web-core"
                'query_results':
                    'webPages': {
                        {   'value': [
                            {   ... # sections of each search result
                                'about': {
                                    'name': 'Some name',
                                    ... # about may have several 'name' entries
            }

        Returns:
             dict {
                'file_name': 'string',
                'original_query': 'string',
                'names_locs': {
                    'Name A': [1, 3, 5, 6],
                    'Name B': [2, 7, 8]
                },
                'steps': { ... }  # See winner methods, each is different.
                'winner': Name,
            }
        """
        assert os.path.isfile(file_path)

        # Load file with run data (saved in get_results/get_row_from_data)
        with open(file_path, "r") as fp:
            try:
                run_data = json.loads(fp.read())
            except Exception as ex:
                print(f'Bane: [ERROR] {file_path} can NOT be opened as a JSON file.')

        assert run_data['query_results']['webPages']['value']

        names_locs = self._get_names_and_locations(run_data['query_results']['webPages']['value'])

        if self.winner_args:
            winner, steps = self.winner_strategy(names_locs, **self.winner_args)
        else:
            winner, steps = self.winner_strategy(names_locs)

        return {
            "file_path": file_path,  # File with intermediate results to process
            "original_query": run_data['query_results']['queryContext']['originalQuery'],  # Input String
            "names_locs": names_locs,  # Name-location dict
            "steps": steps,  # Intermediate steps
            "winner": winner  # Winner
        }

    @with_timer
    def get_results(self, input_string=False, cache_bing_fname=None, winner_strat=None, winner_args={}):
        """
        For a given input_string, perform a query to Bing and process the response.
        Or process all input strings from files in bane/files/run_data/ if no input_string is given
        FIXME: Accept multiple inputs?

        - input_string - str raw input string e.g. "WAL-MART INC"
        - cache_bing_fname - File name to save `run_data` (including Bing web search).
                             Only takes effect `if input_string`.
        - winner_strat - if sent, re-sets the `self.winner_strategy` strategy.
        - winner_args - if sent, re-sets arguments for `self.winner_strategy` (if any).

        Returns:  dict output from `get_row_from_data` if `input_string` sent,
                  or aggregated dict with results from `get_row_from_file`.
        """

        if winner_strat:
            assert winner_strat in self.winner_methods
            self.winner_strategy = self.winner_methods[winner_strat]

        if winner_args:
            self.winner_args = winner_args

        #
        # Single input process:
        #

        if input_string:

            if self.api_fixture:
                with open(self.api_fixture) as fpaf:
                    bing_responses_text = fpaf.read()
                    bing_responses_json = json.loads(bing_responses_text)
                    try:
                        results = bing_responses_json[input_string]['query_results']
                    except Exception as ex:
                        # DEBUG: Error
                        print(f"Bane: [ERROR] {input_string} ot found in Bing responses fixture. \
                                Searching with Bing...")
                        results = self.bing_api.query(input_string)
            else:
                results = self.bing_api.query(input_string)
                if DEBUG:
                    logger.debug(f'Bing results: {pretty(results)}')

            run_data = {
                'input': input_string,
                'bing_api_type': self.bing_api.bing_api_type() if self.bing_api else "Mock",
                'query_results': results
            }

            return self.get_row_from_data(run_data, cache_bing_fname)

        # else:

        #
        # Multi-file input process from intermediate data files:
        #

        assert os.path.isdir("bane/files/run_data/")

        result_lst = []

        for fname in os.listdir("bane/files/run_data/"):
            file_results = self.get_row_from_file("bane/files/run_data/" + fname)
            result_lst.append(file_results)

        return result_lst

    @with_timer
    def get_winner(self, clean_input: str) -> dict:
        """
        Given clean inputs, returns Bane output with corresponding scores.

        Args:
            clean_input: input string to run through Bane

        Returns:
            {
                'thld':
                'winners': {
                    'winner':
                    'score':
                    'norm_score':
                },
                'losers': {
                    ...
                },
                'bing_results':
            }
        """
        assert clean_input

        bane_result = self.get_results(clean_input)

        steps = bane_result.get('steps') or {}  # bane_steps may be None
        sorted_winners = sorted(
            [
                {'winner': winner, 'score': score, 'norm_score': steps['Normalized score'][winner]}
                for winner, score in steps.get('Final score (filtered)', {}).items()
            ],
            key=itemgetter('score'),
            reverse=True,
        )

        winners = [item for item in sorted_winners if item['norm_score'] >= self.config.WINNER_THLD]
        losers = [item for item in sorted_winners if item['norm_score'] < self.config.WINNER_THLD]

        if not winners:
            logger.debug(f'Bane produced empty result for "{clean_input}"')

        for winner in winners:
            winner['is_super_winner'] = float(winner.get('norm_score')) >= self.config.SUPERWINNER_THLD

        return {
            'thld': self.config.WINNER_THLD,
            'winners': winners,
            'losers': losers,
            'bing_results': len(bane_result.get('names_locs') or {}),  # ['names_locs'] may be None
        }


if '__main__' == __name__:
    import datetime

    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('--input', default=None)
    parser.add_argument('--cbfname', default=None)
    parser.add_argument('--bulk', default="bane/files/inputs/0-sample_inputs.txt")
    parser.add_argument('--cached', action='store_true', default=False)
    parser.add_argument('--fixture', default=None)  # 'bane/files/fixture/bing_responses.json'
    parser.add_argument('--winstr', default='zscore')
    parser.add_argument('--skip_zs', action='store_false')
    parser.add_argument('--zs_factor', default=1, type=float)
    args = parser.parse_args()

    winner_args = {}
    out_postfix = ''  # Used for output
    if 'zscore' == args.winstr:

        if args.skip_zs:
            winner_args['zscore'] = False
            out_postfix = out_postfix + "-nozscore"
        else:
            out_postfix = out_postfix + "-zscore"

        if args.zs_factor:
            winner_args['adjust_zs'] = args.zs_factor
            if not args.skip_zs:
                out_postfix = out_postfix + "-f" + str(args.zs_factor)

    else:
        out_postfix = out_postfix + '-' + args.winstr

    bane = Bane(winner_strat=args.winstr, winner_args=winner_args, api_fixture=args.fixture)

    df_columns = [
        "original_query",  # Input String
        "names_locs",  # Name-location dict
        "steps",  # Intermediate steps
        "winner"]  # Winner

    #
    # Process multiple input queries from bane/files/run_data/ files (if any):
    #

    if args.cached:
        result_list = bane.get_results()

        # TODO: Expand `steps`. (See "Process single or bulk inputs" below.)

        result_df = pd.DataFrame(result_list, columns=df_columns)
        # result_df.to_clipboard()  # DEBUG

        csvout_fname = f'{datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S")}_cached_run{out_postfix}.csv'
        result_df.to_csv(f"bane/files/csv/{csvout_fname}")
        print(f"Results written to bane/files/csv/{csvout_fname}")

        exit(0)

    #
    # Process single or bulk inputs from args.bulk file:
    #

    # else:

    if args.input:
        input_strings = [args.input]
        print(f'Processing "{input_strings[0]}"')
        single_fname = "".join([c for c in args.input if c.isalpha() or c.isdigit() or c == ' ']).rstrip()[:8]
        # NOTE: ^ from https://stackoverflow.com/a/7406369/761963
    else:
        assert args.bulk
        with open(args.bulk) as fp:
            input_strings = [x.strip() for x in fp.readlines()]
        print(f'Processing {len(input_strings)} input_strings...')

    istr = 1

    result_lst = []
    for input_str in input_strings:

        if istr % 50 == 0:
            print(istr)
        else:
            print('.', end='')
        istr += 1

        result_dict = bane.get_results(input_str)

        # Expand `steps` into individual columns for dataframe conversion below.
        if result_dict['steps']:
            steps = result_dict['steps']
            df_columns = ['original_query', 'names_locs']
            for step, values in result_dict['steps'].items():
                df_columns.append(step)
                result_dict[step] = values
            df_columns.append('winner')

        result_lst.append(result_dict)

    results_df = pd.DataFrame(result_lst, columns=df_columns)

    csvout_fname = "bane/files/csv/{}_{}{}.csv".format(
        datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S"),
        os.path.basename(single_fname if args.input else args.bulk).split('.')[0],
        out_postfix
    )
    if not os.path.isdir("bane/files/csv/"):  # Check needed for Windows...
        os.makedirs("bane/files/csv/")
    results_df.to_csv(csvout_fname, index=False)
    if args.input:
        print('Winner and score:', result_dict['winner'],
              result_dict['steps']['Final score (filtered)'].get(result_dict['winner'], None))
    print("Results written to {}".format(csvout_fname))
