"""
© 2019 Alternative Data Group. All Rights Reserved.

Example:
    python -m bane.bane2 --value "Kirkland Signature"
    python -m bane.bane2 --generate-training-file
"""
import csv
import logging
from collections import Counter
from itertools import chain
from operator import itemgetter
from os import path
from typing import Dict, List, Tuple
from urllib.parse import urlparse

import tldextract

from bing.bing_web_core import BingWebCore
from domain_tools.company_page_detection import in_blacklist
from mapper.settings import DROPBOX
from packages.utils import strip_accents, pretty, get_timestamp, common, compare
from packages.xgboost.prediction import xgb_load_model, xgb_predict
from packages.xgboost.training_data import generate_training_data
from unofficializing.unofficializing import make_unofficial_name as uo

logger = logging.getLogger(f'adg.{__name__}')

DATA_DIR = path.join(path.dirname(path.abspath(__file__)), 'data')


class Bane:
    bing = BingWebCore()
    model = xgb_load_model(path.join(DATA_DIR, 'xgb_boost.bane.2019.05.30.21.32.FREQMETHOD.pickle'))

    @staticmethod
    def normalize(name: str) -> str:
        """
        Normalizes bing's about->name field: removes accents.
        """
        return strip_accents(name)

    @staticmethod
    def to_predictors(ranks: List[int], max_tuples: int = 15) -> Dict[str, float]:
        """
        Converts list of ranks to list of tuples (index, frequency) and then to dict
        suitable for feeding into xgboost.

        Args:
            ranks - list of bing ranks: [0, 1, 1, 10]

        Returns: dict with up to `max_tuples * 2` keys: {'0': ..., '1': ..., '2': ...}

        Example:
            [0, 3, 3, 5] -> [(3, 2), (0, 1), (5, 1), ...] -> {'0': 3, '1': 2, '2': 0, '3': 1, '4': 5, '5': 1, ...}
        """
        counter = Counter(ranks)
        most_common = counter.most_common(max_tuples)
        tuples = most_common + [(0, 0)] * (max_tuples - len(most_common))
        return {str(i): val for i, val in enumerate(chain.from_iterable(tuples))}

    @classmethod
    def extract_names_ranks(cls, value: str) -> List[dict]:
        """
        Given any string value, queries Bing with it.
        Bing results are scanned for "about"->"name" fields.
        The output is a list of dicts
            {
                'name': 'amazon',  # about-name value
                'ranks': [0, 1, 4],  # list-of-bing-rows-where-this-name-appears
                'website': 'amazon.com',  # website of first bing row
            }
        """
        bing_response = cls.bing.query(value)
        if not bing_response:
            return {}

        bing_rows = bing_response.get('webPages', {}).get('value', [])

        results = []
        for i, row in enumerate(bing_rows):
            for about in row.get('about', []):
                name = about.get('name')
                if not name:
                    continue

                name = cls.normalize(name).lower()
                name_uo = uo(name)

                url = row['url'].lower() if not in_blacklist(row['url']) else None
                if url:
                    # additional check
                    path = urlparse(url).path
                    domain = tldextract.extract(url).domain

                    name_in_path = common(name, path, min_ngrams=min(len(name), 4))
                    name_in_domain = common(name, domain, min_ngrams=min(len(name), 4))

                    if name_in_path and not name_in_domain:
                        logger.debug(f'Name "{name}" appears in url path "{url}" -> not company website')
                        url = None

                    elif i != 0:
                        if not name_in_domain:
                            logger.debug(f'Name "{name}" does not appear in url domain "{url}" -> '
                                         f'not company webste')
                            url = None

                # if name is "Amazon" and one of other names is "Amazon Inc", treat them as the same name
                # (i.e. "collapse" names if they are similar)
                for result in results:
                    existing_name = result['name']
                    if compare(uo(existing_name), name_uo):
                        result['ranks'].append(i)
                        if url:
                            result['websites'][i] = url
                        break
                else:
                    results.append({
                        'name': name,
                        'ranks': [i],
                        'websites': {i: url} if url else {},
                    })

        return results

    @classmethod
    def get_winners(cls, value: str) -> List[dict]:
        """
        Runs bing search on `value`, extracts "about->name" ranks and runs xgboost model on them.

        Args:
            value: any string value

        Returns:
            List of dicts {
                name: ...,
                ranks: [...],
                websites: [...],
                score: model prediction score,
            }.
            Dicts with highest scores first.
        """
        names_ranks = cls.extract_names_ranks(value)

        for row in names_ranks:
            row['score'] = round(xgb_predict(cls.to_predictors(row['ranks']), *cls.model), 3)

        names_ranks.sort(key=itemgetter('score'), reverse=True)

        return names_ranks


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--value')
    parser.add_argument('--generate-training-file', action='store_true', default=False)
    args = parser.parse_args()

    if args.value:
        print(pretty(Bane.get_winners(args.value)))

    elif args.generate_training_file:
        def run_bane(value: str) -> List[Tuple[str, dict]]:
            names_ranks = Bane.extract_names_ranks(value)

            # if total >= 3 and match in ['?', 0]:
            #     if not is_organization(name):
            #         logger.debug(f'Skipping non-organization name: {name}')
            #         match = '(not org)'

            return [(row['name'], Bane.to_predictors(row['ranks'])) for row in names_ranks]

        data = generate_training_data(recognizer=run_bane)

        writer = csv.DictWriter(
            open(path.join(DROPBOX, 'ERs', 'bane', f'bane.{get_timestamp()}.csv'), 'w', encoding='utf-8'),
            fieldnames=data[0].keys(),
            extrasaction='ignore',
        )
        writer.writeheader()
        writer.writerows(data)
