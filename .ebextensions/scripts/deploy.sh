#!/usr/bin/env bash
set -e

# Command to launch the deploy script looks like this:
#   /apptous_deploy/.ebextensions/deploy.sh 2>&1 | /usr/bin/logger -t "DEPLOY" ; test ${PIPESTATUS[0]} -eq 0
# Here we redirect both std and error outputs to logger (and this way send to papertrail)
# And then we use 'test ${PIPESTATUS[0]} to make the whole command return the status of deploy.sh.
# So if deploy script fails then deployment process will fail too.

SCRIPT_PATH=`dirname $0`
FILE_PATH=$SCRIPT_PATH/../files

source $SCRIPT_PATH/utils.sh
# Check for leader: see utils.sh
if is_leader; then
  echo "Start leader deploy"
  # if [[ $FLASK_CONFIG != "production-celery" ]]; then
  #   echo "Run migrations"
  #   ./manage.py migrations upgrade head
  #   echo "Update LogActions directory"
  #   ./manage.py update_log_actions_dir
  # fi
else
  echo "Start non-leader deploy"
fi

# update rsyslog rate limiting parameters
if grep -q "\$SystemLogRateLimitBurst" /etc/rsyslog.conf
then
  echo "rsyslog modification found"
else
  echo "\$SystemLogRateLimitInterval 5" >> /etc/rsyslog.conf
  echo "\$SystemLogRateLimitBurst 10000" >> /etc/rsyslog.conf
  service rsyslog restart
  echo "rsyslog modification done"
fi

if [[ $FLASK_CONFIG == "agd-api-production" ]]; then
  echo 'Production environment, skip celery and dev tools installation'
  exit 0
fi

if which git; then
    echo 'Found git, skip tools install'
else
    echo 'Installing development tools'
    yum update -y
    yum install -y git 
    yum groupinstall -y 'Development Tools'
    # We're using pip virtualenv because the amazon version is buggy  # FIXME: Are we?
    yum install -y htop tmux libxml2-devel libxslt-devel libffi-devel libjpeg-turbo-devel
fi

# if [[ $FLASK_CONFIG == "production-celery" ]]; then
  # copy_ext $FILE_PATH/cron-sync-db /etc/cron.d/sync-db 0644 root root
  # service crond reload
# fi
