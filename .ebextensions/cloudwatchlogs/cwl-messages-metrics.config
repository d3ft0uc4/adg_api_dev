###############################################################################
## Option Settings
##    Namespace: "aws:elasticbeanstalk:application:environment"
##    OptionName: MessagesCWLogGroup
##       Default:   <EnvironmentName>-messagess
##    Description: This is the name of the cloudwatch log group for web messages
##
## To get an SNS alert, add a subscription to the Elastic Beanstalk Notification
##   topic.  (e.g. set your Notificiation Endpoint to your email address)
##
## Metrics
##  Namespace:  ElasticBeanstalk/<EnvironmentName>
##  Metrics:    CWLHttp4xx  CWLHttp5xx
##
## Cloudwatch Alarms
##   CWLHttp5xxCount   - this alarm fires if the number of calls returning
##                        5xx response codes > 10
##   CWLHttp4xxPercent - this alarm fires if the percentage of calls returning
##                        4xx response codes > 10%    
##
##  Note - this demo works with apache containers that are using the default common log format
##      (python and php ruby containers do this - tomcat does not)
###############################################################################


Mappings:
  CWLogs:
    MessagesLogGroup:
      LogFile: "/var/log/messages"
      TimestampFormat: "%b %d %H:%M:%S"


Outputs:
  MessagesCWLogGroup:
    Description: "The name of the Cloudwatch Logs Log Group created for this environments web server messages. You can specify this by setting the value for the environment variable: MessagesCWLogGroup. Please note: if you update this value, then you will need to go and clear out the old cloudwatch logs group and delete it through Cloudwatch Logs."
    Value: { "Ref": "AWSEBCloudWatchLogs8832c8d3f1a54c238a40e36f31ef55a0MessagesLogGroup"}


Resources:
  AWSEBCloudWatchLogs8832c8d3f1a54c238a40e36f31ef55a0MessagesLogGroup:    ## Must have prefix:  AWSEBCloudWatchLogs8832c8d3f1a54c238a40e36f31ef55a0
    Type: "AWS::Logs::LogGroup"
    DependsOn: AWSEBBeanstalkMetadata
    DeletionPolicy: Retain     ## this is required
    Properties:
      LogGroupName:
        "Fn::GetOptionSetting":
          Namespace: "aws:elasticbeanstalk:application:environment"
          OptionName: MessagesCWLogGroup
          DefaultValue: {"Fn::Join":["-", [{ "Ref":"AWSEBEnvironmentName" }, "messagess"]]}
      RetentionInDays: 14


  ## Register the files/log groups for monitoring
  AWSEBAutoScalingGroup:
    Metadata:
      "AWS::CloudFormation::Init":
        CWLogsAgentConfigSetup:
          files:
            ## any .conf file put into /tmp/cwlogs/conf.d will be added to the cwlogs config (see cwl-agent.config)
            "/tmp/cwlogs/conf.d/messages.conf":
              content: |
                [sys-messages]
                file = `{"Fn::FindInMap":["CWLogs", "MessagesLogGroup", "LogFile"]}`
                log_group_name = `{ "Ref" : "AWSEBCloudWatchLogs8832c8d3f1a54c238a40e36f31ef55a0MessagesLogGroup" }`
                ##log_stream_name = {instance_id}
                log_stream_name = sys-messages
                datetime_format = `{"Fn::FindInMap":["CWLogs", "MessagesLogGroup", "TimestampFormat"]}`
              mode: "000400"
              owner: root
              group: root

