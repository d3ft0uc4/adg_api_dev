"""
© 2018 Alternative Data Group. All Rights Reserved.

CLI util to extract merchant name

Currently the CLI util lives in its own python file, but this code is probably going to be moved to the models.py
inside a specific function

Requires Python 3 and:
    Keras>=2.1.6  (which requires to install a machine learning backend first, i.e. tnesorflow)

Script arguments:
    start_model_file:   Keras model used for predicting the start index. (File can be generated with .ipynb files in
                        ../jupyter/merchant_name_extraction/ , same as below.)
    end_model_file:     Keras model used for prediction the end index.
    sentence:           String to extract merchant name from.

Output:
    It prints the results from dict returned by MerchantExtractor

Sample usage from repo's root:
    python3 -m neural_net.merchant_name.cli.extract_name --sentence "RITE AID STORE - 10139 HINGHAM MA" \
        --start-model neural_net/jupyter/merchant_name_extraction/data/start_index_model.h5 \
        --end-model neural_net/jupyter/merchant_name_extraction/data/end_index_model.h5
"""

import os
import argparse
import json

from keras.models import load_model

from neural_net.misc.encoding import NumpyEncoder
from neural_net.merchant_name.models import MerchantExtractor


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Command line tool to extract merchant \
        information using already pretrained models')
    parser.add_argument('--start-model', type=str, help='Keras model used to predict start index', required=True)
    parser.add_argument('--end-model', type=str, help='Keras model used to predict end index', required=True)
    parser.add_argument('--sentence', type=str, help='Sentence to extract merchant information from')
    parser.add_argument('--input-file', type=str, help='File with sentences to extract merchant information from')
    parser.add_argument('--output-file', type=str, help='Output file to save the merchant information')

    args = parser.parse_args()

    assert os.path.isfile(args.start_model) and os.path.isfile(args.end_model)

    if args.sentence is not None and args.input_file is not None:
        parser.print_help()
        exit()

    if args.sentence is not None:
        preds = MerchantExtractor(load_model(args.start_model), load_model(args.end_model)).extract_names([args.sentence])

    if args.input_file is not None:
        assert(os.path.isfile(args.input_file))
        with open(args.input_file) as f:
            preds = MerchantExtractor(load_model(args.start_model), load_model(args.end_model)).extract_names(
                [line.strip() for line in f])

    if args.output_file is not None:
        with open(args.output_file, 'w') as f:
            f.write(json.dumps(preds, cls=NumpyEncoder))
    else:
        print(preds)

