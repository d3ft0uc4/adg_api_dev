"""
© 2018 Alternative Data Group. All Rights Reserved.

Requires Python 3 and:
    numpy>=1.14.0
    Keras>=2.1.6  (which requires to install a machine learning backend first, i.e. tnesorflow)

MerchantExtractor class file
"""

from typing import Dict, Tuple, Union, List

import numpy as np

from keras.models import Sequential

from neural_net.exceptions import NotKerasModel
from neural_net.merchant_name.encoders import OneHotSymbolEncoder


class MerchantExtractor:
    """
    Extracts merchant names from strings

    Main class used in the process of merchant name extraction
    from strings. This class makes uses of already trained keras
    models to predict the indexes at which the merchant name
    starts and ends.

    Arguments:
        start_index_model: Keras model trained to predict the start index
            in the string.
        end_index_model: Keras model trained to predict the end index in
            the string.
        start_index_encoder: An encoder instance suitable for the start_index_model
        end_index_encoder: An encoder instable suitable for the end_index_model
    """

    DEFAULT_ENCODER = OneHotSymbolEncoder()

    def __init__(self,
                 start_index_model,
                 end_index_model,
                 start_index_encoder=DEFAULT_ENCODER,
                 end_index_encoder=DEFAULT_ENCODER
                 ):
        self.start_index_model = start_index_model
        self.end_index_model = end_index_model
        self.start_index_encoder = start_index_encoder
        self.end_index_encoder = end_index_encoder

    @property
    def start_index_model(self):
        return self._start_index_model

    @start_index_model.setter
    def start_index_model(self, model):
        if type(model) == Sequential:
            self._start_index_model = model
        else:
            raise NotKerasModel("%s is not a keras model" % model)

    @property
    def end_index_model(self):
        return self._end_index_model

    @end_index_model.setter
    def end_index_model(self, model):
        if type(model) == Sequential:
            self._end_index_model = model
        else:
            raise NotKerasModel("%s is not a keras model" % model)

    def predict_start_index(self, sentences: List[str]) -> List[Dict[str, Union[str, float]]]:
        results = []
        preds = self.start_index_model.predict(self.start_index_encoder.encode(sentences))

        for pred in preds:
            best_pred_i = np.argmax(pred)
            results.append({
                'index': best_pred_i,
                'prob': pred[best_pred_i]
            })
        return results

    def predict_end_index(self, sentences: List[str]) -> List[Dict[str, Union[str, float]]]:
        results = []
        preds = self.end_index_model.predict(self.end_index_encoder.encode(sentences))
        for pred in preds:
            best_pred_i = np.argmax(pred)
            results.append({
                'index': best_pred_i,
                'prob': pred[best_pred_i]
            })
        return results

    def extract_names(self, sentences: List[str]) -> List[Dict[str, Union[str, Tuple[float, float]]]]:
        """
        Public function to extract the merchant name from a multiple strings
        TODO: Rename to nn_str_cleaner?

        Arguments:
            sentences: String to extract text from

        Returns:
            List of dictionary with the following keys:
                merchant_name: Actual substring with the merchant name
                probs: Tuple with the probability for the start index and the end index
        """

        start_indexes = self.predict_start_index(sentences)
        end_indexes = self.predict_end_index(sentences)
        return [{
            'merchant_name': sentences[i][start_indexes[i]['index']: end_indexes[i]['index']].strip(),
            'start_index': (start_indexes[i]['index'], start_indexes[i]['prob']),
            'end_index': (end_indexes[i]['index'], end_indexes[i]['prob'])
        } for i in range(len(sentences))]
