"""
© 2018 Alternative Data Group. All Rights Reserved.

Requires Python 3 and:
    numpy>=1.14.0

Encoder classes
"""

import re
import numpy as np

from typing import List

from neural_net.merchant_name.normalizers import SentenceNormalizer


class SentenceStatEncoder:
    """
    Encodes a sentence using words stats from the index/word context
    FIXME: This class isn't in use. Should we remove it?

    Arguments:
        words_boundaries: List of possible characters as word boundaries
        digits_regex: regex string used to search for digits
        specials_regex: regex string used to search for special/punctuation characters
    """

    DEFAULT_WORDS_BOUNDARIES = u'. \t\n\r-_'
    DEFAULT_DIGITS_REGEX = r'\d'
    DEFAULT_SPECIALS_REGEX = r'[^A-Za-z\d\s]'

    def __init__(self,
                 words_boundaries=DEFAULT_WORDS_BOUNDARIES,
                 digits_regex=DEFAULT_DIGITS_REGEX,
                 specials_regex=DEFAULT_SPECIALS_REGEX
                 ):
        self.words_boundaries = words_boundaries
        self.digits_regex = digits_regex
        self.specials_regex = specials_regex

    def digits_scanner(self, sentence: str):
        return len(re.findall(self.digits_regex, sentence))

    def specials_scanner(self, sentence: str):
        return len(re.findall(self.specials_regex, sentence))

    def word_at_index(self, index: int, sentence: str) -> str:
        """Returns the word that owns the char for the given index

        Function is useful to get a word from a sentence given a passed index.
        Used while walking the sentence char by char and getting stats from the
        context of the char position.

        Arguments:
            index: index in the sentence from 0 to len(sentence)
            sentence: sentence to extract the word from

        Returns:
            A string (the word)
        """

        start_word = 0
        end_word = len(sentence)
        if index > len(sentence):
            return ''
        for j in range(end_word):
            if j <= index:
                if sentence[j] in self.words_boundaries:
                    start_word = j + 1
            if j >= index:
                if sentence[j] in self.words_boundaries:
                    end_word = j
                    break
        return sentence[start_word:end_word]

    def word_vector(self, word: str):
        """Retrieve the stats for the given word

        Entry point to call all functions used to retrieve the stats

        Returns:
            Array of stats
        """

        return [self.digits_scanner(word), self.specials_scanner(word)]

    def encode(self, sentence: str):
        """Encode the sentence

        Entry point to call the encode functionality

        Returns:
            Array of vectors
        """

        return np.array([self.word_vector(self.word_at_index(i, sentence)) for i in range(len(sentence))])


class OneHotSymbolEncoder:
    """
    One Hot Encode symbols in the given string

    Arguments:
        symbols: List of symbols recognized in the vocabulary
        sentence_max_len: Possible max lenght of the sentence. Sentences
            beyond this lenght an cutted off.
        encoders: List of encoders to extend the one hot encoded vector
    """

    DEFAULT_SYMBOLS = u'''abcdefghijklmnopqrstuvwxyz0123456789+'"`’~@*$#.) (!-®&_/\=:;,%?|+-'''
    UNKNOWN = 'UNKNOWN'
    PAD = 'PAD'
    SENTENCE_MAX_LEN = 130
    DEFAULT_EXTRA_ENCODERS = [SentenceStatEncoder()]

    def __init__(self,
                 symbols=DEFAULT_SYMBOLS,
                 sentence_max_len=SENTENCE_MAX_LEN,
                 char_encoders=[]
                 ):
        self.symbols = symbols
        self.vocab = [self.UNKNOWN, self.PAD] + [s for s in self.symbols]
        self.symbol2index = dict((c, i) for i, c in enumerate(self.vocab))
        self.index2symbol = dict((i, c) for i, c in enumerate(self.vocab))
        self.sentence_max_len = sentence_max_len
        self.extra_char_encoders = char_encoders

    def pad_vector(self, vector):
        """Pads the sentence up to SENTENCE_MAX_LEN if necessary

        Arguments:
            vector: vector filled with symbols identifiers

        Returns:
            vector padded filled with symbols identifiers
        """

        vector_len = len(vector)
        if vector_len >= OneHotSymbolEncoder.SENTENCE_MAX_LEN:
            return np.array(vector[0:OneHotSymbolEncoder.SENTENCE_MAX_LEN], dtype='int8')
        else:
            return np.array(vector + [self.symbol2index[OneHotSymbolEncoder.PAD]
                            for _ in range(OneHotSymbolEncoder.SENTENCE_MAX_LEN - vector_len)], dtype='int8')

    def encode(self, sentences: List[str]):
        """Encode the sentence doing OHE and adding extended vector information

        Arguments:
            sentence: string to encode

        Returns:
            One hot encoded vector
        """

        sentences_n = len(sentences)
        vectors = None

        # Normalize sentences
        sentences = [SentenceNormalizer.separate_words(sentence) for sentence in sentences]

        for i, sentence in enumerate(sentences):
            sentence_len = len(sentence)

            # Encode sentence with all additional encoders
            # print([encoder.encode(sentence) for encoder in self.extra_char_encoders])
            extra_encodings = [encoder.encode(sentence) for encoder in self.extra_char_encoders]
            # Full vector size for each char made up from all the vector encodings
            char_extra_vector_size = sum([1 for encoding in extra_encodings for _ in encoding[0]])
            # Holds full extra vectors
            extra_vector = np.zeros((sentence_len, char_extra_vector_size))

            vectors_dim_acc = 0
            for extra_encoding in extra_encodings:
                for j, vector in enumerate(extra_encoding):
                    extra_vector[j, vectors_dim_acc:vectors_dim_acc + len(vector)] = vector
                    # Only increment v if i is the last char encoding of the sentence
                    if j == sentence_len - 1:
                        vectors_dim_acc += len(vector)

            symbols_vector = self.pad_vector([self.symbol2index[s]
                                              if s in self.vocab
                                              else self.symbol2index[OneHotSymbolEncoder.UNKNOWN]
                                              for s in sentence.lower()]
                                             )

            # At this point we know the vector size so we initialize it here
            if vectors is None:
                vectors = np.zeros((sentences_n, self.sentence_max_len, len(self.vocab) + char_extra_vector_size), dtype='int8')
            # OHE
            for j, symbol in enumerate(symbols_vector):
                vectors[i, j, symbol] = 1
            # Add extra vector for char
            for j, char_vector in enumerate(extra_vector):
                for j, v in enumerate(char_vector):
                    vectors[i, j, len(self.vocab) + j] = v

        return vectors

