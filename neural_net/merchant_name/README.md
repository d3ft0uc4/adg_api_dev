© 2018 Alternative Data Group. All Rights Reserved.

# Merchant NAme Extraction

This module wraps around a neural network string cleaner coded and trained manually with the files in ../jupyter/merchant_name_extraction

See requirements file also (for `pip install -r`).