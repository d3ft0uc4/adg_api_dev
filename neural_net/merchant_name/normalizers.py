"""
© 2018 Alternative Data Group. All Rights Reserved.

Requires Python 3 and:
    numpy>=1.14.0

Encoder classes
"""

import re


class SentenceNormalizer:
    """Text normalizing functions"""

    @classmethod
    def separate_words(cls, sentence: str):
        """Adds an space between all chars and nonchars found in the sentence."""
        return re.sub(r'\s+', " ", " ".join(re.findall(r'[A-Za-z\'\-]+|\d+|\W+', sentence))).strip()