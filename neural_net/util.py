import numpy as np
import random
import itertools
from collections import deque
from sklearn.feature_extraction.text import CountVectorizer
from keras.preprocessing.sequence import pad_sequences


alphabet = "abcdefghijklmnopqrstuvwxyz"
chars = sorted(list(set(alphabet)))
char_indices = dict((c, i) for i, c in enumerate(alphabet))


class Dataset:

    def __init__(self, X, y):
        self.X = X
        self.y = y

    def split(self, rand=True, seed=6, split=0.8):
        data = list(zip(self.X, self.y))
        if rand:
            random.seed(seed)
            random.shuffle(data)
        size = len(data)
        point = int(size * split)
        [X_train, y_train] = zip(*data[:point])
        [X_test, y_test] = zip(*data[point:])
        return Dataset(list(X_train), list(y_train)), Dataset(list(X_test), list(y_test))


def normalize_strings(strings, max_words):
    """Lowercase the input stings and only keep chars from the alphabet."""
    try:
        str_array = strings.values.copy()
    except AttributeError:
        str_array = strings.copy()
    for i, s in enumerate(str_array):
        temp = s.lower().split()[0:max_words]
        for x, y in enumerate(temp):
            slist = []
            for c in y:
                if c in char_indices.keys():
                    slist.append(c)
            temp[x] = "".join(slist)
        str_array[i] = " ".join(temp)
    return str_array


def one_hot_char_encoding(
        strings_subset,
        max_words,
        max_word_length,
        max_sentence_length,
        word_level_padding):
    """Do one hot char encoding."""
    text = "abcdefghijklmnopqrstuvwxyz"
    chars = sorted(list(set(text)))
    char_indices = dict((c, i) for i, c in enumerate(chars))

    if word_level_padding:
        X_train = np.zeros((
            len(strings_subset),
            max_words,
            max_word_length,
            len(chars)), dtype=np.bool)
        for i, sentence in enumerate(strings_subset):
            tokens = sentence.split()
            for w, words in enumerate(tokens):
                for t, char in enumerate(words):
                    if t > (max_word_length-1):
                        continue
                    X_train[i, w, t, char_indices[char]] = 1
        X_train = X_train.reshape(
            X_train.shape[0],
            X_train.shape[1] * X_train.shape[2] * X_train.shape[3])

    else:
        X_train = np.zeros((
            len(strings_subset),
            max_sentence_length,
            len(chars)), dtype=np.bool)
        for i, sentence in enumerate(strings_subset):
            for t, char in enumerate(sentence):
                if t > (max_sentence_length-1):
                    continue
                X_train[i, t, char_indices[char]] = 1
        X_train = X_train.reshape(
            X_train.shape[0],
            X_train.shape[1] * X_train.shape[2])
    return X_train


def word_rotation(sentence):
    lng = len(sentence.split())
    ls = []
    for n in range(lng-1):
        d = deque(sentence.split())
        d.rotate(n+1)
        ns = " ".join(list(d))
        ls.append(ns)
    return ls


def count_vectorizer(strings_subset):
    print("Creating the bag of letters...\n")

    vectorizer = CountVectorizer(
        analyzer="char",
        tokenizer=None,
        preprocessor=None,
        stop_words=None,
        decode_error="ignore",
        vocabulary=[
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
        ]
    )

    S = []
    for x in strings_subset:
        subset_features = vectorizer.fit_transform(x.split())
        S.append(subset_features.toarray())
    X_data = pad_sequences(S)
    print("original X_data Shape", X_data.shape)
    X_data = X_data.reshape(
        X_data.shape[0], X_data.shape[1] * X_data.shape[2])
    print("new X_data Shape", X_data.shape)
    return X_data


def augment(dataset: Dataset) -> Dataset:
    X, y = dataset.X, dataset.y
    data = list(zip(X, y))
    nitems = []
    for x, y in data:
        temp = word_rotation(x)
        if len(temp) > 0:
            nitems.extend(list(zip(temp, itertools.repeat(y))))
    data.extend(nitems)
    return Dataset(data[0], data[1])
