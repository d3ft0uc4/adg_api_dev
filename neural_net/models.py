import os

from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import SGD
from keras.callbacks import ModelCheckpoint
from keras.models import load_model


def model_predict(model, X):
    # A dataframe of (input data size X number of unique targets),
    # where for each input item we have an array of probabilities for each target.
    prediction = model.predict(X, verbose=0)
    # Get the maximum probability for each input:
    confidence = prediction.max(axis=1)
    # Get the position (=target) for max probability.
    prediction = prediction.argmax(axis=1)
    return prediction, confidence


class SequentialModel:

    def __init__(self, dataset, settings):
        self._dataset = dataset
        self._settings = settings
        self._model = self._build()

    def _build(self):
        X, y = self._dataset.X, self._dataset.y
        # build the model: 3 layers MLP
        print('Build model...')

        model = Sequential()
        model.add(Dense(512, activation='relu', input_dim=X.shape[1]))
        model.add(Dropout(0.5))
        model.add(Dense(512, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(512, activation='sigmoid'))
        model.add(Dropout(0.5))
        model.add(Dense(512, activation='sigmoid'))
        model.add(Dropout(0.5))
        model.add(Dense(y.shape[1], activation='softmax'))

        model.summary()

        # Define solver algorithm and compile model
        sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
        model.compile(
            loss='categorical_crossentropy',
            optimizer=sgd,
            metrics=['accuracy'])
        return model

    def fit(self):
        checkpoint = ModelCheckpoint(
            self._settings.weights_file_path,
            monitor=self._settings.monitor,
            verbose=self._settings.verbose,
            save_best_only=self._settings.save_best_only,
            mode=self._settings.mode)
        callbacks_list = [checkpoint]
        self._history = self._model.fit(
            self._dataset.X,
            self._dataset.y,
            validation_split=self._settings.validation_split,
            epochs=self._settings.epochs,
            batch_size=self._settings.batch_size,
            callbacks=callbacks_list)
        return self._history

    def predict(self, X):
        return model_predict(self._model, X)

    def save(self):
        self._model.save(self._settings.model_file_path)


class SequentialModelFromFile:

    def __init__(self, settings):
        self._settings = settings
        self._model = load_model(self._settings.model_file_path)
        self._model.load_weights(self._settings.weights_file_path)

    def predict(self, X):
        return model_predict(self._model, X)


class SequentialModelSettingsDefault:

    def __init__(self, model_path='.') -> None:
        self.validation_split = 0.20
        self.epochs = 50
        self.batch_size = 32
        self.model_file_path = os.path.join(model_path, 'model.h5')
        self.weights_file_path = os.path.join(model_path, 'best_weights.hdf5')
        self.monitor = 'val_acc'
        self.verbose = 1
        self.save_best_only = True
        self.mode = 'max'


class SequentialModelSettings5Epoch:

    def __init__(self, model_path='.') -> None:
        self.validation_split = 0.20
        self.epochs = 5
        self.batch_size = 32
        self.model_file_path = os.path.join(model_path, 'model.h5')
        self.weights_file_path = os.path.join(model_path, 'best_weights.hdf5')
        self.monitor = 'val_acc'
        self.verbose = 1
        self.save_best_only = True
        self.mode = 'max'
