"""
© 2018 Alternative Data Group. All Rights Reserved.

Requires Python 3 and:
    numpy>=1.14.0

Unittests for merchant_name.encoders

Test usage:
    This file can be run as an independent script or can be included for running from
    a TestRunner

    $ python -m neural_net.tests.test_encoders
"""

import unittest

import numpy as np

from neural_net.merchant_name.encoders import OneHotSymbolEncoder
from neural_net.merchant_name.encoders import SentenceStatEncoder

sentences = [
    u'''An unlucky student almost lost a 17th century violin worth almost £200,000''',
    u'''Then let not winter's ragged hand deface'''
]


class CharEchoEncoder:
    """Dummy encoder class that return the same symbol for echo_times each char"""
    def __init__(self, echo_symbol=0, echo_times=1):
        self.echo_symbol = echo_symbol
        self.echo_times = echo_times

    def encode(self, sentence):
        return np.array([[self.echo_symbol for _ in range(self.echo_times)] for _ in sentence])


class SentenceStatEncoderTestSpec(unittest.TestCase):
    def setUp(self):
        self.standard_encoder = SentenceStatEncoder()

    def test_word_at_index(self):
        self.assertEqual(
            self.standard_encoder.word_at_index(0, sentences[0]),
            'An'
        )
        self.assertEqual(
            self.standard_encoder.word_at_index(15, sentences[1]),
            "winter's"
        )
        self.assertEqual(
            self.standard_encoder.word_at_index(len(sentences[0]) - 1, sentences[0]),
            '£200,000'
        )
        self.assertEqual(
            self.standard_encoder.word_at_index(2, sentences[0]),
            ''
        )

    def test_encode(self):
        self.assertEqual(self.standard_encoder.encode(sentences[0]).shape, (len(sentences[0]), 2))


class OneHotSymbolEncoderTestSpec(unittest.TestCase):
    ECHO_TIMES_ENCODER_1 = 10
    ECHO_TIMES_ENCODER_2 = 15

    def setUp(self):
        self.standard_encoder = OneHotSymbolEncoder()
        self.dummy_extra_encoder = OneHotSymbolEncoder(
            char_encoders=[
                CharEchoEncoder(echo_symbol=4, echo_times=OneHotSymbolEncoderTestSpec.ECHO_TIMES_ENCODER_1),
                CharEchoEncoder(echo_symbol=2, echo_times=OneHotSymbolEncoderTestSpec.ECHO_TIMES_ENCODER_2)
            ])

    def test_one_hot_encoded_chars_on(self):
        vector = self.standard_encoder.encode(sentences[0])
        for i, char in enumerate(sentences[0].lower()):
            symbol = char if char in self.standard_encoder.vocab else self.standard_encoder.UNKNOWN
            self.assertEqual(vector[0, i, self.standard_encoder.symbol2index[symbol]], 1)

    def test_one_hot_encoded_chars_off(self):
        vector = self.standard_encoder.encode(sentences[0])
        for i, char in enumerate(sentences[0].lower()):
            symbol = char if char in self.standard_encoder.vocab else self.standard_encoder.UNKNOWN
            ohe_vector = vector[0, i, 0:len(self.standard_encoder.vocab)]
            self.assertEqual(sum(ohe_vector[self.standard_encoder.symbol2index[symbol] + 1:]), 0)
            self.assertEqual(sum(ohe_vector[:self.standard_encoder.symbol2index[symbol]]), 0)

    def test_pad_vector(self):
        padded_vector = self.standard_encoder.pad_vector([
            self.standard_encoder.symbol2index[char]
            if char in self.standard_encoder.vocab
            else self.standard_encoder.symbol2index[self.standard_encoder.UNKNOWN]
            for char in sentences[0].lower()
        ])
        self.assertEqual(len(padded_vector), self.standard_encoder.sentence_max_len)

    def test_pad_vector_cutted_off(self):
        padded_vector = self.standard_encoder.pad_vector([
            self.standard_encoder.symbol2index[char]
            if char in self.standard_encoder.vocab
            else self.standard_encoder.symbol2index[self.standard_encoder.UNKNOWN]
            for char in sentences[0].lower() * 20
        ])
        self.assertEqual(len(padded_vector), self.standard_encoder.sentence_max_len)

    def test_extra_vector(self):
        encoding = self.dummy_extra_encoder.encode(sentences[0])
        # print(encoding[0,0])
        self.assertEqual(encoding.shape[2], len(self.dummy_extra_encoder.vocab) + OneHotSymbolEncoderTestSpec.ECHO_TIMES_ENCODER_1 + OneHotSymbolEncoderTestSpec.ECHO_TIMES_ENCODER_2)
        self.assertEqual(encoding[0, 0, len(self.dummy_extra_encoder.vocab) + 5], 4)
        self.assertEqual(encoding[0, 0, len(self.dummy_extra_encoder.vocab) + OneHotSymbolEncoderTestSpec.ECHO_TIMES_ENCODER_1 + 5], 2)


if __name__ == '__main__':
    unittest.main()