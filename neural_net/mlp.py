from datetime import datetime
import sys
import pandas as pd
import numpy as np
import argparse
import warnings

from neural_net.inputs import load_data_from_db_all, load_data_from_db_1000, load_data_from_csv

from neural_net.encoders import OneHotCharEncoder, CountVectorizerEncoder
from neural_net.encoders import (
    OneHotCharSettingsDefault,
    OneHotCharSettingsNoWordPadding,
    OneHotCharSettingsAugment
)
from neural_net.encoders import CountVectorizerSettingsDefault
from neural_net.models import (
    SequentialModel,
    SequentialModelFromFile,
    SequentialModelSettingsDefault,
    SequentialModelSettings5Epoch
)


warnings.filterwarnings("ignore")


parser = argparse.ArgumentParser()
parser.prog = 'neural_net'
parser.description = """Preprocess and encode string data for neural net training.
Validate model, calculate test accuracy and make predictions using traied model."""
parser.add_argument(
    '-i', '--input',
    help="""Input method: 'Db1000', 'DbAll', 'CSV'""")
parser.add_argument(
    '-e', '--encoder',
    help="""Encoding method:
'OneHotChar', 'OneHotCharAugment', 'OneHotCharNoWordPadding', 'CountVectorizer'""")
parser.add_argument(
    '-m', '--model',
    help="""Model: 'Sequential', 'Sequential5Epoch'""")
parser.add_argument(
    '-p', '--predict',
    help="Load model from disk and return model predictions and their respective probabilties.")

# Examples:
#  python neural_net/mlp.py -m Sequential5Epoch
#  python neural_net/mlp.py -i DbAll
#  python neural_net/mlp.py --predict=1


def test_results(test_dataset, prediction, confidence):
    df = pd.DataFrame(
        np.column_stack(
            [test_dataset.X, prediction, confidence, test_dataset.y]
        ),
        columns=['cleaned_str', 'prediction', 'confidence', 'y_actual'])

    # Add `match` column to the dataframe and get the total match percentage.
    df['match'] = np.where(df.prediction == df.y_actual, 1, 0)
    matches_precent = round(df['match'].astype(float).sum() / len(df['match']), 3)
    return df, matches_precent


def train(original_dataset, Encoder, EncoderSettings, Model, ModelFromFile, ModelSettings):
    encoder = Encoder(EncoderSettings())

    train_dataset, test_dataset = original_dataset.split(split=split_ratio)
    encoded_dataset = encoder.encode(train_dataset)

    # Train the model and save it to the file.
    model = Model(encoded_dataset, ModelSettings(model_path))
    model.fit()
    model.save()
    encoder.save(model_path)

    # Run the prediction on the test dataset.
    predict(test_dataset, Encoder, EncoderSettings, ModelFromFile, ModelSettings)


def predict(test_dataset, Encoder, EncoderSettings, ModelFromFile, ModelSettings):
    encoder = Encoder(EncoderSettings())
    # Load the model from the file.
    model = ModelFromFile(ModelSettings(model_path))
    encoder.load(model_path)

    # Run the prediction on the test dataset.
    encoded_test_X = encoder.encode_X(test_dataset.X)
    prediction_start = datetime.now()
    prediction, confidence = model.predict(encoded_test_X)
    prediction = encoder.decode_y(prediction)
    # Compose the test results - get a dataframe with predicted / actual values.
    df, matches_precent = test_results(test_dataset, prediction, confidence)

    # Print the results.
    print(df)
    print('Prediction: done {} records in: {}'.format(
        encoded_test_X.shape[0], datetime.now() - prediction_start))
    print('number of correct matches, % = {}'.format(matches_precent))


if __name__ == '__main__':
    namespace = parser.parse_args(sys.argv[1:])

    model_path = '.'
    split_ratio = 0.80

    inputs = {
        'Db1000': load_data_from_db_1000,
        'DbAll': load_data_from_db_all,
        'CSV': load_data_from_csv,
    }
    input_name = namespace.input or 'Db1000'
    load_data = inputs[input_name]
    original_dataset = load_data()

    # Encoder / EncoderSettings pair defines the speicfic encoder to use.
    encoders = {
        'OneHotChar': (OneHotCharEncoder, OneHotCharSettingsDefault),
        'OneHotCharAugment': (OneHotCharEncoder, OneHotCharSettingsAugment),
        'OneHotCharNoWordPadding': (OneHotCharEncoder, OneHotCharSettingsNoWordPadding),
        'CountVectorizer': (CountVectorizerEncoder, CountVectorizerSettingsDefault),
    }
    encoder_name = namespace.encoder or 'OneHotChar'
    Encoder, EncoderSettings = encoders[encoder_name]

    # For model we have Model class, ModelFromFile class (used for predictions) and
    # common ModelSettings class
    models = {
        'Sequential': (
            SequentialModel, SequentialModelFromFile, SequentialModelSettingsDefault
        ),
        'Sequential5Epoch': (
            SequentialModel, SequentialModelFromFile, SequentialModelSettings5Epoch
        ),
    }
    model_name = namespace.model or 'Sequential'
    Model, ModelFromFile, ModelSettings = models[model_name]

    if namespace.predict:
        predict(original_dataset, Encoder, EncoderSettings, ModelFromFile, ModelSettings)
    else:
        train(original_dataset, Encoder, EncoderSettings, Model, ModelFromFile, ModelSettings)
