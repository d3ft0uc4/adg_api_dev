import os
import numpy as np
from sklearn.preprocessing import LabelEncoder, LabelBinarizer

from neural_net.util import (
    Dataset,
    one_hot_char_encoding,
    normalize_strings,
    augment,
    count_vectorizer,
)


class OneHotCharEncoder:

    def __init__(self, settings):
        self._settings = settings
        self._label_binarizer = LabelBinarizer()
        self._label_encoder = LabelEncoder()

    def encode(self, dataset: Dataset):
        if self._settings.rotate:
            dataset = augment(dataset)
        encoded_X = self.encode_X(dataset.X)
        encoded_y = self.encode_y(dataset.y)
        return Dataset(encoded_X, encoded_y)

    def encode_X(self, X):
        cleaned_X = normalize_strings(X, self._settings.max_words)
        return one_hot_char_encoding(
            cleaned_X,
            self._settings.max_words,
            self._settings.max_word_length,
            self._settings.max_sentence_length,
            self._settings.word_level_padding)

    def encode_y(self, y):
        self._label_binarizer.fit(y)
        self._label_encoder.fit(y)
        encoded_y = self._label_binarizer.transform(y)
        return encoded_y

    def decode_y(self, y):
        encoded_y = self._label_encoder.inverse_transform(y)
        return encoded_y

    def load(self, data_path):
        self._label_encoder.classes_ = np.load(self._settings.label_encoder_file_path)

    def save(self, data_path):
        np.save(self._settings.label_encoder_file_path, self._label_encoder.classes_)


class OneHotCharSettingsDefault:

    def __init__(self, model_path='.'):
        self.max_words = 5
        self.max_word_length = 25
        self.max_sentence_length = 60
        self.word_level_padding = True
        self.rotate = False
        self.label_encoder_file_path = os.path.join(model_path, 'label_encoder_classes.npy')


class OneHotCharSettingsAugment:

    def __init__(self, model_path='.'):
        self.max_words = 5
        self.max_word_length = 25
        self.max_sentence_length = 60
        self.word_level_padding = True
        self.rotate = True
        self.label_encoder_file_path = os.path.join(model_path, 'label_encoder_classes.npy')


class OneHotCharSettingsNoWordPadding:

    def __init__(self, model_path='.'):
        self.max_words = 5
        self.max_word_length = 25
        self.max_sentence_length = 60
        self.word_level_padding = False
        self.rotate = False
        self.label_encoder_file_path = os.path.join(model_path, 'label_encoder_classes.npy')


class CountVectorizerEncoder:

    def __init__(self, settings):
        self._settings = settings
        self._label_binarizer = LabelBinarizer()
        self._label_encoder = LabelEncoder()

    def encode(self, dataset):
        encoded_X = self.encode_X(dataset.X)
        encoded_y = self.encode_y(dataset.y)
        return Dataset(encoded_X, encoded_y)

    def encode_X(self, X):
        return count_vectorizer(X)

    def encode_y(self, y):
        self._label_binarizer.fit(y)
        self._label_encoder.fit(y)
        encoded_y = self._label_binarizer.transform(y)
        return encoded_y

    def decode_y(self, y):
        encoded_y = self._label_encoder.inverse_transform(y)
        return encoded_y

    def load(self, data_path):
        self._label_encoder.classes_ = np.load(self._settings.label_encoder_file_path)

    def save(self, data_path):
        np.save(
            self._settings.label_encoder_file_path,
            self._label_encoder.classes_)


class CountVectorizerSettingsDefault:

    def __init__(self, model_path='.'):
        self.label_encoder_file_path = os.path.join(model_path, 'cv_label_encoder_classes.npy')
