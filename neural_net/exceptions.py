"""
© 2018 Alternative Data Group. All Rights Reserved.

Exceptions classes
"""


class NotKerasModel(Exception):
    def __init__(self, msg="The given file isn't a Keras model"):
        super(NotKerasModel, self).__init__(msg)