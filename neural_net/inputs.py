import os
import sqlalchemy
import pandas as pd

from neural_net.util import Dataset


def load_data_from_db_all():
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'SQLALCHEMY_DATABASE_URI', 'mysql+pymysql://root:root@localhost/altdg_merchtag')
    db = sqlalchemy.create_engine(SQLALCHEMY_DATABASE_URI)
    dataset = pd.read_sql('''
        select model.final_guess_target_map as target,
               clean_cc.clean_trans_string as raw_string
          from clean_cc
          join entity_chooser_beta model
            on model.merchant_string = case
                 when clean_cc.merchant_string_td2 then clean_cc.merchant_string_td2
                 else clean_cc.merchant_string_dc1
               end
         where (clean_cc.merchant_string_td2 is not NULL
            or clean_cc.merchant_string_dc1 is not NULL)
           and model.is_entity_guess_good = 1
           and model.final_guess_target_map is NOT NULL
    ''', db)
    # Normalized the input strings.
    orig_strings = dataset['raw_string'].astype(str)
    # Get the list of targets.
    targets = dataset['target'].astype(str).values
    return Dataset(orig_strings, targets)


def load_data_from_db_1000():
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'SQLALCHEMY_DATABASE_URI', 'mysql+pymysql://root:root@localhost/altdg_merchtag')
    db = sqlalchemy.create_engine(SQLALCHEMY_DATABASE_URI)
    dataset = pd.read_sql('''
        select model.final_guess_target_map as target,
               clean_cc.clean_trans_string as raw_string
          from clean_cc
          join entity_chooser_beta model
            on model.merchant_string = case
                 when clean_cc.merchant_string_td2 then clean_cc.merchant_string_td2
                 else clean_cc.merchant_string_dc1
               end
         where (clean_cc.merchant_string_td2 is not NULL
            or clean_cc.merchant_string_dc1 is not NULL)
           and model.is_entity_guess_good = 1
           and model.final_guess_target_map is NOT NULL
         limit 1000
    ''', db)
    # Normalized the input strings.
    orig_strings = dataset['raw_string'].astype(str)
    # Get the list of targets.
    targets = dataset['target'].astype(str).values
    return Dataset(orig_strings, targets)


def load_data_from_csv():
    input_path = '../../../adg_api_dev_share/original_data/'
    input_file = input_path+'td2_1point3mm.csv'
    dataset = pd.read_csv(input_file, encoding='latin1')
    dataset = dataset[0:1000]
    return Dataset(dataset['raw_string'], dataset['targets'])
