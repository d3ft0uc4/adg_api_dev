database_path = 'mysql+pymysql://adgsql:adg_dev18@nychousingdata.cpu3eplzthqs.us-east-1.rds.amazonaws.com:3306/altdg_merchtag'

#input_path = '../../../../adg_api_dev_share/original_data/'
input_path = '../../../adg_api_dev_share/original_data/'
#output_path = '../../../../adg_api_dev_share/neural_net/data/'
output_path = '../../../adg_api_dev_share/neural_net/data/'

input_file = input_path+'td2_1point3mm.csv'
input_column_name = 'raw_string'
target_column_name = 'target'

weights_path = 'best_weights.hdf5'
save_model_path = 'test_model.h5'
load_model_path = 'test_model.h5'

split_ratio = 0.80

nn_validation_split = 0.20
nn_epochs=50
nn_batch_size=32

input_sql = "SELECT merchant_string, uid, cnt, data_source FROM agg_merch_master_top_merchants ORDER BY cnt desc"
