from flask import Flask
from flask import jsonify
from sklearn.preprocessing import LabelEncoder
from keras.models import load_model
from str_cleaning import clean_all_str, clean_single_str
from wiki_finder import wiki_finder
import numpy as np
import cPickle as pickle
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException 
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from pyvirtualdisplay import Display
display = Display(visible=0, size=(800, 600))
display.start()
driver = webdriver.Chrome('/usr/local/bin/chromedriver')

with open('query_id.pkl', 'rb') as fp:
    query_id = pickle.load(fp)
raw_y = np.load('raw_y_280k.npy')
encoder = LabelEncoder()
encoder.fit(raw_y)
model = load_model("nn_280k.H5")
text = "abcdefghijklmnopqrstuvwxyz"
chars = sorted(list(set(text)))
char_indices = dict((c, i) for i, c in enumerate(chars))

def find_ticker(input_string):
    
    loginurl = 'https://www.google.com/finance?q='
    address = loginurl + input_string.replace('&','%26')
    driver.get(address)
    ti = 1
    results = {'name': 'N/A', 'ticker': 'N/A', 'exchange':'N/A', 'method': "no_match"}

    try:
        driver.find_element_by_xpath('//*[@id="gf-viewc"]/div/div[2]/font/a/b/i').click()
    except:
        pass
    try:
        WebDriverWait(driver, ti).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="cc-table"]/tbody[1]/tr/td[1]')))
        results['ticker'] = str(driver.find_element_by_xpath('//*[@id="cc-table"]/tbody[1]/tr/td[1]').text)
        results['name'] = str(driver.find_element_by_xpath('//*[@id="appbar"]/div/div[2]/div[1]/span').text)
        exchange = str(driver.find_element_by_xpath('//div[@class="elastic"]').text).split('\n')
        for items in exchange:
            if ":" in items:
                results['exchange'] = items.split(':')[0].replace('(','')

        results['method'] = 'public'
    except:
        try:
            WebDriverWait(driver, ti).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="rct-1"]')))
            results['ticker'] = str(driver.find_element_by_xpath('//*[@id="rct-1"]').text)
            results['name'] = str(driver.find_element_by_xpath('//*[@id="rc-1"]').text)
            results['exchange'] = str(driver.find_element_by_xpath('//*[@id="gf-viewc"]/div/div[2]/form/table/tbody/tr[1]/td[2]').text)
            results['method'] = 'public_list'
            results = wiki_validate(results)
        except:
            try:
                results['ticker'] = str(driver.find_element_by_xpath('//*[@id="gf-viewc"]/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[2]').text)
                results['name']= str(driver.find_element_by_xpath('//*[@id="gf-viewc"]/div/div/div[2]/div[2]/div/div[3]/table/tbody/tr[1]/td[1]').text)
                results['method'] = 'private_list'
                results = wiki_validate(results)
            except:
                try:
                    results['ticker'] = str(driver.find_element_by_xpath('//*[@id="gf-viewc"]/div/div[2]/form/table/tbody/tr[1]/td[2]').text)
                    results['name'] = str(driver.find_element_by_xpath('//*[@id="rc-1"]').text)
                    results['method'] = 3
                    results = wiki_validate(results)
                except:
                    try:
                        results['ticker'] = str(driver.find_element_by_xpath('//*[@id="appbar"]/div/div[2]/div[2]/span').text)
                        results['name'] = str(driver.find_element_by_xpath('//*[@id="appbar"]/div/div[2]/div[1]/span').text)
                        results['method'] = 4
                        results = wiki_validate(results)
                    except:                    
                        pass                                                
    return results

def wiki_validate(results):
    wiki = wiki_finder(results['name'])
    
    if wiki['method'] == 'no_match':
        return results
    wiki_name = wiki['name']
    wiki_ticker = wiki['ticker']
    
    if results['ticker'] == wiki_ticker:
        return results
    else:
        ft = find_ticker(wiki_name)
        ft['method'] = 'wiki'
        return ft

def nn_pred(input_string, prob_cutoff = 0.8):

    query_id['query_id'] += 1

    strings = [str(input_string).lower()]

    for i, s in enumerate(strings):
        temp = s.split()[0:5]
        for x, y in enumerate(temp):
            slist = []
            for c in y:
                if c in char_indices.keys():
                    slist.append(c)
            temp[x] = "".join(slist)
        strings[i] = " ".join(temp)
    
    X = np.zeros((len(strings), 5, 25, len(chars)), dtype=np.bool)
    
    for i, sentence in enumerate(strings):
        temp = sentence.split()
        for w, words in enumerate(temp):
            for t, char in enumerate(words):
                if t>24:
                    continue
                X[i, w, t, char_indices[char]] = 1
        
    X = X.reshape(X.shape[0], X.shape[1] * X.shape[2] * X.shape[3])
    
    y_pred_probas = model.predict_proba(X, verbose=0)
    y_pred_proba = y_pred_probas.max(axis=1)
    y_hat = model.predict_classes(X, verbose=0)
    y_pred = encoder.inverse_transform(y_hat)

    if y_pred_proba[0] > prob_cutoff:
        cf = 'high'
    else:
        cf = 'low'

    if y_pred[0] == "other":
        parent_name = "N/A"
        parent_id = "N/A"
        ticker = "N/A"
        exchange = "N/A"
        merchant_id = "N/A"
        method = "N/A"
    else:
        fm = find_ticker(y_pred[0])
        parent_name = fm['name']
        parent_id = abs(hash(parent_name)) % 1000000
        ticker = fm['ticker']
        exchange = fm['exchange']
        merchant_id = abs(hash(y_pred[0])) % 1000000
        method = fm['method'] 

    orig_str = (input_string[:100] + '...') if len(input_string) > 100 else input_string

    if ticker == 'Private':
        ticker = 'N/A'

    if ticker == "N/A":
        status = 'private'
    else:
        status = 'public'

    results = {
               "query_id": query_id['query_id'],
               "original_string": orig_str,
               "merchant_name": y_pred[0], 
               "merchant_id": merchant_id, 
               "parent_name": parent_name, 
               "parent_id": parent_id, 
               "ticker": ticker, 
               "exchange": exchange, 
               "status": status, 
               "industry_id": "N/A",
               "confidence score": "{0:.2f}".format(y_pred_proba[0]), 
               "confidence level": cf,
               #"method": method
               }

    with open('query_id.pkl', 'wb') as fp:
        pickle.dump(query_id, fp)

    return results

app = Flask(__name__)

@app.route('/')
def api_root():
    intro =  '''
<html>
<body>

<p>
<strong><u>Welcome to the Ticker Tagging API (beta)! </u></strong><br>   
<br>
<strong>Type a string at the end of the URL above like this:</strong><br>
<br>
&nbsp&nbsp&nbsp&nbsp&nbsp http://XXX.XXX.XXX.XXX:5001/[Enter Input String Here]<br>
<br>
<strong>The following fields will be returned in a JSON:</strong><br>
<br>
{<br>
&nbsp&nbsp&nbsp&nbsp  "query_id",<br>
&nbsp&nbsp&nbsp&nbsp  "original_string": only the first 100 characters are displayed,<br>
&nbsp&nbsp&nbsp&nbsp  "merchant_name",<br>
&nbsp&nbsp&nbsp&nbsp  "merchant_id",<br>
&nbsp&nbsp&nbsp&nbsp  "parent_name",<br>
&nbsp&nbsp&nbsp&nbsp  "parent_id",<br>
&nbsp&nbsp&nbsp&nbsp  "ticker",<br>
&nbsp&nbsp&nbsp&nbsp  "exchange",<br>
&nbsp&nbsp&nbsp&nbsp  "status": public / private,<br>
&nbsp&nbsp&nbsp&nbsp  "industry_id",<br>
&nbsp&nbsp&nbsp&nbsp  "confidence score": [0,1],<br>
&nbsp&nbsp&nbsp&nbsp  "confidence level": high / low<br>
}<br>
<br>
ADG &copy; 2017<i> All Rights Reserved.</i>
</p>

</body>
</html>

    '''
    return intro

@app.route('/<input_string>')
def ticker_tagging(input_string):
    result = nn_pred(input_string=input_string, prob_cutoff=0.8)
    return jsonify(result)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001, debug=True)
    #app.run(host='0.0.0.0', debug=True)