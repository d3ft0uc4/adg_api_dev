import h5py
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils
from keras.optimizers import RMSprop
from keras.optimizers import SGD
from keras.preprocessing.text import Tokenizer, text_to_word_sequence
from keras.preprocessing.sequence import pad_sequences
from keras.models import load_model
from collections import Counter, deque
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import LabelEncoder, LabelBinarizer
import numpy
import random
import sys
import itertools
import os
import pandas as pd
import numpy as np
import re
import random
import sqlalchemy
from sqlalchemy import *
import argparse
from neural_net.nn_config import *
import warnings



class str_encoder(object):
    def __init__(self, max_words=5, max_word_length=25, max_sentence_length=60):
        text = "abcdefghijklmnopqrstuvwxyz"
        self.chars = sorted(list(set(text)))
        self.max_words = max_words
        self.max_word_length = max_word_length
        self.max_sentence_length=max_sentence_length
        self.char_indices = dict((c, i) for i, c in enumerate(self.chars))
        self.lb = LabelBinarizer()
        self.le = LabelEncoder()
        self.norm_data = None
        self.training = None
        self.testing = None
        self.X_train = None
        self.y_train = None
        self.X_test = None
        self.y_test = None

    def normalize_strings(self, strings):
        try:
            str_array = strings.values.copy()
        except:
            str_array = strings.copy()
        for i, s in enumerate(str_array):
            temp = s.split()[0:self.max_words]
            for x, y in enumerate(temp):
                slist = []
                for c in y:
                    if c in self.char_indices.keys():
                        slist.append(c)
                temp[x] = "".join(slist)
            str_array[i] = " ".join(temp)
        print('dataset size:', len(str_array))
        return str_array
    
    def normalize_data(self, dataset, features, targets):
        orig_strings = dataset[features].astype(str)
        cleaned_strings = self.normalize_strings(orig_strings)
        raw_y = dataset[targets].astype(str).values
        self.norm_data = list(zip(orig_strings, cleaned_strings, raw_y))
        self.lb.fit(raw_y)
        self.le.fit(raw_y)
        print('no of classes:', len(self.lb.classes_))
        return self.norm_data, self.lb

    def tt_split(self, rand=True, seed=6, split=0.8):
        if rand:
            random.seed(seed)
            random.shuffle(self.norm_data)
        size = len(self.norm_data)
        point = int(size * split)
        self.training = self.norm_data[:point]
        self.testing = self.norm_data[point:]
        return self.training, self.testing

    @staticmethod
    def word_rotation(sentence):
        lng = len(sentence.split())
        ls = []    
        for n in range(lng-1):
            d = deque(sentence.split())
            d.rotate(n+1)
            ns = " ".join(list(d))
            ls.append(ns)
        return ls

    def augment(self):
        nitems = []
        for x, y, z in self.training:
            temp = self.word_rotation(y)  
            if len(temp) > 0:
                nitems.extend(list(zip(itertools.repeat(x), temp, itertools.repeat(z))))
        self.training.extend(nitems)
        return self.training

    def count_vectorizer(self):
        print("Creating the bag of letters...\n")

        vectorizer = CountVectorizer(analyzer = "char",                                        tokenizer = None,                                         preprocessor = None,                                      stop_words = None,                                        decode_error="ignore",
                                     vocabulary=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
                                    ) 

        x_str = []
        y_str = []
        z_str = []
        for x, y, z in self.training:
            x_str.append(x)
            y_str.append(y)
            z_str.append(z)
        orig_strings_subset = np.array(x_str)
        strings_subset = np.array(y_str)
        raw_y_subset = np.array(z_str)

        S = []
        for x in strings_subset:
            subset_features = vectorizer.fit_transform(x.split())
            S.append(subset_features.toarray())
        self.X_train = pad_sequences(S)
        print("original X_train Shape", self.X_train.shape)
        self.X_train = self.X_train.reshape(self.X_train.shape[0], self.X_train.shape[1] * self.X_train.shape[2])
        print("new X_train Shape", self.X_train.shape)
        self.y_train = self.lb.transform(raw_y_subset)
        print("y_train Shape", self.y_train.shape)
        
        x_str = []
        y_str = []
        z_str = []
        for x, y, z in self.testing:
            x_str.append(x)
            y_str.append(y)
            z_str.append(z)
        orig_strings_subset = np.array(x_str)
        strings_subset = np.array(y_str)
        raw_y_subset = np.array(z_str)

        S = []
        for x in strings_subset:
            subset_features = vectorizer.fit_transform(x.split())
            S.append(subset_features.toarray())
        self.X_test = pad_sequences(S)
        print("original X_test Shape", self.X_test.shape)
        self.X_test = self.X_test.reshape(self.X_test.shape[0], self.X_test.shape[1] * self.X_test.shape[2])
        print("new X_test Shape", self.X_test.shape)
        self.y_test = self.lb.transform(raw_y_subset)
        print("y_test Shape", self.y_test.shape)

        return self.X_train, self.y_train, self.X_test, self.y_test

    def one_hot_char_encoding(self, word_level_padding=True, word_rotation=False):
        if word_rotation:
            self.augment()
        x_str = []
        y_str = []
        z_str = []
        for x, y, z in self.training:
            x_str.append(x)
            y_str.append(y)
            z_str.append(z)
        y_str = [''.join(n.split()) for n in y_str]
        
        orig_strings_subset = np.array(x_str)
        strings_subset = np.array(y_str)
        raw_y_subset = np.array(z_str)
        
        if word_level_padding:
            self.X_train = np.zeros((len(strings_subset), self.max_words, self.max_word_length, len(self.chars)), dtype=np.bool)
            for i, sentence in enumerate(strings_subset):
                tokens = sentence.split()
                for w, words in enumerate(tokens):
                    for t, char in enumerate(words):
                        if t>(self.max_word_length-1):
                            continue
                        self.X_train[i, w, t, self.char_indices[char]] = 1
            print("original X_training Shape", self.X_train.shape)
            self.X_train = self.X_train.reshape(self.X_train.shape[0], self.X_train.shape[1] * self.X_train.shape[2] *self.X_train.shape[3])
            
        else:        
            self.X_train = np.zeros((len(strings_subset), self.max_sentence_length, len(self.chars)), dtype=np.bool)
            for i, sentence in enumerate(strings_subset):
                for t, char in enumerate(sentence):
                    if t>(self.max_sentence_length-1):
                        continue
                    self.X_train[i, t, self.char_indices[char]] = 1
            print("original X_training Shape", self.X_train.shape)
            self.X_train = self.X_train.reshape(self.X_train.shape[0], self.X_train.shape[1] * self.X_train.shape[2])

        print("new X_training Shape", self.X_train.shape)
        self.y_train = self.lb.transform(raw_y_subset)
        print("y_training Shape", self.y_train.shape)
        
        print("")
        
        x_str = []
        y_str = []
        z_str = []
        for x, y, z in self.testing:
            x_str.append(x)
            y_str.append(y)
            z_str.append(z)
        y_str = [''.join(n.split()) for n in y_str]
        
        orig_strings_subset = np.array(x_str)
        strings_subset = np.array(y_str)
        raw_y_subset = np.array(z_str)
        
        if word_level_padding:
            self.X_test = np.zeros((len(strings_subset), self.max_words, self.max_word_length, len(self.chars)), dtype=np.bool)
            for i, sentence in enumerate(strings_subset):
                tokens = sentence.split()
                for w, words in enumerate(tokens):
                    for t, char in enumerate(words):
                        if t>(self.max_word_length-1):
                            continue
                        self.X_test[i, w, t, self.char_indices[char]] = 1
            print("original X_test Shape", self.X_test.shape)
            self.X_test = self.X_test.reshape(self.X_test.shape[0], self.X_test.shape[1] * self.X_test.shape[2] *self.X_test.shape[3])
            
        else:        
            self.X_test = np.zeros((len(strings_subset), self.max_sentence_length, len(self.chars)), dtype=np.bool)
            for i, sentence in enumerate(strings_subset):
                for t, char in enumerate(sentence):
                    if t>(self.max_sentence_length-1):
                        continue
                    self.X_test[i, t, self.char_indices[char]] = 1
            print("original X_test Shape", self.X_test.shape)
            self.X_test = self.X_test.reshape(self.X_test.shape[0], self.X_test.shape[1] * self.X_test.shape[2])

        print("new X_test Shape", self.X_test.shape)
        self.y_test = self.lb.transform(raw_y_subset)
        print("y_test Shape", self.y_test.shape)
        
        return self.X_train, self.y_train, self.X_test, self.y_test
    
class neural_net(object):
    def __init__(self, X=None, y=None):
        self.savefile = None
        self.model = None
        self.history = None
        if (len(X) > 0) & (len(y) > 0):
            self.X = X
            self.y = y
            self.build(X, y)
    
    def build(self, X, y):
        # build the model: 3 layers MLP
        print('Build model...')

        self.model = Sequential()
        self.model.add(Dense(512, activation='relu', input_dim=X.shape[1]))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(512, activation='relu'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(512, activation='sigmoid'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(512, activation='sigmoid'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(y.shape[1], activation='softmax'))

        self.model.summary()
        
        #Define solver algorithm and compile model
        sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
        self.model.compile(loss='categorical_crossentropy',
              optimizer=sgd,
              metrics=['accuracy'])
        
        return self.model
    
    def fit(self, validation_split=nn_validation_split, epochs=nn_epochs, batch_size=nn_batch_size, monitor='val_acc', verbose=1, save_best_only=True, mode='max', wt_filepath=weights_path):
        checkpoint = ModelCheckpoint(wt_filepath, monitor=monitor, verbose=verbose, save_best_only=save_best_only, mode=mode)
        callbacks_list = [checkpoint]
        self.history = self.model.fit(self.X, self.y, validation_split=validation_split, epochs=epochs, batch_size=batch_size, callbacks=callbacks_list)
        return self.model, self.history
    
    def predict(self, X, le):
        y_pred_probas = self.model.predict_proba(X, verbose=0)
        y_pred_proba = y_pred_probas.max(axis=1)
        y_hat = self.model.predict_classes(X, verbose=0)
        y_pred = le.inverse_transform(y_hat)
        return y_pred, y_pred_proba

    def test_results(self, subset, X, le):
        y_pred, y_pred_proba = self.predict(X, le)
        x_str = []
        y_str = []
        z_str = []
        for x, y, z in subset:
            x_str.append(x)
            y_str.append(y)
            z_str.append(z)
        orig_strings_subset = np.array(x_str)
        strings_subset = np.array(y_str)
        raw_y_subset = np.array(z_str)
        df = pd.DataFrame(np.column_stack([orig_strings_subset, strings_subset, y_pred, y_pred_proba, raw_y_subset]), columns=['orig_str', 'cleaned_str', 'y_pred', 'y_pred_proba', 'y_actual'])
        return df
    
    @staticmethod
    def score(df):
        df['score'] = np.where(df.y_pred==df.y_actual, 1, 0)
        acc = df['score'].astype(float).sum()/len(df['score'])
        return acc
    
    def save_model(self, model_path=save_model_path):
        self.model.save(model_path)
        print('Model saved to ' + model_path)
        
    def load_model(self, model_path=load_model_path):
        self.model = load_model(model_path)
        print('Model loaded from ' + model_path)
        
    def load_weights(self, wt_filename = weights_path):
        self.model.load_weights(wt_filename)

parser = argparse.ArgumentParser()
parser.prog = 'neural_net'
parser.description = "Preprocess and encode string data for neural net training.  Validate model, calculate test accuracy and make predictions using traied model."
parser.add_argument('-e', '--encode', help="Encoding method.  The model's default, or 'char', is one-hot character encoding. 'count' is count vectorizer.")
parser.add_argument('-r', '--rotate', help="Augment training set by rotating input strings by words.")
parser.add_argument('-z', '--padding', help="Zero padding method.  The model's default, or 'word', is word-level padding.  'sentence' is padding at the sentence level.")
parser.add_argument('-p', '--predict', help="Load model from disk and return model predictions and their respective probabilties.")

if __name__ == '__main__':

    namespace = parser.parse_args(sys.argv[1:])
    dataset = pd.read_csv(input_file, error_bad_lines=False,engine='python')
    dataset = dataset[0:1000]
    encoder = str_encoder()
    encoder.normalize_data(dataset, input_column_name, target_column_name)
    if namespace.predict:
        encoder.training = encoder.norm_data
        encoder.testing = encoder.norm_data
    else:
        encoder.tt_split(split=split_ratio)

    if namespace.encode == 'count':
        encoder.count_vectorizer()
    else:
        if namespace.rotate:
            rotate_bool=True
        else:
            rotate_bool=False

        if namespace.padding == 'sentence':
            padding_bool=False
        else:
            padding_bool=True
            encoder.one_hot_char_encoding(word_level_padding=padding_bool, word_rotation=rotate_bool)

    if namespace.predict:
        model = neural_net(encoder.X_train, encoder.y_train)
        model.load_model()
        pred = model.predict(encoder.X_test, encoder.le)
        print(pred)
    else:
        model = neural_net(encoder.X_train, encoder.y_train)
        model.fit()
        df = model.test_results(encoder.testing, encoder.X_test, encoder.le)
        print('test score = ' + str(round(model.score(df),3)))
        print(df)
        model.save_model(save_model_path)