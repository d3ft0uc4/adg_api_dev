options(scipen=999)
#install.packages(c("xgboost","readr","dplyr"))
library(xgboost)

library(readr)
library(dplyr)

library(e1071)

root = "/home/wolfie/Dropbox (ADG)/adg_api_dev_share/API/ERs/find_maker/2019.06.04.23.34"
name = "find_maker.2019.06.04.23.34"
scores_file = paste(root, paste0(name, ".csv"), sep="/")

setwd(".")
rawin<-read.csv(scores_file) # grab from file - either or is fine

raw_train <- rawin %>% select(-starts_with("X."))
# raw_train <- subset(raw_train, match!="?")
raw_train[is.na(raw_train)]<-0
names(raw_train)[1]<-"match"
ourlabs<-as.numeric(raw_train$match)
numclases<-length(unique(raw_train[,1]))

# yo<-svm(x=as.matrix(raw_train[,2:ncol(raw_train)]), y=raw_train[,1], cross=8)

evalerror <- function(preds, dtrain) {
  labels <- getinfo(dtrain, "label")
  FP <-  (labels == 1) & (preds < 0.5)
  FN  <-  (labels == 0) & (preds > 0.5)
  weightFP <- 3
  err <-    sum(FP*weightFP + FN)   / ( length(labels)* weightFP)
  #err <- as.numeric(sum(labels != (preds > 0)))/length(labels)
  return(list(metric = "error", value = err))
}

xgmodel<-xgboost(data=as.matrix(raw_train[,2:ncol(raw_train)])  ##s  
                 ,booster="gbtree"
                 ,label=ourlabs
                 ,lambda=.1
                 ,lambda_bias =.1
                 ,alpha=.1
                 ,subsample = .80
                 ,colsample_bytree=.85
                 ,eta = .05
                 ,gamma=0.1
                 ,max_depth = 4
                 ,min_child_weight =10
                 ,objective = "binary:logistic"
                 #, eval_metric ="auc"
                 #,num_class = (numclases)
                 ,feval=evalerror
                 ,maximize = F
                 ,early_stopping_rounds=200
                 ,nrounds=1500
                 ,verbose = T
                 #,save_period =4000
                 ,nfold=10
)

xgmodel$best_ntreelimit <- NULL
our_preds<-predict(xgmodel,as.matrix(raw_train[,-1]))
rawin$model_predictions<-round(our_preds)
rawin$model_score_normalized<-our_preds

our_probs<-predict(xgmodel,as.matrix(raw_train[,-1]), outputmargin=T)
rawin$model_score_raw<-our_probs

# our_probs<-yo$fitted

# xgb.plot.multi.trees(xgmodel, feature_names=colnames(raw_train), features_keep = 8)

xgb.importance(model=xgmodel)
xgb.save(xgmodel, paste(root, paste0(name, ".XGBmodel"), sep="/"))
write_csv(rawin, paste(root, paste0(name, ".predictions.csv"), sep="/"))
