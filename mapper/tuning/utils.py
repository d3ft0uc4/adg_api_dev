"""
Module for automatic settings adjustment
"""
from itertools import product, chain
from operator import itemgetter
from typing import List, Optional, Tuple
import logging
import os
from tempfile import gettempdir
from collections import Counter
import json

import matplotlib.pyplot as plt
# import sympy
# from sympy.solvers.inequalities import solve_rational_inequalities
# import mystic

from mapper.settings import db_engine
from packages.utils import similar, remove_dupes, NeverFail, StrFallbackJSONEncoder
from unofficializing.unofficializing import make_unofficial_name as uo

from mapper.entity_recognition import bane, fwfm
from mapper.settings import BaneConfig, FwFmConfig
from mapper.logs import load_logs

logger = logging.getLogger(f'adg.{__name__}')


def adjust_str_dist_thld():
    raise NotImplementedError()


def ask_for_answer(raw_input: str, choices: List[str] = None) -> Tuple[List[str], Optional[str]]:
    """
    Asks user to input company name & ticker for specific raw input string.

    User's response is saved to answers database and returned as (company_names, ticker) tuple.
    """
    print('----------------------')
    print(f'Raw input: {raw_input}')

    print('Please enter company name(s), separated by `|`')
    for i, choice in enumerate(choices):
        print(f'[{i}] {choice}')

    company_names = input().split('|')
    for i, name in enumerate(company_names):
        if name.isdigit():
            company_names[i] = choices[i]

    ticker = input('Please input ticker: ')

    db_engine.execute(
        f"""
        INSERT INTO answers
        SET
            raw_input = %(raw_input)s,
            company_name = %(company_name)s,
            ticker = %(ticker)s
        ON DUPLICATE KEY UPDATE
            company_name = %(company_name)s,
            ticker = %(ticker)s
        """,
        raw_input=raw_input,
        company_name='|'.join(company_names),
        ticker=ticker,
    )

    return company_names, ticker


def generate_constraints(log: dict) -> List[dict]:
    """ Given result from log and answer, generate set of rules which will lead to correct answer """
    constraints = []

    data = log['snapshot']

    # # populate missing answer
    # if not answer:
    #     choices = []
    #     for attempt in data.get('attempts', []):
    #         for group in attempt.get('winners') + attempt.get('losers'):
    #             choices.extend([entity.value for entity in group['graph']])

    #     choices = remove_dupes(choices)

    #     answer = ask_for_answer(log['raw_input'], choices=choices)

    correct_companies = [uo(name) for name in log['correct_company_names']]
    for attempt in data.get('attempts', []):
        for group in attempt.get('losers') + attempt.get('winners'):
            entities = group['graph']
            company_names = [uo(entity.value) for entity in entities]

            should_win = any(
                similar(name1, name2)
                for name1, name2
                in product(
                    correct_companies,
                    company_names,
                )
            )

            # conf_fn = sum([
            #     sympy.symbols(f'k_{entity["source"]}', real=True, postive=True) * entity['score']
            #     for entity in entities
            #     if len(entity['sources']) == 1
            # ])
            #
            #
            # constraints.append(
            #     ((sympy.Poly(conf_fn), CONF_LEVEL_LOW), '>=' if should_win else '<')
            # )

            constraints.append({
                'result': should_win,
                **{
                    entity.source: entity.get('_score') or entity.score
                    for entity in entities
                    if len(entity.sources) == 1
                },
                'n_sources': len(set(entity.source for entity in entities if entity.score)),
            })

    return constraints


# def adjust_comp_graph(dt: timedelta = timedelta(days=14), ask_for_answer: bool = True):
#     """
#     Adjust comparison graph scores based on db log and answers database.

#     For each comparison graph and correct answer, creates an inequity -
#     which values should the scores have in order to successfully select correct winner.

#     List of inequities is passed to inequity solving system.

#     Args:
#         since - since which date take snapshots
#         ask_for_answer - whether to require user to input answers in case of lack
#     """
#     # load logs
#     logs = load_logs(dt=dt, distinct=True)

#     # # load answers
#     # answers = engine.execute(
#     #     f"""
#     #     SELECT * FROM (
#     #         SELECT raw_input, company_name, ticker
#     #         FROM golden_set_domain_merchant
#     #         UNION ALL
#     #         SELECT raw_input, company_name, ticker
#     #         FROM answers
#     #         WHERE raw_input IN %(raw_inputs)s
#     #     ) x
#     #     GROUP BY raw_input
#     #     """,
#     #     raw_inputs=[res['raw_input'] for res in results],
#     # ).fetchall()
#     # answers = [dict(a) for a in answers]

#     # # convert results and answers to dicts
#     # results = {value['raw_input']: value for value in results}
#     # answers = {value['raw_input']: value for value in answers}

#     # # generate constraints
#     # constraints = []
#     # if not ask_for_answer:
#     #     rawins_with_answers = [rawin for rawin in results if rawin in answers]
#     #     results = {rawin: value for rawin, value in results.items() if rawin in rawins_with_answers}

#     logs = [log for log in logs if 'correct_company_names' in log]
#     total = len(logs)
#     constraints = []
#     for i, log in enumerate(logs, start=1):
#         logger.debug(f'{i} / {total}')
#         # answer = answers.get(rawin, {})
#         # if not answer and not ask_for_answer:
#         #     logger.debug('Skipping...')
#         #     continue

#         constraints.extend(generate_constraints(log))

#     fields = set.union(*(set(key for key in constraint.keys() if key != 'result') for constraint in constraints))
#     fields = sorted(fields)

#     print('----- CSV for Gene -----')
#     print(','.join(['response'] + fields))
#     for constraint in constraints:
#         print(','.join(['1' if constraint['result'] else '0'] + [str(constraint.get(field, 0)) for field in fields]))
#     print('------------------------')

#     fields.append('_')
#     buffer = []
#     for constraint in constraints:
#         buffer.extend(constraint.get(field, 0.0) if field != '_' else 1.0 for field in fields)

#     x = np.ndarray(
#         shape=(len(constraints), len(fields)),
#         buffer=np.array(buffer),
#     )
#     y = np.ndarray(
#         shape=(len(constraints), 1),
#         buffer=np.array([CONF_LEVEL_LOW * 2.0 if constraint['result'] else 0.0 for constraint in constraints]),
#     )

#     pretty(constraints)

#     ks = np.linalg.lstsq(x, y)
#     residual = ks[1][0]
#     ks = map(itemgetter(0), ks[0])
#     ks = dict(zip(fields, ks))

#     for constraint in constraints:
#         print(f'{constraint["result"]} |'
#               f'{sum(value * ks.get(key, 0) for key, value in constraint.items()) + 1.0 * ks["_"]}')

#     pretty(f'Residual: {residual}')
#     pretty(ks)


@NeverFail(fallback_value=([], [], []))
def get_groups(log: dict) -> Tuple[List[dict], List[dict], List[dict]]:
    """ Given log row from golden set, returns groups from all attempts that really should fail / should win """
    assert 'correct_company_names' in log

    unknown_groups = []
    wrong_groups = []
    correct_groups = []  # groups that produced correct results

    for attempt in log['snapshot']['attempts']:
        for group in attempt.get('losers', []) + attempt.get('winners', []):
            values = [uo(entity.get('value') or entity.get('name')) for entity in group['graph']]
            correct_values = [uo(value) for value in log['correct_company_names']]

            pairs = list(product(values, correct_values))
            if any(similar(*pair) for pair in pairs):
                correct_groups.append(group)
            elif any(pair[0][:4] == pair[1][:4] for pair in pairs):
                logger.debug('\n'.join([
                    f'Not sure whether group should win or not:',
                    f'Correct: {correct_values}',
                    f'Group: {values}',
                ]))
                unknown_groups.append(group)
            else:
                wrong_groups.append(group)

            # add some extra info to group
            group['raw_input'] = log['raw_input']
            group['cleaned'] = attempt['cleaned']
            group['group_values'] = [f'{entity.get("value") or entity.get("name")} - {entity.get("source", "")}'
                                     for entity in group['graph']]
            group['correct_company_names'] = log['correct_company_names']

    return unknown_groups, wrong_groups, correct_groups


def tune_entity_recognizer(recognizer: str):
    """ Function which adjusts internal thresholds for entity recognition tools """

    # load latest snapshots on golden set
    logger.debug(f'Loading logs')
    logs = load_logs(golden_only=True)

    # set up the tools to run without any internal threshold
    if recognizer == 'bane':
        class ZeroThldBaneConfig(BaneConfig):
            WINNER_THLD = 0

        def er_tool(value):
            # TODO: overwrite config for bane
            return bane(value)  # , config=ZeroThldBaneConfig)

    elif recognizer == 'fwfm':
        class ZeroThldFwFmConfig(FwFmConfig):
            WINNER_THLD = 0

        def er_tool(value):
            # TODO: overwrite config for fwfm
            return fwfm(value)  # , config=ZeroThldFwFmConfig)

    else:
        raise ValueError(f'Unknown recognizer: {recognizer}')

    # iterate over logs and collect score values
    correct = []  # ER entities that match to any correct company name
    wrong = []  # ER entities that don't match correct company names

    total = len(logs)
    for i, log in enumerate(logs, start=1):
        logger.debug(f'{i} / {total}')
        _, _, correct_groups = get_groups(log)
        # now select all winning entities - those which belong to correct groups (those which contains a winner)
        correct_entities = chain(*list(map(itemgetter('graph'), correct_groups)))
        correct_values_uo = [uo(entity['value']) for entity in correct_entities]
        # ^-- these unofficial values are correct answers

        # also add answers themselves (this helps if there's no winning groups at all)
        correct_values_uo += list(map(uo, log['correct_company_names']))

        correct_values_uo = remove_dupes(correct_values_uo)

        for attempt in log['snapshot']['attempts']:
            cleaned = attempt['cleaned']
            if not cleaned:
                continue
            entities = er_tool(cleaned)

            for entity in entities:
                value_uo = uo(entity.value)
                if any(similar(value_uo, correct_value_uo) for correct_value_uo in correct_values_uo):
                    correct.append(entity)
                else:
                    wrong.append(entity)

    path = os.path.join(gettempdir(), f'{recognizer}_entities.json')
    with open(path, 'w') as f:
        data = json.dumps({'correct': correct, 'wrong': wrong}, cls=StrFallbackJSONEncoder)
        f.write(data)
        logger.debug(f'Wrote entities to {path}')

    correct_scores = list(map(itemgetter('score'), correct))
    wrong_scores = list(map(itemgetter('score'), wrong))

    # discrete scores and get their distribution
    precision = 2
    keys = [round(x/pow(10, precision), precision) for x in range(0, pow(10, precision) + 1)]

    correct_distribution = Counter([round(score, precision) for score in correct_scores])
    correct_distribution = {key: correct_distribution[key] for key in keys}

    wrong_distribution = Counter([round(score, precision) for score in wrong_scores])
    wrong_distribution = {key: wrong_distribution[key] for key in keys}

    # draw plots
    plt.xlabel('scores')
    plt.ylabel('occurrences')
    plt.axis(
        [0, 1, 0, max(list(correct_distribution.values()) + list(wrong_distribution.values()))]
    )
    plt.plot(correct_distribution.keys(), correct_distribution.values(), 'g', label='correct')
    plt.plot(wrong_distribution.keys(), wrong_distribution.values(), 'r', label='wrong')
    plt.show()


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument(
        'mode',
        help='Which tuning tool to run',
        const='all',
        nargs='?',
        choices=['er_thld', 'group_score'],
    )
    parser.add_argument('target', help='Specific target for selected tool')
    # parser.add_argument('--ask', help='Whether to ask user for answers (if answer unknown)',
    #                     default=True, action='store_true')
    args = parser.parse_args()

    if args.mode == 'er_thld':
        assert args.target in ['bane', 'fwfm']
        tune_entity_recognizer(args.target)

    print('Done')
