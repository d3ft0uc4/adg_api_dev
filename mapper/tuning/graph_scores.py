"""
Module for training group score calculation using LogisticRegression on entities' scores.

Run:
    python -m mapper.tuning.graph_scores [domain|merchant] [--train-linear-regression]
"""
import csv
import logging
import pickle
from collections import defaultdict
from itertools import chain, product, combinations
from os import path, makedirs
from typing import List, Tuple

import networkx as nx
from sklearn.linear_model import LogisticRegression

from mapper.logs import load_logs
from mapper.settings import DROPBOX
from packages.utils import partition, similar, read_list, get_timestamp
from packages.xgboost.prediction import xgb_load_model, xgb_predict
from unofficializing.unofficializing import make_unofficial_name as uo
from .utils import get_groups

logger = logging.getLogger(f'adg.{__name__}')


DATA_DIR = path.join(path.dirname(path.abspath(__file__)), 'data')
SCORES_FILE_PATH = path.join(DROPBOX, 'group model', '{mode}', '{version}', '{mode}.{version}.csv')


def get_scores(entities: list) -> dict:
    """ Given graph of nodes, returns scores dict - an input to logistic regression """
    scores = defaultdict(int)

    # filter out entities that don't have score and which are not of kind "company"
    all_entities = entities
    entities = [entity for entity in entities if entity.get('score') and entity.get('kind', 'company') == 'company']

    # 1) add {additional param: param's score}
    entities_1st_gen, entities_2nd_gen = partition(
        lambda entity: entity['source'].startswith('company_website['),
        entities
    )

    scores['num_entities'] = sum([  # number of entities (2nd gen entities number multiplied by 0.25)
        len(set([entity['source'] for entity in entities_1st_gen])),
        len(set([entity['source'][len('company_website'):] for entity in entities_2nd_gen])) * 0.25,
        # -0.25 if next(filter(lambda entity: entity['source'] == 'company_website', entities_1st_gen), None) else 0,
    ])

    # 2) add {entity source: entity score} values
    for entity in entities:
        scores[entity['source']] += entity['score']

    # 3) some additional scores
    scores['company_node_exists'] = int(any(entity.get('kind', 'company') == 'company' for entity in all_entities))

    ers_correlation = {
        ('fwfm', 'bane'): 0.5,
        ('fwfm', 'company_website'): 0.25,
        ('fwfm', 'small_parsers'): 0.25,
        ('fwfm', 'db_lookup'): 0.75,
        ('fwfm', 'website_title'): 0.25,
        ('fwfm', 'tfidf_keywords'): 0.25,

        ('bane', 'company_website'): 0.25,
        ('bane', 'small_parsers'): 0.5,
        ('bane', 'website_title'): 0.25,
        ('bane', 'tfidf_keywords'): 0.25,

        ('company_website', 'small_parsers'): 0.25,
        ('company_website', 'website_title'): 0.25,
        ('company_website', 'tfidf_keywords'): 0.25,

        ('small_parsers', 'website_title'): 0.25,
        ('small_parsers', 'tfidf_keywords'): 0.25,

        ('crunchbase', 'crunchbase_api'): 0.75,

        ('website_title', 'tfidf_keywords'): 0.75,
    }

    scores['ers_distance'] = 0
    for entity1, entity2 in product(all_entities, repeat=2):
        source1 = entity1['source'].split('[')[0]
        source2 = entity2['source'].split('[')[0]

        correlation = ers_correlation.get((source1, source2)) or int(source1 == source2)
        scores['ers_distance'] += 1 - correlation

    return scores


WRONG_INPUTS = read_list(path.join(DATA_DIR, 'wrong_inputs.txt'))


def collect_group_scores(mode: str):
    """ Produces scores for logistic regression training """

    # load latest snapshots on golden set
    logger.debug(f'Loading logs for "{mode}" mode')
    logs = \
        load_logs(golden_only=True, mode=mode, golden_table='golden_set_domain_merchant')
    # load_logs(golden_only=True, mode=mode, golden_table='golden_set_bumped')

    logs = [log for log in logs if log['raw_input'] not in WRONG_INPUTS]
    if not logs:
        raise ValueError('No logs found')

    for log in logs:
        snap = log['snapshot']
        snap['attempts'] = [
            attempt
            for attempt in snap.get('attempts', [])
            if not log['correct_cleaned'] or (log['correct_cleaned'] in attempt['cleaned'])
        ]

    correct_groups = []  # all correct groups
    wrong_groups = []  # all wrong groups
    unknown_groups = []

    # get correct and wrong groups
    total = len(logs)
    for i, log in enumerate(logs, start=1):
        logger.debug(f'{i} / {total}')

        unknown, wrong, correct = get_groups(log)
        unknown_groups.extend(unknown)
        correct_groups.extend(correct)
        wrong_groups.extend(wrong)

    # generate all scores
    scores = []
    for should_win, group in chain(
        [(1, group) for group in correct_groups],
        [(0, group) for group in wrong_groups],
        [('?', group) for group in unknown_groups],
    ):
        scores.append({
            'should_win': should_win,
            **get_scores(group['graph']),
            **{
                f'#{key}': value
                for key, value in group.items()
                if key in ['raw_input', 'cleaned', 'group_values', 'correct_company_names']
            },
        })

    return scores


def make_non_linear(scores: List[float]) -> List[float]:
    """ Given list of linear scores, convert them to non-linear using production """
    result = scores[:]
    result.extend([a*b for a, b in combinations(scores, 2)])
    result.extend([a**2 for a in scores])
    return result


def train_regression(mode: str):
    """ Loads scores from previous step and trains logistic regression """
    with open(SCORES_FILE_PATH.format(mode), 'r') as f:
        data = list(csv.reader(f))
    header, data = data[0][1:], data[1:]

    # now exclude cols starting with '#' from both header and data
    bad_cols = []
    header = [col for i, col in enumerate(header) if not col.startswith('#') or bad_cols.append(i)]  # damn i'm good!
    data = [[val for i, val in enumerate(row, start=-1) if i not in bad_cols] for row in data]

    y = [int(row[0]) for row in data]
    X = [[float(x or 0) for x in row[1:]] for row in data]
    X = [make_non_linear(x) for x in X]

    lr = LogisticRegression(solver='liblinear', multi_class='auto').fit(X, y)
    logger.debug(f'Logistic regression mean accuracy: {lr.score(X, y):.2f}')

    pth = path.join(DATA_DIR, f'logistic_regression.{mode}.pickle')
    with open(pth, 'wb') as f:
        pickle.dump((header, lr), f)
    logger.debug(f'Pickled model to "{pth}"')


merchant_header, merchant_model = xgb_load_model(
    path.join(DATA_DIR, 'xgb_boost.merchant.2019.08.06.14.21.pickle')
)

product_header, product_model = xgb_load_model(
    path.join(DATA_DIR, 'xgb_boost.product.2019.05.14.20.11.pickle')
)

domain_header, domain_model = xgb_load_model(
    path.join(DATA_DIR, 'xgb_boost.domain.2019.04.22.17.57.pickle'))

WINNERS_BLACKLIST = read_list(path.join(
    path.dirname(path.abspath(__file__)),
    'data',
    'winner_blacklist.txt'
))


def calculate_graph_score(graph: nx.DiGraph, mode: str) -> float:
    """ For any entities graph calculates its probability to be a winner. Prob >= 0.5 means winner. """
    if mode == 'merchant':
        header, model = merchant_header, merchant_model

    elif mode == 'product':
        header, model = product_header, product_model

    elif mode == 'domain':
        header, model = domain_header, domain_model

    else:
        raise ValueError(f'Unknown mode: {mode}')

    entities = [node['entity'] for node in graph.nodes.values()]
    scores = get_scores(entities)

    prob = xgb_predict(scores, header, model)
    # logger.debug(f'Group scores: {scores}, prob={prob}')

    # increase graph score if any entity is a keyword
    if mode == 'product':
        keywords = [entity for entity in graph.entities if entity.source == 'keywords']
        if keywords and len(keywords) < len(graph):
            prob += 0.4

    # penalize groups which contain blacklisted values (from WINNERS_BLACKLIST)
    if any(
        similar(value_uo, bad_value)
        for value_uo, bad_value
        in product(
            [uo(entity['value']) for entity in entities],
            WINNERS_BLACKLIST
        )
    ):
        logger.debug(f'Found blacklisted entity -> setting zero score for group '
                     f'{[entity["value"] for entity in entities]}')
        prob = 0

    return min(max(prob, 0), 1)


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('mode')
    parser.add_argument('--train-linear-regression', action='store_true')
    args = parser.parse_args()

    scores = collect_group_scores(args.mode)

    # write csv
    pth = SCORES_FILE_PATH.format(mode=args.mode, version=get_timestamp())
    makedirs(path.dirname(pth), exist_ok=True)
    with open(pth, 'w') as f:
        fieldnames = set(list(chain(*[score.keys() for score in scores])))

        def get_order(field: str) -> Tuple[int, str]:
            if field == 'should_win':
                return 0, field
            elif field.startswith('#'):
                return 1, field
            else:
                return 2, field

        fieldnames = sorted(fieldnames, key=get_order)
        writer = csv.DictWriter(f, fieldnames)
        writer.writeheader()
        writer.writerows(scores)

    logger.debug(f'Wrote results to "{pth}"')

    if args.train_linear_regression:
        train_regression(args.mode)

    print('Done')
