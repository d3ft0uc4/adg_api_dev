from collections import Counter
from itertools import groupby, combinations, product
from typing import Optional

from bing.bing_web_core import BingWebCore
from mapper.website_parsers import select_parser, AmazonParser, WalmartParser, SamsClubParser

bwc = BingWebCore()


def detect_brand(value: str) -> Optional[str]:
    bing_response = bwc.query(value)
    bing_rows = bing_response.get('webPages', {}).get('value', [])[:30]

    # ---- brand ----
    brands = []  # (rank, parser, brand)
    for i, row in enumerate(bing_rows):
        parser = select_parser(row['url'])
        if not parser or parser not in [AmazonParser, WalmartParser, SamsClubParser]:
            continue

        entity = parser.parse(row)
        if entity and entity.get('kind', 'company_name') == 'brand':
            brands.append({
                'rank': i,
                'parser': parser,
                'value': entity.value,
            })

    # ---- ANSWER ----
    answer = None

    # ---- answer from brand ----
    if brands:
        brands.sort(key=lambda brand: brand['parser'].__name__)
        groups = {
            parser_name: [brand for brand in grouper]
            for parser_name, grouper
            in groupby(brands, key=lambda brand: brand['parser'].__name__)
        }

        # if there's the same brand from 2 different parsers - accept it
        for parser1, parser2 in combinations(groups, 2):
            for value1, value2 in product(
                    map(lambda brand: brand['value'].lower(), groups[parser1]),
                    map(lambda brand: brand['value'].lower(), groups[parser2]),
            ):
                if (value1 in value2) or (value2 in value1):
                    answer = min(value1, value2, key=len)
                    break

            if answer:
                break

        if not answer:
            # if there's same brand from same parser - accept it
            for brands in groups.values():
                values = [brand['value'].lower() for brand in brands if brand['rank'] <= 20]
                if not values:
                    continue

                value, frequency = Counter(values).most_common(1)[0]
                if frequency >= 2:
                    answer = value
                    break

    return answer


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('value')
    args = parser.parse_args()

    print(detect_brand(args.value))
