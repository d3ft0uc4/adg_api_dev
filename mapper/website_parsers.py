"""
Module for scraping popular websites.

CLI:
    python -m mapper.website_parsers "https://www.facebook.com/A-One-Oil-343113883430/"
"""

import sys
import inspect
import re
from itertools import filterfalse
from typing import Optional
from operator import attrgetter
import logging
from urllib.parse import urlparse, parse_qs

from bs4 import BeautifulSoup

from cc_cleaner.address_detection import AddressExtractor
from mapper.entity import Entity
from domain_tools.utils import get
from packages.utils import NeverFail, clean_company_name
from domain_tools.company_page_detection import in_blacklist
from mapper.string_cleaner_tokenization.tokenizer import STATE_TO_CITIES

logger = logging.getLogger(f'adg.{__name__}')
DEBUG = __name__ == '__main__'


class WebsiteParser:
    URL = re.compile('')

    # len('American Furniture Manufacturing Co') == 35
    MAX_VALUE_LENGTH = 42

    @classmethod
    def match(cls, url: str) -> bool:
        """ Whether this parser can handle specified url """
        return cls.URL.match(url)

    @classmethod
    @NeverFail(fallback_value=None)
    def parse(cls, bing_result: dict) -> Optional[Entity]:
        """ Convert bing result to Entity """
        entity = cls.parse_bing_result(bing_result)
        if entity:
            return entity

        url = bing_result['url']
        response = get(url)
        if not response:
            return

        logger.debug(f'{cls.__name__} parsing {url}')
        try:
            entity = cls.parse_soup(response.get_soup())
        except Exception as exc:
            logger.warning(f'{cls.__name__}: error during parsing "{url}": {exc}')
            return

        if entity is None:
            return

        if len(entity.value) > cls.MAX_VALUE_LENGTH or not entity.value:
            logger.warning(f'{cls.__name__}: weird entity value: "{entity}" (url: {url})')
            return

        if AddressExtractor(entity.value).has_street():
            logger.warning(f'{cls.__name__}: address detected: "{entity}" (url: {url})')
            return

        if entity.get('websites'):
            entity.websites = list(filterfalse(in_blacklist, entity.websites))
            if not entity.websites:
                del entity['websites']

        return entity

    @classmethod
    def parse_bing_result(cls, bing_result: dict) -> Optional[Entity]:
        """ Quick way to retrieve entity without reaching the website """
        return

    @classmethod
    def parse_soup(cls, soup: BeautifulSoup) -> Optional[Entity]:
        try:
            value = soup.find('h1').text.strip()  # find only first-order text
            value = re.sub(r'\(.*?\)', '', value)  # remove everithing in brackets

            return Entity(value=clean_company_name(value))
        except Exception as exc:
            logger.warning(f'{cls.__name__}: title not found: {exc}')
            return


class YahooLocalParser(WebsiteParser):
    # has website sometimes, https://local.yahoo.com/info-99887693-reverend-jim-s-\dam-pub-buchanan-dam?stx=bars,%20%20Burnet,%20TX%2078611&csz=Burnet,%20TX%2078611&fr=lsrp  # noqa
    URL = re.compile(r'https://local\.yahoo\.com/')


class YelpParser(WebsiteParser):
    # has website sometimes https://www.yelp.com/biz/harris-restaurant-san-francisco
    URL = re.compile(r'https://www\.yelp\.[a-z]{2,3}/biz/')

    @classmethod
    def parse_soup(cls, soup: BeautifulSoup) -> Optional[Entity]:
        try:
            value = ' '.join(map(attrgetter('text'), soup.find_all('h1', {'class': 'biz-page-title'})))
            value = re.sub(r'\(.*?\)', '', value)  # remove everithing in brackets

            return Entity(value=clean_company_name(value))
        except Exception as exc:
            logger.warning(f'{cls.__name__}: title not found: {exc}')
            return


class YellowpagesParser(WebsiteParser):
    # has websites section https://www.yellowpages.ca/bus/Ontario/Toronto/Queen-Spadina-Medical-Centre/1290193.html?what=Medical+Clinics&where=Toronto%2C+ON&useContext=true  # noqa
    URL = re.compile(r'https://www\.yellowpages\.com/')

    @classmethod
    def parse_soup(cls, soup: BeautifulSoup) -> Optional[Entity]:
        if soup.find(class_="search-results"):
            return

        return super().parse_soup(soup)


class ReutersParser(WebsiteParser):
    # has websites https://www.reuters.com/finance/stocks/company-profile/NKE
    URL = re.compile(r'https://www\.reuters\.com/finance/stocks/companyProfile/')

    @classmethod
    def parse_soup(cls, soup: BeautifulSoup) -> Optional[Entity]:
        value = soup.find('h1').findAll(text=True, recursive=False)[0].strip()  # find only first-order text
        value = re.sub(r'\(.*?\)', '', value)  # remove everithing in brackets
        return Entity(value=clean_company_name(value))


class HooversParser(WebsiteParser):
    # sells information http://www.hoovers.com/company-information/cs/company-profile.facebook_inc.f1fe73cc6a208e18.html?aka_re=1#contact-anchor  # noqa
    # no website available
    URL = re.compile(r'http://www\.hoovers\.com/company-information/cs/company-profile')


class TwitterParser(WebsiteParser):
    # can have website in info section https://twitter.com/facebook
    URL = re.compile(r'https://twitter\.com/.+')

    @classmethod
    def parse_soup(cls, soup: BeautifulSoup) -> Optional[Entity]:
        value = soup.find('h1').find('a').text.strip()
        return Entity(value=clean_company_name(value))


class FacebookParser(WebsiteParser):
    # can have website in about section https://www.facebook.com/yandex/
    URL = re.compile(r'https://(www|web)\.facebook\.com/.+')

    @staticmethod
    def get_website(soup: BeautifulSoup):
        try:
            col = soup.find('div', {'id': 'pages_side_column'})
            # about box
            box = col.find_all('div', class_='_1xnd')[0]
            box = box.find_all('div', recursive=False)[1]

            link = box.find_all('span', class_='fwb')[0]
            link = link.find_all('a')[0]

            url = link['href']

            # handle "redirect" url
            if url.startswith('http://l.facebook.com/l.php'):
                url = parse_qs(urlparse(url).query)['u'][0]

            return url

        except LookupError:
            pass
        except Exception as e:
            logging.warning(f'Facebook parser failed to find website link with {e}')

    @classmethod
    def parse_bing_result(cls, bing_result: dict) -> Optional[Entity]:
        return
        # if not bing_result.get('name'):
        #     return
        #
        # return Entity(value=clean_company_name(bing_result['name'].rsplit(' - ', maxsplit=1)[0].strip()))

    @classmethod
    def parse_soup(cls, soup: BeautifulSoup) -> Optional[Entity]:
        h1 = soup.find('h1')
        if not h1:
            logger.debug(f'H1 tag not found')
            # well, facebook contains this tag in source code
            return
        value = (h1.find('span') or h1).text.strip()
        website = cls.get_website(soup)

        # - split by dash might help bit is risky if there will be smth like
        # "Best candies in NYC - Arnold's candyshop"
        # - using a cccleaner doesn't help
        entity = Entity(
            value=clean_company_name(value.rsplit(' - ', maxsplit=1)[0].strip()),
        )

        if website and not in_blacklist(website):
            entity.websites = [website]

        return entity


class MapquestParser(WebsiteParser):
    # can have website section https://www.mapquest.com/us/california/foreign-cinema-11772160?zoom=0
    URL = re.compile(r'https://www.mapquest.com/.+')
    STATES = STATE_TO_CITIES.keys()

    @classmethod
    def parse_soup(cls, soup: BeautifulSoup) -> Optional[Entity]:
        entity = super().parse_soup(soup)

        # discard entity if its name ends with ", {STATE}" pattern
        if entity and re.match(rf'.+, ({"|".join(cls.STATES)})$', entity.value, flags=re.IGNORECASE):
            logger.debug(f'MapquestParser returned location, discarding: {entity}')
            return

        return entity


# ---- Products websites ----

class AmazonParser(WebsiteParser):
    URL = re.compile(r'https://www\.amazon\.com/')

    @classmethod
    def match(cls, url: str) -> bool:
        return super().match(url) and '/s?k=' not in url

    @classmethod
    def parse_soup(cls, soup: BeautifulSoup) -> Optional[Entity]:
        try:
            value = soup.find('a', {'id': 'bylineInfo'}).text.strip()
            value = re.sub(r'\n', '', value)
            value = re.sub(r'\s+', ' ', value)
            return Entity(value=value, kind='brand')
        except Exception as exc:
            logger.debug(f'{cls.__name__}: {exc}')


class WalmartParser(WebsiteParser):
    URL = re.compile(r'https://www\.walmart\.com/')

    @classmethod
    def parse_soup(cls, soup: BeautifulSoup) -> Optional[Entity]:
        try:
            value = soup.find('a', {'class': 'prod-brandName'}).text.strip()
            return Entity(value=value, kind='brand')
        except Exception as exc:
            logger.debug(f'{cls.__name__}: {exc}')


class SamsClubParser(WebsiteParser):
    URL = re.compile(r'https://www\.samsclub\.com/')

    @classmethod
    def parse_soup(cls, soup: BeautifulSoup) -> Optional[Entity]:
        try:
            elements = soup.find_all('', {'class': 'sc-product-header-item-number'})
            matches = [re.match(r'^by (.+)', el.text, flags=re.IGNORECASE) for el in elements]
            matches = [m for m in matches if m]
            if matches:
                return Entity(value=matches[0][1].strip(), kind='brand')
        except Exception as exc:
            logger.debug(f'{cls.__name__}: {exc}')


WEBSITE_PARSERS = [
    obj for _, obj in inspect.getmembers(sys.modules[__name__])
    if inspect.isclass(obj) and issubclass(obj, WebsiteParser) and obj != WebsiteParser
]


def select_parser(url: str) -> Optional[WebsiteParser]:
    """ Given any url, select appropriate WebsiteParser for it """
    return next(filter(lambda parser: parser.match(url), WEBSITE_PARSERS), None)


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('url')
    args = parser.parse_args()

    parser = select_parser(args.url)
    if not parser:
        print(f'No matching parser found')
    else:
        print(f'Using {parser.__name__}')
        print(parser.parse({'url': args.url}))
