import json
from typing import List, Optional
from operator import itemgetter
from itertools import groupby
from datetime import timedelta
import logging

from packages.utils import with_timer, similar
from mapper.settings import db_engine
from mapper.response import NO_MATCH
from unofficializing.unofficializing import make_unofficial_name as make_uo


logger = logging.getLogger(f'adg.{__name__}')


@with_timer
def load_logs(
    dt=timedelta(days=7),
    distinct: bool = True,
    mode: Optional[str] = None,
    golden_only: bool = False,
    golden_table: str = 'golden_set_domain_merchant',
) -> List[dict]:
    """ Loads logs for selected period of time """
    results = db_engine.execute(
        f"""
        SELECT
            log.id, log.datetime, log.company_name, log.raw_input, log.ticker, log.snapshot, log.user_ip, log.app_key,
            golden_set.company_name AS correct_company_names, golden_set.ticker as correct_ticker,
            golden_set.min_valuable_string AS correct_cleaned
        FROM api_log AS log
        {'INNER' if golden_only else 'LEFT'} JOIN {golden_table} AS golden_set
        ON log.raw_input = golden_set.raw_input
        AND INSTR(golden_set.mode, log.mode) > 0
        WHERE log.datetime >= SUBTIME(NOW(), %(dt)s)
        {'AND log.mode = %(mode)s' if mode else '--'}
        ORDER BY log.raw_input, log.datetime DESC
        """,
        # -- AND golen_set.mode LIKE CONCAT('\%', log.mode, '\%')
        dt=dt,
        mode=mode,
    ).fetchall()
    results = [dict(r) for r in results]

    for result in results:
        if 'correct_company_names' in result:
            result['correct_company_names'] = (result['correct_company_names'] or '').split('|')
            result['correct_company_names'] = [r or NO_MATCH for r in result['correct_company_names']]

            uo_company_name = make_uo(result['company_name'])
            result['company_name_matches'] = any(
                similar(uo_company_name, make_uo(correct_name), thld=0.9)
                for correct_name in result['correct_company_names']
            )

        if 'correct_ticker' in result:
            result['ticker_matches'] = (result['ticker'] or '').lower() == (result['correct_ticker'] or '').lower()

    if distinct:
        results = [list(group)[0] for key, group in groupby(results, key=lambda row: row['raw_input'].lower().strip())]

    results.sort(key=itemgetter('id'))

    # deserialize snapshots
    results_with_snapshots = []
    for result in results:
        try:
            result['snapshot'] = json.loads(result['snapshot'])
            results_with_snapshots.append(result)
        except Exception:
            pass
    else:
        results = results_with_snapshots

    if not results:
        logger.warning('No logs found')

    return results
