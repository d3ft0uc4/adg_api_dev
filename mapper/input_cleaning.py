"""
© 2018 Alternative Data Group. All Rights Reserved.

Raw input string cleaning tools.
"""
import logging
import re
from functools import wraps
from operator import itemgetter
from os import path
from typing import Tuple, Optional, List

from unidecode import unidecode

from cc_cleaner.address_detection import AddressExtractor
from cc_cleaner.clean_cc import CCCleaner
from domain_tools.related_domains import get_related_domains
from keywords.keyword_extraction import STEMMED_DICTIONARY_WORDS, stemmer
from mapper.string_cleaner_tokenization.tokenizer import tokenize, DATA_DIR, DEFAULT_TOKENIZERS, \
    ProductCodeTokenizer, NumberTokenizer, TargetTokenizer, SizeTokenizer, TransactionTokenizer
from packages.utils import NeverFail, pretty, read_list
from ticker_merchant_map.ticker_merchant_map import TickerFinder

# from torch_cleaner.pytorch_string_cleaner import PyTorchStringCleaner
# from keras.models import load_model
# from neural_net.merchant_name.models import MerchantExtractor

logger = logging.getLogger(f'adg.{__name__}')

ROOT_DIR = path.dirname(path.dirname(path.abspath(__file__)))
PAYMENT_PROCESSORS = read_list(path.join(DATA_DIR, 'payment_processors.txt'))
RE_PAYMENT_PROCESSORS = re.compile(r'\b(' + '|'.join(map(re.escape, PAYMENT_PROCESSORS)) + r')\b', flags=re.I)


class Cleaner:
    """
    Cleaner is a base class for raw input cleaning.
    Each cleaner accepts `raw_input` string and returns list of (cleaned_string, cleanup_score) tuples,
    i.e. one cleaner may produce multiple cleaned strings.

    This base cleaner removes unnecessary separators (repeating spaces and tabs).
    """

    def __str__(self):
        return self.__class__.__name__

    def __call__(self, raw_input: str) -> List[Tuple[str, Optional[float]]]:
        value = re.sub(r'[\t ]+', ' ', raw_input)  # remove unnecessary separators

        return [(value, None)]


class MerchantCleaner(Cleaner):
    """
    Cleaner for minimal processing of transaction strings.

    Removes address and removes payment processors.
    """

    @NeverFail(fallback_value=[])
    def __call__(self, raw_input: str) -> List[Tuple[str, Optional[float]]]:
        """ Does some very basic cleaning """
        value = super().__call__(raw_input)[0][0]

        # remove payment processors (paypal, apple pay etc)
        value = RE_PAYMENT_PROCESSORS.sub('', value)

        # remove address
        value = AddressExtractor(value).extract_street()[1]

        return [(value, None)]


class CcCleaner(Cleaner):
    """
    Cleans raw input strings using CCCleaner, and returns clean strings.
    As CCCleaner does not have a score, returns None in the score element of the tuple.
    """

    cc_cleaner = CCCleaner()  # pre-init CCleaner

    @NeverFail(fallback_value=[])
    def __call__(self, raw_input: str) -> List[Tuple[str, Optional[float]]]:
        assert raw_input
        value = super().__call__(raw_input)[0][0]

        # remove payment processors (paypal, apple pay etc)
        value = RE_PAYMENT_PROCESSORS.sub('', value)

        # remove address
        value = AddressExtractor(value).extract_street()[1]

        return [(self.cc_cleaner.clean(value)[0], None)]


# class NnCleaner(Cleaner):
#     start_index_model = load_model(path.join(ROOT_DIR, 'neural_net', 'data', 'raw_cc_start_index_model_2.h5'))
#     end_index_model = load_model(path.join(ROOT_DIR, 'neural_net', 'data', 'raw_cc_end_index_model_2.h5'))
#     # prepare models for multiprocessing (https://github.com/keras-team/keras/issues/6124)
#     start_index_model._make_predict_function()
#     end_index_model._make_predict_function()

#     merchant_extractor = MerchantExtractor(start_index_model, end_index_model)

#     @NeverFail(fallback_value=[])
#     def __call__(self, raw_input: str) -> List[Tuple[str, Optional[float]]]:
#         """
#         Cleans raw input strings using neural network, and returns clean strings with their (normalized) scores.

#         NOTE: nn_str_cleaner returns a start and end index from the original string to form the clean string
#         e.g. "raw input string"[4:12] = "input str". start_index_prob is the confidence that the start index (4) is
#         correct and end_index_prob is the same for the end index (12).
#         """
#         assert raw_input

#         value = super().__call__(raw_input)[0][0]
#         res = self.merchant_extractor.extract_names([value])[0]

#         output = res['merchant_name']
#         score = self.normalize_score((res['start_index'][1] + res['end_index'][1]) / 2) if output else None

#         return [(output, score)]

#     @staticmethod
#     def normalize_score(score: float) -> float:
#         """ Normalize score with  =1.159771*s^2-0.0286*s  | s is the `internal_score`  Cap at 1.0 """
#         return min(1.159771 * score ** 2 - 0.0286 * score, 1.0)


# class TorchCleaner(Cleaner):
#     torch_cleaner = PyTorchStringCleaner(
#         data_dir=path.join(ROOT_DIR, 'torch_cleaner', 'data'),
#         model_dir=path.join(ROOT_DIR, 'torch_cleaner', 'experiments', 'base_model'),
#         restore_file='best',
#         batch_size=32,
#     )

#     @staticmethod
#     def get_score(start_index_conf: float, end_index_conf: float) -> float:
#         return end_index_conf

#     @NeverFail(fallback_value=[])
#     def __call__(self, raw_input: str) -> List[Tuple[str, Optional[float]]]:
#         """ Cleans raw inputs using torch cleaner """

#         result = self.torch_cleaner.clean_string(raw_input)[0]
#         return [(
#             grow_to_words(raw_input, result['beginIndex'], result['endIndex']),
#             self.get_score(result['beginIndexConf'], result['endIndexConf']),
#         )]


def no_dictionary_words(fn):
    """
    Removes all cleaned inputs which are just dictionary words.
    """
    @wraps(fn)
    def wrapper(*args, **kwargs):
        results = fn(*args, **kwargs)
        return [
            (cleaned, score) for cleaned, score in results
            if ' ' in cleaned
            or stemmer.stem(cleaned) not in STEMMED_DICTIONARY_WORDS
            or TickerFinder.query_db(cleaned)
        ]

    return wrapper


class TokensCleaner(Cleaner):
    """
    Transaction string cleaner, which splits input string into "tokens" - substrings that have a special
    meaning (like "TIME", "TRANSACTION", "WEBSITE" etc).

    After raw input string is split into tokens, we select only those tokens that we think are meaningful for
    our task of company detection.
    """
    TOKENIZERS = DEFAULT_TOKENIZERS

    @NeverFail(fallback_value=[])
    @no_dictionary_words
    def __call__(self, raw_input: str) -> List[Tuple[str, Optional[float]]]:
        value = super().__call__(raw_input)[0][0]
        #
        # if is_product(value):
        #     logger.debug(f'Product detected: {value}')
        #     return [(value, None)]

        # some preprocessing
        value = unidecode(value)
        # remove enclosing crap
        value = re.sub(r'^["\'](.*)["\']$', r'\1', value)

        tokens = tokenize(value, self.TOKENIZERS)
        logger.debug(tokens)

        # first try to pick privileged tokens
        selected_tokens = []
        for i, (value, typ) in enumerate(tokens):
            if typ in ['BRAND', 'COMPANY', 'WEBSITE']:
                selected_tokens.append((value, typ))

            elif typ == 'TARGET':
                result = [(value, typ)]
                # compass (<target>) diversified holdings (<unknown>)
                for token in reversed(tokens[:i-1]):
                    if token[1] == 'UNKNONW':
                        result = [token] + result
                    else:
                        break

                for token in tokens[i+1:]:
                    if token[1] == 'UNKNOWN':
                        result.append(token)
                    else:
                        break

                selected_tokens.extend(result)

        # or try to pick all UNKNOWN tokens
        if not selected_tokens:

            for i, (tkn, typ) in enumerate(tokens):
                # pick all UNKNOWN tokens
                if typ == 'UNKNOWN':
                    selected_tokens.append((tkn, typ))

                # plus every TRANSACTION token between UNKNOWN
                elif typ == 'TRANSACTION':
                    if 0 < i < len(tokens) - 1 and tokens[i-1][1] == tokens[i+1][1] == 'UNKNOWN':
                        selected_tokens.append((tkn, typ))

            # if only one token and it's a CITY
            if not selected_tokens and len(tokens) == 1 and tokens[0][1] == 'CITY':
                selected_tokens = tokens

            # if selected one token with one word
            elif len(selected_tokens) == 1 and len(selected_tokens[0][0].split()) == 1:
                selected_tokens = []

                for i, token in enumerate(tokens):
                    if token[0].isnumeric():
                        continue

                    elif token[1] == 'UNKNOWN':
                        selected_tokens.append(token)

                    elif token[1] in ['CITY', 'LONGNUMBER']:
                        # add city/number token to selected tokens only if prev or next token is UNKNOWN
                        if (i != 0 and tokens[i-1][1] == 'UNKNOWN') or \
                                (i != len(tokens)-1 and tokens[i+1][1] == 'UNKNOWN'):
                            selected_tokens.append(token)

        result = ' '.join(map(itemgetter(0), selected_tokens))

        # if no UNKNOWN tokens
        if not result:
            logger.warning(f'Tokenizer left nothing from "{raw_input}"')

        # postprocessing
        result = re.sub(r'(^[\W-]+|[\W-]+$)', '', result)
        result = re.sub(r'[*]+', ' ', result)

        return [(result, None)]


class ProductTokensCleaner(TokensCleaner):
    TOKENIZERS = DEFAULT_TOKENIZERS + [ProductCodeTokenizer(), NumberTokenizer(), SizeTokenizer()]
    TOKENIZERS = [
        tokenizer for tokenizer in TOKENIZERS
        if not isinstance(tokenizer, (TargetTokenizer, TransactionTokenizer))
    ]

    @staticmethod
    def is_unusable(cleaned_value: str) -> bool:
        """ Checks whether cleaned value is not completely crap """
        words = re.split(r'\s', cleaned_value)
        return len(words) > 1 or len(words[0]) >= 3

    @NeverFail(fallback_value=[])
    def __call__(self, raw_input: str) -> List[Tuple[str, Optional[float]]]:
        results = super().__call__(raw_input)
        return [(cleaned, score) for cleaned, score in results if self.is_unusable(cleaned)]


class RelatedDomainsCleaner(Cleaner):
    @NeverFail(fallback_value=[])
    def __call__(self, domain: str) -> List[Tuple[str, Optional[float]]]:
        """ Returns related domains with another TLDs """
        if domain.endswith('.com'):
            logger.debug(f'"{domain}" ends with ".com", skipping related domains check')
            return []

        return [(rel, None) for rel in get_related_domains(domain, tlds=['com', 'net'])]


class NoCleaner(Cleaner):
    pass


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('cleaner')
    parser.add_argument('input')
    args = parser.parse_args()

    print(pretty(
        locals()[args.cleaner]()(args.input)
    ))
