import calendar
import csv
import logging
import re
from argparse import ArgumentParser
from collections import defaultdict
from itertools import chain, combinations, filterfalse
from operator import itemgetter
from os import path
from typing import Optional, List, Tuple

from cc_cleaner.address_detection import AddressExtractor
from mapper.settings import COMPANY_TERMINATORS
from packages.targets import TARGETS
from packages.utils import read_list, prefixize, NeverFail, pretty, partition, ROOT_DIR

# fix for python 3.6
if not hasattr(re, 'Match'):
    re.Match = None


logger = logging.getLogger(f'adg.{__name__}')

# TODO:
# https://github.com/yourcelf/grocktx/blob/master/grocktx/parser.py
# https://github.com/kulashish/merclean


CC_CLEANER_DATA_DIR = path.join(ROOT_DIR, 'cc_cleaner', 'data', 'auxiliary')
DATA_DIR = path.join(path.dirname(path.abspath(__file__)), 'data')

# ---- locations ----
STATE_TO_CITIES = defaultdict(list)
with open(path.join(CC_CLEANER_DATA_DIR, 'cities_states.csv'), encoding='utf-8') as f:
    csv_reader = csv.reader(f, delimiter=',')
    for line in filter(None, csv_reader):
        state = line[1].lower()
        city = line[0].lower()

        if not city or not state or len(city) < 4:
            continue

        STATE_TO_CITIES[state].append(city)

CITIES = list(chain.from_iterable(STATE_TO_CITIES.values()))

DICTIONARY_WORDS = [word.lower() for word in read_list(path.join(DATA_DIR, 'dictionary_words.txt'))] + [
    'guys',
    # TODO: add stemming
]


class Tokenizer:
    UNKNOWN = 'UNKNOWN'

    @classmethod
    def type(cls):
        class_name = cls.__name__
        assert class_name.endswith('Tokenizer'), f'Class name "{class_name}" does not end with "Tokenizer"'
        return cls.__name__[:-len('Tokenizer')].upper()

    @classmethod
    def check_token(cls, i: int, tokens: List[Tuple[str, str]]) -> bool:
        """
        Additional check that token should be searched for regexp.
        If False is returned, then this token is ignored.

        By default, only tokens of type UNKNOWN are parsed.

        Args:
            i - position of the token among other tokens
            tokens - list of all tokens
        """
        return tokens[i][1] == cls.UNKNOWN

    def __call__(self, tokens: List):
        raise NotImplementedError()


def tokens_by_regexp(
    regexp: str,
    typ: str,
    i: int,
    tokens: List[Tuple[str, str]],
    span_getter: callable = lambda match: match.span(),
) -> Optional[List[Tuple[str, 'Token']]]:
    """
    Given any value and regex, return list of tokens.

    Example:
        regexp: r'\bstring\b'
        typ: Token.LOCATION
        i: position of token among all tokens
        tokens: all tokens

    Result:
        [
            ("some ", Token.UNKNOWN),
            ("string", Token.LOCATION),
            (" here", Token.UNKNOWN),
        ]
    """
    # find all occurrences of regexp in value
    value = tokens[i][0]
    matches = re.finditer(regexp, value, flags=re.IGNORECASE) if isinstance(regexp, str) else regexp.finditer(value)

    # build resulting tokens
    ranges = list(filter(None, map(lambda match: span_getter(match, i, tokens), matches)))

    if not ranges:
        return

    tokens = []
    for i, (start, end) in enumerate(ranges):
        if i == 0:
            gap_start = 0
        else:
            gap_start = ranges[i-1][1]

        tokens.append((value[gap_start:start], Tokenizer.UNKNOWN))
        tokens.append((value[start:end], typ))

    tokens.append((value[end:], Tokenizer.UNKNOWN))

    tokens = list(filter(itemgetter(0), tokens))
    return tokens


class RegexpTokenizer(Tokenizer):
    REGEXP = None

    @classmethod
    def get_span(cls, match: re.Match, i: int, tokens: List[Tuple[str, str]]) -> Optional[Tuple[int, int]]:
        """
        Additional check on every regexp match.

        If None is returned, then this match is ignored.
        Otherwise, return range (start:end) - which value to capture. By default the whole match is captured.
        """
        return match.span()

    @NeverFail(fallback_value=None)
    def __call__(self, tokens: List):
        """
        Scans `tokens` list and tries to find strings that match regex across UNKNOWN tokens.
        If it was found, split UNKNOWN token to UNKNOWN and self.TOKEN_TYPE subtokens.
        """
        assert self.REGEXP, 'Regexp undefined'

        for i in range(len(tokens))[::-1]:
            value = tokens[i][0]
            if not self.check_token(i, tokens):
                continue

            # find all occurrences of regexp in value
            subtokens = tokens_by_regexp(self.REGEXP, self.type(), i, tokens, span_getter=self.get_span)
            if subtokens and any(subtoken[1] != Tokenizer.UNKNOWN for subtoken in subtokens):
                logger.debug(f'{self.__class__.__name__} produced subtokens for value "{value}": {subtokens}')
                tokens[i:i+1] = subtokens


class TimeTokenizer(RegexpTokenizer):
    REGEXP = re.compile(r'\s(?:time\s?)?\d{2}:\d{2}(?::\d{2})?(?:\s?[AP]M)?\s', flags=re.IGNORECASE)


class DateTokenizer(RegexpTokenizer):
    MONTHS = list(calendar.month_name)[1:]
    MONTHS += list(map(lambda month: month[:3], MONTHS))

    REGEXP = re.compile('(?:' + '|'.join([
        r'(?:date\W*|on\W*|[^\d])\d{2}[/.-]\d{2}(?:[/.-]\d{2,4})?(?:[^\d]|$)',  # 17/01/01
        r'(?:\s|^)(?:' + '|'.join(MONTHS) + r')\s?\d{2}(?:\s|$)',  # Feb 14
    ]) + ')', flags=re.IGNORECASE)


class NoCharsTokenizer(RegexpTokenizer):
    """
    Scans `tokens` list and tries to find strings that are long and do not contain alphabetical characters.
    """
    WHITELIST = ['&', '+']
    REGEXP = re.compile(r'( |^)[\W\dx]{4,}( |$)', flags=re.IGNORECASE)

    @classmethod
    def get_span(cls, match: re.Match, i: int, tokens: List[Tuple[str, str]]) -> Optional[Tuple[int, int]]:
        if match[0].strip() not in cls.WHITELIST:
            return match.span()


class LongNumberTokenizer(RegexpTokenizer):
    REGEXP = re.compile(r'(?:\s|^)(?:\w*?)[\dx-]{4,}(?:\w*)(?:\s|$)', flags=re.IGNORECASE)


class CrapTokenizer(RegexpTokenizer):
    """
    Removes strings that consis of crappy symbols.
    """
    REGEXP = re.compile(r'(?:\s|^)[!@#$%^*-]+(?:\s|$)')


class R2D2Tokenizer(RegexpTokenizer):
    """
    Scans `tokens` list and tries to find strings that are like "M06BK6M82" or "M83K14RJ0".
    Detect such strings by checking number of "hops" - changes from letter to digit and vice versa.
    """
    NUM_HOPS = 2
    REGEXP = re.compile(r'\b\w+\b', flags=re.IGNORECASE)

    @classmethod
    def get_span(cls, match: re.Match, i: int, tokens: List[Tuple[str, str]]) -> Optional[Tuple[int, int]]:
        # [^\W\d] - only alphabet characters (https://stackoverflow.com/a/1673804/1935381)
        num_hops = len(re.findall(r'(\d[^\W\d]|[^\W\d]\d)', match[0]))
        if num_hops >= cls.NUM_HOPS:
            return match.span()


class HashNumberTokenizer(RegexpTokenizer):
    """
    Scans `tokens` list and tries to find strings that are like "#9184093".
    """
    REGEXP = re.compile(r'\s(#\d{2,})\b')


class Splitter(Tokenizer):
    REGEXP = None

    @NeverFail(fallback_value=None)
    def __call__(self, tokens: List):
        for i in range(len(tokens))[::-1]:
            value, typ = tokens[i]

            if not self.check_token(i, tokens):
                continue

            subvalues = []
            start = 0
            for match in self.REGEXP.finditer(value):
                ranges = match.regs
                if len(ranges) > 1:
                    ranges = ranges[1:]

                rng = next(filter(lambda rng: rng[0] != -1, ranges))
                subvalues.append(value[start:rng[0]])

                start = rng[1]

            if subvalues:
                subvalues.append(value[start:])
                tokens[i:i+1] = [(' '.join(subvalues), typ)]


class WordNumberSplitter(Splitter):
    REGEXP = re.compile('(?=(?:' + '|'.join([
        r'[\dx]{3}()[^\W\dx]{3}',  # letters after digits
        r'(?:\s|^)\d+()[^\W\d]{3}',  # letters after starting digits
        r'[^\W\dx]{3}()[\dx]{3}',  # digits after letters
    ]) + '))', flags=re.IGNORECASE)


class DelimiterSplitter(Splitter):
    REGEXP = re.compile(r'[:_.@/]')


class ApostropheJoiner(Tokenizer):
    @NeverFail(fallback_value=None)
    def __call__(self, tokens: List):
        """
        For each token, replace `'s` suffix with just `s`
        FROM: ('Dillard\'s', UNKNOWN)
        TO:   ('Dillards', UNKNOWN)
        """
        for i in range(len(tokens))[::-1]:
            value, typ = tokens[i]

            if not self.check_token(i, tokens):
                continue

            tokens[i:i+1] = [(re.sub(r'([^\W\d])[\'`]([sS])\b', r'\1\2', value, flags=re.IGNORECASE), typ)]


class StateTokenizer(RegexpTokenizer):
    """
    Scans `self.tokens` list and for each UNKNOWN token tries to find a state.
    If state was found, split UNKNOWN token to UNKNOWN and STATE subtokens.
    """
    cities = '|'.join(map(re.escape, CITIES))
    states = '|'.join(map(re.escape, STATE_TO_CITIES.keys()))

    REGEXP = re.compile('(' + '|'.join([
        rf'\b(?P<city>{cities}) ?(?P<state_after_city>{states})(?:\s?USA?)?\b',  # either city and state
        rf'\b(?P<state>{states})\b',  # or just state (dangerous)
    ]) + ')', flags=re.IGNORECASE)

    @classmethod
    def get_span(cls, match: re.Match, i: int, tokens: List[Tuple[str, str]]) -> Optional[Tuple[int, int]]:
        state = (match['state'] or match['state_after_city']).lower()
        city = (match['city'] or '').lower()

        # if we found state after city, we are very confident it's a correct match
        if city:
            # if city doesnt match the state
            if city not in STATE_TO_CITIES[state]:
                return

            return match.span('state_after_city')

        # otherwise, do some additional checks
        span = match.span('state')
        other_states = [value for value, typ in tokens if typ == 'STATE']
        # allow only at the end of token, and all other states must be the same
        if span[1] == len(match.string):
            if any(other != state for other in other_states):
                logger.debug(f'Tokenizer: State "{state}" conflicts with already found state(s) {other_states}; '
                             f'tokens={tokens}')
                return

            return span


class PhoneTokenizer(RegexpTokenizer):
    REGEXP = re.compile(r'\b(' + '|'.join([
        r'[\d-]+',
        r'800-?\w{7}',
    ]) + r')\b')

    @classmethod
    def get_span(cls, match: re.Match, i: int, tokens: List[Tuple[str, str]]) -> Optional[Tuple[int, int]]:
        # phone either starts with 800 or contains more than 7 digits
        value = match[0]
        if value.startswith('800') or (7 < len(list(filter(str.isdigit, value))) < 11):
            return match.span()


class CompanyTokenizer(RegexpTokenizer):
    REGEXP = re.compile(r'^(.+\b(?:' + '|'.join(map(re.escape, COMPANY_TERMINATORS)) + r'))\b', flags=re.IGNORECASE)


class CardTokenizer(RegexpTokenizer):
    HIDERS = 'x*'
    REGEXP = re.compile(r'[' + ''.join(HIDERS) + r'\d-]{16,}', flags=re.IGNORECASE)

    @classmethod
    def get_span(cls, match: re.Match, i: int, tokens: List[Tuple[str, str]]) -> Optional[Tuple[int, int]]:
        if len(list(filter(lambda ch: ch.isdigit() or ch.lower() in cls.HIDERS, match[0]))) >= 16:
            return match.span()


class AddressTokenizer(Tokenizer):
    @NeverFail(fallback_value=None)
    def __call__(self, tokens: List):
        """
        Scans `self.tokens` list and tries to find address across UNKNOWN tokens.
        If address was found, split UNKNOWN token to UNKNOWN and ADDRESS subtokens.
        """
        for i in range(len(tokens))[::-1]:
            value, typ = tokens[i]

            if not self.check_token(i, tokens):
                continue

            # AddressExctractor always splits string into [<rest>, <address>] parts
            # Important: AddressExtractor can modify original value, for example:
            # TARGET DEBIT CRD ACH TRAN CO ID:XXXXX15170 POS TARGET -1818 RIVERHEAD NY
            address, rest = AddressExtractor(value).extract_street()
            if address:
                tokens[i:i+1] = [
                    (rest, typ),
                    (address, self.type()),
                ]


class WordsTokenizer(Tokenizer):
    WORDS: List[str] = []
    PREFIX_LENGTH = 4

    RE_DELIMITER = re.compile(r'[ /:*\'"_-]+', flags=re.IGNORECASE | re.DOTALL)

    def __init__(self):
        # Sort the words descending by number of words to remove as many words as possible.
        # We want to remove 'aabybro kommun' before 'aabybro'.
        self.WORDS = list(filter(None, map(str.strip, self.WORDS)))
        self.WORDS.sort(key=len, reverse=True)
        self.WORDS_BY_PREFIX = prefixize(self.WORDS, self.PREFIX_LENGTH)

    @classmethod
    def get_span(cls, match: re.Match, i: int, tokens: List[Tuple[str, str]]) -> Optional[Tuple[int, int]]:
        return match.span()

    @NeverFail(fallback_value=None)
    def __call__(self, tokens: List):
        for i in range(len(tokens))[::-1]:
            value, typ = tokens[i]

            if not self.check_token(i, tokens):
                continue

            words = self.RE_DELIMITER.split(value)

            # select candidates that start with same prefix as value's words' prefixes
            candidates = []
            for prefix in set(map(lambda word: word[:self.PREFIX_LENGTH].lower(), words)):
                candidates.extend(self.WORDS_BY_PREFIX[prefix])
            candidates = list(set(candidates))
            candidates.sort(key=len, reverse=True)

            if not candidates:
                continue

            # find all occurrences of candidates in value
            regexp = r'\b(' + '|'.join(map(re.escape, candidates)) + r')\b'
            subtokens = tokens_by_regexp(regexp, self.type(), i, tokens, span_getter=self.get_span)
            if subtokens and any(subtoken[1] != Tokenizer.UNKNOWN for subtoken in subtokens):
                logger.debug(f'{self.__class__.__name__} produced subtokens for value "{value}": {subtokens}')
                tokens[i:i+1] = subtokens


class CityTokenizer(WordsTokenizer):
    WORDS = CITIES

    @classmethod
    def get_span(cls, match: re.Match, i: int, tokens: List[Tuple[str, str]]) -> Optional[Tuple[int, int]]:
        # allow city only at the end of token
        city = match[0].lower()
        if match.end() != len(match.string):
            return

        # dismiss if dictionary word
        if city in DICTIONARY_WORDS:
            logger.debug(f'City "{city}" is a dictionary word, discarding')
            return

        # other cities must be the same
        other_cities = [value for value, typ in tokens if typ == 'CITY']
        if any(other != city for other in other_cities):
            logger.debug(f'Tokenizer: City "{city}" conflicts with already found cities {other_cities}; '
                         f'tokens={tokens}')
            return

        # city and state must match
        state = ([value for value, typ in tokens if typ == 'STATE'] or [None])[0]
        if state and city not in STATE_TO_CITIES[state.lower()]:
            logger.debug(f'City "{city}" does not belong to already found state "{state}"')
            return

        return match.span()


class LocationTokenizer(WordsTokenizer):
    LOCATIONS = ['united states of america', 'usa']  # read_list(path.join(CC_CLEANER_DATA_DIR, 'locations.txt'))
    # exclude known cities
    WORDS = set(LOCATIONS) - set(CITIES)

    @classmethod
    def get_span(cls, match: re.Match, i: int, tokens: List[Tuple[str, str]]) -> Optional[Tuple[int, int]]:
        # allow location only at the end of token
        if match.end() == len(match.string):
            return match.span()


def get_abbreviations(word: str) -> List[str]:
    """ Returns all possible abbreviations of any word """
    word = re.sub(r'[ -]', '', word)

    CONSONANT = 'qrtpsdfghjklzxcvbnm'
    # VOWEL = 'weyuioa'

    selections = []
    length = len(word)
    for n_select in range(3, length):
        selections.extend(combinations(range(length), n_select))

    for i in range(len(selections))[::-1]:
        selection = selections[i]
        if selection[0] != 0:
            del selections[i]
            continue

        diffs = map(lambda i: selection[i+1] - selection[i] - 1, range(len(selection) - 1))
        if any(d > 2 for d in diffs):
            del selections[i]
            continue

    # first letter is a must
    selections = [sel for sel in selections if sel[0] == 0]
    if not selections:
        return []

    abbreviations = []

    for selection in selections:
        abbr = ''.join(word[i] for i in selection)

        # don't allow these letters other than in first place
        if any(letter in abbr[1:] for letter in ['a', 'o', 'e', 'i']):
            continue

        vowels, consonants = partition(lambda ch: ch in CONSONANT, abbr)
        if len(consonants) <= len(vowels):
            continue

        abbreviations.append(abbr)

    # some obvious non-abbreviations
    BAD_ABBREVIATIONS = re.compile('(' + '|'.join([
        r'\b\w\b',
        r'[weyuioa]{2,}',
        r'[ -]+',
    ]) + ')', flags=re.IGNORECASE)
    abbreviations = list(filterfalse(BAD_ABBREVIATIONS.search, abbreviations))

    return abbreviations


class TransactionTokenizer(RegexpTokenizer):
    TRANSACTION_WORDS = [
        word.lower() for word in
        read_list(path.join(DATA_DIR, 'transaction_words.txt')) +
        read_list(path.join(DATA_DIR, 'payment_processors.txt'))
    ]
    # now extend words by adding all possible abbreviations
    ABBREVIATIONS = list(
        set(chain.from_iterable(map(get_abbreviations, TRANSACTION_WORDS))) - set(DICTIONARY_WORDS)
    )
    WORDS = TRANSACTION_WORDS + ABBREVIATIONS
    WORDS = sorted(list(set(WORDS)))

    REGEXP = re.compile(
        r'(^|\s)'
        r'((' + '|'.join(map(re.escape, WORDS)) + r')[-]?)+'
        r'($|\s)',
        flags=re.IGNORECASE
    )


class TargetTokenizer(RegexpTokenizer):
    TARGETS = sorted(
        list(
            set([target.lower() for target in TARGETS.contents.values()])
            - set(DICTIONARY_WORDS)
            - set(TransactionTokenizer.TRANSACTION_WORDS)
        ),
        key=len,
        reverse=True,
    )
    REGEXP = re.compile(r'(?:^|\s)(' + '|'.join(TARGETS) + r')(?:$|\s)', flags=re.IGNORECASE)


class WebsiteTokenizer(RegexpTokenizer):
    TLDS = sorted(
        read_list(path.join(ROOT_DIR, 'domain_tools', 'data', 'tlds-alpha-by-domain.txt')),
        key=len,
        reverse=True,
    )
    REGEXP = re.compile(r'\b[\w.]{3,}\.(?:' + '|'.join(map(re.escape, TLDS)) + ')', flags=re.IGNORECASE)


class MoneyTokenizer(RegexpTokenizer):
    CURRENCIES = ['$', 'USD']

    RE_CURRENCY = '(?:' + '|'.join(map(re.escape, CURRENCIES)) + ')'
    REGEXP = re.compile(
        rf'(?:{RE_CURRENCY} *)?'  # currency: "USD"
        r'\d{1,6}'  # main amount: "19"
        r'(?:[., ]\d{2})?'  # secondary amount: ".99"
        rf'(?: *{RE_CURRENCY})?'  # currency again: "USD"
        r'(?: */ ?(?:year|yr|y|month|mon|mo|m|day|d))?',  # frequency: "/ day"
        flags=re.IGNORECASE
    )

    @classmethod
    def get_span(cls, match: re.Match, i: int, tokens: List[Tuple[str, str]]) -> Optional[Tuple[int, int]]:
        # money amount contains USD/$ anywhere (regex doesnt check it)
        if any(sym in match[0] for sym in cls.CURRENCIES):
            return match.span()


class IDTokenizer(RegexpTokenizer):
    # TODO: ID software?
    REGEXP = re.compile(r'\bID:?\s*\w+(?:\s|$)', flags=re.IGNORECASE)


class BracketsTokenizer(RegexpTokenizer):
    REGEXP = re.compile(r'\(.*?(?:\)|$)')


class MeasureTokenizer(RegexpTokenizer):
    UNITS = [
        'oz', 'fl oz', 'lbs', 'gallon',
        'ct', 'count',
        'pk', 'pc', 'pack',
        'mg', 'g', 'kg',
        'l',
        'rolls',
    ]
    REGEXP = re.compile(r'(\s|^)\d+([./]\d+)?[ -]?(' + '|'.join(UNITS) + r')(\s|$)', flags=re.IGNORECASE)


class SizeTokenizer(RegexpTokenizer):
    REGEXP = re.compile(r'\b(small|medium|large|XS|S|M|L|XL|XXL|XXXL)\b', flags=re.IGNORECASE)


class ProductCodeTokenizer(R2D2Tokenizer):
    NUM_HOPS = 1

    @classmethod
    def get_span(cls, match: re.Match, i: int, tokens: List[Tuple[str, str]]) -> Optional[Tuple[int, int]]:
        if not any(token[1] == 'MEASURE' for token in tokens):
            # if there is a measure, we are sure it's a product
            # TODO: invent a better way
            return

        return super().get_span(match, i, tokens)


class NumberTokenizer(RegexpTokenizer):
    REGEXP = re.compile(r'(^|\s)[^\s]*\d+[^\s]*($|\s)', flags=re.IGNORECASE)


class BrandTokenizer(RegexpTokenizer):
    REGEXP = re.compile(r'(.*)[®™©]', flags=re.IGNORECASE)


# order matters!
DEFAULT_TOKENIZERS = [
    # before splitting by delimiter
    # TODO: ugly
    TimeTokenizer(),
    DateTokenizer(),
    IDTokenizer(),
    WebsiteTokenizer(),

    # preprocessing
    ApostropheJoiner(),
    WordNumberSplitter(),
    DelimiterSplitter(),

    # tokenizing
    TargetTokenizer(),
    MoneyTokenizer(),
    MeasureTokenizer(),
    CardTokenizer(),
    PhoneTokenizer(),
    AddressTokenizer(),
    StateTokenizer(),
    CityTokenizer(),
    LocationTokenizer(),

    # removing crap
    TransactionTokenizer(),
    R2D2Tokenizer(),
    LongNumberTokenizer(),
    HashNumberTokenizer(),
    NoCharsTokenizer(),
    BracketsTokenizer(),
    CrapTokenizer(),

    # these tokenizers are "greedy", thus they should be run after all other tokenizers
    CompanyTokenizer(),
    BrandTokenizer(),
]


@NeverFail(fallback_value=lambda raw_input: [(raw_input, Tokenizer.UNKNOWN)])
def tokenize(raw_input: str, tokenizers: List[Tokenizer] = DEFAULT_TOKENIZERS) -> List[Tuple[str, 'Token']]:
    """
    Splits original input string into "tokens" - tuples of (string, token_type).

    IN:  'Air Jordans purchase new york'
    OUT: [('Air Jordans', 'UNKNOWN'), ('purchase', 'TRANSACTION'), ('new york', 'CITY')]
    """
    assert raw_input

    tokens = [(raw_input.strip(), Tokenizer.UNKNOWN)]

    for _ in range(2):
        for tokenizer in tokenizers:
            tokenizer(tokens)
            # logger.debug(f'Results from {tokenizer.__class__.__name__}: {pretty(tokens)}')
            tokens = [(token[0].strip(), token[1]) for token in tokens if token[0]]

    return tokens


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('value')
    args = parser.parse_args()

    print(pretty(tokenize(args.value)))
