import json
import logging
import re
from collections import Counter
from datetime import datetime
from itertools import product, chain
from operator import itemgetter, attrgetter
from typing import List, Tuple, Optional

import networkx as nx

from mapper.entity import Entity
from packages.targets import Targets, TARGETS
from packages.utils import remove_dupes, remove_value, with_timer, NeverFail, StrFallbackJSONEncoder, \
    LOCAL, similar, encrypt, is_abbrev, closeness
from ticker_merchant_map.ticker_merchant_map import TickerInfo, EXCHANGES
from unofficializing.unofficializing import make_unofficial_name as uo
from .settings import DB_OUTPUT, LOG_TABLE, CONF_LEVEL_MEDIUM, CONF_LEVEL_HIGH, db_engine, INTERNAL_APP_KEYS

logger = logging.getLogger(f'adg.{__name__}')


def get_app_key(request: dict) -> Optional[str]:
    return {
        k.lower(): v for k, v in request.get('args', {}).items()
    }.get('x_user_key')


def get_user_ip(request: dict) -> Optional[str]:
    return request.get('headers', {}).get('X-Forwarded-For', '').split(',')[0]


@with_timer
def to_db_rowlog(res: dict, response: dict):
    """ Saves results to log table in the db, as well as proxied user info if available. """
    # Disable pooling using NullPool. This is the most simplistic, one shot system that prevents the Engine from
    # using any connection more than once. From
    # docs.sqlalchemy.org/en/rel_1_0/faq/connections.html#how-do-i-use-engines-connections-sessions-with-python-multiprocessing-or-os-fork
    # engine = sqlalchemy.create_engine(SQL_CONNECTION_STRING, poolclass=NullPool)
    db_engine.execute(
        f"""
        INSERT INTO {LOG_TABLE} (
            uuid, raw_input, mode, company_name, ticker, snapshot, user_ip, app_key, execution_time
        )
        VALUES (
            %(uuid)s, %(raw_input)s, %(mode)s, %(company_name)s, %(ticker)s, %(snapshot)s, %(user_ip)s, %(app_key)s,
            %(execution_time)s
        )
        """,
        uuid=res['run_uid'],
        raw_input=response['Original Input'],
        mode=res['mode'],
        company_name=response['Company Name'],
        ticker=response['Ticker'],
        snapshot=json.dumps(res, cls=StrFallbackJSONEncoder),
        user_ip=get_user_ip(res['request']),
        app_key=get_app_key(res['request']),
        execution_time=res['exec_time'].total_seconds() if 'exec_time' in res else None,
    )

    logger.debug(f'Wrote results to db table "api_db_log"')


DOMAIN_UNREACHABLE = 'Domain Unreachable'
NO_MATCH = 'No Match / Small Business'


def set_winner_prob(entities: List[Entity]):
    """ For each entity, set probability that this entity is a good winner """
    for entity in entities:
        entity.winner_prob = 0

        if not entity.get('score'):
            continue

        # penalize SmallParsers ER
        if entity.source.startswith('bing['):
            entity.winner_prob -= 0.25

        # it has ticker
        if entity.get('ticker') and entity.get('exchange'):
            entity.winner_prob += 0.75

        # it has no owners
        if not hasattr(entity, 'owner'):
            entity.winner_prob += 0.25

        # it's wiki page (sometimes entity from wikipedia ER doesn't have wiki page, thus checking 'source')
        # or bloomberg
        if hasattr(entity, 'wiki_page') or entity.get('source') in ['wikipedia', 'bloomberg']:
            entity.winner_prob += 0.5

        # it is someone's owner
        if any(other_entity.get('owner') == entity for other_entity in entities):
            entity.winner_prob += 0.5

        # it is a target
        if hasattr(entity, 'target'):
            entity.winner_prob += 0.25

        entity.winner_prob += 0.0542 * entity.occurs ** 3.162


def get_final_source(entity: Entity) -> str:
    """
    Returns real source of Entity, even if it's a 2nd-gen entity.

    Example: source == bane[fwfm] -> final source == fwfm
    """
    return re.sub(r'(.+\[|\].*)', '', entity['source'])


def pick_winner(graph: nx.DiGraph, cleaned_input: Optional[str] = None) -> Optional[Entity]:
    """
    Given winning group, select a winner Entity

    Args:
        graph: winning graph
        cleaned_input: cleaned input which will be used to pick correct winner entity

    Returns:
        Winning entity (if exists) or None.
    """
    entities = graph.entities

    # select only entities which are companies (or all entities if no companies)
    candidates = [entity for entity in entities if entity.get('kind', 'company') == 'company'] or entities
    if not candidates:
        logger.warning(f'Winning graph does not contain any appropriate candidates -> no winner')
        return

    # select all possible values, but if value came from some ER twice - count it as one occurrence
    values = [
        value for value, source in
        remove_dupes((uo(entity['value']), get_final_source(entity)) for entity in candidates)
    ]

    # add `occurs` attr to each entity, which means how much that value happened in group
    counter = Counter(values)
    for entity in candidates:
        entity._value_uo = uo(entity.value)
        entity.occurs = counter[entity._value_uo]
        entity.closeness_to_input = closeness(entity._value_uo, cleaned_input or '')

    # calculate probability to be a good winner for each entity
    set_winner_prob(candidates)

    # pick entity with biggest probability
    cleaned_input_uo = uo(cleaned_input)
    winner = sorted(
        candidates,
        key=lambda entity: (
            entity.closeness_to_input > 0 or is_abbrev(entity._value_uo, cleaned_input_uo),
            entity.winner_prob,
        ),
        reverse=True,
    )[0]

    if LOCAL.settings.get('PICK_WINNER_FROM_CHILDREN', False):
        # now check whether we picked a parent entity, and return its child
        # (if it has score, i.e. was a result of some ER tool)
        children_nodes = [
            graph.nodes[src]['entity']
            for src, dst, data in graph.edges(data=True)
            if dst == winner.id and data['relation'] == 'parent'
            and graph.nodes[src]['entity'].score
        ]

        if len(children_nodes) > 1:
            logger.debug(f'Winner picking: winning entity {winner} has multiple children, choice is ambiguous: '
                         f'{children_nodes}')
        elif children_nodes:
            winner = children_nodes[0]

    return winner


@NeverFail(fallback_value=(None, None))
def get_confidence(score: float) -> Tuple[float, str]:
    """ Convert raw group score to human-readable tuple (digit value, description) """
    if score < CONF_LEVEL_MEDIUM:
        level = 'Low'
    elif score < CONF_LEVEL_HIGH:
        level = 'Medium'
    else:
        level = 'High'

    score = max(score, 0)
    score = min(score, 1)
    score = round(score, 2)

    return score, level


def get_final_parent(graph: nx.DiGraph) -> Optional[Entity]:
    """
    Given a graph of entities, tries to find a final parent node
    (i.e. node with "parent" relation in and no "parent" relations out)
    """
    for entity in [
        node['entity'] for node in graph.nodes.values()
        if node['entity'].get('kind', 'company') == 'company'
    ]:
        # if entity is first parent and has no parents itself
        if any(
            data['relation'] == 'parent' and data.get('priority', 0) == 0
            for _, _, data
            in graph.in_edges(entity.id, data=True)
        ) and not any(
            data['relation'] == 'parent'
            for _, _, data
            in graph.out_edges(entity.id, data=True)
        ):
            return entity


@with_timer
def generate_response(res: dict, fields: List[str] = []) -> dict:
    """ Generate API response from internal results; log results to DB """
    response = {
        'Original Input': res['original_input'],
        'Company Name': None,
        'Confidence': None,
        'Confidence Level': None,
        'Aliases': [],
        'Alternative Company Matches': [],
        'Related Entities': [],
        'Ticker': None,
        'Exchange': None,
        'Majority Owner': None,
        'FIGI': None,
        'Websites': [],
        'Possible Names': [],
        'Companies Retrieved': [],
        '_DEBUG': {},
    }

    # # quick fix - client should never receive non-company node
    # for attempt in res.get('attempts', []):
    #     for group in attempt.get('winners', []) + attempt.get('losers', []):
    #         to_delete = [entity for entity in group['graph'].entities if entity.get('kind', 'company') != 'company']
    #         for entity in to_delete:
    #             group['graph'].remove_node(entity.id)

    # populate response

    winning_groups = []
    losing_groups = []
    cleaned = None  # cleaned value from winning attempt
    for attempt in res.get('attempts', []):
        winning_groups = attempt.get('winners', [])
        # pick only groups which have at least one company
        winning_groups = [
            group for group in winning_groups
            if any(entity.kind == 'company' for entity in group['graph'].entities)
        ]
        if winning_groups:
            cleaned = attempt['cleaned']
            losing_groups = attempt.get('losers', [])
            break

    # winning_groups = next(filter(None, map(lambda attempt: attempt.get('winners', []), res['attempts'])), [])
    winner = pick_winner(winning_groups[-1]['graph'], cleaned_input=cleaned) if winning_groups else None
    wiki_entities = [entity for entity in winning_groups[-1]['graph'].entities if hasattr(entity, 'wiki_page')] \
        if winning_groups else []

    if not winner:
        res['winner'] = None
        response['Company Name'] = NO_MATCH

    elif len(wiki_entities) == 1 and wiki_entities[0].wiki_page.is_stale():
        logger.debug(f'Company "{winner.value}" is stale -> no winner')
        res['winner'] = None
        response['Company Name'] = NO_MATCH

    else:
        # ---- Company Name ----
        res['winner'] = winner
        response['Company Name'] = winner.value

        # split winning groups
        alternative_groups = winning_groups[:-1]
        winner_group = winning_groups[-1]
        winner_graph = winner_group['graph']

        # ---- Confidence ----
        response['Confidence'], response['Confidence Level'] = get_confidence(winner_group['score'])

        # ---- Aliases ----
        # copy winning graph and delete all edges that are not of type "alias" or "similar"
        alias_graph = winner_graph.copy()
        for node1, node2, data in winner_graph.edges(data=True):
            if data['relation'] not in ['alias', 'similar']:
                alias_graph.remove_edge(node1, node2)

        if winner.id not in alias_graph:
            logger.warning(f'Winner {winner} not in alias graph {alias_graph.entities}')
            aliases = []

        else:
            aliases = [
                alias_graph.nodes[node]['entity']
                for node in nx.algorithms.dag.descendants(alias_graph, winner.id)
            ]
            aliases = [entity for entity in aliases if entity.get('kind', 'company') == 'company']

        # pick aliases from alias entities
        alias_values = list(map(attrgetter('value'), aliases))
        # also add target aliases
        for entity in filter(lambda entity: hasattr(entity, 'target'), aliases):
            alias_values.extend(TARGETS.infos[entity.target]['entity_names'])
        # remove all possible duplicates
        alias_values = remove_value(alias_values, winner.value)
        alias_values = remove_dupes(alias_values)

        res['aliases'] = alias_values
        response['Aliases'] = alias_values

        # ---- Alternatives ----
        alternatives = []
        for group in alternative_groups:
            alternatives.extend(map(itemgetter('entity'), group['graph'].nodes.values()))

        alternatives = [entity for entity in alternatives if entity.get('kind', 'company') == 'company']

        alternative_values = list(map(attrgetter('value'), alternatives))
        alternative_values = remove_dupes(alternative_values)
        res['alternatives'] = alternative_values
        response['Alternative Company Matches'] = alternative_values

        # ---- Ultimate owner & ticker ----
        entities = [
            node['entity'] for node in winner_graph.nodes.values()
            if node['entity'].get('kind', 'company') == 'company'
        ]

        ticker = None
        exchange = None
        ticker_debug = {}
        ticker_entity = None
        final_parent = get_final_parent(winner_graph)

        # if any entity contains ticker - pick it
        ticker_entities = [entity for entity in entities if entity.get('ticker')]

        ticker_exchange_counter = Counter([(entity['ticker'], entity['exchange']) for entity in ticker_entities])
        ticker_counter = Counter([entity['ticker'] for entity in ticker_entities])
        ticker_entities.sort(
            key=lambda entity: (
                -ticker_exchange_counter[(entity.ticker, entity.exchange)],  # prefer most frequent ticker & exchange
                -ticker_counter[entity.ticker],  # prefer most frequent ticker
                hasattr(entity, 'target'),  # prefer non-db tickers
                not hasattr(entity, 'wiki_page'),  # prefer wiki tickers
                EXCHANGES.index(entity.get('exchange')) if entity.get('exchange') in EXCHANGES else len(EXCHANGES)
            )
        )
        if ticker_entities:
            ticker_entity = ticker_entities[0]
            ticker, exchange, ticker_debug = ticker_entity.get_ticker()

        elif not [entity for entity in entities if entity['source'] == 'wikipedia']:
            # if we don't have wikipedia nodes - use external ticker search

            # select best candidate for ticker retrieval
            ticker_candidate = final_parent or winner

            known_entities = list(chain.from_iterable(
                map(attrgetter('entities'), [group['graph'] for group in losing_groups + winning_groups])
            ))
            ticker, exchange, ticker_debug = ticker_candidate.get_ticker(known_entities=known_entities)
            if ticker:
                ticker_entity = ticker_candidate

        if ticker and exchange:
            # perform additional ticker check
            ticker_names = set(filter(
                None,
                TickerInfo.get_info_from_db(ticker, exchange).get('names_uo', []) + [
                    uo(TickerInfo.get_info_from_tradier(ticker, exchange).get('name', '')),
                    uo(TickerInfo.get_info_from_yahoo(ticker, exchange).get('name', '')),
                ]
            ))

            if not any(ticker_names):
                logger.warning(f'No info found for ticker {exchange}:{ticker} -> '
                               f'ticker not verified for "{winner.value}"')

            all_known_names = set(list(chain.from_iterable(
                data['entity']['comparison']['names'] for data in winner_graph.nodes.values()
            )))
            # add unofficialized versions of all known names
            all_known_names = set(list(all_known_names) + list(map(uo, all_known_names)))

            if any(ticker_names) and not any(similar(*pair) for pair in product(ticker_names, all_known_names)):
                logger.warning(
                    f'Company names "{ticker_names}" not found in graph nodes {all_known_names} -> '
                    f'discarding ticker "{ticker}"'
                )
                ticker, exchange, ticker_debug = None, None, {}
                ticker_entity = None

        if ticker and exchange:
            response['Ticker'] = ticker
            response['Exchange'] = exchange

            # ADS:FWB (Adidas) != ADS:NYSE (Alliance Data Systems Corporation)
            figi = TickerInfo.get_info_from_figi(ticker, exchange, names=[winner.value] + alias_values)
            # name may be empty (figi may return None for it) -> use ticker_entity value then
            response['Majority Owner'] = figi.get('name') or ticker_entity.value
            response['FIGI'] = figi.get('figi')

            res['ticker'] = ticker, exchange, ticker_debug

        elif final_parent and final_parent != winner:
            response['Majority Owner'] = final_parent.value

        # ---- Related ----
        related = [
            entity for entity in entities
            if entity != winner
            and entity not in aliases
            and entity.get('kind', 'company') == 'company'
        ]
        related_values = []

        # related from winner graph
        related_values += [{
            'name': entity.value,
            'score': 1.0,
        } for entity in related]

        # related from targets database
        for entity in filter(lambda entity: hasattr(entity, 'target'), entities):
            related_values += Targets.get_related_entities(entity.value)

        # # related from wikipedia subsidiaries
        # wiki_pages = map(attrgetter('wiki_page'), filter(lambda entity: hasattr(entity, 'wiki_page'), entities))
        # wiki_pages = remove_dupes(wiki_pages, key=attrgetter('url'))

        # for page in wiki_pages:
        #     related_values.extend({
        #         'name': name,
        #         'score': WIKI_SUBSIDIARIES_CLOSENESS_SCORE,
        #     } for name, _ in page.infobox.get('Subsidiaries', []))

        related_values = remove_dupes(related_values, key=itemgetter('name'))
        if response['Majority Owner']:
            related_values = remove_value(related_values, response['Majority Owner'], key=itemgetter('name'))
        related_values.sort(key=itemgetter('score'), reverse=True)

        res['related'] = related_values
        response['Related Entities'] = [{
            'Name': rel['name'],
            # 'Closeness Score': round(rel['score'], 2),
        } for rel in related_values]

        possible_names = []
        for attempt in res.get('attempts', []):
            for group in (attempt.get('losers', []) + attempt.get('winners', []))[::-1]:
                for entity in group['graph'].entities:
                    if entity.get('kind', 'company') == 'company':
                        possible_names.append(entity.value)
        possible_names = remove_dupes(possible_names)
        response['Possible Names'] = possible_names

        # ---- Graph ----
        groups = [{
            'keywords': [entity.value for entity in group['graph'].entities if entity.kind == 'keyword'],
            'companies': encrypt(group['graph'].serialize(for_client=True)),
            'confidence': group['score'],
        } for group in attempt.get('losers', []) + attempt.get('winners', [])]

        # remove empty groups
        groups = [group for group in groups if group['companies']]

        # remove empty keywords
        for group in groups:
            if not group['keywords']:
                del group['keywords']

        response['Companies Retrieved'] = groups

        # ---- Websites ----
        websites = list(set(chain.from_iterable(entity['comparison']['websites'] for entity in winner_graph.entities)))
        websites.sort(key=lambda website: (website.count('.'), len(website)))
        response['Websites'] = websites

    # measure execution time
    if 'start_time' in res:
        res['end_time'] = datetime.now()
        res['exec_time'] = res['end_time'] - res['start_time']

    # add request information to debug output
    res['request'] = getattr(LOCAL, 'request', {})
    res['run_uid'] = getattr(LOCAL, 'run_uid', None)

    # append debug output to response
    response['_DEBUG'] = res

    # log internal result to db
    if DB_OUTPUT:
        # if PARALLEL:
        #     # crashes with OSError: [Errno 12] Cannot allocate memory
        #     Process(
        #         target=to_db_rowlog,
        #         args=(res, response)
        #     ).start()
        # else:
        #     to_db_rowlog(res, response)
        to_db_rowlog(res, response)

    # log client inputs to slack
    app_key = get_app_key(res['request'])
    if app_key and app_key not in INTERNAL_APP_KEYS:
        logger.info('\n'.join([
            f'API key:        {app_key}',
            f'Raw input:      {response["Original Input"]}',
            f'Company name:   {response["Company Name"]}',
            f'Majority owner: {response["Majority Owner"]}',
            f'Ticker:         {response["Exchange"]}:{response["Ticker"]}',
        ]))

    return {key: value for key, value in response.items() if key in fields}
