import logging
from typing import Tuple, Optional, Iterable

from packages.targets import TARGETS
from packages.utils import uid
from ticker_merchant_map.ticker_merchant_map import TickerFinder, normalize_tkr_exch, InvalidTickerExchange

logger = logging.getLogger(f'adg.{__name__}')


class Entity:
    """ A class that represents some company / candidate """

    def __init__(self, value: str, score: Optional[float] = None, kind: str = 'company', **kwargs):
        self.id = uid()
        self.value = value
        self.score = score
        self.kind = kind

        for k, v in kwargs.items():
            setattr(self, k, v)

    def __repr__(self):
        vals = {
            k: (v.id if isinstance(v, Entity) else v)
            for k, v in self.__dict__.items()
            if not k.startswith("_") and k != 'id'
        }
        return f'<Entity {self.id}: {vals}>'

    def setdefault(self, key, default):
        if not hasattr(self, key):
            setattr(self, key, default)
        return getattr(self, key)

    def __getitem__(self, key):
        return getattr(self, key)

    def __hasitem__(self, key):
        return hasattr(self, key)

    def __setitem__(self, key, value):
        return setattr(self, key, value)

    def __delitem__(self, key):
        return delattr(self, key)

    def get(self, key, default=None):
        return getattr(self, key, default)

    def get_ticker(self, known_entities: Iterable['Entity'] = tuple()) -> Tuple[Optional[str], Optional[str], dict]:
        """
        Tries to retrieve ticker for given entity

        Args:
            known_entities: list of entities which we already retrieved, which allows to block them if they appear
                during ticker retrieval

        Returns:
            (ticker, exchange, debug_info)
        """

        # 1) check whether node already contains ticker

        # 1.1) check 'ticker' field
        try:
            ticker, exchange = normalize_tkr_exch(self.get('ticker'), self.get('exchange'))
            if ticker and exchange:
                return ticker, exchange, {'source': self}
        except InvalidTickerExchange as exc:
            logger.warning(f'Entity.get_ticker: {exc}')

        # 1.1) check wiki page
        wiki_page = self.get('wiki_page')
        if wiki_page:
            # if wiki page does not contain ticker, it's very likely that company has no ticker at all
            # ^--- not true, "sears holdings" don't have "SHLD" ticker on wiki
            try:
                ticker, exchange = normalize_tkr_exch(*wiki_page.ticker)
                if ticker and exchange:
                    return ticker, exchange, {'source': wiki_page}
            except InvalidTickerExchange as exc:
                logger.warning(f'Entity.get_ticker[wiki_page]: {exc}')

        # 1.2) check targets database
        target = self.get('target')
        if target:
            data = TARGETS.infos[target]
            try:
                ticker, exchange = normalize_tkr_exch(data['ticker'], data['exchange'])
                if ticker and exchange:
                    return ticker, exchange, {'source': 'target', 'data': data}
            except InvalidTickerExchange as exc:
                logger.warning(f'Entity.get_ticker[target]: {exc}')

        # TODO: 1.3) if `company_website` ER wins, we could use anything from `res`;
        # don't forget to increase `company_website` probability then

        # 2) use external sources for finding ticker
        known_tickers = [(e['ticker'], e['exchange']) for e in known_entities if e.get('ticker') and e.get('exchange')]
        result = TickerFinder.get_ticker(self.value, known_tickers=known_tickers)
        ticker = result.get('ticker')
        exchange = result.get('exchange')
        if ticker and exchange:
            return ticker, exchange, {'source': 'TickerFinder', 'data': result}

        return None, None, {}
