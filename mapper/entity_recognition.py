"""
© 2018 Alternative Data Group. All Rights Reserved.

Entity recognition tools.

Each tool is just a wrapper that converts raw input string to graph where nodes are entities.

CLI:
    python -m mapper.entity_recognition [bing|bane|...] "VCNA PRESTIGE CONCRETE PRODUCTS INC"
"""
import csv
import json
import logging
import math
import re
from collections import defaultdict, Counter
# from difflib import SequenceMatcher
from functools import wraps
from itertools import product, chain, combinations, filterfalse
from operator import itemgetter
from os import path
from typing import Union, Optional, List, Tuple, Iterable

import networkx as nx
import requests
from networkx.algorithms.components import weakly_connected_components

from bane.bane import Bane
from bane.organization_entity_check import is_organization
from bing.bing_web_core import BingWebCore
from domain_tools.clean import get_domain
from domain_tools.company_page_detection import get_website, NOT_COMPANY_WEBSITES, NOT_PRODUCT_WEBSITES, in_blacklist
from domain_tools.copyright_extraction import get_copyright
from domain_tools.ssl import SSLCertificateRetrieval
from domain_tools.title import get_title
from domain_tools.utils import get
from domain_tools.whois import WhoisInfoRetrieval
from find_winner.fw_fm import FWFM
from keywords.keyword_extraction import ngrams
from mapper.settings import OPENFIGI_API_KEY, COMPANY_TERMINATORS
from packages.caching import DbCache, ERToolCache
from packages.targets import Targets, TARGETS
from packages.utils import NeverFail, similar, Timer, pretty, remove_dupes, remove_value, \
    LocalsThreadPoolExecutor, conditional_decorator, LOCAL, ROOT_DIR, read_list
from packages.utils import prefixize, tokenize, common_words
from packages.xgboost.prediction import xgb_load_model, xgb_predict
from tfidf.tfidf import extract_keywords as tfidf_extract_keywords
from ticker_merchant_map.ticker_merchant_map import normalize_tkr_exch, TickerInfo, EXCHANGES, \
    BLOOMBERG_EXCHANGES_MAP, InvalidTickerExchange, TickerFinder
from unofficializing.unofficializing import make_unofficial_name as uo
from wiki_finder.wiki_finder2 import WikiPage
from .entity import Entity
from .response import pick_winner
from .website_parsers import select_parser

DATA = path.join(path.dirname(path.abspath(__file__)), 'data')

logger = logging.getLogger(f'adg.{__name__}')
DEBUG = __name__ == '__main__'


# TODO: Extract logo from webpage


def is_cache_enabled(*args, **kwargs) -> bool:
    return LOCAL.settings.get('CACHE_ERS', True)


class CheckEntities:
    """ Decorator to check whether entities are valid (i.e. value not empty, length is not too big etc) """
    RETAILERS = [
        domain.split('.')[0]
        for domain in
        (
            read_list(path.join(ROOT_DIR, 'domain_tools', 'data', 'product_page_blacklist.txt')) +
            read_list(path.join(ROOT_DIR, 'domain_tools', 'data', 'company_page_blacklist.txt'))
        )
    ]

    def __init__(self, min_length: int = 3, max_length: int = 60, remove_retailers: bool = False):
        # max length: "Walt Disney Parks, Experiences and Consumer Products, Inc."
        self.min_length = min_length
        self.max_length = max_length
        self.remove_retailers = remove_retailers

    def __call__(self, fn):
        @wraps(fn)
        def wrapper(*args, **kwargs) -> nx.DiGraph:
            graph = fn(*args, **kwargs)

            nodes = list(graph.nodes())
            for node in nodes:
                try:
                    entity = graph.nodes[node]['entity']
                except Exception:
                    # probably the node was already deleted
                    continue

                length = len(entity.value)
                if not (self.min_length <= length <= self.max_length):
                    logger.warning(f'Bad entity value length, discarding: {entity}')
                    graph.remove_node(node)
                    continue

                value_uo = uo(entity.value)
                if self.remove_retailers and any(similar(value_uo, retailer) for retailer in self.RETAILERS):
                    family = [
                        components for components in weakly_connected_components(graph)
                        if node in components
                    ][0]
                    for node_to_remove in family:
                        graph.remove_node(node_to_remove)
                    continue

                # remove websites that are definitely not company websites
                if entity.get('websites'):
                    entity.websites = list(filterfalse(in_blacklist, entity.websites))

            return graph

        return wrapper


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities()
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def bane(value: str) -> nx.DiGraph:
    """
    Wrapper for retrieving Bane's winners.

    Returns: list of Entities with `score`
    """
    results = Bane(config=LOCAL.settings['BaneConfig']).get_winner(value)
    if DEBUG:
        logger.debug(f'Bane results: {pretty(results)}')

    graph = nx.DiGraph()
    for res in results['winners']:
        if is_organization(res['winner']):
            entity = Entity(
                value=res['winner'],
                score=res['norm_score'],
            )
            graph.add_node(entity.id, entity=entity)

    # now use bane winners as targets for Fw/Fm
    bane_targets = {i: value for i, value in enumerate(
        [data['entity'].value for data in graph.nodes.values()],
        start=len(TARGETS.orig_contents)
    ) if not any(similar(value, target) for target in TARGETS.orig_contents.values())}
    targets = Targets(load_from=bane_targets)

    results = FWFM(config=LOCAL.settings['FwFmConfig'], targets=targets).get_winner(value)
    if DEBUG:
        logger.debug(f'Fw/Fm results: {pretty(results)}')

    fwfm_entities = [Entity(
        value=res['winner'] or res['target'],
        score=res['norm_score'],
        source='fwfm',
    ) for res in results['winners']]
    for entity in fwfm_entities:
        graph.add_node(entity.id, entity=entity)

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities()
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def bane2(value: str) -> nx.DiGraph:
    """
    Wrapper for retrieving Bane's winners.

    Returns: list of Entities with `score`
    """
    from bane.bane2 import Bane
    from find_maker.find_maker2 import FindMaker

    results = Bane.get_winners(value)
    if DEBUG:
        logger.debug(f'Bane results: {pretty(results)}')

    graph = nx.DiGraph()
    for row in results[:4]:
        if row['score'] < 0.4:
            continue

        entity = Entity(
            value=row['name'],
            score=row['score'],
            kind='company' if is_organization(row['name']) else 'unknown',
        )

        # select appropriate website
        domains = list(filterfalse(in_blacklist, map(get_domain, row['websites'].values())))
        most_common = Counter(domains).most_common(3)

        if 0 < len(most_common) < 3:
            entity.websites = [most_common[0][0]]
        elif len(most_common) >= 3:
            # only select most common website if it's more frequent than any other one
            if most_common[0][1] > most_common[1][1]:
                entity.websites = [most_common[0][0]]

        graph.add_node(entity.id, entity=entity)

    # now use bane winners as targets for Fw/Fm
    default_targets = FindMaker().targets
    bane_targets_types = {
        data['entity'].value: data['entity'].get('kind', 'company')
        for data in graph.nodes.values()
        if not any(similar(data['entity'].value, target) for target in default_targets)
    }
    if not bane_targets_types:
        return graph

    results = FindMaker(targets=list(bane_targets_types.keys())).get_winners(value)
    if DEBUG:
        logger.debug(f'Fw/Fm results: {pretty(results)}')

    for name, score in results:
        if score < 0.4:
            continue

        entity = Entity(value=name, score=score, source='find_maker2', kind=bane_targets_types[name])
        graph.add_node(entity.id, entity=entity)

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1, remove_retailers=True)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def bane4products(value: str) -> nx.DiGraph:
    """
    Wrapper for retrieving Bane's winners.

    Returns: list of Entities with `score`
    """
    return bane2(value)


def get_aliases(target: str) -> List[str]:
    """ Returns aliases from targets database """
    metadata = TARGETS.get_metadata(target)
    aliases = metadata['company_aliases']
    aliases = remove_dupes(
        aliases,
        comp_fn=lambda value1, value2:
        similar(uo(value1), uo(value2))
    )
    return aliases


def targets_to_graph(targets: Iterable[Tuple[str, float]]) -> nx.DiGraph:
    """ Given any list of targets data, returns graph with all target-related information """
    graph = nx.DiGraph()

    for target, score in targets:
        info = TARGETS.get_all()[target]

        entity = Entity(
            value=info['official_name'],
            target=target,
        )

        if score:
            entity.score = score

        try:
            ticker, exchange = normalize_tkr_exch(info.get('ticker'), info.get('exchange'))
            if ticker and exchange:
                entity.ticker = ticker
                entity.exchange = exchange
        except InvalidTickerExchange as exc:
            logger.warning(f'Targets_to_graph: {exc}')

        # add aliases where possible
        aliases = get_aliases(target)
        aliases = remove_value(aliases, entity.value)
        if aliases:
            entity.aliases = aliases

        graph.add_node(entity.id, entity=entity)

    # for entities with tickers, add targets with same identifiers
    families = defaultdict(list)  # ticker: entities
    entities = list(node['entity'] for node in graph.nodes.values())
    for entity in filter(lambda entity: entity.get('ticker'), entities):
        target = entity.target
        ticker, exchange = entity['ticker'], entity['exchange']
        families[ticker].append(entity)

        rel_targets = {
            rel_target: info for rel_target, info in TARGETS.get_by_ticker(ticker, exchange).items()
            if rel_target != target
        }
        for rel_target, info in rel_targets.items():
            rel_entity = next(
                filter(
                    lambda rel_entity: rel_entity.get('target') == rel_target,
                    map(itemgetter('entity'), graph.nodes.values()),
                ),
                None
            )
            if not rel_entity:
                rel_entity = Entity(
                    value=info['official_name'],
                    target=rel_target,
                    aliases=get_aliases(rel_target),
                )

                graph.add_node(rel_entity.id, entity=rel_entity)

            families[ticker].append(rel_entity)

    # create relations
    for ticker, family in filter(lambda ticker_family: len(ticker_family[1]) > 1, families.items()):
        parent_entity = None

        ticker_infos = list(filter(None, [
            TickerInfo.get_info_from_tradier(ticker, exchange),
            TickerInfo.get_info_from_yahoo(ticker, exchange),
        ]))

        ticker_names = list(set(filter(None, [uo(info.get('name', '')) for info in ticker_infos])))

        for entity in family:
            known_names = [entity.value] + entity.get('aliases', [])
            if any(similar(*pair, thld=0.8) for pair in product(ticker_names, known_names)):
                parent_entity = entity
                break

        if parent_entity:
            for entity in family:
                if entity == parent_entity:
                    continue

                graph.add_edge(entity.id, parent_entity.id, relation='parent')

        else:
            base_entity = family[0]
            for entity in family[1:]:
                graph.add_edge(entity.id, base_entity.id, relation='unknown')
                graph.add_edge(base_entity.id, entity.id, relation='unknown')

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def fwfm(value: str) -> nx.DiGraph:
    """
    Wrapper for retrieving Fw/Fm's winners.

    Returns: list of Entities, with `score` and `target` attrs.
    """

    results = FWFM(config=LOCAL.settings['FwFmConfig']).get_winner(value)
    if DEBUG:
        logger.debug(f'Fw/Fm results: {pretty(results)}')

    graph = targets_to_graph((res['target'], res['norm_score']) for res in results['winners'])

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1, remove_retailers=True)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def fwfm4products(value: str) -> nx.DiGraph:
    """
    Wrapper for retrieving Fw/Fm's winners.

    Returns: list of Entities, with `score` and `target` attrs.
    """
    return fwfm(value)


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1, remove_retailers=True)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def fwfm2(value: str) -> nx.DiGraph:
    from find_maker.find_maker2 import FindMaker

    results = FindMaker().get_winners(value)
    if DEBUG:
        logger.debug(f'Fw/Fm results (top 10): {pretty(results[:10])}')
    results = [res for res in results if res[1] >= 0.97]

    graph = targets_to_graph(results)

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities()
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def whois(domain: str) -> nx.DiGraph:
    """
    Wrapper for retrieving Whois winners.

    Returns: list of Entities
    """
    graph = nx.DiGraph()

    org = WhoisInfoRetrieval.get_organization(domain)
    if not org:
        return graph

    entity = Entity(value=org, score=1, websites=[domain])
    graph.add_node(entity.id, entity=entity)
    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities()
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def ssl(domain: str) -> nx.DiGraph:
    """
    Wrapper for retrieving SSL winners.

    Returns: list of Entities
    """
    graph = nx.DiGraph()

    org = SSLCertificateRetrieval.get_organization(domain)
    if not org:
        return graph

    entity = Entity(value=org, score=1, websites=[domain])
    graph.add_node(entity.id, entity=entity)
    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities()
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def copyright(domain: str) -> nx.DiGraph:
    """
    Wrapper for retrieving winners from copyright string.

    Returns: list of Entities
    """
    graph = nx.DiGraph()

    results = get_copyright(domain)
    if not results:
        return graph

    entities = [Entity(value=result, score=1, websites=[domain]) for result in results]
    for entity in entities:
        graph.add_node(entity.id, entity=entity)

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def wikipedia(value: Union[str, WikiPage]) -> nx.DiGraph:
    """
    Wrapper for retrieving wikipedia winners.

    Returns: DiGraph of Entities with attrs:
        'wiki_page': WikiPage instance (optional),
        'ticker': company ticker (or None),
        'exchange': exchange for ticker (or None),
    """
    # bump: 1
    graph = nx.DiGraph()

    if isinstance(value, WikiPage):
        pages = [value]
    else:
        pages = WikiPage.from_str(value)

    if not pages:
        return graph

    for page in pages:
        if not page.infobox.get('Title'):
            logger.debug(f'Page {page} has no title -> discarding')
            continue

        last_owner = None
        for owner in page.iter_owners(include_self=True):
            if isinstance(owner, WikiPage):
                entity = Entity(
                    value=owner.infobox['Title'],
                    kind='company' if owner.is_corporate() else 'unknown',
                    wiki_page=owner,
                )
                entity.internal_scores = owner.internal_scores
                if owner == page:
                    entity.score = page.score

                entity.websites = remove_dupes([url for _, url in owner.infobox.get('Website', [])])

                try:
                    ticker, exchange = normalize_tkr_exch(*owner.ticker)
                    if ticker and exchange:
                        entity.ticker, entity.exchange = ticker, exchange
                except InvalidTickerExchange as exc:
                    logger.warning(f'ER[wikipedia]: {exc}')

                graph.add_node(entity.id, entity=entity)
                if last_owner:
                    graph.add_edge(last_owner.id, entity.id, relation='parent')

                last_owner = entity

            elif isinstance(owner, str):
                # if last owner doesn't have WikiPage - add it without page
                # see https://en.wikipedia.org/wiki/Instacart
                # but check whether owner is company - for Valve corp owner is a person
                # see https://en.wikipedia.org/wiki/Valve_Corporation
                if not any(
                        similar(owner, value)
                        for value in [data['entity'].value for data in graph.nodes.values()]
                ) and is_organization(owner, strict=True):
                    entity = Entity(value=owner)

                    graph.add_node(entity.id, entity=entity)
                    if last_owner:
                        graph.add_edge(last_owner.id, entity.id, relation='parent')

                    last_owner = entity

            elif isinstance(owner, list):
                owners = owner
                first_owner = owners[0]

                # sometimes owners may be a mixture of company and not company:
                # https://en.wikipedia.org/wiki/Volkswagen_Group
                # owners:
                # - https://en.wikipedia.org/wiki/Porsche_SE (corporate)
                # - https://en.wikipedia.org/wiki/Lower_Saxony (NOT corporate)
                # - https://en.wikipedia.org/wiki/Qatar_Investment_Authority
                if not (
                        isinstance(first_owner, WikiPage) and first_owner.is_corporate()
                ) and not (
                        isinstance(first_owner, str) and is_organization(first_owner)
                ):
                    logger.debug(f'First owner is not corporate, discarding all owners: {owners}')
                    break

                for i, owner in enumerate(owners):
                    if isinstance(owner, WikiPage) and owner.is_corporate():
                        entity = Entity(value=owner.infobox.get('Title'), wiki_page=owner)

                        try:
                            ticker, exchange = normalize_tkr_exch(*owner.ticker)
                            if ticker and exchange:
                                entity.ticker, entity.exchange = ticker, exchange
                        except InvalidTickerExchange as exc:
                            logger.warning(f'ER[wikipedia]: {exc}')

                        entity.websites = remove_dupes([url for _, url in owner.infobox.get('Website', [])])
                    elif isinstance(owner, str):
                        entity = Entity(value=owner)

                    graph.add_node(entity.id, entity=entity)
                    if last_owner:
                        graph.add_edge(last_owner.id, entity.id, relation='parent', priority=i)

                break

    # add aliases & children for each entity (where possible)
    entities = map(itemgetter('entity'), graph.nodes.values())
    for entity in list(filter(lambda entity: hasattr(entity, 'wiki_page'), entities)):
        page = entity.wiki_page

        # ---- aliases ----
        aliases = [page.article_title]
        for native_name, _ in page.infobox.get('Native name', []):
            aliases.append(native_name)
        aliases = remove_dupes(aliases)
        aliases = remove_value(aliases, entity.value)

        if aliases:
            entity.aliases = aliases

        # ---- children ----
        for name, url in page.infobox.get('Subsidiaries', []):
            subsidiary = next((node['entity'] for node in graph.nodes.values() if node['entity'].value == name), None)
            if not subsidiary:
                subsidiary = Entity(value=name)

                if url and re.match(r'^https://(\w*\.)?wikipedia\.org/', url):
                    subsidiary.wiki_page = WikiPage(url=url)

                graph.add_node(subsidiary.id, entity=subsidiary)

            graph.add_edge(subsidiary.id, entity.id, relation='parent')

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1, remove_retailers=True)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def wikipedia4products(value: Union[str, WikiPage]) -> nx.DiGraph:
    return wikipedia(value)


def _website(website: dict) -> nx.DiGraph:
    from mapper.mapper import map_input

    graph = nx.DiGraph()

    # ---- entity from just website ----
    if website['name']:
        # value may be "Cosmic Coffee + Beer Garden - Restaurant - Austin, Texas  - 101 Reviews - 290 Photos"
        name = re.split(r'\s+-\s+', website['name'])[0]
        if is_organization(name):
            entity = Entity(
                value=name,  # or website['title'],
                websites=[website['url']],
                source='about',
            )
            graph.add_node(entity.id, entity=entity)

    # ---- entities from domain mapper ----
    res = map_input(website['url'], mode='domain')
    if DEBUG:
        logger.debug(f'Mapper result:')
        logger.debug(pretty(res))

    groups = list(chain.from_iterable(
        (attempt.get('losers', []) + attempt.get('winners', []))
        for attempt in res.get('attempts', [])
    ))

    for group in groups:
        graph = nx.compose(graph, group['graph'])

    # ---- winner from domain mapper as separate 1st gen entity ----
    for group in groups:
        winner = pick_winner(group['graph'])
        if not winner:
            continue

        entity = Entity(
            value=winner.value,
            score=group['score'],
            kind=winner.get('kind', 'company'),
        )
        graph.add_node(entity.id, entity=entity)
        graph.add_edge(winner.id, entity.id, relation='similar')
        graph.add_edge(entity.id, winner.id, relation='similar')

    # ---- add some information to all nodes ----
    for entity in map(itemgetter('entity'), graph.nodes.values()):
        # entity.score = 1
        # entity.internal_source = entity.source
        # del entity.source
        entity.url = website['url']

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities()
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def company_website(value: str) -> nx.DiGraph:
    """
    Wrapper for retrieving winners for company website.
    Queries Bing with <value>, takes first page's url and feeds it to domain mapper.
    """
    # CACHE BUMP: v2
    website = get_website(value)
    if not website:
        return nx.DiGraph()

    return _website(website)


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1, remove_retailers=True)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def company_website4products(value: str) -> nx.DiGraph:
    # CACHE BUMP: v2
    website = get_website(value, query_string='{} official website',
                          blacklist=NOT_COMPANY_WEBSITES + NOT_PRODUCT_WEBSITES)
    if not website:
        return nx.DiGraph()

    return _website(website)


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities()
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def tfidf_keywords(value: str) -> nx.DiGraph:
    """ Extracts keywords and adds them as nodes """
    graph = nx.DiGraph()
    # thld = 100

    keywords = tfidf_extract_keywords(value)
    keywords = [(re.sub(r'^(.*) \1', r'\1', kw), score) for kw, score in keywords]
    logger.debug(f'Tf/Idf keywords (top 10): {keywords[:10]}')
    keywords = [
        (word, score) for word, score in keywords
        # if score >= thld   # and (word.count(' ') > 0 or word not in DICTIONARY_WORDS)
    ][:5]

    for word, score in keywords:
        entity = Entity(
            value=word,
            kind='keyword',
            score=min(score / 200, 1),  # score/keywords[0][1],
            internal_scores={'tfidf':  round(score, 3)},
        )

        info = TickerFinder.query_db(word, include_empty_ticker=True)
        if info:
            logger.debug(f'Keyword "{word}" found in top 7k tickers table: {info} -> kind = company')
            entity.kind = 'company'

            if info['ticker'] and info['exchange']:
                entity.ticker = info['ticker']
                entity.exchange = info['exchange']

            aliases = remove_value(info['names'], entity.value)
            if aliases:
                entity.aliases = aliases

        else:
            targets = TARGETS.get_by_similar_name(uo(word))
            if targets:
                logger.debug(f'Keyword "{word}" found in targets: {targets} -> kind = company')
                entity.kind = 'company'

                info = TARGETS.get_metadata(targets[0])

                if info['ticker'] and info['exchange']:
                    entity.ticker = info['ticker']
                    entity.exchange = info['exchange']

                aliases = info.get('company_aliases', [])
                aliases = remove_value(aliases, entity.value)
                if aliases:
                    entity.aliases = aliases

            elif re.match(r'.+ (' + '|'.join(map(re.escape, COMPANY_TERMINATORS)) + r')\.?', word, flags=re.IGNORECASE):
                entity.kind = 'company'

        graph.add_node(entity.id, entity=entity)

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities()
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def small_parsers(value: str) -> nx.DiGraph:
    from .mapper import ComparisonGraph
    graph = ComparisonGraph()

    NUM_RESULTS = 20

    bing = BingWebCore()
    response = bing.query(value)
    suggestions = response.get('spellSuggestions', {}).get('value', [])
    if suggestions:
        logger.warning(f'Found Bing spell suggestions for "{value}", following first one: {pretty(suggestions)}')
        response = bing.query(suggestions[0]['text'])

    if 'webPages' not in response:
        if DEBUG:
            logger.debug(f'No results from Bing')
        return graph

    if DEBUG:
        logger.debug(f'Bing response ({NUM_RESULTS} first): {pretty(response["webPages"]["value"][:NUM_RESULTS])}')

    def process_url(bing_result: dict) -> Optional[Entity]:
        url = bing_result['url']
        parser = select_parser(url)
        if not parser:
            logger.debug(f'No WebsiteParser found for "{url}"')
            return

        entity = parser.parse(bing_result)
        if entity:
            entity.source = parser.__name__
            entity.from_url = url

        return entity

    with LocalsThreadPoolExecutor(
            max_workers=4 if LOCAL.settings['PARALLEL'] else 1,
            thread_name_prefix='BingWebsiteParsersPool',
    ) as pool:
        results = pool.map(process_url, response['webPages']['value'][:NUM_RESULTS])

    for position, entity in filter(lambda pos_entity: pos_entity[1], enumerate(results)):
        entity.position = position
        entity.score = -0.2087336 + 1.223967 * math.e**(-0.05215774 * position)
        # ^-- 0=1, 1=0.95, 2=0.9, 3=0.85, 4=0.8, 10=0.5, 30=0.05
        # Gene: -2.298 * math.log(position) + 9.7801
        # Old: -math.log(position + 1, NUM_RESULTS) + 1
        graph.add_node(entity.id, entity=entity)

    graph.build_edges(allow_same_sources=True)

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities()
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def title(domain: str) -> nx.DiGraph:
    """
    Wrapper for retrieving domain's website title.

    Returns: list of Entities with `score` attr.
    """
    graph = nx.DiGraph()

    title = get_title(domain)
    if not title:
        return graph

    entity = Entity(value=title, score=1)
    graph.add_node(entity.id, entity=entity)
    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def bloomberg(value: str, exact: bool) -> nx.DiGraph:
    """
    Wrapper for retrieving company name from bloomberg.

    Args:
        value: value to search for (company name / domain)
        exact: whether value should appear exactly as typed on bloomberg page
    """
    MAX_RESULTS = 5  # how many bing web results are taken
    MAX_CANDIDATES = 1  # how many candidates are checked for exactness
    QUERY = f'domain:www.bloomberg.com intitle:"Private Company Information" ' + \
            (f'inbody:"{value}"' if exact else value)
    if DEBUG:
        logger.debug(f'Query: {QUERY}')

    graph = nx.DiGraph()

    bing_response = BingWebCore().query(QUERY)
    if not bing_response:
        if DEBUG:
            logger.debug(f'Bing produced no response')
        return graph

    results = bing_response.get('webPages', {}).get('value', [])

    if not results:
        if DEBUG:
            logger.debug(f'Bing produced no results')
        return graph
    elif DEBUG:
        logger.debug(pretty(results))

    URL_PATTERN = 'https://www.bloomberg.com/research/stocks/private/snapshot.asp?privcapId='.lower()
    results = [
        res for res in results[:MAX_RESULTS]
        if res['url'].lower().startswith(URL_PATTERN)
    ][:MAX_CANDIDATES]
    if not results:
        if DEBUG:
            logger.debug(f'No bing results that match desired URL pattern: "{URL_PATTERN}"')
        return graph

    if DEBUG:
        logger.debug(f'Bing found {len(results)} results')

    # # bing doesn't respect "strict" terms in search string, thus we need to check bing results manually
    # def get_response(url, value):
    #     """ Returns response if web page with `url` contains `value` """

    #     logger.debug(f'Checking url "{url}"')

    #     response = get(url)
    #     if not response:
    #         if DEBUG:
    #             logger.debug(f'Bad response from url "{url}"')
    #         return

    #     text = response.get_soup().get_text()
    #     if value not in text:
    #         if DEBUG:
    #             logger.debug(f'Url "{url}" rejected as not containing value "{value}"')
    #         return

    #     return response

    url = results[0]['url']
    response = get(url)
    if not response or not response.ok:
        logger.warning(f'Bloomberg: no response from "{url}"')
        return graph

    name = response.soup.find('span', {'itemprop': 'name'}).get_text()
    entity = Entity(
        value=name,
        url=response.url,
        score=1,
    )

    try:
        entity.websites = [response.soup.find('a', {'itemprop': 'url'}).get_text()]
    except Exception:
        pass

    graph.add_node(entity.id, entity=entity)

    try:
        description = response.soup.find('p', {'id': 'bDesc'}).get_text()
        match = re.search(r'operates as a subsidiary of (.+?)\.$', description)
        if match:
            owner = Entity(value=match[1])
            graph.add_node(owner.id, entity=owner)
            graph.add_edge(entity.id, owner.id, relation='parent')

    except Exception:
        logger.warning(f'Bloomberg: could not parse description at "{response.url}"')

    return graph


def bloomberg_exact(value: str) -> nx.DiGraph:
    return bloomberg(value, exact=True)


def bloomberg_fuzzy(value: str) -> nx.DiGraph:
    return bloomberg(value, exact=False)


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def bloomberg_lite(value: str) -> nx.DiGraph:
    """ Wrapper for getting entities from Bloomberg without actually reaching the website """
    MAX_RESULTS = 5  # how many bing web results are taken

    graph = nx.DiGraph()

    value = re.sub(r'^www\d?.', '', value)

    QUERY = f'site:www.bloomberg.com Private Company Information "{value}"'
    # QUERY = f'site:www.bloomberg.com "{value}" Company Profile'
    if DEBUG:
        logger.debug(f'Query: {QUERY}')

    bing_response = BingWebCore().query(QUERY)
    if not bing_response:
        if DEBUG:
            logger.debug(f'Bing produced no response')
        return graph

    results = bing_response.get('webPages', {}).get('value', [])
    if not results:
        if DEBUG:
            logger.debug(f'Bing produced no results')
        return graph

    if DEBUG:
        logger.debug(f'Bing response (first 5):\n{pretty(results[:5])}')

    URL_PATTERN = 'https://www.bloomberg.com/research/stocks/private/snapshot.asp?privcapId='.lower()
    # URL_PATTERN = 'https://www.bloomberg.com/profiles/companies/'
    for res in results[:MAX_RESULTS]:
        if res['url'].lower().startswith(URL_PATTERN):
            result = res
            break
    else:
        if DEBUG:
            logger.debug(f'No bing results that match desired URL pattern: "{URL_PATTERN}"')
        return graph

    if DEBUG:
        logger.debug(f'Found matching result:\n{pretty(res)}')

    about_names = list(set(map(itemgetter('name'), result.get('about', []))))
    if about_names:
        if DEBUG:
            logger.debug(f'Found name->about: {about_names}')

        name = about_names[0]
        if name != 'Google':
            entity = Entity(value=name, url=res['url'], score=1)
            graph.add_node(entity.id, entity=entity)

    else:
        name_match = re.match(r'(.*): Private', res['name'], flags=re.IGNORECASE)
        name = name_match[1] if name_match else None
        if name:
            if DEBUG:
                logger.debug(f'Found name: {name}')

            entity = Entity(value=name, url=res['url'], score=1)
            graph.add_node(entity.id, entity=entity)

    return graph


bloomberg_search_header, bloomberg_search_model = xgb_load_model(
    path.join(DATA, 'bloomberg_search.xgb_boost.2019.04.17.11.54.pickle'))


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def bloomberg_search(value: str) -> nx.DiGraph:
    graph = nx.DiGraph()

    URL = 'https://search.bloomberg.com/lookup.json'

    response = requests.get(URL, params={
        'query': value,
        'sites': 'bbiz',
        'frag_size': 192,
        'source': 'md8m74nf2j9',
        'types': 'company_public',
        'group_size': 3,
        'fields': ','.join(['name', 'slug', 'ticker_symbol', 'url', 'organization', 'title']),
        '_': 1545062607257,
    })
    if DEBUG:
        logger.debug(f'Request url: {response.request.url}')

    response.raise_for_status()
    results = response.json()[0]['results']
    if DEBUG:
        logger.debug(f'Found {len(results)} items:')
        logger.debug(pretty(results))

    entities = []
    for i, item in enumerate(results):
        entity = Entity(
            value=item['name'],
            url=item['url'],
            internal_scores={
                'position': i,
                'score': item['score'],
            },
            score=1,
        )

        entity.score = xgb_predict(entity['internal_scores'], bloomberg_search_header, bloomberg_search_model)

        try:
            ticker, exchange = normalize_tkr_exch(*item.get('ticker_symbol', ':').split(':'),
                                                  mapping=BLOOMBERG_EXCHANGES_MAP)
            if ticker and exchange:
                entity.ticker, entity.exchange = ticker, exchange
        except InvalidTickerExchange as exc:
            logger.warning(f'ER[bloomberg_search]: {exc}')

        entities.append(entity)

    # entities.sort(key=lambda entity: (
    #     entity.value,
    #     EXCHANGES.index(entity.get('exchange')) if entity.get('exchange') in EXCHANGES else len(EXCHANGES),
    # ))
    entities = remove_dupes(entities, key=itemgetter('value'))
    for entity in entities:
        graph.add_node(entity.id, entity=entity)

    return graph


CRUNCHBASE = [d for d in csv.DictReader(open(path.join(DATA, 'crunchbase.2019.05.04.csv'), 'r', encoding='utf-8'))]
CRUNCHBASE_PREFIX_LENGTH = 5
CRUNCHBASE_BY_PREFIX = prefixize(CRUNCHBASE, prefix_length=CRUNCHBASE_PREFIX_LENGTH, key=itemgetter('name'))


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def crunchbase(value: str) -> nx.DiGraph:
    graph = nx.DiGraph()

    value_uo = uo(value)

    words = [word.lower() for word in tokenize(value_uo) if len(word) >= 4]
    candidates = list(chain.from_iterable(
        [CRUNCHBASE_BY_PREFIX.get(word[:CRUNCHBASE_PREFIX_LENGTH], []) for word in words]
    ))
    if not candidates:
        return graph

    for candidate in candidates:
        name = candidate['name']
        name_uo = uo(name)
        domain = get_domain(candidate['website'])

        internal_scores = {
            # crunchbase's website found in value
            'domain_match': int(bool(domain and re.search(rf'\b{re.escape(domain)}\b', value, flags=re.IGNORECASE))),

            # crunchbase name is similar to value
            'similar': similar(value, name),

            # value contains crunchbase name
            'is_substring': int(bool(re.search(rf'\b{re.escape(name)}\b', value, flags=re.IGNORECASE))),
            # or all(word.lower() in words for word in tokenize(name)),

            # value contains crunchbase uo name
            'uo_is_substring': int(bool(
                name_uo and re.search(rf'\b{re.escape(name_uo)}\b', value, flags=re.IGNORECASE))),
            # or all(word.lower() in words for word in tokenize(name_uo)),

            # crunchbase name contains value
            'has_value': int(bool(re.search(rf'\b{re.escape(value)}\b', name, flags=re.IGNORECASE))),

            # crunchbase value has ticker & exchange
            'has_ticker': int(bool(candidate.get('ticker') and candidate.get('exchange'))),
        }

        if internal_scores['similar'] >= 0.98 or internal_scores['domain_match']:
            score = 1

        elif internal_scores['is_substring'] and internal_scores['has_ticker']:
            score = 0.8

        elif internal_scores['has_value'] and internal_scores['uo_is_substring']:
            score = 0.8

        else:
            score = 0

        if DEBUG and any(internal_scores[key] for key in internal_scores if key != 'has_ticker'):
            logger.debug(pretty({**candidate, 'internal_scores': internal_scores}))

        if score:
            entity = Entity(
                value=candidate['name'],
                score=score,
                internal_scores=internal_scores,
            )
            if candidate['website']:
                entity.websites = [candidate['website']]
            if candidate['ticker'] and candidate['exchange']:
                entity.ticker, entity.exchange = candidate['ticker'], candidate['exchange']

            graph.add_node(entity.id, entity=entity)

    # add relations
    for entity1, entity2 in combinations([node['entity'] for node in graph.nodes.values()], 2):
        # same domains mean that entities are related
        if entity1.websites and entity2.websites and get_domain(entity1.websites[0]) == get_domain(entity2.websites[0]):
            graph.add_edge(entity1.id, entity2.id, relation='similar')
            graph.add_edge(entity2.id, entity1.id, relation='similar')
            continue

        entity1.setdefault('not_related', []).append(entity2.id)
        entity2.setdefault('not_related', []).append(entity1.id)

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def crunchbase_api(value: str) -> nx.DiGraph:
    # https://api.crunchbase.com/v3.1/csv_export/csv_export.tar.gz?user_key=db6f9dfaccc924e7ac040665596632b7
    # https://api.crunchbase.com/v3.1/odm/odm.csv.tar.gz?user_key=db6f9dfaccc924e7ac040665596632b7
    graph = nx.DiGraph()

    key = 'db6f9dfaccc924e7ac040665596632b7'
    URL = 'https://api.crunchbase.com/v3.1/odm-organizations'

    response = requests.get(URL, params={
        'user_key': key,
        'organization_types': 'company',
        'name': uo(value),
    }, timeout=10)
    if not response:
        logger.warning(f'Request to crunchbase failed: {response}')
        return graph

    response.raise_for_status()

    results = response.json()['data']['items']

    if DEBUG:
        logger.debug(pretty(results))

    for item in map(itemgetter('properties'), results[:2]):
        entity = Entity(
            value=item['name'],
            url=f'https://www.crunchbase.com/organization/{item["permalink"]}',
        )

        if item['domain']:
            entity.websites = [item['domain']]

        if item['stock_symbol']:
            try:
                ticker, exchange = normalize_tkr_exch(item['stock_symbol'], item['stock_exchange'])
                if ticker and exchange:
                    entity.ticker, entity.exchange = ticker, exchange
            except InvalidTickerExchange as exc:
                logger.warning(f'ER[crunchbase]: {exc}')

        graph.add_node(entity.id, entity=entity)

    return graph


gkg_header, gkg_model = xgb_load_model(path.join(DATA, 'gkg.xgb_boost.pickle'))


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def google_knowledge_graph(value: str) -> nx.DiGraph:
    graph = nx.DiGraph()

    URL = 'https://kgsearch.googleapis.com/v1/entities:search'
    params = {
        'key': LOCAL.settings['GOOGLE_KNOWLEDGE_KEY'],
        'types': ['Organization', 'Corporation'],
        'query': uo(value),
        'limit': 1,
        'indent': True,
    }
    response = requests.get(URL, params)
    if not response.ok:
        if DEBUG:
            logger.debug(f'Bad response: {response}')
        return graph

    results = response.json()['itemListElement']
    if not results:
        if DEBUG:
            logger.debug(f'No results for query {params}')
        return graph

    if DEBUG:
        logger.debug(pretty(results))

    result = results[0].get('result', {})
    if 'name' not in result:  # sometimes there's no name, as for kg:/m/02fzs
        return graph

    attrs = {}

    attrs['internal_scores'] = {'score': results[0]['resultScore']}
    prob = xgb_predict(attrs['internal_scores'], gkg_header, gkg_model)
    if prob < 0.5:
        logger.debug(f'Result "{result["name"]}" rejected - too low score ({prob})')
        return graph
    attrs['score'] = prob

    description_url = result.get('detailedDescription', {}).get('url', '')
    if 'wikipedia.org' in description_url:
        attrs['wiki_page'] = WikiPage(url=description_url)

    url = result.get('url')
    if url:
        attrs['websites'] = [url]

    entity = Entity(value=result['name'], **attrs)
    graph.add_node(entity.id, entity=entity)

    # follow wiki page
    wiki_page = entity.get('wiki_page')
    if wiki_page and wiki_page.is_corporate():
        wiki_graph = wikipedia(wiki_page)
        if not wiki_graph:
            logger.warning(f'GKG[wikipedia] produced empty graph for WikiPage {wiki_page}')
            return graph

        for entity in map(itemgetter('entity'), wiki_graph.nodes.values()):
            entity.source = 'wikipedia'
            entity.score = None

        graph = nx.compose(graph, wiki_graph)

        first_entity = [
            node for node in wiki_graph.nodes.values()
            if node['entity'].get('wiki_page') == wiki_page
        ][0]['entity']
        graph.add_edge(entity.id, first_entity.id, relation='alias')
        graph.add_edge(first_entity.id, entity.id, relation='alias')

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def db_lookup(value: str) -> nx.DiGraph:
    """ Given any cleaned string, searches it directly in nodes table """
    graph = nx.DiGraph()

    value_uo = uo(value)
    targets = [(target, 1) for target in TARGETS.get_by_similar_name(value_uo)]  # or TARGETS.get_by_ticker(cleaned)

    tokens = tokenize(value_uo)
    for subvalue in chain.from_iterable(ngrams(tokens, n) for n in range(1, 3)):
        targets += [(target, 0.5) for target in TARGETS.get_by_similar_name(' '.join(subvalue), thld=1.0)]

    if not targets:
        return graph

    # TODO: dupes appear for "Adidas TaylorMade Golf Hat  Flexfit Delta Fitted/ -X- Mineral Blue"
    targets = remove_dupes(targets, key=itemgetter(0))
    graph = targets_to_graph(targets)

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def top_7k_lookup(value: str) -> nx.DiGraph:
    """ Given any cleaned string, searches it in 7k tickers table """
    graph = nx.DiGraph()

    value_uo = uo(value)

    entries = [
        (entry, 1) for entry in TickerFinder.tickers
        if any(similar(name_uo, value_uo) for name_uo in entry['names_uo'])
    ]

    tokens = tokenize(value_uo)
    for subvalue in chain.from_iterable(ngrams(tokens, n) for n in range(1, 3)):
        entries += [
            (entry, 0.5) for entry in TickerFinder.tickers
            if any(similar(name_uo, ' '.join(subvalue), thld=1.0) for name_uo in entry['names_uo'])
        ]

    if not entries:
        return graph

    entries = remove_dupes(entries, key=itemgetter(0))

    for entry, score in entries:
        entity = Entity(entry['names'][0], ticker=entry['ticker'], exchange=entry['exchange'], score=score)
        if len(entry['names']) > 1:
            entity.aliases = [entry['names'][1:]]

        graph.add_node(entity.id, entity=entity)

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1, remove_retailers=True)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def db_lookup4products(value: str) -> nx.DiGraph:
    return db_lookup(value)


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities()
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def wikidata(value: str) -> nx.DiGraph:
    graph = nx.DiGraph()

    QUERY = f"""
        SELECT DISTINCT ?item ?itemLabel ?ownerLabel ?parentLabel
        WHERE {{
            ?item wdt:P31 wd:Q4830453;
                    rdfs:label ?label.
            OPTIONAL {{
                ?item wdt:P127 ?owner;
                    wdt:P749 ?parent.
            }}
            SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
            FILTER CONTAINS(LCASE(?label), "{value.lower()}").
        }}
        LIMIT 5
    """

    results = requests.get(f'https://query.wikidata.org/sparql?format=json&query={QUERY}').json()['results']['bindings']

    for result in results:
        entity = Entity(
            value=result['itemLabel']['value'],
            ref=result['item']['value'],
        )
        graph.add_node(entity.id, entity=entity)

        for key in ['ownerLabel']:  # , 'parentLabel']:
            if key in result:
                parent_entity = Entity(
                    value=result[key]['value'],
                )
                graph.add_node(parent_entity.id, entity=parent_entity)
                graph.add_edge(entity.id, parent_entity.id, relation='parent')

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities(min_length=1)
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def open_figi(value: str) -> nx.DiGraph:
    graph = nx.DiGraph()

    OPENFIGI_API_URL = 'https://api.openfigi.com/v2/search'

    response = requests.post(
        OPENFIGI_API_URL,
        data=json.dumps({'query': value}),
        headers={
            'content-type': 'application/json',
            'X-OPENFIGI-APIKEY': OPENFIGI_API_KEY,
        },
    )
    if not response.ok:
        logger.error(f'OpenFigi returned bad response for "{value}": [{response.status_code}] {response.text}')
        return graph

    results = response.json()['data']
    for res in results:
        res['exchange'] = TickerInfo.OPENFIGI_EXCHANGES_MAP.get(res.get('exchCode'))

    if DEBUG:
        logger.debug(pretty(results))

    names = [res['name'] for res in results]
    for name, cnt in Counter(names).most_common(1):
        name = re.sub(r'-CLASS \w$', '', name, flags=re.IGNORECASE)
        entity = Entity(value=name, internal_scores={'occurs': cnt}, score=1)

        # try get ticker
        tkr_exchs = [
            (res['ticker'], res['exchange'])
            for res in results
            if res['name'] == name
            and res['exchange']
            and res['securityType'] == 'Common Stock'
            and res['marketSector'] == 'Equity'
            and 1 <= len(res['ticker']) <= 4
        ]
        tkr_exchs.sort(key=lambda tkr_exch: EXCHANGES.index(tkr_exch[1]))

        if tkr_exchs:
            if len(tkr_exchs) > 1:
                logger.debug(f'Found many tickers for "{name}": {tkr_exchs}')

            entity.ticker, entity.exchange = Counter(tkr_exchs).most_common(1)[0][0]

        graph.add_node(entity.id, entity=entity)

    return graph


@NeverFail(fallback_value=nx.DiGraph())
@CheckEntities()
@conditional_decorator(DbCache(ERToolCache), condition=is_cache_enabled)
def website_title(value: str) -> nx.DiGraph:
    """
    Compares `value` with websites' titles.
    """
    graph = nx.DiGraph()

    bing_response = BingWebCore().query(value)
    if not bing_response:
        return graph

    results = bing_response.get('webPages', {}).get('value', [])
    if not results:
        return graph

    value = uo(value.lower())
    keywords = []
    for row in results:
        title = uo(row['name'].lower())

        longest = max(title, value, key=len)
        shortest = min(title, value, key=len)

        if shortest in longest or similar(shortest, longest, thld=0.95):
            keywords.append(shortest)
        #
        # match = SequenceMatcher(None, longest, shortest).find_longest_match(0, len(longest), 0, len(shortest))
        # if match.size >= 5:
        #     titles.append(longest[match.a:match.a+match.size])

        keywords.extend(common_words(value, title, include_subwords=True))

    counter = Counter(keywords)
    for title, cnt in counter.items():
        entity = Entity(value=title, score=cnt/len(keywords), kind='unknown')
        graph.add_node(entity.id, entity=entity)

    return graph


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('tool')
    parser.add_argument('input')
    args = parser.parse_args()

    with Timer(args.tool):
        graph = locals()[args.tool](args.input)

    print(pretty(nx.node_link_data(graph)))
