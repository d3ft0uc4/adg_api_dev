import os
from os import path
import sys
import sqlalchemy
import logging
from functools import wraps
from dotenv import load_dotenv
from datetime import timedelta
# from slack_log_handler import SlackLogHandler

load_dotenv(dotenv_path=os.path.join('api', 'app', '.env'))


class Configurable:
    """
    Decorator for making any class configurable.

    Usage example:
        class Conf1:
            A = 1

        class Conf2:
            A = 2

        @Configurable(default_config=Conf1)
        class MyClass:
            def test(self):
                return self.config.A

        print(MyClass().test())  # prints "1"
        print(MyClass(Conf2).test())  # prints "2"
    """

    def __init__(self, default_config):
        self.default_config = default_config

    def __call__(self, cls):
        original_init = cls.__init__

        @wraps(original_init)
        def init(slf, config=self.default_config, *args, **kwargs):
            setattr(slf, 'config', config)
            logger.debug(f'{slf.__class__.__name__} initialized with {config.__name__}')
            original_init(slf, *args, **kwargs)

        cls.__init__ = init
        return cls


# ---- Logger ----

LOG_LEVEL = os.environ.get('LOG_LEVEL', 'DEBUG')
LOG_FILE = os.environ.get('LOG_FILE')

logger = logging.getLogger('adg')
logger.setLevel(logging.getLevelName(LOG_LEVEL))
logger.handlers.clear()


class ExtraInfoFilter(logging.Filter):
    def filter(self, record):
        from packages.utils import LOCAL

        record.uid = getattr(LOCAL, 'run_uid', '-')
        record.raw_input = getattr(LOCAL, 'raw_input', '-')
        return True


fmt = '%(asctime)s %(process)s %(threadName)-25s %(levelname)-8s %(name)s %(message)s'

debug_handler = logging.StreamHandler(sys.stdout)
# debug_handler.setLevel(logging.DEBUG)
debug_handler.setFormatter(logging.Formatter(fmt))
logger.addHandler(debug_handler)


# SLACK_WEBHOOK_URL = 'https://hooks.slack.com/services/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'
# if bool(int(os.environ.get('SLACK', True))):
#     slack_handler = SlackLogHandler(
#         SLACK_WEBHOOK_URL, format='```%(message)s```\nInput: `%(raw_input)s`\nUID: `%(uid)s`\nDate: `%(asctime)s`')
#     slack_handler.setLevel(logging.INFO)
#     slack_handler.addFilter(ExtraInfoFilter())
#     logger.addHandler(slack_handler)

if LOG_FILE:
    os.makedirs(os.path.dirname(LOG_FILE), exist_ok=True)
    file_handler = logging.FileHandler(LOG_FILE)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(fmt)
    logger.addHandler(file_handler)

# Suppresses requests and urllib log messages under WARNING level.
logging.getLogger("requests").setLevel(logging.INFO)
logging.getLogger("urllib3").setLevel(logging.INFO)

# ---- Temporary settings that need to be eliminated ----
TEMP_WIKI_CHECK_CANDIDATE = bool(int(os.environ.get('TEMP_WIKI_CHECK_CANDIDATE', True)))
TEMP_PRODUCTS_MAPPER_WEBSITE_AGGRESSIVE = bool(int(os.environ.get('TEMP_PRODUCTS_MAPPER_WEBSITE_AGGRESSIVE', True)))

# ---- Global settings ----
if os.name == 'nt':
    DROPBOX = os.environ.get('DROPBOX', path.join('C:', 'Dropbox (ADG)', 'ADG Team Folder', 'tech',
                                                  'adg_api_dev_share', 'API'))
else:
    DROPBOX = os.environ.get('DROPBOX', path.join(path.expanduser('~'), 'Dropbox (ADG)', 'adg_api_dev_share', 'API'))

PARALLEL = bool(int(os.environ.get('PARALLEL', True)))
CLEANERS_PARALLEL = bool(int(os.environ.get('CLEANERS_PARALLEL', True)))
logger.debug(f'PARALLEL: {PARALLEL}')
logger.debug(f'CLEANERS_PARALLEL: {CLEANERS_PARALLEL}')

USE_CACHE = bool(int(os.environ.get('USE_CACHE', True)))
logger.debug(f'CACHE: {USE_CACHE}')

CACHE_ERS = USE_CACHE and bool(int(os.environ.get('CACHE_ERS', True)))
logger.debug(f'CACHE_ERS: {CACHE_ERS}')

CACHE_TICKERS = USE_CACHE and bool(int(os.environ.get('CACHE_TICKERS', True)))
CACHE_UPDATE = USE_CACHE and bool(int(os.environ.get('CACHE_UPDATE', False)))
logger.debug(f'CACHE_UPDATE: {CACHE_UPDATE}')

CACHE_EXPIRATION = timedelta(days=int(os.environ.get('CACHE_EXPIRATION', 14)))

BING_CACHE = bool(int(os.environ.get('BING_CACHE', True)))
BING_CACHE_EXPIRATION = timedelta(days=30)

STATS = bool(int(os.environ.get('STATS', False)))

INTERNAL_APP_KEYS = os.environ.get('INTERNAL_APP_KEYS', '').split(',')

DATA_DIR = os.environ.get(
    'DATA_DIR',
    os.path.join(os.path.dirname(os.path.dirname((os.path.abspath(__file__)))), 'venv')
)

CACHE_DIR = os.path.join(DATA_DIR, 'cache')
os.environ['TLDEXTRACT_CACHE'] = os.path.join(CACHE_DIR, '.tld_set')

TIMERS = bool(os.environ.get('TIMERS', False))

# ---- Output ----

DB_OUTPUT = bool(int(os.environ.get('DB_OUTPUT', True)))  # to store API runs and debug info to the database
LOG_TABLE = 'api_log'  # table where api runs are logged

CSV_DIR = os.path.join(DATA_DIR, 'csv')
CSV_OUTPUT = bool(int(os.environ.get('CSV_OUTPUT', False)))  # to write a CSV file with the debug info for each run
CSV_TIMING = bool(int(os.environ.get('CSV_TIMING', False)))  # to write speed performance CSV file for each run

# ---- Credentials ----

# TODO: Finish removing secrets from repo e.g. SQL_CONNECTION_STRING
SQL_CONNECTION_STRING = os.environ.get(
    'SQL_CONNECTION_STRING',
    'mysql+pymysql://'
    'adgsql:Dev2018!@adgaurora.cpu3eplzthqs.us-east-1.rds.amazonaws.com:3306/'
    'altdg_merchtag?charset=utf8'
)
GOOGLE_APPLICATION_CREDENTIALS = "access_keys/adgproj-0d8fbbb357be.json"
GBQ_PROJECT_ID = 'peaceful-signer-187500'

# ---- Keys ----
OPENFIGI_API_KEY = 'c538f9aa-21fb-446d-beb1-18eaf3a8e7f3'
TRADIER_API_KEY = 'KhThMA1Fd7rrme6sJuFA3oeGPOg8'

# ---- Bing ----

QUERY_IP = '72.229.117.155'
QUERY_LOCATION = 'lat:40.730610;long:-73.935242;re:5000m;ts:1513278053'
QUERY_USER_AGENT = 'PC-Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; Touch; rv:11.0) like Gecko'

ENTITY_SEARCH_API_KEY = 'ef470e21d5e24b21a056d61bc0ebc236'  # entity search + Web search key ($4/1k hits)
WEB_SEARCH_API_KEY = '713c655ea6c846269e9ba2c1cdad3742'  # Web-only key ($3/1k hits)

CACHE_TABLE_NAME = 'bing_cache'

GOOGLE_SEARCH_API_KEY = 'AIzaSyDz-m-tsbSE1QkRFP-5kyUpUIAJQn0UtQo'
GOOGLE_SEARCH_NAME = '017073476422301494910:u7en323jgtk'

# if True, BingWebCore will be replaced by Google
BING_PROXY_TO_GOOGLE = bool(int(os.environ.get('BING_PROXY_TO_GOOGLE', False)))


# ---- Bane ----
class BaneConfig:
    ZSCORE_PENALTY = 0.666
    THLD_ZSCORE = 5.8
    THLD_SUM = 5
    HI_SCORE = 90

    # normalized score cut-offs
    WINNER_THLD = 0.04
    SUPERWINNER_THLD = 0.8


# ---- Find maker / find winner ----
class FwFmConfig:
    SCORE_TYPES = ["unweighted", "weighted"]
    SEARCH_RESULT_TYPES = ['about', 'displayUrl', 'name', 'snippet']
    # ^ NB: order matters, should be the same as when model was trained
    SEARCH_STRING_MAP = {
        'original': '{}',
        'company': '{} company',
        'owner': 'who owns {}',
        'parent': '{} parent company',
        'brands': '{} brands',
        'sister': '{} sister company',
    }

    # FW/FM normalized score cut-offs
    WINNER_THLD = 0.1
    SUPERWINNER_THLD = 0.8

    SQL_SUBSTRING_PENALTY_POWER = 2
    MODEL = 'full.XGBmodel'


class MinimalFwFmConfig(FwFmConfig):
    SEARCH_STRING_MAP = {
        'original': '{}',
    }
    MODEL = 'minimal.XGBmodel'


# ---- Entity recognizers ----

CONF_LEVEL_LOW = 0.5
CONF_LEVEL_MEDIUM = 0.7
CONF_LEVEL_HIGH = 0.9

MAPPER_CONFIG = {
    'merchant': {
        'cleaners': [
            'TokensCleaner',
            'MerchantCleaner',
        ],
        'recognizers': [
            # 'bane',
            'fwfm',
            'bane2',
            # 'find_maker2',
            'company_website',
            'wikipedia',
            'bloomberg_search',
            'google_knowledge_graph',
            'small_parsers',
            'db_lookup',
            'open_figi',
            'crunchbase',
            'crunchbase_api',
            'website_title',
            'tfidf_keywords',
        ],
    },
    'product': {
        'cleaners': [
            'ProductTokensCleaner',
        ],
        'recognizers': [
            'bane4products',
            'fwfm4products',
            'company_website4products',
            'wikipedia4products',
            'small_parsers',
            'keywords',
            'db_lookup4products',
            'website_title',
        ],
    },
    'domain': {
        'cleaners': [
            'RelatedDomainsCleaner',
            'NoCleaner',
        ],
        'recognizers': [
            'bane2',
            'fwfm',
            'whois',
            'ssl',
            'copyright',
            'wikipedia',
            'bloomberg_lite',
            'tfidf_keywords',
            # 'website_title',
        ],
    }
}

TOTAL_ESTIMATED_MATCHES_THLD = 1

# default closeness score for related entities from wiki's "subsidiaries" field
WIKI_SUBSIDIARIES_CLOSENESS_SCORE = 0.9

# Threshold value:
# AAL -> Aalesunds FK, score=25
GOOGLE_KNOWLEDGE_KEY = 'AIzaSyAEppNSiI2L5NIXvGCRB7P9USFJ0O5eND4'

COMPANY_TERMINATORS = ['INC', 'LLC', 'LTD', 'Limited', 'Corp', 'Corporation', 'Incorporated', 'Company',
                       'PLC', 'AB', 'Aktiengesellschaft', 'AG']

# how many intersections of entities' names in bing results should occur in order for these entities to match
BING_MATCH_THLD = 25

# ---- Singletons ----

DB_POOL_SIZE = 5
DB_POOL_MAX_OVERFLOW = 10
db_engine = sqlalchemy.create_engine(SQL_CONNECTION_STRING, pool_size=DB_POOL_SIZE, max_overflow=DB_POOL_MAX_OVERFLOW)
BING_THRESHOLD = 3


# , echo=True)


# def check_pool(*args):
#     """ Emits warning if pool limit is nearly reached """
#     pool = db_engine.pool

#     pool_size = DB_POOL_SIZE
#     in_pool = pool.checkedin()
#     in_use = pool.checkedout()
#     overflow = pool.overflow()

#     if DB_POOL_MAX_OVERFLOW - in_use <= 1:
#         logger.warning('\n'.join([
#             f'Reached db connections limit:',
#             f'Pool size: {pool_size + DB_POOL_MAX_OVERFLOW} ({pool_size} + {DB_POOL_MAX_OVERFLOW} overflow)',
#             f'Active: {in_pool + in_use} ({in_pool} in pool + {in_use} in use)',
#             f'Overflow: {overflow}',
#         ]))


# sqlalchemy.event.listen(db_engine, 'checkout', check_pool)
