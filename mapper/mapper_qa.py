from packages.utils import qa
from .mapper import map_input


qa(map_input, fields=[
    'Company Name',
    'Aliases',
    'Confidence',
    'Confidence Level',
    'Alternative Company Matches',
    'Ticker',
    'Exchange',
    'Ticker Company Name',
])
