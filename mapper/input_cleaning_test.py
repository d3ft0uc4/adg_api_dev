from .input_cleaning import TorchCleaner


answers = [
    # raw input                                                 minimal valuable string
    ('E-I Interior Desig',                                      'E-I Interior Desig'),
    ('Clarks Botanicals Smoothing Marine Cream',                'Clarks Botanicals'),
    ('BROADWAY FLORIST HOME 503-2885537 OR',                    'BROADWAY FLORIST'),
    ('MERCHANTS CAFE & SALOON',                                 'MERCHANTS CAFE'),
    ('Link Consulting',                                         'Link Consulting'),
    ('Compania Fantastica If You Will Maxi Dress in S',         'Compania Fantastica'),
    ('SP * NO JUMPER',                                          'NO JUMPER'),
    ('BAI LI NAIL',                                             'BAI LI NAIL'),
    ('COUNTRY WINE & SPIRITS',                                  'COUNTRY WINE & SPIRITS'),
    ('HOTELS.COM150328917070',                                  'HOTELS.COM'),
    ('VIDA CN6',                                                'VIDA CN6'),
]


def test_torch():
    cleaner = TorchCleaner()

    for raw_input, answer in answers:
        answer = answer.lower()
        for cleaned, _ in cleaner(raw_input):
            assert answer.lower() in cleaned
