
from packages.utils import similar
from mapper.mapper import map_input, generate_response


# bananarepublic.com
# garmin.bg
# kay.com

def merchant_mapper_test():
    answers = {
        'Superior Silica Sands': (
            ['Superior Silica Sands LLC', 'Emerge Energy Services'],
            'EMES',
        ),
        'WaterFurnace Renewable Energy, Inc.': (
            ['WaterFurnace Renewable Energy, Inc.', 'Nibe Industrier'],
            None,
        ),
        'AmeriPride Services': (
            ['AmeriPride Services', 'Aramark'],
            'ARMK',
        )
    }

    for raw_input, (company_names, ticker) in answers.items():
        res = generate_response(map_input(raw_input, mode='merchant'))
        assert any(similar(res['Company Name'], company_name) for company_name in company_names)
        assert res['Ticker'] == ticker
