# """
# © 2018 Alternative Data Group. All Rights Reserved.

# This module helps to compare different entity retrieval methods.

# CLI:
#     python -m mapper.qa.compare_entity_recognition <path/to/raw_inputs.txt>
# """
# from domain_tools.ssl import SSLCertificateRetrieval
# from domain_tools.whois import WhoisInfoRetrieval

# from domain_tools.clean import get_domain
# from joblib import Parallel, delayed

# from packages.utils import qa
# from mapper.mapper import run_tools


# def get_winners(url: str) -> dict:
#     """ Given url, return winners for all known entity recognition systems """
#     domain = get_domain(url)
#     return run_tools(domain, tools=[])


# qa(get_winners, fields=['bane', 'fwfm', 'whois', 'ssl'])
raise NotImplementedError()
