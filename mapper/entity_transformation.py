from typing import List
from packages.utils import NeverFail

from bane.bane import Bane
from find_winner.fw_fm import FWFM

from mapper.entity import Entity
import mapper.entity_recognition as er


@NeverFail(fallback_value=[])
def bane(entity: Entity) -> List[Entity]:
    if entity.sources[-1] in [er.bane.__name__, er.fwfm.__name__, er.company_website.__name__]:
        return []

    results = Bane().get_winner(entity.value)
    return [Entity(
        value=res['winner'],
        score=res['norm_score'],
     ) for res in results['winners']]


@NeverFail(fallback_value=[])
def fwfm(entity: Entity) -> List[Entity]:
    if entity.sources[-1] in [er.fwfm.__name__, er.company_website.__name__, er.title.__name__]:
        return []

    results = FWFM().get_winner(entity.value)
    return [Entity(
        value=res['winner'],
        score=res['norm_score'],
        target=res['target'],
     ) for res in results['winners']]
