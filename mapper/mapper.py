"""
© 2018 Alternative Data Group. All Rights Reserved.

Mapper tool, version 2.
Maps unstructured string (transaction string / url) to structured company info.

CLI:
    python -m mapper.mapper --mode="domain|merchant" "input1.com" "input2.com"
"""
import logging
import os
import re
from argparse import ArgumentParser
from collections import defaultdict
from concurrent.futures import as_completed
from datetime import datetime
from itertools import product, chain
from operator import itemgetter
from typing import Optional, List, Tuple, Callable
from uuid import uuid4

import networkx as nx
import tldextract
from networkx.classes.function import non_edges

from bing.bing_web_core import BingWebCore
from domain_tools.clean import get_domain
from domain_tools.utils import get
from mapper import entity_recognition as recognizers
from mapper import input_cleaning as cleaners
from mapper import settings
# from domain_tools.company_page_detection import in_blacklist
from mapper.brand_detection import detect_brand
from mapper.entity import Entity
from mapper.entity_recognition import wikipedia
from mapper.response import generate_response
from mapper.settings import CONF_LEVEL_LOW
from mapper.tuning.graph_scores import calculate_graph_score
from packages.utils import pretty, with_timer, similar, Timer, remove_dupes, partition, \
    LocalsThreadPoolExecutor, LOCAL, apply_configuration, NeverFail, is_abbrev, remove_empty_values, compare, \
    closeness
from unofficializing.unofficializing import make_unofficial_name as uo

# from packages.targets import TARGETS

logger = logging.getLogger(f'adg.{__name__}')


class ComparisonGraph(nx.DiGraph):
    """ Graph which selects winning nodes based on edges and scores """

    def serialize(self, for_client: bool = False):
        relations_graph = self.copy()

        if for_client:
            # remove non-company nodes
            to_delete = [
                node for node, data in relations_graph.nodes.items()
                if data['entity'].get('kind', 'company') != 'company'
            ]
            for node in to_delete:
                relations_graph.remove_node(node)

        # leave only "parent" relations
        edges = list(relations_graph.edges(data=True))
        for src, dst, data in edges:
            if data['relation'] != 'parent':
                relations_graph.remove_edge(src, dst)

        if not nx.is_directed_acyclic_graph(relations_graph):
            logger.error(f'Unable to serialize cyclic graph')
            return [self.serialize_entity(entity) for entity in relations_graph.entities]

        root_entities = [
            entity for entity in relations_graph.entities
            if not list(relations_graph.out_edges(entity.id))
        ]
        root_entities.sort(key=lambda entity: entity['source'])

        results = [self.serialize_entity(entity, for_client) for entity in root_entities]
        # results.sort(key=lambda entity: len(entity.get('children', [])), reverse=True)

        return results

    def serialize_entity(self, entity: Entity, for_client: bool = False) -> dict:
        """
        Converts internal Entity object to nice human-readable dict for clients.
        """
        result = {}
        for key, value in entity.__dict__.items():
            # process key
            if key.startswith('_'):
                continue
            elif key == 'value':
                key = 'name'
            elif key == 'comparison':
                key = 'aliases'

            if for_client and key not in ['name', 'aliases', 'source', 'score']:
                continue

            # process value
            if isinstance(value, dict):
                value = remove_empty_values(value)

            if not value:
                continue

            if isinstance(value, Entity):
                value = value.id
            elif isinstance(value, float):
                value = round(value, 3)

            result[key] = value
        #
        # names = self.get('comparison', {}).get('names', [])
        # names = remove_dupes(names)
        # names = remove_value(names, self.value)
        # if names:
        #     result['aliases'] = names
        #
        # websites = self.get('comparison', {}).get('websites', [])
        # if websites:
        #     result['websites'] = websites

        children = [
            self.serialize_entity(self.nodes[src]['entity'], for_client)
            for src, _, data
            in self.in_edges(entity.id, data=True)
            if data['relation'] == 'parent'
        ]
        children.sort(key=lambda entity: len(entity.get('children', [])), reverse=True)

        if children:
            result['children'] = children

        return result

    @property
    def entities(self) -> List[Entity]:
        """ Returns all entities of graph """
        return list(map(itemgetter('entity'), self.nodes.values()))

    @classmethod
    def compare_entities_by_names(cls, entity1: Entity, entity2: Entity) -> bool:
        """ Checks whether two entities refer to the same company """
        # params = {}
        # if any(entity.get('kind', 'company') != 'company' for entity in [entity1, entity2]):
        #     # use strict comparison if any of entities is a brand
        #     params = {'thld': 1}

        match = any(
            (compare(*pair) or is_abbrev(*pair))
            for pair in product(entity1.comparison['names'], entity2.comparison['names'])
        )

        if match:
            match1 = re.match(rf'^.+({"|".join(settings.COMPANY_TERMINATORS)})\b.*', entity1.value, flags=re.IGNORECASE)
            form1 = match1[1].lower() if match1 else None

            match2 = re.match(rf'^.+({"|".join(settings.COMPANY_TERMINATORS)})\b.*', entity2.value, flags=re.IGNORECASE)
            form2 = match2[1].lower() if match2 else None

            if form1 and form2 and not (form1 in form2 or form2 in form1):
                logger.debug(f'Company forms differ: {entity1} and {entity2}')
                # TODO: company form comparison requires some advanced techniques;
                # for example, "ESCO corporation" and "ESCO Group LLC" should match,
                # even that they have different forms;
                # we need to check websites first, and if they match then groups dont matter

                # entity1.setdefault('not_related', []).append(entity2.id)
                # entity2.setdefault('not_related', []).append(entity1.id)
                # return False

        return match

    @staticmethod
    def get_all_domains(domain: str) -> List[str]:
        """
        Given any domain, returns list of all sub-domains:

        Input: ec-2.aws.amazon.com
        Output: ['ec-2.aws.amazon.com', 'aws.amazon.com', 'amazon.com']
        """
        parts = domain.split('.')
        return ['.'.join(parts[i:]) for i in range(len(parts)-1)]

    @classmethod
    def compare_entities_by_websites(cls, entity1: Entity, entity2: Entity) -> bool:
        return bool(set(entity1.comparison['websites']).intersection(set(entity2.comparison['websites'])))

    @classmethod
    def compare_entities_by_names_and_websites(cls, entity1: Entity, entity2: Entity) -> bool:
        for e1, e2 in [(entity1, entity2), (entity2, entity1)]:
            e1_domains = [tldextract.extract(website).domain for website in e1.comparison['websites']]
            if any(domain == name for domain, name in product(e1_domains, e2.comparison['names'])):
                return True

        return False

    #
    # TODO
    # @classmethod
    # def compare_entities_by_name_and_website(cls, entity1: Entity, entity2: Entity) -> bool:
    #     ...

    @staticmethod
    def bing_row_to_text(bing_row: dict) -> str:
        """ Convert bing row to plain text where keywords can be searched """
        return ' '.join([
            bing_row['name'].lower(),
            bing_row['snippet'].lower(),
            bing_row['url'].lower(),
            bing_row.get('about', [{}])[0].get('name', ''),
        ])

    @classmethod
    def compare_entities_by_bing(cls, entity1: Entity, entity2: Entity, bing_rows: list) -> bool:
        for entity in [entity1, entity2]:
            if not hasattr(entity, 'comparison'):
                entity.comparison = {}

            if 'in_bing' not in entity.comparison:
                # all possible values of this entity
                values = map(str.lower, list(entity.comparison['names']) + list(entity.comparison['websites']))

                # select only values with enough length
                values = list(filter(lambda val: len(val) >= 4, values))

                # select positions of bing rows where any of values is met
                entity.comparison['in_bing'] = defaultdict(list)
                for i, row in enumerate(bing_rows):
                    for value in set(values):
                        if i not in entity.comparison['in_bing'][value] and \
                           re.search(r'\b' + re.escape(value) + r'\b', cls.bing_row_to_text(row)):
                            entity.comparison['in_bing'][value].append(i)

        for (value1, occurrences1), (value2, occurrences2) in product(
            entity1.comparison['in_bing'].items(),
            entity2.comparison['in_bing'].items(),
        ):
            intersection = set(occurrences1).intersection(set(occurrences2))

            if value1 in value2 or value2 in value1:
                # here we need to count occurrences when value1 and value2 are not substrings of each other
                substring, superstring = (value1, value2) if value1 in value2 else (value2, value1)
                logger.debug(f'Value "{substring}" is substring of "{superstring}", \
                    performing additional bing intersection check')
                intersection = [
                    i for i in intersection
                    if re.search(
                        r'\b' + substring + r'\b',
                        re.sub(
                            r'\b' + re.escape(superstring) + r'\b',
                            '',
                            cls.bing_row_to_text(bing_rows[i]),
                            flags=re.IGNORECASE,
                        ),
                        flags=re.IGNORECASE,
                    )
                ]
                if not intersection:
                    logger.debug(f'Found no intersection of values "{substring}" and "{superstring}"')

            if len(intersection) >= LOCAL.settings['BING_MATCH_THLD']:
                logger.debug(f'Found bing match for "{value1}" and "{value2}" \
                    (num of intersections: {len(intersection)}')
                return True

        return False

    def build_edges(self, allow_same_sources: bool = False, bing_response: dict = {}):
        """
        Build edges between nodes that match.

        Args:
            allow_same_sources: whether to allow creating edges between nodes that have the same "source" value
            bing_response: results of query to bing - BingWebCore().query(cleaned_string) - which
                will be used to improve nodes matching
        """
        # PATCH: if `wikipedia` and `company_website[wikipedia]` are the same wiki pages ->
        # company_website retrieved correct domain
        entities_wikipedia = [entity for entity in self.entities if entity.source == 'wikipedia'
                              and entity.get('wiki_page')]
        entities_company_website_wikipedia = [entity for entity in self.entities
                                              if entity.source == 'company_website[wikipedia]'
                                              and entity.get('wiki_page')]

        for entity_wiki, entity_company_website in product(entities_wikipedia, entities_company_website_wikipedia):
            if entity_wiki.wiki_page == entity_company_website.wiki_page:
                if not entity_company_website.get('websites'):
                    entity_company_website.websites = []

                entity_company_website.websites.append(entity_company_website.url)

        # build all internal comparison information
        for entity in self.entities:
            entity.comparison = {
                'names': remove_dupes(map(
                    # don't allow empty values after cc & uo
                    lambda value: uo(value) or value,
                    [entity.value] + entity.get('aliases', [])
                )),
                'websites': remove_dupes(chain.from_iterable(
                    self.get_all_domains(get_domain(url)) for url in entity.get('websites', [])
                )),
            }

            # filterfalse(
            #     in_blacklist,
            #     entity.get('websites', []),
            # ),

        # first, generate edges candidates
        edge_candidates = non_edges(self)
        if not allow_same_sources:
            # we don't compare nodes from the same source
            edge_candidates = filter(
                lambda edge: self.nodes[edge[0]]['entity'].source != self.nodes[edge[1]]['entity'].source,
                edge_candidates,
            )

        # materialize edges if their nodes match
        for node1, node2 in edge_candidates:
            entity1 = self.nodes[node1]['entity']
            entity2 = self.nodes[node2]['entity']

            match = \
                self.compare_entities_by_websites(entity1, entity2) or \
                self.compare_entities_by_names(entity1, entity2) or \
                self.compare_entities_by_names_and_websites(entity1, entity2)  # or \
            # self.compare_entities_by_bing(entity1, entity2, bing_response.get('webPages', {}).get('value', []))

            if match:
                self.add_edge(node1, node2, relation='similar')
                self.add_edge(node2, node1, relation='similar')
            #    continue
            #
            # # try to find entity names in other entity's wiki page
            # for e1, e2 in [(entity1, entity2), (entity2, entity1)]:
            #     e1_wiki_page = e1.get('wiki_page')
            #     if not e1_wiki_page:
            #         continue
            #
            #     e1_wiki_text = e1_wiki_page.get_article_text()
            #     e2_names = [e2.value] + e2.comparison['names']
            #
            #     if re.search(r'\b(' + '|'.join(map(re.escape, e2_names)) + r')\b', e1_wiki_text, flags=re.IGNORECASE):
            #         logger.debug(f'Entity {e2.id} ("{e2.value}") was mentioned by '
            #                      f'{e1.id} ("{e1.value}", {e1.wiki_page}) -> match')
            #         self.add_edge(e2.id, e1.id, relation='mentioned_by')

        # if we know that some nodes are not related but they appear in same group -
        # disconnect one of them from the group
        # first, get pairs of "not related" nodes
        not_related = set(chain.from_iterable(
            [tuple(sorted([entity.id, not_related])) for not_related in entity.get('not_related', [])]
            for entity in map(itemgetter('entity'), self.nodes.values())
        ))
        if not not_related:
            return

        # then, we remove relations
        for group in list(nx.weakly_connected_components(self)):
            for id1, id2 in not_related:
                if id1 not in group or id2 not in group:
                    continue

                id1_edges = list(self.out_edges(id1, data=True)) + list(self.in_edges(id1, data=True))
                id2_edges = list(self.out_edges(id2, data=True)) + list(self.in_edges(id2, data=True))

                # websites from all nodes except conflicting ones
                domains = list(map(get_domain, chain.from_iterable(
                    self.nodes[node]['entity'].get('websites', [])
                    for node in group
                    if node not in [id1, id2]
                )))
                id1_score = sum(
                    domains.count(get_domain(website)) for website in self.nodes[id1]['entity'].get('websites', []))
                id2_score = sum(
                    domains.count(get_domain(website)) for website in self.nodes[id2]['entity'].get('websites', []))

                # TODO: add choice by TICKER

                if id1_score == id2_score:
                    id1_score = max(similar(
                        self.nodes[edge[0]]['entity'].value,
                        self.nodes[edge[1]]['entity'].value,
                        thld=0.5
                    ) for edge in id1_edges)
                    id2_score = max(similar(
                        self.nodes[edge[0]]['entity'].value,
                        self.nodes[edge[1]]['entity'].value,
                        thld=0.5
                    ) for edge in id2_edges)

                if id1_score == id2_score:
                    logger.warning(f'Ambiguous "not related" choice using similarity')

                if id1_score >= id2_score:
                    group.remove(id2)
                    self.remove_edges_from(id2_edges)

                else:
                    group.remove(id1)
                    self.remove_edges_from(id1_edges)

    def get_groups(self, mode: str) -> List[dict]:
        """
        Return groups of matching entities and corresponding scores; groups with lowest score first.

        Args:
            mode - 'merchant' or 'domain'
        """
        # find groups; each group is a set of nodes (entities) which all matched
        subgraphs = [self.subgraph(nodes).copy() for nodes in nx.weakly_connected_components(self)]
        groups = [{'graph': subgraph} for subgraph in subgraphs]

        for group in groups:
            group['score'] = calculate_graph_score(group['graph'], mode=mode)

        groups.sort(key=itemgetter('score'))

        # try searching parent of brand if there's any brand (only for best group)
        if groups and groups[-1]['score'] >= LOCAL.settings['CONF_LEVEL_LOW']:
            group = groups[-1]
            entities = group['graph'].entities
            if not any(entity.kind == 'company' for entity in entities):
                value = entities[0].value
                logger.debug(f'Trying to retrieve parent of unknown type "{value}"')

                parent_graph = wikipedia(value)
                group['graph'] = nx.compose(group['graph'], parent_graph)
                for node in parent_graph.nodes.values():
                    node['entity'].source = 'transformation[parent]'
                    node['entity']['comparison'] = {
                        'names': [node['entity'].value],
                        'websites': [],
                    }  # TODO: fix this

        return groups


class Mapper:
    """
    Class for running raw input through different tools and selecting winners together with metadata.

    Internally uses graph where:
        - entities are nodes
        - matches between entities are edges with corresponding weights
    """

    Cleaner: Callable[[str], List[Tuple[str, Optional[float]]]]
    """
    Any function that converts raw input string into "clean" inputs for recognition tools:
    Raw input -> Clean input
    """

    Recognizer: Callable[[str], nx.DiGraph]
    """
    Any function that converts input string to list of entities with scores and additional info.
    If some of retrieved entities refer to the same thing (are aliases), return them in the same sublist.
    Clean input -> [Entity1, [Entity2-1, Entity2-2], Entity3, ...]

    Entity attrs (example):
        'value': 'Recognized company name',  <- required
        'score': 0.85,  <- required
        # any additional information:
        'target': 'company',
        'wiki_page': WikiPage(url='company.com'),
        ...
    }
    """

    def __init__(self, mode: str):
        self.mode = mode
        self.config = LOCAL.settings['MAPPER_CONFIG'][mode]

    @staticmethod
    def split_groups(groups) -> Tuple[List[dict], List[dict]]:
        """
        Split groups into winning and losing groups.

        All groups are sorted ascending (lowest score first)
        """
        return partition(lambda group: group['score'] >= LOCAL.settings['CONF_LEVEL_LOW'], groups)

    # ------------ High-level methods ------------
    @with_timer
    # @NeverFail(fallback_value=[])
    def get_attempts(self, value: str) -> List[dict]:
        """
        Return winner groups and their scores using comparison graph.

        Args:
            value: cleaned input string

        Returns: attempts, i.e. raw debug data: [{
                ...,
                'losers': ...,
                'winners': [{'score': ..., 'entities': [Entity1, ...]}, ...],
            }]
         """

        cleaned_inputs = []

        if LOCAL.settings.get('INPUT_TYPE', '') == 'company name':
            selected_cleaners = ['Cleaner']
        else:
            selected_cleaners = self.config['cleaners']

        for cleaner in [getattr(cleaners, name)() for name in selected_cleaners]:
            for cleaned, score in cleaner(value):
                cleaned_inputs.append((cleaner, cleaned.strip(), score))

        cleaned = remove_dupes(cleaned_inputs, key=itemgetter(1))

        # * each cleaners result is independent, thus run in parallel
        # * cleaners priority matters, so use "map
        if not cleaned:
            attempts = []

        elif settings.PARALLEL and settings.CLEANERS_PARALLEL:
            pool = LocalsThreadPoolExecutor(
                max_workers=len(cleaned),
                thread_name_prefix='AttemptsThreadExecutor',
            )
            attempts = list(pool.map(lambda args: self._get_winner(*args), cleaned))

            pool.shutdown(wait=False)

        else:
            attempts = list(map(lambda args: self._get_winner(*args), cleaned))

        return attempts

    def _time_tool(self, tool, cleaned):
        with Timer(tool.__name__) as timer:
            return tool(cleaned), round(timer.elapsed, 3)

    def _get_winner(self, cleaner, value, score):
        logger.debug(f'Cleaned input: "{value}", score: {score}')

        res = dict()
        res['cleaner'] = cleaner
        res['cleaned'] = value
        res['cleaned_score'] = score

        if not value:
            return res

        # retrieve & use totalEstimatedMatches
        num_results = BingWebCore().query(value).get('webPages', {}).get('totalEstimatedMatches', 0)
        res['totalEstimatedMatches'] = num_results
        if num_results < LOCAL.settings['TOTAL_ESTIMATED_MATCHES_THLD']:
            # bad input
            logger.debug(f'Bad cleaned value, discarding: {value}')
            return res

        # 1) Build comparison graph
        graph = ComparisonGraph()

        # 2) Run domain through several entity recognition tools
        tools = [getattr(recognizers, name) for name in self.config['recognizers']]
        res['er_times'] = {}
        with LocalsThreadPoolExecutor(max_workers=len(tools) if settings.PARALLEL else 1) as pool:
            futures_map = {pool.submit(self._time_tool, tool, value): tool for tool in tools}
            for future in as_completed(futures_map):
                tool = futures_map[future]
                try:
                    subgraph, res['er_times'][tool.__name__] = future.result()
                except Exception:
                    subgraph = nx.DiGraph()
                logger.debug(f'Tool {tool.__name__} completed, processing subgraph')

                # some adjustments
                for data in subgraph.nodes.values():
                    entity = data['entity']

                    subsource = entity.get('source')

                    # TODO: fix this hack
                    entity.source = re.sub(r'4products$', '', tool.__name__)
                    entity.source = re.sub(r'bane2', 'bane', tool.__name__)

                    if subsource:
                        entity.source += f'[{subsource}]'

                graph = nx.compose(graph, subgraph)

        logger.debug('Updating graph edges')
        graph.build_edges(bing_response=BingWebCore().query(value))
        groups = graph.get_groups(mode=self.mode)

        # if cleaner has some score, use it to adjust groups' scores
        if score and score < 1:
            for group in groups:
                group['_score'] = group['score']
                group['score'] *= score

        res['losers'], res['winners'] = self.split_groups(groups)

        # if multiple groups win, use `params` to find the most preferred one
        value_uo = uo(value)
        for group in res['winners']:
            group['params'] = {
                'close_to_cleaned': max(closeness(uo(entity.value), value_uo) for entity in group['graph'].entities),
                'quantized_score': (group['score'] + 0.001) // 0.1 * 0.1,  # 0.99 == 0.95,
                'has_companies': any(entity.get('kind', 'company') == 'company' for entity in group['graph'].entities),
                'num_entities': len(group['graph']),
            }
        res['winners'].sort(key=lambda group: tuple(group['params'].values()))

        # when user inputs company name, we *should* return something
        if not res['winners'] and res['losers'] and LOCAL.settings.get('INPUT_TYPE') == 'company name':
            for i in range(len(res['losers'])):
                group = res['losers'][i]
                if any(similar(value, entity.value, thld=0.85) for entity in group['graph'].entities):
                    group['score'] += CONF_LEVEL_LOW
                    res['winners'] = [group]
                    res['losers'] = res['losers'][:i] + res['losers'][i+1:]
                    break

        return res


@with_timer
@NeverFail(
    fallback_value=lambda raw_input, mode: {'original_input': raw_input, 'mode': mode, 'start_time': datetime.now()}
)
def map_input(raw_input: str, mode: str) -> dict:
    """ Maps url to a dict of structured company information """

    if not hasattr(LOCAL, 'run_uid'):  # bc merchant mapper may call domain submapper
        LOCAL.run_uid = str(uuid4())
        LOCAL.raw_input = raw_input

    res = {
        'original_input': raw_input,
        'mode': mode,
        'start_time': datetime.now(),
    }

    if mode == 'domain':
        # convert url to domain
        res['domain_original'] = get_domain(raw_input)
        response = get(res['domain_original'])

        # # check whether domain is live
        # res['is_alive'] = response is not None
        # res['is_parked'] = is_parked(res['domain_original'])
        # if res['is_parked']:
        #     return res

        res['domain'] = get_domain(response.url) if response else res['domain_original']
        value = res['domain']

    elif mode == 'merchant':
        value = raw_input

    elif mode == 'product':
        value = raw_input

        #
        # from mapper.input_cleaning import ProductTokensCleaner
        # from domain_tools.company_page_detection import get_website
        #
        # value = raw_input
        #
        # cleaned = ProductTokensCleaner()(value)[0][0]
        # res['cleaned'] = cleaned
        # if not cleaned:
        #     return res
        #
        # website = (get_website(
        #     cleaned,
        #     aggressive=LOCAL.settings['TEMP_PRODUCTS_MAPPER_WEBSITE_AGGRESSIVE'],
        #     query_string='{} official website',
        # ) or {}).get('url')
        # if not website:
        #     return res
        #
        # domain = get_domain(website)
        #
        # return {**res, **map_input(domain, mode='domain'), **res}  # double **res for correct order of keys

    else:
        raise ValueError(f'Unknown mode: {mode}')

    res['attempts'] = Mapper(mode=mode).get_attempts(value)

    if mode == 'product' and res['attempts'] and not res['attempts'][-1].get('winners', []):
        brand = detect_brand(value)
        if brand:
            res['attempts'].extend(Mapper(mode='merchant').get_attempts(f'{brand} brand'))

    return res


@with_timer
def map_inputs_to_response(
    raw_inputs: List[str],
    mode: str,
    debug: bool = False,
    output_graph: bool = False,
) -> List[dict]:
    """ Maps multiple raw inputs to valid API response """
    if not raw_inputs:
        return []

    logger.debug(f'{"-" * 16} Running {mode} mapper {"-" * 16}')
    logger.debug(f'Inputs: {raw_inputs}')

    with LocalsThreadPoolExecutor(
            max_workers=len(raw_inputs) if settings.PARALLEL else 1,
            thread_name_prefix='MapInputsPool',
    ) as pool:
        futures = [pool.submit(
            lambda rawin:
            generate_response(
                map_input(rawin, mode=mode),
                fields=[
                    'Original Input',
                    'Company Name',
                    'Confidence',
                    'Confidence Level',
                    'Aliases',
                    'Alternative Company Matches',
                    'Related Entities',
                    'Ticker',
                    'Exchange',
                    'Majority Owner',
                    'FIGI',
                    'Websites',
                ] + (
                    ['Possible Names'] if mode == 'product' else []
                ) + (
                    ['Companies Retrieved'] if output_graph else []
                ) + (
                    ['_DEBUG'] if debug else []
                ),
            ),
            rawin
        ) for rawin in raw_inputs]
    results = [future.result() for future in futures]

    return results


if __name__ == '__main__':
    from pyinstrument import Profiler
    from tempfile import gettempdir
    import subprocess
    import json

    parser = ArgumentParser()
    parser.add_argument('--mode', type=str, default='domain')
    parser.add_argument('--output-graph', action='store_true', default=False)
    parser.add_argument('--profile', required=False, action='store_true', default=False)
    parser.add_argument('--settings', type=json.loads, default={})
    parser.add_argument('raw_inputs', type=str, nargs='+')
    args = parser.parse_args()

    profiler = Profiler() if args.profile else None
    if profiler:
        logger.debug('Running profiler')
        profiler.start()

    apply_configuration(args.settings)
    result = map_inputs_to_response(args.raw_inputs, mode=args.mode, debug=True, output_graph=args.output_graph)

    if profiler:
        profiler.stop()

        out_file = os.path.join(gettempdir(), f'profile_{args.raw_inputs[0]}.html')
        with open(out_file, 'w') as f:
            f.write(profiler.output_html())
            logger.debug(f'Wrote profiling info to "{out_file}"')

        subprocess.call(('xdg-open', out_file))

    else:
        print(pretty(result))
