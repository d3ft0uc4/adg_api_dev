"""
© 2018 Alternative Data Group. All Rights Reserved.

Module for calculating precision/recall of mappers.

CLI:
    python -m mapper.precisions.mapper_merchant <path_to_csv>
    # CSV file should contain "Raw Input", "Company Name" and "Ticker" columns.
"""

from mapper.mapper import map_input, generate_response
from mapper.response import NO_MATCH, DOMAIN_UNREACHABLE
from packages.utils import get_precision_recall


def wrapper(inp: str) -> dict:
    res = generate_response(map_input(inp, mode='merchant'))
    if res['Company Name'] in [NO_MATCH, DOMAIN_UNREACHABLE]:
        res['Company Name'] = None

    return [res]


get_precision_recall(wrapper)
