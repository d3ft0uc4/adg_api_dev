from packages.utils import get_precision_recall
from mapper.entity_recognition import fwfm
from mapper.input_cleaning import cc_clean


def wrapper(raw_str: str) -> dict:
    clean_str = cc_clean(raw_str)[0][0]
    results = fwfm(clean_str)
    print(results)
    return [{'Company Name': value['value']} for value in results]


get_precision_recall(wrapper)
