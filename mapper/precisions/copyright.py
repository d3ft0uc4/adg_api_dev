from packages.utils import get_precision_recall
from domain_tools.copyright_extraction import get_copyright


def wrapper(domain: str) -> dict:
    copyrights = get_copyright(domain)
    return [{'Company Name': value} for value in copyrights]


get_precision_recall(wrapper)
