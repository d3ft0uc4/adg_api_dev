from packages.utils import get_precision_recall
from mapper.entity_recognition import bane


def wrapper(domain: str) -> dict:
    results = bane(domain)
    print(results)
    return [{'Company Name': value['value']} for value in results]


get_precision_recall(wrapper)
