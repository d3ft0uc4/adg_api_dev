from packages.utils import get_precision_recall
from domain_tools.ssl import SSLCertificateRetrieval


def wrapper(domain: str) -> dict:
    return [{'Company Name': SSLCertificateRetrieval.get_organization(domain)}]


get_precision_recall(wrapper)
