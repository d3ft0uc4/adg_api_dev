from packages.utils import get_precision_recall
from domain_tools.whois import WhoisInfoRetrieval


def wrapper(domain: str) -> dict:
    return [{'Company Name': WhoisInfoRetrieval.get_organization(domain)}]


get_precision_recall(wrapper)
