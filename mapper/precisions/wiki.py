from packages.utils import get_precision_recall
from wiki_finder.wiki_finder2 import WikiPage


def wrapper(domain: str) -> dict:
    page = WikiPage.from_str(domain)
    if not page:
        return {'Company Name': None, 'Ticker': None}

    company = page.infobox.get('Title')

    ticker = None
    for page in page.iter_owners(include_self=True):
        ticker, _ = page.ticker
        if ticker:
            break

    return [{'Company Name': company, 'Ticker': ticker}]  # TODO: add parent pages as well


get_precision_recall(wrapper)
