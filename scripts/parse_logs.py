import json
from collections import Counter

from joblib import Parallel, delayed

import mapper.logs
import itertools

from bing.bing_web_core import BingWebCore
from domain_tools.clean import get_domain


def get_cleaned___domains():
    try:
        cleaned___domains = json.load(open("/home/ishsonya/workspace/adg_api_dev/my_scratches/cleaned_domains.json"))
    except FileNotFoundError:
        resp = mapper.logs.load_logs(golden_only=True)

        cleaned = itertools.chain(*[set(y['cleaned'] for y in x['snapshot']['attempts']) for x in resp])

        bing = BingWebCore()

        def get_urls(s):
            resp_list = bing.query(s).get('webPages', {}).get('value', [])
            urls = list(set(get_domain(x['url']) for x in resp_list))
            return s, urls

        cleaned___domains = dict(Parallel(n_jobs=3)(delayed(get_urls)(s) for s in cleaned))

        json.dump(cleaned___domains,
                  open("/home/ishsonya/workspace/adg_api_dev/my_scratches/cleaned_domains.json", 'w+'))
    return cleaned___domains

def get_url___count():
    cleaned___domains = json.load(open("/home/ishsonya/workspace/adg_api_dev/my_scratches/cleaned_domains.json"))
    counts = Counter(itertools.chain(*cleaned___domains.values()))
    json.dump(counts, open('/home/ishsonya/workspace/adg_api_dev/my_scratches/domain___count.json', 'w+'))
    return counts

def get_bad_names():
    counts = json.load(open('/home/ishsonya/workspace/adg_api_dev/my_scratches/domain___count.json'))
    values = sorted(counts.items(), reverse=True, key=lambda x: x[1])

if __name__ == '__main__':
    counts = json.load(open('/home/ishsonya/workspace/adg_api_dev/my_scratches/domain___count.json'))
    # org_names = [x for x in counts if '.org' in x]
    # gov_names = [x for x in counts if '.gov' in x]
    # bad_names = [x for x in counts if counts[x] > 25] now picked by hand
    wiki_names = [x for x in counts if 'wiki' in x]
    # all = list(set(itertools.chain(gov_names, wiki_names, bad_names)))
    print(*wiki_names, sep='\n')
    print(len(wiki_names))

