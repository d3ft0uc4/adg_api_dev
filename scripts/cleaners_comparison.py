"""
Script to compare cleaners results on golden set
"""
import csv
from operator import itemgetter
from itertools import chain
from packages.utils import read_list
from os import path

from mapper.settings import db_engine
from mapper.input_cleaning import CcCleaner, TokensCleaner

ROOT = path.dirname(path.dirname(path.abspath(__file__)))

data = db_engine.execute(
    """
    SELECT raw_input, min_valuable_string
    FROM golden_set_domain_merchant
    WHERE mode = "merchant"
    ORDER BY raw_input
    """
).fetchall()
data = [dict(d) for d in data]

for rawin in set(read_list(path.join(ROOT, 'scripts', 'data', 'client_inputs.txt'))):
    data.append({
        'raw_input': rawin,
    })

data.sort(key=lambda d: d['raw_input'])

# ---- limit --------|
data = data[:200]  # |
# -------------------|

CLEANERS = [CcCleaner, TokensCleaner]

FIELDS = ['raw_input', 'min_valuable_string'] + list(chain.from_iterable([
    cleaner.__name__,
    f'{cleaner.__name__}_correct',
    f'{cleaner.__name__}_redundancy',
] for cleaner in CLEANERS))

for i, row in enumerate(data):
    print(i, '/', len(data), ' - ', row['raw_input'])
    min_str = (row.get('min_valuable_string') or '').lower()

    for cleaner in CLEANERS:
        name = cleaner.__name__
        result = cleaner()(row['raw_input'])[0][0].lower()

        row[name] = result

        if min_str:
            row[f'{name}_correct'] = min_str in result
            row[f'{name}_redundancy'] = len(result) - len(min_str)

pth = '/tmp/cleaners_comparison.csv'
with open(pth, 'w') as f:
    writer = csv.DictWriter(f, fieldnames=FIELDS)
    writer.writeheader()
    writer.writerows(data)

    total = {}
    for cleaner in CLEANERS:
        name = cleaner.__name__
        total[f'{name}_correct'] = sum(filter(None, map(lambda d: d.get(f'{name}_correct'), data)))
        total[f'{name}_redundancy'] = sum(filter(None, map(lambda d: d.get(f'{name}_redundancy'), data)))
    writer.writerow(total)

print(f'Wrote results to {pth}')
