from packages.utils import qa
from domain_tools.company_page_detection import *

if __name__ == '__main__':
    def func(domain):
        if not domain.strip():
            return ''
        return is_main_page(domain)


    qa(lambda domain: {'is_main_page': func(domain)}, fields=['is_main_page'])
