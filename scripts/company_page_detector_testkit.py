import logging
import os
from copy import copy
from datetime import datetime

import pandas
from adblockparser import AdblockRules
from joblib import Parallel, delayed

from bing.bing_web_core import BingWebCore
from domain_tools.abparser import lookup
from domain_tools.clean import get_domain
from domain_tools.company_page_detection import is_company_page, get_website, in_blacklist, AdDetector
from domain_tools.utils import get
from packages.utils import Timer, with_timer, read_list
import re2

from domain_tools.company_page_detection import ClassAdDetector, get_all_attributes

logger = logging.getLogger()


class ChainForker:
    def __init__(self, configs):
        self.chains = configs.get('chains', [[]])
        if isinstance(self.chains, list):
            self.chains = enumerate(self.chains)
        if isinstance(self.chains, dict):
            self.chains = self.chains.items()

    @classmethod
    def setparams(cls, *args, **kwargs):
        return cls(*args, **kwargs)

    def _clean(self, data: dict):
        return [data]

    def _reduce(self, outputs):
        return {'outputs': outputs}

    def _map(self, data: dict, name, chain: list):
        logger.debug(f'IN {self.__class__.__name__}: {name} {data}')
        x = copy(data)
        try:
            chain = [x[0].setparams(x[1]) for x in chain]
            for mapper in chain:
                x = mapper.map(x)
        except Exception as e:
            logger.exception(e)
        logger.debug(f'OUT {self.__class__.__name__}: {name} {data}')
        x['fork_name'] = name
        return x

    def map(self, data: dict):
        x = self._clean(data)
        outputs = Parallel(n_jobs=1, prefer='threads')(
            [delayed(self._map)(x, name, chain) for name, chain in self.chains])
        data.update(self._reduce(outputs))
        return data


class Mapper:
    def __init__(self, configs: dict):
        self.log = configs.get('log', False)

    @classmethod
    def setparams(cls, *args, **kwargs):
        return cls(*args, **kwargs)

    @classmethod
    def _clean(cls, data: dict):
        return [data]

    def _map(self, *args, **kwargs) -> dict:
        raise NotImplementedError

    def map(self, data: dict) -> dict:
        args = self._clean(data)
        if self.log:
            logger.debug(f'IN {self.__class__.__name__}: {args}')
        d = self._map(*args)
        if self.log:
            logger.debug(f'OUT {self.__class__.__name__}: {d}')
        data.update(d)
        return data


class CollectionHandler:
    def __init__(self, configs):
        self.chain = [x[0].setparams(x[1]) for x in configs.get('chain', [])]
        self.threads_nr = configs.get('threads_nr', 5)
        self.log = configs.get('log', False)

    @classmethod
    def setparams(cls, configs):
        return cls(configs)

    @classmethod
    def _clean(cls, data: dict):
        return [data]

    def _serialize(self, *args, **kwargs):
        raise NotImplementedError

    def _reduce(self, outputs):
        raise NotImplementedError

    def _map(self, data: dict) -> dict:
        if self.log:
            logger.debug(f'IN {self.__class__.__name__}: {data}')
        x = copy(data)
        try:
            for mapper in self.chain:
                x = mapper.map(x)
        except Exception as e:
            logger.exception(e)
        if self.log:
            logger.debug(f'OUT {self.__class__.__name__}: {x}')
        return x

    def map(self, data: dict) -> dict:
        args = self._clean(data)
        inputs = self._serialize(*args)
        outputs = Parallel(n_jobs=self.threads_nr, prefer='threads')(
            [delayed(self._map)(input) for input in inputs])

        data.update(self._reduce(outputs))
        return data


class DatasetHandler(CollectionHandler):
    def __init__(self, configs={}):
        super(DatasetHandler, self).__init__(configs)
        self.chain = [x[0].setparams(x[1]) for x in configs.get('chain', [])]
        self.dataset_path = configs.get('dataset_path', None)
        self.df = configs.get('df')
        self.name = configs.get('name', 'experiment')
        self.path = configs.get('path', '')
        self.compare_func = configs.get('compare', self._compare)
        self.threads_nr = configs.get('threads_nr', 10)

    @staticmethod
    def _compare(ans, correct):
        return ans == correct

    def get_dataset(self):
        if isinstance(self.df, pandas.DataFrame):
            pass
        else:
            self.df = pandas.read_csv(self.dataset_path)
        return self.df.to_dict('records')

    def _serialize(self, *args, **kwargs):
        return self.get_dataset()

    def _reduce(self, outputs):
        print(outputs[0].keys())
        outs = []
        for output in outputs:
            d = {}
            for a in output:
                if a == 'bing_outputs':
                    continue
                d[a] = output[a]
            for o in output['bing_outputs']:
                x = copy(o)
                x.update(d)
                outs.append(x)

        print(outs)
        pandas.DataFrame.from_records(outs).to_csv(os.path.join(self.path, f'{self.name}_{datetime.now()}.csv'),
                                                   index=False)

        return {'outputs': outputs}


def is_self_referrence(final_domain, link):
    if link is None:
        return None
    link_domain = get_domain(link)
    if link_domain in final_domain or final_domain in link_domain:
        return True
    return False


easylist = AdblockRules([])


def is_ad(is_selfref, link, rules=easylist):
    if is_selfref:
        return False
    return rules.should_block(link)


# def get_all_attributes(soup, attributes=['href', 'src']):
#     links = []
#     for v in attributes:
#         links.extend([x[v] for x in soup.find_all(attrs={v: True})])
#     links_hrefs = []
#     for link, source in links:
#         if isinstance(link, list):
#             for x in link:
#                 links_hrefs.append(x)
#         else:
#             links_hrefs.append(link)
#     return links_hrefs(soup, attributes=['href', 'src']):
#     links = []
#     for v in attributes:
#         links.extend([x[v] for x in soup.find_all(attrs={v: True})])
#     links_hrefs = []
#     for link, source in links:
#         if isinstance(link, list):
#             for x in link:
#                 links_hrefs.append(x)
#         else:
#             links_hrefs.append(link)
#     return links_hrefs


class SelfRefMapper(Mapper):
    def _clean(cls, data: dict):
        return [data['final_domain'], data['link']]

    def _map(self, final_domain, link):
        return {'selfref': is_self_referrence(final_domain, link)}


class EasylistMapper(Mapper):
    def __init__(self, configs):
        options = ['third-party', 'elemhide', 'script', 'ping']
        self.easylist = AdblockRules(configs.get('rules_list', []), options)
        self.name = configs.get('name', 'easylist')
        self.conds = {x: True for x in options}

    def _clean(cls, data: dict):
        return [data.get('selfref', False), data['link']]

    def _map(self, selfref, link):
        is_ad(selfref, link, self.easylist)
        rule = None
        if is_ad:
            for r in self.easylist.rules:
                if r.match_url(link, self.conds):
                    rule = r.rule_text
                    break
        return {'is_ad': is_ad, 'name': self.name, 'rule': rule}


class AddetectorMultiplier(CollectionHandler):
    LINK_ATTRS = ['href', 'src']

    def __init__(self, configs):
        super(AddetectorMultiplier, self).__init__(configs)
        self.debug = configs.get('debug')
        self.path = configs.get('path')

    def _clean(cls, data: dict):
        return [data['response'], data.get('is_in_blacklist', False)]

    def _serialize(self, response, blacklisted):
        if blacklisted or response is None:
            return []
        links = get_all_attributes(response.soup, self.LINK_ATTRS)
        final_domain = get_domain(response.url)

        inputs = []
        for link in links:
            inputs.append({'link': link, 'final_domain': final_domain})
        return inputs

    def _reduce(self, outputs):
        if len(outputs) == 0:
            return {}
        fork_names = outputs[0].get('fork_names', [''])
        df = pandas.DataFrame.from_records(outputs)
        if len(outputs) > 0 and self.debug:
            df.to_csv(os.path.join(self.path, outputs[0]['final_domain'].replace('.', '_') + '.csv'), index=False)
        ans = {}
        for fork_name in fork_names:
            df = df.loc[df[f'{fork_name}_is_ad'.strip('_')]].drop_duplicates(['link'])
            count = df['link'].drop_duplicates().size
            ans[f'ad_count_{fork_name}'.strip('_')] = count
            ans[f'elems_adblock_{fork_name}'.strip('_')] = df['link'].values
            ans[f'rules_adblock_{fork_name}'.strip('_')] = df[f'{fork_name}_rule'.strip('_')].values

        return ans


class BlockClassMultipler(CollectionHandler):
    LINK_ATTRS = ['class']

    def __init__(self, configs):
        super(BlockClassMultipler, self).__init__(configs)
        self.debug = configs.get('debug')
        self.path = configs.get('path', '')
        self.path = os.path.join(self.path, 'cls')
        os.makedirs(self.path, exist_ok=True)

    def _clean(cls, data: dict):
        return [data['response'], data.get('is_in_blacklist', False)]

    def _serialize(self, response, blacklisted):
        if blacklisted or response is None:
            return []
        links = get_all_attributes(response.soup, self.LINK_ATTRS)
        final_domain = get_domain(response.url)
        inputs = []
        for link in links:
            inputs.append({'class': link, 'final_domain': final_domain})
        return inputs

    def _reduce(self, outputs):
        if len(outputs) == 0:
            return {'ad_count': 0}
        df = pandas.DataFrame.from_records(outputs)
        if len(outputs) > 0 and self.debug:
            df.to_csv(os.path.join(self.path, outputs[0]['final_domain'].replace('.', '_') + '.csv'), index=False)
        df = df.loc[df['is_ad']].drop_duplicates(['class'])
        count = df['class'].drop_duplicates().size
        return {'class_ad_count': count, 'elems_class': df['class'].values, 'rules_class': df['class'].values}


class BlockClassMapper(Mapper):
    def __init__(self, configs):
        self.rules = configs.get('rules', [])
        self.rules = set(self.rules)

    def _clean(cls, data: dict):
        return [data.get('class', '@@@@@')]

    def _map(self, cls):
        return {'is_ad': cls in self.rules}


class ResponseMapper(Mapper):
    def _clean(cls, data: dict):
        return [data['url']]

    def _map(self, url):
        r = get(url)
        if r is None:
            r = get(url, use_proxy=True)
        return {'response': r}


class CompanyWebsiteMapper(Mapper):
    def _clean(cls, data: dict):
        return [data['cleaned']]

    def _map(self, cleaned):
        return {'company_website': get_website(cleaned)}


def compare_urls(u1, u2):
    if not isinstance(u1, str) and not isinstance(u2, str):
        return True
    if not isinstance(u1, str) or not isinstance(u2, str):
        return False
    dm1 = get_domain(u1)
    dm2 = get_domain(u2)
    return dm1 in dm2 or dm2 in dm1


class MatchMapper(Mapper):
    def _clean(cls, data: dict):
        return [data['company_website'], data['prev_result']]

    def _map(self, company_website, prev_result):
        if not pandas.isna(company_website) and not pandas.isna(prev_result):
            return {'match': True}
        elif not pandas.isna(company_website) or not pandas.isna(prev_result):
            return {'match': False}
        else:
            return {'match': compare_urls(company_website, prev_result)}


class WikiMatch(Mapper):
    def _clean(cls, data: dict):
        return [data['company_website'], data['wiki_website']]

    def _map(self, company_website, wiki_websites_string):
        if not isinstance(company_website, str) and not isinstance(wiki_websites_string, str):
            return {'is_in_wiki': True}
        elif not isinstance(company_website, str) or not isinstance(wiki_websites_string, str):
            return {'is_in_wiki': False}
        else:
            wiki_ws = [x.strip() for x in wiki_websites_string.split('\n')]
            return {'is_in_wiki': any(compare_urls(x, company_website) for x in wiki_ws)}


class IsCompanyMapper(Mapper):
    def _clean(cls, data: dict):
        return [data['url']]

    def _map(self, url):
        return {'is_company_result': is_company_page(url)}


class IsCompanyPage(Mapper):
    def _clean(cls, data: dict):
        return [data.get('ad_count'), data['blacklisted']]

    def _map(self, ad_count, blacklisted):
        if blacklisted:
            return {'is_company': False}
        if ad_count:
            return {'is_company': False}
        return {'is_company': True}


class BingMapper(Mapper):
    bing = BingWebCore()

    def _clean(cls, data: dict):
        return [data['cleaned']]

    def _map(self, cleaned):
        if not isinstance(cleaned, str):
            return {'bing_response': []}
        bing_response = self.bing.query(cleaned)
        results = bing_response.get('webPages', {}).get('value', [])
        return {'bing_response': results[:3]}


class BingMult(CollectionHandler):
    def _clean(cls, data: dict):
        return [data['bing_response']]

    def _serialize(self, bing_response):
        # return [{n: v for n, v in list(x.items()) + [('rank', i)]} for i, x in enumerate(bing_response)]
        return [{'url': x['url'], 'rank': i, 'bing': x} for i, x in enumerate(bing_response)]

    def _reduce(self, outputs):
        return {'bing_outputs': outputs}


class GeneralMapper(Mapper):
    def __init__(self, configs):
        super(self.__class__, self).__init__(configs)
        self.rules = AdblockRules(
            read_list('/home/ishsonya/workspace/adg_api_dev/domain_tools/data/easylist_general_block.txt'),
            use_re2=True)

    def _clean(cls, data: dict):
        return [data.get('response')]

    def _map(self, response):
        if response is None:
            return {f'{self.__class__.__name__}_ad_count': None}
        attrs = get_all_attributes(response.soup)
        fd = get_domain(response.url)

        for i, l in enumerate(attrs):
            if is_self_referrence(fd, l):
                attrs[i] = None
        attrs = filter(None, attrs)
        blocked = list(filter(None, map(self.rules.should_block, attrs)))
        return {f'{self.__class__.__name__}_blocked': blocked, f'{self.__class__.__name__}_ad_count': len(blocked)}


class ThirdpartyMapper(Mapper):
    def __init__(self, configs):
        super(self.__class__, self).__init__(configs)
        self.rules = AdblockRules(
            read_list('/home/ishsonya/workspace/adg_api_dev/domain_tools/data/easylist_thirdparty.txt'), use_re2=True)

    def _clean(cls, data: dict):
        return [data.get('response')]

    def _map(self, response):
        if response is None:
            return {f'{self.__class__.__name__}_ad_count': None}
        attrs = get_all_attributes(response.soup)
        fd = get_domain(response.url)

        for i, l in enumerate(attrs):
            if is_self_referrence(fd, l):
                attrs[i] = None
        attrs = filter(None, attrs)
        blocked = list(filter(None, map(self.rules.should_block, attrs)))
        return {f'{self.__class__.__name__}_blocked': blocked, f'{self.__class__.__name__}_ad_count': len(blocked)}


class ConversionMapper(Mapper):
    def __init__(self, configs):
        super(self.__class__, self).__init__(configs)
        self.rules = AdblockRules(read_list('/home/ishsonya/workspace/adg_api_dev/domain_tools/data/easyprivacy.txt'),
                                  use_re2=True)

    def _clean(cls, data: dict):
        return [data.get('response')]

    def _map(self, response):
        if response is None:
            return {f'{self.__class__.__name__}_ad_count': None}
        attrs = get_all_attributes(response.soup)
        fd = get_domain(response.url)

        for i, l in enumerate(attrs):
            if is_self_referrence(fd, l):
                attrs[i] = None
        attrs = filter(None, attrs)
        blocked = list(filter(None, map(self.rules.should_block, attrs)))
        return {f'{self.__class__.__name__}_blocked': blocked, f'{self.__class__.__name__}_ad_count': len(blocked)}


class ClassMapper(Mapper):
    def __init__(self, configs):
        super(self.__class__, self).__init__(configs)
        self.rules = set(read_list('/home/ishsonya/workspace/adg_api_dev/domain_tools/data/classes_rules.txt'))

    def _clean(cls, data: dict):
        return [data.get('response')]

    def _map(self, response):
        if response is None:
            return {f'{self.__class__.__name__}_ad_count': None}
        attrs = get_all_attributes(response.soup, ['class'])
        blocked = list(filter(None, map(lambda x: x in self.rules, attrs)))
        return {f'{self.__class__.__name__}_blocked': blocked, f'{self.__class__.__name__}_ad_count': len(blocked)}


class BlacklistMapper(Mapper):
    def _clean(cls, data: dict):
        return [data.get('url')]

    def _map(self, url):
        if not isinstance(url, str):
            return {'blacklisted': None}
        return {'blacklisted': in_blacklist(url)}


class OldAddetectorMapper(Mapper):
    lookup = lookup()

    def _clean(cls, data: dict):
        return [data['response']]

    def _map(self, response):
        if response is None:
            return {f'{self.__class__.__name__}_ad_count': None, f'{self.__class__.__name__}_blocked': []}
        attrs = get_all_attributes(response.soup)
        fd = get_domain(response.url)

        for i, l in enumerate(attrs):
            if is_self_referrence(fd, l):
                attrs[i] = None
        attrs = filter(None, attrs)
        blocked = list(filter(None, map(self.lookup.match_url, attrs)))
        return {f'{self.__class__.__name__}_blocked': blocked, f'{self.__class__.__name__}_ad_count': len(blocked)}


class NewAddetectorMapper(Mapper):
    rules = ClassAdDetector()

    def _clean(cls, data: dict):
        return [data['response']]

    def _map(self, response):
        if response is None:
            return {f'{self.__class__.__name__}_ad_count': None, f'{self.__class__.__name__}_blocked': []}
        attrs = get_all_attributes(response.soup, ['class'])
        blocked = self.rules.get_ads(attrs)
        return {f'{self.__class__.__name__}_blocked': blocked, f'{self.__class__.__name__}_ad_count': len(blocked)}


class NewGetCompanyWs(Mapper):
    def _clean(cls, data: dict):
        return [data['cleaned']]

    def _map(self, cleaned):
        return {'new_company_ws': get_website(cleaned)}


class OldGetCompanyWs(Mapper):
    def _clean(cls, data: dict):
        return [data['cleaned']]

    def _map(self, cleaned):
        bing = [x['url'] for x in BingWebCore().query(cleaned).get('webPages', {}).get('value', [])[:3]]
        for url in bing:
            if in_blacklist(url):
                continue
            r = get(url)
            if r is None:
                continue
            if AdDetector(r.soup, r.request.url, r.url).count_ads() > 0:
                continue
            return {'old_company_ws'}


class MatchMapper(Mapper):
    def _clean(cls, data: dict):
        return [data['new_company_ws'], data['old_company_ws']]

    def _map(self, new_ws, old_ws):
        if not isinstance(new_ws, str) and not isinstance(old_ws, str):
            return {'match': True}
        if not isinstance(new_ws, str) or not isinstance(old_ws, str):
            return {'match': False}
        return {'match': compare_urls(new_ws, old_ws)}


class WikiMapper(Mapper):
    def _clean(cls, data: dict):
        return [data['new_company_ws'], data['wiki_websites']]

    def _map(self, new_ws, wiki_str):
        if not isinstance(new_ws, str) and not isinstance(wiki_str, str):
            return {'is_in_wiki': True}
        if not isinstance(wiki_str, str) or not isinstance(new_ws, str):
            return {'is_in_wiki': False}
        wiki_ws = [x.strip() for x in wiki_str.split('\n')]
        return {'is_in_wiki': any(compare_urls(new_ws, x) for x in wiki_ws)}


if __name__ == '__main__':
    path = '/home/ishsonya/workspace/adg_api_dev/scripts/company_website_er'
    # ws = pandas.read_csv(
    #     '/home/ishsonya/workspace/adg_api_dev/scripts/company_website_er/websites_dataset.csv').drop_duplicates(['url'])
    # ws.to_csv('/home/ishsonya/workspace/adg_api_dev/scripts/company_website_er/websites_dataset.csv', index=False)
    # name = 'is_company_ws_old'
    # DatasetHandler(
    #     {'chain': [(IsCompanyMapper, {})], 'name': name, 'path': path, 'df': ws, 'log': True, 'threads_nr': 1}).map({})

    name = 'company_ws'
    df = pandas.read_csv('/home/ishsonya/workspace/adg_api_dev/scripts/company_website_er/clean_string_dataset.csv')
    # df = df.head()
    chain = [(CompanyWebsiteMapper, {}), (MatchMapper, {}), (WikiMatch, {})]
    c = [(ResponseMapper, {}), (GeneralMapper, {}), (ConversionMapper, {}), (ThirdpartyMapper, {}), (ClassMapper, {}),
         (BlacklistMapper, {}), (OldAddetectorMapper, {})]
    c = [(ResponseMapper, {}), (OldAddetectorMapper, {}), (NewAddetectorMapper, {}), (BlacklistMapper, {})]
    chain = [(BingMapper, {}), (BingMult, {'chain': c})]
    # chain = [(Ne)]
    DatasetHandler(
        {'chain': chain, 'name': name, 'path': path, 'df': df, 'log': True, 'threads_nr': 10}).map({})

