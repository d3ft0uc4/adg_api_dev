import os
from datetime import timedelta
import pickle
from pprint import pprint
from time import time

import pandas

import mapper.logs

"""Scripts to check logs."""


def filter_single_node_winners(resp):
    """check resp.snapshot.winner
    find winner's connected subgraph in winners
    check if it has only one node in it"""
    filtered_logs = []
    max_subgraphs = []
    for log in resp:
        winner = log.get('snapshot', {}).get('winner', {})
        if winner is None:
            continue
        max_subgraph = 1
        for attempt in log.get('snapshot', {}).get('attempts', []):
            for graph in attempt.get('winners', []):
                for node in graph.get('graph', []):
                    if node.get('id') == winner.get('id') and node.get('value_uo') == winner.get('value_uo'):
                        max_subgraph = max(max_subgraph, len(graph.get('graph', [])))

        max_subgraphs.append(max_subgraph)
        if max_subgraph == 1:
            filtered_logs.append(log)
    pandas.Series(max_subgraphs).plot(kind='hist')
    return filtered_logs


def filter_company_website_only_winners():
    """Check if winner from log has only company websites as reference."""
    root = '/home/ishsonya/workspace/adg_api_dev'

    f = os.path.join(root, 'scripts/filtered_only_company_winners.pkl')
    try:
        filtered_logs = pickle.load(open(f, 'wb+'))
    except:
        t = time()
        f2 = os.path.join(root, 'scripts/req.pkl')
        try:
            resp = pickle.load(open(f2, 'rb'))
        except:
            resp = mapper.logs.load_logs(timedelta(days=21))
            pickle.dump(resp, open(f2, 'wb+'))
        print(f'Getting all logs took {time() - t} seconds')
        t = time()
        filtered_logs = []
        for log in resp:
            winner = log.get('snapshot', {}).get('winner', {})
            if winner is None:
                continue

            all_companies = True
            for attempt in log.get('snapshot', {}).get('attempts', []):
                for graph in attempt.get('winners', []):
                    for node in graph.get('graph', []):
                        if winner.get('id') == node.get('id'):
                            for local_node in graph.get('graph', []):
                                if 'company_website' not in local_node.get('source', 'company_website'):
                                    all_companies = False
            if all_companies:
                filtered_logs.append(log)

        pickle.dump(filtered_logs, open(f, 'wb+'))
        print(f'filtering logs took {time() - t} seconds')
    print(len(filtered_logs))
    return filtered_logs


def check_suspicious_logs(filtered_logs, filename):
    """Check and view logs. Save progress in csv in scripts folder.

    s for save
    c for show
    i for inspect
    r for repeat
    h for help

    n for next
    <your string> for real company name
    """
    f = f'/home/ishsonya/workspace/adg_api_dev/scripts/{filename}.csv'
    columns = ['id', 'real_name', 'result', 'raw_input']
    try:
        id___expected_name = pandas.read_csv(f)
        records = []
    except Exception as e:
        print(e)
        id___expected_name = pandas.DataFrame(columns=columns)
        records = []
    for i, log in enumerate(filtered_logs):
        print(f'{i} out of {len(filtered_logs)}')
        id, result, raw = log.get('id'), log.get('company_name'), log.get('raw_input')
        if id in id___expected_name['id'].values:
            continue
        print(f'Result: {result}')
        print(f'Raw input: {raw}')

        r = input()
        if r == 's':
            """s for save"""
            id___expected_name = pandas.concat([id___expected_name, pandas.DataFrame(records, columns=columns)])
            id___expected_name.to_csv(f, index=False)
            records = []
            r = input()
        if r == 'c':
            """c for show"""
            print(id___expected_name.info())
            print(pandas.DataFrame(records, columns=columns))
            print('----')
            r = input()
        if r == 'i':
            """i for inspect"""
            pprint(log)
            print('----')
            r = input()
        if r == 'r':
            """r for repeat"""
            print(f'{i} out of {len(filtered_logs)}')
            print(f'Result: {result}')
            print(f'Raw input: {raw}')
            print('-----')
            r = input()
        if r == 'h':
            print("s for save\nc for show\ni for inspect\nr for repeat\n"
                  "h for help\n\nn for next\n<your string> for real company name")
            print('---')
            r = input()

        if r == 'n':
            """n for next"""
            records.append((id, result, result, raw))
        elif r == 'q':
            """q for quit"""
            break
        else:
            """real name here"""
            real_name = r
            records.append((id, real_name, result, raw))
        print('===============================')
    id___expected_name = pandas.concat([id___expected_name, pandas.DataFrame(records, columns=columns)])
    id___expected_name.to_csv(f, index=False)


if __name__ == '__main__':
    filtered_logs = filter_company_website_only_winners()
    check_suspicious_logs(filtered_logs, 'company_website_winners')
