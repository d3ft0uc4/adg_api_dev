import re

from mapper.settings import COMPANY_TERMINATORS
from packages.targets import TARGETS
from packages.utils import pretty
from tfidf.tfidf import extract_keywords
import csv
from mapper.input_cleaning import TokensCleaner
from ticker_merchant_map.ticker_merchant_map import TickerFinder
from unofficializing.unofficializing import make_unofficial_name as uo
from domain_tools.company_page_detection import get_website


class DamnStupidMapper:
    cleaner = TokensCleaner()

    @staticmethod
    def is_company(name: str) -> bool:
        name_uo = uo(name)

        info = TickerFinder.query_db(name_uo, include_empty_ticker=True)
        if info:
            return True

        targets = TARGETS.get_by_similar_name(name_uo)
        if targets:
            return True

        if re.match(r'.+ (' + '|'.join(map(re.escape, COMPANY_TERMINATORS)) + r')\.?', name, flags=re.IGNORECASE):
            return True

        return False

    @classmethod
    def map(cls, raw_input: str) -> dict:
        res = {
            'raw_input': raw_input,
        }

        cleaned = cls.cleaner(raw_input)[0][0]
        res['cleaned'] = cleaned
        if not cleaned:
            return res

        if cls.is_company(cleaned):
            res['company_name'] = cleaned
            return res

        keywords = extract_keywords(res['cleaned'])
        res['keywords'] = keywords

        website = get_website(cleaned)
        res['website'] = website

        name = keywords[0][0] if keywords else None
        res['company_name'] = name if name and cls.is_company(name) else None

        return res


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('value', nargs='?')
    args = parser.parse_args()

    if args.value:
        print(pretty(DamnStupidMapper.map(args.value)))

    else:
        raw_inputs = [
            'A ONE OIL',
            'abb ltd',
            'ABLE AUTO',
            'academy sports + outdoors',
            'actively learn inc seattle washington',
            'adams resources & energy, inc.',
            'adecco group ag',
            'adidas ag',
            'advance auto parts inc. roanoke virginia united states of america',
            'aep industries inc.',
            'agilent technologies, inc.',
            'allied aviation holding co',
            'altra industrial motion corp.',
            'amazon video on demand amzn.com/billwa su0e6nhd7gw see all tags',
            'ameren corporation',
            'american air liquide holdings inc',
            'american capital, ltd.',
            'american electric power company, inc.',
            'american furniture manufacturing ecru mississippi',
            'ameripride services inc minnetonka minnesota united states of america',
            'APPLE GRANNY SMITH',
            'APPLE HEALTH FO/1011 EL',
            'apple inc cupertino california united states of america',
            'arcalyst® (rilonacept) injection for subcutaneous use tx~~08888~~5465280004457953~~30493~~0~~~~0066',
            'archer-daniels-midland company',
            'armstrong flooring, inc.',
            'ASRock Z97 Extreme4 LGA 1150 Intel Z97 HDMI SATA 6Gb/s USB 3.0 ATX Intel Motherboard',
            'autozone inc. memphis tennessee united states of america',
            'aviva',
            'axs catalyst infinity  brookline   wr16155     617-734-6810',
            'azure  payroll    ***********8240~~08888~~~~46883~~0~~~~0032',
            'B-SIDE',
            'bareburger great neck ny',
            'barrick gold corporation',
            'bathbodyworks',
            'bed bath & beyond stamford 00002-00896 203-323-7714',
            'bellsouth corporation atlanta georgia united states of america',
            'beyond tangy tangerine 05/26 #000962458 purchase                   3636 ranch rd 620  austin        tx',
            'BIGFISHGAMES*253009559',
            'boots 1136',
            'bp moto hilton par',
            'brantano footwear',
            'BROADWAY FLORIST HOME 503-2885537 OR',
            'buckle inc kearney nebraska',
            'buffalo wild wings inc.',
            'burberry group plc',
            'bwx technologies, inc.',
            'canon powershot sx620 hs 20.2mp',
            'carmax business services, llc henrico virginia united states of america',
            'carter\'s #784 000000stamford 15697384 8887829548',
            'cash sainsby spr21',
            'charles & colvard morrisville north carolina united states of america',
            'chevrolet cruze #1161           saint charlesmo',
            'chevron phillips chemical co llc',
            'chopt - uws new york ny',
            'cinemark @  marysville marysville   ks',
            'Clarks Botanicals Smoothing Marine Cream ',
            'clinique new york new york united states of america',
            'cms energy corporation',
            'coleman pmb8110 12 v 2715plano       106         9722200200',
            'compass diversified holdings',
            'comstock mining, inc.',
            'concenter',
            'CONVENE AT 777 SOUTH FIGU',
            'core i7-5650u @ online payment - thank you',
            'COUNTRY WINE & SPIRITS',
            'craft brew alliance, inc.',
            'credit risk monitoring',
            'CROSSROADS TRADING CO-U',
            'crossy road',
            'Crush Yogurt - Youngsvi',
            'cubic corporation',
            'cvs/pharmacy #06048 staten island ny',
            'daktronics, inc.',
            'dana incorporated',
            'Debit Purchase 09/02 card 7364 Wm Superc Wal-mahouma La',
            'deckers brands goleta california united states of america',
            'destination maternity corporation moorestown new jersey united states of america',
            'dillard\'s 795 quail sp   oklahoma cityok',
            'discovery cove orlando florida  7900131492  (214) 792 — 4223',
            'disney store traff manchester',
            'duane reade #14236 new york ny',
            'E-I Interior Desig',
            'eaton electric holdings llc',
            'EBPA LLC',
            'ecommercefuel bozeman montana',
            'EDGX EXCHANGE INC',
            'edw c levy co',
            'egoiste chanel cr~~03051~~~~37417~~0~~14~~0303 tx',
            'el pollo loco inc costa mesa california united states of america',
            'ELLIES DESI KITCHEN INC',
            'EMPLOYERS HR.L',
            'energizer holdings, inc.',
            'entegris, inc.',
            'entergy corporation',
            'epay*poway surg ctr',
            'ergon inc.',
            'esco corporation',
            'evonik corp',
            'exxonmobil 47315353 wayland mi',
            'FAMILY PRACTICE CENTER',
            'federal-mogul',
            'fedex 424106603 memphis tn',
            'ferrellgas #01166*            columbus     ms',
            'filly flair colton south dakota',
            'firstenergy corporation',
            'Fish Pool ASA',
            'fitbit inc san francisco california united states of america',
            'food lion #1442 frederick md',
            'food4less #0313 hawthorne ca',
            'forterra',
            'fortune brands home & security, inc.',
            'FOUR TIMES SQUARE ASSOC LLC',
            'frankley s/w costa',
            'fred\'s @ barneys new y chicago il',
            'freeport-mcmoran, inc.',
            'g shock ga100c-8a 866-5761039  ca',
            'gamestop corp. grapevine texas united states of america',
            'gap 2422',
            'godaddy',
            'graco inc.',
            'graham corporation',
            'grand theft auto v@online store  xxxxx0042 pssignuph tx',
            'gristedes # 511 new york ny',
            'groupon inc chicago illinois united states of america',
            'guinness storehouse ® brewery dublin 910-763-4638 nc',
            'gulf oil 91800671 portland me',
            'harman international industries, incorporated',
            'harsco corporation',
            'Healthy Choice Simply Steamers Creamy Spinach & Tomato ',
            'hertz rent-a-car new york ny',
            'HIDDEN VALLEY GLACIERS',
            'holcim (us) inc',
            'home bargains',
            'homegoods #405 000000405 simi valley ca',
            'HOTELS.COM150328917070',
            'IN *PALMETTO TWIST SHE',
            'international bus mach',
            'inven trust properties corp',
            'jakks pacific, inc. 2951 28th st, santa monica, ca 90405-2961, us',
            'jb poindexter & co inc',
            'jimmyjohns',
            'JINBAI ENTPR CO LTD.',
            'K C S DAWG AND BURGER',
            'kia motors corp.',
            'kinder morgan, inc.',
            'kirby corporation',
            'kohl\'s #0374 levittown ny',
            'la fitness 09492558100 ca',
            'leapfrog enterprises emeryville california united states of america',
            'LIGHTING ELEGANCE',
            'lighting science west warwick rhode island',
            'Like You Mean It Wedge',
            'Link Consulting',
            'louisiana-pacific corporation',
            'M & J FOOD STORE INC 3',
            'mamas & papas',
            'MARKET DISTRICT #4036',
            'MAXICARE HEALTHCARE',
            'mcdonalds rest.',
            'meguiar\'s, inc. 17991 mitchell s, irvine, ca 92614-6015, us',
            'mercury insurance group los angeles california united states of america',
            'mercury systems inc',
            'merlin entertainments plcâ',
            'MERNM',
            'merrill bob',
            'met food staten island ny',
            'minerals technologies inc.',
            'MIYAKO OF DADELAND JAPA',
            'mobile fun limited brooklyn new york',
            'mueller industries, inc.',
            'multi packaging solutions international limited',
            'NEW IMAGE HAIR CLINIC I',
            'new york stock exchange new york new york united states of america',
            'Nike $10 Gift Card (Email Delivery)',
            'NNT CHUBBYS OF HENR232713',
            'nyc taxi 5k82 new york ny',
            'occidental petroleum corporation',
            'old navy us 5899 stamford 807682 family clothing',
            'omega protein corporation',
            'ONE PRINCE DELI & TOBA',
            'ONLINE TRANSFER TO MARKHAM M CHECKING XXXXXX0850 REF #IB05KWRVFD ON 12/24/18',
            'onyx neurovascular  w baptist rdcolorado sprico~~08888~~5438058098520942~~26613~~0~~~~0066',
            'oracle',
            'oral-b sensitive clean@ebay e-check depot type: online pmt id: citictpco: home depotach ecc webach trace 091409683258939',
            'oshkosh b\'gosh atlanta georgia united states of america',
            'otrs gmbh cupertino california',
            'OVERTURE, LLC',
            'oxygen4energy llc highland park illinois',
            'park electrochemical corp',
            'PAYPAL *DISCOUNTPOO',
            'PAYPAL *SFINTLLTD',
            'performance designed products llc burbank california',
            'performance food group henrico virginia united states of america',
            'peripheral interventions angiojet epayment    ach pmt    ***********3182~~08888~~~~94100~~0~~~~0065',
            'PET FOOD EXPRESS 1007',
            'POS Debit - Visa Check Card 0000 - TACO BELL #0000 JACKSONVILL',
            'POS PURCHASE          POSXXXXXXXX  XXXXXXX     EXPRESS LLC              PITTSBURGH   PA',
            'primark',
            'PUERTO DEL REY SEA SHO',
            'Puffs Plus Lotion Facial Tissues 3 boxes (124 count each) - 1 ea ',
            'purchase - domestic london tesco stores 5282',
            'PURCHASE AUTHORIZED ON 08/28 CAR2GO 877-488-4224 DC S468241036285974 CARD 5361',
            'purchase: air jordans 2018',
            'QuickPay with Zelle payment from MIN KIM WFCT05NKB8MX',
            'RACEWAY 6712 39067129 ATHENS AL',
            'rainbow shops',
            'ralphs #0645 el segundo ca',
            'ramco gershenson properties trust',
            'ray-ban wayfarer rb2140f 901s 52 asian fit sunglasses #1161           saint charlesmo',
            'raytek inc po box 1820, santa cruz, ca 95061-1820, us',
            'READY READING GLASSES',
            'REAL DE OAXACA',
            'regal beloit corporation',
            'regal entertainment group',
            'remington ep7035  29036100  xxxxx0043 washington dc',
            'RICKER\'S #48 CARMEL IN',
            'ROCKIN B FEED & SUPPLY',
            'roku, inc los gatos california united states of america',
            'rolls-royce motor cars limited',
            'Roses',
            'RTH COAST CO-OP - EURE',
            'Ryska Posten AB',
            'sacat marks & spen',
            'sadasds',
            'safeway inc.',
            'sainsburys s/mkts',
            'sandflea  m/o e/c 800-793-3167 ny',
            'sandisk corporation milpitas california united states of america',
            'save-a-lot ltd saint louis missouri united states of america',
            'schlumberger n.v.',
            'SENSODYNE CMPLT TP ',
            'sephora',
            'shake shack 1116         washington   dc',
            'sharis of clackamas 17 clackamas or',
            'shell oil 57442465605   gro 000146031   8313737381',
            'shopbop.com 877-746-726 11316269dm9 clothing',
            'sky king fireworks melbourne florida',
            'smart & final inc los angeles california united states of america',
            'source energy services',
            'southwestairlines',
            'southwestern energy 23              spotsylvania va~~08888~~4552250092549828~~69290~~0~~~~0066',
            'spotify spotify pr',
            'st. jude medical, inc.',
            'stamps.com inc el segundo california united states of america',
            'starbucks #07352 new y new york ny',
            'starbucks westholm',
            'stop & shop llc',
            'STORE 1240',
            'stride rite corp waltham massachusetts united states of america',
            'sullivan\'s steakhouse irving texas united states of america',
            'MEDICARD PHILIPPINES INC',
            'sunoco 0368305901 ridgefield nj',
            'superdry',
            'superior silica sands',
            'suzuki m109r boulevard intruder m1800r b.o.s.s.       denver       co',
            'sysco corporation houston texas united states of america',
            't-mobile usa bellevue washington united states of america',
            'ted baker',
            'TEDESCHI FOOD SHOP #090',
            'Tele2 Sverige AB',
            'tesla gears',
            'tetra technologies, inc.',
            'textbooks.com columbia missouri united states of america',
            'THE BONE & JOING CLI',
            'the fresh grocer',
            'THE STORE EL-168681',
            'thechildrensplace',
            'THIRSTY OTTER',
            'TIM KAISER STUDIOS INC',
            'timberland uk ltd',
            'TIMES SQUARE ENTERTAINMEN',
            'timken',
            'TRAILHEAD COFFEE BAR A',
            'transcanada corporation',
            'tree bachelaur',
            'trimas corporation',
            'TST* REBELLION DC WASHINGTON DC',
            'uber technologies inc 866-576-1039 ca',
            'universal stainless & alloy products, inc.',
            'us department of energy',
            'USA*BARDIN VENDING',
            'VALLEY WIDE COOPERATIV',
            'vcna prestige concrete products inc',
            'veolia environmental services',
            'vereit, inc.',
            'vf outlet inc reading pennsylvania',
            'VIDA CN6',
            'VILLAGE CINEMA',
            'vistaprint nv waltham massachusetts',
            'VOYG TRANSPORT SERVICES INC',
            'waterfurnace renewable energy, inc.',
            'weis markets inc sunbury pennsylvania united states of america',
            'WESTIN MAUI DINING',
            'wilson pro staff rf 85 #0432 san jose ca',
            'wing supply bath pennsylvania',
            'wingstop 537 xxxxx0040 chicago il',
            'winnebago industries, inc.',
            'WINSUPPLY PORTLAND ME C',
            'woodward, inc.',
            'xcel energy inc.',
            'ymca corporate grand rapids mi',
            'zara jacket  321 pta               718-499-2412 ny',
        ]


        file = open('/tmp/scratch.csv', 'w')
        writer = csv.DictWriter(file, fieldnames=DamnStupidMapper.map('old navy').keys())
        writer.writeheader()

        for i, raw_input in enumerate(raw_inputs, start=1):
            print(i, '/', len(raw_inputs))
            writer.writerow(DamnStupidMapper.map(raw_input))
            file.flush()
