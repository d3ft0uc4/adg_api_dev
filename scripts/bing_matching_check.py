from operator import itemgetter
from itertools import combinations
from collections import defaultdict
import json
import csv

from mapper.logs import load_logs
from bing.bing_web_core import BingWebCore
from packages.utils import logger, StrFallbackJSONEncoder

logs = load_logs()
bwc = BingWebCore()


results = []
for i, log in enumerate(logs, start=1):
    print(i, '/', len(logs), ' - ', log['raw_input'])
    snap = log['snapshot']
    for attempt in snap['attempts']:
        cleaned = attempt['cleaned']
        bing_results = bwc.query(cleaned).get('webPages', {}).get('value', [])

        groups = list(map(itemgetter('graph'), attempt.get('losers', []) + attempt.get('winners', [])))
        groups_of_values = []
        for entities in groups:
            group_of_values = []
            for entity in entities:
                group_of_values += entity.get('comparison', {}).get('names', []) + entity.get('comparison', {}).get('websites', [])

            groups_of_values.append(group_of_values)

        occurrences = defaultdict(set)
        for values in groups_of_values:
            if not values:
                continue

            for i, row in enumerate(bing_results):
                name_and_snippet = row['name'].lower() + ' ' + row['snippet'].lower()
                if any(value in name_and_snippet for value in values):
                    occurrences[', '.join(values)].add(i)

        for (values1, occurrences1), (values2, occurrences2) in combinations(occurrences.items(), 2):
            intersection = occurrences1.intersection(occurrences2)
            if intersection:
                logger.debug(f'Intersection of "{values1}" and "{values2}"')
                results.append({
                    'json': json.dumps(log, cls=StrFallbackJSONEncoder),
                    'values1': values1,
                    'values2': values2,
                    'intersection_pos': intersection,
                    'intersection': json.dumps([bing_results[i] for i in intersection]),
                    'bing_response': json.dumps(bing_results),
                })

with open('/tmp/bing_match.csv', 'w') as f:
    writer = csv.DictWriter(f, fieldnames=list(results[0].keys()))
    writer.writeheader()
    writer.writerows(results)
