# import csv
import datetime
import json

from bing.bing_web_core import BingWebCore
from domain_tools.company_page_detection import in_blacklist
from mapper.logs import load_logs
from packages.utils import logger

# from collections import defaultdict
# from itertools import chain

# with open(
#     '/home/wolfie/Dropbox (ADG)/adg_api_dev_share/API/ERs/company website/base/company_website_features_v2.csv',
#     'r',
# ) as f:
#     data = [row for row in csv.DictReader(f)]

data = load_logs(dt=datetime.timedelta(days=24))

data = []
for i, row in enumerate(data, start=1):
    print(f'{i}/{len(data)} - {row["raw_input"]}')
    try:
        attempts = row['snapshot']['attempts']
        for attempt in attempts:
            if attempt.get('winners', []):
                bing_response = BingWebCore().query(attempt['cleaned'])
                bing_results = bing_response.get('webPages', {}).get('value', [])

                data += [
                    {
                        'url': res['url'],
                        'name': res['name'],
                        'snippet': res['snippet'],
                    }
                    for res in bing_results
                    if not in_blacklist(res['url'])
                ]

    except Exception as exc:
        logger.warning(exc)
#
# # generate TFs
# for doc in documents:
#     doc['tokens'] = tokenize(doc['name'] + ' ' + doc['snippet'])
#
#     doc['TF'] = {}
#     for token in set(doc['tokens']):
#         doc['TF'][token] = doc['tokens'].count(token)
#
# # get all words
# words = sorted(set(chain.from_iterable(doc['tokens'] for doc in documents)))
#
# # colculate idfs
# idfs = defaultdict(int)
# for doc in documents:
#     for word in set(doc['tokens']):
#         idfs[word] += 1
#
# words_len = len(words)
# idfs = {word: (words_len / occurs) for word, occurs in idfs.items()}
#
# # calculate TF/IDF scores
# tf_idfs = []
# for doc in documents:
#     doc['TF/IDF'] = {}
#     for word, tf in doc['TF'].items():
#         tf_idf = tf * idfs[word]
#         doc['TF/IDF'][word] = tf_idf
#         tf_idfs.append(tf_idf)

# output to csv table
date = datetime.datetime.now().strftime('%Y.%m.%d.%H.%M')
dir = f'/home/wolfie/Dropbox (ADG)/adg_api_dev_share/API/ERs/company website/tf idf/'

unique_data = [dict(tup) for tup in set(tuple(d.items()) for d in data)]
json.dump(unique_data, open(dir + f'corpus.{date}.src.json', 'w'))


print(f'Wrote to {dir}')
