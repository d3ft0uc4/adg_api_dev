import csv
from os import path

from mapper import entity_recognition
from mapper.settings import DROPBOX
from packages.utils import get_timestamp
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument('er')
args = parser.parse_args()

er = getattr(entity_recognition, args.er)

cleaned = """
A ONE OIL
abb ltd
ABLE AUTO
academy sports + outdoors
actively learn inc
adams resources & energy, inc
adecco group ag
adidas ag
advance auto parts inc
aep industries inc
agilent technologies, inc
allied aviation holding
altra industrial motion corp
amazon video amzn.com
ameren corporation
american air liquide holdings inc
american capital, ltd
american electric power company, inc
american furniture manufacturing
ameripride inc
APPLE GRANNY
APPLE HEALTH FO/1011 EL
apple inc
arcalyst injection subcutaneous use tx~~08888
archer-daniels-midland company
armstrong flooring, inc
Intel Z97 HDMI SATA 6Gb/s USB 3 0 ATX Intel Motherboard
autozone inc
aviva
axs catalyst infinity
azure
B-SIDE
bareburger great neck
barrick gold corporation
bathbodyworks
bed bath & beyond
bellsouth corporation
beyond tangy tangerine ranch rd 620
BIGFISHGAMES
boots
bp moto hilton par
brantano footwear
BROADWAY FLORIST HOME
buckle inc
buffalo wild wings inc
burberry group plc
bwx technologies, inc
canon powershot
carmax business services, llc
carters
sainsby spr21
charles & colvard
chevrolet cruze
chevron phillips chemical co llc
chopt
cinemark
Clarks Botanicals Smoothing Marine Cream
clinique new york
cms energy corporation
coleman pmb
compass diversified holdings
comstock mining, inc
concenter
CONVENE AT
core i7-5650u
COUNTRY WINE & SPIRITS
craft brew alliance, inc
risk monitoring
CROSSROADS TRADING CO-U
crossy road
Crush Yogurt Youngsvi
cubic corporation
cvs/pharmacy
daktronics, inc
dana incorporated
Wm Superc Wal-mahouma
deckers brands
destination maternity corporation
dillards 795 quail
discovery cove
disney store traff
duane reade
E-I Interior Desig
eaton electric holdings llc
EBPA LLC
ecommercefuel bozeman
EDGX EXCHANGE INC
edw c levy
egoiste chanel cr~~03051~~~~37417~~0~~14~~0303
el pollo loco inc
ELLIES DESI KITCHEN INC
EMPLOYERS HR L
energizer holdings, inc
entegris, inc
entergy corporation
epay poway surg ctr
ergon inc
esco corporation
evonik corp
exxonmobil
FAMILY PRACTICE CENTER
federal-mogul
fedex
ferrellgas
filly flair
firstenergy corporation
Fish Pool ASA
fitbit inc
food lion
food4less
forterra
fortune brands home & security, inc
ASSOC LLC
frankley s/w
barneys new y
freeport-mcmoran, inc
g shock -8a
gamestop corp
gap
godaddy
graco inc
graham corporation
grand theft auto v store pssignuph
gristedes
groupon inc
guinness storehouse brewery
gulf oil
harman international industries, incorporated
harsco corporation
Healthy Choice Simply Steamers Creamy & Tomato
hertz rent-a-car
HIDDEN VALLEY GLACIERS
holcim inc
home bargains
homegoods
HOTELS.COM
PALMETTO TWIST SHE
international bus mach
inven trust properties corp
jakks pacific, inc
jb poindexter & co inc
jimmyjohns
JINBAI ENTPR CO LTD
K C S DAWG AND BURGER
kia motors corp
kinder morgan, inc
kirby corporation
kohls
la fitness
leapfrog enterprises
LIGHTING ELEGANCE
lighting science
Like You Mean It Wedge
Link Consulting
louisiana-pacific corporation
M & J FOOD STORE INC
mamas & papas
MARKET DISTRICT
MAXICARE HEALTHCARE
mcdonalds rest
MEDICARD PHILIPPINES INC
meguiars, inc
mercury insurance group
mercury systems inc
merlin entertainments plca
MERNM
merrill bob
met food
minerals technologies inc
MIYAKO OF DADELAND JAPA
fun limited
mueller industries, inc
multi packaging solutions international limited
NEW IMAGE HAIR CLINIC I
new york stock exchange
Nike
NNT CHUBBYS OF HENR
nyc taxi 5k82
occidental petroleum corporation
old navy us
omega protein corporation
ONE PRINCE DELI & TOBA
MARKHAM M
onyx neurovascular w baptist rdcolorado sprico~~08888
oracle
ebay
oshkosh b'gosh
otrs gmbh
OVERTURE, LLC
oxygen4energy llc
park electrochemical corp
DISCOUNTPOO
SFINTLLTD
performance designed products llc
performance food group
peripheral interventions angiojet
PET FOOD EXPRESS
TACO BELL
EXPRESS LLC
primark
PUERTO DEL REY SEA SHO
Puffs Plus Lotion Facial Tissues 3 boxes count each) 1 ea
tesco stores
CAR2GO
air jordans
MIN KIM
RACEWAY
rainbow shops
ralphs
ramco gershenson properties trust
ray-ban wayfarer 901s 52 asian fit sunglasses
raytek inc
READY READING GLASSES
REAL DE OAXACA
regal beloit corporation
regal entertainment group
remington ep7035
RICKERS
ROCKIN B FEED & SUPPLY
roku, inc
rolls-royce motor cars limited
Roses
RTH COAST CO-OP
Ryska Posten
sacat marks & spen
sadasds
safeway inc
sainsburys s/mkts
sandflea m/o e/c
sandisk corporation
save-a-lot ltd
schlumberger n v
SENSODYNE CMPLT TP
sephora
shake shack
sharis of clackamas 17
shell oil
shopbop.com
sky king fireworks
smart & final inc
source energy
southwestairlines
southwestern energy 23
spotify spotify
st  jude medical, inc
stamps.com
starbucks
starbucks westholm
stop & shop llc
STORE
stride rite corp
sullivans steakhouse
sunoco
superdry
superior silica sands
suzuki boulevard intruder b o s s
sysco corporation
t-mobile
ted baker
TEDESCHI FOOD SHOP
Tele2 Sverige
tesla gears
tetra technologies, inc
textbooks.com
THE BONE & JOING CLI
the fresh grocer
THE STORE
thechildrensplace
THIRSTY OTTER
TIM KAISER STUDIOS INC
timberland uk ltd
TIMES ENTERTAINMEN
timken
TRAILHEAD COFFEE BAR A
transcanada corporation
tree bachelaur
trimas corporation
TST  REBELLION
uber technologies inc
universal stainless & alloy products, inc
us department of energy
USA BARDIN VENDING
VALLEY WIDE COOPERATIV
vcna prestige concrete products inc
veolia environmental
vereit, inc
vf outlet inc
VIDA CN6
VILLAGE CINEMA
vistaprint nv
VOYG TRANSPORT INC
waterfurnace renewable energy, inc
weis markets inc
WESTIN MAUI DINING
wilson pro staff rf 85
wing supply bath
wingstop
winnebago industries, inc
WINSUPPLY C
woodward, inc
xcel energy inc
ymca corporate
zara jacket 321 pta
""".split('\n')[1:-1]

file = open(path.join(DROPBOX, 'ERs', args.er, f'qa.{get_timestamp()}.csv'), 'w')
writer = csv.DictWriter(
    file,
    fieldnames=['cleaned', 'results']
)
writer.writeheader()

results = []
for i, inp in enumerate(cleaned, start=1):
    print(i, '/', len(cleaned))

    graph = er(inp)
    entities = [node['entity'] for node in graph.nodes.values()]
    print(entities)

    writer.writerow({
        'cleaned': inp,
        'results': '\n'.join(f'{entity.value} - {entity.score:.2f}' for entity in entities),
    })
    file.flush()
