"""
Script to check how we can improve cleaned strings which have low totalEstimatedMatches values
"""
from itertools import chain
from os import path
from bing.bing_web_core import BingWebCore
from mapper.settings import db_engine, DROPBOX
import csv
from packages.utils import get_timestamp


data = db_engine.execute(
    """
    SELECT DISTINCT
        raw_input,
        json_extract(snapshot, '$.attempts[0].cleaned') as cleaned,
        json_extract(snapshot, '$.attempts[0].totalEstimatedMatches') as total_matches
    FROM api_log
    WHERE mode = "merchant"
    AND json_extract(snapshot, '$.attempts[0].totalEstimatedMatches') < 20000
    ORDER BY datetime DESC
    LIMIT 100
    """
).fetchall()
data = [dict(d) for d in data]

pth = path.join(DROPBOX, 'total estimated matches', f'{get_timestamp()} - total matches under 20k.csv')
file = open(pth, 'w')
writer = csv.DictWriter(file, fieldnames=['raw_input',
                                          'cleaned', 'total_matches', 'bing',
                                          'cleaned2', 'total_matches2', 'bing2'])
writer.writeheader()

for i, row in enumerate(data, start=1):
    print(i, '/', len(data), ' - ', row['raw_input'])

    row['cleaned'] = row['cleaned'][1:-1]
    bing = BingWebCore().query(row['cleaned']).get('webPages', {}).get('value', [])
    total_matches = row['total_matches']

    cleaned2 = row['cleaned'] + ' company'
    bing2_result = BingWebCore().query(cleaned2).get('webPages', {})
    bing2 = bing2_result.get('value', [])
    total_matches2 = bing2_result.get('totalEstimatedMatches', 0)

    writer.writerow({
        **row,
        'bing': '\n'.join(map(lambda row: row['name'] + '\n' + row['url'] + '\n', bing)),
        'cleaned2': cleaned2,
        'bing2': '\n'.join(map(lambda row: row['name'] + '\n' + row['url'] + '\n', bing2)),
        'total_matches2': total_matches2,
    })
    file.flush()

