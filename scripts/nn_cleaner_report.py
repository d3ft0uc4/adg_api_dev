from mapper.logs import load_logs
from packages.utils import pretty


logs = load_logs(golden_only=True)

results = []
for log in logs:
    attempts = log['snapshot']['attempts']
    if len(attempts) >= 2 and 'nn_clean' in attempts[1]['cleaner']:
        winner_src = None
        if attempts[-1].get('winners', []):
            winner_src = attempts[-1]['cleaner']
        results.append({
            'raw input': log['raw_input'],
            'cc cleaned': attempts[0]['cleaned'],
            'nn cleaned': attempts[1]['cleaned'],
            'who gave winner': winner_src,
            'winner': (log['snapshot'].get('winner', {}) or {}).get('value'),
            'is correct winner': log['company_name_matches'],
            'answer': log['correct_company_names'],
        })
print(pretty(filter(lambda res: 'nn' in res['who gave winner'], results)))

print('---- NN ----')
print(pretty(list(filter(lambda res: 'nn' in (res.get('who gave winner') or ''), results))))
