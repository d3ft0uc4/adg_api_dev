"""
Script to check Bing's "totalEstimatedMatches" values
"""
from itertools import chain

from bing.bing_web_core import BingWebCore
from mapper.settings import db_engine
from mapper.mapper import map_inputs_to_response
import csv


data = db_engine.execute(
    """
    SELECT raw_input, min_valuable_string, company_name
    FROM golden_set_domain_merchant
    WHERE mode = "merchant"
    ORDER BY raw_input
    """
).fetchall()
data = [dict(d) for d in data]


bing = BingWebCore()

pth = '/tmp/total_estimated_matches.csv'
file = open(pth, 'w')
writer = csv.DictWriter(file, fieldnames=['raw_input', 'company_name', 'num_results', 'er_winners', 'api_answer'])
writer.writeheader()

for i, row in enumerate(data, start=1):
    print(i, '/', len(data), ' - ', row['raw_input'])

    for inp in [row['raw_input'], row['min_valuable_string']]:
        if not inp:
            continue

        num_results = bing.query(inp).get('webPages', {}).get('totalEstimatedMatches', 0)

        response = map_inputs_to_response([inp], mode='merchant', debug=True)[0]

        attempts = response['_DEBUG']['attempts']

        if attempts[0].get('winners', []):
            attempt_no = 0
        elif len(attempts) > 1:
            attempt_no = 1
        else:
            attempt_no = 0

        groups = attempts[attempt_no].get('losers', []) + attempts[attempt_no].get('winners', [])
        graphs = [group['graph'] for group in groups]
        entities = list(chain.from_iterable(graph.entities for graph in graphs))

        writer.writerow({
            'raw_input': inp,
            'company_name': row['company_name'],
            'num_results': num_results,
            'er_winners': '\n'.join(f'{entity.value} ({entity.source} {entity.score})' for entity in entities),
            'api_answer': response['Company Name'],
        })
        file.flush()

