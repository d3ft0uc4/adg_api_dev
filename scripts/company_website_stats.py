# script to collect all values of "company_website" and how they come
from mapper.logs import load_logs
from datetime import timedelta
import csv
import re
from domain_tools.company_page_detection import get_website

logs = load_logs(dt=timedelta(days=14), mode='merchant')

results = []
total = len(logs)
for i, log in enumerate(logs, start=1):
    print(i, '/', total)
    snap = log['snapshot']
    for attempt in snap['attempts']:
        res = {
            'raw input': snap['original_input'],
            'cleaner': re.match(r'<function (.*?) at .*>', attempt['cleaner'])[1],
            'cleaned': attempt['cleaned'],
        }

        company_website = None
        for group in attempt.get('winners', []) + attempt.get('losers', []):
            for entity in group.get('graph', []):
                if isinstance(entity, str):
                    continue

                company_website = entity.get('company_website')
                if company_website:
                    break

            if company_website:
                break

        res['company_website'] = company_website or get_website(res['cleaned'])
        res['winner'] = None

        results.append(res)

    winner = snap.get('winner', {})
    if isinstance(winner, dict):
        results[-1]['winner'] = winner.get('value')

pth = '/tmp/comp_website_stats.csv'
with open(pth, 'w') as f:
    writer = csv.DictWriter(f, list(results[0].keys()))
    writer.writeheader()
    writer.writerows(results)

print(f'Wrote results to "{pth}"')
