import requests
from joblib import Parallel, delayed
import logging
from packages.utils import Timer, logger
from random import shuffle
from typing import Optional
from operator import itemgetter
import sys
from os import path
from argparse import ArgumentParser
from collections import Counter
import matplotlib.pyplot as plt

from packages.utils import read_list

logger.setLevel(logging.DEBUG)

parser = ArgumentParser()
parser.add_argument('number', type=int, default=16)
parser.add_argument('--repeat', type=int, default=1)
args = parser.parse_args()

inputs = read_list(path.join(path.dirname(path.abspath(__file__)), 'data', 'client_inputs.txt'))[:60]
inputs = list(set(inputs))

def make_request(rawin) -> Optional[float]:
    with Timer(rawin) as timer:
        # response = requests.post('https://api-2445582026130.production.gw.apicast.io/merchant-mapper?X_user_key=55b99f8d0a9db4915ed41a3ea7e04bb8', json=[rawin])
        response = requests.post('http://adg-api-dev.us-east-1.elasticbeanstalk.com/mapper-debug', headers={'X-3scale-proxy-secret-token': 'rkMcYebOqEIRmpFFm0nGIBex7m8pRVtR', 'X-Configuration': '{"CACHE_ERS": false}'}, json=[rawin])
        if not response.ok:
            logger.error(f'Error for {rawin}: {response.status_code}')
            return

        return timer.elapsed
        # logger.debug(f'Result for "{rawin}": {response.json()[0]["Company Name"]}')

times = []
for i in range(args.repeat):
    logger.debug(f'--------- Batch #{i+1} ---------')
    shuffle(inputs)
    to_process = inputs[:args.number]
    times.extend(Parallel(n_jobs=len(to_process), prefer='threads')(delayed(make_request)(rawin) for rawin in to_process))

times = list(map(lambda time: round(time) if time else 0, times))

counter = Counter(times)

plt.xlabel('time')
plt.ylabel('occurrences')
plt.axis(
    [0, max(times), 0, max(counter)]
)
plt.plot(counter.keys(), counter.values(), 'ro')
# plt.plot(wrong_distribution.keys(), wrong_distribution.values(), 'r', label='wrong')
plt.show()
