# script to collect winners from "company_website" ER only
from mapper.logs import load_logs
from datetime import timedelta
import csv
import re
import json
from domain_tools.company_page_detection import get_website
from functools import reduce
from operator import itemgetter

logs = load_logs(dt=timedelta(days=14), mode='merchant')

results = []
total = len(logs)
for i, log in enumerate(logs, start=1):
    print(i, '/', total)
    snap = log['snapshot']
    attempts_winners = filter(None, map(lambda attempt: attempt.get('winners', []), snap['attempts']))
    winning_groups = reduce(lambda winners1, winners2: winners1 + winners2, attempts_winners, [])

    winning_groups = filter(
        lambda group:
            all(source.startswith('company_website') for source in map(itemgetter('source'), group['graph'])),
        winning_groups
    )

    for group in winning_groups:
        results.append({
            'raw_input': log['raw_input'],
            'company_website': next(filter(None, map(lambda entity: entity.get('company_website'), group['graph'])), None),
            'company_name_matches': log.get('company_name_matches', ''),
            'group': json.dumps(group),
        })


pth = '/tmp/winners_from_company_website.csv'
with open(pth, 'w') as f:
    writer = csv.DictWriter(f, list(results[0].keys()))
    writer.writeheader()
    writer.writerows(results)

print(f'Wrote results to "{pth}"')
