import json
import re

# 1) GENERATE CORPUS (skip this if you already have corpus)

from bing.bing_web_core import BingWebCore
from domain_tools.company_page_detection import in_blacklist
from mapper.logs import load_logs
import datetime

# load logs with snapshots, long-running task
data = load_logs(dt=datetime.timedelta(days=24))

data = []
for i, row in enumerate(data, start=1):
    print(f'{i}/{len(data)} - {row["raw_input"]}')
    try:
        # for each successful cleaned string (which produced any winning groups) get bing.query(cleaned)
        attempts = row['snapshot']['attempts']
        for attempt in attempts:
            if attempt.get('winners', []):
                bing_response = BingWebCore().query(attempt['cleaned'])
                bing_results = bing_response.get('webPages', {}).get('value', [])

                # extract url, name and snippet from bing rows (if row's website is not blacklisted)
                data += [
                    {
                        'url': res['url'],
                        'name': res['name'],
                        'snippet': res['snippet'],
                    }
                    for res in bing_results
                    if not in_blacklist(res['url'])
                ]

    except Exception as exc:
        print(exc)

# remove duplicates
unique_data = [dict(tup) for tup in set(tuple(d.items()) for d in data)]

# convert data (list of dicts) to corpus (list of strings)
corpus = [row['name'] + ' ' + row['snippet'] for row in data]   # <-- ["name1 snippet1", "name2 snippet2", ...]

# output corpus to json files
json.dump(corpus, open('Dropbox (ADG)/adg_api_dev_share/API/ERs/company website/tf idf/corpus.json', 'w'))


# 2) APPLY CORPUS TO ANYTHING

# load corpus
corpus = json.load(open('Dropbox (ADG)/adg_api_dev_share/API/ERs/company website/tf idf/corpus.json', 'r'))

VALUE1 = 'amazon company'
VALUE2 = 'amzn web services'

# ---- variant 1: py_stringmatching ----
import py_stringmatching

# this library accepts list of documents, so we need to tokenize each corpus sentence ourselves
documents = [tokenize(sentence) for sentence in corpus]
tfidf1 = py_stringmatching.SoftTfIdf(documents, threshold=0.8)
score1 = tfidf1.get_raw_score(tokenize(VALUE1), tokenize(VALUE2))
print(f'Score1={score1:.2f}')

# ---- variant 2: https://github.com/potatochip/soft_tfidf/blob/master/soft_tfidf.py ----
from soft_tfidf import soft_tfidf

# tfidf2 = soft_tfidf.SoftTfidf(corpus[:10000])  # you may use this one if you don't want to wait forever
tfidf2 = soft_tfidf.SoftTfidf(corpus)  # may take quite long time (corpus is big)
score2 = tfidf2.similarity(VALUE1, VALUE2, threshold=0.8)
print(f'Score2={score2:.2f}')
