import csv
import re
from collections import Counter
from itertools import groupby, combinations, product
from operator import itemgetter

from bing.bing_web_core import BingWebCore
from domain_tools.clean import get_domain
from domain_tools.company_page_detection import in_blacklist, NOT_COMPANY_WEBSITES, NOT_PRODUCT_WEBSITES
from mapper.website_parsers import select_parser, AmazonParser, WalmartParser, SamsClubParser


raw_inputs = """
GUSSETED CARD CASE I
Little Giant® Work Platform
Wincup Foam Container 8 oz White 500 ct (F8)
Great Value Party 18 Oz Cups 100ct
Justice League (2017) (Walmart Exclusive) (Blu-ray + DVD + Digital Copy + Pins)
Line fare (6.74mi 11m 46s)
Basyx VL616 Series Stacking Guest Chair w/Arms
BI100 EDIT PINK PEONY PAC
Revitalizing Supreme Plus Global Anti-Aging Cell Power Creme
TechCrunch Staff
ESPRESSO BEANS 2LBS KS/STARBUCKS COFFEE P360
62mm Pro Series Snap On...
Teenage Mutant Ninja Turtles Boys Footed Sleepers Green M
Sealy 12 Hybrid Queen Mattress
CLAMATO 12/16 OZ SL DOM 22 MO P96
Members Mark Shredded Parmesan Cheese by Argitoni (24 oz.)
Canned Wild Tuna Chili Lime
RSVP! (21+ event)
Rocky Mountain Chocolate Factory® Party Pack Assor
NV OAT/HNY 49/2CT PCH 1.5 2BARS/PCH SL149 T54H3P162
Pirates Booty White Cheddar 0.5 oz 36 ct
Philips hue
DL-METHIONINE 500 MG 150CT TAB BTL
PENDAFLEX FILE FOLDER/LTR MANILLA/1/3-CUT - 150CT.
PEETS COFFEE & TEA GC 5-$20 GIFT CARDS
PEETS DICKASONS 2 LBS WHOLE BEAN BLEND SL90P216
ITALIAN PANINI
1-Way PORT IMP → PIER 11 MOBILE TICKET
’REPLICA’ Jazz Club Travel Spray
2  Dapper Doo (Awesome Haircut & Shampoo)
5.11 Tactical Trainer Belt - 1.5\ Wide
72\X10CHRCOAL FIBERGLAS INSECT SCRN
Club-Mate Drink Original...
Fresh Thyme Mushrooms - Baby Bella 8 Ounce
Great Value Radiatore Macaroni 16 oz
H-E-B Traditional Garlic Bread 8 oz  H-E-B
Hamburger Helper Betty Crocker  Stroganoff
Kale Vates Blue Curled Great Heirloom Vegetable BULK 30000 Seeds By Seed Kingdom
Love Live!School idol festival Harvest Moon Pack: Aqours
Marketside Cherry Almond Gorgonzola Chopped Salad Kit 13.6 oz
Mystery Match Special Offer
Ride (14.63mi 29m 51s)
Spinach Mushroom & Sofrito Soufflé
TAPCON 3/16X2-3/4 PHILLIPS 75 PK.
V8 Splash Watermelon Cherry 64 oz.
Vetiq Maximum Strength Dog Hip & Joint Medicine - Hip and Joint Medicine for Dogs Chew Treat 90 Count
******* PT 2.5
1 1/2IN. ALU. LETTER N BLACK & GOLD
10-Piece Classic Wings Combo (Classic Wings Combos)
10Set X Hario 02 100-Count...
2-Ply Cotton Pinpoint Oxford Snap Tab Collar Dress Shirt
24 Hour Protection
3D Universal Car Seat Cover Breathable PU Leath...
3D USB LED Magical Moon Night ...
Adidas TaylorMade Golf Hat: Flexfit Delta Fitted/Large-X-Large Mineral Blue
Adult 4-Day Go Oahu E-Card
Adult 4-Day Los Angeles Go E-Card
alcove Gillian 8-Pc. Bed Set - Queen
An Evening With Jimmy LaFave
Ancona Slim III 30 Under Cabinet Range Hood | AN-1217
ANGELSOFT*PS TP/2PLY/60CT GPC16560
Bai Infusion Sunrise Variety Pack 18 oz 15-count
BETR TRND DRS #A#
BEZOX Toenail Clippers...
BIFUNIE Yellow Sticky Traps...
Big Buck Hunter Arcade
BLACK PEPPER CRACK 5LB SL DOM 36 MO T10H4
Blackstone Elite 10 Gel Infused Twin Memory Foam Mattress
Boars Head Deli American Cheese Yellow (Regular Sliced)
BOURBON Chocolate in Rice Cake Mochi 8pcs
Boys Uniform Long Sleeve Solid Pique Polo
C-MATE CREAMER-REG/180CT SL DOM 9 MO T30H5 P150
CAULIFLOWER **1/2 CASE**
Chicken Malai Roll
Classic Pro 3D
CLINIQUE Naturally Glossy Mascara Jet Brown
Collective Concepts - Krissa Keyhole Halter Blouse
Copperfit Pro Series Knee Sleeve
Copperfit™ 2-Pack Large/XL Gripper Socks
Cracker Jack Original 1.25 oz 24 ct
Creamed Corn
Despicable Me Minions Cake Topper & Birthday Candle Set
DISCOUNT 26.00 $ OFF
DISCOUNT 51.00 % OFF
Double Black Diamond Packable Down Throw 2-Pack Purple
Drawer of Rubies (Hidden City: Hidden Object Adventure)
eco Kloud 9? Compostable Plate Bagasse Sugarcane 5
EMPOWER RLG WLKER - BLUE
Enfagrow Toddler Next Step Non-GMO Stage 3 Natural
Equal Sweetener Portion Packets 1000 ct
Feather Necklace
FIBER1 OAT & CHOC 36/1.4Z P288 T72H4 SL99
Fireball Cinn Whiskey 50
Fisher-Price® 4-in-1 Sling n Seat Bath Tub in Pink/White
Flat-front Leather Jacket
Foldable Can Cooler - Black
Galaxy J3 Eclipse in Black
GARDETTOS VEND 42/1.75OZ #49448 T16H4 P64
GE 7.4 CuFt Aluminized Alloy Drum Electric Dryer with HE Sensor Dry in White
GINGER ***5Lbs***
Gobbler Skillet Breakfast
Google Chromecast HDMI Streaming Media Player 2-pack Bundle
Green Lantern Comic Fist T-Shirt- 2XLarge
HEAD Mens Grid 2.0 Low...
Hi-Chew Fruit Chews Variety Pack 30 oz.
Honest Kids Organic Juice Drink Variety Pack 6 fl
Hot Brown Honey 20-22 Feb
Hoxton Safe
HP950XL High Yield Ink Cartridge Black 2 pk
Ice Cream Making - Ice Cream Making Wednesday June 27th 10:00-11:30 am
Japan Wazaki M Forged Soft...
JIF-SET FLOOR &WALL PATCH 25LB
Kars Trail Mix Sweet N Salty 2 oz 24-count
Kele by NYX- Unisex Ion Golf Sunglasses
KELL SPECIAL K CRL
Kirkland Signature 2-Ply Bath Tissue 30 Rolls
Kirkland Signature Breakfast Blend Coffee Light R
Kirkland Signature Cashew Clusters 32 oz.
Kirkland Signature Clear Cutlery 360-Count
Kirkland Signature Colombian Supremo Whole Bean 3
Kirkland Signature Extra Virgin Olive Oil 2 L
Kirkland Signature Organic Extra Virgin Olive Oil 2 Liter
Kirkland Signature Pecan Halves 32 oz.
KIRKLAND SIGNATURE SINGLE DOSE LAUNDRY 152 COUNTS
Kirkland Signature Sumatran Whole Bean Coffee 3 lb 2-pack
Kirkland Signature Sunshine Candy Mix 530-count
Kirkland Signature Super Extra Large Peanuts 40 o
Kirkland Signature Supreme Diapers Size 5; 150ct
Kirkland Signature Supreme Diapers Size 5; Quantit
Kirkland Signature Supreme Diapers Size 6; 120ct
Kirkland Signature Whole Almonds 3 lbs
Kirkland Signature Whole Dried Blueberries 20 oz
Kirkland Signature Whole Dried Blueberries 20 oz.
Kirkland Signature Whole Unsalted Cashews 2 lb 8oz
Kirkland Signature Wild Alaskan Pink Salmon 6 oz
KMART ALL : SYW Bonus Pts Earned : 15000 : Sears Home Warranty : SYW Bonus Pts Earned : 48999 :
Krusteaz Cornbread and Muffin Mix 5 lbs
KS 2% LOWFAT MILK 2/1 GAL
KS FUNHOUSE TREATS 92Z PALLET 180
KS NONFAT MILK 2/1 GAL
KS ORG 2%CHOC MLK 24/8.25SL180 P128/NW=119
KS ORG PEANUT BUTR 2/28OZ CREAMY SL360 P480
KS TRAIL MIX W/M&MS 64 OZ C10T6H4 P240 SL150
KS TRAIL MIX W/M&MS 64 OZ SL150 C10T6H4 P240
KS TRAIL MIX W/M&MS 64 OZC10T6H4 P240
LADIES TEAM
Lamaze Womens Nursing Henley Long Sleeve Shirt and Pant Pajamas Set - Black M
Lifetime 52 XL Portable Basketball Hoop
Lindsay Extra Large Black Pitted Olives 6 oz 8-c
Liver Calf Sknd&Dvnd Mr Hc
Mesa 9 Flatweave Hand-Woven Wool Rug
Mobile Preset Double Pack (Amalfi Coast + Ibiza)
Morton Iodized 4 Oz Shakers Salt & Pepper
Mountain House Premium Meal Assortment 32 Servings
Mustela Newborn Arrival...
N LS MESS MAGNET BLACK 4T
NACHO CHEESE SAUCE #10CAN 106 OZ QUE BUENO P288
Nature Made® Prenatal Multi + DHA 150 Liquid Softgels
NJoy Morning Harvest Gourmet Oatmeal Hot Cereal K
Novaform 3? EVENcor GelPlus Gel Memory Foam Mattre
Oh Baby by Motherhood™ Pointelle Scoopneck Sweater - Maternity
Oliso® Pro-1000 Vacuum Food Sealer Starter Kit
One Personalized Story Book
ORG AUSSIE BITES 30OZ UNIVRSL BKRY SL60P240/600
OUTSHINE YOGRT BAR
Panthers @ Predators - 5 Tickets
Plush Slipper Socks Women ...
Portofino Comfort 3-piece Chaise Loungers in Espresso
PRACTICAL MEDITATION: KNOW YOUR POWER
PRO+ Monthly Subscription
Purell Instant Hand Sanitizer Pump Bottle 8oz 12ct
QUNOL MEGA COQ10 100MG
Red Bull Energy Drink 12 fl. oz. 24-count
Renegade POOP EMOJI FIDGET...
Royal Authentic White Basmati Rice 20 lbs.
Runabout Skort Misses Regular
Safety 1st® Baby On Board Sign in Yellow
Schott Zwiesel
Secret Garden: An Inky...
Secret Scent Expressions Gel Ooh-la-la Lavender
Sky Hunter
Sleep Science Ara 13 Split-King Memory Foam Mattr
SMOKY MT MIX
Sound BlasterX Katana...
SPECTRUM MP-8.5X11/10REAM 999705/20#/92BRITE
Starter pack (Harvest Land)
Striped Knit Short
Style Me-#FullyStyled
Sugar Cookie (Frosty & Bakery)
Sunshine Cheez-It Crackers Cheddar 1.5 oz 45 ct
Superfood Air-Whip Hyaluronic Acid Moisture Cream
Texture – Unlimited Magazines
The Lost Room: Disc 1
TheraPure 3” Memory Foam Mattress Topper with Cool Touch Cover Twin Extra Long
Thermal Sleep Henley
Too Good at Goodbyes (Instrumental Version)
Track - Anthem Lights: Pompeii
trunature Grape Seed and Resveratrol 150 Tablets
Uwell Crown Sub ohm Tank Temperature Control Top Refilling Atomizer RTA RBA atty
vitafusion? Womens Multivitamin 220 Gummies
Voss Artesian Water 16.9 fl. oz 24-count
Whidbey Island Things To Do
Wiccan Meditations by Starhawk
Womens Backpack Leather...
Wonder bread hot dog buns
wood iphone 6/ 6 plus case per...
Zipfizz Healthy Energy Drink Mix 30 Tubes Fruit
NEWMANS OWN ORGANIC 100CT KCUPS FY16-18 P96
A&E VTY ASEPTIC 36/6.75 Z100% JUICE VARIETY P=104
MOEN CALDWELL BN ROMAN TUB
CURAD 3GVNYL-XLG/100CT 3P CUR8234/POWDER FREE
Lyft fare (17.83mi 35m 31s)
Lyft fare (2.77mi 8m 36s)
Lyft fare (3.11mi 9m 47s)
Lyft fare (3.79mi 10m 32s)
Lipton Unsweetened Family Black Iced Tea Bags 24 ct
LG 30CuFt | Super-Capacity 3-Door French Door | St
La Croix Lime 12 oz 24-pack
SONICARE DIAMONDCLEAN 6PK
KNOTTS STRAWBERRY 36/2OZ#59637 T15H7P105
Kleenex Anti-Viral Facial Tissue Upright 18 Ct
KS STEAK STRIPS 12 OZ C30T5H3 SL240 P450
6-Month Bundle Subscription
H-E-B Select Ingredients Jumbo Shells 12 oz  H-E-B
Maidenform Ladies Comfort Devotion Bra 2-pack Black & Latte Size 38DD
Georgia-Pacific Preference Bath Tissue Rolls 2-Ply
Cascadian Farm Organic Honey Nut Os Cereal
GATORADE FIERCE GRAP
Franke FRX-02 Triflow Water...
Fitbit Charge HR Wireless Activity & Heart Rate Wristband
Fellowes® Powershred® 60Cs 10-Sheet Cross-Cut Shredder
Earth Friendly Products ECOS® Liquid Laundry Detergent Magnolia & Lily 2-pack
DURACELL 9V-8PK CARDED MN16TB8TC201/ALKAINE/P288
Duracell Coppertop Alkaline C Batteries 14 ct
Doritos Flavored Tortilla Chips Nacho Cheese 10.5 Oz
COSTCO CASH CARD
CORSAIR GAMING VOID PRO RGB W
Cinnabon — Two Classic Rolls
A&H+OXI CLEAN LIQ LAUNDRY 160LDS/250OZ T24H4 P96
Calvin Klein Jeans Mens Osaka Blue Straight Fit Denim 30 X 30
Blendtec Total Blender WildSide & Twister Jars -
AUDIOQUEST PEARL 2M (6.56 FT.) BLACK/WHI
James Bond™ Aston Martin DB5 V39
Arlo Q HD 1080p Wi-Fi IP Monitoring Camera 2-Pack
Abyss Light Up Protective Flash Case For Apple iPhone 6 Plus - Navy
KS MICROWAVE PCORN 44/3.3 P132 T33X4
Machinable Rare Earth Magnetic Bar 1/2\ Wide x 1/2\ Long x 1/8\ Thick
Scotch-Brite Stainless Steel Scrubber 16-count
""".split('\n')[1:-1]

bwc = BingWebCore()

results = []
for i, raw_input in enumerate(raw_inputs, start=1):
    print(i, '/', len(raw_inputs))
    bing_response = bwc.query(raw_input)
    bing_rows = bing_response.get('webPages', {}).get('value', [])[:30]

    # ---- official website ----
    domains = [get_domain(row['url']) for row in bing_rows]
    try:
        website, website_cnt = Counter(domains[:4]).most_common(1)[0]
    except Exception:
        website, website_cnt = '', 0

    if website_cnt >= 2 and not in_blacklist(website, NOT_PRODUCT_WEBSITES + NOT_COMPANY_WEBSITES):
        official_website = website
    else:
        official_website = ' '

    # ---- ebay and amazon - no answer ----
    no_answer = []
    for i, row in enumerate(bing_rows):
        url = row['url']
        if re.match(r'^https://www\.ebay\.com/sch/i\.html\?_nkw=', url, flags=re.IGNORECASE):
            no_answer.append(f'{i}: ebay')
        elif re.match(r'^https://www\.amazon\.com/[\w-]+/s\?k=[\w+]+', url, flags=re.IGNORECASE):
            no_answer.append(f'{i}: amazon')

    # ---- brand ----
    brands = []  # (rank, parser, brand)
    for i, row in enumerate(bing_rows):
        parser = select_parser(row['url'])
        if not parser or parser not in [AmazonParser, WalmartParser, SamsClubParser]:
            continue

        entity = parser.parse(row)
        if entity and entity.get('kind', 'company_name') == 'brand':
            brands.append({
                'rank': i,
                'parser': parser,
                'value': entity.value,
            })

    # ---- ANSWER ----
    answer = None

    # ---- answer from brand ----
    if brands:
        brands.sort(key=lambda brand: brand['parser'].__name__)
        groups = {
            parser_name: [brand for brand in grouper]
            for parser_name, grouper
            in groupby(brands, key=lambda brand: brand['parser'].__name__)
        }

        # if there's the same brand from 2 different parsers - accept it
        for parser1, parser2 in combinations(groups, 2):
            for value1, value2 in product(
                map(lambda brand: brand['value'].lower(), groups[parser1]),
                map(lambda brand: brand['value'].lower(), groups[parser2]),
            ):
                if (value1 in value2) or (value2 in value1):
                    answer = min(value1, value2, key=len)
                    break

            if answer:
                break

        if not answer:
            # if there's same brand from same parser - accept it
            for brands in groups.values():
                values = [brand['value'].lower() for brand in brands if brand['rank'] <= 20]
                if not values:
                    continue

                value, frequency = Counter(values).most_common(1)[0]
                if frequency >= 2:
                    answer = value
                    break

    results.append({
        'raw_input': raw_input,
        'websites': '\n'.join([row['url'] for row in bing_rows][:10]),
        'official_website': official_website,
        # 'no_answer': '\n'.join(no_answer),
        'brands': '\n'.join(map(str, brands)),
        'brands_answer': answer or ' ',
    })

writer = csv.DictWriter(open('/tmp/product_mapper.csv', 'w'), fieldnames=results[0].keys())
writer.writeheader()
writer.writerows(results)
