from itertools import product, chain
from os import path
import csv
from mapper.settings import DROPBOX
from packages.utils import similar
from ticker_merchant_map.ticker_merchant_map import TickerFinder, TickerInfo
from unofficializing.unofficializing import make_unofficial_name as uo


base_path = path.join(DROPBOX, 'ticker finding', '7k_top_tickers')
reader = csv.DictReader(
    open(path.join(base_path, 'random_100_tickers_from_golden.csv'), 'r')
)

data = [{
    'raw_input': r['raw_input'],
    'company_names': list(map(str.lower, filter(None, r['company_name'].split('|')))),
    'tickers': list(map(str.lower, filter(None, r['ticker'].split('|')))),
} for r in reader]

for i, row in enumerate(data, start=1):
    print(i, '/', len(data))

    row['tickers_from_7k'] = list(map(
        str.lower,
        filter(
            None,
            set([(TickerFinder.query_db(company_name) or {}).get('ticker') for company_name in row['company_names']])
        )
    ))
    row['company_names_from_7k'] = list(map(
        str.lower,
        filter(
            None,
            set(
                chain.from_iterable(
                    [(TickerInfo.get_info_from_db(ticker) or {}).get('names', []) for ticker in row['tickers']]
                )
            )
        )
    ))

    row['ticker_matches'] = any(t1 == t2 for t1, t2 in product(row['tickers'], row['tickers_from_7k']))
    row['company_name_matches'] = any(similar(uo(n1), uo(n2)) for n1, n2 in product(row['company_names'], row['company_names_from_7k']))

writer = csv.DictWriter(
    open(path.join(base_path, 'random_100_tickers_from_golden_QA.csv'), 'w'),
    fieldnames=data[0].keys(),
)
writer.writeheader()
for row in data:
    row['company_names'] = '\n'.join(row['company_names'])
    row['tickers'] = '\n'.join(row['tickers'])
    row['tickers_from_7k'] = '\n'.join(row['tickers_from_7k'])
    row['company_names_from_7k'] = '\n'.join(row['company_names_from_7k'])
    writer.writerow(row)
