"""
Script to compare finding wikipedia page from cleaned string and from winner value.
Needed for checking whether disabling wiki ticker finder is a good idea.
"""
from mapper.entity_recognition import wikipedia
from wiki_finder.wiki_finder2 import WikiPage
from operator import itemgetter
from mapper.logs import load_logs
import csv


logs = load_logs(golden_only=True, mode='merchant')

results = []
for i, log in enumerate(logs, start=1):
    print(i, '/', len(logs))
    snap = log['snapshot']
    winner = snap.get('winner')
    if not winner:
        continue
    for attempt in snap['attempts']:
        if attempt.get('winners'):
            break
    cleaned = attempt['cleaned']
    wikipedia_graph = wikipedia(cleaned)
    entities_from_wiki_er = list(filter(lambda entity: entity['score'], map(itemgetter('entity'), wikipedia_graph.nodes.values())))
    wiki_from_cleaned = entities_from_wiki_er[0]['wiki_page'] if entities_from_wiki_er else None

    wiki_from_winner = WikiPage.from_str(winner['value'])
    match = False

    results.append({
        'log': log,
        'cleaned': cleaned,
        'wiki_from_cleaned': wiki_from_cleaned,
        'winner': winner['value'],
        'wiki_from_winner': wiki_from_winner,
        'match': getattr(wiki_from_cleaned, 'url', None) == getattr(wiki_from_winner, 'url', None),
    })

with open('/tmp/wiki.csv', 'w') as f:
    writer = csv.DictWriter(f, fieldnames=list(results[0].keys()))
    writer.writeheader()
    writer.writerows(results)
