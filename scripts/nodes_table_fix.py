import csv
from itertools import product
from os import path

from mapper.settings import DROPBOX
from packages.utils import remove_dupes, similar, tokenize, get_timestamp
from ticker_merchant_map.ticker_merchant_map import TickerFinder, TickerInfo, normalize_tkr_exch, InvalidTickerExchange
from packages.targets import TARGETS

from packages.utils import logger
from unofficializing.unofficializing import make_unofficial_name as uo

# logger = logging.getLogger(__file__)


targets = TARGETS.get_all()


errors = []
for i, (target, target_info) in enumerate(targets.items(), start=1):
    print(i, '/', len(targets))

    target_names = [target_info['official_name']] + target_info['entity_names'] + target_info['unofficial_names']
    target_names = remove_dupes(map(str.lower, target_names))
    target_names_uo = list(map(uo, target_names))

    try:
        target_ticker, target_exchange = normalize_tkr_exch(target_info['ticker'], target_info['exchange'])
    except InvalidTickerExchange as exc:
        logger.error(exc)
        continue

    if (target_info['ticker'] or target_info['exchange']) and (not target_ticker or not target_exchange):
        logger.warning('\n'.join([
            f'Ticker: {target}',
            f'Error: Bad target ticker/exchange: {target_info}',
        ]))
        continue

    error = {
        'target': target,
        'target_names': target_names_uo,
        'target_ticker': target_ticker,
        'target_exchange': target_exchange,
    }

    # for current target, search corresponding records in database (using target as search query)
    db_infos_by_name = list(filter(None, [TickerFinder.query_db(target_name) for target_name in target_names]))
    if db_infos_by_name:
        if len(db_infos_by_name) > 1:
            logger.warning(f'Found multiple db records for target "{target}": {db_infos_by_name}')

        # ensure that ticker and exchange match in nodes table and db
        if any((target_exchange == db_info.get('exchange')) and not (target_ticker == db_info.get('ticker')) for db_info in
                   db_infos_by_name):
            error.update({
                'db_infos': db_infos_by_name,
                'error': 'Target ticker doesn\'t match ticker in db',
            })
            logger.error(error)
            errors.append(error)
            continue

    # for current ticker & exchange, search corresponding records in database (using ticker & exchange as search query)
    if target_ticker and target_exchange:
        db_info_by_ticker = TickerInfo.get_info_from_db(target_ticker, target_exchange)

        # ensure that company names match
        if db_info_by_ticker and not any(
            similar(target_name_uo, db_name_uo) or any(token.lower() in db_name_uo for token in tokenize(target_name_uo))
            for target_name_uo, db_name_uo
            in product(target_names_uo, db_info_by_ticker['names_uo'])
        ):
            error.update({
                'db_infos': db_info_by_ticker,
                'error': 'Company names don\'t match',
            })
            errors.append(error)
            logger.error(error)
            continue

    # for current ticker, search records in database (using ticker only!)
    if target_ticker and target_exchange:
        db_info_by_ticker = TickerInfo.get_info_from_db(target_ticker)
        # if company names match
        if db_info_by_ticker and any(
            similar(target_name_uo, db_name_uo) or any(token.lower() in db_name_uo for token in tokenize(target_name_uo))
            for target_name_uo, db_name_uo
            in product(target_names_uo, db_info_by_ticker['names_uo'])
        ):
            if (target_ticker, target_exchange) != (db_info_by_ticker.get('ticker'), db_info_by_ticker.get('exchange')):
                error.update({
                    'db_infos': db_info_by_ticker,
                    'error': 'Ticker-exchange pairs don\'t match',
                })
                errors.append(error)
                logger.error(error)
                continue

writer = csv.DictWriter(open(path.join(DROPBOX, 'ticker finding', f'{get_timestamp()}.nodes_table_fix.csv'), 'w'),
                        fieldnames=errors[0].keys())
writer.writeheader()
writer.writerows(errors)
