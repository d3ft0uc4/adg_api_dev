import csv
import datetime

from domain_tools.company_page_detection import *
from domain_tools.utils import get, get_domain
from packages.utils import logger

with open(
    '/home/wolfie/Dropbox (ADG)/adg_api_dev_share/API/ERs/company website/features/company_website_features.golden_set.csv',
    'r',
) as f:
    data = [row for row in csv.DictReader(f)]

results = []
for i, row in enumerate(data, start=1):
    url = row['#url']
    print(f'{i}/{len(data)} - {url}')
    try:
        bing_response = BingWebCore().query(row['#cleaned'])
        bing_results = bing_response.get('webPages', {}).get('value', [])
        bing_urls = list([res['url'] for res in bing_results])

        # when we skip blacklisted websites for training data,
        # we lose accuracy:
        # without blacklisted websites: precision=0.926, FP=18/311
        # with    blacklisted websites: precision=0.940, FP=14/311
        # if in_blacklist(url):
        #     logger.warning(f'Skipping blacklisted url: {url}')
        #     continue

        if url in bing_urls:
            i = bing_urls.index(url)
        else:
            url = get_domain(url)
            bing_urls = [get_domain(url) for url in bing_urls]
            if url in bing_urls:
                i = bing_urls.index(url)
            else:
                logger.debug(f'Url "{url}" not in bing results')
                continue

        results.append({
            '#raw_input': row['#raw_input'],
            '#cleaned': row['#cleaned'],
            '#url': url,
            'correct': row['correct'],
            **get_internal_scores(bing_response, i, get(url)),
        })
    except Exception as exc:
        logger.exception(url)

date = datetime.datetime.now().strftime('%Y.%m.%d.%H.%M')
path = f'/home/wolfie/Dropbox (ADG)/adg_api_dev_share/API/ERs/company website/model/company_website_scores.{date}.csv'
with open(path, 'w') as f:
    writer = csv.DictWriter(f, fieldnames=list(results[0].keys()))
    writer.writeheader()
    writer.writerows(results)

print(f'Wrote results to {path}')
