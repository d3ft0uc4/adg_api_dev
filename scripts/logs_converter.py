# script to convert old log format to the new one

import sqlalchemy
from mapper.settings import SQL_CONNECTION_STRING
import pickle
# from mapper.mapper import ComparisonGraph
import json
from packages.utils import StrFallbackJSONEncoder

engine = sqlalchemy.create_engine(SQL_CONNECTION_STRING)

results = engine.execute(
    """
    SELECT id, snapshot
    FROM api_log AS log
    WHERE
        log.snapshot != ""
        AND NOT ISNULL(log.snapshot)
        AND NOT log.snapshot LIKE %(beginning)s
    """,
    beginning='%{"original_input":%'
).fetchall()
results = [dict(r) for r in results]

unpacked = []
errors = []
total = len(results)
for i, res in enumerate(results, start=1):
    print(f'{i} / {total} \t\t', end='\r')
    try:
        unpacked.append({
            'id': res['id'],
            'snapshot': pickle.loads(res['snapshot']),
        })
    except Exception as exc:
        print(exc)
        print('')
        errors.append(res)

total = len(unpacked)
for i, res in enumerate(unpacked, start=1):
    print(f'{i} / {total} (id={res["id"]}) \t\t', end='\r')
    engine.execute(
        """
        UPDATE api_log
        SET snapshot = %(snapshot)s
        WHERE id = %(id)s
        """,
        id=res['id'],
        snapshot=json.dumps(res['snapshot'], cls=StrFallbackJSONEncoder),
    )
