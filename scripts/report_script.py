import os

import pandas as pd
import sqlalchemy
import datetime
from mapper.settings import SQL_CONNECTION_STRING
from joblib import Parallel, delayed
import json

"""Check how bing cache is working."""

def new_check(q, not_cached_all):
    """Check if query looks suspicious."""
    all_not_cached_datetims = not_cached_all.loc[not_cached_all['query'] == q]
    latest_dt = all_not_cached_datetims.datetime.max()
    for dt in all_not_cached_datetims.datetime.values:
        delta = latest_dt - dt
        if delta == pd.Timedelta(0):
            pass
        elif delta < pd.Timedelta('14D'):
            print(f'{q}')
            return q


def get_candidates(not_cached_recent, not_cached_all):
    """Get all suspicious bing queries."""
    errors = Parallel(5, backend='threading')(
        delayed(new_check)(q, not_cached_all) for q in not_cached_recent['query'].values)
    errors = [x for x in errors if not x is None]

    root = '/home/ishsonya/workspace/adg_api_dev/scripts'
    json.dump(errors, open(os.path.join(root, 'candidates.json'), 'w+'))
    return errors


if __name__ == '__main__':

    today = datetime.datetime(2019, 1, 21, 10, 0, 0)

    select_not_cached_same_name_template = 'SELECT datetime FROM bing_queries WHERE query = %s and cache_hit = 0;'
    select_not_cached_recent = 'select query from bing_queries where datetime >  \'{}\' and cache_hit = 0;'.format(
        today)
    select_not_cached_all = 'select query, datetime from bing_queries where cache_hit = 0;'.format(
        today)

    engine = sqlalchemy.create_engine(SQL_CONNECTION_STRING)
    not_cached_recent = pd.read_sql(select_not_cached_recent, engine)

    not_cached_all = pd.read_sql(select_not_cached_all, engine)

    errors = get_candidates(not_cached_recent, not_cached_all)

    """For each suspicious bing query show every not cached instance of it."""
    for i, candidate in enumerate(errors):
        print(f'{candidate} {i} out of {len(errors)}')
        print(not_cached_all.loc[not_cached_all['query'] == candidate])
        input("NEXT")
