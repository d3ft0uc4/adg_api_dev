from packages.utils import qa
from domain_tools.company_page_detection import *
from domain_tools.utils import get

if __name__ == '__main__':
    def func(domain):
        if not domain.strip():
            return ''
        response = get(domain)
        if not response:
            return 'No response'
        return has_pagination(response)


    qa(lambda domain: {'has_pagination': func(domain)}, fields=['has_pagination'])
