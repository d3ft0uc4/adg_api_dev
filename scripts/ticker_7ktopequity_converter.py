import csv
from mapper.settings import DROPBOX
from os import path
import re


EXCHANGES_MAP = {
    'New York':     'NYSE',
    'Tokyo':        'TYO',
    'Shanghai':     'SSE',
    'NASDAQ GS':    'NASDAQ',
    'Hong Kong':    'HKG',
    'London':       'LSE',
    'Toronto':      'TSX',
    'ASE':          None,
    'EN Paris':     None,
    'Xetra':        None,
    'SIX-SW':       'SIX',
    'Stockholm':    None,
    'BrsaItaliana': None,
    'Soc.Bol SIBE': None,
    'NASDAQ GM':    'NASDAQ',
    'EN Amsterdam': None,
    'EN Brussels':  None,
    'Oslo':         None,
    'Helsinki':     None,
    'Copenhagen':   None,
    'Vienna':       None,
    'NASDAQ CM':    'NASDAQ',
    'Athens':       None,
    'EN Dublin':    None,
    'EN Lisbon':    None,
    'NYSEAmerican': None,
    'London Intl':  'LSE',
    'Frankfurt':    'FWB',
    'Hamburg':      None,
    'FN Stockholm': None,
    'Stuttgart':    None,
    'HI-MTF':       None,
    'Canadian Sec': None,
    'Nagoya':       None,
    'Valletta':     None,
    'OTC-X BEKB':   None,
    'Venture':      None,
    'Reykjavik':    None,
    'Luxembourg':   None,
    'NYSE Arca':    'NYSE',
    'Munich':       None,
    'Nordic GM':    None,
    'BME Outcry':   'BME',
    'Sapporo':      None,
    'LSE EuropeQS': None,
    'Cboe BZX':     None,
    'Investors Ex': None,
}


reader = csv.DictReader(open(path.join(DROPBOX, 'ticker finding', 'data', '7ktopequity.csv'), 'r'))
data = [r for r in reader]

for d in data:
    d['ticker'] = re.sub(r'\w+ Equity', '', d['ticker'])
    # d['exchange'] = EXCHANGES_MAP.get(d['exchange'])
    # if not d['exchange']:
    #     continue

    d['market_cap'] = d['market_cap'].replace(',', '')
    d['revenue'] = d['revenue'].replace(',', '')

writer = csv.DictWriter(open(path.join(DROPBOX, 'ticker finding', 'data', '7ktopequity_converted.csv'), 'w'),
                        fieldnames=data[0].keys())
writer.writeheader()
writer.writerows(data)
