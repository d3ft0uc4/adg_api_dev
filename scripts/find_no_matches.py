"""
Script to find entities which sound similar but balong to different groups
"""
from mapper.logs import load_logs
from itertools import combinations, product
import csv
from packages.utils import similar

logs = load_logs()

results = []
for log in logs:
    snap = log['snapshot']
    for attempt in snap['attempts']:
        groups = list(map(lambda group: group['graph'], attempt.get('losers', []) + attempt.get('winners', [])))
        if len(groups) > 1:
            for group1, group2 in combinations(groups, 2):
                for entity1, entity2 in product(group1, group2):
                    if similar(entity1['value'], entity2['value'], thld=0.85):
                        results.append({
                            'log': log,
                            'entity1': entity1,
                            'entity2': entity2,
                        })


results2 = {}
for r in results:
    results2[(r['entity1']['value'], r['entity2']['value'])] = r

with open('/tmp/similar.csv', 'w') as f:
    writer = csv.DictWriter(f, fieldnames=['log', 'entity1', 'entity2', 'entity1_val', 'entity2_val'])
    writer.writeheader()
    for r in results2.values():
        writer.writerow({
            **r,
            'entity1_val': r['entity1']['value'],
            'entity2_val': r['entity2']['value'],
        })