import csv

CRUNCHBASE_EXCHANGES_MAP = {
    **{exch: exch for exch in [
        'NASDAQ',
        'NYSE',
        'SIX',
        'TYO',
        'FWB',
        'TSX',
        'BME',
        'ASX',
        'SZSE',
        'SSE',
        'KRX',
        'HKG',
    ]},

    'LON': 'LSE',
    'EURONEXT': 'ENX',

    **{exch: None for exch in [
        'ADX',
        'AFX',
        'AMEX',
        'AMS',
        'BCBA',
        'BIT',
        'BKK',
        'BLSE',
        'BM',
        'BMV',
        'BOM',
        'BSE',
        'BVC',
        'BVFB',
        'BVMF',
        'CAS',
        'CNSX',
        'COL',
        'CPH',
        'CSE',
        'CVE',
        'DFM',
        'EBR',
        'EGX',
        'ELI',
        'EPA',
        'ETA',
        'ETR',
        'FRA',
        'HEL',
        'HNX',
        'HOSE',
        'IDX',
        'ISE',
        'IST',
        'ISX',
        'JP',
        'JSE',
        'KLSE',
        'KOSDAQ',
        'KSE',
        'LSE',
        'MCX',
        'MOEX',
        'MSE',
        'NBO',
        'NEEQ',
        'NIG',
        'NPEX',
        'NSE',
        'NYSEARCA',
        'NYSEMKT',
        'NYX',
        'NZE',
        'OSE',
        'OSL',
        'OTCBB',
        'OTCMKTS',
        'OTCPINK',
        'OTCQB',
        'OTCQX',
        'PRG',
        'PSE',
        'QSE',
        'RSE',
        'SGX',
        'SHA',
        'SHE',
        'STO',
        'SWX',
        'TADAWUL',
        'TAL',
        'TLV',
        'TPE',
        'TSE',
        'TSEC',
        'USE',
        'VIE',
        'VTX',
        'WSE',
    ]},
}


reader = csv.DictReader(open('crunchbase/organizations.csv', 'r'))
data = [d for d in reader if d['primary_role'] == 'company']

results = []
for d in data:
    r = {
        'name': d['name'],
        'website': d['homepage_url'],
    }

    exchange, ticker = d['stock_symbol'].split(':')[:2]
    if ticker and exchange:
        r['ticker'] = ticker.upper()
        r['exchange'] = CRUNCHBASE_EXCHANGES_MAP[exchange.upper()]

    results.append(r)

results = list(map(dict, set(map(lambda d: tuple(d.items()), results))))

writer = csv.DictWriter(open('/tmp/crunchbase.csv', 'w'), fieldnames=['name', 'website', 'ticker', 'exchange'])
writer.writeheader()
writer.writerows(results)
