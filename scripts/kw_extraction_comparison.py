from mapper.logs import load_logs
from tfidf.tfidf import extract_keywords as tfidf_extract_keywords
from keywords.keyword_extraction import extract_keywords as simple_extract_keywords
from mapper.settings import DROPBOX
from os import path
import csv
from packages.utils import get_timestamp


logs = load_logs(mode='merchant', golden_only=True)

file = open(path.join(DROPBOX, 'keyword extraction', f'kw_extraction_comparison.{get_timestamp()}.csv'), 'w')
writer = csv.DictWriter(file, fieldnames=['raw_input', 'cleaned', 'correct_company_names', 'tfidf(raw_input)', 'tfidf(cleaned)', 'simple'])
writer.writeheader()


for i, log in enumerate(logs, start=1):
    print(i, '/', len(logs))
    snap = log['snapshot']
    if not snap.get('attempts'):
        continue

    attempt = snap['attempts'][0]

    if not attempt['cleaned']:
        continue

    writer.writerow({
        'raw_input': snap['original_input'],
        'cleaned': attempt['cleaned'],
        'correct_company_names': '\n'.join(log['correct_company_names']),
        'tfidf(raw_input)': '\n'.join(f'{kw} - {score:.0f}' for kw, score in tfidf_extract_keywords(snap['original_input'])[:10]),
        'tfidf(cleaned)': '\n'.join(f'{kw} - {score:.0f}' for kw, score in tfidf_extract_keywords(attempt['cleaned'])[:10]),
        # 'simple': '\n'.join(f'{kw} - {score:.0f}' for kw, score in simple_extract_keywords(attempt['cleaned'])[:10]),
    })
    file.flush()
