from packages.utils import qa
from cc_cleaner.address_detection import AddressExtractor
from cc_cleaner.clean_cc import CCCleaner
import usaddress


def test(rawin):
    extractor = AddressExtractor(rawin)
    address, rest = extractor.extract_street()
    full_output = extractor.address_list
    return {'street': address, 'rest': rest, 'full_output': full_output}


if __name__ == '__main__':
    qa(lambda rawin: test(rawin), fields=['street', 'rest'])
