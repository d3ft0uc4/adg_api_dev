import csv
from itertools import chain
from typing import Tuple
from os import path
from datetime import datetime
from argparse import ArgumentParser

from mapper.logs import load_logs
from mapper.tuning.graph_scores import WRONG_INPUTS
from mapper.tuning.utils import get_groups
from mapper import entity_recognition
from unofficializing.unofficializing import make_unofficial_name as uo
from packages.utils import similar

parser = ArgumentParser()
parser.add_argument('er')
args = parser.parse_args()

ER = args.er
assert hasattr(entity_recognition, ER)
DIR = path.join('/home/wolfie/Dropbox (ADG)/adg_api_dev_share/API/ERs/', ER)

logs = load_logs(golden_only=True, golden_table='golden_set_domain_merchant') + \
       load_logs(golden_only=True, golden_table='golden_set_bumped')
logs = [log for log in logs if log['raw_input'] not in WRONG_INPUTS]

for log in logs:
    snap = log['snapshot']
    snap['attempts'] = [
        attempt
        for attempt in snap['attempts']
        if not log['correct_cleaned'] or (log['correct_cleaned'] in attempt['cleaned'])
    ]

# ---- using separate ER run ---
scores = []
total = len(logs)
for i, log in enumerate(logs, 1):
    print(f'{i} / {total}')
    for attempt in log['snapshot']['attempts']:
        cleaned = attempt['cleaned']
        correct_values = [uo(value) for value in log['correct_company_names']]

        entities = [
            node['entity'] for node in getattr(entity_recognition, ER)(cleaned).nodes.values()
            if any(node['entity'].internal_scores.values())
        ]
        for entity in entities:
            if any(uo(entity.value).lower() == correct.lower() for correct in correct_values):
                match = 1
            elif any(entity.value[:4] == correct[:4] for correct in correct_values):
                match = '?'
            else:
                match = 0

            scores.append({
                '#raw_input': log['raw_input'],
                '#cleaned': cleaned,
                '#correct_values': correct_values,
                '#entity': entity.value,
                'match': match,
                **entity.internal_scores,
            })


# ---- using golden set qa ----
# correct_groups = []
# wrong_groups = []
# unknown_groups = []
#
# # get correct and wrong groups
# total = len(logs)
# for i, log in enumerate(logs, start=1):
#     print(f'{i} / {total}')
#
#     unknown, wrong, correct = get_groups(log)
#
#     unknown_groups.extend(unknown)
#     correct_groups.extend(correct)
#     wrong_groups.extend(wrong)
#
# # generate all scores
# scores = []
# for should_win, group in chain(
#         [(1, group) for group in correct_groups],
#         [(0, group) for group in wrong_groups],
#         [('?', group) for group in unknown_groups],
# ):
#     for entity in group['graph']:
#         if entity['source'] == ER:
#             scores.append({
#                 'should_win': should_win,
#                 **{
#                     f'#{key}': value
#                     for key, value in group.items()
#                     if key in ['raw_input', 'cleaned', 'correct_company_names', 'group_values']
#                 },
#                 '#value': entity['value'],
#                 **entity['internal_scores'],
#             })

# write csv
pth = path.join(DIR, f'{datetime.now().strftime("%Y.%m.%d.%H.%M")}.csv')
with open(pth, 'w') as f:
    fieldnames = list(scores[0].keys())

    def get_order(field: str) -> Tuple[int, str]:
        if field == 'should_win':
            return 0, field
        elif field.startswith('#'):
            return 1, field
        else:
            return 2, field


    fieldnames = sorted(fieldnames, key=get_order)
    writer = csv.DictWriter(f, fieldnames)
    writer.writeheader()
    writer.writerows(scores)

print(f'Wrote results to "{pth}"')
