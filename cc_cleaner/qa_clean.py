from packages.utils import qa
from .clean_cc import CCCleaner

cc_cleaner = CCCleaner()
if __name__ == '__main__':
    qa(lambda rawin: {'output': cc_cleaner.clean(rawin)[0]}, fields=['output'])
