"""
© 2018 Alternative Data Group. All Rights Reserved.

Class for cleaning raw cc data using predefined set of rules.

Usage example:
    from cc_cleaner.clean_cc import CCCleaner
    raw_cc = 'CVS/PHARMACY #07843      WINCHESTER   VA'
    result = CCCleaner().clean(raw_cc)[0]
    print(result)

See also run_cc_cleaner

Usage from console:
    python3 -m cc_cleaner.clean_cc "CVS/PHARMACY #07843      WINCHESTER   VA" "MET FOOD STATEN ISLAND NY"
"""

import csv
import os
import re
from collections import defaultdict
from itertools import filterfalse

from cc_cleaner.utils import substring_before
from packages.utils import Timer, pretty
from mapper.settings import COMPANY_TERMINATORS

dir_path = os.path.dirname(os.path.realpath(__file__))


def sublist_index(superlist: list, sublist: list) -> int:
    """
    Finds occurrence of sublist in superlist:
    sublist_index([1, 2, 3, 4, 5], [4, 5]) == 3
    """
    assert superlist
    assert sublist

    for i in range(len(superlist)):
        if superlist[i:i+len(sublist)] == sublist:
            return i

    return -1


class CCCleaner:
    _auxiliary_data = os.path.join(dir_path, 'data', 'auxiliary')
    _regexXx = re.compile('(XXXXX\d{4})')
    _regexPhoneNumber1 = re.compile('(\d{3}-\d{3}-\d{4})')
    _regexPhoneNumber2 = re.compile('(\d{3}-\d{7})')
    _regexPhoneNumber3 = re.compile('(800-\w{7})')
    _regexHashtagNumberWithSpace = re.compile('([^|\S]# \d+[$|\S])')  # Works either at start, end, or middle of string.
    _regexHashtagNumberInsideWord = re.compile('(#\d{2,})')
    _regexRepeatingChars = re.compile(r"(\S)(\1{2,})(?=.?)(\1{2,}?)?(?=.?)(\1{2,}?)?")
    _regexUrlAfterSlash = re.compile(r"(\..{2, 3}) /.* $")
    _regexStreetNumber = re.compile(r"([0-9]+(ST|ND|TH))")
    _regexCharsBetweenNumbers = re.compile(r"[0-9]+\D+[0-9]+")
    _regexMoneySignBefore = re.compile(r'(\$|USD)[0-9]+([., ][0-9]+)?')
    _regexMoneySignAfter = re.compile(r"[0-9]+([., ][0-9]+)?(\$|USD)")
    _regexAllInsideBrackets = re.compile(r"\(.*?\)")

    _state_to_cities = defaultdict(list)
    _city_to_states = defaultdict(list)
    with open(os.path.join(_auxiliary_data, 'cities_states.csv'), encoding='utf-8') as f:
        csv_reader = csv.reader(f, delimiter=',')
        for line in csv_reader:
            if line:
                state = line[1].upper()
                city = line[0].upper()
                _state_to_cities[state].append(city)
                _city_to_states[city].append(state)

    with Timer('Loading CC Cleaner locations'):
        _locations = []
        with open(os.path.join(_auxiliary_data, 'locations.txt'), encoding="utf-8") as f:
            for line in f:
                location = line.strip().upper()
                if location:
                    _locations.append(location)
        # Sort the locations descending by number of words to remove as many words as possible.
        # We want to remove 'aabybro kommun' before 'aabybro'.
        # self._locations.sort(key=len, reverse=True)

        # group locations by prefix
        _prefix_length = 4
        _locations_by_prefix = defaultdict(list)
        for location in _locations:
            for prefix_length in range(2, _prefix_length + 1):
                _locations_by_prefix[location[:prefix_length]].append(location)

    _bad_words = []
    with open(os.path.join(_auxiliary_data, 'bad_words.txt'), encoding="utf-8") as f:
        for line in f:
            word = line.strip().upper()
            if word:
                _bad_words.append(word)


    @staticmethod
    def hash_combine(parts):
        return '###' + '###'.join(parts) + '###'

    def clean(self, raw_string, testing=False):
        found_city = None
        found_state = None
        found_location = None
        found_processor = None

        original_string = raw_string.upper()
        words = self._str_to_words(original_string)

        """
        Remove more than one space.

        From: MASSAGE   ENVY
        To:   MASSAGE ENVY
        """
        clean_string = ' '.join(words)

        """
        Special case: remove # at the beginning of the sentence

        From: #NETFLIX.COM
        To:   NETFLIX.COM
        """
        if clean_string.startswith('#'):
            clean_string = clean_string[1:]
            words = self._str_to_words(clean_string)
        """
        Special case: remove `CHECKCARD 1234 `

        From: CHECKCARD  0107 STARBUCKS
        To:   STARBUCKS
        """
        if len(words) >= 3:
            if words[0] == 'CHECKCARD' and words[1].isnumeric():
                clean_string = ' '.join(words[2:])
                words = self._str_to_words(clean_string)

        """
        Remove letter-digit crap.
        FROM: Amazon.com*M08SX4C51
        TO:   Amazon.com*
        """
        words = list(filterfalse(
            lambda word:
                re.match(r'^\w+$', word, flags=re.IGNORECASE) and len(re.findall(r'(\d[^\W\d]|[^\W\d]\d)', word)) >= 2,
            words
        ))
        clean_string = ' '.join(words)

        """
        Split group of digits which "sticks" to group of letters.
        FROM: 1022MCDONALD'S1122
        TO:   1022 MCDONALD'S 1122
        """
        for i in range(len(words))[::-1]:
            word = words[i]

            subwords = []
            start = 0
            for match in re.finditer(r'(?:\d{3}()[^\W\d]{3}|[^\W\d]{3}()\d{3})', word):
                end = match.span(1)[1]
                if end < 0:
                    end = match.span(2)[1]
                subwords.append(word[start:end])

                start = end

            if subwords:
                subwords.append(word[end:])
                words[i:i+1] = subwords

        clean_string = ' '.join(words)

        """
        Special case: remove `PAYPAL`, `SQ`

        From: PAYPAL *WIKIMEDIAFO
        To:   WIKIMEDIAFO
        """
        if len(words) >= 2:
            for p in ('PAYPAL', 'SQ'):
                if words[0] == p:
                    if words[1].startswith('*'):
                        clean_string = ' '.join([words[1][1:]] + words[2:])
                        found_processor = p
                    else:
                        clean_string = ' '.join(words[1:])
                        found_processor = p
                    words = self._str_to_words(clean_string)
                    break

        """
        Special case: remove `PURCHASE`

        From: PURCHASE - SIXPACKSHORTCUTS.COM
        To:   SIXPACKSHORTCUTS.COM
        """
        if len(words) >= 3:
            if words[0] == 'PURCHASE' and words[1] == '-':
                clean_string = ' '.join(words[2:])
            words = self._str_to_words(clean_string)

        if len(words) >= 5:
            if words[-1] == 'USA':
                clean_string = ' '.join(words[:-1])
            words = self._str_to_words(clean_string)

        """
        Special case: remove `Debit PIN Purchase`/`Debit Card Purchase` at beginning.

        From: DEBIT PIN PURCHASE MTA VENDING MACHINES NEW YORK NY
        To:   MTA VENDING MACHINES NEW YORK NY
        """
        if len(words) >= 3:
            DEBIT_STATEMENTS = ('DEBIT PIN PURCHASE ', 'DEBIT CARD PURCHASE ')
            for ds in DEBIT_STATEMENTS:
                if clean_string.startswith(ds):
                    clean_string = clean_string[len(ds):]
                    words = self._str_to_words(clean_string)
                    break

        """
        Special case: remove financial statements such as `INTEREST ON PURCHASE`

        From: INTEREST ON PURCHASE
        To:   <empty>
        """
        FINANCIAL_STATEMENTS = ('INTEREST ', 'ATM ', 'CASH WITHDRAWAL ', 'DEPOSIT ', 'TRANSFER TO ')
        for fs in FINANCIAL_STATEMENTS:
            if clean_string.startswith(fs):
                clean_string = ''
                words = []
                break

        """
        Remove hashtag number.

        From: WAL-MART #4593
        To:   WAL-MART
        """
        to_remove = []
        for word in words:
            if word.startswith('#') and word[1:].isnumeric():
                to_remove.append(word)
        if to_remove:
            words = [w for w in words if w not in to_remove]
            clean_string = ' '.join(words)
        """
        Remove hashtag number with space in between them.

        From: TRUGREEN  # 5685
        To:   TRUGREEN
        """
        clean_string = self._regexHashtagNumberWithSpace.sub('', clean_string)

        """
        Remove $money, money$.

        From: Nike $10 Gift Card (Email Delivery)
        To:   Nike Gift Card (Email Delivery)
        """
        clean_string = self._regexMoneySignBefore.sub('', clean_string)
        clean_string = self._regexMoneySignAfter.sub('', clean_string)

        """
        Remove everything inside brackets

        From: Nike Gift Card (Email Delivery)
        To:   Nike Gift Card
        """
        clean_string = self._regexAllInsideBrackets.sub('', clean_string)
        """
        Remove hashtag number that has no spaces.

        From: WAL-MART SUPERCENTER#1770ABC
        To:   WAL-MART SUPERCENTER ABC
        """
        clean_string = self._regexHashtagNumberInsideWord.sub(' ', clean_string)
        words = self._str_to_words(clean_string)
        clean_string = ' '.join(words)

        """
        Check if asterisk inside word, and remove all characters before it.

        From: BWI*BOINGO
        TO: BOINGO
        """

        clean_words = []

        for word in words:
            if not "*" in word:
                clean_words.append(word)
                continue

            clean_words.append(substring_before(word,'*'))

        words = clean_words
        clean_string = ' '.join(words)

        """
        Remove Q03, QPS, Q39 From String
        """
        clean_words = []

        if len(words) > 1:
            for word in words:
                if word in ('Q03','QPS','Q39'):
                    continue #So we don't add this to the list.

                for p in ('Q03','QPS','Q39'):
                    word = word.replace(p,'')

                clean_words.append(word)

            words = clean_words
            clean_string = ' '.join(words)

        """
        Remove asterisks.

        From: BWI*BOINGO WIRELESS
        To:   BWI BOINGO WIRELESS
        """
        clean_string = clean_string.replace('*', ' ')
        words = self._str_to_words(clean_string)

        """
        Remove dots.

        From: WILLIAM H. SADLIER, INC. 9 PINE FL 2
        To:   WILLIAM H SADLIER, INC 9 PINE FL 2
        """
        clean_words = []
        for w in words:
            if w.endswith('.'):
                w = w[:-1]
            clean_words.append(w)
        words = clean_words
        clean_string = ' '.join(words)

        """
        Remove everything after company form.

        From: WILLIAM H SADLIER, INC 9 PINE FL 2
        To:   WILLIAM H SADLIER, INC
        """
        match = re.match(
            r'^(.+\b(' + r'|'.join(re.escape(term) for term in COMPANY_TERMINATORS) + r'))\b.*',
            clean_string,
            flags=re.IGNORECASE,
        )
        if match:
            clean_string = match[1]
            words = self._str_to_words(clean_string)

            # now we don't want to delete at least 3 words before company form
            # extract them from the process and return back in the end
            last_words = words[-4:]
            words = words[:-4]
        else:
            last_words = []
        clean_string = ' '.join(words)

        """
        Remove number from word if followed by a city name.

        From: THE MEAN FIDDLER 00000009NEW YORK     NY
        To:   THE MEAN FIDDLER NEW YORK NY
        """
        # TODO: (LOW) Use the trie algorithm if this proves to be too slow.
        if len(words) > 1:
            cities = self._city_to_states.keys()
            cities_first_word = [c.split()[0] for c in cities]
            for i in range(len(words)):
                word = words[i]
                number_groups = re.findall(r'(\d{2,})', word)
                if number_groups:
                    # We take the rightmost group of digits.
                    number = number_groups[len(number_groups) - 1]
                    num_index = word.rfind(number)

                    word_before = word[:num_index]
                    supposed_city_first_word = word[num_index + len(number):]
                    if supposed_city_first_word in cities:
                        words[i] = word_before + ' ' + supposed_city_first_word
                        continue
                    elif supposed_city_first_word in cities_first_word:
                        if i + 1 < len(words):
                            supposed_city = ' '.join([supposed_city_first_word, words[i + 1]])
                            if supposed_city in cities:
                                words[i] = word_before + ' ' + supposed_city_first_word
                                continue

                        if i + 2 < len(words):
                            supposed_city = ' '.join([supposed_city_first_word, words[i + 1], words[i + 2]])
                            if supposed_city in cities:
                                words[i] = word_before + ' ' + supposed_city_first_word
                                continue

            clean_string = ' '.join(words)
            words = self._str_to_words(clean_string)

        """
        Add space if city and state without space.

        From: REDAPPLE PROMENADE       SEATTLEWA
        To:   REDAPPLE PROMENADE SEATTLE WA
        """
        if words and len(words[-1]) >= 5 and words[-1].isalpha():
            for st in self._state_to_cities.keys():
                found = False
                if words[-1].endswith(st):
                    last_word_without_state = words[-1][:-len(st)]
                    cities = self._state_to_cities[st]
                    for supposed_city in cities:
                        if supposed_city.split()[-1] == last_word_without_state:
                            clean_string = clean_string[:-2] + ' ' + clean_string[-2:]
                            words = self._str_to_words(clean_string)
                            found = True
                            break
                if found:
                    break

        if words and len(words) >= 3:
            found = False
            states = set(self._state_to_cities.keys())
            if words[-1] in states:
                state = words[-1]
                cities = self._state_to_cities[state]

                space_positions = None
                for city in cities:
                    space_positions = []

                    i = -2
                    cur_word = words[i]

                    city_words = city.split()
                    for j in range(-1, -1 - len(city_words), -1):

                        last_word_of_city = city_words[j]
                        if not cur_word.endswith(last_word_of_city):
                            space_positions = []
                            break

                        cur_word = cur_word[:-len(last_word_of_city)]
                        if not cur_word:
                            i -= 1
                            if abs(i) > len(words):
                                break
                            cur_word = words[i]
                        else:
                            space_positions.append((i, -len(last_word_of_city)))
                    else:
                        break

                if len(space_positions) == 1:
                    for pos_word, pos_in_word in space_positions:
                        word = words[pos_word]
                        new_word = word[:pos_in_word] + ' ' + word[pos_in_word:]
                        words[pos_word] = new_word
                    clean_string = ' '.join(words)
                    words = self._str_to_words(clean_string)

        """
        Remove city and state at end.

        From: REDAPPLE PROMENADE       SEATTLE      WA
        To:   REDAPPLE PROMENADE
        """
        city_and_state_removed = False
        if words:
            found_city, found_state, clean_string = self._detect_city_and_state(words, supposed_state_index=-1)
            words = self._str_to_words(clean_string)
            city_and_state_removed = found_city is not None or found_state is not None

        """
        Remove state without city at end.

        From: MASSAGE ENVY       IN
        To:   MASSAGE ENVY
        """
        if not city_and_state_removed and words:
            if words[-1] in self._state_to_cities.keys():
                found_state = words[-1]
                clean_string = ' '.join(words[:-1])
                words = self._str_to_words(clean_string)
        clean_string = self._regexXx.sub('', clean_string)
        clean_string = self._regexPhoneNumber1.sub('', clean_string)
        clean_string = self._regexPhoneNumber2.sub('', clean_string)
        clean_string = self._regexPhoneNumber3.sub('', clean_string)
        words = self._str_to_words(clean_string)
        """
        Remove cutoff city and state, combined

        From: EL TAMBO GRILL         MIAMFL
        To: EL TAMBO GRILL
        """
        if words and not found_city and not found_state:
            states = self._state_to_cities.keys()
            for state in states:
                if 4 <= len(words[-1]) and words[-1].endswith(state):
                    cities = self._state_to_cities[state]
                    num_words_to_city = defaultdict(list)
                    for city in cities:
                        num_words_to_city[len(city.split())].append(city)

                    last_word_without_state = words[-1][:-len(state)]
                    if len(words) >= 4:
                        for city in num_words_to_city[3]:
                            supposed_start_of_city = ' '.join([words[-3], words[-2], last_word_without_state])
                            if city.startswith(supposed_start_of_city):
                                found_city = city
                                found_state = state
                                words = words[:-3]
                                break
                    if len(words) >= 3:
                        for city in num_words_to_city[2]:
                            supposed_start_of_city = ' '.join([words[-2], last_word_without_state])
                            if city.startswith(supposed_start_of_city):
                                found_city = city
                                found_state = state
                                words = words[:-2]
                                break

                    if found_city:
                        clean_string = ' '.join(words)
                        break

        """
        If two words before city & state, remove everything after.

        From: SHELL OIL 57444468805 CLAREMONT CA - SERVICE STATIONS (WITH OR W/O ANCILLARY SERVICES)
        To: SHELL OIL
        """
        if not found_city and not found_state:
            for i in range(2, len(words)):
                found_city, found_state, clean_string = self._detect_city_and_state(words, supposed_state_index=i)
                if found_city or found_state:
                    words = self._str_to_words(clean_string)
                    break
        """
        If no city was found so far, try using `locations.txt`.
        """
        if not found_city and not found_state and len(words) > 2:
            NUM_IMMUTABLE = 1
            immutable_parts = words[:NUM_IMMUTABLE]
            mutable_parts = words[NUM_IMMUTABLE:]

            # select "candidates" - only those locations which have same prefix as any of words
            prefixes = [part[:self._prefix_length] for part in mutable_parts]
            candidates = []
            for prefix in prefixes:
                candidates.extend(self._locations_by_prefix.get(prefix, []))
            candidates.sort(key=len, reverse=True)

            value = ' '.join(mutable_parts)
            for location in filter(lambda location: location in value, candidates):
                value = re.sub(rf'\b{re.escape(location)}\b', '', value, flags=re.IGNORECASE)

            words = immutable_parts + value.split(' ')
            clean_string = ' '.join(immutable_parts + [value])

            # # ---- Previous crap ----
            # if len(words) > 2:
            #     words_up_to_first_str = self.hash_combine(words[:1])
            #     words_after_first_str = self.hash_combine(words[1:])
            # else:
            #     words_up_to_first_str = None
            #     words_after_first_str = None
            # prefixes = [part[:self._prefix_length] for part in words[1:]]
            # candidates = []
            # for prefix in prefixes:
            #     candidates.extend(self._locations_by_prefix.get(prefix, []))

            # candidates.sort(key=lambda x: len(x.split()), reverse=True)
            # should_restart = True
            # while should_restart:
            #     should_restart = False
            #     for location in candidates:
            #         location_parts = [w for w in location.split() if w]
            #         location_hash = self.hash_combine(location_parts)
            #         if words_after_first_str and len(location_parts) >= 2:
            #             if location_hash in words_after_first_str:
            #                 words_after_first_str = words_after_first_str.replace(location_hash, ' ')
            #                 words = [w for w in (words_up_to_first_str + words_after_first_str).split('###') if w]
            #                 clean_string = ' '.join(words)
            #                 found_location = ' '.join(location_parts)
            #                 should_restart = True
            #                 break
            #         elif len(location_parts) == 1:
            #             a = ' '.join(words[:-1])
            #             b = len(re.findall("[a-zA-Z']+", a))
            #             if words[-1].strip() == location_parts[0].strip() and b >= 2:
            #                 found_location = location_parts[0]
            #                 words = words[:-1]
            #                 clean_string = ' '.join(words)
            #                 should_restart = True
            #                 break

        """
        Remove street number from string

        From: BAXTER INTERNATIONAL LTD. 5TH"
        To:   BAXTER INTERNATIONAL LTD.
        """
        clean_string = re.sub(self._regexStreetNumber, '', clean_string)
        words = self._str_to_words(clean_string)

        """
        Remove strings where number between chars

        From: NYC TAXI 5K82
        To:   NYC TAXI
        """
        clean_string = ' '.join([w for w in words if not re.match(self._regexCharsBetweenNumbers,w)])
        words = self._str_to_words(clean_string)

        """
        Remove grouped numbers.

        From: HOTELS.COM150328917070
        To:   HOTELS.COM

        From: MEXX1234
        To:   MEXX
        """
        clean_string = re.sub(r'(\bx|\d)[x\d-]{2,}(x\b|\d)', '', clean_string, flags=re.IGNORECASE)
        words = self._str_to_words(clean_string)

        """
        Remove repeating chars

        From: HOTELS.COM US XXX-XXX-XXXX WA
        To:   HOTELS.COM US - WA
        """

        clean_string = ' '.join(words)
        clean_string = self._regexRepeatingChars.sub('', clean_string)
        words = self._str_to_words(clean_string)

        """
        Remove chars after a slash of a url
        From: AMAZON MKTPLACE PMTS     AMZN.COM/BILLWA
        To:   AMAZON MKTPLACE PMTS     AMZN.COM
        """

        clean_string = ' '.join(words)
        # For some reason using the precompiled one does not work here
        #clean_string = self._regexUrlAfterSlash.sub(r"\1", clean_string)
        clean_string = re.sub(r"(\..{2,3})/.*$", r"\1", clean_string)
        words = self._str_to_words(clean_string)
        """
         Remove symbols from begning/end of string
         From: !amazon"
         To:   amazon
         """

        clean_string = ' '.join(words)
        clean_string = clean_string.strip('#&!~",')
        words = self._str_to_words(clean_string)

        """
        Remove blacklisted words.
        FROM: CHECKCARD STARBUCKS PINEVI
        TO:   STARBUCKS PINEVI
        """
        if not testing:
            words = [w for w in words if w not in self._bad_words]
            clean_string = ' '.join(words)

        # Return company name back
        words += last_words
        clean_string = ' '.join(words)

        if raw_string.strip() == clean_string.strip():
            return (
                clean_string.strip(),
                found_city.strip() if found_city else None,
                found_state.strip() if found_state else None,
                found_location.strip() if found_location else None,
                found_processor.strip() if found_processor else None,
            )
        else:
            return self.clean(clean_string)

    def _detect_city_and_state(self, words, supposed_state_index):
        cities = self._state_to_cities.get(words[supposed_state_index], None)
        if not cities and len(words) > supposed_state_index:
            cities = self._state_to_cities\
                .get(' '.join(words[supposed_state_index:supposed_state_index + 2]), None)
        if cities:
            # Move from more to fewer words because cities can have substring cities.
            # E.g. we want to check for SOUTH SAN FRANCISCO before SAN FRANCISCO.
            for i in range(3, 0, -1):
                test_city_string_words = words[supposed_state_index - i:supposed_state_index]
                test_city_string = ' '.join(test_city_string_words)
                if test_city_string in cities and sublist_index(words, test_city_string_words) > 1:
                    found_state = words[supposed_state_index]
                    found_city = test_city_string
                    clean_string = ' '.join(words[:supposed_state_index - i])
                    return found_city, found_state, clean_string
        return None, None, ' '.join(words)

    @staticmethod
    def _str_to_words(clean_string):
        WORD_SEPARATORS = '[ :,*@#~]+'
        words = [s.strip() for s in re.split(WORD_SEPARATORS, clean_string)]
        words = [s for s in words if s]
        return words


if __name__ == '__main__':
    import argparse
    import json

    parser = argparse.ArgumentParser()
    parser.add_argument('raw_input')
    args = parser.parse_args()

    result = CCCleaner().clean(args.raw_input)[0]
    print(pretty(result))
