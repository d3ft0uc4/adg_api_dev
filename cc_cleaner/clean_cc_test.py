from .clean_cc import CCCleaner


cleaner = CCCleaner()
def clean(rawin: str) -> str:
    return cleaner.clean(rawin)[0]


def test_location():
    assert clean('axs catalyst infinity brookline 617-734-6810') == 'AXS CATALYST INFINITY'

def test_remove_one_word_location():
    # Positive examples.
    assert clean('BLUE MARTINI MIAMI') == 'BLUE MARTINI'
    assert clean('BLUE MIAMI') == 'BLUE MIAMI'
    assert clean('STARBUCKS #08506 MIAMI') == 'STARBUCKS'
    assert clean('VIRGINIA\'S CAFE') == 'VIRGINIA\'S CAFE'

def test_remove_two_word_location():
    # Positive examples.
    assert clean('SOME COMPANY SAN ANTONIO NAME') == 'SOME COMPANY NAME'
    assert clean('WAIKIKI RESTAURANT INC SAN JUAN PRI') == 'WAIKIKI RESTAURANT INC'
    assert clean('BURGER KING #09866') == 'BURGER KING'
    assert clean('WABASH COLLEGE BOOKSTR') == 'WABASH COLLEGE BOOKSTR'
    assert clean('EL CRISTO RESTAURANT') == 'EL CRISTO RESTAURANT'

def test_fine_string():
    assert clean('ABC DEF') == 'ABC DEF'


# class CCCleanerTests(unittest.TestCase):


#     def test_remove_state_at_end(self):
#         self._clean_and_assert(
#             raw='MASSAGE ENVY       IN',
#             clean='MASSAGE ENVY',
#             city=None,
#             state='IN')

#     def test_remove_city_and_state_at_end(self):
#         self._clean_and_assert(
#             raw='REDAPPLE PROMENADE       SEATTLE      WA',
#             clean='REDAPPLE PROMENADE',
#             city='SEATTLE',
#             state='WA')
#         self._clean_and_assert(
#             raw='SAND OPTICS            MIAMI        FL',
#             clean='SAND OPTICS',
#             city='MIAMI',
#             state='FL')
#         self._clean_and_assert(
#             raw='TGI FRIDAYS            DULUTH       GA',
#             clean='TGI FRIDAYS',
#             city='DULUTH',
#             state='GA')

#     def test_remove_city_and_state_at_end_combined(self):
#         self._clean_and_assert(
#             raw='REDAPPLE PROMENADE       SEATTLEWA',
#             clean='REDAPPLE PROMENADE',
#             city='SEATTLE',
#             state='WA')
#         self._clean_and_assert(
#             raw='TARTINE LP               SAN FRANCISCOCA',
#             clean='TARTINE LP',
#             city='SAN FRANCISCO',
#             state='CA')
#         self._clean_and_assert(
#             raw='GYU-KAKU RESTAURANT    NEW YORK CITYNY',
#             clean='GYU-KAKU RESTAURANT',
#             city='NEW YORK CITY',
#             state='NY')

#     def test_remove_city_words_combined(self):
#         self._clean_and_assert(
#             raw='CB2 #53 SANFRANCISCO CA',
#             clean='CB2',
#             city='SAN FRANCISCO',
#             state='CA')
#         self._clean_and_assert(
#             raw='CB2 #53 NEW YORKCITY NY',
#             clean='CB2',
#             city='NEW YORK CITY',
#             state='NY')
#         # self._clean_and_assert(
#         #     'PAYBYPHONE SANFRANCISCO CA',
#         #     'PAYBYPHONE')
#         # self._clean_and_assert(
#         #     'ETIHADAIR   6077221178 NEWYORK      NY',
#         #     'ETIHADAIR')
#         # self._clean_and_assert(
#         #     'ETIHADAIR   6077221178 NEWYORKCITY      NY',
#         #     'ETIHADAIR')
#         # self._clean_and_assert(
#         #     'ETIHADAIR   6077221178 NEWYORK CITY      NY',
#         #     'ETIHADAIR')
#         # self._clean_and_assert(
#         #     'ETIHADAIR   6077221178 NEW YORKCITY      NY',
#         #     'ETIHADAIR')

#     def test_remove_cutoff_city_state_combined(self):
#         self._clean_and_assert(
#             raw='EL TAMBO GRILL         MIAMFL',
#             clean='EL TAMBO GRILL',
#             city='MIAMI',
#             state='FL')
#         self._clean_and_assert(
#             raw='NIJIYA MARKET #35      WEST LOS ANGECA',
#             clean='NIJIYA MARKET WEST',
#             city='LOS ANGELES',
#             state='CA')
#         self._clean_and_assert(
#             raw='SFO UAL DOMEST617348 5 SOUTH SAN FRACA',
#             clean='SFO UAL 5',
#             city='SOUTH SAN FRANCISCO',
#             state='CA')

#     def test_remove_phone_number(self):
#         self._clean_and_assert(
#             raw='GEICO                 800-841-3000',
#             clean='GEICO')
#         self._clean_and_assert(
#             raw='MASSAGE ENVY 0518      317-8733909',
#             clean='MASSAGE ENVY')
#         self._clean_and_assert(
#             raw='MASSAGE ENVY 0518      317-8733909',
#             clean='MASSAGE ENVY')

#     def test_remove_xxxxx(self):
#         self._clean_and_assert(
#             raw='TARGET        00024869  XXXXX0043',
#             clean='TARGET')

#     def test_remove_hashtag_number(self):
#         self._clean_and_assert(
#             raw='WAL-MART #4593',
#             clean='WAL-MART')
#         self._clean_and_assert(
#             raw='LITTLE GIANT BP #4',
#             clean='LITTLE GIANT BP')

#     def test_do_not_remove_number_on_first_place(self):
#         self._clean_and_assert(
#             raw='98 LIQUOR LLC            HATTIESBURG  MS',
#             clean='98 LIQUOR LLC',
#             city='HATTIESBURG',
#             state='MS')

#     def test_remove_hashtag_number_with_space(self):
#         self._clean_and_assert(
#             raw='TRUGREEN  # 5685          859-283-1922 KY',
#             clean='TRUGREEN',
#             city=None,
#             state='KY')

#     def test_remove_hashtag_number_when_missing_spaces(self):
#         self._clean_and_assert(
#             raw='WAL-MART SUPERCENTER#1770CRANBERRY TWPPA',
#             clean='WAL-MART SUPERCENTER',
#             city='CRANBERRY TWP',
#             state='PA')

#     def test_remove_numbers_of_length_ge_3(self):
#         self._clean_and_assert(
#             raw='USPS 11586400935211440',
#             clean='USPS')
#         self._clean_and_assert(
#             raw='QT 1099       97010995',
#             clean='QT')

#     def test_remove_strings_with_at_least_four_digits(self):
#         self._clean_and_assert(
#             raw='OLD NAVY CANADA 5488SC SCARBOROUGH CAN',
#             clean='OLD NAVY CANADA SCARBOROUGH',
#             city=None,
#             state='CAN')

#     def test_remove_numbers_in_word_if_followed_by_city_name(self):
#         self._clean_and_assert(
#             raw='THE MEAN FIDDLER 00000009CRANE     IN',
#             clean='THE MEAN FIDDLER',
#             city='CRANE',
#             state='IN')
#         self._clean_and_assert(
#             raw='THE MEAN FIDDLER 00000009NEW YORK     NY',
#             clean='THE MEAN FIDDLER',
#             city='NEW YORK',
#             state='NY')
#         self._clean_and_assert(
#             raw='THE MEAN FIDDLER00000009NEW YORK     NY',
#             clean='THE MEAN FIDDLER',
#             city='NEW YORK',
#             state='NY')
#         self._clean_and_assert(
#             raw='THE MEAN FIDDLER00000009Croton On Hudson    NY',
#             clean='THE MEAN FIDDLER',
#             city='CROTON ON HUDSON',
#             state='NY')

#     def test_remove_city_with_multiple_words_and_state_at_end(self):
#         self._clean_and_assert(
#             raw='TARGET        00024869  XXXXX0043 SOUTH LEBANON OH',
#             clean='TARGET',
#             city='SOUTH LEBANON',
#             state='OH')
#         self._clean_and_assert(
#             raw='KROGER 408  XXXXX0045 CINCINNATI OH',
#             clean='KROGER',
#             city='CINCINNATI',
#             state='OH')

#     def test_remove_asterisk(self):
#         self._clean_and_assert(
#             raw='BWI*BOINGO WIRELESS',
#             clean='BWI BOINGO WIRELESS')

#     def test_remove_hashtag_at_start(self):
#         self._clean_and_assert(
#             raw='#NETFLIX.COM',
#             clean='NETFLIX.COM')

#     def test_remove_hashtags(self):
#         self._clean_and_assert(
#             raw='AMI*AMICA INSURANCEAP 800-242-6422 RI',
#             clean='AMI AMICA INSURANCEAP',
#             city=None,
#             state='RI')

#     def test_remove_special_case_checkcard(self):
#         self._clean_and_assert(
#             raw='CHECKCARD  0107 STARBUCKS #11203                            Brentwood    TN 55432864007000103416859',
#             clean='STARBUCKS',
#             city='BRENTWOOD',
#             state='TN')

#         self._clean_and_assert(
#             raw='CHECKCARD  0113 ABC*WORKOUT ANYTI                           800-6226290  TN 75418234013005367476022',
#             clean='ABC WORKOUT ANYTI',
#             city=None,
#             state='TN')

#     def test_remove_special_case_paypal(self):
#         self._clean_and_assert(
#             raw='PAYPAL *WIKIMEDIAFO 4158396885 CA',
#             clean='WIKIMEDIAFO',
#             city=None,
#             state='CA',
#             processor='PAYPAL')
#         self._clean_and_assert(
#             raw='PAYPAL *KENNYCHI05',
#             clean='KENNYCHI05',
#             processor='PAYPAL')

#         self._clean_and_assert(
#             raw='PAYPAL GIOVANNICAR  XXXXX0046 XXXXX7733 CA',
#             clean='GIOVANNICAR',
#             city=None,
#             state='CA',
#             processor='PAYPAL')

#     def test_remove_special_case_purchase(self):
#         self._clean_and_assert(
#             raw='PURCHASE - SIXPACKSHORTCUTS.COM     AUSTIN       TX',
#             clean='SIXPACKSHORTCUTS.COM',
#             city='AUSTIN',
#             state='TX')

#     def test_remove_special_case_sq(self):
#         self._clean_and_assert(
#             raw='SQ *MARQUETTE WINES IN   MARQUETTE    MI',
#             clean='MARQUETTE WINES IN',
#             city='MARQUETTE',
#             state='MI',
#             processor='SQ')

#     def test_canada(self):
#         self._clean_and_assert(
#             raw='HUDSON NEWS              EDMONTON     AB',
#             clean='HUDSON NEWS',
#             city='EDMONTON',
#             state='AB')

#     def test_abc(self):
#         self._clean_and_assert(
#             raw='QVC*435247572602*      800-367-9444 PA',
#             clean='QVC',
#             city=None,
#             state='PA')

#     def test_remove_q_variants(self):
#         self._clean_and_assert(
#             raw="TRADER JOE'S #662 QPS WASHINGTON DC",
#             clean="TRADER JOE'S",
#             city="WASHINGTON",
#             state='DC'
#         )

#     # '
#     # #SUNPASS OPERATIONS    561-2189574  FL
#     # #GEICO                 800-841-3000 DC
#     # MASSAGE ENVY 0518      317-8733909  IN
#     # '

#     def test_multiple(self):
#         self._clean_and_assert(
#             raw='TARGET        00024869  XXXXX0043 SOUTH LEBANON OH',
#             clean='TARGET',
#             city='SOUTH LEBANON',
#             state='OH')

#     def test_remove_debit_statements(self):
#         self._clean_and_assert(
#             raw='Debit PIN Purchase MTA VENDING MACHINES NEW YORK NY',
#             clean='MTA VENDING MACHINES',
#             city='NEW YORK',
#             state='NY')
#         self._clean_and_assert(
#             raw='Debit Card Purchase AMAZON MKTPLACE PMTS AMZN.COM/BILL WA',
#             clean='AMAZON MKTPLACE PMTS AMZN.COM/BILL',
#             city=None,
#             state='WA')

#     def test_remove_financial_statements(self):
#         self._clean_and_assert(
#             raw='Interest on Purchases',
#             clean='',
#             city=None,
#             state=None)

#     def test_remove_usa(self):
#         self._clean_and_assert(
#             raw='ROYAL FARMS #150 WHITE MARSH MD 21162 USA',
#             clean='ROYAL FARMS',
#             city='WHITE MARSH',
#             state='MD')
#         self._clean_and_assert(
#             raw='USPS CHANGE OF66100959 MEMPHIS TN 38188 USA',
#             clean='USPS CHANGE',
#             city='MEMPHIS',
#             state='TN')

#     def test_remove_all_after_city_and_state_that_follow_two_words(self):
#         self._clean_and_assert(
#             raw='SHELL OIL 57444468805 CLAREMONT CA - SERVICE STATIONS (WITH OR W/O ANCILLARY SERVICES)',
#             clean='SHELL OIL',
#             city='CLAREMONT',
#             state='CA')
#         self._clean_and_assert(
#             raw='EL REY PHILADELPHIA PA - EATING PLACES, RESTAURANTS',
#             clean='EL REY',
#             city='PHILADELPHIA',
#             state='PA')
#         self._clean_and_assert(
#             raw='KEAN COFFEE NEWPORT BEACH CA 926600000 USA',
#             clean='KEAN COFFEE',
#             city='NEWPORT BEACH',
#             state='CA')

#     def _clean_and_assert(self, raw, clean, city=None, state=None, location=None, processor=None):
#         cleaned_raw, found_city, found_state, found_location, found_processor = cc_cleaner.clean(raw,testing=True)
#         self.assertEqual(clean, cleaned_raw)
#         self.assertEqual(city, found_city)
#         self.assertEqual(state, found_state)
#         self.assertEqual(location, found_location)
#         self.assertEqual(processor, found_processor)
