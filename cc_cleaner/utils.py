

def substring_between(value, a, b):
    """
    Get Substring between
    :param value: value to search
    :param a: substring search start string
    :param b: substring search end string
    :return: a the substring between the two given values/
    """
    # Find and validate before-part.
    pos_a = value.find(a)
    if pos_a == -1: return ""
    # Find and validate after part.
    pos_b = value.rfind(b)
    if pos_b == -1: return ""
    # Return middle part.
    adjusted_pos_a = pos_a + len(a)
    if adjusted_pos_a >= pos_b: return ""
    return value[adjusted_pos_a:pos_b]


def substring_before(value, a):
    """
    Get substring before the search string.
    :param value: string that's being searched
    :param a: search string
    :return: found substring
    """
    # Find first part and return slice before it.
    pos_a = value.find(a)
    if pos_a == -1: return ""
    return value[0:pos_a]


def substring_after(value, a):
    """
    Retrieve substring after the search string
    :param value: string that's being searched
    :param a: search string
    :return: found substring
    """
    # Find and validate first part.
    pos_a = value.rfind(a)
    if pos_a == -1: return ""
    # Returns chars after the found string.
    adjusted_pos_a = pos_a + len(a)
    if adjusted_pos_a >= len(value): return ""
    return value[adjusted_pos_a:]
