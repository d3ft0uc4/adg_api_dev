"""
© 2018 Alternative Data Group. All Rights Reserved.

Tools for running `raw_cc` entries through CCCleaner and saving outputs to `clean_cc` table.

Usage from console:
    python -m cc_cleaner.run_cc_cleaner
"""
import os
import sqlalchemy

from datetime import datetime
from sqlalchemy import Column, DateTime, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.session import sessionmaker

from cc_cleaner.clean_cc import CCCleaner
from mapper.settings import db_engine

from abc import ABC

import csv


Base = declarative_base()
Session = sessionmaker()


class RawCC(Base):
    __tablename__ = 'raw_cc'

    uid = Column(String, primary_key=True)
    raw_trans_string = Column(String)
    merchant_string_td2 = Column(String)
    merchant_string_dc0 = Column(String)
    merchant_string_dc1 = Column(String)
    merchant_string_td0 = Column(String)
    td_ticker = Column(String)
    td_err_merchantname = Column(String)
    cardtype = Column(String)


class CleanCC(Base):
    __tablename__ = 'clean_cc'

    uid_raw_cc = Column(String, primary_key=True)
    cc_datestamp = Column(DateTime, primary_key=True)
    raw_trans_string = Column(String)
    clean_trans_string = Column(String)
    city = Column(String)
    state = Column(String)
    version = Column(String)
    merchant_string_td2 = Column(String)
    merchant_string_dc0 = Column(String)
    merchant_string_dc1 = Column(String)
    merchant_string_td0 = Column(String)
    location = Column(String)
    processor = Column(String)


VERSION = '0.0.8'
NUM_ROWS = 1000000000

class CCCleanBaseRunner(ABC):

    _cc_cleaner = CCCleaner()

    def run(self):
        pass


class CCCleanCSVRunner(CCCleanBaseRunner):
    #todo implement option from argparse to take in csv location data

    def run(self):

        timestamp = datetime.now()


        dir_path = os.path.dirname(os.path.realpath(__file__))

        csv_data_file = os.path.join(dir_path,'data','cleaner_qa.csv')

        if os.path.exists(csv_data_file):
            print("Found Cleaner_qa.csv")

        print("Loading rows from {0}".format(csv_data_file))
        clean_csv_data = []

        with open(csv_data_file,'r+') as csv_file:
            csv_reader = csv.reader(csv_file,delimiter=',')

            for row in csv_reader:
                column = []
                for _c in row:
                    column.append(_c)


                row_number = column[0]

                input_data = column[1]

                previously_cleaned = column[2]


                new_clean_data, found_city, found_state, found_location, found_processor = self._cc_cleaner.clean(previously_cleaned)

                clean_csv_data.append([row_number,input_data,previously_cleaned,new_clean_data])
                print("Saved Cleaned [ {0} ] -- {1}".format(input_data,new_clean_data))

        new_csv_data_file = os.path.join(dir_path,'data','cleanest_qa.csv')

        if os.path.exists(new_csv_data_file):
            os.remove(new_csv_data_file)
            print("removed previous csv file")

        with open(os.path.join(dir_path,'data','cleanest_qa.csv'), 'w+') as csv_file:
            print("Writing CSV Data to File")


            csv_writer = csv.writer(csv_file,delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)

            for row in clean_csv_data:
                print(",".join(row))
                csv_writer.writerow(row)

        print("CSV Data written")

class CCCleanDatabaseRunner(CCCleanBaseRunner):

    def run(self):
        datestamp_now = datetime.now()

        session = self._setup_db()

        print('Loading rows from raw_cc')
        raw_rows = self._load_raw_rows(session)
        print('Raw rows loaded')
        for i, raw_row in enumerate(raw_rows):
            if i % 100 == 0 and i != 0:
                print('Running {} out of {}, {:.2f}%'.format(i, len(raw_rows), (i * 100 / len(raw_rows))))
                session.commit()

            clean_string, city, state, location, processor = self._cc_cleaner.clean(raw_row.raw_trans_string)

            row = CleanCC(
                uid_raw_cc=raw_row.uid,
                cc_datestamp=datestamp_now,
                raw_trans_string=raw_row.raw_trans_string,
                clean_trans_string=clean_string,
                city=city,
                state=state,
                version=VERSION,
                merchant_string_td2=raw_row.merchant_string_td2,
                merchant_string_dc0=raw_row.merchant_string_dc0,
                merchant_string_dc1=raw_row.merchant_string_dc1,
                merchant_string_td0=raw_row.merchant_string_td0,
                location=location,
                processor=processor,
            )
            session.add(row)
        session.commit()

    def _setup_db(self):
        Session.configure(bind=db_engine)
        return Session()

    def _load_raw_rows(self, session):
        rows = session.query(RawCC.uid, RawCC.raw_trans_string, RawCC.merchant_string_td2, RawCC.merchant_string_dc0,
                             RawCC.merchant_string_dc1, RawCC.merchant_string_td0) \
            .filter((RawCC.merchant_string_td2 != None) | (RawCC.merchant_string_dc0 != None) |
                    (RawCC.merchant_string_dc1 != None) | (RawCC.merchant_string_td0 != None)) \
            .limit(NUM_ROWS).all()
        return list(rows)


if __name__ == '__main__':
    CCCleanCSVRunner().run()

