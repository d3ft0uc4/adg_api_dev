# Adds "long" state column to cities_states.csv and outputs to cities_states_description.csv
import pandas as pd
import us

df = pd.read_csv('cities_states.csv', header=None)

def short_to_long(state):
    try:
        return getattr(us.states, state).name
    except:
        pass

    return {
        'AE': 'U.S. Armed Forces – Europe',
        'AP': 'U.S. Armed Forces – Pacific',
        'FM': 'Micronesia',
        'MH': 'Marshall Islands',
        'PW': 'Palau',
        'AB': 'Alberta',
        'BC': 'British Columbia',
        'MB': 'Manitoba',
        'NB': 'New Brunswick',
        'NL': 'Newfoundland and Labrador',
        'NT': 'Northwest Territories',
        'NS': 'Nova Scotia',
        'NU': 'Nunavut',
        'ON': 'Ontario',
        'PE': 'Prince Edward Island',
        'QC': 'Quebec',
        'SK': 'Saskatchewan',
        'YT': 'Yukon',
        'CAN': 'Canada',
    }[state]



df[2] = df.apply(lambda row: short_to_long(row[1]), axis=1)

df.to_csv('cities_states_description.csv', index=False, header=None)
