from cc_cleaner.address_detection import AddressExtractor


def test_street_extraction():
    answers = [
        # raw_input,                     street,               not street
        ('CONVENE AT 777 SOUTH FIGU',    '777 SOUTH FIGU',     'CONVENE AT'),
        ('400 BEDFORD AVENUE',           '400 BEDFORD AVENUE', ''),
        ('FOUR TIMES SQUARE ASSOC  LLC', '',                   'FOUR TIMES SQUARE ASSOC  LLC'),
        ('157 MAIN STREET',              '157 MAIN STREET',    ''),
    ]

    for raw_input, correct_street, correct_other in answers:
        street, other = AddressExtractor(raw_input).extract_street()
        assert street.strip() == correct_street and other.strip() == correct_other, \
            f'Error for "{raw_input}": street="{street}", other="{other}"'
