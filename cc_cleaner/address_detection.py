"""
© 2019 Alternative Data Group. All Rights Reserved.

Class for detection address parts from raw string.

Usage example:
    from cc_cleaner.address_detection import AddressExtractor
    raw_cc = "Papa John's Pizza 1632 E MAIN ST, KALAMAZOO, MI 49048-1902, US"
    result = AddressExtractor(raw_cc).extract_street()
    print(result)

Usage from console:
    python -m cc_cleaner.address_detection "Papa John's Pizza 1632 E MAIN ST, KALAMAZOO, MI 49048-1902, US"
"""
import logging
import re
from functools import lru_cache
from operator import itemgetter
from typing import Tuple

import usaddress

from packages.utils import logger, cached_method, pretty

logger = logging.getLogger(f'adg.{__name__}')
DEBUG = __name__ == '__main__'


def count_digits(value: str) -> int:
    """ Counts number of digits in string """
    return len(re.findall(r'\d', value))


class AddressExtractor:
    # These are the keys thats ALL needs to be present in the input to start extracting
    KEYS_INSURE_STREET_EXIST = [
        'AddressNumber',
        'StreetName',
        # 'PlaceName' ! Maybe this is too aggressive
    ]

    # ANY of these inputs needs to be present in the input to start extracting
    KEYS_ANY_INSURE_STREET_EXIST = [
        'StreetNamePreDirectional',
        'StreetNamePostType',
        'StateName',
        'ZipCode']

    def __init__(self, input_string):

        self.input_string = input_string
        self.address_parts = None

    @staticmethod
    @lru_cache(maxsize=1)
    def get_bad_words():
        from mapper.string_cleaner_tokenization.tokenizer import DICTIONARY_WORDS
        from mapper.string_cleaner_tokenization.tokenizer import TransactionTokenizer
        from mapper.settings import COMPANY_TERMINATORS

        return DICTIONARY_WORDS + TransactionTokenizer.TRANSACTION_WORDS + COMPANY_TERMINATORS

    @cached_method
    def get_address_parts(self):
        parts = usaddress.parse(self.input_string)

        # now run some additional checks on `parts`
        result = []
        for value, kind in parts:
            checker = getattr(self, f'check_{kind.lower()}', None)
            if not checker or checker(value):
                result.append((value, kind))
            else:
                logger.debug(f'Checker "{checker.__name__}" blocked value "{value}"')
                result.append((value, 'Ambiguous'))

        if DEBUG:
            logger.debug(f'Address parts:\n{pretty(result)}')
        return result

    @staticmethod
    def check_addressnumber(value: str) -> bool:
        return any(map(str.isdigit, value))

    @staticmethod
    def check_zipcode(value: str) -> bool:
        return 5 <= len(list(filter(str.isdigit, value))) <= 9

    def check_recipient(self, value: str) -> bool:
        return value not in self.get_bad_words()

    def check_streetname(self, value: str) -> bool:
        return value not in self.get_bad_words()

    def check_streetnameposttype(self, value: str) -> bool:
        return value not in self.get_bad_words()

    @cached_method
    def get_address_types(self):
        return list(map(itemgetter(1), self.get_address_parts()))

    @cached_method
    def get_address_values(self):
        return list(map(itemgetter(0), self.get_address_parts()))

    def has_street(self) -> bool:
        return all([
            all([key in self.get_address_types() for key in self.KEYS_INSURE_STREET_EXIST]),
            any(key in self.get_address_types() for key in self.KEYS_ANY_INSURE_STREET_EXIST),
            all(  # require AddressNumber to contain not too many digits
                count_digits(number) <= 4
                for number in map(
                    itemgetter(0), filter(
                        lambda part: part[1] == 'AddressNumber',
                        self.get_address_parts()
                    )
                )
            )
        ])

    def extract_street(self) -> Tuple[str, str]:
        """
        Splits string into address and all other text.

        Returns: tuple of (address string, other string)
        """
        if not self.has_street():
            return '', self.input_string

        parts_types = self.get_address_types()
        terms_to_test = self.KEYS_INSURE_STREET_EXIST + self.KEYS_ANY_INSURE_STREET_EXIST
        street_candidate_indexes = [i for i, typ in enumerate(parts_types) if typ in terms_to_test]

        address_start = min(street_candidate_indexes)
        address_string = ' '.join(self.get_address_values()[address_start:])
        rest_string = ' '.join(self.get_address_values()[:address_start])

        return address_string, rest_string


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('raw_input')
    args = parser.parse_args()

    address, other = AddressExtractor(args.raw_input).extract_street()
    print(f'Address: {address}')
    print(f'Rest: {other}')
