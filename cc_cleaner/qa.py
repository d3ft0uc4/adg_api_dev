from argparse import ArgumentParser
from cc_cleaner.clean_cc import CCCleaner
import pandas as pd

parser = ArgumentParser()
parser.add_argument('raw_inputs', nargs='+')
args = parser.parse_args()

cleaner = CCCleaner()

results = []
total = len(args.raw_inputs)
for i, raw_input in enumerate(args.raw_inputs):
    print(f'{i+1}/{total}: {raw_input}')
    results.append(
        (raw_input, cleaner.clean(raw_input)[0])
    )

df = pd.DataFrame(results)
out_path = '/tmp/cleaner_qa.csv'
df.to_csv(out_path)
print(f'Wrote results to {out_path}')
