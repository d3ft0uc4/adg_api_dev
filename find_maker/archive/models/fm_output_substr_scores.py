from sqlalchemy import Column, DateTime, Float, String

from find_maker.models.base import Base


class FmOutputSubstrScores(Base):
    __tablename__ = 'fm_output_substr_scores'

    uid_fm_output = Column(String, primary_key=True)
    score_substr = Column(Float(precision=4), nullable=False)
    fm_substr_datestamp = Column(DateTime)
