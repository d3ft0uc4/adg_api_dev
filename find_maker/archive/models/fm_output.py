from sqlalchemy import Column, DateTime, Float, JSON, String

from find_maker.models.base import Base


class FmOutput(Base):
    __tablename__ = 'fm_output'

    uid = Column(String, primary_key=True)
    created_at = Column(DateTime)
    uidlist_input_string = Column(String)
    input_string = Column(String)
    search_addon_string = Column(String)
    search_result_type = Column(String)
    uid_target = Column(String)
    target_name = Column(String)
    target_source = Column(String)
    score_type = Column(String)
    search_result_ranks = Column(JSON)
    score = Column(Float(precision=4))
    run_code = Column(String)

    def __str__(self):
        return 'FM output row: {}, {}, {}, {}, {}'.format(
            self.uidlist_input_string, self.search_addon_string, self.search_result_type, self.target_name, self.score)
