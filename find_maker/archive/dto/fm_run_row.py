class FMRunRow:

    def __init__(self, query_type=None, bing_search_string=None,
                 merchant_string=None, uid_merchant_string=None, log=None, json_source=None, uid=None):
        self.query_type = query_type
        self.bing_search_string = bing_search_string
        self.merchant_string = merchant_string
        self.uid_merchant_string = uid_merchant_string
        self.log = log
        self.json_source = json_source
        self.uid = uid
