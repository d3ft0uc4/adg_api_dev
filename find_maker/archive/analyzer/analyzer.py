import sqlalchemy

from collections import defaultdict
from sqlalchemy.orm.session import sessionmaker

from mapper.settings import SQL_CONNECTION_STRING


Session = sessionmaker()
engine = sqlalchemy.create_engine(SQL_CONNECTION_STRING)
Session.configure(bind=engine)

session = Session()

all_targets = [
    "'box', 'jack box', 'mail boxes'",
    "'evans', 'bob evans'",
    "'hsbc', 'hsbc bank', 'hsbc finance'",
    "'snap', 'snap-on', 'snap fitness'",
    "'apple', 'big apple bagels'",
    "'nordstrom', 'nordstrom rack'",
    "'express', 'panda express', 'fedex express', 'petmed express', 'chicken express', 'tapioca express', 'express scripts', 'american express', 'pet food express', 'holiday inn express', 'estes express lines', 'learning express toys', 'chinese gourmet express'",
    "'goody', 'sam goody', 'goody''s stores'",
    "'habit', 'habit burger and grill'",
    "'arco', 'atlantic richfield arco'",
    "'fedex', 'fedex express', 'fedex office'",
    "'honda', 'honda motor'",
    "'total', 'total wine & more', 'bally total fitness'",
    "'bravo', 'brio', 'bravo brio restaurant'",
    "'pantry', 'white han pantry', 'original pantry cafe'",
    "'caesars', 'little caesars'",
    "'champion', 'champion energy'",
    "'warehouse', 'sportsman''s warehouse', 'off broadway shoe warehouse', 'auto parts warehouse'",
    "'vitamin shop', 'vitamin shoppe'",
]

with open('result.csv', 'w') as f:
    for i, targets in enumerate(all_targets):
        data = session.execute(
            """
                SELECT input_string, target_name, target_name_clean, SUM(score), SUM(COALESCE(score_substr, score)) FROM fm_output_substr_calc
                WHERE run_code = 'T2T-2'
                      AND target_name_clean IN (%s)
                    GROUP BY target_name_clean, input_string
                    ORDER BY input_string, target_name
                    LIMIT 10000
            """ % targets)
        print(targets)

        d = defaultdict(dict)

        f.write('%d,%s' % ((i + 1), targets) + '\n\n')

        for row in data:
            input_string = row[0]
            target_name = row[1]
            target_name_clean = row[2]
            score = row[3]
            score_new = row[4]

            d[input_string][target_name] = (score, score_new)

        for input_string, dd in d.items():
            h = ['',] + list(dd.keys())
            # v = [input_string,] + list(dd.values())

            tpls = [(k, s, sn) for k, (s, sn) in dd.items()]
            tpls.sort(key=lambda tpl: -tpl[2])

            if len(tpls) >= 2:
                f.write(input_string + ',,' + ','.join([str(k) for k, _, _ in tpls]) + '\n')
                f.write('orig. scores:,,' + ','.join([str(s) for _, s, _ in tpls]) + '\n')
                f.write('new scores:,,' + ','.join([str(sn) for _, _, sn in tpls]) + '\n\n')
                # print('-' * 80)

        f.write('\n\n\n')
