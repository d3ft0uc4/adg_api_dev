# Created by dgreis at 11/22/17
Feature: FindMaker
  # Find Maker should correctly tally counts

  Scenario: Correct Counts
    Given a json bing result packet
    And a special filtered Find_Maker instance
    When we call Find_Maker.process
    Then the score should equal 2


  #Scenario: Search Results Remain Stable

  Scenario: New Score Matches Old Score
    Given an old set of scores located in
    """
      find_maker/temp/246d86_output
    """
    And the same set of inputs in search results located in
    """
      bing_querier/run_2017_12_08_113031_UTC
    """
    And a matching target set based on a database pull
    When we call full Find_Maker program from version 3
    Then the old scores should match the new scores
