import os


def columns_version_lookup(shape_col):
    if shape_col == 9:
        cols = ['uid','uid_merchant_string','merchant_string','search_addon_string','search_result_type',
                 'uid_target_list','target_name_cleaned','score_type','score']
    elif shape_col == 10:
        cols = ['uid','uid_merchant_string','merchant_string','search_addon_string','search_result_type',
                 'uid_target_list','target_name_cleaned','score_type','search_result_ranks','score']
    elif shape_col == 11:
        cols = ['uid','fm_datestamp','uid_merchant_string','merchant_string','search_addon_string','search_result_type',
                 'uid_target_list','target_name_cleaned','score_type','search_result_ranks','score']
    else:
        print("Unknown FM_Output shape; Please implement.")
        raise Exception
    return cols


def return_filtered_dir_files(dir_path):
    known_bad_guys = ['README.md','.DS_Store']
    return filter(lambda x: x not  in known_bad_guys, os.listdir(dir_path) )


def test_equality(odf, ndf):
    ex_cols = ['uid', 'uid_merchant_string', 'uid_target_list','search_result_ranks','fm_datestamp']
    o = filter_cols(odf, ex_cols)
    n = filter_cols(ndf, ex_cols)
    ois = o['search_addon_string'].value_counts(dropna=False)
    nis = n['search_addon_string'].value_counts(dropna=False)
    assert (ois == nis).values.all()
    o = o.sort_values(['merchant_string','search_addon_string', 'search_result_type', 'target_name_cleaned']).reset_index(drop=True)
    n = n.sort_values(['merchant_string','search_addon_string', 'search_result_type', 'target_name_cleaned']).reset_index(drop=True)
    o.drop('search_addon_string',axis=1,inplace=True)
    n.drop('search_addon_string',axis=1,inplace=True)
    n['score'] = n['score'].astype(float)
    o['score'] = o['score'].astype(float)
    return o.equals(n)

def filter_cols(df, ex_cols):
    d = df[filter(lambda x: x not in ex_cols, df.columns)]
    return d