import os
import json
import imp
import datetime
import pandas as pd

from behave import given, when, then
from hamcrest import assert_that, equal_to

from find_maker.find_maker import FindMaker
from packages.search_results import SearchResults
from packages.targets import Targets
from packages.utils import find_data_dir_abs_path, find_root_dir
from find_maker.features.steps.helper_fns import columns_version_lookup, return_filtered_dir_files, test_equality
from packages.settings import TOP_LEVEL_PACKAGE_NAME, REL_DATA_DIR
from packages.str_mapping import map_all_str

@given(u'a json bing result packet')
def step_impl(context):
    search_results = SearchResults(load_from='disk')
    contents = search_results.contents
    contents = {k: v for k, v in contents.items() if 'Amazon.com' in contents[k]['input']}
    #del(contents[0]['search_results']['manufacturer'])
    #del(contents[0]['search_results']['original'])
    #contents[0]['search_results']['company']['results'] = [
    #    {u'displayUrl': u'https://www.creditcardcatalog.com/capital-one-credit-card-payment...'},
    #    {u'displayUrl': u'https://www.doxo.com/info/capital-one-financial-corp'}]
    context.contents = contents

@given(u'a special filtered FindMaker instance')
def step_impl(context):
    fm = FindMaker( datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    score_types=['unweighted'],
                    search_result_types=['snippet'],
                    query_types=['company'])
    target_list = ['amazon.com']
    t_container = Targets(load_from='database')
    t_contents = dict([(str(i), target_list[i]) for i in range(len(target_list))])  # fake uids
    t_container.contents = t_contents  # override
    fm.set_targets_container(t_container)
    context.fm = fm

@when(u'we call FindMaker.process')
def step_impl(context):
    fm = context.fm
    uid_jsons = context.contents.items()
    for uid_json in uid_jsons:
        fm.process(uid_json)
    context.fm = fm

@then(u'the score should equal {int_input}')
def step_impl(context,int_input):
    integer = eval(int_input)
    fm = context.fm
    df = fm.output_fm_out_tbl(how='dataframe')
    score = df.loc[0,'score']
    assert_that(score, equal_to(integer))

@given(u'an old set of scores located in')
def step_impl(context):
    data_dir = find_data_dir_abs_path(TOP_LEVEL_PACKAGE_NAME,REL_DATA_DIR)
    old_fm_output_dir_path = data_dir + '/' + context.text.strip()
    old_fm_output_files = return_filtered_dir_files(old_fm_output_dir_path)
    context.old_fm_output_files = old_fm_output_files
    odf = pd.concat([pd.read_csv(old_fm_output_dir_path + '/' + f,sep="|",header=None) for f in old_fm_output_files])
    shape_col = odf.shape[1]
    df_columns = columns_version_lookup(shape_col)
    odf.columns = df_columns
    context.odf = odf


if __name__ == '__main__':
    @given(u'the same set of inputs in search results located in')
    def step_impl(context):
        data_dir = find_data_dir_abs_path(TOP_LEVEL_PACKAGE_NAME,REL_DATA_DIR)
        search_jsons_dir_path = data_dir + '/' + context.text.strip()
        search_json_files = [search_jsons_dir_path + '/' + sjf for sjf in return_filtered_dir_files(search_jsons_dir_path)]
        context.search_json_files = search_json_files
        search_jsons = list()
        for sjf in search_json_files:
            with open(sjf, 'r') as fp:
                jsn = json.load(fp)
                search_jsons.append(jsn)
        context.search_jsons = search_jsons
        json_merch_strings = set([jsn['input'] for jsn in search_jsons])
        odf = context.odf
        odf_merch_strings = set(odf['merchant_string'].unique().tolist())
        if "McDonald's" not in odf_merch_strings:
            print("popping McDonald's out of json input string set")
            json_merch_strings.remove("McDonald's")
        assert_that(odf_merch_strings,equal_to(json_merch_strings))
        #There is no reliable search uid, so user must ensure
        #the results are static between the two runs
        #the best way to do this is to run this scenario
        #immediately after running FindMaker for the
        #commit you're interested in validating against.
        #Then make sure you've pointed the program to the
        #folder with the same set of jsons


@given(u'a matching target set based on a database pull')
def step_impl(context):
    t_container = Targets(load_from='database')
    target_list = t_container.contents.values()
    context.target_list = target_list
    odf = context.odf
    odf_targets = set(odf['target_name_cleaned'].tolist())
    target_set = set(target_list)
    assert_that(odf_targets,equal_to(target_set))

@when(u'we call full FindMaker program from version {int_input}')
def step_impl(context,int_input):
    ver = eval(int_input)
    if ver == 0:   # Note version 0 is when findmaker was refactored from wide to long
        print("Running Find Maker Version 0, i.e. when findmaker was first refactored from wide to long form")
        root_dir = find_root_dir(TOP_LEVEL_PACKAGE_NAME)
        find_maker_v0 = imp.load_source('unused_arg', root_dir + '/find_maker.py')
        find_maker_v0.fm_csv_path = root_dir + '/find_maker/data/temp/fm_output/' #because there's nothing older than this
        tdf = pd.DataFrame({ 'targets_input': context.target_list})
        targets = map_all_str( tdf['targets_input'] , invertdict=True)
        allJSON = context.search_json_files
        [find_maker_v0.find_maker(j, targets, load_json=True, write_to_file=True) for j in allJSON]
        print("Done running Find Maker Version 0")
    elif ver == 1 or ver == 2:
        print("Running Version 1/2 of FindMaker")
        root_dir = find_root_dir(TOP_LEVEL_PACKAGE_NAME)
        find_maker_module = imp.load_source('unused_arg',root_dir + '/find_maker/find_maker.py')
        search_jsons = context.search_jsons
        contents = search_jsons         #Up to user to make sure these point to same JSONs asthose used to generate output in old fm output dir
        t_container = Targets(load_from='disk')
        tdf = pd.DataFrame({ 'targets_input': context.target_list})
        t_container.contents = map_all_str(tdf['targets_input'], invertdict=True) #Override
        targets = t_container.contents
        fm = find_maker_module.FindMaker()
        fm.set_targets(targets)
        cnt = 0
        for jsn in contents:
            fm.process(jsn)
            print("done processing jsn " + str(cnt + 1) + "/" + str(len(contents)))
            cnt += 1
        find_maker_module.FIND_MAKER_OUTPUT_DIR = '/find_maker/temp/fm_output'
        if ver == 1:
            fm.output(how='disk')
        if ver == 2:
            fm.output_fm_out_tbl(how='disk')
        print("Done running Find Maker Version 1/2")
    elif ver == 3:
        print("Running Version 3 of FindMaker")
        root_dir = find_root_dir(TOP_LEVEL_PACKAGE_NAME)
        find_maker_module = imp.load_source('unused_arg',root_dir + '/find_maker/find_maker.py')
        search_jsons = context.search_jsons
        contents = dict([(str(i),search_jsons[i]) for i in range(len(search_jsons))])  #fake uids
        t_container = Targets(load_from='database')
        target_list = context.target_list
        t_contents = dict([(str(i),target_list[i]) for i in range(len(target_list))]) #fake uids
        t_container.contents = t_contents #override
        fm = find_maker_module.FindMaker(datetime.datetime.now())
        fm.set_targets_container(t_container)
        cnt = 0
        for uid_jsn in contents.items():
            fm.process(uid_jsn)
            print("done processing jsn " + str(cnt + 1) + "/" + str(len(contents)))
            cnt += 1
        fm.output_fm_out_tbl(how='disk')
        print("Done with version 3 of FindMaker")


@then(u'the old scores should match the new scores')
def step_inpl(context):
    #root_dir = find_root_dir(TOP_LEVEL_PACKAGE_NAME)
    data_dir = find_data_dir_abs_path(TOP_LEVEL_PACKAGE_NAME,REL_DATA_DIR)
    new_output_dir = data_dir + '/find_maker/temp/fm_output'
    new_output_files = [new_output_dir + '/' + nf for nf in return_filtered_dir_files(new_output_dir)]
    ndf = pd.concat([pd.read_csv(nof, sep="|",header=None) for nof in new_output_files])
    cols_ndf = columns_version_lookup(ndf.shape[1])
    ndf.columns = cols_ndf
    odf = context.odf
    if len(new_output_files) < len(context.old_fm_output_files):
        print("Dropoff in input files, probably due to legacy file casing issue; equalizing input sizes...")
        set_odf = set(odf['merchant_string'])
        set_ndf = set(ndf['merchant_string'])
        to_kill = list(set_odf.difference(set_ndf))
        old_len = len(odf)
        odf = odf[~odf['merchant_string'].isin(to_kill)]
        new_len = len(odf)
        print("length of odf was: " + str(old_len) + ", now " + str(new_len))
    elif len(new_output_files) > len(context.old_fm_output_files):
        print("Dropoff in input files, probably due to legacy file casing issue; equalizing input sizes...")
        set_odf = set(odf['merchant_string'])
        set_ndf = set(ndf['merchant_string'])
        to_kill = list(set_ndf.difference(set_odf))
        old_len = len(ndf)
        ndf = ndf[~ndf['merchant_string'].isin(to_kill)]
        new_len = len(ndf)
        print("length of ndf was: " + str(old_len) + ", now " + str(new_len))
    result = test_equality(odf,ndf)
    assert_that(result,equal_to(True))





