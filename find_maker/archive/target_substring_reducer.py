import itertools
import sqlalchemy

from collections import defaultdict
from datetime import datetime

from sqlalchemy.orm.session import sessionmaker

from find_maker.models.fm_output import FmOutput
from find_maker.models.fm_output_substr_scores import FmOutputSubstrScores
from mapper.settings import SQL_CONNECTION_STRING

Session = sessionmaker()


class TargetSubstringReducer:
    """
    Reduces the score of a target for a certain input string, if supertargets are present for that input string.
    The formula: new_score = target_score - max(supertarget1_score, supertarget2_score, ...)

    We want to reduce the score in this case because it's contained in every instance of supertargets and will
    inherently have a bigger score.

    These new scores are written to `fm_output_substr_scores`. To see all data, use a view `fm_output_substr_calc`.
    """

    def run(self):
        now = datetime.now()

        engine = sqlalchemy.create_engine(SQL_CONNECTION_STRING)
        Session.configure(bind=engine)

        # base_filter = FmOutput.created_at == '2018-01-23 23:24:20'  # MERCHANT-TO-TARGET
        # base_filter = FmOutput.created_at == '2018-01-19 20:06:35'  # TARGET-TO-TARGET
        base_filter = FmOutput.run_code == 'T2T-2'

        session = Session()
        targets = session.query(FmOutput.target_name.distinct()).filter(base_filter).all()
        targets = [x[0] for x in targets]

        target_to_superstring_target = defaultdict(list)

        # Iterate all pairs of targets.
        for target1, target2 in itertools.combinations(targets, 2):
            target_words_1 = target1.split()
            target_words_2 = target2.split()
            if all((w in target_words_2) for w in target_words_1):
                target_to_superstring_target[target1].append(target2)

        i = 1
        num_targets = len(target_to_superstring_target)
        for target, superstring_targets in target_to_superstring_target.items():
            print('{} out of {}, {:.2}%'.format(i, num_targets, i / num_targets * 100))
            i += 1

            uidlist_input_strings = session.query(FmOutput.uidlist_input_string.distinct()) \
                .filter(FmOutput.target_name == target).filter(base_filter).all()
            uidlist_input_strings = [r[0] for r in uidlist_input_strings]

            all_rows = session.query(FmOutput.uidlist_input_string,
                                     FmOutput.search_addon_string,
                                     FmOutput.search_result_type,
                                     FmOutput.score_type,
                                     FmOutput.score) \
                .filter(FmOutput.uidlist_input_string.in_(uidlist_input_strings),
                        FmOutput.target_name.in_(superstring_targets)) \
                .filter(base_filter) \
                .all()

            # print(target, len(uidlist_input_strings))
            for uidlist_input_string in uidlist_input_strings:

                substring_rows = session.query(FmOutput.uid,
                                               FmOutput.search_addon_string,
                                               FmOutput.search_result_type,
                                               FmOutput.score_type,
                                               FmOutput.score) \
                    .filter(FmOutput.uidlist_input_string == uidlist_input_string,
                            FmOutput.target_name == target) \
                    .filter(base_filter) \
                    .all()

                rows = [r for r in all_rows if r.uidlist_input_string == uidlist_input_string]

                if rows:
                    data = defaultdict(list)
                    for row in rows:
                        data[(row.search_addon_string, row.search_result_type, row.score_type)].append(row.score)
                    for key, scores in data.items():
                        search_addon_string, search_result_type, score_type = key
                        max_score = max(scores)

                        rr = [r for r in substring_rows
                              if (r.search_addon_string == search_addon_string and
                                  r.search_result_type == search_result_type and
                                  r.score_type == score_type)]
                        if rr:
                            substring_row = rr[0]

                            new_row = FmOutputSubstrScores(uid_fm_output=substring_row.uid,
                                                           score_substr=substring_row.score - max_score,
                                                           fm_substr_datestamp=now)
                            session.add(new_row)

            session.commit()


if __name__ == '__main__':
    TargetSubstringReducer().run()
