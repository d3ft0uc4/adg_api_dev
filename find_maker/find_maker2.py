"""
© 2019 Alternative Data Group. All Rights Reserved.

Examples:
    python -m find_maker.find_maker2 "Kirkland Signature"
    python -m find_maker.find_maker2 --generate-training-file
"""

import csv
import logging
import re
from collections import defaultdict
from operator import itemgetter
from os import path
from typing import List, Dict, Tuple, Iterable

from bane.bane2 import Bane
from bing.bing_web_core import BingWebCore
from mapper.settings import DROPBOX
from packages.targets import TARGETS
from packages.utils import get_timestamp, pretty
from packages.utils import prefixize, tokenize
from packages.xgboost.prediction import xgb_predict, xgb_load_model
from packages.xgboost.training_data import generate_training_data
from .preprocessing import preprocessed_text, preprocessed_url

logger = logging.getLogger(f'adg.{__name__}')
DATA_DIR = path.join(path.dirname(path.abspath(__file__)), 'data')


class FindMaker:
    bing = BingWebCore()
    model = xgb_load_model(path.join(DATA_DIR, 'xgb_boost.find_maker.2019.06.04.23.34.pickle'))

    def __init__(
        self,
        bing_fields: List[str] = ('about', 'url', 'name', 'snippet'),
        search_queries: List[str] = (
            '{}',
            '{} company',
            'who owns {}',
            '{} parent company',
            '{} brands',
            '{} sister company',
        ),
        targets: Iterable[str] = None,
    ):
        self.bing_fields = bing_fields
        self.search_queries = search_queries
        self.targets = targets or list(TARGETS.get_all().keys())

        self.prefix_len = 4
        self.targets_by_prefix = prefixize(self.targets, self.prefix_len)

    def count_targets_in_text(self, text: str) -> Dict[str, int]:
        """
        Counts how much each target from `self.targets` occurs in text.

        Args:
            text: any string

        Returns: dictionary {target: how much the target occurs in text}
        """
        words = tokenize(text)
        prefixes = {word[:self.prefix_len] for word in words}

        candidates = []
        for prefix in prefixes:
            candidates.extend(self.targets_by_prefix[prefix])

        if not candidates:
            return {}

        text = ' '.join(words)
        results = {}
        for candidate in candidates:
            count = len(re.findall(rf'(?=(\s|^){re.escape(candidate)}(\s|$))', text))
            if count:
                results[candidate] = count

        return results

    @staticmethod
    def preprocess_text(text):
        """ Text preprocessing before counting number of occurrences of candidates """
        return preprocessed_text(text)

    @staticmethod
    def preprocess_url(url):
        """ Url preprocessing before counting number of occurrences of candidates """
        return preprocessed_url(url)

    def count_targets(self, value: str) -> List:
        """
        Given any value, queries bing and extracts ranks of targets in different bing row fields.

        Args:
            value: any string

        Returns: list of
            {
                'target': target name,
                'query': query type ("{}", "who owns {}", "{} sister company" etc),
                'field': which field of bing rows was scanned ("name", "about", "snippet", "url")
                'ranks': in which bing rows the target was found ([0, 5, 7, 7, 10]),
            }
        """

        # make bing requests
        bing_rows = {}  # {'{}': rows, 'who owns {}': rows, ...}
        for query in self.search_queries:
            bing_response = self.bing.query(query.format(value))
            bing_rows[query] = bing_response.get('webPages', {}).get('value', []) if bing_response else []

        # scan bing responses and extract ranks for each target
        results = []
        for field in self.bing_fields:
            field_getter = getattr(self, f'get_{field}')
            for query, rows in bing_rows.items():

                targets_counts = {}

                for i, row in enumerate(rows):
                    text = field_getter(row)

                    for target, frequency in self.count_targets_in_text(text).items():
                        if target not in targets_counts:
                            targets_counts[target] = {
                                'target': target,
                                'query': query,
                                'field': field,
                                'ranks': [],
                            }

                        targets_counts[target]['ranks'] += [i] * frequency

                results += list(targets_counts.values())

        return results

    @staticmethod
    def get_about(bing_row: dict) -> str:
        """ Extracts "about->name" field from bing row """
        values = []
        for about in bing_row.get('about', []):
            values += about.values()

        return ' '.join(values)

    @classmethod
    def get_snippet(cls, bing_row: dict) -> str:
        """ Extracts snippet from bing row """
        return cls.preprocess_text(bing_row.get('snippet', ''))

    @classmethod
    def get_name(cls, bing_row: dict) -> str:
        """ Extracts name from bing row """
        return cls.preprocess_text(bing_row.get('name', ''))

    @classmethod
    def get_url(cls, bing_row: dict) -> str:
        """ Extracts url from bing row """
        return cls.preprocess_url(bing_row.get('displayUrl', ''))

    def to_predictors(self, entry) -> Dict[str, float]:
        return {
            'query_id': self.search_queries.index(entry['query']),
            'field_id': self.bing_fields.index(entry['field']),
            **Bane.to_predictors(entry['ranks']),
        }

    @staticmethod
    def prob_multiply(*probs: float) -> float:
        """
        Multiplies probabilities. Each additional argument increases the resulting probability, but the result
        still cannot exceed 1.

        Args:
            *probs: probabilities: [0.9, 0.8]

        Returns: final probability: 0.9 + (1-0.9)*0.8

        """
        res = probs[0]
        for prob in probs[1:]:
            res += (1.0 - res) * prob

        return res

    def get_winners(self, value: str) -> List[Tuple[str, float]]:
        """
        Runs bing search on `value`, extracts targets and their ranks and runs xgboost model on them.

        Args:
            value: any string value

        Returns:
            List of (target, model_prediction) tuples. Tuples with highest scores first.
        """
        targets = self.count_targets(value)
        if __name__ == '__main__':
            logger.debug(targets)

        targets_scores = defaultdict(list)
        for target in targets:
            targets_scores[target['target']].append(round(xgb_predict(self.to_predictors(target), *self.model), 3))

        results = [(target, self.prob_multiply(*scores)) for target, scores in targets_scores.items()]
        results = [res for res in results if res[1] > 0.01]
        results.sort(key=itemgetter(1), reverse=True)

        return results


if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('--value')
    parser.add_argument('--generate-training-file', action='store_true', default=False)
    args = parser.parse_args()

    if args.value:
        print(pretty(FindMaker().get_winners(args.value)))

    elif args.generate_training_file:
        def run_fm(value: str) -> List[Tuple[str, dict]]:
            fm = FindMaker()
            rows = fm.count_targets(value)
            for row in rows:
                row.update(fm.to_predictors(row))
            return [(row['target'], row) for row in rows]

        data = generate_training_data(recognizer=run_fm)

        writer = csv.DictWriter(
            open(path.join(DROPBOX, 'ERs', 'find_maker', f'find_maker.{get_timestamp()}.csv'), 'w', encoding='utf-8'),
            fieldnames=data[0].keys(),
            extrasaction='ignore',
        )
        writer.writeheader()
        writer.writerows(data)
