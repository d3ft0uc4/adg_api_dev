"""
© 2018 Alternative Data Group. All Rights Reserved.

This module defines a Score class which, given Bing's json output and targets container, may
calculate score based on target's occurrences in json response.

Usage (in ipython3 from repo root):
    from packages.targets import Targets
    from find_maker.find_maker import FindMaker
    from find_maker.score import Score

    targets_values = ['walmart']

    fm = FindMaker()
    jsn = fm.get_search_results({'0': targets_values[0]})['0']
    jsn['uidlist_input_string'] = '0'
    jsn['input_string'] = targets_values[0]

    targets = Targets(load_from={str(i): value for i, value in enumerate(targets_values)})

    Score(run_code='demo_API').generate_output(jsn, targets)
"""

import math

import nltk
import pandas as pd
import re

from collections import defaultdict, Counter
from datetime import datetime

from packages.settings import RUN_CODE
from mapper.settings import Configurable, FwFmConfig
from find_maker.preprocessing import preprocessed_text, preprocessed_url
from packages.targets import TARGETS
from packages.utils import uid

import logging

logger = logging.getLogger(f'adg.{__name__}')

FM_OUTPUT_COLS = ['uid', 'created_at', 'uidlist_input_string', 'input_string', 'search_addon_string',
                  'search_result_type', 'uid_target', 'target_name', 'score_type', 'search_result_ranks',
                  'score', 'run_code', 'target_source', 'target_name_clean']


@Configurable(default_config=FwFmConfig)
class Score:

    now = datetime.now()  # TODO: this is senseless

    def __init__(self, run_code=RUN_CODE, targets=TARGETS):
        assert run_code
        self.run_code = run_code
        self.targets = targets


    def generate_output(self, jsn):
        """
        TODO: Docstring this.

        :param jsn:
        :return:
        """

        dfs = []

        for query_type, query_string in self.config.SEARCH_STRING_MAP.items():
            bing_returned_results = jsn[query_type]['query_results']
            web_pages = bing_returned_results.get('webPages')

            if not web_pages:
                logger.debug(f'webPages not found for "{query_type}" query')
                continue

            for search_result_type in self.config.SEARCH_RESULT_TYPES:
                hits = defaultdict(list)
                for search_result_position, value in enumerate(web_pages['value'], start=1):
                    raw_text = value.get(search_result_type)
                    if not raw_text:
                        continue

                    text = self._clean_bing_response_text(raw_text, search_result_type)

                    # Add current search position the number of times a hit appeared in it.
                    for token, count in self._count_targets_by_substrings(text).items():
                        hits[token].extend([search_result_position] * count)

                df = self._counts_output_for_weight_func(
                    hits, query_string, search_result_type, jsn['uidlist_input_string'],
                    jsn['input_string'], 'weighted', self.weighted_func
                )

                dfs.append(df)

        fm_output_df = pd.concat(dfs, ignore_index=True) if dfs else pd.DataFrame()

        return fm_output_df

    def _clean_bing_response_text(self, raw_text, search_result_type):
        if search_result_type in ['snippet', 'name']:
            return preprocessed_text(raw_text)
        elif search_result_type == 'about':
            values = list(raw_text[0].values())
            return preprocessed_text(values[0]) if values else ''
        elif search_result_type == 'displayUrl':
            return preprocessed_url(raw_text)

        # DEBUG: Error
        raise Exception("FM: [CRIT] Need to implement _count_tokens for search_result_type ", search_result_type)

    def _count_targets_by_tokens(self, text):
        words = re.split('\s', text)
        prefixes = set([word[:self.targets.prefix_length] for word in words])

        candidates = []
        for prefix in prefixes:
            candidates.extend(self.targets.with_prefix[prefix])

        if not candidates:
            return {}

        # ---------
        result = {}
        for num_of_words in range(1, self.targets.max_words + 1):
            fdist_ngram = nltk.FreqDist(nltk.ngrams(words, num_of_words))

            for t, hit_freq_ngram in fdist_ngram.items():
                hit = ' '.join(t)
                if hit in candidates:
                    result[hit] = hit_freq_ngram

        return result

    def _count_targets_by_arrays(self, text):
        words = re.split('\s', text)
        prefixes = {word[:self.targets.prefix_length] for word in words}

        candidates = []
        for prefix in prefixes:
            candidates.extend(self.targets.with_prefix[prefix])

        if not candidates:
            return {}

        candidates_words = [re.split('\s', candidate) for candidate in candidates]
        # ---------
        matches = []

        for pos in range(len(words)):
            for candidate_words in candidates_words:
                if candidate_words == words[pos:pos+len(candidate_words)]:
                    matches.append(' '.join(candidate_words))

        return Counter(matches)

    def _count_targets_by_substrings(self, text):

        words = [word for word in re.split('\W', text) if word]
        prefixes = {word[:self.targets.prefix_length] for word in words}

        candidates = []
        for prefix in prefixes:
            candidates.extend(self.targets.with_prefix[prefix])

        if not candidates:
            return {}

        # ----------
        text_with_spaces = f' {" ".join(words)} '
        results = {}
        for candidate in candidates:
            count = text_with_spaces.count(f' {candidate} ')
            if count:
                results[candidate] = count

        return results

    # def _count_targets(self, text):
    #     tokens = re.split('\s', text)
    #
    #     result = {}
    #     for num_of_words in range(1, self.targets.max_words + 1):
    #         fdist_ngram = nltk.FreqDist(nltk.ngrams(tokens, num_of_words))
    #         targets = self.targets.trunc_group_by_length[num_of_words]
    #
    #         for t, hit_freq_ngram in fdist_ngram.items():
    #             hit = ' '.join(t)
    #             if hit in targets:
    #                 result[hit] = hit_freq_ngram
    #
    #     return result

    @staticmethod
    def weighted_func(x):
        return sum([(1 / math.log(2 + rank * 0.75)) for rank in x])

    def _counts_output_for_weight_func(self, hits, search_addon_string, search_result_type,
                                       uidlist_input_string, input_string, score_type, weight_func):
        hits_keys = hits.keys()
        result = [{
            'uid': uid(),
            'uid_target': target_uid,
            'target_name_clean': target_name_clean,
            'target_name': self.targets.orig_contents[target_uid],
            'search_result_ranks': hits[target_name_clean],
            'score': weight_func(hits[target_name_clean]),
            'score_type': score_type,
            'search_result_type': search_result_type,
            'search_addon_string': search_addon_string,
            'input_string': input_string,
            'uidlist_input_string': uidlist_input_string,
            'created_at': self.now,
            'run_code': self.run_code,
        } for target_uid, target_name_clean in self.targets.contents.items() if target_name_clean in hits_keys]

        return pd.DataFrame.from_records(result)

    def _jsonify(self, x):
        return '{' + '"r":' + str(x) + '}'


if __name__ == '__main__':
    s = Score('')
    hits = defaultdict(list)
    text = 'is gap abc inc gap inc abc'
    targets = ['gap inc']
    i = 0
    max_words = 2
    s._count_tokens(hits, text, i, targets, max_words)
