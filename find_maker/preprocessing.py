import re


def preprocessed_text(text):
    text = text.lower()

    # Remove certain characters.
    to_remove = ['(', ')', ',', '-', '"']
    for r in to_remove:
        text = text.replace(r, '')

    # Remove all dots except ones followed by 2 to 4 letters.
    text = re.sub('\.(?![a-zA-Z]{2,4})', '', text)

    text = text.replace('&', ' and ')

    # Remove extra spaces
    text = ' '.join(text.split())

    return text


def preprocessed_url(url):
    url = url.lower()
    return ' '.join(re.split('[^a-z]', url))


if __name__ == '__main__':
    assert preprocessed_text("()abc.com abc.de abc.info d.e.f") == "abc.com abc.de abc.info def"
    assert preprocessed_text("a(b( ) , . - \"   'c") == "ab 'c"
    assert preprocessed_text("a&b") == "a and b"
    assert preprocessed_text("a & b") == "a and b"
    assert preprocessed_text("ABC") == "abc"

    assert preprocessed_url("www.abc-def.com") == "www abc def com"
