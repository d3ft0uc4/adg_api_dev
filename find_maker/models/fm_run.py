from sqlalchemy import Column, DateTime, Float, JSON, String

from find_maker.models.base import Base


class FmRun(Base):
    """
    A table to store a record of running requests to Bing.
    Its 1 Bing request per row in the DB.
    """
    __tablename__ = 'fm_run'

    uid = Column(String, primary_key=True)
    created_at = Column(DateTime)
    uidlist_input_string = Column(String)
    input_string = Column(String)
    bing_search_string = Column(String)
    input_string_uidsource = Column(String)
    json_source = Column(String)
    log = Column(String)
    run_code = Column(String)
