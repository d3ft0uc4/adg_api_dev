"""
© 2018 Alternative Data Group. All Rights Reserved.

Usage (in ipython3 from repo root):
    from find_maker.find_maker import *
    from copy import copy

    # First, load targets (dict of {uid: input string})
    # NOTE test: 5 hardcoded targets in Targets class
    targets_container = Targets(load_from='test')  # {'0': 'microsoft', '1': 'BP P.L.C.', '2': 'AT&T', ...}

    # Get sample search items
    search_items = MerchantStrings(load_from='test')  # NOTE test: 2 hardcoded search items in MerchantStrings class
    search_items.preprocess_text()

    fm = FindMaker(targets_container, run_code='FM_TEST_RUN')

    # Now, query Bing for these `search_items` (and their different forms)
    search_results = fm.get_search_results(search_items)
    # NOTE: Python3 (homebrew) crashes on macOS #

    # Finally, run FindMaker
    fm.bulk_process(search_results, search_items)

Usage from console:
    python3 -m find_maker.find_maker \
            --targets "microsoft" "BP P.L.C." "AT&T" \
            --search-items "Microsoft Corp" \
            --run-code="RUN-CODE" \
"""

import os
from collections import defaultdict
from datetime import datetime
from glob import glob
from traceback import format_exc
from typing import List

import pandas as pd
import sqlalchemy

from mapper.settings import PARALLEL, db_engine, Configurable, FwFmConfig, TIMERS
from packages.settings import REL_DATA_DIR, FIND_MAKER_OUTPUT_DIR, RUN_CODE
from packages.targets import Targets, TARGETS
from packages.utils import LocalsThreadPoolExecutor

from bing.bing_web_core import BingWebCore

from find_maker.models.fm_run import FmRun
from find_maker.score import Score
from packages.utils import conditional_decorator, Timer, with_timer

import logging

logger = logging.getLogger(f'adg.{__name__}')


@Configurable(default_config=FwFmConfig)
class FindMaker:
    """
    Find Maker uses a large database of company names (“makers”) and Bing web searches to try matching targets for
    the raw input string.

    Attributes:
        results: A dict of {<input string>: <FindMaker output dataframe for this input>}
        run_rows: A list of `FmRun` objects (see `find_maker.models.FmRun`).
    """

    _output_dir = os.path.join(REL_DATA_DIR, FIND_MAKER_OUTPUT_DIR, 'fm_output')

    def __init__(self, run_code, targets_container=None):
        """
        Args:
            targets_container: `packages.targets.Targets` - a container for storing and preprocessing dict of
                               {uid: string}. May load from several sources. If undefined, all available targets will
                               be loaded (see `_load_targets`)
            run_code: Any code that will identify this run of FindMaker. Will be present in resulting dataframes.
        """
        self.targets_container = targets_container or TARGETS
        assert isinstance(self.targets_container, Targets)

        self.run_code = run_code

        self.creation_date = pd.to_datetime(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

        # attrs for temporary storing results
        self.results = {}

        # lists for storing resulting dataframes
        self.result_dataframes = []
        self.run_dataframes = []

    @conditional_decorator(with_timer, TIMERS)
    def bulk_process(self, search_results, search_items, output_choice='database'):
        """
        Calls `process` on every result in `search_results`, with time measuring, flushing and optional skipping of
        already processed results.
        Running `FindMaker(targets)` on MerchantStrings will turn merchant to target (or to nothing if no match found).
        Running `FindMaker(targets)` on targets themselves will result in removing duplicates from targets.

        Args:
            search_results: Dict with results from query to Bing (see `get_search_results`)
            search_items: Dict of {uid: input_string}
            output_choice: 'database'|'dataframe'|'disk': How to store results
        """

        with LocalsThreadPoolExecutor(
            max_workers=len(search_results) if PARALLEL else 1,
            thread_name_prefix='FmScoreExecutor',
        ) as pool:
            for uid_score in pool.map(
                lambda args: self.process(*args),
                [(uid, search_items[uid], jsn) for uid, jsn in search_results.items()]
            ):
                if not uid_score:
                    continue

                uid, scores = uid_score
                self.results[uid] = scores

        getattr(self, f'save_results_to_{output_choice}')()
        self.flush_results()

    @conditional_decorator(with_timer, TIMERS)
    def process(self, uid, input_string, jsn):
        """
        Calls `find_maker/score` module to process response from Bing and
        - updates `results[input_string]` with resulting dataframe;
        - appends FmRun instances to `run_rows` (see `find_maker.models.FmRun`).

        Args:
            uid: Uid of search item
            input_string: Query sent to Bing (e.g. 'microsoft' or 'AT&T')
            jsn: Response from Bing for selected input string and every QUERY_TYPE:
            {
                'original': Bing search results for query 'AT&T',
                'owner': Bing search results for query 'who owns AT&T',
                # ...
            }

        Returns:
            (uid, dataframe)

        Resulting dataframe contains following cols:
        - uid: UID for this row for reference
        - created_at: FindMaker creation date
        - uidlist_input_string: UID of search item
        - input_string: Original input string ("AT&T") passed to Bing search
        - search_addon_string: Modification string ("{}", "who owns {}" etc) for `input_string`
        - search_result_type: Name of section in bing result ("snippet", "name", "about")
        - uid_target: Target's UID
        - target_name: Unprocessed candidate's name ("AT&T") for looking in Bing search results
        - score_type: 'weighted' (# of occurrences with respect to position) or 'unweighted'
                      (only number of occurrences)
        - search_result_ranks: Array of the search results rank locations where target_name was found
                               ({"r":[1, 2, 3, 5, 7, ...]})
        - score: The count of times target was found in search results (float)
        - run_code: Code which uniquely identifies each run of FindMaker
        - target_source: Source table name for target strings, which may potentially come from different tables.
        - target_name_clean: Preprocessed candidate name ("at and t")
        """

        jsn['uidlist_input_string'] = uid
        jsn['input_string'] = input_string

        try:
            return uid, Score(self.config, run_code=self.run_code, targets=self.targets_container).generate_output(jsn)
        except Exception:
            logger.error(f'FM: Unable to process search item {uid} "{input_string}": {format_exc()}')

    def save_results_to_disk(self, output_dir=_output_dir):
        """
        Processing results. Saves `self.results` to specified dir.

        Args:
            output_dir: Path to dir which will store resulting <uid>.txt files.
        """

        dir = os.path.join(output_dir, 'results')
        os.makedirs(dir, exist_ok=True)
        for uid, df in self.results.items():
            df.to_csv(os.path.join(dir, uid + ".csv"), index=False)
            logger.debug(f'Saved result to {dir}/{uid}.csv')

    def save_results_to_dataframe(self):
        """
        Saves `self.results` to `self.result_dataframes`.
        """
        if not self.results.values():
            return

        comb_df = pd.concat([df for df in self.results.values()], ignore_index=True)
        self.result_dataframes.append(comb_df)

    @conditional_decorator(with_timer, TIMERS)
    def save_results_to_database(self, table_name='fm_output'):
        """
        Saves `self.results` to database table.

        Args:
            table_name: Name of a table for saving results.
        """
        if not self.results.values():
            return

        comb_df = pd.concat([df for df in self.results.values()], ignore_index=True).round(4)
        comb_df.to_sql(
            table_name, db_engine, if_exists='append', index=False, dtype={'search_result_ranks': sqlalchemy.types.JSON}
        )

    def flush_results(self):
        self.results = {}

    @staticmethod
    def _get_uid(file):
        return os.path.split(file)[-1].rsplit('.', maxsplit=1)[0]

    def _get_uids_on_disk(self, output_dir=_output_dir):
        """ Returns a list of UIDs which are already saved on disk """
        dir = os.path.join(output_dir, 'results')
        files = glob(os.path.join(dir, '*.csv'))
        return [self._get_uid(file) for file in files]

    def load_results_from_disk(self, output_dir=_output_dir):
        """
        Loads results from dir and sets `self.results`.

        Args:
            output_dir: Directory for FindMaker output.
            how: (Unused)
            batch: (Unused)
        """
        assert not self.results

        dir = os.path.join(output_dir, 'results')
        files = glob(os.path.join(dir, '*.csv'))
        for file in files:
            df = pd.read_csv(file, sep='|', header=0)  # , names=FM_OUTPUT_COLS)
            df['created_at'] = pd.to_datetime(df['created_at'])

            self.results[self._get_uid(file)] = df

    def _runs_to_dataframe(self):
        """ Processing runs """
        return pd.DataFrame(
            [{col.name: getattr(row, col.name) for col in row.__table__.columns}
                for row in self.run_rows]
        )

    def save_runs_to_database(self, table_name='fm_run'):
        """
        Saves `self.run_rows` to specified database table.
        """
        # FIXME: search_results_ranks
        self._runs_to_dataframe().to_sql(table_name, db_engine, if_exists='append', index=False,
                                         dtype={'search_result_ranks': sqlalchemy.types.JSON})

    def save_runs_to_dataframe(self):
        """
        Saves dataframe with contents from `self.run_rows` to `self.run_dataframes`.
        """
        self.run_dataframes.append(self._runs_to_dataframe())

    def save_runs_to_disk(self, output_dir=_output_dir):
        """
        Saves `self.run_rows` to specified dir.

        Args:
            output_dir: Path to dir which will store resulting <creation_date>.txt files.
        """

        dir = os.path.join(output_dir, 'runs')
        os.makedirs(dir, exist_ok=True)
        date = str(self.creation_date).replace(':', '_').replace('-', '_').replace(' ', '_')
        self._runs_to_dataframe().to_csv(os.path.join(dir, '{}.csv'.format(date)), sep="|", index=False)

    def flush_runs(self):
        self.run_rows = []

    def load_runs_from_disk(self, output_dir=_output_dir):
        """
        Loads FmRun rows from dir and sets `self.run_rows`.

        Args:
            output_dir: Directory for FindMaker output.
        """
        assert not self.run_rows

        dir = os.path.join(output_dir, 'runs')
        files = glob(os.path.join(dir, '*.csv'))

        df = pd.concat([pd.read_csv(file, sep="|", header=0) for file in files], ignore_index=True)
        df['created_at'] = pd.to_datetime(df['created_at'])

        for _, row in df.iterrows():
            self.run_rows.append(FmRun(**row.to_dict()))

    @conditional_decorator(with_timer, TIMERS)
    def get_search_results(self, search_items):
        """
        Given search items, gets `SearchResults` for each query_type-item combination.
        For query types see `SEARCH_STRING_MAP` keys. Each query type changes search item. E.g. if search item was "microsoft"
        and query_type was "owner", resulting query would be "who owns microsoft". See `SEARCH_STRING_MAP`
        for convertion reference.

        Args:
            search_items: Dict of {uid: search_string}.

        Returns:
            Dict with Bing's response (in json) for each uid and each query_type, e.g.
            {
                '0': {
                    'original': <json search results for "microsoft">,
                    'owner': <json search result for "who owns microsoft">,
                    'parent': <json search result for "microsoft parent company">,
                },
                '1': {
                    # ...
                # ...
        """
        # first, create list of queries to Bing
        queries = []
        for query_type, query_string in self.config.SEARCH_STRING_MAP.items():
            for uid, input_str in search_items.items():
                query_id = (uid, query_type)  # e.g. ('1', 'owner') or ('2', 'original')
                formatted_input_str = query_string.format(input_str)
                # ^ e.g. "who owns Microsoft" or "Microsoft parent company"
                queries.append((
                    query_id,
                    formatted_input_str,
                ))

        # now run all these queries in parallel
        search_results = [
            (queries[i][0], response)
            for i, response in enumerate(
                BingWebCore().bulk_query([q[1] for q in queries])
            )
        ]

        # now just reformat results
        results = defaultdict(dict)
        for (uid, query_type), jsn in search_results:
            results[uid][query_type] = jsn

        return results


if '__main__' == __name__:
    import argparse
    from find_maker.preprocessing import preprocessed_text

    parser = argparse.ArgumentParser()
    parser.add_argument('--targets', nargs='*')
    parser.add_argument('--run-code', default=RUN_CODE)
    parser.add_argument('search_items', nargs='+')
    args = parser.parse_args()

    if args.targets:
        targets = Targets(load_from={str(i): value for i, value in enumerate(args.targets)})
    else:
        targets = TARGETS
    logger.debug(f'{len(targets.contents)} targets')

    search_items = {str(i): preprocessed_text(value) for i, value in enumerate(args.search_items)}
    logger.debug('Search items: {}'.format(search_items))

    logger.debug('Run code: {}'.format(args.run_code))

    with Timer('Find maker'):
        fm = FindMaker(run_code=args.run_code, targets_container=targets)
        search_results = fm.get_search_results(search_items)
        fm.bulk_process(search_results, search_items, output_choice='disk')

    logger.debug("Results flushed to DB")
