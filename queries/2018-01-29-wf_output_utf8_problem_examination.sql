-- Example of the issue:
select input_string, child_name, replace(convert(child_name USING ascii), '?', ''), wf_output_clean_tbl.*
from wf_output_clean_tbl
where input_string in ('Ben & Jerry''S', 'Nijiya Market')

+---------------+-----------------------------------------+-------------------+---------------------+---------------+----------------------------------------+-----------------+-------------------+--------------+----------------+----------------------------+------------------+---------------------------------------------------------------+----------------+------------------+-----------------------+------------------------------+-------------------------+
| input_string  | child_name                              | uid_wf_run        | uid_agg_merch_master| input_string  | child_name                             | child_industry  | child_ticker_list | child_ticker | child_exchange | parent1_name               | parent1_industry | parent1_ticker_list                                           | parent1_ticker | parent1_exchange | company_url_count_pg1 | company_table_head_count_pg1 | article_title_count_pg1 |
+---------------+-----------------------------------------+-------------------+---------------------+---------------+----------------------------------------+-----------------+-------------------+--------------+----------------+----------------------------+------------------+---------------------------------------------------------------+----------------+------------------+-----------------------+------------------------------+-------------------------+
| Ben & Jerry'S | Ben & Jerryâ€™s Homemade Holdings, Inc. | 488dc4c2-131e-... | 56bf6cb1-3ed4-...   | Ben & Jerry'S | Ben & Jerryâ€™s Homemade Holdings, Inc.| Food processing | NULL              | NULL         | NULL           | Unilever N.V. Unilever plc | Consumer goods   | [(u'Euronext', u'UNA'), (u'LSE', u'ULVR'), (u'NYSE', u'UN,')] | UNA            | Euronext         | 8                     | 0                            | 56                      |
| Nijiya Market | Nijiya Market ãƒ‹ã‚¸ãƒ¤ãƒžãƒ¼ã‚±ãƒƒãƒˆ  | 71b85948-559f-... | 9ecd451b-11fb-...   | Nijiya Market | Nijiya Market ãƒ‹ã‚¸ãƒ¤ãƒžãƒ¼ã‚±ãƒƒãƒˆ | market          | NULL              | NULL         | NULL           | NULL                       | NULL             | NULL                                                          | NULL           | NULL             | 118                   | 0                            | 59                      |
| Ben & Jerry's | Ben & Jerryâ€™s Homemade Holdings, Inc. | 03e66d17-3fdf-... | 72910853-c664-...   | Ben & Jerry's | Ben & Jerryâ€™s Homemade Holdings, Inc.| Food processing | NULL              | NULL         | NULL           | Unilever N.V. Unilever plc | Consumer goods   | [(u'Euronext', u'UNA'), (u'LSE', u'ULVR'), (u'NYSE', u'UN,')] | UNA            | Euronext         | 8                     | 0                            | 56                      |
| Nijiya Market | Nijiya Market ãƒ‹ã‚¸ãƒ¤ãƒžãƒ¼ã‚±ãƒƒãƒˆ  | 5d328cfb-e97f-... | 4f9cdf75-ed78-...   | Nijiya Market | Nijiya Market ãƒ‹ã‚¸ãƒ¤ãƒžãƒ¼ã‚±ãƒƒãƒˆ | market          | NULL              | NULL         | NULL           | NULL                       | NULL             | NULL                                                          | NULL           | NULL             | 117                   | 0                            | 58                      |
+---------------+-----------------------------------------+-------------------+---------------------+---------------+----------------------------------------+-----------------+-------------------+--------------+----------------+----------------------------+------------------+---------------------------------------------------------------+----------------+------------------+-----------------------+------------------------------+-------------------------+

select input_string, child_name, replace(convert(child_name USING ascii), '?', ''), wf_output_clean_tbl.*
from wf_output_clean_tbl
where input_string in ('Ben & Jerry''S', 'Nijiya Market')

+---------------+-------------------------------------------------------------------+---------------------------------------------------+
| input_string  | child_name                                                        | replace(convert(child_name USING ascii), '?', '') |
+---------------+-------------------------------------------------------------------+---------------------------------------------------+
| Ben & Jerry'S | Ben & Jerryâ€™s Homemade Holdings, Inc.                           | Ben & Jerrys Homemade Holdings, Inc.              |
| Nijiya Market | Nijiya Market ãƒ‹ã‚¸ãƒ¤ãƒžãƒ¼ã‚±ãƒƒãƒˆ                            | Nijiya Market                                     |
| Ben & Jerry's | Ben & Jerryâ€™s Homemade Holdings, Inc.                           | Ben & Jerrys Homemade Holdings, Inc.              |
| Nijiya Market | Nijiya Market ãƒ‹ã‚¸ãƒ¤ãƒžãƒ¼ã‚±ãƒƒãƒˆ                            | Nijiya Market                                     |
+---------------+-------------------------------------------------------------------+---------------------------------------------------+

-- Find all cases of wrong data:

SELECT input_string,
       child_name
FROM wf_output_clean_tbl
WHERE CONVERT(child_name USING BINARY) RLIKE CONCAT('[', UNHEX('80'), '-', UNHEX('FF'), ']')

-- One case examination:
-- Estee Lauder - Estée Lauder Companies
-- The EstÃ©e Lauder Companies Inc.                                                                                                                                                             |
-- http://www.i18nqa.com/debug/utf8-debug.html
--
-- U+00E9	0xE9	é	Ã©	%C3 %A9
SELECT input_string,
       child_name,
       convert(convert(child_name using binary) using latin1) as bin_latin1,  -- incorrect
       convert(convert(child_name using binary) using utf8) bin_utf8,  -- incorrect
       convert(convert(convert(child_name using latin1) using binary) using utf8) latin1_bin_utf8  -- correct !
FROM wf_output_clean_tbl
where input_string = 'Estee Lauder'

-- Find records with new line
select child_name,
       substring(child_name, 1, locate("\n", child_name) - 1)
  from wf_output_clean_tbl
 where child_name RLIKE "\n";

-- Get the correct data for wrong records:
-- - fix the utf issue
-- - keep only the first line in child_name
select convert(convert(convert(
         (case when locate("\n", child_name) > 0
             then substring(child_name, 1, locate("\n", child_name) - 1)
             else child_name
          end)
      using latin1) using binary) using utf8) as converted_name
      -- child_name as original_name
  from wf_output_clean_tbl
 where convert(convert(child_name using binary) using latin1) != convert(convert(child_name using binary) using utf8)
