-- BEF_entities_targets_tbl
CREATE INDEX `idx_entity` on BEF_entities_targets_tbl (entity(1024));

-- FW_groups
CREATE INDEX `idx_match_target` on FW_groups (match_target(1024));
CREATE INDEX `idx_cluster_id` on FW_groups (cluster_id);

-- agg_merch_master
CREATE INDEX `idx_uid` on agg_merch_master (uid(1024));
CREATE INDEX `idx_merchant_string` on agg_merch_master (merchant_string(1024));
CREATE INDEX `idx_tojoin` on agg_merch_master (tojoin(1024));
CREATE INDEX `idx_data_source` on agg_merch_master (data_source(1024));

-- bing_entityfinder_output
CREATE INDEX `idx_merchant_string` on bing_entityfinder_output (merchant_string(1024));
CREATE INDEX `idx_uid_agg_merch_master` on bing_entityfinder_output (uid_agg_merch_master(1024));
CREATE INDEX `idx_entity_name` on bing_entityfinder_output (entity_name(1024));
CREATE INDEX `idx_uid` on bing_entityfinder_output (uid(1024));
CREATE INDEX `idx_bing_api_type` on bing_entityfinder_output (bing_api_type(1024));
CREATE INDEX `idx_run_code` on bing_entityfinder_output (run_code(1024));

-- match_targets_unofficial
CREATE INDEX `idx_raw` on match_targets_unofficial (`Raw`(1024));
CREATE INDEX `idx_raw_source` on match_targets_unofficial (`Raw Source`(1024));
CREATE INDEX `idx_correct` on match_targets_unofficial (`Correct`(1024));

-- wf_output
CREATE INDEX `idx_uid_wf_run` on wf_output (uid_wf_run(1024));
CREATE INDEX `idx_uid_agg_merch_master` on wf_output (uid_agg_merch_master(1024));
CREATE INDEX `idx_input_string` on wf_output (input_string(1024));
CREATE INDEX `idx_child_name` on wf_output (child_name(1024));
CREATE INDEX `idx_parent1_name` on wf_output (parent1_name(1024));
CREATE INDEX `idx_parent2_name` on wf_output (parent2_name(1024));
CREATE INDEX `idx_target_bool` on wf_output (target_bool);

-- wf_output_clean_tbl
CREATE INDEX `idx_uid_wf_run` on wf_output_clean_tbl (uid_wf_run(1024));
CREATE INDEX `idx_uid_agg_merch_master` on wf_output_clean_tbl (uid_agg_merch_master(1024));
CREATE INDEX `idx_input_string` on wf_output_clean_tbl (input_string(1024));
CREATE INDEX `idx_child_name` on wf_output_clean_tbl (child_name(1024));
CREATE INDEX `idx_parent1_name` on wf_output_clean_tbl (parent1_name(1024));
CREATE INDEX `idx_company_url_count_pg1` on wf_output_clean_tbl (company_url_count_pg1(1024));
CREATE INDEX `idx_company_table_head_count_pg1` on wf_output_clean_tbl (company_table_head_count_pg1(1024));
CREATE INDEX `idx_article_title_count_pg1` on wf_output_clean_tbl (article_title_count_pg1(1024));

-- wf_wiki_pages
CREATE INDEX `idx_uid` on wf_wiki_pages (uid(1024));
CREATE INDEX `idx_uid_wf_run` on wf_wiki_pages (uid_wf_run(1024));
CREATE INDEX `idx_scrape_order` on wf_wiki_pages (scrape_order(1024));
CREATE INDEX `idx_key` on wf_wiki_pages (`key`(1024));

-- wf_run
CREATE INDEX `idx_uid` on wf_run (uid(1024));

-- fm_output
CREATE INDEX `idx_uid` on fm_output (UID(1024));
CREATE INDEX `idx_fm_datestamp` on fm_output (fm_datestamp);
CREATE INDEX `idx_uid_input_string` on fm_output (UID_input_string(1024));
CREATE INDEX `idx_input_string` on fm_output (input_string(1024));
CREATE INDEX `idx_uid_target` on fm_output (UID_target(1024));
CREATE INDEX `idx_target_name` on fm_output (target_name(1024));
CREATE INDEX `idx_score_type` on fm_output (score_type(1024));
CREATE INDEX `idx_score` on fm_output (score);
CREATE INDEX `idx_run_code` on fm_output (run_code(1024));

-- ft_output
CREATE INDEX `idx_uid` on ft_output (ft_output_uid(1024));
CREATE INDEX `idx_uid_input_string` on ft_output (uid_input_string(1024));
CREATE INDEX `idx_data_source` on ft_output (data_source(1024));
CREATE INDEX `idx_input_string` on ft_output (input_string(1024));
CREATE INDEX `idx_company_name` on ft_output (company_name(1024));
CREATE INDEX `idx_company_ticker` on ft_output (company_ticker(1024));
CREATE INDEX `idx_type` on ft_output (type(1024));
CREATE INDEX `idx_exchange` on ft_output (exchange(1024));
CREATE INDEX `idx_log` on ft_output (log(1024));

CREATE INDEX `idx_uid_target` on fm_output (UID_target(1024));
CREATE INDEX `idx_target_name` on fm_output (target_name(1024));
CREATE INDEX `idx_score_type` on fm_output (score_type(1024));
CREATE INDEX `idx_score` on fm_output (score);
CREATE INDEX `idx_run_code` on fm_output (run_code(1024));

-- clean_cc
CREATE INDEX `idx_td2` on clean_cc (td2(1024));
CREATE INDEX `idx_dc1` on clean_cc (dc1(1024));

-- entity_match_target_map
CREATE INDEX `idx_uid_match_target` on entity_match_target_map (uid_match_target);
CREATE INDEX `idx_entity_name` on entity_match_target_map (entity_name);
CREATE INDEX `idx_entity_source` on entity_match_target_map (entity_source);
CREATE INDEX `idx_match_target` on entity_match_target_map (match_target);
