/*
 * The levenshtein distance function.
 */

-- http://www.artfulsoftware.com/infotree/qrytip.php?id=552
-- Also:
-- https://stackoverflow.com/questions/5322917/how-to-compute-similarity-between-two-strings-in-mysql
-- https://stackoverflow.com/questions/13909885/how-to-add-levenshtein-function-in-mysql
-- https://stackoverflow.com/questions/634995/implementation-of-levenshtein-distance-for-mysql-fuzzy-search
-- https://gist.github.com/Kovah/df90d336478a47d869b9683766cff718
-- https://snippets.aktagon.com/snippets/610-levenshtein-distance-for-mysql
--

/*
DELIMITER $$
DROP FUNCTION IF EXISTS levenshtein $$
DROP FUNCTION IF EXISTS levenshtein_ratio $$
CREATE FUNCTION levenshtein( s1 VARCHAR(255), s2 VARCHAR(255) )
  RETURNS INT
  DETERMINISTIC
  BEGIN
    DECLARE s1_len, s2_len, i, j, c, c_temp, cost INT;
    DECLARE s1_char CHAR;
    -- max strlen=255
    DECLARE cv0, cv1 VARBINARY(256);
    SET s1_len = CHAR_LENGTH(s1), s2_len = CHAR_LENGTH(s2), cv1 = 0x00, j = 1, i = 1, c = 0;
    IF s1 = s2 THEN
      RETURN 0;
    ELSEIF s1_len = 0 THEN
      RETURN s2_len;
    ELSEIF s2_len = 0 THEN
      RETURN s1_len;
    ELSE
      WHILE j <= s2_len DO
        SET cv1 = CONCAT(cv1, UNHEX(HEX(j))), j = j + 1;
      END WHILE;
      WHILE i <= s1_len DO
        SET s1_char = SUBSTRING(s1, i, 1), c = i, cv0 = UNHEX(HEX(i)), j = 1;
        WHILE j <= s2_len DO
          SET c = c + 1;
          IF s1_char = SUBSTRING(s2, j, 1) THEN
            SET cost = 0; ELSE SET cost = 1;
          END IF;
          SET c_temp = CONV(HEX(SUBSTRING(cv1, j, 1)), 16, 10) + cost;
          IF c > c_temp THEN SET c = c_temp; END IF;
            SET c_temp = CONV(HEX(SUBSTRING(cv1, j+1, 1)), 16, 10) + 1;
            IF c > c_temp THEN
              SET c = c_temp;
            END IF;
            SET cv0 = CONCAT(cv0, UNHEX(HEX(c))), j = j + 1;
        END WHILE;
        SET cv1 = cv0, i = i + 1;
      END WHILE;
    END IF;
    RETURN c;
  END $$
CREATE FUNCTION levenshtein_ratio( s1 VARCHAR(255), s2 VARCHAR(255) )
  RETURNS INT
  DETERMINISTIC
  BEGIN
    DECLARE s1_len, s2_len, max_len INT;
    SET s1_len = LENGTH(s1), s2_len = LENGTH(s2);
    IF s1_len > s2_len THEN
      SET max_len = s1_len;
    ELSE
      SET max_len = s2_len;
    END IF;
    RETURN ROUND((1 - LEVENSHTEIN(s1, s2) / max_len) * 100);
END $$
DELIMITER ;

*/

/*
 * The composite model view combines data from wikipedia and bing.
 * The input is agg_merch_master_top_merchants_unique.merchant_string,
 * for each merchant_string we find the corresponding row in
 * - wf_output_unique_merch
 * - bing_entityfinder_output (for web-entity API results)
 * - bing_entityfinder_output (for entity-entity API results)
 *
 */

DROP VIEW IF EXISTS composite_model;

create view composite_model as

select merchant.merchant_string,
       merchant.uidlist_agg_merch_master_top_merchants,
       wf.child_name as wf_entity_name,
       wf.uid_wf_run as uid_wf_run,
       -- for now use entity_match_target_map.match_target as
       -- unofficial_name, later to be replaced
       wf_map.match_target as wf_unofficial_name,
       bing_web.entity_name as befweb_entity_name,
       bing_web.uid as befweb_uid,
       bing_web_map.match_target as befweb_unofficial_name,
       bing_entity.entity_name as befentity_entity_name,
       bing_entity.uid as befentity_uid,
       bing_entity_map.match_target as befentity_unofficial_name,
       strcmp(wf_map.match_target, bing_web_map.match_target) as str_dist_wf_befweb,
       strcmp(wf_map.match_target, bing_entity_map.match_target) as str_dist_wf_befentity,
       strcmp(bing_web_map.match_target, bing_entity_map.match_target) as str_dist_befweb_befentity,
       wf_run.wiki_page_rank as wf_wiki_page_rank,
       wf.company_url_count_pg1 as wf_company_url_count,
       wf.article_title_count_pg1 as wf_article_title_count,
       wf.company_table_head_count_pg1 as wf_company_table_head_count,
       bing_entity.entity_scenario as befentity_scenario,
       bing_entity.entity_type_hints as befentity_type_hints,
       bing_entity.entity_type_display_hint as befentity_type_display_hint
  from agg_merch_master_top_merchants_unique merchant
  left
  join wf_output_unique_merch wf on wf.input_string = merchant.merchant_string
  left
  join wf_run on wf_run.uid_wf_output = wf.uid_wf_run
  left
  join entity_match_target_map wf_map on wf_map.entity_name = wf.child_name
  left
  join bing_entityfinder_output bing_web on bing_web.merchant_string = merchant.merchant_string
  left
  join entity_match_target_map bing_web_map on bing_web_map.entity_name = bing_web.entity_name
  left
  join bing_entityfinder_output bing_entity on bing_entity.merchant_string = merchant.merchant_string
  left
  join entity_match_target_map bing_entity_map on bing_entity_map.entity_name = bing_entity.entity_name
where bing_web.bing_api_type = 'web-entity'
  and bing_web.run_code = 'full_run_2' and bing_web.uid is not null
  and bing_entity.bing_api_type = 'entity-entity'
  and bing_entity.run_code = 'full_run_2' and bing_entity.uid is not null
;
