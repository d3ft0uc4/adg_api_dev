-- The view to make BF source unique.
-- We need this because BF web and BF entity are often equal and
-- we want to count this case (web = entity) as 1 match, not as two
-- matches

-- The view to get the list of bing entities where both
-- web-entity and entity-entity have same result (same entity_name).
DROP VIEW IF EXISTS bing_entityfinder_output_two_matches;
CREATE VIEW bing_entityfinder_output_two_matches AS (
SELECT max(uid) as uid,
       merchant_string,
       entity_name,
       group_concat(bing_api_type order by uid) as bing_api_types,
       group_concat(uid order by uid) as uids
  FROM bing_entityfinder_output
WHERE run_code = 'full_run_2' AND uid IS NOT NULL
  AND bing_api_type is not NULL
  AND entity_name is not NULL
GROUP BY merchant_string, entity_name
HAVING count(bing_api_type) = 2
);

-- The view to get the list of bing entity where we have
-- only one match from web-entity or entity-entity or
-- we have two different matches.
DROP VIEW IF EXISTS bing_entityfinder_output_unique;
CREATE VIEW bing_entityfinder_output_unique AS (
SELECT uid,
       merchant_string,
       entity_name,
       bing_api_type
  FROM bing_entityfinder_output
WHERE run_code = 'full_run_2' AND uid IS NOT NULL
  AND bing_api_type is not NULL
  AND entity_name is not NULL
  AND merchant_string NOT IN (
      SELECT DISTINCT merchant_string FROM bing_entityfinder_output_two_matches
  )
);

-- Query to select merchant_string and related matches from bing web-entity and entity-entity side-by-side.
-- We select all distinct merchant strings from bf_base and then join queries to select appropriate
-- web-entity and entity-entity results.
SELECT DISTINCT bf_base.merchant_string,
     bf_web.bing_api_type,
     bf_web.entity_name,
     bf_entity.bing_api_type,
     bf_entity.entity_name,
     bf_web.entity_name IS  NULL AND bf_entity.entity_name IS NULL as no_match,
     (bf_web.entity_name IS NOT NULL AND bf_entity.entity_name IS NULL) OR
     (bf_web.entity_name IS NULL AND bf_entity.entity_name IS NOT NULL) as one_match,
     bf_web.entity_name IS NOT NULL AND bf_entity.entity_name IS NOT NULL AND
     bf_web.entity_name = bf_entity.entity_name as two_match,
     bf_web.entity_name IS NOT NULL AND bf_entity.entity_name IS NOT NULL AND
     bf_web.entity_name != bf_entity.entity_name as two_no_match
FROM bing_entityfinder_output bf_base
LEFT JOIN (
    SELECT merchant_string, bing_api_type, entity_name
      FROM bing_entityfinder_output bf_web
    WHERE bf_web.bing_api_type = 'web-entity' AND bf_web.run_code = 'full_run_2' AND bf_web.uid IS NOT NULL
) as bf_web on bf_web.merchant_string = bf_base.merchant_string
LEFT JOIN (
    SELECT merchant_string, bing_api_type, entity_name
      FROM bing_entityfinder_output bf_entity
    WHERE bf_entity.bing_api_type = 'entity-entity' AND bf_entity.run_code = 'full_run_2' AND bf_entity.uid IS NOT NULL
) as bf_entity on bf_entity.merchant_string = bf_base.merchant_string
ORDER BY bf_base.merchant_string


-- The view with 5 sources for each merchant_string:
-- WF, BEF-web, BEF-entity, FT and FW
DROP VIEW IF EXISTS combined_entity_data;
create view combined_entity_data as
select *
 FROM (
   (
   -- WF data
   SELECT wf.child_name as entity_name,
          SUBSTR(wf.child_name, 1, 10) as entity_name_short,
          wf_map.match_target as match_target,
          'wf' as source,
          wf.uid_wf_run as uid_entity_source,
          wf.input_string as merchant_string,
          groups.memberships as group_number
     FROM wf_output_unique_merch wf
     JOIN entity_match_target_map wf_map ON wf_map.entity_name = wf.child_name
     LEFT
     JOIN fw_response_pivot_t2t_groups groups ON groups.targets = wf_map.match_target
   )
  UNION
   (
   -- BF data where both web and entity API results match,
   -- we count them only once
   SELECT bf.entity_name as entity_name,
          SUBSTR(bf.entity_name, 1, 10) as entity_name_short,
          bf_map.match_target as match_target,
          'bf-web-and-entity' as source,
          bf.uid as uid_entity_source,
          bf.merchant_string as merchant_string,
          groups.memberships as group_number
     FROM bing_entityfinder_output_two_matches bf
     JOIN entity_match_target_map bf_map ON bf_map.entity_name = bf.entity_name
     LEFT
     JOIN fw_response_pivot_t2t_groups groups ON groups.targets = bf_map.match_target
   )
  UNION
   (
   -- BF data from web-entity API (where there is no entity-entity data or
   -- entity-entity produces different result)
   SELECT bf.entity_name as entity_name,
          SUBSTR(bf.entity_name, 1, 10) as entity_name_short,
          bf_map.match_target as match_target,
          'bf-web' as source,
          bf.uid as uid_entity_source,
          bf.merchant_string as merchant_string,
          groups.memberships as group_number
     FROM bing_entityfinder_output_unique bf
     JOIN entity_match_target_map bf_map ON bf_map.entity_name = bf.entity_name
     LEFT
     JOIN fw_response_pivot_t2t_groups groups ON groups.targets = bf_map.match_target
    where bf.bing_api_type = 'web-entity'
   )
  UNION
   (
   -- BF data from entity-entity API (where there is no entity-entity data or
   -- entity-entity produces different result)
   SELECT bf.entity_name as entity_name,
          SUBSTR(bf.entity_name, 1, 10) as entity_name_short,
          bf_map.match_target as match_target,
          'bf-entity' as source,
          bf.uid as uid_entity_source,
          bf.merchant_string as merchant_string,
          groups.memberships as group_number
     FROM bing_entityfinder_output_unique bf
     JOIN entity_match_target_map bf_map ON bf_map.entity_name = bf.entity_name
     LEFT
     JOIN fw_response_pivot_t2t_groups groups ON groups.targets = bf_map.match_target
    where bf.bing_api_type = 'entity-entity'
   )
  UNION
   (
   -- FT data
   SELECT ft.company_name as entity_name,
          SUBSTR(ft.company_name, 1, 10) as entity_name_short,
          ft_map.match_target,
          'ft' as source,
          ft.uid as uid_entity_source,
          ft.input_string as merchant_string,
          groups.memberships as group_number
     FROM ft_output ft
     JOIN entity_match_target_map ft_map ON ft_map.entity_name = ft.company_name
     LEFT
     JOIN fw_response_pivot_t2t_groups groups ON groups.targets = ft_map.match_target
   )
  UNION
   (
   SELECT fw.entity_name,
          SUBSTR(fw.entity_name, 1, 10) as entity_name_short,
          fw.match_target,
          'fw' as source,
          NULL as uid_entity_source,
          fw.merchant_string as merchant_string,
          groups.memberships as group_number
     FROM fw_m2t fw
     LEFT
     JOIN fw_response_pivot_t2t_groups groups ON groups.targets = fw.match_target
  )
 ) as entity_data

select * from combined_entity_data limit 100;

-- Get merchant_string plus match_target
  SELECT merchant_string, match_target,
         max(entity_name),
         count(*) as cnt,
         group_concat(source order by uid_entity_source) as sources,
         group_concat(uid_entity_source order by uid_entity_source) as uids_entity_source
    FROM combined_entity_data
   GROUP BY merchant_string, match_target
   HAVING cnt >= 2
  LIMIT 5

/*
+-----------------+------------------------------------+-----------------------------------------+----------+
| merchant_string | match_target                       | max(entity_name)                        | count(*) |
+-----------------+------------------------------------+-----------------------------------------+----------+
| 1&1 Internet    | 1&1 internet                       | 1&1 Internet                            |        4 |
| 1-800-Flowers   | 1-800-flowers                      | 1-800-Flowers                           |        3 |
| 1-800-Got-Junk  | 1-800-got-junk                     | 1-800-GOT-JUNK?                         |        3 |
| 1-800-Got-Junk  | rbds rubbish boys disposal service | RBDS Rubbish Boys Disposal Service Inc. |        2 |
| 16 Handles      | 16 handles                         | 16 Handles                              |        4 |
+-----------------+------------------------------------+-----------------------------------------+----------+
*/

-- Combine by match target, select the record with max count
SELECT * FROM (
  SELECT merchant_string, match_target,
         max(entity_name),
         count(*) as cnt,
         group_concat(source order by uid_entity_source) as sources,
         group_concat(uid_entity_source order by uid_entity_source) as uids_entity_source
    FROM combined_entity_data
   GROUP BY merchant_string, match_target
   HAVING count(*) >= 2
  ) as grouped_entity_data
GROUP BY merchant_string
HAVING cnt = max(cnt)
LIMIT 5

/*
+-----------------+----------------+------------------+-----+
| merchant_string | match_target   | max(entity_name) | cnt |
+-----------------+----------------+------------------+-----+
| 1&1 Internet    | 1&1 internet   | 1&1 Internet     |   4 |
| 1-800-Flowers   | 1-800-flowers  | 1-800-Flowers    |   3 |
| 1-800-Got-Junk  | 1-800-got-junk | 1-800-GOT-JUNK?  |   3 |
| 16 Handles      | 16 handles     | 16 Handles       |   4 |
| 1sale           | google         | Google LLC       |   2 |
+-----------------+----------------+------------------+-----+
*/

DROP VIEW IF EXISTS combined_entity_data_grouped_by_match_target;
create view combined_entity_data_grouped_by_match_target as
SELECT * FROM (
  SELECT merchant_string, match_target,
         max(entity_name) as entity_name, 
         max(group_number) as group_number,
         count(*) as cnt,
         group_concat(source order by uid_entity_source) as sources,
         group_concat(uid_entity_source order by uid_entity_source) as uids_entity_source
    FROM combined_entity_data
   GROUP BY merchant_string, match_target
   HAVING count(*) >= 2
  ) as grouped_entity_data
GROUP BY merchant_string
HAVING cnt = max(cnt)
;


DROP VIEW IF EXISTS combined_entity_data_grouped_by_match_target;
create view combined_entity_data_grouped_by_match_target as
select a.*, b.match_target_official official_name from (
    SELECT * FROM (
          SELECT merchant_string, match_target,
                 max(entity_name) as entity_name, 
                 max(group_number) as group_number,
                 count(*) as cnt,
                 group_concat(source order by uid_entity_source) as sources,
                 group_concat(uid_entity_source order by uid_entity_source) as uids_entity_source
            FROM combined_entity_data
           GROUP BY merchant_string, match_target
           HAVING count(*) >= 2
          ) as grouped_entity_data
        GROUP BY merchant_string
        HAVING cnt = max(cnt)
        ) a
   join targets_collapsed_offNames b
   on a.match_target=b.match_target
   where b.match_target_good = 1; 
;



-- Get merchant_string + entity name, where we have >= 2 same entity names
  SELECT merchant_string, entity_name_short, count(*) as cnt,
         group_concat(source order by uid_entity_source) as sources,
         group_concat(uid_entity_source order by uid_entity_source) as uids_entity_source
    FROM combined_entity_data
   GROUP BY merchant_string, entity_name_short
   HAVING count(*) >= 2

/*
| merchant_string                                           | entity_name_short | cnt |
+-----------------------------------------------------------+-------------------+-----+
| 1&1 Internet                                              | 1&1 Intern        |   4 |
| 1-800-Flowers                                             | 1-800-Flow        |   3 |
| 1-800-Got-Junk                                            | 1-800-GOT-        |   3 |
| 1-800-Got-Junk                                            | RBDS Rubbi        |   2 |
*/

-- Combine by entity_name, select the record with max count
SELECT * FROM (
  SELECT merchant_string,
         max(entity_name) as entity_name,
         max(match_target) as match_target,
         count(*) as cnt,
         group_concat(source order by uid_entity_source) as sources,
         group_concat(uid_entity_source order by uid_entity_source) as uids_entity_source
    FROM combined_entity_data
   GROUP BY merchant_string, entity_name_short
   HAVING count(*) >= 2
  ) as grouped_entity_data
GROUP BY merchant_string
HAVING cnt = max(cnt)
LIMIT 5

/*
+-----------------+----------------+------------------+-----+
| merchant_string | match_target   | max(entity_name) | cnt |
+-----------------+----------------+------------------+-----+
| 1&1 Internet    | 1&1 internet   | 1&1 Internet     |   4 |
| 1-800-Flowers   | 1-800-flowers  | 1-800-Flowers    |   3 |
| 1-800-Got-Junk  | 1-800-got-junk | 1-800-GOT-JUNK?  |   3 |
| 16 Handles      | 16 handles     | 16 Handles       |   4 |
| 1sale           | google         | Google LLC       |   2 |
+-----------------+----------------+------------------+-----+
*/

-- Combine by entity_name, select the record with max count
DROP VIEW IF EXISTS combined_entity_data_grouped_by_entity_name;
create view combined_entity_data_grouped_by_entity_name as
SELECT * FROM (
  SELECT merchant_string,
         max(entity_name) as entity_name,
         max(match_target) as match_target,
         max(group_number) as group_number,
         count(*) as cnt,
         group_concat(source order by uid_entity_source) as sources,
         group_concat(uid_entity_source order by uid_entity_source) as uids_entity_source
    FROM combined_entity_data
   GROUP BY merchant_string, entity_name_short
   HAVING count(*) >= 2
  ) as grouped_entity_data
GROUP BY merchant_string
HAVING cnt = max(cnt)
;

-- Select entity in two ways - by match target first and the rest by entity name.
DROP VIEW IF EXISTS combined_entity_to_merchant_string_two_rules;
create view combined_entity_to_merchant_string_two_rules as
select *, 'by_match_target' as select_type
  from combined_entity_data_grouped_by_match_target
union
select *, 'by_entity_name' as select_type
  from combined_entity_data_grouped_by_entity_name
    WHERE merchant_string not IN (
        select distinct merchant_string from combined_entity_data_grouped_by_match_target
    )


-- Combine by group, select the record with max count
DROP VIEW IF EXISTS combined_entity_data_grouped_by_group_number;
CREATE VIEW combined_entity_data_grouped_by_group_number as
SELECT * FROM (
  SELECT merchant_string,
         max(match_target),
         max(entity_name),
         group_number,
         count(*) as cnt,
         group_concat(source order by uid_entity_source) as sources,
         group_concat(uid_entity_source order by uid_entity_source) as uids_entity_source
    FROM combined_entity_data
   WHERE group_number is not NULL
   GROUP BY merchant_string, group_number
   HAVING count(*) >= 2
  ) as grouped_entity_data
GROUP BY merchant_string
HAVING cnt = max(cnt)
;

-- Add the third rule - if there is no match, also compare the
-- group from fw_response_pivot_t2t_groups
DROP VIEW IF EXISTS combined_entity_to_merchant_string;
create view combined_entity_to_merchant_string as
select *
  from combined_entity_to_merchant_string_two_rules
union
select *, 'by_group' as select_type
  from combined_entity_data_grouped_by_group_number
    WHERE merchant_string not IN (
        select distinct merchant_string from combined_entity_to_merchant_string_two_rules
    )

select entity.*, merchants.*
  from combined_entity_to_merchant_string entity
  join agg_merch_master_top_merchants_unique merchants
    on merchants.merchant_string = entity.merchant_string;

select * from agg_merch_master_top_merchants_unique merchants
  left join combined_entity_to_merchant_string entity on entity.merchant_string = merchants.merchant_string






-- brand_master_nodes Gene's version. Uses the "collpased" targets table targets_collapsed_offNames  to get official names and main match_targets

CREATE VIEW `altdg_merchtag`.`brand_master_nodes` as 
 
select a.*, b.match_target from
  (SELECT uuid() AS `uid`, `combined_names`.`official_name` AS `official_name`,
          group_concat(`combined_names`.`match_target`
                       ORDER BY `combined_names`.`match_target` ASC separator ',') AS `match_targets`,
          group_concat(DISTINCT `combined_names`.`identifiers`
                       ORDER BY `combined_names`.`identifiers` ASC separator ',') AS `identifiers`,
          'BRAND' AS `node_type`,
          curdate() AS `created_date`,
          '2999-12-31' AS `archive_date`,
          group_concat(`combined_names`.`entity_names`
                       ORDER BY `combined_names`.`entity_names` ASC separator ',') AS `entity_names`,
          group_concat(`combined_names`.`merchant_strings`
                       ORDER BY `combined_names`.`merchant_strings` ASC separator ',') AS `merchant_strings`,
          group_concat(DISTINCT `combined_names`.`industry`
                       ORDER BY `combined_names`.`industry` ASC separator ',') AS `industry`
   FROM
     (select combined_names1.*, b.match_target_official official_name from
     (SELECT `combined_entity_data`.`match_target` AS `match_target`,
             group_concat(DISTINCT `combined_entity_data`.`entity_name`
                          ORDER BY `combined_entity_data`.`entity_name` ASC separator ',') AS `entity_names`,
             group_concat(DISTINCT `combined_entity_data`.`merchant_string`
                          ORDER BY `combined_entity_data`.`merchant_string` ASC separator ',') AS `merchant_strings`,
             group_concat(DISTINCT `combined_entity_data`.`identifier`
                          ORDER BY `combined_entity_data`.`identifier` ASC separator ',') AS `identifiers`,
             group_concat(DISTINCT `combined_entity_data`.`industry`
                          ORDER BY `combined_entity_data`.`industry` ASC separator ',') AS `industry`
      FROM
        (SELECT `map`.`entity_name` AS `entity_name`,
                `map`.`entity_name_short` AS `entity_name_short`,
                `map`.`match_target` AS `match_target`,
                `map`.`source` AS `source`,
                `map`.`uid_entity_source` AS `uid_entity_source`,
                `map`.`merchant_string` AS `merchant_string`,
                `map`.`group_number` AS `group_number`,
                `tickers`.`ticker` AS `ticker`,
                `tickers`.`exchange` AS `exchange`,
                `tickers`.`industry` AS `industry`,
                concat(`tickers`.`ticker`,':',`tickers`.`exchange`) AS `identifier`
         FROM (`altdg_merchtag`.`combined_entity_data` `map`
               LEFT JOIN (
                            (SELECT `altdg_merchtag`.`wf_output_unique_merch_tbl`.`child_name` AS `entity_name`,
                                    `altdg_merchtag`.`wf_output_unique_merch_tbl`.`child_ticker` AS `ticker`,
                                    `altdg_merchtag`.`wf_output_unique_merch_tbl`.`child_exchange` AS `exchange`,
                                    `altdg_merchtag`.`wf_output_unique_merch_tbl`.`child_industry` AS `industry`
                             FROM `altdg_merchtag`.`wf_output_unique_merch_tbl`
                             WHERE (`altdg_merchtag`.`wf_output_unique_merch_tbl`.`child_ticker` IS NOT NULL))
                          UNION
                            (SELECT `altdg_merchtag`.`wf_output_unique_merch_tbl`.`parent1_name` AS `entity_name`,
                                    `altdg_merchtag`.`wf_output_unique_merch_tbl`.`parent1_ticker` AS `ticker`,
                                    `altdg_merchtag`.`wf_output_unique_merch_tbl`.`parent1_exchange` AS `exchange`,
                                    `altdg_merchtag`.`wf_output_unique_merch_tbl`.`parent1_industry` AS `industry`
                             FROM `altdg_merchtag`.`wf_output_unique_merch_tbl`
                             WHERE (`altdg_merchtag`.`wf_output_unique_merch_tbl`.`parent1_ticker` IS NOT NULL))
                          UNION
                            (SELECT `altdg_merchtag`.`ft_output`.`company_name` AS `entity_name`,
                                    `altdg_merchtag`.`ft_output`.`company_ticker` AS `ticker`,
                                    `altdg_merchtag`.`ft_output`.`exchange` AS `exchange`,
                                    NULL AS `NULL`
                             FROM `altdg_merchtag`.`ft_output`
                             WHERE ((`altdg_merchtag`.`ft_output`.`run_code` = 'main-run-1')
                                    AND (`altdg_merchtag`.`ft_output`.`company_ticker` IS NOT NULL)))) `tickers` on((`map`.`entity_name` = `tickers`.`entity_name`)))) `combined_entity_data`
      GROUP BY `combined_entity_data`.`match_target`) `combined_names1`
        join targets_collapsed_offNames b
   on combined_names1.match_target=b.match_target
   where b.match_target_good = 1 ) combined_names
   GROUP BY `combined_names`.`official_name`) a

   join targets_collapsed_offNames b
   on a.official_name=b.match_target_official
   where b.match_target_group_winner=1