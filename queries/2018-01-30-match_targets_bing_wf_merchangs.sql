-- See https://docs.google.com/document/d/1Hie4x4RO3A6UhlGBhSoNB52Q-CMzQ8K9ILPt17FJbzU/edit#heading=h.ozmahykg37xf

-- Example of the data we want to merge, for PetMed Express company
-- The match_targets_unofficial_map:
select targets.*
  from match_targets_unofficial_map targets
where targets.match_target like ('%petmed%')

+----------------------+--------+-------------------------+
| match_target         | source | match_target_unofficial |
+----------------------+--------+-------------------------+
| PetMed Express, Inc. | BEF    | petmed express          |
+----------------------+--------+-------------------------+

-- The wikipedia output:
select wf.input_string,
       wf.child_name,
       wf.parent1_name,
       wf.uid_agg_merch_master
  from wf_output_clean_tbl wf
 where wf.input_string like ('%petmed%')

+----------------+----------------------+--------------+--------------------------------------+
| input_string   | child_name           | parent1_name | uid_agg_merch_master                 |
+----------------+----------------------+--------------+--------------------------------------+
| Petmed Express | PetMed Express, Inc. | NULL         | 29180349-dc27-427c-a5ac-bb1175dc377f |
| PetMed Express | PetMed Express, Inc. | NULL         | f79833c8-a0f8-4b74-a491-1e9d869815d9 |
| PetMeds        | PetMed Express, Inc. | NULL         | c2a5ae48-7f16-4b92-8fcb-7eac5f9f5045 |
+----------------+----------------------+--------------+--------------------------------------+

-- The Bing output:
select bing.entity_name,
       bing.merchant_string,
       bing.entity_url,
       bing.bing_api_type,
       bing.created_at,
       bing.uid_agg_merch_master
  from bing_entityfinder_output bing
 where bing.entity_name like ('%petmed%')

+----------------------+-----------------+-----------------------------+---------------+----------------------------+---------------------------------------------------------------------------+
| entity_name          | merchant_string | entity_url                  | bing_api_type | created_at                 | uid_agg_merch_master                                                      |
+----------------------+-----------------+-----------------------------+---------------+----------------------------+---------------------------------------------------------------------------+
| PetMed Express, Inc. | PetMed Express  | http://www.1800petmeds.com/ | entity-entity | 2018-01-03 15:25:41.340003 | 29180349-dc27-427c-a5ac-bb1175dc377f,f79833c8-a0f8-4b74-a491-1e9d869815d9 |
| PetMed Express, Inc. | PetMeds         | http://www.1800petmeds.com/ | entity-entity | 2018-01-03 15:25:41.352503 | c2a5ae48-7f16-4b92-8fcb-7eac5f9f5045                                      |
| PetMed Express, Inc. | PetMed Express  | http://www.1800petmeds.com/ | web-entity    | 2018-01-03 15:28:25.300341 | 29180349-dc27-427c-a5ac-bb1175dc377f,f79833c8-a0f8-4b74-a491-1e9d869815d9 |
| PetMed Express, Inc. | PetMeds         | http://www.1800petmeds.com/ | web-entity    | 2018-01-03 15:28:25.322357 | c2a5ae48-7f16-4b92-8fcb-7eac5f9f5045                                      |
+----------------------+-----------------+-----------------------------+---------------+----------------------------+---------------------------------------------------------------------------+

select *
  from agg_merch_master_top_merchants_unique merchants
where merchants.merchant_string like ('%petmed%')
+-----------------+---------------------------------------------------------------------------+
| merchant_string | uidlist_agg_merch_master_top_merchants                                    |
+-----------------+---------------------------------------------------------------------------+
| PetMed Express  | 29180349-dc27-427c-a5ac-bb1175dc377f,f79833c8-a0f8-4b74-a491-1e9d869815d9 |
| PetMeds         | c2a5ae48-7f16-4b92-8fcb-7eac5f9f5045                                      |
+-----------------+---------------------------------------------------------------------------+


-- The query to join
--  match_targets_unofficial_map to bing_entityfinder_output to wf_output_clean_tbl to
--  agg_merch_master_top_merchants_unique
--  with filter by targets.source
select targets.*
     , bing.entity_name
     , wf.child_name
     , (case when targets.source = "BEF"
             then bing.merchant_string
             else wf.input_string
        end) as input_string
     , merchants.merchant_string
  from match_targets_unofficial_map targets
  left join bing_entityfinder_output bing
         on bing.entity_name = targets.match_target and targets.source = "BEF"
  left join wf_output_clean_tbl wf
         on wf.child_name = targets.match_target and targets.source = "WF"
  left join agg_merch_master_top_merchants_unique merchants on merchants.merchant_string = (
        case when targets.source = "BEF"
             then bing.merchant_string
             else wf.input_string
        end)
where merchants.merchant_string like ('%petmed%')

-- The source type in match_targets_unofficial_map is 'BEF', so we only get data from bing
-- although we also have the data in wikipedia output (so the next version of the query doesn't use filters)
+----------------------+--------+-------------------------+----------------------+------------+----------------+-----------------+
| match_target         | source | match_target_unofficial | entity_name          | child_name | input_string   | merchant_string |
+----------------------+--------+-------------------------+----------------------+------------+----------------+-----------------+
| PetMed Express, Inc. | BEF    | petmed express          | PetMed Express, Inc. | NULL       | PetMed Express | PetMed Express  |
| PetMed Express, Inc. | BEF    | petmed express          | PetMed Express, Inc. | NULL       | PetMeds        | PetMeds         |
| PetMed Express, Inc. | BEF    | petmed express          | PetMed Express, Inc. | NULL       | PetMed Express | PetMed Express  |
| PetMed Express, Inc. | BEF    | petmed express          | PetMed Express, Inc. | NULL       | PetMeds        | PetMeds         |
+----------------------+--------+-------------------------+----------------------+------------+----------------+-----------------+

-- Query without "source" filters
select targets.*
     , entity.entity_name
     , output.child_name
     , (case when entity.merchant_string
             then entity.merchant_string
             else output.input_string
        end) as input_string
     , merchants.merchant_string
  from match_targets_unofficial_map targets
  left join bing_entityfinder_output entity
         on entity.entity_name = targets.match_target
  left join wf_output_clean_tbl output
         on output.child_name = targets.match_target
  left join agg_merch_master_top_merchants_unique merchants on merchants.merchant_string = (
        case when entity.merchant_string
             then entity.merchant_string
             else output.input_string
        end)
group by targets.match_target, targets.source, targets.match_target_unofficial
order by RAND()
limit 30;

-- where merchants.merchant_string like ('%petmed%')
-- where targets.match_target = 'PetMed Express, Inc.'

/*
+--------------------------------+--------+---------------------------+-------------------------------+--------------------------------+-------------------------+-------------------------+
| match_target                   | source | match_target_unofficial   | entity_name                   | child_name                     | input_string            | merchant_string         |
+--------------------------------+--------+---------------------------+-------------------------------+--------------------------------+-------------------------+-------------------------+
| Big Boy Restaurants            | BEF    | big boy                   | Big Boy Restaurants           | NULL                           | NULL                    | NULL                    |
| Giordano's Pizzeria            | BEF    | giordano's pizzeria       | Giordano's Pizzeria           | NULL                           | NULL                    | NULL                    |
| NRG Energy, Inc.               | WF     | nrg energy                | NULL                          | NRG Energy, Inc.               | Reliant Energy          | Reliant Energy          |
| Comerica                       | BEF    | comerica                  | Comerica                      | NULL                           | NULL                    | NULL                    |
| Quality Food Centers, Inc.     | WF     | quality food centers      | NULL                          | Quality Food Centers, Inc.     | QFC                     | QFC                     |
| Alagasco                       | BEF    | alagasco                  | Alagasco                      | NULL                           | NULL                    | NULL                    |
| Martinizing Dry Cleaning       | BEF    | martinizing dry cleaning  | Martinizing Dry Cleaning      | NULL                           | NULL                    | NULL                    |
| Bay Area Rapid Transit         | BEF    | bay area rapid transit    | Bay Area Rapid Transit        | NULL                           | NULL                    | NULL                    |
| Enterprise Car Sales           | BEF    | enterprise car sales      | Enterprise Car Sales          | NULL                           | NULL                    | NULL                    |
| Five Below                     | BEF    | five below                | Five Below                    | five below                     | Five Below              | Five Below              |
| Teavana                        | BEF    | teavana                   | Teavana                       | NULL                           | NULL                    | NULL                    |
| Allen Edmonds Shoe Corporation | WF     | allen edmonds shoe        | NULL                          | Allen Edmonds Shoe Corporation | Allen Edmonds shop      | Allen Edmonds shop      |
| Tedeschi Food Shops            | BEF    | tedeschi food shops       | Tedeschi Food Shops           | NULL                           | NULL                    | NULL                    |
| RevZilla                       | WF     | revzilla                  | NULL                          | RevZilla                       | Revzilla Motorsports    | Revzilla Motorsports    |
| Anytime Fitness                | BEF    | anytime fitness           | Anytime Fitness               | Anytime Fitness                | Anytime Fitness         | Anytime Fitness         |
| Perry Ellis International      | WF     | perry ellis               | NULL                          | Perry Ellis International      | Perry Ellis             | Perry Ellis             |
| La Quinta Inns & Suites        | BEF    | la quinta inns and suites | La Quinta Inns & Suites       | NULL                           | NULL                    | NULL                    |
| Deckers Brands                 | WF     | deckers brands            | NULL                          | Deckers Brands                 | Deckers                 | Deckers                 |
| Mobil                          | BEF    | mobil                     | Mobil                         | Mobil                          | Mobil                   | Mobil                   |
| Savers, Inc.                   | WF     | savers                    | NULL                          | Savers, Inc.                   | Savers                  | Savers                  |
| Dallas Area Rapid Transit      | BEF    | dallas area rapid transit | Dallas Area Rapid Transit     | NULL                           | NULL                    | NULL                    |
| Great Wall Supermarket         | BEF    | great wall supermarket    | Great Wall Supermarket        | NULL                           | NULL                    | NULL                    |
| Seafood City                   | BEF    | seafood city              | Seafood City                  | NULL                           | NULL                    | NULL                    |
| Budget Truck Rental            | BEF    | budget truck rental       | Budget Truck Rental           | NULL                           | NULL                    | NULL                    |
| Greyhound Lines                | BEF    | greyhound lines           | Greyhound Lines               | NULL                           | NULL                    | NULL                    |
| Frontier Airlines              | BEF    | frontier airlines         | Frontier Airlines             | Frontier Airlines              | Frontier Airlines       | Frontier Airlines       |
| Scheels All Sports             | BEF    | scheels all sports        | Scheels All Sports            | Scheels All Sports             | Scheels                 | Scheels                 |
| Cornwell Tools                 | BEF    | cornwell tools            | Cornwell Tools                | NULL                           | NULL                    | NULL                    |
| Mandarin Oriental Hotel Group  | BEF    | mandarin oriental hotel   | Mandarin Oriental Hotel Group | Mandarin Oriental Hotel Group  | Jinlong Oriental Market | Jinlong Oriental Market |
| Tory Burch                     | BEF    | tory burch                | Tory Burch                    | Tory Burch                     | Tory Burch              | Tory Burch              |
+--------------------------------+--------+---------------------------+-------------------------------+--------------------------------+-------------------------+-------------------------+
*/

-- Query without source filters, with UIDs
select targets.*
     , bing.uid
     , bing.uid_agg_merch_master
     , bing.entity_name
     , wf.child_name
     , wf.uid_wf_run
     , wf.uid_agg_merch_master
     , (case when bing.merchant_string
             then bing.merchant_string
             else wf.input_string
        end) as input_string
     , merchants.merchant_string
  from match_targets_unofficial_map targets
  left join bing_entityfinder_output bing
         on bing.entity_name = targets.match_target
  left join wf_output_clean_tbl wf
         on wf.child_name = targets.match_target
  left join agg_merch_master_top_merchants_unique merchants on merchants.merchant_string = (
        case when bing.merchant_string
             then bing.merchant_string
             else wf.input_string
        end)
where targets.match_target = 'PetMed Express, Inc.'

--group by targets.match_target, targets.source, targets.match_target_unofficial

+----------------------+--------+-------------------------+--------------------------------------+---------------------------------------------------------------------------+----------------------+----------------------+--------------------------------------+--------------------------------------+----------------+-----------------+
| match_target         | source | match_target_unofficial | uid                                  | uid_agg_merch_master                                                      | entity_name          | child_name           | uid_wf_run                           | uid_agg_merch_master                 | input_string   | merchant_string |
+----------------------+--------+-------------------------+--------------------------------------+---------------------------------------------------------------------------+----------------------+----------------------+--------------------------------------+--------------------------------------+----------------+-----------------+
| PetMed Express, Inc. | BEF    | petmed express          | dda9911e-0297-4637-92cc-3e28211fe9d0 | 29180349-dc27-427c-a5ac-bb1175dc377f,f79833c8-a0f8-4b74-a491-1e9d869815d9 | PetMed Express, Inc. | PetMed Express, Inc. | 0b0b3289-aefd-4763-bfc0-6584af13b72b | 29180349-dc27-427c-a5ac-bb1175dc377f | Petmed Express | PetMed Express  |
| PetMed Express, Inc. | BEF    | petmed express          | 991ce450-fcbf-4866-9734-a96d8651eaf8 | c2a5ae48-7f16-4b92-8fcb-7eac5f9f5045                                      | PetMed Express, Inc. | PetMed Express, Inc. | 0b0b3289-aefd-4763-bfc0-6584af13b72b | 29180349-dc27-427c-a5ac-bb1175dc377f | Petmed Express | PetMed Express  |
| PetMed Express, Inc. | BEF    | petmed express          | c5ceb071-6f19-44be-af38-a6091663c176 | 29180349-dc27-427c-a5ac-bb1175dc377f,f79833c8-a0f8-4b74-a491-1e9d869815d9 | PetMed Express, Inc. | PetMed Express, Inc. | 0b0b3289-aefd-4763-bfc0-6584af13b72b | 29180349-dc27-427c-a5ac-bb1175dc377f | Petmed Express | PetMed Express  |
| PetMed Express, Inc. | BEF    | petmed express          | a524778a-0f51-4b16-9451-97b500c9fe7f | c2a5ae48-7f16-4b92-8fcb-7eac5f9f5045                                      | PetMed Express, Inc. | PetMed Express, Inc. | 0b0b3289-aefd-4763-bfc0-6584af13b72b | 29180349-dc27-427c-a5ac-bb1175dc377f | Petmed Express | PetMed Express  |
| PetMed Express, Inc. | BEF    | petmed express          | dda9911e-0297-4637-92cc-3e28211fe9d0 | 29180349-dc27-427c-a5ac-bb1175dc377f,f79833c8-a0f8-4b74-a491-1e9d869815d9 | PetMed Express, Inc. | PetMed Express, Inc. | 5b719ad7-e16a-425e-9499-e0a48265d3f2 | f79833c8-a0f8-4b74-a491-1e9d869815d9 | PetMed Express | PetMed Express  |
| PetMed Express, Inc. | BEF    | petmed express          | 991ce450-fcbf-4866-9734-a96d8651eaf8 | c2a5ae48-7f16-4b92-8fcb-7eac5f9f5045                                      | PetMed Express, Inc. | PetMed Express, Inc. | 5b719ad7-e16a-425e-9499-e0a48265d3f2 | f79833c8-a0f8-4b74-a491-1e9d869815d9 | PetMed Express | PetMed Express  |
| PetMed Express, Inc. | BEF    | petmed express          | c5ceb071-6f19-44be-af38-a6091663c176 | 29180349-dc27-427c-a5ac-bb1175dc377f,f79833c8-a0f8-4b74-a491-1e9d869815d9 | PetMed Express, Inc. | PetMed Express, Inc. | 5b719ad7-e16a-425e-9499-e0a48265d3f2 | f79833c8-a0f8-4b74-a491-1e9d869815d9 | PetMed Express | PetMed Express  |
| PetMed Express, Inc. | BEF    | petmed express          | a524778a-0f51-4b16-9451-97b500c9fe7f | c2a5ae48-7f16-4b92-8fcb-7eac5f9f5045                                      | PetMed Express, Inc. | PetMed Express, Inc. | 5b719ad7-e16a-425e-9499-e0a48265d3f2 | f79833c8-a0f8-4b74-a491-1e9d869815d9 | PetMed Express | PetMed Express  |
| PetMed Express, Inc. | BEF    | petmed express          | dda9911e-0297-4637-92cc-3e28211fe9d0 | 29180349-dc27-427c-a5ac-bb1175dc377f,f79833c8-a0f8-4b74-a491-1e9d869815d9 | PetMed Express, Inc. | PetMed Express, Inc. | f3a33271-822f-4d9f-b832-0c674289baf6 | c2a5ae48-7f16-4b92-8fcb-7eac5f9f5045 | PetMeds        | PetMeds         |
| PetMed Express, Inc. | BEF    | petmed express          | 991ce450-fcbf-4866-9734-a96d8651eaf8 | c2a5ae48-7f16-4b92-8fcb-7eac5f9f5045                                      | PetMed Express, Inc. | PetMed Express, Inc. | f3a33271-822f-4d9f-b832-0c674289baf6 | c2a5ae48-7f16-4b92-8fcb-7eac5f9f5045 | PetMeds        | PetMeds         |
| PetMed Express, Inc. | BEF    | petmed express          | c5ceb071-6f19-44be-af38-a6091663c176 | 29180349-dc27-427c-a5ac-bb1175dc377f,f79833c8-a0f8-4b74-a491-1e9d869815d9 | PetMed Express, Inc. | PetMed Express, Inc. | f3a33271-822f-4d9f-b832-0c674289baf6 | c2a5ae48-7f16-4b92-8fcb-7eac5f9f5045 | PetMeds        | PetMeds         |
| PetMed Express, Inc. | BEF    | petmed express          | a524778a-0f51-4b16-9451-97b500c9fe7f | c2a5ae48-7f16-4b92-8fcb-7eac5f9f5045                                      | PetMed Express, Inc. | PetMed Express, Inc. | f3a33271-822f-4d9f-b832-0c674289baf6 | c2a5ae48-7f16-4b92-8fcb-7eac5f9f5045 | PetMeds        | PetMeds         |
+----------------------+--------+-------------------------+--------------------------------------+---------------------------------------------------------------------------+----------------------+----------------------+--------------------------------------+--------------------------------------+----------------+-----------------+
