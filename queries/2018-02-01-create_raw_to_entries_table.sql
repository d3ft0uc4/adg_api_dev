-- Join composite_model_output with clean_cc
--
-- clean_cc has td2 and dc1 columns that are merchant strings
-- (also td0 and dc0, but we ignore them)
-- we need to join composite_model_output to that

DROP VIEW IF EXISTS raw_to_entities;
DROP TABLE IF EXISTS raw_to_entities_tbl;

create view raw_to_entities AS (
select model.merchant_string,
       model.uidlist_agg_merch_master_top_merchants,
       -- model.source_entity_name as entity_name,
       model.unofficial as unofficial_entity_name,
       model.cluster_id,
       clean_cc.uid_raw_cc as uid_raw_cc,
       clean_cc.raw_trans_string,
       clean_cc.clean_trans_string
  from clean_cc
  join composite_model_output_tbl model
    on model.merchant_string = case
       when clean_cc.td2 then clean_cc.td2
       else clean_cc.dc1
    end
 where clean_cc.td2 is not NULL
    or clean_cc.dc1 is not NULL
);

CREATE TABLE raw_to_entities_tbl as SELECT * FROM raw_to_entities;

CREATE INDEX `idx_merchant_string` on raw_to_entities_tbl (merchant_string(1024));
CREATE INDEX `idx_unofficial_entity_name` on raw_to_entities_tbl(unofficial_entity_name(1024));
CREATE INDEX `idx_cluster_id` on raw_to_entities_tbl(cluster_id);

drop view if exists mlp_training_set;

create view mlp_training_set as (
  select clean_trans_string,
         unofficial_entity_name
    from raw_to_entities_tbl
);

-- The version with union:
/*
(
select *
  from clean_cc
  left
  join composite_model_output model
    on model.merchant_string = clean_cc.td2
 where clean_cc.td2 is not NULL
 limit 10
)
union
(
select *
  from clean_cc
  left
  join composite_model_output model
    on model.merchant_string = clean_cc.dc1
 where clean_cc.td2 is NULL
   and clean_cc.dc1 is not NULL
 limit 10
)
*/

/* Examination

select * from composite_model_output limit 5;

+------------------------+----------------------------------------+------------------------+------------+
| merchant_string        | uidlist_agg_merch_master_top_merchants | unofficial             | cluster_id |
+------------------------+----------------------------------------+------------------------+------------+
| 76                     | e2a08e3b-7167-4349-bcfd-7e0b65d17bc7   | 76                     |          1 |
| 21st Century Insurance | c63bf2d0-6c23-4041-ad6a-1f1de5b83277   | 21st century insurance |          3 |
| A.C. Moore             | ab3a6172-5a9f-41ae-b5d6-887d6997c20a   | a.c. moore             |          4 |
| AbeBooks               | 57ccf11f-f442-4491-b041-b9ab41d29c52   | abebooks               |          6 |
| Aloft Hotels           | dbd98fbd-1bd0-4c8b-9a21-42ec0ae35fb6   | aloft hotels           |          8 |
+------------------------+----------------------------------------+------------------------+------------+

select * from clean_cc limit 5;
+--------------------------------------+---------------------+-------------------------------------------+-------------------------------+------------+-------+---------+------+------+------+------+
| uid                                  | cc_datestamp        | raw_trans_string                          | clean_trans_string            | city       | state | version | td2  | dc0  | dc1  | td0  |
+--------------------------------------+---------------------+-------------------------------------------+-------------------------------+------------+-------+---------+------+------+------+------+
| 000012f5-5994-4d36-b46a-2076f4f60a6c | 2018-01-24 22:06:59 | KMART 03895 KMART #3895  PITTSBURGH   PA | KMART KMART                   | PITTSBURGH | PA    | 0.0.3   | NULL | NULL | NULL | NULL |
| 00003de4-4265-48d0-952e-480645a7028f | 2018-01-24 22:06:59 | Interest Charged on Purchases | INTEREST CHARGED ON PURCHASES | NULL       | NULL  | 0.0.3   | NULL | NULL | NULL | NULL |
| 00006122-0de5-4653-b6f2-eeb8b0dabad1 | 2018-01-21 23:35:06 | WAL-MART #2537 | WAL-MART                      | NULL       | NULL  | 0.0.2   | NULL | NULL | NULL | NULL |
| 00006ba1-0bf0-455b-bc89-13166d3ad7c1 | 2018-01-24 22:06:59 | MCDONALD'S F32123        MARQUETTE    MI | MCDONALD'S                    | MARQUETTE  | MI    | 0.0.3   | NULL | NULL | NULL | NULL |
| 0000ae9b-94ea-47ee-b9c5-329f37dd919e | 2018-01-24 22:06:59 | USPS.COM CLICK66100611 | USPS.COM                      | NULL       | NULL  | 0.0.3   | NULL | NULL | NULL | NULL |
+--------------------------------------+---------------------+-------------------------------------------+-------------------------------+------------+-------+---------+------+------+------+------+

select * from raw_cc limit 5;
+--------------------------------------+-----------------------------------------------+----------+---------+----------------+----------+-----------+---------------------+----------+
| uuid                                 | raw_trans_string                              | td2      | dc0     | dc1            | td0      | td_ticker | td_err_merchantname | cardtype |
+--------------------------------------+-----------------------------------------------+----------+---------+----------------+----------+-----------+---------------------+----------+
| 8397d778-d6b1-11e7-8c9a-9b8cf041e0b1 | STEP INTO SUCCESS INC  XXXXX0049 XXXXX1075 MI | NULL     | NULL    | NULL           | NULL     | NULL      | NULL                | card     |
| 8397d779-d6b1-11e7-8c9a-9b8cf041e0b1 | LTFLIFE TIME MO DUES  XXXXX0043 XXXXX6432 MN  | NULL     | NULL    | Dues           | NULL     | NULL      | NULL                | card     |
| 8397d77a-d6b1-11e7-8c9a-9b8cf041e0b1 | #NETFLIX.COM           NETFLIX.COM  CA        | Netflix  | Netflix | Netflix        | Netflix  | NULL      | NULL                | card     |
| 8397d77b-d6b1-11e7-8c9a-9b8cf041e0b1 | PETSUITES ERLANGER       ERLANGER     KY      | NULL     | NULL    | PETSuites      | NULL     | NULL      | NULL                | card     |
| 8397d77c-d6b1-11e7-8c9a-9b8cf041e0b1 | PP FACEBOOKINC  XXXXX0046 XXXXX7733 CA        | Facebook | NULL    | Facebook, Inc. | Facebook | NULL      | NULL                | card     |
+--------------------------------------+-----------------------------------------------+----------+---------+----------------+----------+-----------+---------------------+----------+

select count(*) from clean_cc;
310000

select count(*) from clean_cc where td2 is not NULL or dc1 is not NULL;
98619

select 98619.0 / 310000.0;
0.31813

select count(*) from raw_cc;
4396733

select count(*) from raw_cc where td2 is not NULL or dc1 is not NULL;
2941756

select 2941756.0 / 4396733.0;
0.66908

*/
