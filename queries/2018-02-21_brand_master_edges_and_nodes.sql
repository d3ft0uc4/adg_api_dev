create view brand_master_edges as 
select uuid() as uid, nodes.official_name as node1_name, nodes.uid as uid_node1, nodes2.official_name as node2_name, nodes2.uid as uid_node2
, edges.model_score as model_score, 'DIRECT' as edge_type
, CURDATE() as created_date, '2999-12-31' as archive_date 
from fw_response_pivot_t2t as edges

left join brand_master_nodes as nodes on edges.input_string = nodes.match_targets
left join brand_master_nodes as nodes2 on edges.match_target = nodes2.match_targets

where edges.model_preds = 1;




create view brand_master_nodes as 
(select uuid() as uid, entity_name as official_name, match_target as match_targets
, null as identifiers, 'BRAND' as node_type
, CURDATE() as created_date, '2999-12-31' as archive_date 
from combined_entity_to_merchant_string group by match_target, entity_name)