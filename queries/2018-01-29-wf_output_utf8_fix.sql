/*
 * The script to fix the utf8 issue in wf_output and wf_output_clean_tbl.
 * The data with unicode chars was incorectly stored as latin1 (and probably the
 * table encoding was later changed to utf8), so some of the records have broken
 * utf chars, see wf_output_utf8_problem_examination.sql for examples.
 */

-- We need to update:
-- - wf_output.child_name
-- - wf_output.parent1_name
-- - wf_output.parent2_name
-- - wf_output_clean_tbl.child_name
-- - wf_output_clean_tbl.parent1_name

/*
 * The data to fix:
 *
SELECT input_string, child_name
FROM wf_output_clean_tbl
WHERE CONVERT(child_name USING BINARY) RLIKE CONCAT('[', UNHEX('80'), '-', UNHEX('FF'), ']');

SELECT input_string, parent1_name
FROM wf_output_clean_tbl
WHERE CONVERT(parent1_name USING BINARY) RLIKE CONCAT('[', UNHEX('80'), '-', UNHEX('FF'), ']');

SELECT input_string, child_name
FROM wf_output
WHERE CONVERT(child_name USING BINARY) RLIKE CONCAT('[', UNHEX('80'), '-', UNHEX('FF'), ']');

SELECT input_string, parent1_name
FROM wf_output
WHERE CONVERT(parent1_name USING BINARY) RLIKE CONCAT('[', UNHEX('80'), '-', UNHEX('FF'), ']');

SELECT input_string, parent2_name
FROM wf_output
WHERE CONVERT(parent2_name USING BINARY) RLIKE CONCAT('[', UNHEX('80'), '-', UNHEX('FF'), ']');
*/

-- The procedure to fix the data:
-- - rename existing data to wf_output_backup, wf_output_clean_tbl_backup
-- - create new tables: wf_output, wf_output_clean_tbl, copy the data from "*_backup" tables
-- - run queries to fix the utf problem on the new wt_output and wf_output_clean_tbl

alter table wf_output rename wf_output_backup;
CREATE TABLE wf_output LIKE wf_output_backup;
INSERT wf_output SELECT * FROM wf_output_backup;

alter table wf_output_clean_tbl rename wf_output_clean_tbl_backup;
CREATE TABLE wf_output_clean_tbl LIKE wf_output_clean_tbl_backup;
INSERT wf_output_clean_tbl SELECT * FROM wf_output_clean_tbl_backup;

update wf_output_clean_tbl
set child_name = convert(convert(convert(
         (case when locate("\n", child_name) > 0
             then substring(child_name, 1, locate("\n", child_name) - 1)
             else child_name
          end)
      using latin1) using binary) using utf8)
 where convert(convert(child_name using binary) using latin1) != convert(convert(child_name using binary) using utf8);

update wf_output_clean_tbl
set parent1_name = convert(convert(convert(
         (case when locate("\n", parent1_name) > 0
             then substring(parent1_name, 1, locate("\n", parent1_name) - 1)
             else parent1_name
          end)
      using latin1) using binary) using utf8)
 where convert(convert(parent1_name using binary) using latin1) != convert(convert(parent1_name using binary) using utf8);

update wf_output
set child_name = convert(convert(convert(
         (case when locate("\n", child_name) > 0
             then substring(child_name, 1, locate("\n", child_name) - 1)
             else child_name
          end)
      using latin1) using binary) using utf8)
 where convert(convert(child_name using binary) using latin1) != convert(convert(child_name using binary) using utf8);

update wf_output
set parent1_name = convert(convert(convert(
         (case when locate("\n", parent1_name) > 0
             then substring(parent1_name, 1, locate("\n", parent1_name) - 1)
             else parent1_name
          end)
      using latin1) using binary) using utf8)
 where convert(convert(parent1_name using binary) using latin1) != convert(convert(parent1_name using binary) using utf8);

update wf_output
set parent2_name = convert(convert(convert(
         (case when locate("\n", parent2_name) > 0
             then substring(parent2_name, 1, locate("\n", parent2_name) - 1)
             else parent2_name
          end)
      using latin1) using binary) using utf8)
 where convert(convert(parent2_name using binary) using latin1) != convert(convert(parent2_name using binary) using utf8);


-- Another plan: add new fields for fixed data:
-- - add wf_output.child_name_cleaned, store the fixed data into it
-- - do the same with wf_output.parent1_name, wf_output.parent2_name
-- - do the same with wf_output_clean_tbl.child_name and wf_output_clean_tbl.parent1_name

/*
-- Create copies of original tables, just for the case, can be removed later
alter table wf_output rename wf_output_backup;
CREATE TABLE wf_output LIKE wf_output_backup;
INSERT wf_output SELECT * FROM wf_output_backup;

alter table wf_output_clean_tbl rename wf_output_clean_tbl_backup;
CREATE TABLE wf_output_clean_tbl LIKE wf_output_clean_tbl_backup;
INSERT wf_output_clean_tbl SELECT * FROM wf_output_clean_tbl_backup;

-- Add fields for cleaned output
alter table wf_output add column child_name_cleaned text after child_name;
alter table wf_output add column parent1_name_cleaned text after parent1_name;
alter table wf_output add column parent2_name_cleaned text after parent2_name;
alter table wf_output_clean_tbl add column child_name_cleaned text after child_name;
alter table wf_output_clean_tbl add column parent1_name_cleaned text after parent1_name;

-- Initially set cleaned values to be same as initial values:
update wf_output_clean_tbl set child_name_cleaned = child_name;
update wf_output_clean_tbl set parent1_name_cleaned = parent1_name;
update wf_output set child_name_cleaned = child_name;
update wf_output set parent1_name_cleaned = parent1_name;
update wf_output set parent2_name_cleaned = parent2_name;

-- Update "cleaned" fields to correct values (fix the utf8 issue):
update wf_output_clean_tbl
set child_name_cleaned = convert(convert(convert(
         (case when locate("\n", child_name) > 0
             then substring(child_name, 1, locate("\n", child_name) - 1)
             else child_name
          end)
      using latin1) using binary) using utf8)
 where convert(convert(child_name using binary) using latin1) != convert(convert(child_name using binary) using utf8);

update wf_output_clean_tbl
set parent1_name_cleaned = convert(convert(convert(
         (case when locate("\n", parent1_name) > 0
             then substring(parent1_name, 1, locate("\n", parent1_name) - 1)
             else parent1_name
          end)
      using latin1) using binary) using utf8)
 where convert(convert(parent1_name using binary) using latin1) != convert(convert(parent1_name using binary) using utf8);

update wf_output
set child_name_cleaned = convert(convert(convert(
         (case when locate("\n", child_name) > 0
             then substring(child_name, 1, locate("\n", child_name) - 1)
             else child_name
          end)
      using latin1) using binary) using utf8)
 where convert(convert(child_name using binary) using latin1) != convert(convert(child_name using binary) using utf8);

update wf_output
set parent1_name_cleaned = convert(convert(convert(
         (case when locate("\n", parent1_name) > 0
             then substring(parent1_name, 1, locate("\n", parent1_name) - 1)
             else parent1_name
          end)
      using latin1) using binary) using utf8)
 where convert(convert(parent1_name using binary) using latin1) != convert(convert(parent1_name using binary) using utf8);

update wf_output
set parent2_name_cleaned = convert(convert(convert(
         (case when locate("\n", parent2_name) > 0
             then substring(parent2_name, 1, locate("\n", parent2_name) - 1)
             else parent2_name
          end)
      using latin1) using binary) using utf8)
 where convert(convert(parent2_name using binary) using latin1) != convert(convert(parent2_name using binary) using utf8);

SELECT input_string,
     child_name_cleaned, child_name,
     parent1_name, parent1_name_cleaned,
     parent2_name, parent2_name_cleaned
FROM wf_output
WHERE child_name_cleaned != child_name OR
      parent1_name != parent1_name_cleaned OR
      parent2_name != parent2_name_cleaned;

SELECT input_string,
     child_name_cleaned, child_name,
     parent1_name, parent1_name_cleaned
FROM wf_output_clean_tbl
WHERE child_name_cleaned != child_name OR
      parent1_name != parent1_name_cleaned;
*/


/*
-- Re-create view wf_output_clean since we have renamed the table it
-- was build for.
DROP VIEW IF EXISTS `wf_output_clean`;

CREATE VIEW `wf_output_clean` AS
SELECT `a`.`uid_wf_run` AS `uid_wf_run`
     , `a`.`uid_agg_merch_master` AS `uid_agg_merch_master`
     , `a`.`input_string` AS `input_string`
     , `a`.`child_name` AS `child_name`
     , `a`.`child_industry` AS `child_industry`
     , `a`.`child_ticker_list` AS `child_ticker_list`
     , `a`.`child_ticker` AS `child_ticker`
     , `a`.`child_exchange` AS `child_exchange`
     , `a`.`parent1_name` AS `parent1_name`
     , `a`.`parent1_industry` AS `parent1_industry`
     , `a`.`parent1_ticker_list` AS `parent1_ticker_list`
     , `a`.`parent1_ticker` AS `parent1_ticker`
     , `a`.`parent1_exchange` AS `parent1_exchange`
     , (
    SELECT `wf_wiki_pages`.`value`
      FROM `wf_wiki_pages`
     WHERE ((`wf_wiki_pages`.`uid_wf_run`  = `a`.`uid_wf_run`)
       AND (`wf_wiki_pages`.`scrape_order` = 1)
   AND (`wf_wiki_pages`.`key`          = 'company_url_count'))) AS `company_url_count_pg1`,(
    SELECT `wf_wiki_pages`.`value`
      FROM `wf_wiki_pages`
     WHERE ((`wf_wiki_pages`.`uid_wf_run`  = `a`.`uid_wf_run`)
       AND (`wf_wiki_pages`.`scrape_order` = 1)
   AND (`wf_wiki_pages`.`key`          = 'company_table_head_count'))) AS `company_table_head_count_pg1`,(
    SELECT `wf_wiki_pages`.`value`
      FROM `wf_wiki_pages`
     WHERE ((`wf_wiki_pages`.`uid_wf_run`  = `a`.`uid_wf_run`)
       AND (`wf_wiki_pages`.`scrape_order` = 1)
   AND (`wf_wiki_pages`.`key`          = 'article_title_count'))) AS `article_title_count_pg1`
  FROM `wf_output` `a`
 WHERE ((`a`.`target_bool`             = TRUE)
   AND (`a`.`child_name` is not null));
*/
