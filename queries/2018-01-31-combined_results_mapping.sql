/*

Build the combined_results_mapping table:
* select agg_merch_master_top_merchants_unique.merchant_string
* join 3 sources:
** bing_entityfinder_output on merchant_string = merchant_string with bing_api_type = web-entity (source will be bef-web)
** bing_entityfinder_output on merchant_string = merchant_string with bing_api_type = entity-entity (source will be bef-entity)
** wf_output_unique_merch on input_string = merchant_string (source will be wf)

Columns:
* merchant_string (from agg_merch_master_top_merchants_unique)
* source [one of: bef-web, bef-entity, wf]
* scraped_entity
** bing_entityfinder_output.entity_name for source = bef-web
** bing_entityfinder_output.entity_name for source = bef-entity
** wf_output_unique_merch.child_name for source = wf
* unofficial_scraped_entity [official-to-unofficial transformation on scraped_entity company name string]
** mapping can be taken from the match_targets_unofficial_map table, match_target - official name, match_target_unofficial - unoffical
* unofficial_cluster_id from FW_groups table

*/

/* too many `case` statemets here, the similar query with unions below looks better
select case when bing_web.bing_api_type = 'web-entity'
            then 'bef-web'
            when bing_entity.bing_api_type = 'entity-entity'
            then 'bef-entity'
            when wf.input_string is not NULL
            then 'wf'
            else NULL
       end as source,
       case when bing_web.bing_api_type = 'web-entity'
            then bing_web.entity_name
            when bing_entity.bing_api_type = 'entity-entity'
            then bing_entity.entity_name
            when wf.input_string is not NULL
            then wf.child_name
            else NULL
       end as scraped_entity,
       case when bing_web.bing_api_type = 'web-entity'
            then bing_web_map.match_target_unofficial
            when bing_entity.bing_api_type = 'entity-entity'
            then bing_entity_map.match_target_unofficial
            when wf.input_string is not NULL
            then wf_map.match_target_unofficial
            else NULL
      end as unofficial_scraped_entity
  from agg_merch_master_top_merchants_unique merchant
  left
  join bing_entityfinder_output bing_web
    on bing_web.bing_api_type = 'web-entity'
   and bing_web.merchant_string = merchant.merchant_string
  join match_targets_unofficial_map bing_web_map
    on bing_web_map.match_target = bing_web.entity_name
  left
  join bing_entityfinder_output bing_entity
    on bing_entity.bing_api_type = 'entity-entity'
   and bing_entity.merchant_string = merchant.merchant_string
  join match_targets_unofficial_map bing_entity_map
    on bing_entity_map.match_target = bing_entity.entity_name
  left
    -- TODO: replace the table to wf_output_unique_merch
  join wf_output wf
    on wf.input_string = merchant.merchant_string
  join match_targets_unofficial_map wf_map
    on wf_map.match_target = wf.input_string
 order
    by merchant.merchant_string
 limit 20;
 */

-- Similar to above, but with unions
(
select merchant.merchant_string,
       merchant.uidlist_agg_merch_master_top_merchants as uidlist_agg_merch_master_top_merchants,
       case
          when bing.bing_api_type = 'web-entity'
          then 'bef-web'
          else 'bef-entity'
       end as source,
       bing.uid as source_uid,
       bing.entity_name as scraped_entity_name,
       map.match_target_unofficial as unofficial_scraped_entity_name,
       groups.cluster_id
  from agg_merch_master_top_merchants_unique merchant
  left
  join bing_entityfinder_output bing
    on bing.merchant_string = merchant.merchant_string
  left
  join match_targets_unofficial_map map
    on map.match_target = bing.entity_name
  left
  join FW_groups groups
    -- TODO: should we join to match_targets_unofficial_map?
    on groups.match_target = map.match_target_unofficial
where (
      bing.bing_api_type = 'web-entity'
   or bing.bing_api_type = 'entity-entity'
  )
  and bing.run_code = 'full_run_2' and bing.uid is not null
limit 10
) union (
select merchant.merchant_string,
       merchant.uidlist_agg_merch_master_top_merchants as uidlist_agg_merch_master_top_merchants,
       'wf' as source,
       wf.uid_wf_run as source_uid,
       wf.child_name as scraped_entity_name,
       map.match_target_unofficial as unofficial_scraped_entity_name,
       groups.cluster_id
  from agg_merch_master_top_merchants_unique merchant
   -- TODO: replace the table to wf_output_unique_merch
  left
  join wf_output_unique_merch wf
    on wf.input_string = merchant.merchant_string
  left
  join match_targets_unofficial_map map
    on map.match_target = wf.child_name
  left
  join FW_groups groups
    -- TODO: should we join to match_targets_unofficial_map?
    on groups.match_target = map.match_target_unofficial
limit 10
)
