-- Run example:
-- mysql --defaults-extra-file=local.cnf altdg_merchtag_local < adg_api_dev/queries/2018-01-31-create_wf_output_unique_merch_table.sql
-- mysql --defaults-extra-file=production.cnf altdg_merchtag < adg_api_dev/queries/2018-01-31-create_wf_output_unique_merch_table.sql

-- The unique data from wf_output_clean_tbl
DROP VIEW IF EXISTS wf_output_unique_merch;
DROP TABLE IF EXISTS wf_output_unique_merch_tbl;

CREATE VIEW wf_output_unique_merch AS (
select
      input_string,
      child_name,
      child_industry,
      child_ticker_list,
      child_ticker,
      child_exchange,
      parent1_name,
      parent1_industry,
      parent1_ticker_list,
      parent1_ticker,
      parent1_exchange,
      max(company_url_count_pg1) as company_url_count_pg1,
      max(article_title_count_pg1) as article_title_count_pg1,
      max(company_table_head_count_pg1) as company_table_head_count_pg1,
      max(uid_wf_run) as uid_wf_run,
      group_concat(uid_agg_merch_master separator ',') as uidlist_agg_merch_master,
      count(*) as count_original_rows
 from wf_output_clean_tbl
group
   by input_string,
      child_name,
      child_industry,
      child_ticker_list,
      child_ticker,
      child_exchange,
      parent1_name,
      parent1_industry,
      parent1_ticker_list,
      parent1_ticker,
      parent1_exchange
);

CREATE TABLE wf_output_unique_merch_tbl as SELECT * FROM wf_output_unique_merch;

CREATE INDEX `idx_uid_wf_run` on wf_output_unique_merch_tbl (uid_wf_run(1024));
CREATE INDEX `idx_uidlist_agg_merch_master` on wf_output_unique_merch_tbl (uidlist_agg_merch_master(1024));
CREATE INDEX `idx_input_string` on wf_output_unique_merch_tbl (input_string(1024));
CREATE INDEX `idx_child_name` on wf_output_unique_merch_tbl (child_name(1024));
CREATE INDEX `idx_parent1_name` on wf_output_unique_merch_tbl (parent1_name(1024));

/*

-- Get the input_strings that are still non-unique
select input_string, count(*)
  from temp_wf_output_unique_merch
group by input_string
having count(*) > 1;

-- Below are examination results for `wf_output` table.
-- We don't use it, the main source is the `wf_output_clean_tbl`.
--
-- Get unique data from wf_output,
-- select any uid_wf_run
-- group uid_agg_merch_master into comma-separated list
select
      count(*),
      input_string,
      child_name,
      child_industry,
      child_ticker_list,
      child_ticker,
      child_exchange,
      parent1_name,
      parent1_industry,
      parent1_ticker_list,
      parent1_ticker,
      parent1_exchange,
      parent2_name,
      parent2_industry,
      parent2_ticker_list,
      parent2_ticker,
      parent2_exchange,
      uid_wf_run,
      group_concat(uid_agg_merch_master separator ',')
 from wf_output
group
   by input_string,
      child_name,
      child_industry,
      child_ticker_list,
      child_ticker,
      child_exchange,
      parent1_name,
      parent1_industry,
      parent1_ticker_list,
      parent1_ticker,
      parent1_exchange,
      parent2_name,
      parent2_industry,
      parent2_ticker_list,
      parent2_ticker,
      parent2_exchange


-- Find which input_strings are still duplicate after we
-- make the data unique:
CREATE TEMPORARY TABLE IF NOT EXISTS wf_output_unique_merch AS (
select
      count(*), input_string, child_name, child_industry, child_ticker_list, child_ticker, child_exchange,
      parent1_name, parent1_industry, parent1_ticker_list, parent1_ticker, parent1_exchange,
      parent2_name, parent2_industry, parent2_ticker_list, parent2_ticker, parent2_exchange,
      uid_wf_run, group_concat(uid_agg_merch_master separator ',')
 from wf_output
group
   by input_string, child_name, child_industry, child_ticker_list, child_ticker, child_exchange,
      parent1_name, parent1_industry, parent1_ticker_list, parent1_ticker, parent1_exchange,
      parent2_name, parent2_industry, parent2_ticker_list, parent2_ticker, parent2_exchange
);
select input_string, count(*)
  from temp_wf_output_unique_merch
 group
    by input_string
having count(*) > 1;

+--------------------------+----------+
| input_string             | count(*) |
+--------------------------+----------+
| Bristol Farms            |        2 |
| Cafe Duke                |        2 |
| China House              |        2 |
| CVS Pharmacy             |        2 |
| Daily Grind              |        2 |
| E-Z Mart                 |        2 |
| Good Fortune Supermarket |        2 |
| Goody's                  |        2 |
| La Hacienda              |        2 |
| MARTA                    |        2 |
| Mi Cocina Restaurant     |        2 |
| Nail Spa                 |        2 |
| New China                |        2 |
| Oncue Express            |        2 |
| Paper Source             |        2 |
| Pappasito's Cantina      |        2 |
| Pepco                    |        2 |
| ScoreSense               |        2 |
| Togo's                   |        2 |
+--------------------------+----------+

-- Get the full data for duplicate input_strings
select
      count(*), input_string, child_name, child_industry, child_ticker_list, child_ticker, child_exchange,
      parent1_name, parent1_industry, parent1_ticker_list, parent1_ticker, parent1_exchange,
      parent2_name, parent2_industry, parent2_ticker_list, parent2_ticker, parent2_exchange,
      uid_wf_run, group_concat(uid_agg_merch_master separator ',')
 from wf_output
where input_string in (
   'Bristol Farms',
   'Cafe Duke',
   'China House',
   'CVS Pharmacy',
   'Daily Grind',
   'E-Z Mart',
   'Good Fortune Supermarket',
   'Goody\'s',
   'La Hacienda',
   'MARTA',
   'Mi Cocina Restaurant',
   'Nail Spa',
   'New China',
   'Oncue Express',
   'Paper Source',
   'Pappasito\'s Cantina',
   'Pepco',
   'ScoreSense',
   'Togo\'s'
)
group
   by input_string, child_name, child_industry, child_ticker_list, child_ticker, child_exchange,
      parent1_name, parent1_industry, parent1_ticker_list, parent1_ticker, parent1_exchange,
      parent2_name, parent2_industry, parent2_ticker_list, parent2_ticker, parent2_exchange;
*/
