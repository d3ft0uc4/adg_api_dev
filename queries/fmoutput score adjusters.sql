
drop view fm_output_substr_scores;
CREATE view fm_output_substr_scores  AS
  select a.uid uid_fm_output, max(a.input_string) input_string, max(a.target_name_clean) target_sub, max(b.target_name_clean) target_super, max( a.search_addon_string)  search_addon_string, max(a.search_result_type) search_result_type,
  max(a.created_at) created_at, max(a.run_code) run_code, max(a.score_type) score_type, max(a.score) score, a.score- MAX(b.score) score_substr1 ,  MAX(b.score) / (a.score*1.0) substr_ratio,
  case when MAX(b.score) / (a.score*1.0) >=0.4 then 0
  else   a.score- MAX(b.score) 
  end as score_substr2
  from fm_output a
    right  join fm_output b
        on  a.input_string=b.input_string
         and a.target_name_clean != b.target_name_clean
          and b.target_name_clean REGEXP_CONTAINS  concat('(^|[ ])', a.target_name_clean, '($|[ ])')
          and a.search_addon_string=b.search_addon_string
          and a.search_result_type=b.search_result_type
          and a.score_type=b.score_type
          and a.run_code=b.run_code
  GROUP BY a.uid;
  
drop view fm_output_substr_scores_bak;
CREATE view fm_output_substr_scores_bak  AS
  select a.uid uid_fm_output, max(a.input_string) input_string, max(a.target_name_clean) target_sub, max(b.target_name_clean) target_super, max( a.search_addon_string)  search_addon_string, max(a.search_result_type) search_result_type,
  max(a.created_at) created_at, max(a.run_code) run_code, max(a.score_type) score_type, max(a.score) score, a.score- MAX(b.score) score_substr1 ,  MAX(b.score) / (a.score*1.0) substr_ratio,
  case when MAX(b.score) / (a.score*1.0) >=0.4 then 0
  else   a.score- MAX(b.score) 
  end as score_substr2
  from fm_output_bak_may2018 a
    right  join fm_output_bak_may2018 b
        on  a.input_string=b.input_string
         and a.target_name_clean != b.target_name_clean
          and b.target_name_clean REGEXP_CONTAINS  concat('(^|[ ])', a.target_name_clean, '($|[ ])')
          and a.search_addon_string=b.search_addon_string
          and a.search_result_type=b.search_result_type
          and a.score_type=b.score_type
          and a.run_code=b.run_code
  GROUP BY a.uid;
  
    
create view `peaceful-signer-187500.altdg_merchtag.fm_output_substr_ratios` as 
select input_string, target_sub target_name, run_code, score_type, avg(substr_ratio) avg_substr_ratio
from `peaceful-signer-187500.altdg_merchtag.fm_output_substr_scores`
group by input_string, target_sub, run_code, score_type





drop view `peaceful-signer-187500.altdg_merchtag.fm_output_joined_substr1`;
create view `peaceful-signer-187500.altdg_merchtag.fm_output_joined_substr1` as
select a.*, b.avg_substr_ratio,
case when b.avg_substr_ratio is not null and b.avg_substr_ratio>.55
then 0
else a.score
end as adj_score1
from `peaceful-signer-187500.altdg_merchtag.fm_output` a
left join `peaceful-signer-187500.altdg_merchtag.fm_output_substr_ratios` b
on a.input_string=b.input_string 
and a.target_name=b.target_name 
and a.run_code=b.run_code 
and a.score_type=b.score_type


GRANT ALL PRIVILEGES ON altdg_merchtag.* TO 'boris.serebrov'@'%';

select * from `peaceful-signer-187500.altdg_merchtag.fm_output` limit 10

select 
REGEXP_REPLACE(REGEXP_REPLACE(uid,"^b'",""),"'$","") uid,
created_at,
REGEXP_REPLACE(REGEXP_REPLACE(uidlist_input_string,"^b'",""),"'$","") uidlist_input_string,
REGEXP_REPLACE(REGEXP_REPLACE(input_string,"^b'",""),"'$","") input_string,
REGEXP_REPLACE(REGEXP_REPLACE(search_addon_string,"^b'",""),"'$","") search_addon_string,
REGEXP_REPLACE(REGEXP_REPLACE(search_result_type,"^b'",""),"'$","") search_result_type,
REGEXP_REPLACE(REGEXP_REPLACE(uid_target,"^b'",""),"'$","") uid_target,
REGEXP_REPLACE(REGEXP_REPLACE(target_name,"^b'",""),"'$","") target_name,
REGEXP_REPLACE(REGEXP_REPLACE(score_type,"^b'",""),"'$","") score_type,
REGEXP_REPLACE(REGEXP_REPLACE(search_result_ranks,"^b'",""),"'$","") search_result_ranks,
score,
REGEXP_REPLACE(REGEXP_REPLACE(run_code,"^b'",""),"'$","") run_code,
REGEXP_REPLACE(REGEXP_REPLACE(target_source,"^b'",""),"'$","") target_source,
REGEXP_REPLACE(REGEXP_REPLACE(target_name_clean,"^b'",""),"'$","") target_name_clean
from `peaceful-signer-187500.altdg_merchtag.fm_output`



create view  `peaceful-signer-187500.altdg_merchtag.fm_output_substr_combined` as 
select a.*, b.avg_substr_ratio,
case when b.avg_substr_ratio is not null and b.avg_substr_ratio>.55
then 0
else a.score
end as adj_score2
from `peaceful-signer-187500.altdg_merchtag.fm_output` a
left join `peaceful-signer-187500.altdg_merchtag.fm_output_substr_ratios` b
on a.input_string=b.input_string 
and a.target_name=b.target_name 
and a.run_code=b.run_code 
and a.score_type=b.score_type;


drop view fm_output_substr_scores;
CREATE 
VIEW `altdg_merchtag`.`fm_output_substr_scores` AS
    SELECT 
        `a`.`uid` AS `uid_fm_output`,
        MAX(`a`.`input_string`) AS `input_string`,
        MAX(`a`.`target_name_clean`) AS `target_sub`,
        MAX(`b`.`target_name_clean`) AS `target_super`,
        MAX(`a`.`search_addon_string`) AS `search_addon_string`,
        MAX(`a`.`search_result_type`) AS `search_result_type`,
        MAX(`a`.`created_at`) AS `created_at`,
        MAX(`a`.`run_code`) AS `run_code`,
        MAX(`a`.`score_type`) AS `score_type`,
        MAX(`a`.`score`) AS `score`,
        (`a`.`score` - MAX(`b`.`score`)) AS `score_substr1`,
        (MAX(`b`.`score`) / (`a`.`score` * 1.0)) AS `substr_ratio`,
        (CASE
            WHEN ((MAX(`b`.`score`) / (`a`.`score` * 1.0)) <= 0.4) THEN 0
            ELSE (`a`.`score` - MAX(`b`.`score`))
        END) AS `score_substr2`
    FROM
        (`altdg_merchtag`.`fm_output` `b`
        LEFT JOIN `altdg_merchtag`.`fm_output` `a` ON (((`a`.`input_string` = `b`.`input_string`)
            AND (`a`.`target_name_clean` <> `b`.`target_name_clean`)
            AND (`b`.`target_name_clean` REGEXP CONCAT('(^|[ ])', `a`.`target_name_clean`, '($|[ ])'))
            AND (`a`.`search_addon_string` = `b`.`search_addon_string`)
            AND (`a`.`search_result_type` = `b`.`search_result_type`)
            AND (`a`.`score_type` = `b`.`score_type`)
            AND (`a`.`run_code` = `b`.`run_code`))))
    GROUP BY `a`.`uid`
    
    
    select * from fm_output_substr_scores limit 100
        
    select * from fm_output limit 100
    
     
    CREATE  INDEX idx_inputs ON `altdg_merchtag`.`fm_output_bak_may2018` (`input_string`);
    CREATE  fulltexgt INDEX idx_tgt_name ON `altdg_merchtag`.`fm_output_bak_may2018` (`target_name_clean`);
    
    ALTER TABLE `altdg_merchtag`.`fm_output` 
ADD FULLTEXT INDEX `idx_search_addon` (`search_addon_string` ASC, `search_result_type` ASC, `score_type` ASC);

    
    insert into  fm_output
    select * from fm_output_bak_may2018
    where run_code = 'T2T-3'
    
    
    ----------------------
    select aa.input_string, aa.search_addon_string, aa.search_result_type, aa.target_name,  aa.adj_score2 score , bb.match_target_official input_str_offc , aa.target_name_offc,
cnt_adj_mult, cnt_adj_mult*adj_score2 score_frq_adj
from   

(select a.* , b.match_target_official target_name_offc, is_org
from  `peaceful-signer-187500.altdg_merchtag.fm_output_substr_combined` a 
join `peaceful-signer-187500.altdg_merchtag.targets_collapsed_offNames_cleanCol`  b
on a.target_name=b.match_target
join `peaceful-signer-187500.altdg_merchtag.officialName_BEFcheck`  c
on b.match_target_official=c.official_name
where match_target_good=1
and c.is_org = 1
and run_code ='T2T-3'
and score_type='weighted') aa

join  `peaceful-signer-187500.altdg_merchtag.targets_collapsed_offNames_cleanCol` bb
on aa.input_string=bb.match_target
join `peaceful-signer-187500.altdg_merchtag.officialName_BEFcheck`  cc
on bb.match_target_official=cc.official_name
left join `peaceful-signer-187500.altdg_merchtag.fm_output_m2t_based_freq_adj_tbl` fr
on fr.target_name=aa.target_name
where match_target_good=1
and cc.is_org = 1