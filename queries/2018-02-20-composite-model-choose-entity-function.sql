-- The function to select one entity for the given merchant_string
DROP FUNCTION IF EXISTS select_entity;
DELIMITER //
CREATE FUNCTION select_entity(merchant_string TEXT)
RETURNS TEXT
DETERMINISTIC
BEGIN
   DECLARE wf_entity_name TEXT;
   DECLARE wf_match_target TEXT;
   DECLARE bf_web_entity_name TEXT;
   DECLARE bf_web_match_target TEXT;
   DECLARE bf_entity_entity_name TEXT;
   DECLARE bf_entity_match_target TEXT;
   DECLARE ft_entity_name TEXT;
   DECLARE ft_match_target TEXT;
   DECLARE fw_entity_name TEXT;
   DECLARE fw_match_target TEXT;

   DECLARE equal_count INTEGER;
   DECLARE match_target TEXT;
   DECLARE entity_name TEXT;
   DECLARE uid_match_target TEXT;

   SELECT wf.child_name
          wf_map.match_target
     INTO wf_entity_name, wf_match_target
     FROM wf_output_unique_merch wf
     JOIN entity_match_target_map wf_map ON wf_map.entity_name = wf.child_name
    WHERE wf.input_string = merchant_string
    LIMIT 1
   ;

   SELECT bf.entity_name
          bf_map.match_target
     INTO bf_web_entity_name, bf_web_match_target
     FROM bing_entityfinder_output bf
     JOIN entity_match_target_map bf_map ON bf_map.entity_name = bf.entity_name
    where bf.merchant_string = merchant_string
      AND bf.bing_api_type = 'web-entity'
      AND bf.run_code = 'full_run_2' AND bf.uid IS NOT NULL
    LIMIT 1
   ;

   SELECT bf.entity_name
          bf_map.match_target
     INTO bf_entity_entity_name, bf_entity_match_target
     FROM bing_entityfinder_output bf
     JOIN entity_match_target_map bf_map ON bf_map.entity_name = bf.entity_name
    where bf.merchant_string = merchant_string
      AND bf.bing_api_type = 'entity-entity'
      AND bf.run_code = 'full_run_2' AND bf.uid IS NOT NULL
    LIMIT 1
   ;

   SELECT ft.company_name
          ft_map.match_target
     INTO ft_entity_name, ft_match_target
     FROM ft_output ft
     JOIN entity_match_target_map ft_map ON ft_map.entity_name = ft.company_name
    WHERE ft.input_string = merchant_string
    LIMIT 1
   ;

   SELECT fw_map.entity_name
          fw.target_name
     INTO fw_entity_name, fw_match_target
     FROM fw_response_pivot_GE fw
     JOIN entity_match_target_map fw_map ON fw_map.match_target = fw.target_name
    WHERE fw.search_target = merchant_string
      AND fw.predict_pred = 1
    LIMIT 1
   ;

   -- We will return the entity_match_target_map.uid_match_target from the function.
   -- Probably, it would be good to select entity name and match target independently,
   -- as now we are returning uid_match_target from the entity_match_target_map, this
   -- way we have to have the match target record in order to have the result
   -- (in general case we could not have the record in entity_match_target_map).
   --
   -- Problem with returning two values (entity name and match target) is technical:
   -- if we use MySQL function, we can't return two values
   -- if we use MySQL procedure, we can't use the result in query (select / join)
   -- so for now we use function with one returning value.
   --
   -- An alternative solution could be to use declarative SQL to find the entity, but
   -- for now I have no clear idea how to do that (and also the result will probably
   -- not as clear as function)


  -- Get numbers of equal match targets for different sources
  SELECT match_target, count(*)
  INTO match_target, equal_count
  from (
     select wf_match_target as match_target, 'wf' as source union
     select bf_web_match_target as match_target, 'bf_web' as source union
     select bf_entity_match_target as match_target, 'bf_entity' as source union
     select ft_match_target as match_target, 'ft' as source union
     select fw_match_target as match_target, 'fw' as source
  ) as targets
  group by match_target
  order by count(*) desc
  limit 1;

  IF equal_count >= 2
    -- If there are 2 or more sources that have the same match target,
    -- return it.
    SELECT uid_match_target
      INTO uid_match_target
      FROM entity_match_target_map
     WHERE entity_match_target_map.match_target = match_target
    ;
    RETURN uid_match_target
  END IF;

  -- Get numbers of equal entity names for different sources
  SELECT entity_name, count(*)
  INTO entity_name, equal_count
  from (
     select SUBSTR(wf_entity_name, 1, 10) as entity_name, 'wf' as source union
     select SUBSTR(bf_web_entity_name, 1, 10) as entity_name, 'bf_web' as source union
     select SUBSTR(bf_entity_entity_name, 1, 10) as entity_name, 'bf_entity' as source union
     select SUBSTR(ft_entity_name, 1, 10) as entity_name, 'ft' as source union
     select SUBSTR(fw_entity_name, 1, 10) as entity_name, 'fw' as source
  ) as targets
  group by entity_name
  order by count(*) desc
  limit 1;

  IF equal_count >= 2
    -- If there are 2 or more sources that have the same match target,
    -- return it.
    SELECT uid_match_target
      INTO uid_match_target
      FROM entity_match_target_map
     WHERE SUBSTR(entity_match_target_map.entity_name, 1, 10) = entity_name
    ;
    RETURN uid_match_target
  END IF;

  RETURN NULL;
END
//
DELIMITER ;

select select_entity('Embeya');

select agg_merch_master_top_merchants_unique.merchant_string,
       entity.*
  from agg_merch_master_top_merchants_unique
  join entity_match_target_map entity
    on entity.uid_match_target = select_entity(agg_merch_master.merchant_string)
 limit 10;
