SELECT
    entity_data.entity_name       AS entity_name,
    entity_data.match_target      AS match_target,
    entity_data.source            AS source
FROM
((
        SELECT
            wf.child_name              AS entity_name,
            wf_map.match_target        AS match_target,
            'wf_child'                 AS source
        FROM wf_output_unique_merch_tbl wf
        LEFT JOIN entity_match_target_map wf_map ON wf_map.entity_name = wf.child_name
        WHERE wf.child_name is not null

) UNION (

        SELECT
            wf.parent1_name            AS entity_name,
            wf_map.match_target        AS match_target,
            'wf_parent'                AS source
        FROM wf_output_unique_merch_tbl wf
        LEFT JOIN entity_match_target_map wf_map ON wf_map.entity_name = wf.child_name
        WHERE wf.parent1_name is not null

) UNION (
        SELECT
            bf.entity_name              AS entity_name,
            bf_map.match_target         AS match_target,
            'bf-web-and-entity'         AS source

        FROM bing_entityfinder_output_two_matches bf
        LEFT JOIN entity_match_target_map bf_map ON bf_map.entity_name = bf.entity_name
        WHERE bf.entity_name is not null

) UNION (
        SELECT
            bf.entity_name              AS entity_name,
            bf_map.match_target         AS match_target,
            'bf-web'                    AS source
        FROM bing_entityfinder_output_unique bf
        LEFT JOIN entity_match_target_map bf_map ON bf_map.entity_name = bf.entity_name
        WHERE bf.bing_api_type = 'web-entity' and bf.entity_name is not null

) UNION (
        SELECT
            bf.entity_name              AS entity_name,
            bf_map.match_target         AS match_target,
            'bf-entity'                 AS source
        FROM bing_entityfinder_output_unique bf
        LEFT JOIN entity_match_target_map bf_map ON bf_map.entity_name = bf.entity_name
        WHERE bf.bing_api_type = 'entity-entity' and bf.entity_name is not null
) UNION (
        SELECT
            ft.company_name              AS entity_name,
            ft_map.match_target          AS match_target,
            'ft'                         AS source
        FROM ft_output ft
        LEFT JOIN entity_match_target_map ft_map ON ft_map.entity_name = ft.company_name
        WHERE ft.company_name is not null
) UNION (
        SELECT
            fw.entity_name              AS entity_name,
            fw.match_target             AS match_target,
            'fw'                        AS source 
        FROM
            fw_m2t fw
        WHERE fw.entity_name is not null
)) entity_data;