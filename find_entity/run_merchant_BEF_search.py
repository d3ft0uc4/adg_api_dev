"""
Bing Entity Finder (BEF)
© 2018 Alternative Data Group. All Rights Reserved.

TODO: Move this to a requirements file and doc that here.
Requires Python 3 and:
    pandas>=0.22.0
    sqlalchemy>=1.2.1
    pymysql>=0.8.0

Run as module from root dir:
    python3 -m find_entity.run_merchant_BEF_search

"""

import os

import pandas as pd
import sqlalchemy

from mapper.settings import SQL_CONNECTION_STRING
from packages.settings import TMP_DIR

from bing.bing_entity_entity import BingEntityEntity
from bing.bing_web_entity import BingWebEntity

from find_entity.entity_results import EntityResults
from find_entity.settings import CSV_RESULTS_FILE_NAME, ENTITY_OUTPUT_DB_TABLE_NAME, \
    MERCHANTS_DB_TABLE_NAME, MERCHANT_LIST_FILE_PATH, NUM_OF_RERUNS, BEF_RUN_CODE, \
    SHOULD_READ_INTERMEDIATE, SHOULD_WRITE_RESULTS_TO_CSV, \
    SHOULD_WRITE_RESULTS_TO_DB, NUM_OF_MERCHANTS_TO_QUERY


class BingEntityFinder:
    """ Retrieves a list of merchants from the DB, queries bing for entities, and saves the results to the DB. """

    def __init__(self, run_code=BEF_RUN_CODE, num_merchants=100):
        """
        Constructor, sets _num_merchants (100 by default).
        :param run_code:
        :param num_merchants:
        """
        self.run_code = run_code
        self.num_merchants = num_merchants

    def run(self):
        """
        Runs BEF process `NUM_OF_RERUNS` times, calling `run_iteration`, prints results report.
        TODO: Complete docstring
        """

        any_successful_query_per_iteration = []
        for i in range(NUM_OF_RERUNS):
            any_successful_query = self.run_iteration()
            any_successful_query_per_iteration.append(any_successful_query)

            if i == 1:
                if not any_successful_query:
                    print('BEF: [WARN] There were no new entities found in the 2nd run. Aborting additional reruns.')
                    break

        # DEBUG:
        print()
        print('BEF: [DEBUG] Iteration report:')
        print("\t# | Any new entities found")
        print("\t------------------------")
        for i, any_successful_query in enumerate(any_successful_query_per_iteration):
            print(f'\t{(i + 1)} | {any_successful_query}')
        print()

    def run_iteration(self):
        """
        Main method in class. Process reads (self.num_merchants) merchants from `MERCHANTS_DB_TABLE_NAME` db table,
        Gets `results_entity_entity`, `result_web_entity`...
        TODO: Complete docstring
        :return: ???
        """

        # TODO: Use class property and connect only once.
        engine = sqlalchemy.create_engine(SQL_CONNECTION_STRING)

        merchants_table = self._read_table(engine, MERCHANTS_DB_TABLE_NAME)
        # FIXME: MERCHANT_LIST_FILE_PATH not used.

        if not os.path.isdir(TMP_DIR):
            os.mkdir(TMP_DIR)

        def get_company_data(bing_api_type):
            uids_with_entity = self._get_uids(engine, bing_api_type)
            # Only keep input strings that haven't already been outputted.
            return list(filter(lambda data: data[0] not in uids_with_entity,
                               zip(merchants_table['uidlist_agg_merch_master_top_merchants'],
                                   merchants_table['merchant_string'])))

        results_entity_entity, result_web_entity = self._query_bing_entities(get_company_data('entity-entity'),
                                                                             get_company_data('web-entity'))
        df = pd.concat([results_entity_entity.as_data_frame(), result_web_entity.as_data_frame()])

        print(f'BEF: [INFO] Saving entity results for {len(df)} companies.')
        print(df)

        csv_results_file_path = os.path.join(TMP_DIR, CSV_RESULTS_FILE_NAME)

        if SHOULD_WRITE_RESULTS_TO_CSV:
            if not os.path.exists(csv_results_file_path):
                header = True
                mode = 'w'
            else:
                header = False
                mode = 'a'

            try:
                df.to_csv(csv_results_file_path, mode=mode, header=header, index=False)
            except Exception as ex:
                print('BEF: [ERROR] Unable to write merchant entity dataframe to a CSV file:', str(ex))

        if SHOULD_WRITE_RESULTS_TO_DB:
            try:
                print('BEF: [INFO] Saving the queried entity_results to the DB.')
                df.to_sql(ENTITY_OUTPUT_DB_TABLE_NAME, engine, if_exists='append', index=False)
                print('BEF: [INFO] The queried entity_results were saved to the DB.')
            except Exception as ex:
                print('BEF: [ERROR] Unable to write merchant entity dataframe to DB:', str(ex))

        return any(df['entity_name'])

    def _read_table(self, engine, db_table_name=None, file_path=None):
        """
        Retrieves company UIDs and its names from the DB (`db_table_name`), or from `file_path`.
        FIXME: file_path not used.
        """

        if db_table_name:
            print(f'BEF: [INFO] Retrieving `{db_table_name}` table from the DB.')
            table = pd.read_sql(f'SELECT * FROM `{db_table_name}` LIMIT {self.num_merchants}', engine)
            print(f'BEF: [INFO] Retrieved {len(table)} lines:')
        elif file_path:
            uids = []
            merchant_strings = []
            with open(file_path) as f:
                for line in f:
                    parts = line.split(';')
                    uids.append(parts[0].strip())
                    merchant_strings.append(parts[1].strip())
            table = {
                'uid': uids,
                'merchant_string': merchant_strings,
            }
        else:
            raise Exception("BEF: [CRIT] _read_table expects either db_table_name or file_path params sent.")
        return table

    def _get_uids(self, engine, bing_api_type):
        """ Reads all the uids that have already been queried for this run code & `bing_api_type`. """

        table = pd.read_sql(f'SELECT uidlist_agg_merch_master, entity_name FROM {ENTITY_OUTPUT_DB_TABLE_NAME} '
                            f'WHERE run_code = "{self.run_code}" AND bing_api_type = "{bing_api_type}"', engine)

        uids_with_entity = []
        for _, row in table.iterrows():
            uid = row[0]
            if row[1] is not None:
                uids_with_entity.append(uid)

        return uids_with_entity

    def _query_bing_entities(self, search_data_for_entity_entity, search_data_for_web_entity):
        """ Returns bing entity search results for provided search data params. """

        if not SHOULD_READ_INTERMEDIATE:
            print('BEF: [DEBUG] Querying Bing.')  # DEBUG
            results_entity_entity = EntityResults(
                contents=BingEntityEntity().bulk_query(search_data_for_entity_entity))
            results_web_entity = EntityResults(
                contents=BingWebEntity().bulk_query(search_data_for_web_entity))
            print('BEF: [DEBUG] Bing query finished.')  # DEBUG
        else:
            intermediate_json_dir = os.path.join(TMP_DIR, self.run_code)

            def read_intermediate_results(intermediate_json_dir):
                contents = {}
                for file_name in os.listdir(intermediate_json_dir):
                    uid = file_name.split('$%&')[0]
                    json_data = os.path.join(intermediate_json_dir, file_name)
                    contents = (uid, json_data)
                results = EntityResults(contents=contents)
                return results

            print('BEF: [DEBUG] Reading intermediate JSON files.')  # DEBUG
            results_entity_entity = read_intermediate_results(os.path.join(intermediate_json_dir,
                                                                           'entity-entity'))
            results_web_entity = read_intermediate_results(os.path.join(intermediate_json_dir, 'entity-entity'))

        return results_entity_entity, results_web_entity


if '__main__' == __name__:
    BingEntityFinder(
        num_merchants=NUM_OF_MERCHANTS_TO_QUERY).run()  # NUM_OF_MERCHANTS_TO_QUERY=2 by default (find_entity.settings)
