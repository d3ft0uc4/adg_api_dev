"""
© 2018 Alternative Data Group. All Rights Reserved.

Find Entity settings file
"""

#####################
# Run Configuration #
#####################

NUM_OF_MERCHANTS_TO_QUERY = 2

QUERY_BATCH_SIZE = 1000

BEF_RUN_CODE = 'A1'

SHOULD_READ_INTERMEDIATE = False
SHOULD_WRITE_RESULTS_TO_CSV = False
SHOULD_WRITE_RESULTS_TO_DB = True


############
# Settings #
############

CSV_RESULTS_FILE_NAME = '!entity_results.csv'

ENTITY_OUTPUT_DB_TABLE_NAME = 'bing_entityfinder_output'

MERCHANTS_DB_TABLE_NAME = 'agg_merch_master_top_merchants_unique'  # View
MERCHANT_LIST_FILE_PATH = 'merchant_list.txt'

NUM_OF_RERUNS = 1
