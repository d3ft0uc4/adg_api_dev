"""
© 2018 Alternative Data Group. All Rights Reserved.

Requires Python 3 and:
    joblib==0.11
    pandas==0.22.0

"""

import json
import os
import time

import pandas as pd

from datetime import datetime

from joblib import Parallel, delayed

from find_entity.settings import BEF_RUN_CODE
from packages.utils import uid as uid_func


class EntityResults:

    def __init__(self, contents):
        self.contents = contents

    def save_intermediate_jsons(self, folder):
        os.makedirs(folder, exist_ok=True)

        for key, data in self.contents:
            string = key + '$%&' + data['input']
            out_filename = os.path.join(folder, string.replace('/', '_') + '.json')
            with open(out_filename, 'w') as fp:
                json.dump(data, fp)

    def as_data_frame(self):
        """ Converts the bing response to a dataframe. """

        df = pd.DataFrame(columns=['merchant_string', 'uidlist_agg_merch_master', 'entity_name', 'entity_url',
                                   'entity_scenario', 'entity_type_hints', 'entity_type_display_hint', 'uid'])

        for uid, json_value in self.contents:
            merchant = json_value['input']
            bing_api_type = json_value['bing_api_type']
            entities = json_value['query_results']

            name = None
            url = None
            scenario = None
            entity_type_hints = None
            entity_type_display_hint = None

            if len(entities) > 0:
                entity = entities[0]
                name = entity['name']
                if 'url' in entity:
                    url = entity['url']

                entity_presentation_info = entity['entityPresentationInfo']
                scenario = entity_presentation_info['entityScenario']

                entity_type_hits_list = entity_presentation_info.get('entityTypeHints', None)
                if entity_type_hits_list:
                    entity_type_hints = ','.join(entity_type_hits_list)
                else:
                    entity_type_hints = None

                entity_type_display_hint = entity_presentation_info.get('entityTypeDisplayHint', None)

                print(name, url, scenario, entity_type_hints, entity_type_display_hint)

            df = df.append({
                'created_at': datetime.now(),
                'merchant_string': merchant,
                'uidlist_agg_merch_master': uid,
                'entity_name': name,
                'entity_url': url,
                'entity_scenario': scenario,
                'entity_type_hints': entity_type_hints,
                'entity_type_display_hint': entity_type_display_hint,
                'run_code': BEF_RUN_CODE,
                'bing_api_type': bing_api_type,
                'uid': uid_func(),
            }, ignore_index=True)

        return df


if __name__ == '__main__':
    input_data = [('1', 'Microsoft'), ('2', 'Facebook')]
    r = BulkEntityRunner().bulk_parallel_run(input_data).contents

    print(json.dumps(r, indent=4))
